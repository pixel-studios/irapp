package com.irplus.core.dashboard;

import com.irplus.dto.CustomerGroupingMngmntInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

public interface DashboardCore {
	
	public IRPlusResponseDetails getDashboard(String siteId) throws BusinessException;

	public IRPlusResponseDetails listAllFiles(Integer userId) throws BusinessException;
	public IRPlusResponseDetails BankwiseFile(CustomerGroupingMngmntInfo user,Long branchId) throws BusinessException;
}
