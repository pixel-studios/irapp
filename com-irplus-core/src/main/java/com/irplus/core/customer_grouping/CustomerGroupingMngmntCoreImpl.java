package com.irplus.core.customer_grouping;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.client.customer_grouping.ICustomerGroupingMngmntDaoClient;
import com.irplus.dto.CustomerGroupingDetails;
import com.irplus.dto.CustomerGroupingMngmntInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.util.BusinessException;

@Component
public class CustomerGroupingMngmntCoreImpl implements ICustomerGroupingMngmntCore{

	private static final Logger log = Logger.getLogger(CustomerGroupingMngmntCoreImpl.class);
	
	@Autowired
	ICustomerGroupingMngmntDaoClient iCustomerGroupingMngmntDaoClient;

	@Override
	public IRPlusResponseDetails createCustomerGrp(CustomerGroupingMngmntInfo customerGroupingMngmntInfo)
			throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerGroupingMngmntDaoClient.createCustomerGrp(customerGroupingMngmntInfo);
	
		
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails volumeTrendSummary(CustomerGroupingMngmntInfo trendDateRange)
			throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerGroupingMngmntDaoClient.volumeTrendSummary(trendDateRange);
	
		
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails getdatacustomerDatewise(CustomerGroupingMngmntInfo customerdatewiseinfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerGroupingMngmntDaoClient.getdatacustomerDatewise(customerdatewiseinfo);
		
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails getcustomergroupDatewise(CustomerGroupingMngmntInfo customergrpdateinfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerGroupingMngmntDaoClient.getcustomergroupDatewise(customergrpdateinfo);
		
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails filterCustomerdatewise(CustomerGroupingMngmntInfo customerdateinfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerGroupingMngmntDaoClient.filterCustomerdatewise(customerdateinfo);
		
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails showgroupingReport(CustomerGroupingMngmntInfo customerInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerGroupingMngmntDaoClient.showgroupingReport(customerInfo);
		
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails getcustomerdateWise(CustomerGroupingMngmntInfo customerdateinfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerGroupingMngmntDaoClient.getcustomerdateWise(customerdateinfo);
		
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails showcustomerWiseReport() throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerGroupingMngmntDaoClient.showcustomerWiseReport();
		
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails showOneCustomerGrpById(String customerGroupingMngmntId) throws BusinessException {
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerGroupingMngmntDaoClient.showOneCustomerGrpById(customerGroupingMngmntId);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateOneCustomerGrps(CustomerGroupingMngmntInfo customerGroupingMngmntInfo)
			throws BusinessException {
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerGroupingMngmntDaoClient.updateOneCustomerGrps(customerGroupingMngmntInfo);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails statusUpdatCustmrGrpById(StatusIdInfo statusIdInfo) throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerGroupingMngmntDaoClient.statusUpdatCustmrGrpById(statusIdInfo);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getCustomrGrpDtalsViewById(String customerGroupingMngmntId) throws BusinessException {
	
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerGroupingMngmntDaoClient.getCustomrGrpDtalsViewById(customerGroupingMngmntId);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails showAllCustomerGrps() throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntDaoClient.showAllCustomerGrps();
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails bankIdArrays() throws BusinessException{

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerGroupingMngmntDaoClient.bankIdArrays();
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails filterGrpdatewise(CustomerGroupingMngmntInfo grpdateinfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerGroupingMngmntDaoClient.filterGrpdatewise(grpdateinfo);
		
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails custmrIdArrays(String barnchId) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerGroupingMngmntDaoClient.custmrIdArrays(barnchId);
		
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails createCustomerMappingGroup(CustomerGroupingMngmntInfo customergroupingMngmntInfo)
			throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerGroupingMngmntDaoClient.createCustomerMappingGroup(customergroupingMngmntInfo);
	
		irPlusResponseDetails = iCustomerGroupingMngmntDaoClient.addMappingDetails(customergroupingMngmntInfo.getCustomergroupingDetails());
		return irPlusResponseDetails;
	}
	
	
	@Override
	public IRPlusResponseDetails showAllCustomerGroup() throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerGroupingMngmntDaoClient.showAllCustomerGroup();
		
		return irPlusResponseDetails;
	}
	
	
	@Override
	public IRPlusResponseDetails deleteGroup(CustomerGroupingMngmntInfo groupInfo)
			throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerGroupingMngmntDaoClient.deleteGroup(groupInfo);
	
		irPlusResponseDetails =  iCustomerGroupingMngmntDaoClient.deleteGroupDetails(groupInfo.getCustomergroupingDetails());
		return irPlusResponseDetails;
	}
	
	

	@Override
	public IRPlusResponseDetails getcustomergroupInfo(Integer customerGrpId) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
	
		irPlusResponseDetails = iCustomerGroupingMngmntDaoClient.getcustomergroupInfo(customerGrpId);
		
		return irPlusResponseDetails;
	}
	
/*	@Override
	public IRPlusResponseDetails EditCustGrp(CustomerGroupingDetails editgroupInfo)
			throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerGroupingMngmntDaoClient.EditCustGrp(editgroupInfo);
	
		
		return irPlusResponseDetails;
	}*/
	
	@Override
	public IRPlusResponseDetails updateCustomerMappingGroup(CustomerGroupingMngmntInfo customergroupingMngmntInfo)
			throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerGroupingMngmntDaoClient.updateCustomerMappingGroup(customergroupingMngmntInfo);
	
		irPlusResponseDetails = iCustomerGroupingMngmntDaoClient.updatecustMappingDetails(customergroupingMngmntInfo.getCustomergroupingDetails());
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails showVolumeTrendReportWeekwise() throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerGroupingMngmntDaoClient.showVolumeTrendReportWeekwise();
		
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails volumeTrendrangewise(CustomerGroupingMngmntInfo trendRange)
			throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerGroupingMngmntDaoClient.volumeTrendrangewise(trendRange);
	
		
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails showVolumeTrendReport(CustomerGroupingMngmntInfo currentDate) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerGroupingMngmntDaoClient.showVolumeTrendReport(currentDate);
		
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails getbankwiseReport(String bankid) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerGroupingMngmntDaoClient.getbankwiseReport(bankid);
		
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails getbankDaywiseReport(String bankid) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerGroupingMngmntDaoClient.getbankDaywiseReport(bankid);
		
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails volumeTrendBankrangewise(CustomerGroupingMngmntInfo BankRange) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerGroupingMngmntDaoClient.volumeTrendBankrangewise(BankRange);
		
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails CustomerGrpFilter(CustomerGroupingMngmntInfo GrpListInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerGroupingMngmntDaoClient.CustomerGrpFilter(GrpListInfo);
		
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails showDaywiseReport(CustomerGroupingMngmntInfo currentDate) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntDaoClient.showDaywiseReport(currentDate);
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails showDaywiseCustomerReport(CustomerGroupingMngmntInfo customerInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntDaoClient.showDaywiseCustomerReport(customerInfo);
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails showDaywiseCustomerGroupReport(CustomerGroupingMngmntInfo customerInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntDaoClient.showDaywiseCustomerGroupReport(customerInfo);
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails DaycustomerReport(CustomerGroupingMngmntInfo customerInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntDaoClient.DaycustomerReport(customerInfo);
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails DaycustomerGroupReport(CustomerGroupingMngmntInfo customerInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntDaoClient.DaycustomerGroupReport(customerInfo);
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails getGroupCustomerwise(CustomerGroupingMngmntInfo customerInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntDaoClient.getGroupCustomerwise(customerInfo);
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails filterGroups(CustomerGroupingMngmntInfo grpFilterInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntDaoClient.filterGroups(grpFilterInfo);
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails AutocompleteGroup(String term,String userid) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntDaoClient.AutocompleteGroup(term,userid);
		return irPlusResponseDetails;
	}
}