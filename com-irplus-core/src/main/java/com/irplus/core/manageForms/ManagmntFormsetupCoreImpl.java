package com.irplus.core.manageForms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.client.manageForms.IManagmntFormsetupDaoClient;
import com.irplus.dto.FormsManagementDTO;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.util.BusinessException;


@Component
public class ManagmntFormsetupCoreImpl implements IManagmntFormsetupCore{

	@Autowired
	IManagmntFormsetupDaoClient iManagmntFormsetupDaoClient;
	
	@Override
	public IRPlusResponseDetails createMngmntformSetup(FormsManagementDTO formsManagementDTO) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		irPlusResponseDetails =  iManagmntFormsetupDaoClient.createMngmntformSetup(formsManagementDTO);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails dynamicSelectFieldTypes() throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		irPlusResponseDetails =  iManagmntFormsetupDaoClient.dynamicSelectFieldTypes();
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails showOneMngmntformSetup(String id) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		irPlusResponseDetails =  iManagmntFormsetupDaoClient.showOneMngmntformSetup(id);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateMngmntformSetup(FormsManagementDTO formsManagementDTO) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		irPlusResponseDetails =  iManagmntFormsetupDaoClient.updateMngmntformSetup(formsManagementDTO);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails statusUpdate(StatusIdInfo statusIdInfo) throws BusinessException {		

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		irPlusResponseDetails =  iManagmntFormsetupDaoClient.statusUpdate(statusIdInfo);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails showListofForms() throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 		
		irPlusResponseDetails =  iManagmntFormsetupDaoClient.showListofForms();
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails showAllCustomerForms(Integer customerid) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		irPlusResponseDetails =  iManagmntFormsetupDaoClient.showAllCustomerForms(customerid);
		return irPlusResponseDetails;
	}
	
}