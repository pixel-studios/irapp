package com.irplus.core.dbinfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.client.dbinfo.IDBPropertiesDaoClient;
import com.irplus.dto.DBDTO;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

@Component
public class IDBPropertiesCoreImpl implements IDBPropertiesCore{

	@Autowired
	IDBPropertiesDaoClient dbpro;
	
	@Override
	public IRPlusResponseDetails createDBProperties(DBDTO dbdto) throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		irPlusResponseDetails = dbpro.createDBProperties(dbdto);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateDBProperties(DBDTO dbdto) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		irPlusResponseDetails = dbpro.updateDBProperties(dbdto);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails showDBProperties() throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		irPlusResponseDetails = dbpro.showDBProperties();
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails deleteDBProperties(DBDTO dbdto) throws BusinessException{

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		irPlusResponseDetails = dbpro.deleteDBProperties(dbdto);
		return irPlusResponseDetails;
	}

	
}
