package com.irplus.core.customerMappingMngmnt;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.client.customerMappingMngmnt.ICustomerMappingDaoClient;
import com.irplus.dto.CustomerMapSupportInfo;
import com.irplus.dto.CustomerMappingInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.util.BusinessException;

@Component
public class CustomerMappingCoreImpl implements ICustomerMappingCore{

	private static final Logger log = Logger.getLogger(CustomerMappingCoreImpl.class);
	
	@Autowired 
	ICustomerMappingDaoClient iCustomerMappingDaoClient;

	@Override
	public IRPlusResponseDetails getCustomerMappingById(String custommerId) throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerMappingDaoClient.getCustomerMappingById(custommerId);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateCustomerMapping(CustomerMappingInfo customerMappingInfo)
			throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails  =  iCustomerMappingDaoClient.updateCustomerMapping(customerMappingInfo);
		
	
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails showAllCustomerMapping() throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerMappingDaoClient.showAllCustomerMapping();
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails statusUpdateCustmrMpng(StatusIdInfo statusIdInfo) throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerMappingDaoClient.statusUpdateCustmrMpng(statusIdInfo);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails createCustomerMapping(CustomerMapSupportInfo customerMapSupportInfo)
			throws BusinessException {
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerMappingDaoClient.createCustomerMapping(customerMapSupportInfo);
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails CustomerBankingBanks(String SiteId) throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerMappingDaoClient.CustomerBankingBanks(SiteId);
		return irPlusResponseDetails;
	}
	
	
	@Override
	public IRPlusResponseDetails showBranchFile(String branchId) throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerMappingDaoClient.showBranchFile(branchId);
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails showBranchCustomer(String branchId) throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerMappingDaoClient.showBranchCustomer(branchId);
		return irPlusResponseDetails;
	}
	
	
	@Override
	public IRPlusResponseDetails createCustomerMappingFields(CustomerMappingInfo[] customerMappingInfo)
			throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails  =  iCustomerMappingDaoClient.createCustomerMappingFields(customerMappingInfo);
		
	
		return irPlusResponseDetails;
	}
	
	
	@Override
	public IRPlusResponseDetails showAllCustomerMappedFields() throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerMappingDaoClient.showAllCustomerMappedFields();
		
		return irPlusResponseDetails;
	}
}