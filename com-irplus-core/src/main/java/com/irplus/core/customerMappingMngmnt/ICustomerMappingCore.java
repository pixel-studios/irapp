package com.irplus.core.customerMappingMngmnt;

import com.irplus.dto.CustomerMapSupportInfo;
import com.irplus.dto.CustomerMappingInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.util.BusinessException;

public interface ICustomerMappingCore {


	public IRPlusResponseDetails createCustomerMapping(CustomerMapSupportInfo customerMapSupportInfo)
			throws BusinessException;
	public IRPlusResponseDetails getCustomerMappingById(String custommerId) throws BusinessException;

	public IRPlusResponseDetails updateCustomerMapping(CustomerMappingInfo customerMappingInfo)
			throws BusinessException;

	public IRPlusResponseDetails showAllCustomerMapping() throws BusinessException;

	public IRPlusResponseDetails statusUpdateCustmrMpng(StatusIdInfo statusIdInfo) throws BusinessException;
	public IRPlusResponseDetails CustomerBankingBanks(String SiteId) throws BusinessException;
	public IRPlusResponseDetails showBranchFile(String branchId) throws BusinessException;
	
	public IRPlusResponseDetails showBranchCustomer(String branchId) throws BusinessException;

	public IRPlusResponseDetails createCustomerMappingFields(CustomerMappingInfo[] customerMappingInfo)
			throws BusinessException;
	
	
	public IRPlusResponseDetails showAllCustomerMappedFields() throws BusinessException;
}

