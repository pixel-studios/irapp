package com.irplus.core.sitesetup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.client.sitesetup.SiteSetupDaoClient;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.SiteInfo;
import com.irplus.util.BusinessException;

@Component
public class SiteSetupCoreImpl implements SiteSetupCore{
	
	 @Autowired
	 SiteSetupDaoClient  siteSetupDaoClient;

	@Override
	public IRPlusResponseDetails getSiteInfo(String siteId) throws BusinessException {
		
		return siteSetupDaoClient.getSiteInfo(siteId);
	}

	@Override
	public IRPlusResponseDetails updateSite(SiteInfo siteInfo) throws BusinessException {

		return siteSetupDaoClient.updateSite(siteInfo);
	}

	@Override
	public IRPlusResponseDetails updateFolder(SiteInfo siteInfo) throws BusinessException {
		
		return siteSetupDaoClient.updateFolder(siteInfo);
	}

	@Override
	public IRPlusResponseDetails licenseValidity() throws BusinessException {
		
		return siteSetupDaoClient.licenseValidity();
	}

	@Override
	public IRPlusResponseDetails siteInfo() throws BusinessException {
		// TODO Auto-generated method stub
		return siteSetupDaoClient.siteInfo();
	}

}
