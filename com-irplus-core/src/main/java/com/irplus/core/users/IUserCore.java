package com.irplus.core.users;

import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.UserFilterInfo;
import com.irplus.dto.users.UserBean;
import com.irplus.util.BusinessException;

public interface IUserCore {
		
    public IRPlusResponseDetails createUser(UserBean userBeanInfo) throws BusinessException;

	public IRPlusResponseDetails getUserById(String userId) throws BusinessException;		

	public IRPlusResponseDetails updateUser(UserBean userBeanInfo) throws BusinessException;

	public IRPlusResponseDetails deleteUserById(String userId) throws BusinessException;
		
	public IRPlusResponseDetails findAllUsers(String siteId) throws BusinessException;
	public IRPlusResponseDetails getUserBank(String userId)  throws BusinessException;
	public IRPlusResponseDetails getOtherBank(String bankId) throws BusinessException;
	public IRPlusResponseDetails updateProfile(UserBean userBeanInfo) throws BusinessException;
	public IRPlusResponseDetails createUserNoDup(UserBean userBean) throws BusinessException;

	public IRPlusResponseDetails getAll_Role_Banks(String siteId) throws BusinessException;

	public IRPlusResponseDetails updateUserAndBank(UserBean userBeanInfo) throws BusinessException;

	public IRPlusResponseDetails updateStatusUser(UserBean userBeanInfo) throws BusinessException;
	
	//
	public IRPlusResponseDetails filterUsers(UserFilterInfo userFilterInfo) throws BusinessException ;

	public IRPlusResponseDetails validateUser(UserBean userBean) throws BusinessException;

public IRPlusResponseDetails fetchMenu(int roleId) throws BusinessException;
public IRPlusResponseDetails AutocompleteUsername(String term,String siteId) throws BusinessException; 
	
}
