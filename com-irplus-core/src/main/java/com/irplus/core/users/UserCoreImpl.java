package com.irplus.core.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.client.users.IUserDaoClient;
import com.irplus.dao.users.IUserDao;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.UserFilterInfo;
import com.irplus.dto.users.UserBean;
import com.irplus.util.BusinessException;

@Component
public class UserCoreImpl implements IUserCore{

	@Autowired 
	IUserDaoClient userDaoclient;
	
	@Autowired
	IUserDao userDao;
	
	
	@Override
	public IRPlusResponseDetails createUser(UserBean userBeanInfo) throws BusinessException {
		
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();		
		usersResponseDetails =userDaoclient.createUser(userBeanInfo);
		usersResponseDetails =userDao.createUserBank(userBeanInfo.getUserBank());
		return usersResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getUserById(String userId) throws BusinessException {
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();
		usersResponseDetails = userDaoclient.getUserById(userId);
		return usersResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateUser(UserBean userBeanInfo) throws BusinessException {
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();
		usersResponseDetails = userDaoclient.updateUser(userBeanInfo);
		usersResponseDetails =userDao.updateUserBank(userBeanInfo.getUserBank());
		return usersResponseDetails;
	}

	@Override
	public IRPlusResponseDetails deleteUserById(String userId) throws BusinessException {
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();
		usersResponseDetails = userDaoclient.deleteUserById(userId);
		return usersResponseDetails;
	}

	@Override
	public IRPlusResponseDetails findAllUsers(String siteId) throws BusinessException {
		
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();
		
		usersResponseDetails = userDaoclient.findAllUsers(siteId);
		return usersResponseDetails;
	}

	@Override
	public IRPlusResponseDetails createUserNoDup(UserBean userBean) throws BusinessException {
		
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();
		usersResponseDetails = userDaoclient.createUserNoDup(userBean);
		return usersResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getAll_Role_Banks(String siteId) throws BusinessException {
		
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();
		usersResponseDetails = userDaoclient.getAll_Role_Banks(siteId);
		return usersResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateUserAndBank(UserBean userBeanInfo) throws BusinessException {

		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();
		usersResponseDetails = userDaoclient.updateUserAndBank(userBeanInfo);
		return usersResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateStatusUser(UserBean userBeanInfo) throws BusinessException {
		
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();
		usersResponseDetails = userDaoclient.updateStatusUser(userBeanInfo);
		return usersResponseDetails;
	}

	@Override
	public IRPlusResponseDetails filterUsers(UserFilterInfo userFilterInfo) throws BusinessException {

		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();
		usersResponseDetails = userDaoclient.filterUsers(userFilterInfo);
		return usersResponseDetails;
	}
	@Override
	public IRPlusResponseDetails validateUser(UserBean userBean) throws BusinessException
	{
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();
		
		usersResponseDetails = userDaoclient.validateUser(userBean);
		return usersResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails fetchMenu(int roleId) throws BusinessException
	{
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();
		
		usersResponseDetails = userDaoclient.fetchMenu(roleId);
		return usersResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails getOtherBank(String bankId) throws BusinessException {
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();
		usersResponseDetails = userDaoclient.getOtherBank(bankId);
		return usersResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getUserBank(String userId) throws BusinessException {
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();
		usersResponseDetails = userDaoclient.getUserBank(userId);
		return usersResponseDetails;
	}

	
	@Override
	public IRPlusResponseDetails updateProfile(UserBean userBeanInfo) throws BusinessException {
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();
		usersResponseDetails = userDaoclient.updateUser(userBeanInfo);
		usersResponseDetails =userDao.changeDefaultBank(userBeanInfo.getUserBank());
		return usersResponseDetails;
	}
	@Override
	public IRPlusResponseDetails AutocompleteUsername(String term,String siteId) throws BusinessException {
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();
		usersResponseDetails = userDaoclient.AutocompleteUsername(term,siteId);
		return usersResponseDetails;
}
	
}