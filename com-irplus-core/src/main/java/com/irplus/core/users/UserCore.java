package com.irplus.core.users;

import com.irplus.dto.users.UserBean;
import com.irplus.dto.users.UsersResponseDetails;
import com.irplus.util.BusinessException;

public interface UserCore {
		
    public UsersResponseDetails createUser(UserBean userBeanInfo) throws BusinessException;

	public UsersResponseDetails getUserById(String userId) throws BusinessException;		

	public UsersResponseDetails updateUser(UserBean userBeanInfo) throws BusinessException;

	public UsersResponseDetails deleteUserById(String userId) throws BusinessException;
		
	public UsersResponseDetails findAllUsers() throws BusinessException;
	
}
