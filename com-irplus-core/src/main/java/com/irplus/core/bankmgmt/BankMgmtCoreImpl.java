package com.irplus.core.bankmgmt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.IRPlusDao;
import com.irplus.dao.bankbranchdao.IBankBranchMgmtDao;
import com.irplus.dao.client.bankmgmt.IBankMgmtDaoClient;
import com.irplus.dao.contactmgmt.IContactMgmtDao;
import com.irplus.dto.BankFilterInfo;
import com.irplus.dto.BankInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

@Component
public class BankMgmtCoreImpl implements IBankMgmtCore {

    @Autowired
    IBankMgmtDaoClient iBankMgmtDaoClient;
  
    @Autowired
    IBankBranchMgmtDao iBankBranchMgmtDao;
    
    @Autowired
    IContactMgmtDao iContactMgmtDao;
    
    @Autowired
	IRPlusDao irplusdao;
    
    
    @Transactional
	@Override
	public IRPlusResponseDetails createBank(BankInfo bankInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
	
		irPlusResponseDetails = iBankMgmtDaoClient.createBank(bankInfo);
		
		irPlusResponseDetails = iBankMgmtDaoClient.createBankBranch(bankInfo);
				
		irPlusResponseDetails = iContactMgmtDao.addContacts(bankInfo.getContacts());
		
		irPlusResponseDetails = iBankMgmtDaoClient.addBankLicense(bankInfo);
		
		return irPlusResponseDetails;
		
	}

	@Override
	public IRPlusResponseDetails listBanks(String siteId) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iBankMgmtDaoClient.listBanks(siteId);
		
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails filterBanks(BankFilterInfo bankFilterInfo) throws BusinessException
	{		
		return iBankMgmtDaoClient.filterBanks(bankFilterInfo);
	
	}
	
	@Override
	public IRPlusResponseDetails updateBank(BankInfo bankInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
	
		irPlusResponseDetails = iBankMgmtDaoClient.updateBank(bankInfo);
		
		irPlusResponseDetails = iBankMgmtDaoClient.updateBranch(bankInfo);
		
		irPlusResponseDetails = iContactMgmtDao.updateContacts(bankInfo.getContacts());
		
		irPlusResponseDetails = iBankMgmtDaoClient.addBankLicense(bankInfo);
				
		return irPlusResponseDetails;
		
	}
	
	@Override
	public IRPlusResponseDetails updateBankStatus(BankInfo bankInfo) throws BusinessException {

		
		return iBankMgmtDaoClient.updateBankStatus(bankInfo);
		
		
	}
	@Override
	public IRPlusResponseDetails updateBranchStatus(BankInfo bankInfo) throws BusinessException {
		
		return iBankMgmtDaoClient.updateBranchStatus(bankInfo);
	}
	
	
	@Override
	public IRPlusResponseDetails getBankInfo(String bankId) throws BusinessException
	{
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iBankMgmtDaoClient.getBankInfo(bankId);
		
		BankInfo bankInfo = irPlusResponseDetails.getBank();
		
		
		if(bankInfo!=null)
		{
			Long bankid=bankInfo.getBankId();
			String siteId = bankInfo.getSiteId();
			
			irPlusResponseDetails = iBankMgmtDaoClient.getBranchInfo(bankInfo);
			
			irPlusResponseDetails = iContactMgmtDao.getContacts(bankInfo);
			
			IRPlusResponseDetails irPlusResponseDetails1 = irplusdao.getMasterData(siteId);
			
			irPlusResponseDetails.setMasterData(irPlusResponseDetails1.getMasterData());
			
			
		}
		
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails deleteBank(BankInfo bankInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iBankMgmtDaoClient.deleteBank(bankInfo);
		
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails addLocation(BankInfo bankInfo) throws BusinessException
	{

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
			
		irPlusResponseDetails = iBankBranchMgmtDao.createBankBranch(bankInfo);
		
		irPlusResponseDetails = iContactMgmtDao.addContacts(bankInfo.getContacts());
				
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails updateLocation(BankInfo bankInfo) throws BusinessException
	{
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iBankBranchMgmtDao.updateBranch(bankInfo);
		
		irPlusResponseDetails = iContactMgmtDao.updateContacts(bankInfo.getContacts());
		
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails listLocations(String bankId) throws BusinessException
	{
		return iBankBranchMgmtDao.listBankBranches(bankId);
	}

	@Override
	public IRPlusResponseDetails deleteLocation(BankInfo bankInfo) throws BusinessException
	{
		return iBankBranchMgmtDao.deleteBranch(bankInfo);
	}

	@Override
	public IRPlusResponseDetails getBranchInfo(Long branchId)throws BusinessException
	{
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iBankMgmtDaoClient.getBranchInfo(branchId);
		
		irPlusResponseDetails = iContactMgmtDao.getContacts(irPlusResponseDetails.getBank());
		
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails filterBranches(BankFilterInfo bankFilterInfo) throws BusinessException
	{
		return iBankMgmtDaoClient.filterBranches(bankFilterInfo);
	}
	@Override
	public IRPlusResponseDetails deleteContact(String contactId) throws BusinessException
	{
		return iContactMgmtDao.deleteContact(contactId);
	}

	
}
