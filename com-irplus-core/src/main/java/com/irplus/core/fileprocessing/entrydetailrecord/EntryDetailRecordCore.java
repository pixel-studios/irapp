package com.irplus.core.fileprocessing.entrydetailrecord;

import java.util.List;

import com.irplus.dto.IRPlusResponseDetails;

import com.irplus.dto.fileprocessing.WEBEntryDetailRecordBean;
import com.irplus.util.BusinessException;

public interface EntryDetailRecordCore {

	public Boolean insertRecord(WEBEntryDetailRecordBean entryDetailRecordBean);
	public Boolean deleteRecord(WEBEntryDetailRecordBean entryDetailRecordBean);
    public List<WEBEntryDetailRecordBean> listAllRecords();
    public IRPlusResponseDetails getRecordByEntryId(Integer fileId) throws BusinessException;
    public Boolean updateRecord(WEBEntryDetailRecordBean batchHeaderRecord);
    public List<WEBEntryDetailRecordBean> getByQuery(Integer Id);
}
