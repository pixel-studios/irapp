package com.irplus.core.fileprocessing.fileheaderrecord;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.client.fileprocessing.fileheaderrecord.FileHeaderRecordDaoClient;
import com.irplus.dto.CustomerGroupingMngmntInfo;
import com.irplus.dto.FileTypeInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.fileprocessing.FileHeaderRecordBean;
import com.irplus.util.BusinessException;

@Component
public class FileHeaderRecordCoreImpl implements FileHeaderRecordCore{

	@Autowired
	private FileHeaderRecordDaoClient fileHeaderRecordDaoClient;
	
	@Override
	public Boolean insertRecord(FileHeaderRecordBean fileHeaderRecord) {
		return fileHeaderRecordDaoClient.insertRecord(fileHeaderRecord);
	}

	@Override
	public Boolean deleteRecord(FileHeaderRecordBean fileHeaderRecord) {
		return fileHeaderRecordDaoClient.deleteRecord(fileHeaderRecord);
	}

	@Override
	public IRPlusResponseDetails listAllRecords(String userId)throws BusinessException{
		return fileHeaderRecordDaoClient.listAllRecords(userId);
	}

	@Override
	public IRPlusResponseDetails getRecordByBatchId(Integer fileId) throws BusinessException {
		return fileHeaderRecordDaoClient.getRecordByBatchId(fileId);
	}

	@Override
	public Boolean updateRecord(FileHeaderRecordBean fileHeaderRecord) {
		return fileHeaderRecordDaoClient.updateRecord(fileHeaderRecord);
	}
	@Override
	public IRPlusResponseDetails getMasterDataChart(Integer userId,String siteId) throws BusinessException {
		return fileHeaderRecordDaoClient.getMasterDataChart(userId,siteId);
	}
	@Override
	public IRPlusResponseDetails DaywiseFiles(CustomerGroupingMngmntInfo fileheadrec) throws BusinessException {
		return fileHeaderRecordDaoClient.DaywiseFiles(fileheadrec);
	}
	
	@Override
	public IRPlusResponseDetails DaywiseFilesGroup(CustomerGroupingMngmntInfo fileheadrec) throws BusinessException {
		return fileHeaderRecordDaoClient.DaywiseFilesGroup(fileheadrec);
	}
	@Override
	public IRPlusResponseDetails BankwiseFileList(CustomerGroupingMngmntInfo fileinfo) throws BusinessException {
		return fileHeaderRecordDaoClient.BankwiseFileList(fileinfo);
	}
	@Override
	public IRPlusResponseDetails listfileLog() throws BusinessException {
		// TODO Auto-generated method stub
		return fileHeaderRecordDaoClient.listfileLog();
	}
	
	@Override
	public IRPlusResponseDetails GroupwiseFileList(CustomerGroupingMngmntInfo Grpinfo) throws BusinessException {
		return fileHeaderRecordDaoClient.GroupwiseFileList(Grpinfo);
	}
	
	@Override
	public IRPlusResponseDetails filterFiles(FileTypeInfo Grpinfo) throws BusinessException {
		return fileHeaderRecordDaoClient.filterFiles(Grpinfo);
	}
}
