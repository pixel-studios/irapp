package com.irplus.core.fileprocessing.entrydetailrecord;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.client.fileprocessing.entrydetailrecord.EntryDetailRecordDaoClient;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.fileprocessing.WEBEntryDetailRecordBean;
import com.irplus.util.BusinessException;

@Component
public class EntryDetailRecordCoreImpl implements EntryDetailRecordCore {
	@Autowired
	private EntryDetailRecordDaoClient entryDetailRecordDaoClient;
	
	@Override
	public Boolean insertRecord(WEBEntryDetailRecordBean entryDetailRecordBean) {
		
		return entryDetailRecordDaoClient.insertRecord(entryDetailRecordBean);
	}

	@Override
	public Boolean deleteRecord(WEBEntryDetailRecordBean entryDetailRecordBean) {
		
		return entryDetailRecordDaoClient.deleteRecord(entryDetailRecordBean);
	}

	@Override
	public List<WEBEntryDetailRecordBean> listAllRecords() {
		
		return entryDetailRecordDaoClient.listAllRecords();
	}

	@Override
	public IRPlusResponseDetails getRecordByEntryId(Integer fileId) throws BusinessException {
		
		return entryDetailRecordDaoClient.getRecordByEntryId(fileId);
	}

	@Override
	public Boolean updateRecord(WEBEntryDetailRecordBean batchHeaderRecord) {
		
		return entryDetailRecordDaoClient.updateRecord(batchHeaderRecord);
	}

	@Override
	public List<WEBEntryDetailRecordBean> getByQuery(Integer Id) {
		
		return entryDetailRecordDaoClient.getByQuery(Id);
	}

}
