package com.irplus.core.fileprocessing.batchheaderrecord;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.client.fileprocessing.batchheaderrecord.BatchHeaderRecordDaoClient;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.fileprocessing.BatchHeaderRecordBean;
import com.irplus.util.BusinessException;

@Component
public class BatchHeaderRecordCoreImpl implements BatchHeaderRecordCore{

	@Autowired
	private BatchHeaderRecordDaoClient batchHeaderRecordDaoClient;
	
	@Override
	public Boolean insertRecord(BatchHeaderRecordBean batchHeaderRecord) {
		return batchHeaderRecordDaoClient.insertRecord(batchHeaderRecord);
	}

	@Override
	public Boolean deleteRecord(BatchHeaderRecordBean batchHeaderRecord) {
		return batchHeaderRecordDaoClient.deleteRecord(batchHeaderRecord);
	}

	@Override
	public List<BatchHeaderRecordBean> listAllRecords() {
		return batchHeaderRecordDaoClient.listAllRecords();
	}

	
	@Override
	public Boolean updateRecord(BatchHeaderRecordBean batchHeaderRecord) {
		return batchHeaderRecordDaoClient.updateRecord(batchHeaderRecord);
	}

	@Override
	public List<BatchHeaderRecordBean> getByQuery(Integer Id) {
		return batchHeaderRecordDaoClient.getByQuery(Id);
	}

	@Override
	public IRPlusResponseDetails getRecordByBatchId(Integer fileId) throws BusinessException {
		return batchHeaderRecordDaoClient.getRecordByBatchId(fileId);
	}

}
