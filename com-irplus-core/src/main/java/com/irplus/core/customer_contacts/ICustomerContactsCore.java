package com.irplus.core.customer_contacts;

import com.irplus.dto.CustomeContactsArrayInfo;
import com.irplus.dto.CustomerBean;
import com.irplus.dto.CustomerContactsBean;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

public interface ICustomerContactsCore {

		public IRPlusResponseDetails createCustomerContacts(CustomerContactsBean customerContactsBean)throws BusinessException;
	
	 	public IRPlusResponseDetails addContacts(CustomerContactsBean[] customerContactsBean)throws BusinessException;

	 	public IRPlusResponseDetails getCustomerContactsById(CustomerBean customerBean) throws BusinessException;
		
		public IRPlusResponseDetails updateCustomerConstatnts(CustomerContactsBean customerContactsBean) throws BusinessException;

		public IRPlusResponseDetails deleteCustomerContactsById(String customerContactsId) throws BusinessException;
		
		public IRPlusResponseDetails showAllCustomerContacts() throws BusinessException;
		
		public IRPlusResponseDetails updateCustomerConstactsOnly(CustomeContactsArrayInfo customeContactsArrayInfo) throws BusinessException;
		
		
}
