package com.irplus.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.IRPlusDao;
import com.irplus.dto.BankInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.LicenseInfo;
import com.irplus.util.BusinessException;


@Component
public class IRPlusCore{
	
	@Autowired
	
	IRPlusDao irplusdao;
	
	public IRPlusResponseDetails getMasterData(String siteId) throws BusinessException {

		return irplusdao.getMasterData(siteId);
	}

	public IRPlusResponseDetails validateLicense(LicenseInfo licInfo) throws BusinessException {
		// TODO Auto-generated method stub
		return irplusdao.validateLicense(licInfo);
	}

}
