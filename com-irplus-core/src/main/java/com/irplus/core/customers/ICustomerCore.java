package com.irplus.core.customers;

import com.irplus.dto.CustomerBean;
import com.irplus.dto.CustomerFilter;
import com.irplus.dto.FormsManagementDTO;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.util.BusinessException;

public interface ICustomerCore {

	public IRPlusResponseDetails createCustomer(CustomerBean customerBean) throws BusinessException;

	public IRPlusResponseDetails getCustomerById(CustomerBean customerBean) throws BusinessException;

	public IRPlusResponseDetails updateCustomer(CustomerBean customersBean) throws BusinessException;

	public IRPlusResponseDetails deleteCustomerById(String custommerId) throws BusinessException;

	public IRPlusResponseDetails showAllCustomer(String siteId) throws BusinessException;

	public IRPlusResponseDetails showAllBanks(String SiteId) throws BusinessException;

	public IRPlusResponseDetails updateCutomerStaus(StatusIdInfo statusIdInfo) throws BusinessException;

	public IRPlusResponseDetails customerFilter(CustomerFilter customerFilter) throws BusinessException;

	public IRPlusResponseDetails showAllFileProcessing() throws BusinessException;

	public IRPlusResponseDetails showAllBusinessProcessing() throws BusinessException;
	
	public IRPlusResponseDetails showBankBranch(String bankId) throws BusinessException;
	public IRPlusResponseDetails Autocomplete(String term,String userid) throws BusinessException;   
	
	public IRPlusResponseDetails AutocompleteCustomer(String term,String userid) throws BusinessException; 
	
	public IRPlusResponseDetails AutocompleteCustomerCode(String term,String userid) throws BusinessException; 
	public IRPlusResponseDetails AutocompleteBank(String term,String userid) throws BusinessException; 
	public IRPlusResponseDetails AddCustomerDataEntry(FormsManagementDTO formsManagement) throws BusinessException;

}
