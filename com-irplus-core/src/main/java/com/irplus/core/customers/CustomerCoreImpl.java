package com.irplus.core.customers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.IRPlusDao;
import com.irplus.dao.client.customers.ICustomerDaoClient;
import com.irplus.dao.contactmgmt.IContactMgmtDao;
import com.irplus.dao.customer_contacts.ICustomerContactsDao;
import com.irplus.dto.CustomerBean;
import com.irplus.dto.CustomerFilter;
import com.irplus.dto.CustomerGroupingMngmntInfo;
import com.irplus.dto.FormsManagementDTO;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.util.BusinessException;

@Component
public class CustomerCoreImpl  implements ICustomerCore{

	@Autowired
	ICustomerDaoClient iCustomerDaoClient;
	
	 @Autowired
	 ICustomerContactsDao iCustomerContactsDao;
	
	 @Autowired
	IRPlusDao irplusdao;
	    
	@Transactional
	@Override
	public IRPlusResponseDetails createCustomer(CustomerBean customerBean) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerDaoClient.createCustomer(customerBean);
		
		irPlusResponseDetails = iCustomerContactsDao.addContacts(customerBean.getCustomerContact());
		
		irPlusResponseDetails = iCustomerDaoClient.addCustomerLicense(customerBean);
		
		//irPlusResponseDetails = iCustomerDaoClient.addCustomerOtherLicense(customerBean);
		
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getCustomerById(CustomerBean customerBean) throws BusinessException {
		
		

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerDaoClient.getCustomerById(customerBean);
		
		Long bankId=irPlusResponseDetails.getCustomer().getParentBankId();
		
		System.out.println("bankId ::::::::::::::"+bankId);
			
		IRPlusResponseDetails irPlusResponseDetails1 = irplusdao.getBankProcess(bankId);
		
		irPlusResponseDetails.setMasterData(irPlusResponseDetails1.getMasterData());
		
		return irPlusResponseDetails;
		
	}

	@Override
	public IRPlusResponseDetails updateCustomer(CustomerBean customerBean) throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerDaoClient.updateCustomer(customerBean);
		irPlusResponseDetails = iCustomerContactsDao.updateCustomerContacts(customerBean.getCustomerContact());
		irPlusResponseDetails = iCustomerDaoClient.addCustomerLicense(customerBean);
		
		
		
		return irPlusResponseDetails;	}

	
	@Override
	public IRPlusResponseDetails showAllCustomer(String siteId) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerDaoClient.showAllCustomer(siteId);
		
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails deleteCustomerById(String custommerId) throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerDaoClient.deleteCustomer(custommerId);
		
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails showAllBanks(String SiteId) throws BusinessException {
	
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerDaoClient.showAllBanks(SiteId);
		
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateCutomerStaus(StatusIdInfo statusIdInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerDaoClient.updateCutomerStaus(statusIdInfo);
		
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails customerFilter(CustomerFilter customerFilter) throws BusinessException{

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerDaoClient.customerFilter(customerFilter);
		
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails showAllFileProcessing() throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerDaoClient.showAllFileProcessing();
		
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails showAllBusinessProcessing() throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerDaoClient.showAllBusinessProcessing();
		
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails showBankBranch(String bankId) throws BusinessException {
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerDaoClient.showBankBranch(bankId);
		
		IRPlusResponseDetails irPlusResponseDetails1 = irplusdao.getBankProcess(new Long(bankId));
		
		irPlusResponseDetails.setMasterData(irPlusResponseDetails1.getMasterData());
		
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails Autocomplete(String term,String userid) throws BusinessException {
		return iCustomerDaoClient.Autocomplete(term,userid);
	}
	
	@Override
	public IRPlusResponseDetails AutocompleteCustomer(String term,String userid) throws BusinessException {
		return iCustomerDaoClient.AutocompleteCustomer(term,userid);
	}
	
	@Override
	public IRPlusResponseDetails AutocompleteCustomerCode(String term,String userid) throws BusinessException {
		return iCustomerDaoClient.AutocompleteCustomerCode(term,userid);
	}
	
	@Override
	public IRPlusResponseDetails AutocompleteBank(String term,String userid) throws BusinessException {
		return iCustomerDaoClient.AutocompleteBank(term,userid);
	}


	@Override
	public IRPlusResponseDetails AddCustomerDataEntry(FormsManagementDTO formsManagement)
			throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerDaoClient.AddCustomerDataEntry(formsManagement);
	
		irPlusResponseDetails = iCustomerDaoClient.AddCustomerDataEntryDetails(formsManagement.getDataEntryFormValidationDTO());
		return irPlusResponseDetails;
	}
}