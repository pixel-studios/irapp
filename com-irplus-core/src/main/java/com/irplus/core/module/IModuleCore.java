package com.irplus.core.module;

import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.module.ModuleBean;
import com.irplus.util.BusinessException;

public interface IModuleCore {

	public IRPlusResponseDetails createModule(ModuleBean moduleInfo) throws BusinessException;
	   
	public IRPlusResponseDetails createByConditionModule(ModuleBean moduleInfo) throws BusinessException;

	public IRPlusResponseDetails getModuleById(String moduleId) throws BusinessException;		
	
	public IRPlusResponseDetails updateModule(ModuleBean moduleInfo) throws BusinessException;
	
	public IRPlusResponseDetails deleteModuleById(String moduleId) throws BusinessException;
		
	public IRPlusResponseDetails findAllModules() throws BusinessException;
	//
	public IRPlusResponseDetails createModuleNoDuplct(ModuleBean moduleInfo) throws BusinessException;
	
	public IRPlusResponseDetails updateModuleStatus(StatusIdInfo siInfo) throws BusinessException;
}

