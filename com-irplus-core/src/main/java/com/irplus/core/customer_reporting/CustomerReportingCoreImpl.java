package com.irplus.core.customer_reporting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.irplus.dao.client.customer_reporting.ICustomerReportingDaoClient;
import com.irplus.dto.BankReportingsInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.customerreporting.CustomerRepotingBean;
import com.irplus.dto.customerreporting.CustomerRepotingResponseDetails;
import com.irplus.util.BusinessException;

@Component
public class CustomerReportingCoreImpl implements ICustomerReportingCore {

	@Autowired
	ICustomerReportingDaoClient iCustomerReportingDaoClient;
	
	@Override
	public IRPlusResponseDetails createCustomerReport(CustomerRepotingBean customerReportingBean)
			throws BusinessException {

		IRPlusResponseDetails iRPlusResponseDetails = new IRPlusResponseDetails();
		iRPlusResponseDetails = iCustomerReportingDaoClient.createCustomerReport(customerReportingBean);
		return iRPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getCustomerReportById(String custommerReportingId) throws BusinessException {
		
		IRPlusResponseDetails customerRepotingResponseDetails = new IRPlusResponseDetails();
		
		customerRepotingResponseDetails =  iCustomerReportingDaoClient.getCustomerReportById(custommerReportingId);
		return customerRepotingResponseDetails;
	}

	@Override
	public CustomerRepotingResponseDetails updateCustomerReports(CustomerRepotingBean customerReportingBean)
			throws BusinessException {
		
		CustomerRepotingResponseDetails customerRepotingResponseDetails = new CustomerRepotingResponseDetails();
		
		customerRepotingResponseDetails =  iCustomerReportingDaoClient.updateCustomerReports(customerReportingBean);
		return customerRepotingResponseDetails;
	}

	@Override
	public CustomerRepotingResponseDetails deleteCustomerReportById(String custommerReportingId)
			throws BusinessException {
		CustomerRepotingResponseDetails customerRepotingResponseDetails = new CustomerRepotingResponseDetails();
		
		customerRepotingResponseDetails =  iCustomerReportingDaoClient.deleteCustomerReportById(custommerReportingId);
		return customerRepotingResponseDetails;
	}

	@Override
	public CustomerRepotingResponseDetails showAllCustomerReports() throws BusinessException {
		CustomerRepotingResponseDetails customerRepotingResponseDetails = new CustomerRepotingResponseDetails();
		
		customerRepotingResponseDetails =  iCustomerReportingDaoClient.showAllCustomerReports();
		return customerRepotingResponseDetails;
	}

	@Override
	public IRPlusResponseDetails dynamicCustomerReportsBybankId(BankReportingsInfo banksReportsInfo)
			throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerReportingDaoClient.dynamicCustomerReportsBybankId(banksReportsInfo);
		
		return irPlusResponseDetails;
	}
	
}
