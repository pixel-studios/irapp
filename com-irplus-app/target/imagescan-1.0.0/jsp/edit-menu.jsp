<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
	<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Edit Menu</h1>
			</div>
			<!-- END PAGE TITLE -->
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				
				<li class="active">
					Add Role
				</li>
			</ul>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 text-right">
				<a href="manage-menu.jsp" class="btn blue"><i class="fa fa-reply"></i> Back </a>
			</div>
			</div>
			</div>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
				
					
					<div class="portlet light col-md-12 col-sm-12 col-xs-12">
						<div class="portlet-body">
							<div class="col-md-10 col-md-offset-1">
							<form class="form-horizontal " id="jvalidate" role="form">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-4 control-label">
										Menu Name : <sup class="red-req">*</sup></label>
										<div class="col-md-8">
											<input type="text" class="form-control" placeholder="" required value="Admin Settings"/>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label">
										Description :</label>
										<div class="col-md-8">
											<textarea class="form-control" placeholder="">Admin Settings</textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label">
										Sorting order :</label>
										<div class="col-md-8">
											<input type="text" class="form-control" placeholder="" value="5"/>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label">
											Status
										</label>
										<div class="col-md-8">
											<label class="menu-checkbox">
												<input type="checkbox" checked class="icheck" data-checkbox="icheckbox_square-grey">  
											</label>
										</div>
									</div>
									<div class="col-md-8 col-md-offset-4 col-sm-8 col-sm-offset-4  col-xs-12">
										<div class="form-actions noborder ">
											<button type="button" class="btn default">Cancel</button>
											<input type="submit" class="btn blue" value="Update" onclick="validfn();"/>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<script>
jQuery(document).ready(function() {       
   	// initiate layout and plugins
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	Demo.init(); // init demo features\
});



function submitForm()
{
	var person = {
            rolename: $("#modulename").val(),
			statusId:"11",
			userId:"5"
        }

		//alert("data "+jQuery("#addModuleForm").serialize());

	

    $.ajax({
            method: 'post',
            url: '../role/create',
			data: JSON.stringify(person),
			contentType: 'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success: function (response) {
				
					
					location.href='manage-roleperm.jsp';
					//jQuery("#manageModule").click();
							   
            },

		 error:function(response,statusTxt,error){
			/* alert("Failed to add record :"+response.responseJSON.statusMsg); */
		 }
        });
}  
    </script>










































<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>