<script src="../resources/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<div class="page-header-top fixed">
		<div class="container-fluid">
		<div class="row">
			<!-- BEGIN LOGO -->
			<div class="col-md-3">
			<div class="page-logo header-logo">
				<a href="index.jsp">
				<span class="logowrap">
				<img id="site-logo" src="" alt="" class="logo-default"></span>
				<span class="logo-sub logocaption" id="logo-sub"></span>
				</a>
			</div>
			</div>
		<div class="col-md-7">
			<a href="javascript:;" class="menu-toggler"></a>
			<div class="page-header-menu">

						<div id="usermenu" class="hor-menu ">
							<ul class="nav navbar-nav">
							</ul>
						</div>

				</div>
</div>
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->

			<!-- END RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN TOP NAVIGATION MENU -->
			<div class="col-md-2">
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">

					<li class="dropdown dropdown-user dropdown-dark">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<img alt="" class="img-circle" src="../resources/assets/admin/layout3/img/avatar9.jpg">
						<span class="username username-hide-mobile"><%=session.getAttribute("username")%></span>
						</a>
						<ul class="dropdown-menu dropdown-menu-default">
							<li>
								<a href="profile.jsp">
								<i class="fa fa-user"></i> My Profile </a>
							</li>

							<li>
								<a href="javascript:doLogout()">
								<i class="fa fa-sign-out"></i> Log Out </a>
							</li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
			</div>
			</div>
			<!-- END TOP NAVIGATION MENU -->
		</div>
		</div>
	</div>




	<article id="pageloader" class="white-loader" style="display: block;">
	<div class="vertical-outer">
		<div class="vertical-inner">
			<div class="loader">Loading...</div>
		</div>
	</div>
</article>


	<script>
	var roleId = <%=session.getAttribute("roleId")%>;
	var siteId=<%=session.getAttribute("siteId")%>;
	
	$(document).ready(function() {
		
		
		var userInfo = {
					roleId:roleId


				}
	$.ajax({
					method: 'post',
					url: '../fetchMenu',
					data: JSON.stringify(userInfo),
					contentType: 'application/json',
					//dataType:'JSON',
					//crossDomain:'true',
					success: function (response) {
					//alert();
						//var menuhtml = '<ul class="nav navbar-nav"><li class="menu-dropdown classic-menu-dropdown">	<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">Admin</a><ul class="dropdown-menu pull-left"><li><a href="manage-menu.jsp">Manage Menu </a></li></ul>'

						//alert("usermenu"+response.userMenus[0].menuName);//.empty();

						 $("#usermenu").empty();

						 var userMenuList = response.userMenus;

						 var menu = "<ul class='nav navbar-nav'><li><a href='index.jsp'><i class='fa fa-dashboard'></i> Dashboard</a></li>";

						 for (var i=0;i<userMenuList.length ;i++ )
						 {
							 //alert(userMenuList[i].menuName);
							 menu+="<li class='menu-dropdown classic-menu-dropdown'><a href='javascript:;' class='hover-menu' data-hover='megamenu-dropdown' data-close-others='true' data-toggle='dropdown'><i class='"+userMenuList[i].menuicon+"'></i>"+userMenuList[i].menuName+"</a><ul class='dropdown-menu pull-left'>";
							 var subMenuList = userMenuList[i].subMenu;
							 for (var j=0;j<userMenuList[i].subMenu.length ;j++ )
							 {
								 menu+="<li><a href="+userMenuList[i].subMenu[j].modulepath+">"+userMenuList[i].subMenu[j].modulename+" </a></li>";
							 }
								menu+="</ul></li>";
						}

						menu +="</ul>";
						//alert("menu :"+menu);
						//alert("menu :"+menuhtml);

						 $("#usermenu").html(menu);
					$(".hor-menu .hover-menu").hover(function(){
								//alert("hoverd");
				$(".hor-menu .menu-dropdown").removeClass("open");
				$(this).parent(".menu-dropdown").addClass("open");


			});
			$(".hor-menu .hover-menu").mouseleave(function(){
				$(".hor-menu .menu-dropdown").removeClass("open");

			});
				$(".dropdown-menu").hover(function(){
				//alert("ul hovered");
				$(this).parent(".menu-dropdown").addClass("open");
			});
			$(".dropdown-menu").mouseleave(function(){
				$(".hor-menu .menu-dropdown").removeClass("open");
				//alert("ul leaved");
			});


					},

				 error:function(response,statusTxt,error){


						if (response.status==400)
						{
							showSessionExpiry();
						}

				 }
				});
	})


	$(window).load(function(){
		$("#pageloader .spinner").delay(0).fadeOut("slow");
		$("#pageloader").delay(1000).fadeOut("slow");

	});
	
	
	$(".loadSearch").click(function(){
		
		$("#pageloader .spinner").delay(0).fadeOut("slow");
		$("#pageloader").delay(1000).fadeOut("slow");

	});




	$(document).ready(function(){


		$.ajax({

			method:'get',
			url:'../site/getSiteInfo/'+siteId,
			contentType:'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success:function(response){

			var site = response.siteInfo;
			//$("#site-fname").val(site.siteName);


			if(site.siteName==''){
				$("#logo-sub").append("AB Processing Center");
			}else{
				$("#logo-sub").append(site.siteName);
			}


			if(site.siteLogo==''){
				$("#site-logo").attr('src',"../resources/images/sample-logo.jpg");
			}else{
				$("#site-logo").attr('src',"http://tcmuonline.com:8080/fileimages/Images/SiteLogo/"+site.siteLogo);
			}
			},
			error:function(response,statusTxt,error){

					/* alert("Failed to add record :"+response.responseJSON); */
				 }
		});



	});







	function doLogout()
	{
		//alert("I am here");
		 var userInfo = {
					username: '<%=session.getAttribute("username")%>'


				}
			$.ajax({
					method: 'post',
					url: '../logout',
					data: JSON.stringify(userInfo),
					contentType: 'application/json',
					//dataType:'JSON',
					//crossDomain:'true',
					success: function (response) {

						location.href='login.jsp';
					},

				 error:function(response,statusTxt,error){

					location.href='login.jsp';
				 }
				});
	}
	</script>
