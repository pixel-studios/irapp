<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
	<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<!--<div class="page-title">
				<h1>Add Role</h1>
			</div>-->
			<!-- END PAGE TITLE -->
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
		<!--	<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				
				<li class="active">
					Add Role
				</li>
			</ul>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 text-right">
				<a href="manage-role.jsp" class="btn blue"><i class="fa fa-reply"></i> Back </a>
			</div>
			</div>
			</div>-->
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
				
					
					<div class="portlet light col-md-12 col-sm-12 col-xs-12">
					<div class="pagestick-title">
									<span>Add Roles</span>
									</div>
						<div class="portlet-body">
							<div class="col-md-10 col-md-offset-1">
							<form class="form-horizontal " method="post" role="form" id="jvalidate">
								<div class="form-body">
									<div class="form-group">
									
									<input type="hidden" class="form-control" placeholder="" id="" name=""/>
										<label class="col-md-4 control-label">
										Role Name :  <sup class="red-req">*</sup></label>
										<div class="col-md-5">
											<input type="text" id="role-name" name="role-name" class="form-control" placeholder="" required />
										</div>
									</div>
									
									
									
									<div class="form-group">
										<label class="col-md-4 control-label">Status :</label>
										<div class="col-md-8">
											<div class="input-group">
												<div class="icheck-list">
													<label>
														<input class="cls_status icheck" id="role-status" name="role-status" type="checkbox" data-checkbox="icheckbox_square-grey"> 
													</label>
												</div>
											</div>
										</div>
									</div>
									
									
								<!-- 	<div class="form-group">
										<label class="col-md-4 control-label">Is Restriction :</label>
										<div class="col-md-8">
											<div class="input-group">
												<div class="icheck-list">
													<label>
														<input class="cls_restriction icheck" id="role-restriction" name="module-restriction" type="checkbox" data-checkbox="icheckbox_square-grey"> 
													</label>
												</div>
											</div>
										</div>
									</div> -->
									
								 <!-- 	<div class="form-group">
										<label class="col-md-4 control-label">Bank Visibility :</label>
										<div class="col-md-8">
											<div class="input-group">
												<div class="icheck-list">
													<label>
														<input class="cls_bank icheck" id="role-bank" name="role-bank" type="checkbox" data-checkbox="icheckbox_square-grey"> 
													</label>
												</div>
											</div>
										</div>
									</div> -->
									
									<div class="col-md-8 col-md-offset-4 col-sm-8 col-sm-offset-4  col-xs-12">
										<div class="form-actions noborder ">
											
											<button type="button" onClick="cancelModule();" class="btn default">Cancel</button>
											
											<input type="button" id="dv_save" class="btn blue" value="Create" onClick="createRole()"/>
											
											<input type="button" id="dv_update" class="btn blue" value="Update" onClick="roleUpdate()"/>
											
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<script>
     var roleid_t = <%= request.getParameter("roleid") %>;
	 var user_id = <%= session.getAttribute("userid") %>;
	 
jQuery(document).ready(function() {       

//	alert(user_id);
   	// initiate layout and plugins
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	Demo.init(); // init demo features
	
	
	
	//var roleid_t = <%= request.getParameter("roleid") %>;
	//alert('a='+roleid_t);
	
	if(roleid_t !=null){
	
	$.ajax({		
			method:'get',
			url:'../role/showOne/'+roleid_t,
			contentType:'application/json',
			dataType:'JSON',
			crossDomain:'true',
			
			success:function(response){
				//alert(response);
			   var roleInfo = JSON.stringify(response);
								
				$.each(JSON.parse(roleInfo), function(idx, obj) {	
				
					$("#role-name").val(obj.rolename);
					
					if(obj.statusId == 1){
						$(".cls_status").prop('checked', 'checked');
						$(".cls_status").parent('div').addClass('checked'); 
					}
					if(obj.isrestricted == 1){
						$(".cls_restriction").prop('checked', 'checked');
						$(".cls_restriction").parent('div').addClass('checked');
					}
					
					if(obj.bankVisibility == true){
						$(".cls_bank").prop('checked', 'checked');
						$(".cls_bank").parent('div').addClass('checked');
					}
					
					icheck_reintialize();
								
				});
			}
		});	
	
	$("#dv_save").hide();
	$("#dv_update").show();
	
	}else{
		
		icheck_reintialize();
		
		$("#dv_save").show();
		$("#dv_update").hide();	
	}
});


function icheck_reintialize(){
	$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue',
		increaseArea: '20%' // optional
	});
}

function createRole()
{
	if($("#jvalidate").valid())
	{
	
		var chk_role_status ="0";
		var chk_role_restriction ="1";
		var	chk_role_bank = false;
		
			if($("#role-status").is(':checked')){
				chk_role_status="1";
			}
			if($("#role-bank").is(':checked')){
				chk_role_bank="true";
			}	
			if($("#role-restriction").is(':checked')){
				chk_role_restriction="1";
			}	
	
	var roleInfo = {
//roleid : "",
			//roleid : "",
			rolename: $("#role-name").val(),						
			statusId: chk_role_status,
			userId : user_id,		
			isrestricted :chk_role_restriction,
			bankVisibility :chk_role_bank
			
        }
		//alert(JSON.stringify(roleInfo));

	

    $.ajax({
	
	
            method: 'post',
            url: '../role/create_no_duplicate',
			data: JSON.stringify(roleInfo),
			contentType: 'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success: function (response) {
				
		//	alert(JSON.stringify(response));
				 swal({title: "Done",text: "Role Created",type: "success"},function(){window.location = "manage-role.jsp";});
				//alert("Added record :"+response.errMsgs[0]);
				//	alert("Added");
				//	location.href='manage-role.jsp';
				//jQuery("#manageModule").click();
							   
            },

		 error:function(response,statusTxt,error){
		/* 	alert("Failed to add record :"+response.responseJSON.errMsgs[0]); */
			
			if(response.responseJSON.errMsgs[0]=="Error :: Duplicate name entered"){
				
				location.href='edit-role.jsp';
			}
		 }
        });
	}
}  


//update role

function roleUpdate(){

	//if(validfn())
	if($("#jvalidate").validate())
	{
	//alert('fst')
	//alert("roleid_t "+roleid_t);
	
		var chk_role_status ="0";
		var chk_role_restriction ="1";
		var	chk_role_bank = false;
		
			if($("#role-status").is(':checked')){
				chk_role_status="1";
			}
			if($("#role-bank").is(':checked')){
				chk_role_bank="true";
			}	
			if($("#role-restriction").is(':checked')){
				chk_role_restriction="1";
			}	
	
	var roleInfo = {
			roleid : roleid_t,
			rolename: $("#role-name").val(),						
			statusId: chk_role_status,
			userId :user_id,		
			isrestricted :chk_role_restriction,
			bankVisibility :chk_role_bank
			
        }
	//	alert(JSON.stringify(roleInfo));
	
	$.ajax({
			method:'post',
			url: '../role/update',
			data: JSON.stringify(roleInfo),
			contentType: 'application/json',
			dataType:'JSON',
			crossDomain:'true',
			
			success: function (response) {
			
				 swal({title: "Done",text: "Role Updated",type: "success"},function(){window.location = "manage-role.jsp";});

				//alert("Updated Role");
				//location.href='manage-role.jsp';													   
			},

		 error:function(response,statusTxt,error){
		 
			/* alert("Failed to add record :"+response.responseJSON); */
		 }
		});
	}
//	else
//	return false;
}


function cancelModule(){
	
//	alert("Cancelled Page");
	location.href='manage-role.jsp';
}
    </script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>