<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp" />
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->

<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE
			<div class="page-title">
				<h1>User Profile </h1>
			</div>
			 END PAGE TITLE -->
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB 
			<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<ul class="page-breadcrumb breadcrumb">
							<li>
								<a href="#">Home</a><i class="fa fa-circle"></i>
							</li>
							<li class="active">
								 Admin User Profile
							</li>
						</ul>
					</div>
				</div>
			</div>		
			END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN PROFILE SIDEBAR -->
					<div class="profile-sidebar">
						<!-- PORTLET MAIN -->
						<div class="portlet light profile-sidebar-portlet">
							<!-- SIDEBAR USERPIC -->
							<div class="profile-userpic">
								
								<span class="userpic-inner">
									<img src="../resources/assets/admin/layout3/img/avatar9.jpg" id="usr-logo" class="img-responsive" alt="" />
									
									<a class="edit-pic bank-img" data-toggle="modal" data-target="#imgupload-prof">
										<i class="fa fa-edit"></i>
									</a>
								
								</span>
							</div>
							<!-- END SIDEBAR USERPIC -->
							<!-- SIDEBAR USER TITLE -->
							<div class="profile-usertitle">
								<div class="profile-usertitle-name">
									
								</div>
								<div class="irplus-id">
								
								</div>
							</div>
							<!-- END SIDEBAR USER TITLE -->
							
							<!-- SIDEBAR MENU -->
							<div class="profile-usermenu">
								<ul class="nav">
								</ul>
							</div>
							<!-- END MENU -->
						</div>
						<!-- END PORTLET MAIN -->
						
					</div>
					<!-- END BEGIN PROFILE SIDEBAR -->
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content boxwith-sidebar">
						<div class="row">
							<div class="col-md-12">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light col-md-12 col-sm-12 col-xs-12">
					<div class="pagestick-title">
								<span>Profile</span>
							</div>
						<div class="portlet-body form">
							<form action="#" id="jvalidate" class="prof-form" name="userProfile" method="post">
								<input type="hidden" class="form-control" id="usr-userLogo" name="usr-userLogo" />
								<fieldset class="col-md-6 col-sm-6 col-xs-12">
									<label>First Name <sup class="red-req">*</sup></label>
									<input type="text" class="form-control" id="licence-fname" name="licence-fname" required />
                                    <input type="hidden" name="userid" id="userid" value="" />
								</fieldset>
								<fieldset class="col-md-6 col-sm-6 col-xs-12">
									<label>Last Name</label>
									<input type="text" class="form-control" id="licence-lname" name="licence-lname" />
								</fieldset>
								<fieldset class="col-md-6 col-sm-6 col-xs-12">
									<label>Role</label>
									<select class="form-control" id="roleId" name="roleId" required = "required">
										
									</select>
								</fieldset>
								<fieldset class="col-md-6 col-sm-6 col-xs-12">
									<label>Email</label>
									<input type="Email" class="form-control" id="licence-email" name="licence-email" />
								</fieldset>
								<fieldset class="col-md-6 col-sm-6 col-xs-12">
									<label>Contact No.<sup class="red-req">*</sup></label>
									<input type="text" class="form-control" id="ct-no" name="ct-no" required />
								</fieldset>
								<fieldset class="col-md-6 col-sm-6 col-xs-12">
									<label>Username <sup class="red-req">*</sup></label>
									<input type="text" class="form-control" id="licence-uname" name="licence-uname" required />
								</fieldset>
                                
								<fieldset class="col-md-6 col-sm-6 col-xs-12" >
									<label>Password <sup class="red-req">*</sup></label>
									<input type="password" class="form-control" id="licence-pswd" name="password" required />
								</fieldset>
								<fieldset class="col-md-6 col-sm-6 col-xs-12">
								<label>Confirm Password <sup class="red-req">*</sup></label>
								<div class="input-group">
      
      <input type="password" class="form-control" id="licence-cpswd" name="confirmpassword" required />
	  <span class="input-group-btn">
        <button class="btn grey" type="button" onclick="showPassword()"><i class="fa fa-eye"></i></button>
      </span>
    </div>
									
									
								</fieldset>
                              
								<fieldset class="col-md-6 col-sm-6 col-xs-12">
									<label>Default Bank</label>
									<select class="form-control select2" id="def_bank">
										
									</select>
								</fieldset>
								<fieldset class="col-md-6 col-sm-6 col-xs-12 custom-multiselect">
									<input type="hidden" id="hidSelectedOptions" />
									<label>Other Bank</label>
									<select class="form-control multiselect" multiple="multiple" id="other_bank">
										
									</select>
								</fieldset>
								
								<!--add status -->
									<div class="form-group">
									<div class="col-md-6 col-sm-6 col-xs-12">
										<label class="col-md-3 control-label p-t-10">Active :</label>
										<div class="col-md-9 ">
											<div class="input-group">
												<div class="icheck-list">
													<label>
														<input class="cls_status icheck" id="role-status" name="role-status" type="checkbox" value="1" data-checkbox="icheckbox_square-grey"> 
													</label>
												</div>
											</div>
										</div>
									</div>
									</div>
								<!---close  -->
								
								<div class="col-md-12 col-sm-12 col-xs-12 nopad">
								<div class="form-actions noborder text-right">
									<button type="button" class="btn default" onClick="cancl();">Cancel</button>
                                    <input type="button" id="dv_save" class="btn blue" value="Create" onClick="createUser()"/>
									<input type="button"  id="dv_update" class="btn blue" value="Update" onClick="updateUser();" />
								</div>
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->

<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<script src="../resources/assets/irplus/js/userLogoUpload.js"></script>


<div class="modal fade" id="imgupload-prof" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Choose a file to upload</h4>
        </div>
        <div class="modal-body">
			<form role="form" id="fileForm" class="form-horizontal" action="javascript:uploadUserLogo()">
				<div class="col-md-12 col-sm-12 col-xs-12">
				
				<input id="user-img-upload" name="userPhoto" type="file" multiple class="file-loading">
				</div>
			</form>
        </div>
        <div class="modal-footer">
          
        </div>
      </div>
      
    </div>
  </div>

</body>

<script>
	var userId=<%= request.getParameter("userid")%>;
	var adminId=<%=session.getAttribute("userid")%>;
	var siteId=<%=session.getAttribute("siteId")%>;
	
	jQuery(document).ready(function() {  
		
		// initiate layout and plugins
			Metronic.init(); // init metronic core components
			Layout.init(); // init current layout
			Demo.init(); // init demo features
		
		
		
		if(userId!=null){
			
			
			$.ajax({
		
			method:'get',
			url:'../user/show/all_roles_banks/'+siteId,
			contentType:'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success:function(response)
			{
			//alert(response.oneUserBean.roleArrayList)
			//oneUserBean
				var roleInfo = JSON.stringify(response.userBean.roleArrayList);
				var listItems= "<option value=''>Select Role</option>";
				var jsonData = roleInfo;
				for (var i = 0; i < response.userBean.roleArrayList.length; i++){
				//alert(response.roleBeans[i].rolename)
				  listItems += "<option value='" + response.userBean.roleArrayList[i].roleid + "'>" + response.userBean.roleArrayList[i].rolename + "</option>";
				}
				
				var bankInfo = JSON.stringify(response.userBean.bankList);
				var listBnkItems= "<option value='' disabled hidden>Select Bank</option>";
				//var jsonData = bankInfo;
				for (var j = 0; j < response.userBean.bankList.length; j++)
				{
				//alert(response.roleBeans[i].rolename)
				  listBnkItems += "<option value='" + response.userBean.bankList[j].bankId + "'>" + response.userBean.bankList[j].bankName + "</option>";
				  
				  
				}
					
				/*$.each(JSON.parse(roleInfo), function(idx, obj) {
				
				var listItems= "";
	var jsonData = roleInfo;
	
		for (var i = 0; i < roleInfo.length; i++){
		
		  //listItems+= "<option value='" + jsonData.Table[i].stateid + "'>" + jsonData.Table[i].statename + "</option>";
		}
	  //  $("#<%//=DLState.ClientID%>").html(listItems);
	  
				
				});*/
				//alert(listItems)
				//alert($("#roleId").innerHTML)
			$("#roleId").html('');
			$("#roleId").html(listItems);
			

							
			$("#def_bank").html('');
			$("#def_bank").html(listBnkItems);
			
			$("#other_bank").html('');
			$("#other_bank").html(listBnkItems);
			$("#other_bank").multiselect({
				 nonSelectedText:'select'
			});
			
			//$('.multiselect-container.dropdown-menu').slimScroll();
			$(".multiselect-container.dropdown-menu").mCustomScrollbar({
			theme:"dark"
			});
			
			 data_fetch();

			},
			error:function(response,statusTxt,error){
				 
			/* 		alert("Failed to add record :"+response.responseJSON); */
				 }
		});
		

		
		
		
			$("#dv_update").show();
			$("#dv_save").hide();
		}else{
		
			 $("#def_bank").change(function () {
					var str;
				    $( "select option:selected").each(function() {
				        str=$(this).val();
				        getbank(str);
				    });
				        
				}).change(); 

				
			$.ajax({
		
			method:'get',
			url:'../user/show/all_roles_banks/'+siteId,
			contentType:'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success:function(response)
			{
			//alert(response.oneUserBean.roleArrayList)
			//oneUserBean
				var roleInfo = JSON.stringify(response.userBean.roleArrayList);
				var listItems= "<option value=''>Select Role</option>";
				var jsonData = roleInfo;
				for (var i = 0; i < response.userBean.roleArrayList.length; i++){
				//alert(response.roleBeans[i].rolename)
				  listItems += "<option value='" + response.userBean.roleArrayList[i].roleid + "'>" + response.userBean.roleArrayList[i].rolename + "</option>";
				}
				
				var bankInfo = JSON.stringify(response.userBean.bankList);
				var listBnkItems= "<option value='' selected disabled >Select Bank</option>";
				//var jsonData = bankInfo;
				for (var j = 0; j < response.userBean.bankList.length; j++)
				{
				//alert(response.roleBeans[i].rolename)
				  listBnkItems += "<option value='" + response.userBean.bankList[j].bankId + "'>" + response.userBean.bankList[j].bankName + "</option>";
				  
				  
				}
					
				/*$.each(JSON.parse(roleInfo), function(idx, obj) {
				
				var listItems= "";
	var jsonData = roleInfo;
	
		for (var i = 0; i < roleInfo.length; i++){
		
		  //listItems+= "<option value='" + jsonData.Table[i].stateid + "'>" + jsonData.Table[i].statename + "</option>";
		}
	  //  $("#<%//=DLState.ClientID%>").html(listItems);
	  
				
				});*/
				//alert(listItems)
				//alert($("#roleId").innerHTML)
			$("#roleId").html('');
			$("#roleId").html(listItems);
			
							
			$("#def_bank").html('');
			$("#def_bank").html(listBnkItems);
			
			$("#other_bank").multiselect({
				 nonSelectedText:'select other bank'
			});
			
		},
			error:function(response,statusTxt,error){
				 
				/* 	alert("Failed to add record :"+response.responseJSON); */
				 }
		});
		
		
		
			$("#dv_update").hide();
			$("#dv_save").show();
		}
		
		
		
		
function data_fetch(){
	
	
		$.ajax({		
				method:'get',
				url:'../user/showOne/'+userId,
				contentType:'application/json',
				dataType:'JSON',
				crossDomain:'true',
				
				success:function(response){
				
				var userBean = response.userBeans;
	
				
				
			
				
				$("#licence-fname").val(userBean[0].firstName);
				$("#userid").val(userBean[0].userid);
				$("#licence-lname").val(userBean[0].lastName);
		//		$("#menu-sortingorder").val(userBean.sortingorder);
				$("#licence-uname").val(userBean[0].username);
				$('#licence-uname').prop('readonly', true);
				$("#licence-pswd").val(userBean[0].password);
				$("#licence-cpswd").val(userBean[0].password);
				$("#licence-email").val(userBean[0].emailAddress);
				$("#ct-no").val(userBean[0].contactno);
				$("#roleId").val(userBean[0].roleId).trigger('change');
				$("#roleId").attr("disabled", true); 

				if(userBean[0].isactive == 1)
				{
					$("#role-status").prop('checked', 'checked'); //icheckbox_square-grey
						$("#role-status").parent('div').addClass('checked'); 
				}
				$("#role-status").val(userBean[0].isactive);	
				if(userBean[0].userPhoto==''){
					$("#usr-logo").attr('src',"../resources/assets/admin/layout3/img/avatar9.jpg");	
				}else{
					$("#usr-logo").attr('src',"http://tcmuonline.com:8080/fileimages/Images/UserLogo/"+userBean[0].userPhoto);
					
					$("#usr-userLogo").val(userBean[0].userPhoto);
					
				}
		
				
				var i=0;
				var otherBanks=[];
			var count=0;
				for(i;i<userBean[0].userBank.length;i++){
					//alert("loop count"+i);
					if(userBean[0].userBank[i].isDefault==1){
						$("#def_bank").val(userBean[0].userBank[i].branchId).trigger('change');
					
					}if(userBean[0].userBank[i].isDefault==0){
						
						otherBanks[count]=userBean[0].userBank[i].branchId;
						count++;
					}
					
				}
				
				$("#hidSelectedOptions").val(otherBanks);
				
				
				 $("#other_bank").multiselect({
				       selectedText: "# of # selected"
				    });
				    var hidValue = $("#hidSelectedOptions").val();
				     //alert(hidValue);
				    var selectedOptions = hidValue.split(",");
				    for(var i in selectedOptions) {
				        var optionVal = selectedOptions[i];
				        $("#other_bank").find("option[value="+optionVal+"]").prop("selected","selected");
				    }
				    $("#other_bank").multiselect('refresh');

				},
				error:function(response,statusTxt,error)
				{
					/* alert("Failed to add record :"+response.responseJSON); */
				}
			});	
	
		
		
	}
	
	});
	
	
	
function icheck_reintialize(){
	$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue',
		increaseArea: '20%' // optional
	});
}

function createUser()
{
	
	 var password = $("#licence-pswd").val();
     var confirmPassword = $("#licence-cpswd").val();
     if (password != confirmPassword) {
         alert("Passwords do not match.");
         return false; 
     }
	
	if($("#jvalidate").valid())
	{
		
	
		var defaultBank=$("#def_bank").val();
		//alert("defaul6t Bank id :"+defaultBank);
		
		var otherbankCount=$("#other_bank option:selected").length
		//alert("otherbankCount :"+otherbankCount);
		
		
		var chk_role_status ="0";
	  //var chk_role_restriction ="0";
	  //var	chk_role_bank = false;
		
			if($("#role-status").is(':checked')){
				chk_role_status="1";
			}
			if($("#role-bank").is(':checked')){
				chk_role_bank="true";
			}	
		
			
			var checkboxValues = [];
			

			 
	      
			
		var userBank=[];
		
		userBank[0]=
		{
			branchId:defaultBank,
			isDefault:1,
			adminId:adminId,
			isactive:'1'
		};
		
		//alert("default bank : "+JSON.stringify(userBank[0]));
		
	    var message = [];
	    var selected = $("#other_bank option:selected");
	    selected.each(function(i, selected){
	        message[i]=$(this).val();
	    });
	    
	   
		for(var k=0;k<otherbankCount;k++){
			
			
	        
			userBank[k+1]=
			{
				branchId:message[k],
				isDefault:0,
				adminId:adminId,
				isactive:'1'
			}
			//alert("other bank : "+JSON.stringify(userBank[k+1]));
		}
		var userBean={
			 siteId:siteId,
			 firstName:$("#licence-fname").val(),
			 lastName:$("#licence-lname").val(),
			 username:$("#licence-uname").val(),
			 password:$("#licence-pswd").val(),
			 roleId:$("#roleId").val(),
			 userBank:userBank,
			 adminId:adminId,
			 emailAddress:$("#licence-email").val(),
			 contactno:$("#ct-no").val(),
			 userPhoto:$("#usr-userLogo").val(),
			 isactive:chk_role_status
		};
		 
				$.ajax({
					method: 'post',
					url: '../user/create',
					data: JSON.stringify(userBean),
					contentType: 'application/json',
					dataType:'JSON',
					crossDomain:'true',
					success: function (response) 
					{
						swal({title: "Done",text: "User Created Successfully",type: "success"},function(){window.location = "manage-user.jsp";});
					},
					
				 error:function(response,statusTxt,error){
				 
					/* alert("Failed to add record :"+response.responseJSON); */
				 }
				});
	
	}
	 return true;
}  



///for updating user details
function updateUser(){
	 var password = $("#licence-pswd").val();
     var confirmPassword = $("#licence-cpswd").val();
     if (password != confirmPassword) {
         alert("Passwords do not match.");
         return false; 
     }
	if($("#jvalidate").valid())
	{
	
			var defaultBank=$("#def_bank").val();
		//alert("defaul6t Bank id :"+defaultBank);
		
		var otherbankCount=$("#other_bank option:selected").length
		//alert("otherbankCount :"+otherbankCount);
		
		
		var chk_role_status ="0";
	  //var chk_role_restriction ="0";
	  //var	chk_role_bank = false;
		
			if($("#role-status").is(':checked')){
				chk_role_status="1";
			}
			if($("#role-bank").is(':checked')){
				chk_role_bank="true";
			}	
		
			
			var checkboxValues = [];
			

			 
	      
			
		var userBank=[];
		
		userBank[0]=
		{
			branchId:defaultBank,
			isDefault:1,
			adminId:adminId,
			isactive:'1'
		};
		
		//alert("default bank : "+JSON.stringify(userBank[0]));
		
	    var message = [];
	    var selected = $("#other_bank option:selected");
	    selected.each(function(i, selected){
	        message[i]=$(this).val();
	    });
	    
	   
		for(var k=0;k<otherbankCount;k++){
			
			
	        
			userBank[k+1]=
			{
				branchId:message[k],
				isDefault:0,
				adminId:adminId,
				isactive:'1'
			}
			//alert("other bank : "+JSON.stringify(userBank[k+1]));
		}
		var userBean={
			 userid:userId,
			 siteId:siteId,
			 firstName:$("#licence-fname").val(),
			 lastName:$("#licence-lname").val(),
			 username:$("#licence-uname").val(),
			 password:$("#licence-pswd").val(),
			 roleId:$("#roleId").val(),
			 userBank:userBank,
			 adminId:adminId,
			 emailAddress:$("#licence-email").val(),
			 contactno:$("#ct-no").val(),
			 userPhoto:$("#usr-userLogo").val(),
			 isactive:chk_role_status
		};
		//alert(JSON.stringify(userBean));
	
		
		
		$.ajax({
			method: 'post',
			url: '../user/update',
			data: JSON.stringify(userBean),
			contentType: 'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success: function (response) 
			{
				swal({title: "Done",text: "User Updated Successfully",type: "success"},function(){window.location = "manage-user.jsp";});
			},
			
		 		error:function(response,statusTxt,error){
		 
			/* alert("Failed to add record :"+response.responseJSON); */
		 }
		});
		
		
		
		
		
		
  
	}
	return true;
}  


function cancl(){
	//alert("Hi");
window.location.href = "manage-user.jsp" ;	
}	


function showPassword() {
    var x = document.getElementById("licence-cpswd");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
} 






function getbank(str){
	
	
	

	var data=str;
    $.ajax({
  		
  		method:'get',
  		url:'../user/getOtherBank/'+data,
  		contentType:'application/json',
  		dataType:'JSON',
  		crossDomain:'true',
  		success:function(response)
  		{
  			
  			$("#other_bank").multiselect('destroy');
  			var bankInfo = JSON.stringify(response.userBean.OtherbankList);
  			var otherBnkItems= "<option value='' disabled hidden>Select Bank</option>";
  			//var jsonData = bankInfo;
  			for (var j = 0; j < response.userBean.OtherbankList.length; j++)
  			{
  			//alert(response.roleBeans[i].rolename)
  			  otherBnkItems += "<option value='"+response.userBean.OtherbankList[j].bankId + "'>" + response.userBean.OtherbankList[j].bankName + "</option>";
  			  
  			  
  			}
  				
  	
  	
  		$("#other_bank").html(otherBnkItems);
  		 $("#other_bank").multiselect({
  			 nonSelectedText:'select other bank'
  		});   		
  		
  		$(".multiselect-container.dropdown-menu").mCustomScrollbar({
  		theme:"dark"
  		});
  		
  		},
  		error:function(response,statusTxt,error){
  			 
  			
  			 }
  	});

}


</script>


<!-- END JAVASCRIPTS -->
<!-- END BODY -->
</html>