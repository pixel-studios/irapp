<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp" />
<!-- BEGIN BODY -->
<body class="page-md login validate-page">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->

<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
		<div class="logo page-logo">
	<a href="index.jsp">
				<span>
				<img src="../resources/assets/admin/layout3/img/logo-default.png" alt="logo" class="logo-default"></span>
				<span class="logo-sub">AB Processing Center</span>
	</a>
</div>
	<!-- BEGIN LOGIN FORM -->
	<form role="form" id="fileForm" class="form-horizontal" action="javascript:uploadLicense()">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="medium-title"><!--<span><img src="../resources/images/fileupload.png"></span>-->Upload License File</div>
			<input id="bank-img-upload" name="licFile" type="file" multiple class="file-loading">
			</div>
	</form>
	<!-- END LOGIN FORM -->
</div>
<div class="copyright">
	 2018 &copy; IR plus. Admin panel.
</div>
<!-- END LOGIN -->
<jsp:include page="includes/footer-js.jsp" />
<!-- Modal -->
 <jsp:include page="includes/footer.jsp" />

<script>

var isLicFile = function(name) {
    return name.match(/lic$/i)
};
    

	function uploadLicense()
	{
		//alert("uploading License file");
		//var file =  $("#bank-img-upload");
	    var filename = $.trim($("#bank-img-upload").val());
		//;/alert("filename: "+JSON.stringify(new FormData(document.getElementById("fileForm"))));
        
        if (!(isLicFile(filename) )) {
            alert('Please browse a lic file to upload ...');
            return;
        }
        
        $.ajax({
            url: '../license/upload',
            type: "POST",
            data: new FormData(document.getElementById("fileForm")),
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function(data) {
              //alert("file uploaded");
			  $("#license-success").show();
			   $("#license-validate").hide();
			},
           error:function(response,statusTxt,error){
				 
			//alert("Failed to add record :"+response.responseJSON.statusMsg);

			swal({title: "Oops...",text: response.responseJSON.statusMsg,type: "error"},function(){});
	
				 }
          });
		}
</script>

</body>
<!-- END BODY -->
</html>