<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp" />
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
	<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<div class="page-title">
				<h1>Manage Database Info</h1>
			</div>
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB 
			<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<ul class="page-breadcrumb breadcrumb">
							<li>
								<a href="#">Home</a><i class="fa fa-circle"></i>
							</li>
							
							<li class="active">
								Manage Database Info
							</li>
						</ul>
					</div>	
				</div>
			</div>	
			END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					
					<div class="profile-content fileupload-container">
						<div class="row">
							<div class="portlet light col-md-12 col-sm-12 col-xs-12">
								<div class="portlet-body">
									<div class="col-md-8 col-md-offset-2 ">
										<form role="form" class="form-horizontal filepath-form">
											<fieldset class="col-md-12 col-sm-12 col-xs-12">
												<label class="col-sm-4 ">Host / IP</label>
												<div class="col-sm-7">
													<input type="text"  class="form-control"/>
												</div>
											</fieldset>
											<fieldset class="col-md-12 col-sm-12 col-xs-12">
												<label class="col-sm-4">User Name</label>
												<div class="col-sm-7">
													<input type="text"  class="form-control"/>
												</div>
											</fieldset>
											<fieldset class="col-md-12 col-sm-12 col-xs-12">
												<label class="col-sm-4">Password</label>
												<div class="col-sm-7">
													<input type="password"  class="form-control"/>
												</div>
											</fieldset>
											<div class="col-md-8 col-md-offset-4 col-sm-8 col-sm-offset-4  col-xs-12">
										<div class="form-actions noborder ">
											<button type="button" class="btn default">Cancel</button>
											<input type="submit" class="btn blue" value="Submit" onclick="validfn();">
										</div>
									</div>
										</form>
									</div>
									
								</div>
							</div>
						</div>
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>