<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container-fluid">
			<!-- BEGIN PAGE BREADCRUMB -->
		
				<!--<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				
				<li class="active">
					Manage Role Permission
				</li>
			</ul>
			</div>
	 
			</div>
			</div>-->
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
								<div class="portlet light col-md-12 col-sm-12 col-xs-12">
						<div class="pagestick-title">
								<span>Manage Role Permission</span>
							</div>
									<div class="portlet-body">
										<div class="table-scrollable table-scrollable-borderless">
											<table id="rolepr-table-id" class="table">
											<col width="50%"></col>
											<col width="50%"></col>
												<thead>
													<tr class="uppercase">
														
														<th>
															 Role Name
														</th>
													<!--	<th>
															 View
														</th>
														<th>
															 add
														</th>
														<th>
															 edit
														</th>
														<th>
															 delete
														</th>
														<th>
															 approval
														</th>
														<th>
															 Status
														</th>-->
														<th>
															 Options
														</th>
													</tr>
												</thead>

												
											</table>
										</div>
									</div>
								</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />

<script>
$(document).ready(function()
{  	 

	  $('#rolepr-table-id').removeAttr('width').DataTable( {/*
		  
		  "ajax" :{
			"url":"../rolepermission/showAll",  
			"dataSrc":"rolePermissonsBean"
		  },
		"processing": true,
		
		"columnDefs": [
      { "width": "1%", "targets": 0 },
      { "width": "14%", "targets": 1 },
      { "width": "7%", "targets": 2 },
	  { "width": "7%", "targets": 3},
	  { "width": "7%", "targets": 4 },
	  { "width": "7%", "targets": 5 },
	  { "width": "7%", "targets": 6},
      { "width": "12%", "targets": 7 },
      { "width": "12%", "targets": 8 }],
	  
        "columns": [
			{"data":"permissionid"},
			{"data":"roleName"},
            {"data":"viewprm"},
			{"data":"addprm"},
			{"data":"editprm"},
			{"data":"deleteprm"},
			{"data":"approval"},
			{ data: null, render: function ( data, type, row ) {
				if(data.statusId=='1')
				return '<button type="button" class="btn green btn-xs">Active</button>';
				else
				return '<button type="button" class="btn red btn-xs">inactive</button>';
			}},	
			
			{ data: null, render: function ( data, type, row ){
                return  '<a href="manage-roleperm.jsp?permissionid='+data.permissionid+'" class="edit-btn" data-toggle="tooltip" data-placement="right" title="Edit!"><i class="fa fa-edit"></i></a>'
			}}
			
        ]
    */
	
		  
		  ajax :{
			"url":"../roleprm/showAll",  
			"dataSrc":"roleBeans"
		  },
		//"ajax": "../role/showAll",
		"processing": true,
        //"serverSide": true,x
		
		"columnDefs": [
      { "width": "9%", "targets": 0 },
      { "width": "6%", "targets": 1 }],
		
        "columns": [
			{"data":"rolename"},
            //{"data":"bankVisibility"},
			/*{
				data: null, render: function ( data, type, row ) {
		 
			if(data.isrestricted=='1')
			return '<button type="button" class="btn green btn-xs">YES</button>';
			else
			return '<button type="button" class="btn red btn-xs">NO</button>';
			}},*/
	
/* 		{ data: null, render: function ( data, type, row ) {
		   // alert(data.isactive);
		   var urlval = 'rolepermission/status/'+data.roleid;
			if(data.statusId=='1')
			return '<button type="button" class="btn green btn-xs" onclick="funchangestatus(\''+urlval+'\',0)">Active</button>';
			else
			return '<button type="button" class="btn red btn-xs" onclick="funchangestatus(\''+urlval+'\',1)">inactive</button>';
			}},	 */
		
		{ data: null, render: function ( data, type, row ){
              //  alert("data.moduleid"+data.moduleid);
			  var urlval = 'rolepermission/delete/'+data.roleid;
                return  '<a href="edit-roleperm.jsp?roleid='+data.roleid+'" class="edit-btn btn blue btn-xs" data-toggle="tooltip" data-placement="right" title="Edit!"><i class="fa fa-edit"></i></a>'
			/*	<a href="#" class="delete-btn btn red btn-xs" data-toggle="tooltip" data-placement="right" title="Delete!" onclick="funStats(\''+urlval+'\',1)"><i class="fa fa-trash-o"></i></a>' */
			}}
			
        ]
    
	});
});
</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>