<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp"/>
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp"/>
			
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container-fluid	">
	
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container-fluid">
		
			<div class="col-md-6 col-sm-6 col-xs-12 text-right">
				<div class="fixedbtn-container">
				<a href="javascript:void(0);" class="btn green-meadow serach m-t-15 fileupload-trigger">
					<i class="fa fa-upload"></i> Upload
				</a>
				<a href="javascript:void(0);" class="btn serach m-t-15 search-trigger">
					<i class="fa fa-search"></i> search
				</a>
				<a href="javascript:void(0);" class="btn serach m-t-15 chart-trigger" title=chart>
				<i class="fa fa-bar-chart"></i> 
				</a>
			</div>
			</div>
		
			<div class="row margin-top-10">
				<div class="col-md-12">
					
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
					<div class="col-md-12 nopad searchmain-wraper" id="search-wraper">
					<div class="portlet light">
						<span class="searchclose"> &times; </span>
						<form role="form">


									<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">From date</label>
										<input type="text" class="form-control fromdate" id="datepicker" placeholder="From date" autocomplete="off">
									</div>
									</div>

	<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">To date</label>
										<input type="text" class="form-control todate" id="datepicker2" placeholder="To date" autocomplete="off">
									</div>
									</div> 

	<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">Bank Name</label>
										<input type="text" class="form-control" id="filter-bname" placeholder="Bank Name" List="list"  autocomplete="off">
									<datalist id="list"></datalist>
									</div>
									</div>
									
									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">Customer Id</label>
										<input type="text" class="form-control" id="filter-custId" placeholder="Customer Id"  List="customerCode" autocomplete="off">
									<datalist id="customerCode"></datalist>
									</div>
									</div>
									
									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">Customer Name</label>
										<input type="text" class="form-control" id="filter-cname" placeholder="Customer Name" List="customerlist" autocomplete="off">
										<datalist id="customerlist"></datalist>
									</div>
									</div>

							

									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">File Processing</label>
										<select class="form-control select2" id="fileprocessdd">
											<option value="">File Type</option>

										</select>

									</div>
									</div>

								<!-- 	<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">Business processing</label>
										<select class="form-control select2" id="busiprocessdd">
											<option value="">Business Process</option>

										</select>
									</div>
									</div> -->
							<!-- 		<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">Status</label>
										<select class="form-control select2" id="filter-bstatus">
											<option value="">ProcessedStatus</option>
											<option >Completed</option>
											<option >Error</option>
										</select>

									</div>
									</div>  -->
									<div class="col-md-12 col-sm-12 col-xs-12 text-right">
									<label for="form_control_1">&nbsp;</label>
									<div class="form-md-line-input pb-15">
									<button type="button" class="btn blue" onClick="loadBanks('filter')">SEARCH</button>

									</div>
									</div>
							</div>
							</form>
							 </div>
							</div>
							<!--UPLOAD FILES-->
										
							<div class="col-md-12 nopad searchmain-wraper" id="upload-wraper">
					<div class="portlet light">
						<span class="searchclose"> &times; </span>
						<form role="form" id="fileForm" class="" action="javascript:uploadFiles()">
											<div class="col-md-12 col-sm-12 col-xs-12 nopad">
														<div class="medium-title">UPLOAD FILES FOR PROCESS</div>
											
														<fieldset class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
																<label for="form_control_1">File Type <sup class="red-req">*</sup></label>
																<select class="form-control select2" id="fileType" name="fileType" required>
																<option value="ACH">ACH</option>
																<option value="LOCKBOX">LOCKBOX</option>
																</select>
															</fieldset>
															<fieldset class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
																<label for="form_control_1">File Name<sup class="red-req"></sup></label>
																	<input id="bank-img-upload" name="licFile" type="file" multiple class="file-loading">

															</fieldset>
											
											
											
											
											</div>
										</form>
							 </div>
							</div>
							
							<!--END OF UPLOAD-->
						<div class="col-md-12 nopad searchmain-wraper" id="chart-wraper" >
				
				<div class="portlet light">
					<span class="searchclose"> &times </span>
							<div class="portlet light" id="container"></div>
							 </div>
							</div>
						<div class="row">
							<div class="col-md-12">
								<div class="portlet light col-md-12 col-sm-12 col-xs-12">
								<div class="pagestick-title">
									<span>Processed Files</span>
								</div>
									<div class="portlet-body">
										<div class="table-scrollable table-scrollable-borderless">
											<table id="file-list" class="table">
												<thead>
													<tr class="uppercase">
														<th>
															File name
														</th>
													 	<th>
															Bank Name
														</th> 
														<th>
															File Type
														</th>
														<th>
															Processed Date
														</th>
														<th>
															Processed Time
														</th>
														<!-- <th>
															Origin
														</th>
														<th>
															Destination
														</th>
														<th>
															Total Credit
														</th>
														<th>
															Total Debit
														</th> -->
														
													<!--<th>
															Processed
														</th>
														<th>
															Waiting DE
														</th>
														<th>
															 Options
														</th>-->
														<th>
															Status
														</th>
														
														<th>
															Options														</th>
													</tr>
												</thead>
											<!--	<tbody>
													<tr>
														<td>
															Bof_100617
														</td>
														<td>
															Bank of America
														</td>
														<td>
															06/21/2016
														</td>
														<td>
															10
														</td>
														<td>
															4
														</td>
														<td>
															6
														</td>
														<td>
															<a href="file-process" class="edit-btn" data-toggle="tooltip" data-placement="right" title="Batch View">
																<i class="fa fa-eye"></i>
															</a>
														</td>
														<td>
															<button type="button" class="btn red btn-xs">In progress</button>
														</td>
														<td>
															<a href="file-process-detail.jsp" class="edit-btn" data-toggle="tooltip" data-placement="right" title="View">
																<i class="fa fa-eye"></i>
															</a>
														</td>
													</tr>	
													<tr>
														<td>
															Bof_100618
														</td>
														<td>
															Citi Bank
														</td>
														<td>
															06/23/2016
														</td>
														<td>
															100
														</td>
														<td>
															100
														</td>
														<td>
															0
														</td>
														<td>
															<a href="file-process-detail.jsp" class="edit-btn" data-toggle="tooltip" data-placement="right" title="Batch View">
																<i class="fa fa-eye"></i>
															</a>
														</td>
														<td>
															<button type="button" class="btn red btn-xs">In Progress</button>
														</td>
														<td>
															<a href="file-process-detail.jsp" class="edit-btn" data-toggle="tooltip" data-placement="right" title="View">
																<i class="fa fa-eye"></i>
															</a>
														</td>
													</tr>
												</tbody>-->
											</table>
										</div>
									</div>
								</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
 <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>

<script type="text/javascript">

var userId=<%=session.getAttribute("userid")%>;
var siteId=<%=session.getAttribute("siteId")%>;
$(document).ready(function()
{  	 
	
	
		$.ajax({
			method: 'get',
			url: '../master/getData/'+siteId,
			contentType: 'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success: function (response) {

					//alert("Added"+JSON.stringify(response));
					//alert("response.masterData "+response.masterData.busPros[0].processName);
					//location.href='manage-bank.jsp';
				if(response.masterData!=undefined&&response.masterData!=null)
				{
					var buspros= response.masterData.busPros;

					for(j=0;j<buspros.length;j++)
					{
						//alert("In loop");
						//alert("buspros[j].processName"+JSON.stringify(buspros[j]));

						var chk = '<option value='+buspros[j].businessProcessId+'>'+buspros[j].processName+'</option>';

						$('#busiprocessdd').append(chk);
					}
					var fileTypes= response.masterData.fileTypes;

					for(j=0;j<fileTypes.length;j++)
					{
						//alert("In loop");
						//alert("buspros[j].processName"+JSON.stringify(buspros[j]));

						var chk = '<option value='+fileTypes[j].filetypeid+'>'+fileTypes[j].filetypename+'</option>';

						$('#fileprocessdd').append(chk);


					}
				}


			},

		 error:function(response,statusTxt,error){


		 }
		});
	

	
	var url ='';
	var method = '';
	var data='';
	
	
	
	loadBanks('list');
	
	
	$.ajax({
	   	 method: 'get',
	   	 url: '../master/getData/Fileprocess/'+siteId+'/'+userId,
	   	 contentType: 'application/json',
	   	
	   	 dataType:'JSON',
	   	 crossDomain:'true',
	   	 success: function (response) {

	
	   	 if(response.masterData!=undefined&&response.masterData!=null)
	   	 {

	 if(response.masterData.fileTypes.length==1){
		 if(response.masterData.fileTypes[0].filetypename=='ACH'){
			 visitorData(response);
		 }
if(response.masterData.fileTypes[0].filetypename=='Lockbox'){
newData(response);
		 }
	
	 }
	 
	 else{
		
	 TypeData(response);
	 
	 
	 }
	
	   	 }


	   	 },

	   	  error:function(response,statusTxt,error){


	   	  }
	   	 });
	
	
});
	
	
	function loadBanks(operation)
	{
		
		
		var password = 'password';
		var url_string = window.location.href;
	    var url = new URL(url_string);
		var fromdate = url.searchParams.get("fromdate");
		var todate = url.searchParams.get("todate");

		
		var BetweendateValues = url.searchParams.get("Betweendate");
		if(BetweendateValues!=null){
		var Betweendate1 = BetweendateValues.replace(/\s/g, "+"); 
	    var Betweendate2 = CryptoJS.AES.decrypt(Betweendate1, password); 
		var betweeendate =Betweendate2.toString(CryptoJS.enc.Utf8);
		}
		
		var filetypeidval = url.searchParams.get("filetypeid");
		if(filetypeidval!=null){
		var filetypeid1 = filetypeidval.replace(/\s/g, "+"); 
	    var filetypeid2 = CryptoJS.AES.decrypt(filetypeid1, password); 
		var filetypeid =filetypeid2.toString(CryptoJS.enc.Utf8);
		}
		
		
		var bankIdval = url.searchParams.get("bankId");
		if(bankIdval!=null){
		var bankIdval1 = bankIdval.replace(/\s/g, "+"); 
	    var bankIdval2 = CryptoJS.AES.decrypt(bankIdval1, password); 
		var bankId =bankIdval2.toString(CryptoJS.enc.Utf8);
		}
		
		
	 	var newvalue = url.searchParams.get("value");
		
		var grpIdvalue = url.searchParams.get("grpIdval"); 
		
	
	
	/* 	var value = $.base64.decode(newvalue);
		var CustgrpId = $.base64.decode(grpIdvalue); */ 
	
		if(operation=='list')
			{
	
	   if(bankIdval==null && BetweendateValues!=null  && newvalue==null && grpIdvalue!=null){
	    url='../fileprocess/groupwise/files',
		data =
		{
				currentdate:betweeendate,
				customerGrpId:CustgrpId,
				siteId:siteId,
				userid:userId,
				fileTypeId:filetypeid
	
		}
		method= 'post';
	}
	if(bankIdval!=null && BetweendateValues!=null){
	    url='../fileprocess/bankwise/files',
		data =
		{
				currentdate:betweeendate,
				bankwiseId:bankId,
				siteId:siteId,
				userid:userId,
				fileTypeId:filetypeid
	
		}
	
		method= 'post';
		
	}
/* 	if(BetweendateValues!=null && bankIdval==null && newvalue!=null){
		alert("bank")
	    url='../file-process/group/datewise',
		data =
		{
				currentdate:betweeendate,
			
				siteId:siteId,
				userid:userId,
				fileTypeId:filetypeid
	
		}
	
		method= 'post';
		
	} */
	if(BetweendateValues!=null && bankIdval==null && newvalue==null && grpIdvalue==null){
		
	    url='../file-process/new/datewise',
		data =
		{
				currentdate:betweeendate,
				siteId:siteId,
				userid:userId,
				fileTypeId:filetypeid
	
		}
		method= 'post';
	}

	if(BetweendateValues==null && bankIdval==null && filetypeidval==null){
		
		url='../file-process/showAll/'+userId;
		data={};
		method= 'get';
	
		}
			}
		  if(operation=='filter')
			{
				
				url="../file/filter";
				method =  'post';
				data =
				{
					
						
						userId:userId,
						fromDate:$('.fromdate').val(),
						toDate:$('.todate').val(), 
						bankname:$('#filter-bname').val(),
				
						filetypeid:$('#fileprocessdd').val(),
						bankCustCode:$('#filter-custId').val(),
						customername:$('#filter-cname').val(),
					
				}
			
			}  
	
			
	$.fn.dataTable.ext.errMode = 'throw'
	  $('#file-list').DataTable( {searching: false,lengthChange:false,pageLength:50,
			ajax : ({
				method: method,
				url: url,
				dataSrc: "allFileList",
				data: data,
				crossDomain:'true',
		
			 error:function(response,statusTxt,error){


			 }
			}),

	"processing": true,
	"destroy": true,
	"ordering": true,
		 /*  ajax :{
			"url":"../file-process/showAll",  
			"dataSrc":"allFileList"
		  }, */
		//"ajax": "../role/showAll",
		//"processing": true,
        //"serverSide": true,x
		
		"order": [[3,"desc" ],[4,"desc" ]],
		
        "columns": [
			{"data":"achFileName"},
			{"data":"bankName"},
			{"data":"fileType"},
			{"data":"processedDate"},
			{"data":"processedTime"},


		  /* {"data":"bankVisibility"},*/
			{
				data: null, render: function ( data, type, row ) {
		 
			if(data.isProcessed=='1')
			return '<button type="button" class="btn green btn-xs">Completed</button>';
			else
			return '<button type="button" class="btn red btn-xs">In Progress</button>';
			}}, 
			{ 
				data: null, render: function ( data, type, row ) 
				{
			
                return '<form action="file-process-detail.jsp" method="post"><input type="hidden" name="fileHeaderRecordId" value="'+data.fileHeaderRecordId+'" /><button type="submit" class="btn btn-xs green-meadow"><i class="fa fa-eye"></i></button></form>'
				}
			},
			
				
        ],
        "fnDrawCallback": function( settings )
		{
    
	  $('.make-switch').bootstrapSwitch(
		{
			 size: 'mini'
		});
		},
		responsive: true
    });
	}

	
	    	 
	       	 function TypeData (response) { 
	 	 	
	        	 Highcharts.chart('container', {
	        	
	        	  
	        	     chart: {
	        	         type: 'column',
	        	         
	        	     },
	        	     title: {
	        	         text: 'Monthly Transactions Summary'
	        	     },
	        	    /*  subtitle: {
	        	         text: 'Source: WorldClimate.com'
	        	     }, */
	        	     xAxis: {
	        	         categories: [
	        	        	 'Jan',
	        	             'Feb',
	        	             'Mar',
	        	             'Apr',
	        	             'May',
	        	             'Jun',
	        	             'Jul',
	        	             'Aug',
	        	             'Sep',
	        	             'Oct',
	        	             'Nov',
	        	             'Dec'
	        	         ],
	        	         crosshair: true
	        	     },
	        	     yAxis: {
	        	         min: 0,
	        	         title: {
	        	             text: 'Transactions'
	        	         }
	        	     },
	        	     tooltip: {
	        	         headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	        	         pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	        	             '<td style="padding:0"><b>{point.y:1f}<span>&nbsp</span> Transactions</b></td></tr>',
	        	         footerFormat: '</table>',
	        	         shared: true,
	        	         useHTML: true
	        	     },
	        	     plotOptions: {
	        	         column: {
	        	             pointPadding: 0.2,
	        	             borderWidth: 0
	        	         }
	        	     },
	        	     series: [{
	        	         name: response.masterData.fileTypes[0].filetypename,
	        	    
	        	         data: [response.masterData.fileTypesInfo[0].MonthlyTrans,response.masterData.fileTypesInfo[1].MonthlyTrans,response.masterData.fileTypesInfo[2].MonthlyTrans,response.masterData.fileTypesInfo[3].MonthlyTrans,response.masterData.fileTypesInfo[4].MonthlyTrans,response.masterData.fileTypesInfo[5].MonthlyTrans,response.masterData.fileTypesInfo[6].MonthlyTrans,response.masterData.fileTypesInfo[7].MonthlyTrans,response.masterData.fileTypesInfo[8].MonthlyTrans,response.masterData.fileTypesInfo[9].MonthlyTrans,response.masterData.fileTypesInfo[10].MonthlyTrans,response.masterData.fileTypesInfo[11].MonthlyTrans]
	            },
	            {
	            	 name: response.masterData.fileTypes[1].filetypename,
	             	
	        	         data: [response.masterData.LockfileTypesInfo[0].MonthlyTrans,response.masterData.LockfileTypesInfo[1].MonthlyTrans,response.masterData.LockfileTypesInfo[2].MonthlyTrans,response.masterData.LockfileTypesInfo[3].MonthlyTrans,response.masterData.LockfileTypesInfo[4].MonthlyTrans,response.masterData.LockfileTypesInfo[5].MonthlyTrans,response.masterData.LockfileTypesInfo[6].MonthlyTrans,response.masterData.LockfileTypesInfo[7].MonthlyTrans,response.masterData.LockfileTypesInfo[8].MonthlyTrans,response.masterData.LockfileTypesInfo[9].MonthlyTrans,response.masterData.LockfileTypesInfo[10].MonthlyTrans,response.masterData.LockfileTypesInfo[11].MonthlyTrans]
	            	
	             
	        	     }] 
	        	 });
	        	
	      	  }
	    	    	 
	       	function newData (response) { 
	    	 	
	        	 Highcharts.chart('container', {
	        	
	        	  
	        	     chart: {
	        	         type: 'column'
	        	     },
	        	     title: {
	        	         text: 'Monthly Transactions Summary'
	        	     },
	        	    /*  subtitle: {
	        	         text: 'Source: WorldClimate.com'
	        	     }, */
	        	     xAxis: {
	        	         categories: [
	        	             'Jan',
	        	             'Feb',
	        	             'Mar',
	        	             'Apr',
	        	             'May',
	        	             'Jun',
	        	             'Jul',
	        	             'Aug',
	        	             'Sep',
	        	             'Oct',
	        	             'Nov',
	        	             'Dec'
	        	         ],
	        	         crosshair: true
	        	     },
	        	     yAxis: {
	        	         min: 0,
	        	         title: {
	        	             text: 'Transactions'
	        	         }
	        	     },
	        	     tooltip: {
	        	         headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	        	         pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	        	             '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
	        	         footerFormat: '</table>',
	        	         shared: true,
	        	         useHTML: true
	        	     },
	        	     plotOptions: {
	        	         column: {
	        	             pointPadding: 0.2,
	        	             borderWidth: 0
	        	         }
	        	     },
	           
	            series: [{
	             name: response.masterData.fileTypes[0].filetypename,
           	
 	         data: [response.masterData.LockfileTypesInfo[0].MonthlyTrans,response.masterData.LockfileTypesInfo[1].MonthlyTrans,response.masterData.LockfileTypesInfo[2].MonthlyTrans,response.masterData.LockfileTypesInfo[3].MonthlyTrans,response.masterData.LockfileTypesInfo[4].MonthlyTrans,response.masterData.LockfileTypesInfo[5].MonthlyTrans,response.masterData.LockfileTypesInfo[6].MonthlyTrans,response.masterData.LockfileTypesInfo[7].MonthlyTrans,response.masterData.LockfileTypesInfo[8].MonthlyTrans,response.masterData.LockfileTypesInfo[9].MonthlyTrans,response.masterData.LockfileTypesInfo[10].MonthlyTrans,response.masterData.LockfileTypesInfo[11].MonthlyTrans]
      	
	        	     }] 
	        	 });
	        	
	      	  }
	    	 function visitorData (response) { 
	    	 	
	        	 Highcharts.chart('container', {
	        	
	        	  
	        	     chart: {
	        	         type: 'column'
	        	     },
	        	     title: {
	        	         text: 'Monthly Transactions Summary'
	        	     },
	        	    /*  subtitle: {
	        	         text: 'Source: WorldClimate.com'
	        	     }, */
	        	     xAxis: {
	        	         categories: [
	        	             'Jan',
	        	             'Feb',
	        	             'Mar',
	        	             'Apr',
	        	             'May',
	        	             'Jun',
	        	             'Jul',
	        	             'Aug',
	        	             'Sep',
	        	             'Oct',
	        	             'Nov',
	        	             'Dec'
	        	         ],
	        	         crosshair: true
	        	     },
	        	     yAxis: {
	        	         min: 0,
	        	         title: {
	        	             text: 'Transactions'
	        	         }
	        	     },
	        	     tooltip: {
	        	         headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	        	         pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	        	             '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
	        	         footerFormat: '</table>',
	        	         shared: true,
	        	         useHTML: true
	        	     },
	        	     plotOptions: {
	        	         column: {
	        	             pointPadding: 0.2,
	        	             borderWidth: 0
	        	         }
	        	     },
	           
	            series: [{
	            	  name: response.masterData.fileTypes[0].filetypename,
	        	    
	        	         data: [response.masterData.fileTypesInfo[0].MonthlyTrans,response.masterData.fileTypesInfo[1].MonthlyTrans,response.masterData.fileTypesInfo[2].MonthlyTrans,response.masterData.fileTypesInfo[3].MonthlyTrans,response.masterData.fileTypesInfo[4].MonthlyTrans,response.masterData.fileTypesInfo[5].MonthlyTrans,response.masterData.fileTypesInfo[6].MonthlyTrans,response.masterData.fileTypesInfo[7].MonthlyTrans,response.masterData.fileTypesInfo[8].MonthlyTrans,response.masterData.fileTypesInfo[9].MonthlyTrans,response.masterData.fileTypesInfo[10].MonthlyTrans,response.masterData.fileTypesInfo[11].MonthlyTrans]
	         
	        	     }] 
	        	 });
	        	
	      	  } 


$(function() {
	
    $( "#datepicker" ).datepicker();
});
$(function() {
	
    $( "#datepicker2" ).datepicker();
});




function uploadFiles()
	{
		
		var fileType=$("#fileType").val();
		
		
        
        $.ajax({
            url: '../fileprocess/upload/'+fileType,
            type: "POST",
            data: new FormData(document.getElementById("fileForm")),
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function(data) {
            	swal({title: "Done",text: "File Uploaded Successfully",type: "success"},function(){window.location = "file-process-list.jsp";});
			 
			},
           error:function(response,statusTxt,error){
				 
		

			swal({title: "Oops...",text: response.responseJSON.statusMsg,type: "error"},function(){});
	
				 }
          });
		}

</script>
<script>
$(function() {
	
 $("#filter-bname").autocomplete({     

    source : function(request, response) {
    
      $.ajax({
           url : '../autocomplete/filter',
           type : 'get',
           data : {
        	   userid:userId,
                  term : request.term
           },
           dataType : "json",
           success : function(data) {
        	 
        	   var banklist="";
        	   for(i=0;i<data.list.length;i++){
        		   
        		   banklist += "<option value='"+data.list[i].bankName +"'></option>";

       
        	   }
        	   $("#list").html(banklist);
          
        	  
           }
    });
 }
});  
});


$(function() {
		 $("#filter-cname").autocomplete({     

		    source : function(request, response) {
		    	
		      $.ajax({
		           url : '../autocomplete/filter/customer',
		           type : 'get',
		           data : {
		        	   userid:userId,
		                  term : request.term
		           },
		           dataType : "json",
		           success : function(data) {
		        	   
		        	   var customerlist="";
		        	   for(i=0;i<data.customerBean.length;i++){
		        		 
		        		   customerlist += "<option value='"+data.customerBean[i].companyName +"'></option>";

		        	   }
		        	   $("#customerlist").html(customerlist);
		          
		        	  
		           }
		    });
		 }
		}); 
		}); 
		
$(function() {
	 $("#filter-custId").autocomplete({     

	    source : function(request, response) {
	    	 autoFocus:true
	      $.ajax({
	    	     url : '../autocomplete/filter/customerCode',
	           type : 'get',
	           data : {
	        	   userid:userId,
	                  term : request.term
	           },
	           dataType : "json",
	           success : function(data) {
	        	   
	        	   var customercode="";
	        	   for(i=0;i<data.customerBean.length;i++){
	        		 
	        		   customercode += "<option value='"+data.customerBean[i].bankCustomerCode +"'></option>";

	        	   }
	        	   $("#customerCode").html(customercode);
	          
	        	  
	           }
	    });
	 }
	}); 
	}); 
	
	
</script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>