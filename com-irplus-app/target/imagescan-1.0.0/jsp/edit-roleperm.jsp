<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<!--<div class="page-head">
		<div class="container">
			
			<div class="col-md-6 col-sm-6 col-xs-12 page-title">
				<h1>Manage Role Permission</h1>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 text-right">
				<a href="manage-roleperm.jsp" class="btn darkgrey m-t-15">
					<i class="fa fa-reply"></i> 
					Back 
				</a>
			</div>
			
			
		</div>
	</div>-->
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
		
				<!--<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				
				<li class="active">
					Edit Role Permission
				</li>
			</ul>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 text-right">
				<a href="manage-roleperm.jsp" class="btn blue">
					<i class="fa fa-reply"></i> 
					Back 
				</a>
			</div>
			</div>
			</div>-->
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
				
					
					<div class="portlet light col-md-12 col-sm-12 col-xs-12">
						<div class="pagestick-title">
								<span>Manage Role Permission</span>
							</div>
						<div class="portlet-body">
							<div class="col-md-10 col-md-offset-1">
							<form class="form-horizontal " role="form" id="jvalidate">
								<div class="form-body">
			
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">
                                            Role Name : <sup class="red-req">*</sup></label>
                                            <div class="col-md-5">
                                        <input type="text" name="role-permission" id="role-permission" class="form-control" placeholder="" value="" required />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                           
                                            <div class="col-md-9 nopad">
                                                <div class="form-group">		
                                                <div class="box-header with-border">		
                                                    <h3 class="medium-title edit-role-head"><i class="fa fa-list-ul"></i> Permission List</h3>
                                                </div>
                                                </div>
                                                
                                                <div id="dv_dyn_module_list">
                                                    
                                                </div>
                                            </div>
                                        </div>
										<div class="col-md-8 col-md-offset-4 col-sm-8 col-sm-offset-4  col-xs-12 nopad">
											<div class="row">
											<div class="form-actions noborder ">
												<input type="button" class="btn blue" value="Update" id="creatupdatbtn" />
						<button type="button" class="btn default" onClick="cancel();" >Cancel</button>
												
											</div>
										</div>
										</div>
								</div>
							</form>
						</div>
					</div>
				</div>
					<!-- END SAMPLE FORM PORTLET-->
			</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

<script>

///chklin
var roleid_t = <%= request.getParameter("roleid") %>;


jQuery(document).ready(function() {  
	
	$.ajax({		
			method:'get',
			url:'../role/showOne/'+roleid_t,
			contentType:'application/json',
			dataType:'JSON',
			crossDomain:'true',
			
			success:function(response){
				
			var moduleInfo = JSON.stringify(response);
				
				$.each(JSON.parse(moduleInfo), function(idx, obj) {				
				 $("#role-permission").val(obj.rolename);
				});
			}
		});	
		
		
		//  getting menuid based  getting the all selected modulemenus

//rolepermission/showOneRolePopup/4
	$.ajax({		
			method:'get',			
	//		url:'../module/show/list',			
			url:'../rolepermission/showOneRolePopup/'+roleid_t,
			contentType:'application/json',
			dataType:'JSON',
			crossDomain:'true',	
			beforeSend: function() {
				//loading();
				
 			},		
			success:function(response){
			
			//var mod_Info = JSON.stringify(response.rolePermissonsBean);	
			var mod_Info = JSON.stringify(response.rolePermissonsBean);	
				if(mod_Info)	
					$.each(JSON.parse(mod_Info), function(idx, obj) {
					//alert(roleid_t)
					//alert("All module"+obj);
					//alert("active module list : obj.modulename : "+obj.moduleName +"obj.menuName : "+obj.menuName);	

					//alert(obj.modulemenusId);
					
					$(".cls_add_"+obj.modulemenusId).prop('checked', 'checked');
					$(".cls_add_"+obj.modulemenusId).parent('div').addClass('checked'); 
					$(".cls_edit_"+obj.modulemenusId).prop('checked', 'checked');
					$(".cls_edit_"+obj.modulemenusId).parent('div').addClass('checked'); 
					$(".cls_view_"+obj.modulemenusId).prop('checked', 'checked');
					$(".cls_view_"+obj.modulemenusId).parent('div').addClass('checked'); 
					$(".cls_delete_"+obj.modulemenusId).prop('checked', 'checked');
					$(".cls_delete_"+obj.modulemenusId).parent('div').addClass('checked'); 
					
				});
			}
		});	
		
		
	$.ajax({		
			method:'get',			
	//		url:'../module/show/list',			
			url:'../menumodule/status/list',
			contentType:'application/json',
			dataType:'JSON',
			crossDomain:'true',			
			success:function(response){
			var mod_Info = JSON.stringify(response.moduleMenuBean);	
					//alert("Entered list of all module"+mod_Info);	
					var module_list='';
					var mnunam = '';
					var dispmnunam = '';
					$.each(JSON.parse(mod_Info), function(idx, obj) {
					//alert("All module"+obj);
					//alert("active module list"+obj.modulename);						
					//alert(obj.moduleid);
					if(mnunam == '' || mnunam != obj.menuName)
					{
					mnunam = obj.menuName;
					dispmnunam = obj.menuName;
					}
					else
					{
					dispmnunam = '';
					}
					module_list +='<div class="form-group" ><div class="col-sm-12 list-head">'+dispmnunam+'</div><div class="col-sm-5"><input type="hidden" name="mdlmnuid[]" value="'+obj.modulemenuid+'" ><label class="col-md-12 control-label">'+obj.moduleName+':</label></div><div class="col-sm-7"><label class="ichk-label"><input type="checkbox" class="cls_add_'+obj.modulemenuid+'" name="addprm[]" value="'+obj.modulemenuid+'" > <span>Add</span> </label><label class="ichk-label"><input type="checkbox"  class="cls_edit_'+obj.modulemenuid+'" name="editprm[]" ><span>Edit</span> </label><label class="ichk-label"><input type="checkbox"  class="cls_view_'+obj.modulemenuid+'" name="viewprm[]" ><span>View</span> </label><label class="ichk-label"><input type="checkbox" name="deleteprm[]" class="cls_delete_'+obj.modulemenuid+'" ><span>Delete</span> </label></div></div>'; 															
				});
				
				$("#dv_dyn_module_list").html(module_list);
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-blue',
					radioClass: 'iradio_square-blue',
					increaseArea: '20%' // optional
				});				
			}
	});
		
		//closed
		
		
		//create
		
		
		function createUpdtRolePerm(){
		
		//if(validfn())
		//{
		/*$("input[name='addprm[]']").each( function () {
		alert($(this).val());
	});*/
		//	}
			//return false;
/**			var chk_module_status =0;
			var chk_isreport ="0";
			
			if($("#module-status").is(':checked')){
				chk_module_status=1;
			}
			if($("#module-isreport").is(':checked')){
				chk_isreport="1";
			}		 */
$('input[name="mdlmnuid[]"]').each(function() {
									console.log(this.value);
										
										
										var mmid = $(this).val();
							if($(".cls_add_"+mmid).prop('checked') || $(".cls_edit_"+mmid).prop('checked') || $(".cls_view_"+mmid).prop('checked') || $(".cls_delete_"+mmid).prop('checked'))
							{
							
								if($(".cls_add_"+mmid).prop('checked'))
								$adchk = true;
								else
								$adchk = false;
								
								if($(".cls_edit_"+mmid).prop('checked'))
								$edtchk = true;
								else
								$edtchk = false;
								
								if($(".cls_view_"+mmid).prop('checked'))
								$vwchk = true;
								else
								$vwchk = false;
								
								if($(".cls_delete_"+mmid).prop('checked'))
								$delchk = true;
								else
								$delchk = false;
			/*					
			strcon += '{'+'"modulemenusId":'+mmid+','+'"rolesId":'+mmid+','+
									"rolesId":roleid_t,
									"statusId":"1",
									"userId":"2",
									"isactive":"1",
									"addprm":$adchk,
									"editprm":$edtchk,
									"deleteprm":$delchk,
									"viewprm":$vwchk,
									"approval":$vwchk
									}*/															
																		
									strcon +={ "modulemenusId" : mmid, "rolesId":roleid_t,
									"statusId":"1",
									"userId":"2",
									"isactive":"1",
									"addprm":$adchk,
									"editprm":$edtchk,
									"deleteprm":$delchk,
									"viewprm":$vwchk,
									"approval":$vwchk };
									
									
									
							}
							
									
							
							});
							
			
			
			var module_Info ={ "rolesId" : roleid_t, "roleprmArray" :[]
								
					/*$(".cls_add_"+obj.modulemenusId).prop('checked', 'checked');
					$(".cls_add_"+obj.modulemenusId).parent('div').addClass('checked'); 
								"rolesId":roleid_t,
								*/
	
				};
				//	alert(JSON.stringify(module_Info));
					
					/*$.ajax({
					method: 'post',
					url: '../rolepermission/update/crtupt',
					data: JSON.stringify(module_Info),
					contentType: 'application/json',
					dataType: 'JSON',
					crossDomain: 'true',
					success: function (response) {
						
						var moduleInfo = JSON.stringify(response.moduleMenuBean);
						alert(moduleInfo);
							alert("Added ModuleMenu");
							location.href='manage-modulemenu.jsp';
					
					},
					
				 error:function(response,statusTxt,error){
				 
					alert("Failed to add record :"+response.responseJSON);
					
					var moduleInfo = JSON.stringify(response.moduleMenuBean);
						alert(moduleInfo);
				 }
				});*/
		}  
		
		$('#creatupdatbtn').click(function()
		{
			createUpdtRolePerm();
		});
	});	
	
	function cancel(){
		
	location.href='manage-roleperm.jsp';
	}
	
	
	////chklin
</script>