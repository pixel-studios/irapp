<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp" />
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
	<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Bank Location Setup</h1>
			</div>
			<!-- END PAGE TITLE -->
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
		
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<!--<div class="row margin-top-10">
				<div class="col-md-12">-->
					<!-- BEGIN PROFILE SIDEBAR -->
				<!--	<div class="profile-sidebar" style="width: 250px;">-->
						<!-- PORTLET MAIN -->
					<!--	<div class="portlet light profile-sidebar-portlet">-->
							<!-- SIDEBAR USERPIC -->
							<!--<div class="profile-userpic">
								<span class="userpic-inner">
									<img id="bnk-bnklogo" class="img-responsive" alt="" src="../resources/assets/admin/layout3/img/avatar9.jpg"/>
									<a class="edit-pic bank-img" data-toggle="modal" data-target="#imgupload">
										<i class="fa fa-edit"></i>
									</a>
								</span>	
							</div>-->
							
							<!-- END SIDEBAR USERPIC -->
							<!-- SIDEBAR USER TITLE -->
						<!--	<div class="profile-usertitle">
								<div class="profile-usertitle-name">
									 
								</div>
								<div class="irplus-id">
								
								</div>
								<div class="profile-usertitle-job">
									
								</div>
							</div> -->
							<!-- END SIDEBAR USER TITLE -->
							<!-- SIDEBAR MENU -->
						<!--	<div class="profile-usermenu">
								<ul class="nav"></ul>
							</div>-->
							<!-- END MENU -->
						<!--</div> -->
						<!-- END PORTLET MAIN -->
					<!--</div>-->
					<!-- END BEGIN PROFILE SIDEBAR -->
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
								<div class="portlet light col-md-12 col-sm-12 col-xs-12" id="form_wizard_1">
									<div class="portlet-body form">
										<form action="#" class="form-horizontal" id="submit_form" method="POST">
											<div class="form-wizard">
													<ul class="nav nav-pills nav-justified steps">
														<li class="active">
															<a href="#tab1" data-toggle="tab" class="step">
																<span class="number">
																	1 </span>
																<span class="desc">
																	<i class="fa fa-check"></i> 
																Bank Location 
																</span>
															</a>
														</li>
														
														<li class=" ">
															<a href="#tab2" data-toggle="tab" class="step">
																<span class="number">
																	2 </span>
																<span class="desc">
																	<i class="fa fa-check"></i> 
																Contacts 
																</span>
															</a>
														</li>
													</ul>
													<div id="bar" class="progress progress-striped" role="progressbar">
														<div class="progress-bar progress-bar-success">
														</div>
													</div>
													<div class="tab-content">
														<div class="tab-pane active" id="tab1">
															<input type="hidden" class="form-control" id="bnk-id" name="bnk-id" />
															<input type="hidden" class="form-control" id="bnk-branchid" name="bnk-branchid" />
															<input type="hidden" class="form-control" id="bnk-bankLogo" name="bnk-bankLogo" />
															<input type="hidden" class="form-control" id="bnk-isactive" name="bnk-isactive" />
															
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label>Branch Location <sup class="red-req">*</sup></label>
																<input type="text" class="form-control" name="bnkloc-brchloc" id="bnkloc-brchloc" required />
															</fieldset>
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label>Bank Own Id <sup class="red-req">*</sup></label> 
																<input type="text" class="form-control" name="bnkloc-brchowncode" id="bnkloc-brchowncode" required />
															</fieldset>
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">	
																<label>Location Id <sup class="red-req">*</sup></label>
																<input type="text" class="form-control" name="bnkloc-locid" id="bnkloc-locid" required />
															</fieldset>
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label>RT (Routing Number) <sup class="red-req">*</sup></label>
																<input type="text" class="form-control" name="bnkloc-rtno" id="bnkloc-rtno" required />
															</fieldset>
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label>DDA (Direct Deposit Account) <sup class="red-req">*</sup></label>
																<input type="text" class="form-control" name="bnkloc-ddano" id="bnkloc-ddano" required />
															</fieldset>
															<div class="portlet-title col-md-12 col-sm-12 col-xs-12 ">
															<div class="caption">
																<span class="caption-subject font-green-sharp bold uppercase">Primary contact</span>
															</div>
															</div>
															<input type="hidden" class="form-control" id="bnkloc-contactid" name="bnkloc-contactid" />
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label>First Name <sup class="red-req">*</sup></label>
																<input type="text" class="form-control" id="bnkloc-fname" 
																name="bnkloc-fname" required />
															</fieldset>
															</fieldset>
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label>Last Name</label>
																<input type="text" class="form-control" id="bnkloc-lname" name="bnkloc-lname" />
															</fieldset>
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label>Contact <sup class="red-req">*</sup></label>
																<input type="text" class="form-control" id="bnkloc-ctct"
																name="bnkloc-ctct" required />
															</fieldset>
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Email <sup class="red-req">*</sup></label>
															<input type="email" class="form-control" id="bnkloc-email"
																name="bnkloc-email" required />
															</fieldset>
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label>Address Line 1</label>
																<textarea class="form-control" id="bnkloc-addr1"
																name="bnkloc-addr1" rows="3"></textarea>
															</fieldset>
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label>Address Line 2</label>
																<textarea class="form-control" rows="3" id="bnkloc-addr2"
																name="bnkloc-addr2"></textarea>
															</fieldset>
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label>City</label>
																<input type="text" class="form-control" id="bnkloc-city"
																name="bnkloc-city" />
															</fieldset>
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label>state</label>
																<input type="text" class="form-control" id="bnkloc-state"
																name="bnkloc-state" />
															</fieldset>
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label>Zip code</label>
																<input type="text" class="form-control" id="bnkloc-zipcode"
																name="bnkloc-zipcode" />
															</fieldset>
															<input type="hidden" class="form-control" id="bnkloc-isdefault" name="bnkloc-isdefault" />
															<input type="hidden" class="form-control" id="bnkloc-isactive" name="bnkloc-isactive" />
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															
																<label for="form_control_1">Website</label>
																<input type="text" class="form-control" id="bnkloc-web"
																name="bnkloc-web" />
															
															</fieldset>
														</div>
														<div class="tab-pane" id="tab2">
															<div id ="contact_form" class="col-md-12 col-sm-12 col-xs-12 bankaccount-listcont">
															
															<div class="form-group text-right">
															<button type="button" class="btn blue add-row-bank"><i class="fa fa-plus"></i> Add row</button>
															</div>
															<div class="col-md-12 col-sm-12 col-xs-12 bankaccount-single">
																	<div class="col-md-12 col-sm-12 col-xs-12 nopad">
																		<input type="hidden" class="form-control" id="contact_id1" name="contact_id1" />
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																			<div class="form-group">
																				<label class="control-label">First Name</label>
																			<input type="text" class="form-control" name="contact_fname1" id="contact_fname1">
																			</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Last Name</label>
																			<input type="text" class="form-control" id="contact_lname1" name="contact_lname1">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																			<div class="form-group">
																				<label class="control-label">Contact Role</label>
																				<input type="text" class="form-control" name="contact_crole1" id="contact_crole1">
																			</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Phone No.</label>
																			<input type="text" class="form-control" name="contact_fphoneno1" id="contact_fphoneno1">
																		</div>
																		</fieldset>
																	</div>
																	<div class="col-md-12 col-sm-12 col-xs-12 nopad">
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Email</label>
																			<input type="email" class="form-control " name="contact_femail1" id="contact_femail1">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Address 1</label>
																			<input type="text" class="form-control" name="contact_faddr11" id="contact_faddr11">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Address 2</label>
																			<input type="text" class="form-control" name="contact_faddr21" id="contact_faddr21">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">City</label>
																			<input type="text" class="form-control" name="contact_fcity1" id="contact_fcity1">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">State</label>
																			<input type="text" class="form-control" name="contact_fstate1" id="contact_fstate1">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Zip code</label>
																			<input type="text" class="form-control" name="contact_fzcode1" id="contact_fzcode1">
																		</div>
																		</fieldset>
																	</div>															
															</div>
													</div>
														</div>
													</div>	
												</div>	
											<div class="form-actions">
												<div class="row">
													<div class="col-md-offset-3 col-md-9 text-right">
														<a href="javascript:;" class="btn default button-previous">
														<i class="m-icon-swapleft"></i> Back </a>
														<a href="javascript:;" class="btn blue button-next" onclick="validfn();">
														Continue <i class="m-icon-swapright m-icon-white"></i>
														</a>
														<a href="javascript:submitBankLocationForm()" class="btn green button-submit">
														Submit <i class="m-icon-swapright m-icon-white"></i>
														</a>
													</div>
												</div>
											</div>	
											</div>
										</form>
									</div>
								
							
					</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<!-- END JAVASCRIPTS -->
<script src="../resources/assets/irplus/js/bankBranchLogoUpload.js"></script>
<!-- END JAVASCRIPTS -->
<div class="modal fade" id="imgupload" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Choose a file to upload</h4>
        </div>
        <div class="modal-body">
			<form role="form" id="fileForm" class="form-horizontal" action="javascript:uploadBranchLogo()">
								<div class="col-md-12 col-sm-12 col-xs-12">
								
								<input id="bank-img-upload" name="branchLogo" type="file" class="file-loading">
				</div>
			</form>
        </div>
        <div class="modal-footer">
          
        </div>
      </div>
      
    </div>
  </div>



</body>
<script>
	var branchId = <%= request.getParameter("branchId") %>;
	var bankId;
	//alert("bankId: "+bankId);
	
	
	$(document).ready(function() {  
	
		 $.ajax({
				method: 'get',
				url: '../branch/get/'+branchId,
				contentType: 'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success: function (response) {
					
						//alert("Added"+JSON.stringify(response));
						//alert("response.masterData "+response.masterData.busPros[0].processName);
						//location.href='manage-bank.jsp';
					if(response.bank!=undefined&&response.bank!=null)
					{
						var bankInfo= response.bank;
						bankId=bankInfo.bankId;
					$("#bnk-id").val(bankId);
					$("#bnk-bnklogo").attr('src',".."+bankInfo.branchLogo);
					$("#bnk-bankLogo").val(bankInfo.branchLogo);
					$("#bnk-branchid").val(bankInfo.branchId);					
					$("#bnkloc-brchloc").val(bankInfo.branchLocation);
					$("#bnkloc-brchowncode").val(bankInfo.branchOwnCode);
					$("#bnkloc-locid").val(bankInfo.locationId);
					$("#bnkloc-rtno").val(bankInfo.rtNo);
					$("#bnkloc-ddano").val(bankInfo.ddano);
					var contact = bankInfo.contacts[0];
					$("#bnkloc-contactid").val(contact.contactId);
					$("#bnkloc-fname").val(contact.firstName);
					$("#bnkloc-lname").val(contact.lastName);
					$("#bnkloc-ctct").val(contact.contactNo);
					$("#bnkloc-email").val(contact.emailId);
					$("#bnkloc-addr1").val(contact.address1);
					$("#bnkloc-addr2").val(contact.address2);
					//$("#bnkloc-isdefault").val(bankInfo.isdefault);
					$("#bnkloc-state").val(bankInfo.state);
					$("#bnkloc-city").val(bankInfo.city);
					$("#bnkloc-zipcode").val(bankInfo.zipcode);
					$("#bnkloc-web").val(bankInfo.website);
					//ftypelist: ftypelist,
					//bplist:bplist,
					$("#bnk-isactive").val(bankInfo.bankStatus);

					var contacts= bankInfo.contacts;
					//alert("contacts.length" +contacts.length);
					if(contacts.length>1)
					{
														
						for (var i=1;i<contacts.length ;i++ )
						{
							//alert("Calling value of i "+i);

							if (i<contacts.length-1)
							{
								$('.add-row-bank').click();
							}
							
							
							var bankCtct = contacts[i];
							//alert(JSON.stringify(bankCtct));
							$("#contact_id"+i).val(bankCtct.contactId);
							$("#contact_fname"+i).val(bankCtct.firstName);
							$("#contact_lname"+i).val(bankCtct.lastName);
							$("#contact_crole"+i).val(bankCtct.contactRole);
							$("#contact_fphoneno"+i).val(bankCtct.contactNo);
							$("#contact_femail"+i).val(bankCtct.emailId);
							$("#contact_faddr1"+i).val(bankCtct.address1);
							$("#contact_faddr2"+i).val(bankCtct.address2);
							$("#contact_fstate"+i).val(bankCtct.state);
							$("#contact_fcity"+i).val(bankCtct.city);
							$("#contact_fzcode"+i).val(bankCtct.zipcode);
							
						}

					}
					}
						
								   
				},

			 error:function(response,statusTxt,error){
			 
				/* alert("Failed to add record :"+response.responseJSON); */
			 }
			});
	});
	function submitBankLocationForm()
	{
		var numItems = $('div.bankaccount-single').size();

		//alert(numItems);

		var url_data = ($("#submit_form").serialize());//.replace(/+/g,'%20');//replace(/ /g, '+');
	
		var result = queryStringToJSON(url_data);

		var contacts = [];

		contacts[0]=
		{
			contactId: $("#bnkloc-contactid").val(),
			branchId:  $("#bnk-branchid").val(),
			firstName: $("#bnkloc-fname").val(),
			lastName: $("#bnkloc-lname").val(),
			contactNo: $("#bnkloc-ctct").val(),
			emailId: $("#bnkloc-email").val(),
			address1: $("#bnkloc-addr1").val(),
			address2: $("#bnkloc-addr2").val(),
			isdefault:'1',
			state: $("#bnkloc-state").val(),
			city: $("#bnkloc-cty").val(),
			zipcode:$("#bnkloc-zcode").val(),
			isactive:'1'
		};

       for(var k=1;k<=numItems;k++)
		{
		   
			contacts[k]=
			{
				contactId:result["contact_id"+k],
				branchId:  $("#bnk-branchid").val(),
				firstName: result["contact_fname"+k],
				lastName: result["contact_lname"+k],
				contactRole: result["contact_crole"+k],
				contactNo: result["contact_fphoneno"+k],
				emailId: result["contact_femail"+k],
				address1: result["contact_faddr1"+k],
				address2: result["contact_faddr2"+k],
				isdefault:'0',
				state: result["contact_fstate"+k],
				city: result["contact_fcity"+k],
				zipcode:result["contact_fzcode"+k],
				isactive:'1'

			}
		}
		var bankInfo = {
					siteId:"12",
					branchId:branchId,
					branchLocation:$("#bnkloc-brchloc").val(),
					branchOwnCode:$("#bnkloc-brchowncode").val(),	
					locationId:$("#bnkloc-locid").val(),
					rtNo: $("#bnkloc-rtno").val(),
					ddano: $("#bnkloc-ddano").val(),
					branchLogo:$("#bnk-bankLogo").val(),
					contacts: contacts,
					//isdefault:$("#bnkloc-isdefault").val(),
					state: $("#bnkloc-state").val(),
					city: $("#bnkloc-city").val(),
					zipcode:$("#bnkloc-zipcode").val(),
					website:$("#bnkloc-web").val(),
					isactive:$("#bnk-isactive").val()
					
				}
			//alert("BankInfo :"+JSON.stringify(bankInfo));
			//console.log("BankInfo :"+JSON.stringify(bankInfo));
			$.ajax({
					method: 'post',
					url: '../branch/update',
					data: JSON.stringify(bankInfo),
					contentType: 'application/json',
					dataType:'JSON',
					crossDomain:'true',
					success: function (response) {
						
						 swal({title: "Done",text: "Updated",type: "success"},function(){window.location.href='manage-bank-location.jsp?bankId='+bankId;});

							//location.href='manage-bank.jsp';
							
									   
					},

				 error:function(response,statusTxt,error){
				 
				/* 	alert("Failed to add record :"+response.responseJSON); */
				 }
				});
		
	}

	var queryStringToJSON = function (url) {
    if (url === '')
        return '';
    var pairs = (url || location.search).slice(1).split('&');
    var result = {};
    for (var idx in pairs) {
        var pair = pairs[idx].split('=');
        if (!!pair[0])
            result[pair[0].toLowerCase()] = decodeURIComponent(pair[1] || '');
    }
    return result;
}

	function deleteContact(contactId)
	{
		//alert("contactId : "+contactId);

		$.ajax({
				method: 'get',
				url: '../contact/delete/'+contactId,
				contentType: 'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success: function (response) {
											   
				},

			 error:function(response,statusTxt,error){
			 
			/* 	alert("Failed to delete contact :"+response.responseJSON); */
			 }
			});
	}

	</script>
<!-- END BODY -->
</html>