<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp" />
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
	<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="bnk-setup-container">
				<div class="col-md-12 nopad">
					<!-- BEGIN PROFILE SIDEBAR -->
					<div class="profile-sidebar">
						<!-- PORTLET MAIN -->
						<div class="portlet light profile-sidebar-portlet">
							<!-- SIDEBAR USERPIC -->
							<div class="profile-userpic">
								<span class="userpic-inner">
									<img id="bnk-bnklogo" src="../resources/assets/admin/layout3/img/avatar9.jpg" class="img-responsive" alt="">
									
								</span>	
							</div>
							<!-- END SIDEBAR USERPIC -->
							<!-- SIDEBAR USER TITLE -->
							<div class="profile-usertitle">
							<div class="profile-usertitle-name">
									<div id="bnk-bname" class="profile-usertitle-name">
									
								</div>
								</div>
								<div class="irplus-id">
								
								</div>
								<div class="profile-usertitle-job">
									
								</div>
							</div>
							<!-- END SIDEBAR USER TITLE -->
							<!-- SIDEBAR MENU -->
						<div class="profile-usermenu">
								<ul class="nav"></ul>
							</div>
							<!-- END MENU -->
					</div>
						<!-- END PORTLET MAIN -->
				</div>
					<!-- END BEGIN PROFILE SIDEBAR -->
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content boxwith-sidebar">
						<div class="row">
							<div class="col-md-12">
								<div class="portlet light col-md-12 col-sm-12 col-xs-12" id="form_wizard_1">
								<div class="pagestick-title">
								<span>Bank Location Setup</span>
							</div>
									<div class="portlet-body form">
										<form action="#" class="form-horizontal" id="submit_form" method="POST">
											<div class="form-wizard">
													<ul class="nav nav-pills nav-justified steps">
														<li class="active">
															<a href="#tab1" data-toggle="tab" class="step">
																<span class="number">
																	1 </span>
																<span class="desc">
																	<i class="fa fa-check"></i> 
																Bank Location 
																</span>
															</a>
														</li>
														
														<li class=" ">
															<a href="#tab2" data-toggle="tab" class="step">
																<span class="number">
																	2 </span>
																<span class="desc">
																	<i class="fa fa-check"></i> 
																Contacts 
																</span>
															</a>
														</li>
													</ul>
													<div id="bar" class="progress progress-striped" role="progressbar">
														<div class="progress-bar progress-bar-success">
														</div>
													</div>
													<div class="tab-content">
														<div class="tab-pane active" id="tab1">
															<input type="hidden" class="form-control" id="bnk-id" name="bnk-id" />
															<input type="hidden" class="form-control" id="bnk-branchid" name="bnk-branchid" />
															<input type="hidden" class="form-control" id="bnk-isactive" name="bnk-isactive" />
															<input type="hidden" class="form-control" id="bnkloc-isdefault" name="bnkloc-isdefault" />
															<input type="hidden" class="form-control" id="bnkloc-isactive" name="bnkloc-isactive" />
															
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label>Location Name <sup class="red-req">*</sup></label>
																<input type="text" class="form-control" name="bnkloc-brchloc" id="bnkloc-brchloc" required />
															</fieldset>
															
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">	
																<label>Location Id <sup class="red-req">*</sup></label>
																<input type="text" class="form-control" name="bnkloc-locid" id="bnkloc-locid" required />
															</fieldset>
															
															<div class="portlet-title col-md-12 col-sm-12 col-xs-12 ">
															<div class="caption">
																<span class="caption-subject font-green-sharp bold uppercase">Primary contact</span>
															</div>
															</div>
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label>First Name</label>
																<input type="text" class="form-control" id="bnkloc-fname" 
																name="bnkloc-fname" />
															</fieldset>
															
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label>Last Name</label>
																<input type="text" class="form-control" id="bnkloc-lname" name="bnkloc-lname" />
															</fieldset>
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label>Contact </label>
																<input type="text" class="form-control" id="bnkloc-ctct" name="bnkloc-ctct"  />
															</fieldset>
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Email </label>
															<input type="email" class="form-control" id="bnkloc-email"
																name="bnkloc-email" />
															</fieldset>
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label>Address Line 1</label>
																<textarea class="form-control" id="bnkloc-addr1"
																name="bnkloc-addr1" rows="3"></textarea>
															</fieldset>
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label>Address Line 2</label>
																<textarea class="form-control" rows="3" id="bnkloc-addr2"
																name="bnkloc-addr2"></textarea>
															</fieldset>
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label>City</label>
																<input type="text" class="form-control" id="bnkloc-city"
																name="bnkloc-city" />
															</fieldset>
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label>state</label>
																<input type="text" class="form-control" id="bnkloc-state"
																name="bnkloc-state" />
															</fieldset>
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label>Zip code</label>
																<input type="text" class="form-control" id="bnkloc-zipcode"
																name="bnkloc-zipcode" />
															</fieldset>
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															
																<label for="form_control_1">Website</label>
																<input type="text" class="form-control" id="bnkloc-web"
																name="bnkloc-web" />
															
															</fieldset>
																
																<!--add status -->
																	<div class="form-group" id="default">
																	<div class="col-md-6 col-sm-6 col-xs-12">
																		<label class="col-md-3 control-label p-t-10">Default :</label>
																		<div class="col-md-9 ">
																			<div class="input-group">
																				<div class="icheck-list">
																					<label>
																						<input class="cls_status icheck" id="role-status" name="role-status" type="checkbox" value="1" data-checkbox="icheckbox_square-grey"> 
																					</label>
																				</div>
																			</div>
																		</div>
																	</div>
																	</div>
																<!---close  -->
								
														</div>
														<div class="tab-pane" id="tab2">
															<div id ="contact_form" class="col-md-12 col-sm-12 col-xs-12 bankaccount-listcont">
															<div class="form-group text-right">
																<button type="button" class="btn blue add-row-bank"><i class="fa fa-plus"></i> Add row</button>
															</div>
															<div class="col-md-12 col-sm-12 col-xs-12 bankaccount-single">
																	<div class="col-md-12 col-sm-12 col-xs-12 nopad">
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																			<div class="form-group">
																				<label class="control-label">First Name</label>
																			<input type="text" class="form-control contact_fname" name="contact_fname1" id1="1" id="contact_fname1">
																			</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Last Name</label>
																			<input type="text" class="form-control contact_lname" id="contact_lname1" id1="1" name="contact_lname1">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																			<div class="form-group">
																				<label class="control-label">Contact Role</label>
																				<input type="text" class="form-control contact_crole" name="contact_crole1" id1="1" id="contact_crole1">
																			</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Phone No.</label>
																			<input type="text" class="form-control contact_fphoneno" name="contact_fphoneno1" id1="1" id="contact_fphoneno1">
																		</div>
																		</fieldset>
																	</div>
																	<div class="col-md-12 col-sm-12 col-xs-12 nopad">
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Email</label>
																			<input type="email" class="form-control contact_femail " name="contact_femail1" id1="1" id="contact_femail1">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Address 1</label>
																			<input type="text" class="form-control contact_faddr1" name="contact_faddr11" id1="1" id="contact_faddr11">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Address 2</label>
																			<input type="text" class="form-control contact_faddr2" name="contact_faddr21" id1="1" id="contact_faddr21">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">City</label>
																			<input type="text" class="form-control contact_fcity" name="contact_fcity1" id1="1" id="contact_fcity1">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">State</label>
																			<input type="text" class="form-control contact_fstate" name="contact_fstate1" id1="1" id="contact_fstate1">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Zip code</label>
																			<input type="text" class="form-control contact_fzcode" name="contact_fzcode1"  id1="1" id="contact_fzcode1">
																		</div>
																		</fieldset>
																	</div>
															</div>
														</div>
														</div>
													</div>	
												</div>	
											<div class="form-actions">
												<div class="row">
													<div class="col-md-offset-3 col-md-9 text-right">
														<a href="javascript:;" class="btn default button-previous">
														<i class="m-icon-swapleft"></i> Back </a>
														<a href="javascript:;" class="btn blue button-next" onclick="validfn();">
														Continue <i class="m-icon-swapright m-icon-white"></i>
														</a>
														<a href="javascript:updateLocation()" class="btn blue button-save">
														 Update<i class="m-icon-swapright m-icon-white"></i>
														</a>
														<a href="javascript:submitBankLocationForm()" class="btn green button-submit">
														Submit <i class="m-icon-swapright m-icon-white"></i>
														</a>
													</div>
												</div>
											</div>	
											
										</form>
										</div>
									</div>
								
							
					</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<!-- END JAVASCRIPTS -->
<script src="../resources/assets/irplus/js/bankBranchLogoUpload.js"></script>
<!-- END JAVASCRIPTS -->
<div class="modal fade" id="imgupload" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Choose a file to upload</h4>
        </div>
        <div class="modal-body">
			<form role="form" id="fileForm" class="form-horizontal" action="javascript:uploadBranchLogo()">
								<div class="col-md-12 col-sm-12 col-xs-12">
								
								<input id="bank-img-upload" name="branchLogo" type="file" class="file-loading">
								</div>
			</form>
        </div>
        <div class="modal-footer">
          
        </div>
      </div>
      
    </div>
  </div>
</body>
<script>
	var bankId=<%=request.getParameter("bankId")%>;
	var userId=<%=session.getAttribute("userid")%>;
	var siteId=<%=session.getAttribute("siteId")%>;
	var branchId=<%=request.getParameter("branchId")%>;
	
	if(branchId!=null){
		
		
		
			 $.ajax({
					method: 'get',
					url: '../branch/get/'+branchId,
					contentType: 'application/json',
					dataType:'JSON',
					crossDomain:'true',
					success: function (response) {
						
							//alert("Added"+JSON.stringify(response));
							//alert("response.masterData "+response.masterData.busPros[0].processName);
							//location.href='manage-bank.jsp';
						if(response.bank!=undefined&&response.bank!=null)
						{
							var bankInfo= response.bank;
							bankId=bankInfo.bankId;
						$("#bnk-id").val(bankId);
						$("#bnk-branchid").val(bankInfo.branchId);					
						$("#bnkloc-brchloc").val(bankInfo.branchLocation);
						$("#bnkloc-locid").val(bankInfo.locationId);
						$("#bnk-bname").html(bankInfo.bankName);
						if(bankInfo.bankLogo==''){
							$("#bnk-bnklogo").attr('src',"../resources/assets/admin/layout3/img/avatar9.jpg");	
						}else{
							$("#bnk-bnklogo").attr('src',"http://tcmuonline.com:8080/fileimages/Images/BankLogo/"+bankInfo.bankLogo);
						}
						
						var contact = bankInfo.contacts[0];
						$("#bnkloc-contactid").val(contact.contactId);
						$("#bnkloc-fname").val(contact.firstName);
						$("#bnkloc-lname").val(contact.lastName);
						$("#bnkloc-ctct").val(contact.contactNo);
						$("#bnkloc-email").val(contact.emailId);
						$("#bnkloc-addr1").val(contact.address1);
						$("#bnkloc-addr2").val(contact.address2);
						$("#bnkloc-isdefault").val(bankInfo.isdefault);
						$("#bnkloc-state").val(bankInfo.state);
						$("#bnkloc-city").val(bankInfo.city);
						$("#bnkloc-zipcode").val(bankInfo.zipcode);
						$("#bnkloc-web").val(bankInfo.website);
						//ftypelist: ftypelist,
						//bplist:bplist,
						$("#bnk-isactive").val(bankInfo.bankStatus);
						if(bankInfo.isdefault == '1')
						{
							$("#role-status").prop('checked', 'checked'); //icheckbox_square-grey
								$("#role-status").parent('div').addClass('checked'); 
						}
						var contacts= bankInfo.contacts;
						//alert("contacts.length" +contacts.length);
						if(contacts.length>1)
						{
															
							for (var i=1;i<contacts.length ;i++ )
							{
								//alert("Calling value of i "+i);

								if (i<contacts.length-1)
								{
									$('.add-row-bank').click();
								}
								
								
								var bankCtct = contacts[i];
								//alert(JSON.stringify(bankCtct));
								$("#contact_id"+i).val(bankCtct.contactId);
								$("#contact_fname"+i).val(bankCtct.firstName);
								$("#contact_lname"+i).val(bankCtct.lastName);
								$("#contact_crole"+i).val(bankCtct.contactRole);
								$("#contact_fphoneno"+i).val(bankCtct.contactNo);
								$("#contact_femail"+i).val(bankCtct.emailId);
								$("#contact_faddr1"+i).val(bankCtct.address1);
								$("#contact_faddr2"+i).val(bankCtct.address2);
								$("#contact_fstate"+i).val(bankCtct.state);
								$("#contact_fcity"+i).val(bankCtct.city);
								$("#contact_fzcode"+i).val(bankCtct.zipcode);
								
							}

						}
						}
							
									   
					},

				 error:function(response,statusTxt,error){
				 
				/* 	alert("Failed to add record :"+response.responseJSON); */
				 }
				});
			 

	
	$("#default").show();
	
		
	}else{
		
		$.ajax({
			method: 'get',
			url: '../bank/get/'+bankId,
			contentType: 'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success: function (response) {
				
					//alert("Added"+JSON.stringify(response));
					//alert("response.masterData "+response.masterData.busPros[0].processName);
					//location.href='manage-bank.jsp';
				if(response.bank!=undefined&&response.bank!=null)
				{
					var bankInfo= response.bank;
					
					
				$("#bnk-id").val(bankInfo.bankId);
				if(bankInfo.bankLogo==''){
					$("#bnk-bnklogo").attr('src',"../resources/assets/admin/layout3/img/avatar9.jpg");	
				}else{
					$("#bnk-bnklogo").attr('src',"http://tcmuonline.com:8080/fileimages/Images/BankLogo/"+bankInfo.bankLogo);
				}
				
				$("#bnk-bankLogo").val(bankInfo.bankLogo);
				$("#bnk-branchid").val(bankInfo.branchId);
				$("#bnk-bname").html(bankInfo.bankName);
				
				
				
				}
			
					
							   
			},

		 error:function(response,statusTxt,error){
		 
			/* alert("Failed to add record :"+response.responseJSON); */
		 }
		});	 

		$("#default").hide();
	}
	
	
	//alert("bankId: "+bankId);
		function submitBankLocationForm()
	{
		var numItems = $('div.bankaccount-single').size();

		//alert(numItems);
var bankInfo ="";
		var url_data = ($("#submit_form").serialize());//.replace(/+/g,'%20');//replace(/ /g, '+');
	
		var result = queryStringToJSON(url_data);
		//console.log(result);

		var contacts = [];
		var count=1;
		contacts[0]=
		{
			firstName: $("#bnkloc-fname").val(),
			lastName: $("#bnkloc-lname").val(),
			contactNo: $("#bnkloc-ctct").val(),
			emailId: $("#bnkloc-email").val(),
			address1: $("#bnkloc-addr1").val(),
			address2: $("#bnkloc-addr2").val(),
			isdefault:'1',
			state: $("#bnkloc-state").val(),
			city: $("#bnkloc-cty").val(),
			zipcode:$("#bnkloc-zcode").val(),
			isactive:'1'

		};
	$(".contact_fname").each(function()
				
				{
							  var k  = $(this).attr("id1");
			contacts[count]=
			{
				firstName: result["contact_fname"+k],
				lastName: result["contact_lname"+k],
				contactRole: result["contact_crole"+k],
				contactNo: result["contact_fphoneno"+k],
				emailId: result["contact_femail"+k],
				address1: result["contact_faddr1"+k],
				address2: result["contact_faddr2"+k],
				isdefault:'0',
				state: result["contact_fstate"+k],
				city: result["contact_fcity"+k],
				zipcode:result["contact_fzcode"+k],
				isactive:'1'
			}
		
		
		 bankInfo = {
					siteId:siteId,
					bankId:bankId,
					branchLocation:$("#bnkloc-brchloc").val(),
					locationId:$("#bnkloc-locid").val(),
					userId:userId,
					contacts:contacts,
					isdefault:'0',
					state:$("#bnkloc-state").val(),
					city:$("#bnkloc-city").val(),
					zipcode:$("#bnkloc-zipcode").val(),
					website:$("#bnkloc-web").val(),
					isactive:'1'
					
				}
count++;
			
				});
			//alert("BankInfo :"+JSON.stringify(bankInfo));
			$.ajax({
					method: 'post',
					url: '../branch/create',
					data: JSON.stringify(bankInfo),
					contentType: 'application/json',
					dataType:'JSON',
					crossDomain:'true',
					success: function (response) {
						
						 swal({title: "Done",text: "Bank Location created",type: "success"},function(){window.location.href='manage-bank.jsp';});

							
							
									   
					},

				 error:function(response,statusTxt,error){
				 
				/* 	alert("Failed to add record :"+response.responseJSON); */
				 }
				});
		
	}
	var queryStringToJSON = function (url) {
    if (url === '')
        return '';
    var pairs = (url || location.search).slice(1).split('&');
    var result = {};
    for (var idx in pairs) {
        var pair = pairs[idx].split('=');
        if (!!pair[0])
            result[pair[0].toLowerCase()] = decodeURIComponent(pair[1] || '');
    }
    return result;
}


	
	
	
	function updateLocation()
	{
		var numItems = $('div.bankaccount-single').size();
		var bankInfo="";
		var chk_role_status ="0";
		  //var chk_role_restriction ="0";
		  //var	chk_role_bank = false;
			
				if($("#role-status").is(':checked')){
					chk_role_status="1";
				}
		//alert(numItems);

		var url_data = ($("#submit_form").serialize());//.replace(/+/g,'%20');//replace(/ /g, '+');
	
		var result = queryStringToJSON(url_data);
		var count=1;
		var contacts = [];

		contacts[0]=
		{
			contactId: $("#bnkloc-contactid").val(),
			branchId:  $("#bnk-branchid").val(),
			firstName: $("#bnkloc-fname").val(),
			lastName: $("#bnkloc-lname").val(),
			contactNo: $("#bnkloc-ctct").val(),
			emailId: $("#bnkloc-email").val(),
			address1: $("#bnkloc-addr1").val(),
			address2: $("#bnkloc-addr2").val(),
			isdefault:'1',
			state: $("#bnkloc-state").val(),
			city: $("#bnkloc-cty").val(),
			zipcode:$("#bnkloc-zcode").val(),
			isactive:'1'
		};
	$(".contact_fname").each(function()
				
				{
							  var k  = $(this).attr("id1");
		   
			contacts[count]=
			{
				contactId:result["contact_id"+k],
				branchId:  $("#bnk-branchid").val(),
				firstName: result["contact_fname"+k],
				lastName: result["contact_lname"+k],
				contactRole: result["contact_crole"+k],
				contactNo: result["contact_fphoneno"+k],
				emailId: result["contact_femail"+k],
				address1: result["contact_faddr1"+k],
				address2: result["contact_faddr2"+k],
				isdefault:'0',
				state: result["contact_fstate"+k],
				city: result["contact_fcity"+k],
				zipcode:result["contact_fzcode"+k],
				isactive:'1'

			}
		
       
     
		 bankInfo = {
					siteId:siteId,
					branchId:branchId,
					bankId:$("#bnk-id").val(),
					branchLocation:$("#bnkloc-brchloc").val(),
					locationId:$("#bnkloc-locid").val(),
					contacts: contacts,
					isdefault:$("#bnkloc-isdefault").val(),
					state: $("#bnkloc-state").val(),
					city: $("#bnkloc-city").val(),
					zipcode:$("#bnkloc-zipcode").val(),
					website:$("#bnkloc-web").val(),
					isdefault:chk_role_status,
					isactive:$("#bnk-isactive").val()
					
				
				}
	count++;
	
});
			//alert("BankInfo :"+JSON.stringify(bankInfo));
			//console.log("BankInfo :"+JSON.stringify(bankInfo));
			$.ajax({
					method: 'post',
					url: '../branch/update',
					data: JSON.stringify(bankInfo),
					contentType: 'application/json',
					dataType:'JSON',
					crossDomain:'true',
					success: function (response) {
						
						 swal({title: "Done",text: "Updated Successfully",type: "success"},function(){window.location.href='manage-bank.jsp';});

							//location.href='manage-bank.jsp';
							
									   
					},

				 error:function(response,statusTxt,error){
				 
				/* 	alert("Failed to add record :"+response.responseJSON); */
				 }
				});
		
	}


 /*formwizard start*/
   var FormWizard = function () {
   return {
           //main function to initiate the module
           init: function () {
               if (!jQuery().bootstrapWizard) {
                   return;
               }

               function format(state) {
                   if (!state.id) return state.text; // optgroup
                   return "<img class='flag' src='../../assets/global/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
               }

               // $("#country_list").select2({
                   // placeholder: "Select",
                   // allowClear: true,
                   // formatResult: format,
                   // formatSelection: format,
                   // escapeMarkup: function (m) {
                       // return m;
                   // }
               // });

               var form = $('#submit_form');
               var error = $('.alert-danger', form);
               var success = $('.alert-success', form);

               form.validate({
                   doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                   errorElement: 'span', //default input error message container
                   errorClass: 'help-block help-block-error', // default input error message class
                   focusInvalid: false, // do not focus the last invalid input
                   rules: {
                       //account
                       username: {
                           minlength: 5,
                           required: true
                       },
                       password: {
                           minlength: 5,
                           required: true
                       },
                       rpassword: {
                           minlength: 5,
                           required: true,
                           equalTo: "#submit_form_password"
                       },
                       //profile
                       fullname: {
                           required: true
                       },
                       url:
   					{
   						required: true,
   						url: true
   					},
                       email: {
                           required: true,
                           email: true
                       },
                       phone: {
                           required: true
                       },
                       gender: {
                           required: true
                       },
                       address: {
                           required: true
                       },
                       city: {
                           required: true
                       },
                       country: {
                           required: true
                       },
                       //payment
                       card_name: {
                           required: true
                       },
                       card_number: {
                           minlength: 16,
                           maxlength: 16,
                           required: true
                       },
                       card_cvc: {
                           digits: true,
                           required: true,
                           minlength: 3,
                           maxlength: 4
                       },
                       card_expiry_date: {
                           required: true
                       },
                       'payment[]': {
                           required: true,
                           minlength: 1
                       }
                   },

                   messages: { // custom messages for radio buttons and checkboxes
                       'payment[]': {
                           required: "Please select at least one option",
                           minlength: jQuery.validator.format("Please select at least one option")
                       }
                   },

                   errorPlacement: function (error, element) { // render error placement for each input type
                       if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                           error.insertAfter("#form_gender_error");
                       } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                           error.insertAfter("#form_payment_error");
                       } else {
                           error.insertAfter(element); // for other inputs, just perform default behavior
                       }
                   },

                   invalidHandler: function (event, validator) { //display error alert on form submit   
                       success.hide();
                       error.show();
                       Metronic.scrollTo(error, -200);
                   },

                   highlight: function (element) { // hightlight error inputs
                       $(element)
                           .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                   },

                   unhighlight: function (element) { // revert the change done by hightlight
                       $(element)
                           .closest('.form-group').removeClass('has-error'); // set error class to the control group
                   },

                   success: function (label) {
                       if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                           label
                               .closest('.form-group').removeClass('has-error').addClass('has-success');
                           label.remove(); // remove error label here
                       } else { // display success icon for other inputs
                           label
                               .addClass('valid') // mark the current input as valid and display OK icon
                           .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                       }
                   },

                   submitHandler: function (form) {
                       success.show();
                       error.hide();
                       //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
                   }

               });


               var handleTitle = function(tab, navigation, index) {
                   var total = navigation.find('li').length;
                   var current = index + 1;
                   // set wizard title
                   $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
                   // set done steps
                   jQuery('li', $('#form_wizard_1')).removeClass("done");
                   var li_list = navigation.find('li');
                   for (var i = 0; i < index; i++) {
                       jQuery(li_list[i]).addClass("done");
                   }
   			if(branchId==null){
   		//alert("if");
				   	 if (current == 1) {
						
				         $('#form_wizard_1').find('.button-previous').hide();
				     } else {
						 
				         $('#form_wizard_1').find('.button-previous').show();
				     }
				
				     if (current >= total) {
						 
				         $('#form_wizard_1').find('.button-next').hide();
				         $('#form_wizard_1').find('.button-submit').show();
				         $('#form_wizard_1').find('.button-save').hide();
				         
				     } else {

				         $('#form_wizard_1').find('.button-next').show();
				         $('#form_wizard_1').find('.button-submit').hide();
				     }
   			}else{
   				//alert("else");
   				if (current == 1) {
					 //alert("1");
                       $('#form_wizard_1').find('.button-previous').hide();
                   } else {
					   // alert("2");
						$('#form_wizard_1').find('.button-next').hide();
                       $('#form_wizard_1').find('.button-previous').show();
                   }

                   if (current >= total) {
					    //alert("3");
                       $('#form_wizard_1').find('.button-next').hide();
                       $('#form_wizard_1').find('.button-submit').hide();
                       $('#form_wizard_1').find('.button-save').show();
                       //displayConfirm();
                   } else {
					    //alert("4");
                       //$('#form_wizard_1').find('.button-next').show();
                       $('#form_wizard_1').find('.button-submit').hide();
                       $('#form_wizard_1').find('.button-save').hide();
					   $('#form_wizard_1').find('.button-next').show();
                   }
	   			 
   				
   				
   				
   				
   				
   			}
   			
                   Metronic.scrollTo($('.page-title'));
               }

               // default form wizard
               $('#form_wizard_1').bootstrapWizard({
                   'nextSelector': '.button-next',
                   'previousSelector': '.button-previous',
                   onTabClick: function (tab, navigation, index, clickedIndex) {
                       return false;
                     
                   },
                   onNext: function (tab, navigation, index) {
                       success.hide();
                       error.hide();

                       if (form.valid() == false) {
                           return false;
                       }

                       handleTitle(tab, navigation, index);
                   },
                   onPrevious: function (tab, navigation, index) {
                       success.hide();
                       error.hide();

                       handleTitle(tab, navigation, index);
                   },
                   onTabShow: function (tab, navigation, index) {
                       var total = navigation.find('li').length;
                       var current = index + 1;
                       var $percent = (current / total) * 100;
                       $('#form_wizard_1').find('.progress-bar').css({
                           width: $percent + '%'
                       });
                   }
               });

               $('#form_wizard_1').find('.button-previous').hide();
               $('#form_wizard_1').find('.button-save').hide();
               
               
   			$('#form_wizard_1 .button-submit').click(function () {
                   //alert('Finished! Hope you like it :)');
               }).hide();

               //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
               $('#country_list', form).change(function () {
                   form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
               });
           }

       };

   }();
	</script>
<!-- END BODY -->
</html>