<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<jsp:include page="includes/style.jsp" />
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-menu-fixed" class to set the mega menu fixed  -->
<!-- DOC: Apply "page-header-top-fixed" class to set the top menu fixed  -->
<script src="../resources/assets/global/jquery.min.js" type="text/javascript"></script>
<script src="../resources/assets/global/jquery.validate.js"></script>
<body class="page-md">
<!-- BEGIN HEADER -->
<div class="page-header">
	<!-- BEGIN HEADER TOP -->
	<jsp:include page="includes/header-top.jsp" />
	<!-- END HEADER TOP -->
	<!-- BEGIN HEADER MENU -->
	<jsp:include page="includes/header-menu.jsp" />
	<!-- END HEADER MENU -->
</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="bnk-setup-container">
				<div class="col-md-12 nopad">
					<!-- BEGIN PROFILE SIDEBAR -->
					<div class="profile-sidebar">
						<!-- PORTLET MAIN -->
						<div class="portlet light profile-sidebar-portlet">
							<!-- SIDEBAR USERPIC -->
							<div class="profile-userpic">
								<span class="userpic-inner">
									<img id="bnk-bnklogo" src="../resources/assets/admin/layout3/img/avatar9.jpg" class="img-responsive" alt="">
									<a class="edit-pic bank-img" data-toggle="modal" data-target="#imgupload">
										<i class="fa fa-edit"></i>
									</a>
								</span>	
							</div>
							<!-- END SIDEBAR USERPIC -->
							<!-- SIDEBAR USER TITLE -->
							<div class="profile-usertitle">
								<div id="bnk-bname" class="profile-usertitle-name">
									
								</div>
								<div id="bnk-bcode" class="irplus-id">
								
								</div>
								
							</div>
							<!-- END SIDEBAR USER TITLE -->
							<!-- SIDEBAR MENU -->
							<div class="profile-usermenu">
								<ul class="nav">
									
								</ul>
							</div>
							<!-- END MENU -->
						</div>
						<!-- END PORTLET MAIN -->
						
					</div>
					<!-- END BEGIN PROFILE SIDEBAR -->
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content boxwith-sidebar">
						<div class="row">
							<div class="col-md-12">
								<div class="portlet light col-md-12 col-sm-12 col-xs-12" id="form_wizard_1">
									<div class="pagestick-title">
										<span>Bank Setup</span>
									</div>
									<div class="portlet-body form">
													
									<form action="#" class="form-horizontal" name="submit_form" id="submit_form" method="POST">	
										<div class="form-wizard">
											<div class="form-body">
												<ul class="nav nav-pills nav-justified steps">
													<li>
														<a href="#tab1" data-toggle="tab" class="step">
														<span class="number">
														1 </span>
														<span class="desc">
														<i class="fa fa-check"></i> Bank Information </span>
														</a>
													</li>
													<li>
														<a href="#tab2" data-toggle="tab" class="step">
														<span class="number">
														2 </span>
														<span class="desc">
														<i class="fa fa-check"></i> Business Process </span>
														</a>
													</li>
													<li>
														<a href="#tab3" data-toggle="tab" class="step active">
														<span class="number">
														3 </span>
														<span class="desc">
														<i class="fa fa-check"></i> Contacts </span>
														</a>
													</li>
												</ul>
												<div id="bar" class="progress progress-striped" role="progressbar">
														<div class="progress-bar progress-bar-success"></div>
												</div>
												<div class="tab-content">
													<div class="tab-pane active" id="tab1">
													<input type="hidden" class="form-control" id="bnk-id" name="bnk-id" />
														<input type="hidden" class="form-control" id="bnk-bankLogo" name="bnk-bankLogo"/>
														<input type="hidden" class="form-control" id="bnk-folder" name="bnk-folder"/>
														
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Bank Name <sup class="red-req">*</sup></label>
															<input type="text" class="form-control" id="bnk-name" name="bnk-name" required/>
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Bank Id <sup class="red-req">*</sup></label>
															<input type="text" class="form-control" id="bnk-ownid" name="bnk-ownid" required />
															<!-- <div><small>Note : Lockbox File ( banknum tag under Daily_Batch_Def )</small></div> -->
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Location</label>
															<input type="text" class="form-control" id="bnk-branchloc" name="bnk-branchloc"  />
														</fieldset>
														<input type="hidden" class="form-control" id="bnk-branchid" name="bnk-branchid" />
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Location Id </label>
															<input type="text" class="form-control" id="bnk-locid" name="bnk-locid"  />
														</fieldset>

														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">RT (Routing Number) <sup class="red-req">*</sup></label>
															<input type="text" class="form-control" id="bnk-rtnum" name="bnk-rtnum" required />
															<!-- <div><small>Note : ( ACH File - Immediate Destination  )</small></div> --> 
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">DDA (Direct Deposit Account) <sup class="red-req">*</sup></label>
															<input type="text" class="form-control" id="bnk-dda" name="bnk-dda" required />
														</fieldset>
														<fieldset class="portlet-title col-md-12 col-sm-12 col-xs-12 ">
															<span class="caption-subject font-green-sharp bold uppercase">Primary contact</span>
														</fieldset>
												<input type="hidden" class="form-control bnk-contactid" id="bnk-contactid" name="bnk-contactid" />
												<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">First Name <sup class="red-req">*</sup></label>
															<input type="text" class="form-control" id="bnksetup-fname" name="bnksetup-fname" required />
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Last Name</label>
															<input type="text" class="form-control" id="bnksetup-lname" name="bnksetup-lname" />
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Phone No </label>
															<input type="text" class="form-control" id="bnksetup-ctct" name="bnksetup-ctct"  data-mask="(999) 999-9999"/>
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Email </label>
															<input type="email" class="form-control" id="bnksetup-email" name="bnksetup-email" />
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Address Line 1</label>
															<textarea class="form-control" rows="3" id="bnksetup-addr1" name="bnksetup-addr1"/></textarea>
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Address Line 2</label>
															<textarea class="form-control" rows="3" id="bnksetup-addr2" name="bnksetup-addr2" ></textarea>
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">City </label>
															<input type="text" class="form-control" id="bnksetup-cty" name="bnksetup-cty" />
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">state </label>
															<input type="text" class="form-control" id="bnksetup-state" name="bnksetup-state"/>
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Zip code </label>
															<input type="text" class="form-control" id="bnksetup-zcode" name="bnksetup-zcode" />
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Comments</label>
															<input type="text" class="form-control" id="bnksetup-web" name="bnksetup-web" >
														</fieldset>
															<input type="hidden" class="form-control" id="bnk-isdefault" name="bnk-isdefault" />
														<input type="hidden" class="form-control" id="bnk-isactive" name="bnk-isactive" />
													</div>
													<div class="tab-pane" id="tab2">
														<div class="col-md-12 col-sm-12 col-xs-12 nopad">
															<h3 class="medium-title">Licensing Functioning</h3>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">File Processing : <sup class="red-req">*</sup></label>
																<div class="col-md-8">
																	<div class="input-group custom-multiselect">
																		<input type="hidden" id="hidSelectedOptions" />
									
																		<select class="form-control multiselect" multiple="multiple" id="fileTypeChk" required>
																			
																		</select>
																	</div>
																</div>
														</div>
														<!-- <div class="form-group">
															<label class="col-md-4 control-label">Business Processing : <sup class="red-req">*</sup></label>
																<div class="col-md-8">
																		<div class="input-group">
																			<div id="businessprocchk"class="icheck-list">
																			</div>
																		</div>
																</div>
														</div> -->
													</div>
													<div class="tab-pane" id="tab3">
														<div id ="contact_form" class="col-md-12 col-sm-12 col-xs-12 bankaccount-listcont">
															<div class="form-group text-right">
																<button type="button" class="btn blue add-row-bank"><i class="fa fa-plus"></i> Add row</button>
															</div>
															<div class="col-md-12 col-sm-12 col-xs-12 bankaccount-single">
																	<div class="col-md-12 col-sm-12 col-xs-12 nopad">
															<input type="hidden" class="form-control contact_id" id="contact_id1" name="contact_id1" />
																	
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																			<div class="form-group">
																				<label class="control-label">First Name</label>
																			<input type="text" class="form-control contact_fname" name="contact_fname1" id1="1" id="contact_fname1">
																			</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Last Name</label>
																			<input type="text" class="form-control contact_lname" id="contact_lname1" id1="1" name="contact_lname1">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																			<div class="form-group">
																				<label class="control-label">Contact Role</label>
																				<input type="text" class="form-control contact_crole" name="contact_crole1" id1="1" id="contact_crole1">
																			</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Phone No.</label>
																			<input type="text" class="form-control contact_fphoneno" name="contact_fphoneno1" id1="1" id="contact_fphoneno1">
																		</div>
																		</fieldset>
																	</div>
																	<div class="col-md-12 col-sm-12 col-xs-12 nopad">
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Email</label>
																			<input type="email" class="form-control contact_femail" name="contact_femail1" id1="1" id="contact_femail1">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Address 1</label>
																			<input type="text" class="form-control contact_faddr1" name="contact_faddr11" id1="1" id="contact_faddr11">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Address 2</label>
																			<input type="text" class="form-control contact_faddr2" name="contact_faddr21" id1="1" id="contact_faddr21">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">City</label>
																			<input type="text" class="form-control contact_fcity" name="contact_fcity1" id1="1" id="contact_fcity1">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">State</label>
																			<input type="text" class="form-control contact_fstate" name="contact_fstate1" id1="1" id="contact_fstate1">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Zip code</label>
																			<input type="text" class="form-control contact_fzcode" name="contact_fzcode1" id1="1" id="contact_fzcode1">
																		</div>
																		</fieldset>
																	</div>
															</div>
														</div>
													</div>
												</div>
												
												<div class="form-actions">
													<div class="">
														<div class="col-md-offset-3 col-md-9 text-right">
															<a href="javascript:;" class="btn default button-previous">
															<i class="m-icon-swapleft"></i> Back </a>
															<a href="javascript:;" class="btn blue button-next">
															Continue <i class="m-icon-swapright m-icon-white"></i>
															</a>
															
																<a href="javascript:submitUpdateBankForm()" class="btn green button-submit" id="dv_update">
															Update <i class="m-icon-swapright m-icon-white"></i>
															</a>
															<a href="javascript:submitBankForm()" class="btn green button-submit button-save" id="dv_save">
															Submit <i class="m-icon-swapright m-icon-white"></i>
															</a>
														</div>
													</div>
												</div>
												
												
												
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>		
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<script src="../resources/assets/irplus/js/bankLogoUpload.js"></script>
<!-- END JAVASCRIPTS -->
<div class="modal fade" id="imgupload" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Choose a file to upload</h4>
        </div>
        <div class="modal-body">
			<form role="form" id="fileForm" class="form-horizontal" action="javascript:uploadBankLogo()">
								<div class="col-md-12 col-sm-12 col-xs-12">
								
								<input id="bank-img-upload" name="bankLogo" type="file" class="file-loading">
								</div>
			</form>
        </div>
        <div class="modal-footer">
          
        </div>
      </div>
      
    </div>
  </div>
</body>

<script>
var siteId=<%=session.getAttribute("siteId")%>;
var userId=<%=session.getAttribute("userid")%>;
var bankId = <%=request.getParameter("bankId") %>;
	$(document).ready(function() {  
		
		 if(bankId!=null){

$.ajax({
				method: 'get',
				url: '../bank/get/'+bankId,
				contentType: 'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success: function (response) {
					
						//alert("Added"+JSON.stringify(response));
						//alert("response.masterData "+response.masterData.busPros[0].processName);
						//location.href='manage-bank.jsp';
					if(response.bank!=undefined&&response.bank!=null)
					{
						var bankInfo= response.bank;
						
						
					$("#bnk-id").val(bankInfo.bankId);
					if(bankInfo.bankLogo==''){
						$("#bnk-bnklogo").attr('src',"../resources/assets/admin/layout3/img/avatar9.jpg");	
					}else{
						$("#bnk-bnklogo").attr('src',"http://tcmuonline.com:8080/fileimages/Images/BankLogo/"+bankInfo.bankLogo);
					}
					
					$("#bnk-bankLogo").val(bankInfo.bankLogo);
					$("#bnk-branchid").val(bankInfo.branchId);
					$("#bnk-bname").html(bankInfo.bankName);
					$("#bnk-bcode").html(bankInfo.bankCode);
					//alert("Value" +bankInfo.branchLocation);
					$("#bnk-branchloc").val(bankInfo.branchLocation);
					$("#bnk-blocationId").html(bankInfo.locationId);
					$("#bnk-name").val(bankInfo.bankName);
					$("#bnk-ownid").val(bankInfo.bankCode);
					$("#bnk-locid").val(bankInfo.locationId);
					$("#bnk-rtnum").val(bankInfo.rtNo);
					$("#bnk-dda").val(bankInfo.ddano);
					$("#bnk-isactive").val(bankInfo.bankStatus);
					
					var contact = bankInfo.contacts[0];
					$("#bnk-contactid").val(contact.contactId);
					$("#bnksetup-fname").val(contact.firstName);
					$("#bnksetup-lname").val(contact.lastName);
					$("#bnksetup-ctct").val(contact.contactNo);
					$("#bnksetup-email").val(contact.emailId);
					$("#bnksetup-addr1").val(contact.address1);
					$("#bnksetup-addr2").val(contact.address2);
					$("#bnk-isdefault").val(bankInfo.isdefault);
					$("#bnksetup-state").val(bankInfo.state);
					$("#bnksetup-cty").val(bankInfo.city);
					$("#bnksetup-zcode").val(bankInfo.zipcode);
					$("#bnksetup-web").val(bankInfo.website);
					//ftypelist: ftypelist,
					//bplist:bplist,
					
					var contacts= bankInfo.contacts;
					
					if(contacts.length>1)
					{
														
						for (var i=1;i<contacts.length ;i++ )
						{
							//alert("Calling value of i "+i);

							if (i<contacts.length-1)
							{
								$('.add-row-bank').click();
							}
							
							
							var bankCtct = contacts[i];
							//alert(JSON.stringify(bankCtct));
							$("#contact_id"+i).val(bankCtct.contactId);
							$("#contact_fname"+i).val(bankCtct.firstName);
							$("#contact_lname"+i).val(bankCtct.lastName);
							$("#contact_crole"+i).val(bankCtct.contactRole);
							$("#contact_fphoneno"+i).val(bankCtct.contactNo);
							$("#contact_femail"+i).val(bankCtct.emailId);
							$("#contact_faddr1"+i).val(bankCtct.address1);
							$("#contact_faddr2"+i).val(bankCtct.address2);
							$("#contact_fstate"+i).val(bankCtct.state);
							$("#contact_fcity"+i).val(bankCtct.city);
							$("#contact_fzcode"+i).val(bankCtct.zipcode);
							
						}

					}
					
					setBankLicense(response);
					}
						
								   
				},

			 error:function(response,statusTxt,error){
			 
				/* alert("Failed to add record :"+response.responseJSON); */
			 }
			});	 
			 
		 }
		 else{
			 $.ajax({
					method: 'get',
					url: '../master/getData/'+siteId,
					contentType: 'application/json',
					dataType:'JSON',
					crossDomain:'true',
					success: function (response) {
						
							//alert("Added"+JSON.stringify(response));
							//alert("response.masterData "+response.masterData.busPros[0].processName);
							//location.href='manage-bank.jsp';
						if(response.masterData!=undefined&&response.masterData!=null)
						{/* 
							var buspros= response.masterData.busPros;

							for(j=0;j<buspros.length;j++)
							{
								//alert("In loop");
								//alert("buspros[j].processName"+JSON.stringify(buspros[j]));
								
								var chk = '<label><input type="checkbox" name="bscheckbox" class="icheck" data-checkbox="icheckbox_square-grey" value='+buspros[j].businessProcessId+'>'+buspros[j].processName+'</label>';
			
								$('#businessprocchk').append(chk).find('.icheck').iCheck({checkboxClass: 'icheckbox_square-grey'});
							} */
							var listBnkItems;
							var fileTypes= response.masterData.fileTypes;

							for(j=0;j<fileTypes.length;j++)
							{
								 listBnkItems += "<option value='"+fileTypes[j].filetypeid+"'>"+fileTypes[j].filetypename+"</option>";
								
							}
							
							
							$("#fileTypeChk").html('');
							$("#fileTypeChk").html(listBnkItems);
						 	$("#fileTypeChk").multiselect({
								 nonSelectedText:'Select File Type'
								 
							});
							 
						
							var bankSequence = response.masterData.bankFolderSequence[0];
							
							$("#bnk-folder").val(bankSequence.sequenceName+""+bankSequence.sequenceNumber);
							
						}
							
									   
					},

				 error:function(response,statusTxt,error){
				 
					/* alert("Failed to add record :"+response.responseJSON); */
				 }
				});
			 
			 
		 }
		 
		 
		 
		 
		 
	});
	function setBankLicense(response)
	{
		//var buspros= response.masterData.busPros;
		var bankInfo= response.bank;
		/* if(bankInfo.bplist!=undefined)
		{
			var bpList = bankInfo.bplist.split(",");
			for(j=0;j<buspros.length;j++)
			{
				var value = $.inArray(buspros[j].businessProcessId+"", bpList);
				//alert("value: "+value+" buspros[j].businessProcessId :"+buspros[j].businessProcessId +" bpList: "+bpList);
				var chk = '';

				if(value!=-1)
				{
					chk = '<label><input type="checkbox" required class="icheck" name="bcheckbox" checked="true" data-checkbox="icheckbox_square-grey" value='+buspros[j].businessProcessId+'>'+buspros[j].processName+'</label>';

				}
				else
				{
					chk = '<label><input type="checkbox" required name="bcheckbox" class="icheck" data-checkbox="icheckbox_square-grey" value='+buspros[j].businessProcessId+'>'+buspros[j].processName+'</label>';

				}
				
				//alert("chk:"+chk+" val "+value);

				$('#businessprocchk').append(chk).find('.icheck').iCheck({checkboxClass: 'icheckbox_square-grey'});
			}
		}
		else
		{
			for(j=0;j<buspros.length;j++)
			{
				var chk = '<label><input type="checkbox" class="icheck"  data-checkbox="icheckbox_square-grey" value='+buspros[j].businessProcessId+'>'+buspros[j].processName+'</label>';

				$('#businessprocchk').append(chk).find('.icheck').iCheck({checkboxClass: 'icheckbox_square-grey'});
			}
		} */
		var fileTypes= response.masterData.fileTypes;
		if(bankInfo.ftypelist!=undefined)
		{
			var ftypeList = bankInfo.ftypelist.split(",");
		
			var listBnkItems;
			var i=0;
			var otherBanks=[];
		var count=0;
			for(j=0;j<fileTypes.length;j++)
				
			{
				var value = $.inArray(fileTypes[j].filetypeid+"", ftypeList);
			
				if(value!=-1)
				{
				listBnkItems += "<option value='"+fileTypes[j].filetypeid+"'>"+fileTypes[j].filetypename+"</option>";
				 otherBanks[count]=fileTypes[j].filetypeid;
					count++;
				}else{
					listBnkItems += "<option value='"+fileTypes[j].filetypeid+"'>"+fileTypes[j].filetypename+"</option>";
					
				}
			}
		
			
			$("#fileTypeChk").html('');
			$("#fileTypeChk").html(listBnkItems);
			$("#fileTypeChk").multiselect({
				 nonSelectedText:'Select File Type',
				 
			});
			
			//$('.multiselect-container.dropdown-menu').slimScroll();
			$(".multiselect-container.dropdown-menu").mCustomScrollbar({
			theme:"dark"
			});
			$("#hidSelectedOptions").val(otherBanks);
			
			
			
			
			 $("#fileTypeChk").multiselect({
			       selectedText: "# of # selected"
			    });
			    var hidValue = $("#hidSelectedOptions").val();
			     //alert(hidValue);
			    var selectedOptions = hidValue.split(",");
			    for(var i in selectedOptions) {
			        var optionVal = selectedOptions[i];
			        $("#fileTypeChk").find("option[value="+optionVal+"]").prop("selected","selected");
			    }
			    $("#fileTypeChk").multiselect('refresh');
			
		}
		else
		{
			for(j=0;j<fileTypes.length;j++)
			{
				listBnkItems += "<option value='"+fileTypes[j].filetypeid+"'>"+fileTypes[j].filetypename+"</option>";

			}
			$("#fileTypeChk").html('');
			$("#fileTypeChk").html(listBnkItems);
			$("#fileTypeChk").multiselect({
				 nonSelectedText:'Select File Type',
				 
			});
			
			//$('.multiselect-container.dropdown-menu').slimScroll();
			$(".multiselect-container.dropdown-menu").mCustomScrollbar({
			theme:"dark"
			});	
			
		}
		
	}
	function submitBankForm()
	{

		
		//validfn();

		//alert($("#submit_form").serialize());
		//console.log($("#submit_form").serialize());
		//alert($("#contact_form").serialize());
		//alert($("#contact_fname").val());

		
		var numItems = $('div.bankaccount-single').size();
		
		var url_data = ($("#submit_form").serialize());//.replace(/+/g,'%20');//replace(/ /g, '+');
	
		var result = queryStringToJSON(url_data);
		//console.log(result);
	var bankInfo ="";
		var count=1;
		
		var contacts = [];

		contacts[0]=
		{
			firstName: $("#bnksetup-fname").val(),
			lastName: $("#bnksetup-lname").val(),
			contactNo: $("#bnksetup-ctct").val(),
			emailId: $("#bnksetup-email").val(),
			address1: $("#bnksetup-addr1").val(),
			address2: $("#bnksetup-addr2").val(),
			isdefault:'1',
			state: $("#bnksetup-state").val(),
			city: $("#bnksetup-cty").val(),
			zipcode:$("#bnksetup-zcode").val(),
			isactive:'1'
		};
		$(".contact_fname").each(function()
				
				{
							  var k  = $(this).attr("id1");
							 
				     /*       Editcustdetails[count]= 
								{
									 customerId:result["groupcustomername"+k],
									 bankId:result["groupbranchname"+k],
										userId:userId,
									isactive:'1'
										
								}  */
		   
			contacts[count]=
			{
				firstName: result["contact_fname"+k],
				lastName: result["contact_lname"+k],
				contactRole: result["contact_crole"+k],
				contactNo: result["contact_fphoneno"+k],
				emailId: result["contact_femail"+k],
				address1: result["contact_faddr1"+k],
				address2: result["contact_faddr2"+k],
				isdefault:'0',
				state: result["contact_fstate"+k],
				city: result["contact_fcity"+k],
				zipcode:result["contact_fzcode"+k],
				isactive:'1'
			}
		

		//alert("result contact_fname1"+result["contact_fname1"]);
		
		/* var bplist=''; */
		var ftypelist='';

		
		/* $('#businessprocchk input:checked').each(function() {
				bplist= bplist+$(this).val()+",";
			}); */
		$('#fileTypeChk option:checked').each(function() {
				ftypelist= ftypelist+$(this).val()+",";
			});
		//alert("bplist:"+bplist+" ftypelist:"+ftypelist);

		/* bplist = bplist.substring(0,bplist.length-1); */

		ftypelist = ftypelist.substring(0,ftypelist.length-1);
		
	

		 bankInfo = {
					siteId:siteId,
					bankName: $("#bnk-name").val(),
					bankCode:$("#bnk-ownid").val(),						
					branchOwnCode:$("#bnk-ownid").val(),
					branchLocation:$("#bnk-branchloc").val(),
					userId:userId,
					bankLogo: $("#bnk-bankLogo").val(),
					bankFolder:$("#bnk-folder").val(),
					locationId:$("#bnk-locid").val(),
					rtNo: $("#bnk-rtnum").val(),
					ddano: $("#bnk-dda").val(),
					contacts:contacts,
					isdefault:'1',
					state: $("#bnksetup-state").val(),
					city: $("#bnksetup-cty").val(),
					zipcode:$("#bnksetup-zcode").val(),
					website:$("#bnksetup-web").val(),
					ftypelist:ftypelist,
				/* 	bplist:bplist, */
					isactive:'1'
					
				}
		count++;
			
				});
		
		//swal('Good job!',"BankInfo :"+JSON.stringify(bankInfo),'success')
			$.ajax({
					method: 'post',
					url: '../bank/create',
					data: JSON.stringify(bankInfo),
					contentType: 'application/json',
					dataType:'JSON',
					crossDomain:'true',
					success: function (response) 
					{
						swal({title: "Done",text: "Bank Created Successfully",type: "success"},function(){window.location = "manage-bank.jsp";});
					},
					
				 error:function(response,statusTxt,error){
				 
				/* 	alert("Failed to add record :"+response.responseJSON); */
				 }
				});
		
		}

function submitUpdateBankForm()
	{
	
		
		//validfn();
		
		//alert($("#submit_form").serialize());
var bankInfo="";
		var numItems = $('div.bankaccount-single').size();
		var count=1;
		//alert(numItems);

		var url_data = ($("#submit_form").serialize());//.replace(/+/g,'%20');//replace(/ /g, '+');
	
		var result = queryStringToJSON(url_data);

		var contacts = [];

		contacts[0]=
		{
			contactId: $("#bnk-contactid").val(),
			branchId:  $("#bnk-branchid").val(),
			firstName: $("#bnksetup-fname").val(),
			lastName: $("#bnksetup-lname").val(),
			contactNo: $("#bnksetup-ctct").val(),
			emailId: $("#bnksetup-email").val(),
			address1: $("#bnksetup-addr1").val(),
			address2: $("#bnksetup-addr2").val(),
			isdefault:'1',
			state: $("#bnksetup-state").val(),
			city: $("#bnksetup-cty").val(),
			zipcode:$("#bnksetup-zcode").val(),
			isactive:'1'
		};

	$(".contact_fname").each(function()
				
				{
							  var k  = $(this).attr("id1");
							  
							 
			contacts[count]=
			{
				contactId:result["contact_id"+k],
				branchId:  $("#bnk-branchid").val(),
				firstName: result["contact_fname"+k],
				lastName: result["contact_lname"+k],
				contactRole: result["contact_crole"+k],
				contactNo: result["contact_fphoneno"+k],
				emailId: result["contact_femail"+k],
				address1: result["contact_faddr1"+k],
				address2: result["contact_faddr2"+k],
				isdefault:'0',
				state: result["contact_fstate"+k],
				city: result["contact_fcity"+k],
				zipcode:result["contact_fzcode"+k],
				isactive:'1'

			}
	

		/* var bplist=''; */
		var ftypelist='';
		/* 
		$('#businessprocchk input:checked').each(function() {
				bplist= bplist+$(this).val()+",";
			}); */
		$('#fileTypeChk option:checked').each(function() {
				ftypelist= ftypelist+$(this).val()+",";
			});
		//alert("bplist:"+bplist+" ftypelist:"+ftypelist);

	    //alert("$(bnk-id).val() "+$("#bnk-id").val());

		/* bplist = bplist.substring(0,bplist.length-1); */

		ftypelist = ftypelist.substring(0,ftypelist.length-1);

		
			 bankInfo = {
					siteId:siteId,
					bankName: $("#bnk-name").val(),
					bankCode: $("#bnk-ownid").val(),
					locationId:$("#bnk-locid").val(),
					branchLocation:$("#bnk-branchloc").val(),
					rtNo: $("#bnk-rtnum").val(),
					ddano: $("#bnk-dda").val(),
					bankId: $("#bnk-id").val(),
					branchId: $("#bnk-branchid").val(),
					bankLogo: $("#bnk-bankLogo").val(),
					bankStatus:$("#bnk-isactive").val(),
					userId:userId,
					contacts:contacts,
					isdefault:'1',
					state: $("#bnksetup-state").val(),
					city: $("#bnksetup-cty").val(),
					zipcode:$("#bnksetup-zcode").val(),
					website:$("#bnksetup-web").val(),
					ftypelist: ftypelist,
					/* bplist:bplist, */
					isactive:'1'
					
				}
			count++;
			
	});
	
		$.ajax({
					method: 'post',
					url: '../bank/update',
					data: JSON.stringify(bankInfo),
					contentType: 'application/json',
					dataType:'JSON',
					crossDomain:'true',
					success: function (response) {
						 swal({title: "Done",text: "Bank Updated Successfully",type: "success"},function(){window.location = "manage-bank.jsp";});
							// swal('Done!','Added','success'),function()
							// {
								 // window.location = 'manage-bank.jsp';
							// }
							//location.href='manage-bank.jsp';
					},

				 error:function(response,statusTxt,error){
				 
					/* alert("Failed to add record :"+response.responseJSON); */
				 }
				});
		
	}


   var queryStringToJSON = function (url) {
    if (url === '')
        return '';
    var pairs = (url || location.search).slice(1).split('&');
    var result = {};
    for (var idx in pairs) {
        var pair = pairs[idx].split('=');
        if (!!pair[0])
            result[pair[0].toLowerCase()] = decodeURIComponent(pair[1] || '');
    }
    return result;
}
   
   
   


   /*formwizard start*/
   var FormWizard = function () {
   return {
           //main function to initiate the module
           init: function () {
               if (!jQuery().bootstrapWizard) {
                   return;
               }

               function format(state) {
                   if (!state.id) return state.text; // optgroup
                   return "<img class='flag' src='../../assets/global/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
               }

               // $("#country_list").select2({
                   // placeholder: "Select",
                   // allowClear: true,
                   // formatResult: format,
                   // formatSelection: format,
                   // escapeMarkup: function (m) {
                       // return m;
                   // }
               // });

               var form = $('#submit_form');
               var error = $('.alert-danger', form);
               var success = $('.alert-success', form);

               form.validate({
                   doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                   errorElement: 'span', //default input error message container
                   errorClass: 'help-block help-block-error', // default input error message class
                   focusInvalid: false, // do not focus the last invalid input
                   rules: {
                       //account
                       username: {
                           minlength: 5,
                           required: true
                       },
                       password: {
                           minlength: 5,
                           required: true
                       },
                       rpassword: {
                           minlength: 5,
                           required: true,
                           equalTo: "#submit_form_password"
                       },
                       //profile
                       fullname: {
                           required: true
                       },
                       url:
   					{
   						required: true,
   						url: true
   					},
                       email: {
                           required: true,
                           email: true
                       },
                       phone: {
                           required: true
                       },
                       gender: {
                           required: true
                       },
                       address: {
                           required: true
                       },
                       city: {
                           required: true
                       },
                       country: {
                           required: true
                       },
                       //payment
                       card_name: {
                           required: true
                       },
                       card_number: {
                           minlength: 16,
                           maxlength: 16,
                           required: true
                       },
                       card_cvc: {
                           digits: true,
                           required: true,
                           minlength: 3,
                           maxlength: 4
                       },
                       card_expiry_date: {
                           required: true
                       },
                       'payment[]': {
                           required: true,
                           minlength: 1
                       }
                   },

                   messages: { // custom messages for radio buttons and checkboxes
                       'payment[]': {
                           required: "Please select at least one option",
                           minlength: jQuery.validator.format("Please select at least one option")
                       }
                   },

                   errorPlacement: function (error, element) { // render error placement for each input type
                       if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                           error.insertAfter("#form_gender_error");
                       } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                           error.insertAfter("#form_payment_error");
                       } else {
                           error.insertAfter(element); // for other inputs, just perform default behavior
                       }
                   },

                   invalidHandler: function (event, validator) { //display error alert on form submit   
                       success.hide();
                       error.show();
                       Metronic.scrollTo(error, -200);
                   },

                   highlight: function (element) { // hightlight error inputs
                       $(element)
                           .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                   },

                   unhighlight: function (element) { // revert the change done by hightlight
                       $(element)
                           .closest('.form-group').removeClass('has-error'); // set error class to the control group
                   },

                   success: function (label) {
                       if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                           label
                               .closest('.form-group').removeClass('has-error').addClass('has-success');
                           label.remove(); // remove error label here
                       } else { // display success icon for other inputs
                           label
                               .addClass('valid') // mark the current input as valid and display OK icon
                           .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                       }
                   },

                   submitHandler: function (form) {
                       success.show();
                       error.hide();
                       //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
                   }

               });

               var displayConfirm = function() {
                   $('#tab4 .form-control-static', form).each(function(){
                       var input = $('[name="'+$(this).attr("data-display")+'"]', form);
                       if (input.is(":radio")) {
                           input = $('[name="'+$(this).attr("data-display")+'"]:checked', form);
                       }
                       if (input.is(":text") || input.is("textarea")) {
                           $(this).html(input.val());
                       } else if (input.is("select")) {
                           $(this).html(input.find('option:selected').text());
                       } else if (input.is(":radio") && input.is(":checked")) {
                           $(this).html(input.attr("data-title"));
                       } else if ($(this).attr("data-display") == 'payment[]') {
                           var payment = [];
                           $('[name="payment[]"]:checked', form).each(function(){ 
                               payment.push($(this).attr('data-title'));
                           });
                           $(this).html(payment.join("<br>"));
                       }
                   });
               }

               var handleTitle = function(tab, navigation, index) {
                   var total = navigation.find('li').length;
                   var current = index + 1;
                   // set wizard title
                   $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
                   // set done steps
                   jQuery('li', $('#form_wizard_1')).removeClass("done");
                   var li_list = navigation.find('li');
                   for (var i = 0; i < index; i++) {
                       jQuery(li_list[i]).addClass("done");
                   }
   			if(bankId!=null){
   	//alert("if");
                   if (current == 1) {
                       $('#form_wizard_1').find('.button-previous').hide();
                   } else {
                       $('#form_wizard_1').find('.button-previous').show();
                   }

                   if (current >= total) {
                       $('#form_wizard_1').find('.button-next').hide();
                       $('#form_wizard_1').find('.button-submit').show();
                       $('#form_wizard_1').find('.button-save').hide();
                       displayConfirm();
                   } else {
                       $('#form_wizard_1').find('.button-next').show();
                       $('#form_wizard_1').find('.button-submit').hide();
                   }
   				
   			}else{
   				//alert("else");
   				if (current == 1) {
                       $('#form_wizard_1').find('.button-previous').hide();
                   } else {
                       $('#form_wizard_1').find('.button-previous').show();
                   }

                   if (current >= total) {
                       $('#form_wizard_1').find('.button-next').hide();
                       $('#form_wizard_1').find('.button-submit').hide();
                       $('#form_wizard_1').find('.button-save').show();
                       displayConfirm();
                   } else {
                       $('#form_wizard_1').find('.button-next').show();
                       $('#form_wizard_1').find('.button-submit').hide();
                   }
   			}
   			
                   Metronic.scrollTo($('.page-title'));
               }

               // default form wizard
               $('#form_wizard_1').bootstrapWizard({
                   'nextSelector': '.button-next',
                   'previousSelector': '.button-previous',
                   onTabClick: function (tab, navigation, index, clickedIndex) {
                       return false;
                       /*
                       success.hide();
                       error.hide();
                       if (form.valid() == false) {
                           return false;
                       }
                       handleTitle(tab, navigation, clickedIndex);
                       */
                   },
                   onNext: function (tab, navigation, index) {
                       success.hide();
                       error.hide();

                       if (form.valid() == false) {
                           return false;
                       }

                       handleTitle(tab, navigation, index);
                   },
                   onPrevious: function (tab, navigation, index) {
                       success.hide();
                       error.hide();

                       handleTitle(tab, navigation, index);
                   },
                   onTabShow: function (tab, navigation, index) {
                       var total = navigation.find('li').length;
                       var current = index + 1;
                       var $percent = (current / total) * 100;
                       $('#form_wizard_1').find('.progress-bar').css({
                           width: $percent + '%'
                       });
                   }
               });

               $('#form_wizard_1').find('.button-previous').hide();
               
   			$('#form_wizard_1 .button-submit').click(function () {
                   //alert('Finished! Hope you like it :)');
               }).hide();

               //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
               $('#country_list', form).change(function () {
                   form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
               });
           }

       };

   }();
	</script>
	
	
	
<!-- END BODY -->
</html>