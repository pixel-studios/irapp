<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<head>
	

</head>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Batch Information</h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<div class="col-md-6 nopad">
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="#">File Info</a> <i class="fa fa-circle"></i>
				</li>
				<li class="active">
					Batch Info 
				</li>
			</ul>
			</div>
			<div class="col-md-6 nopad">
				<h1 class="file-name" id="fileName"><i class="fa fa-file-text"></i> </h1>
			</div>
			<div class=" margin-top-10">
				<div class="col-md-12 portlet light">
					<div class="col-md-3 nopad-left tree-list">
						<ul class="cd-accordion-menu animated">
							<li class="has-children">
								<input type="checkbox" name ="group-1" id="group-1" checked>
								<label for="group-1" class="active">Batch <span id ="batchNumber"></span></label>
								<ul id="first-level-list">
									<!-- <li class="first-level-list" >
										<input type="checkbox" name ="sub-group-1" id="sub-group-1" checked>
										<label for="sub-group-1">Item 1</label>

										<ul class="second-level-list" id="">
											
											<li><a href="#0">Scan Doc</a></li>
										</ul>
									</li> -->
								</ul>
							</li>
						</ul>
						<div class="batchform-wraper">
						<div class="medium-title">Batch - Info</div>
						<div class="form-group">
						<div class="row">
							<div class="col-md-6">Customer Name <span class="pull-right">:</span></div>
							<div class="col-md-6"><span id="customerName"></span></div>
						</div>
						</div>
						<div class="form-group">
						<div class="row">
							<div class="col-md-6">Transactions <span class="pull-right">:</span></div>
							<div class="col-md-6"><span id="transactionSize"></span></div>
						</div>
						</div>
						
						<div class="form-group">
						<div class="row">
							<div class="col-md-6">Scan Doc <span class="pull-right">:</span></div>
							<div class="col-md-6"><span id="scanDocSize"></span></div>
						</div>
						
						</div>
						<div class="form-group">
						<div class="row">
							<div class="col-md-6">Total Credit <span class="pull-right">:</span></div>
							<div class="col-md-6"><span id="totalCredit"></span></div>
						</div>
						
						</div>
						<div class="form-group">
						<div class="row">
							<div class="col-md-6">Total Debit <span class="pull-right">:</span></div>
							<div class="col-md-6"><span id="totalDebit"></span></div>
						</div>
						
						</div>
						
						</div>
					</div>	
					<div class="col-md-9  tree-list-content">
						<div class="col-sm-12 nopad tree-border-bottom">
							 <div class="col-sm-6 nopad text-right pull-right" id="backButton">
								
							</div>
							<div class="col-sm-6 nopad">
								<ul>
									<li>
										<a href="">Batch</a>
									</li>
									<li>
										<span> > </span>
									</li>
									<li>
										<a href="">Transactions </a>
									</li>
									
								</ul>
							</div>
						</div>	
						<div class="col-md-12 paymentremit-wraper">
						<div class="row">
							<div class="col-md-6 lightborder-right">
								<div class="medium-title">Payment</div>
								<div class="col-md-12 cheque-wraper">
								  <div class="col-md-12 nopad form-group">
								  <div class="row">
								     <div class="col-md-6">
										<div><input type ="text"  class="white-formfield text-left" id="p_name"></div>
								   	     						   		
								   	  </div>
								  	  <div class="col-md-6 text-right">
									  <div><input type ="text"  class="white-formfield text-right"></div>
								     </div>
								  </div>
								  </div>
								  <div class="col-md-12 nopad">
								  <div class="col-md-12 nopad form-group accname">
								     <p>PAY TO THE ORDER OF</p>
								    	<div class="row">
								    	<div class="col-md-7 nopad-right">
										<input type ="text" class="individualName form-control" id="payTo">
										</div>
								    	<div class="col-md-5">
										
										<div class="input-group">
											  <span class="input-group-addon">$</span>
											  <input type="text" class="form-control amount" id="p_amount">
											 
										</div>
										</div>
										</div>
										</div>
								      
								  </div>
								  <div class="col-md-12 form-group">
								      <div class="row">
									  <div class="col-md-10 nopad">
									  <input type ="text" id="amountInWords" class="amount-words">
									</div>	
									<div class="col-md-2 nopad text-right">
									  <div class="amtword-caption">DOLLARS</div>
									  </div>
									  </div>
									  </div>
								  <div class="col-md-12 nopad">
								  <div class="row">
								     <div class="col-md-6">
										<div><input type ="text"  class="white-formfield text-left" id="memoNo"></div>
								   	     <h6>Memo</h6>
								   		
								   	  </div>
								  	  <div class="col-md-6 text-right">
									  <div><input type ="text"  class="white-formfield text-right" id="p_signature"></div>
								     </div>
								  </div>
								  <div class="row">
								     <div class="col-md-6">
										<div><input type ="text"  class="white-formfield text-left" id="p_receivingDfiAccount"></div>
								   	    							   		
								   	  </div>
								  	  <div class="col-md-6 text-right">
									  <div><input type ="text"  class="white-formfield text-right" id="p_dfiAccountNo"></div>
								     </div>
								  </div>
								  </div>
							  </div>
								
								<div class="col-md-12 form-file-process nopad">
							<form action="">
							
								<div class="row">
								<fieldset class="col-md-6">	
									<label class="col-md-12 control-label nopad">Record Type Code</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" id="recordTypeCode" placeholder="">
										</div>
								</fieldset>
								
								<fieldset class="col-md-6 ">	
									<label class="col-md-12 control-label nopad">Transaction code</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="transactionCode">
										</div>
								</fieldset>	
								
								
								
							</div>
							<div class="row">
								<fieldset class="col-md-6">	
									<label class="col-md-12 control-label nopad">Receiving DFI Identification</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="receivingDFI">
										</div>
								</fieldset>
								<fieldset class="col-md-6">	
									<label class="col-md-12 control-label nopad">DFI Account No</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="dfiAccountNo">
										</div>
								</fieldset>
							</div>
							<div class="row">
								
								<fieldset class="col-md-6 ">	
									<label class="col-md-12 control-label nopad">Individual Identification No</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="individualId">
										</div>
								</fieldset>
								<fieldset class="col-md-6">	
									<label class="col-md-12 control-label nopad">Individual Name</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="individualName">
										</div>
								</fieldset>	
							</div>
							<div class="row">
								
								
								<fieldset class="col-md-6 ">	
									<label class="col-md-12 control-label nopad">Check Digit</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="checkDigit">
										</div>
								</fieldset>
								<fieldset class="col-md-6">	
									<label class="col-md-12 control-label nopad">Amount</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" class="amount" id="amount">
										</div>
								</fieldset>
							</div>
								
								
							
							
							
									
							</form>	
						</div>
							</div>
							<div class="col-md-6 ">
							<div class="medium-title">Remittance</div>
							<div class="col-md-12 cheque-wraper">
								  <div class="col-md-12 nopad form-group">
								  <div class="row">
								      <div class="col-md-6 ">
									      <h6></h6>
									  	</div>
									  	<div class="col-md-6 text-right">
									      <h6>DATE : </h6>
									      
									 	</div>
								  </div>
								  </div>
								  <div class="col-md-12 nopad">
								  <div class="col-md-12 nopad form-group accname">
								     <p>ACCT #</p>
								    	<div class="row">
								    	<div class="col-md-7 nopad-right">
										<input type ="text" class="individualName form-control" id="r_name">
										</div>
								    	<div class="col-md-5">
										<div><input type ="text"  class="white-formfield text-left" id="r_name"></div>
										<div class="input-group">
											  <span class="input-group-addon">$</span>
											  <input type="text" class="form-control amount" id="r_amount">
											 
											</div>
										</div>
										</div>
										</div>
								      
								  </div>
								  <div class="col-md-12 form-group">
								      <div class="row">
									  <div class="col-md-10 nopad">
									  <input type ="text" class="amount-words" id="r_accountNo">
									</div>	
								</div>
									  </div>
								   <div class="col-md-12 nopad">
								  <div class="row">
								     <div class="col-md-6">
										<div><input type ="text"  class="white-formfield text-left" id="traceNo"></div>
								   	     <h6>Trace Number</h6>
								   		
								   	  </div>
								  	  <div class="col-md-6 text-right">
									  <div><input type ="text"  class="white-formfield text-right"></div>
								         <h6></h6>
								   		</div>
								  </div>
								  </div>
							  </div>
							<div class="col-md-12 form-file-process">
							<form action="">
								
							<div class="row">
									<fieldset class="col-md-6 ">	
										<label class="col-md-12 control-label nopad">Trace Number</label>
											<div class="col-md-12 nopad">
												<input type="text" class="form-control" placeholder="" id="traceNumber">
											</div>
									</fieldset>	
									
									<fieldset class="col-md-6 ">	
										<label class="col-md-12 control-label nopad">Format Code</label>
											<div class="col-md-12 nopad">
												<input type="text" class="form-control" placeholder="" id="discretionaryData">
											</div>
									</fieldset>	
									
								</div>
								
								
							<div class="row">
									<fieldset class="col-md-6 ">	
										<label class="col-md-12 control-label nopad">Addenda Record Indicator</label>
											<div class="col-md-12 nopad">
												<input type="text" class="form-control" placeholder="" id="addendaIndicator">
											</div>
									</fieldset>	
									
									<fieldset class="col-md-6 ">	
										<label class="col-md-12 control-label nopad">Discretionary Data</label>
											<div class="col-md-12 nopad">
												<input type="text" class="form-control" placeholder="" id="discretionaryData">
											</div>
									</fieldset>	
									
								</div>
								
								
								
									
							</form>	
						</div>
						</div>
						
						</div>
						<div class="col-md-12 nopad">
							<div class="medium-title">Scan Reports</div>
							<div class="col-md-12  nopad content-para" id="scanDoc">
							<p></p>
							</div>
						</div>
						
					<!--  	<div class="col-md-3 col-md-offset-9 col-sm-3 col-sm-offset-9 col-xs-12 text-right nopad">
									<div class="form-actions noborder form-group">
										<button type="button" class="btn default">Cancel</button>
										<button type="button" class="btn blue">Update</button>
									</div>
						</div>-->
						
						
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<script>
     var batchHeaderRecordId = <%=request.getParameter("batchHeaderRecordId")%>;
     var entryDetails=null;
     $(document).ready(function()
    	{  	
    
 		$.ajax({
 	
 			method:'get',
 			url:'../file-process/getEntry/'+batchHeaderRecordId,
 			contentType:'application/json',
 			dataType:'JSON',
 			crossDomain:'true',
 			success:function(response){
 						entryDetails=response.bhrBean[0]			
 					var companyName=response.bhrBean[0].companyName;
 					var batchNumber=response.bhrBean[0].batchNumber;
 					
 					var fileName=response.bhrBean[0].fileHeaderRecordBean.achFileName;
 					var backButton=response.bhrBean[0].fileHeaderRecordBean.fileHeaderRecordId;
 					var transactionSize=response.bhrBean[0].entryDetailRecordBean.length;
 					var scanDocSize=response.bhrBean[0].cCDAddendaRecordBean.length;
 					
 					
 					var totalCredit=response.bhrBean[0].batchControlRecordBean[0].totalcreditEntryDollarAmount;
 					var totalDebit=response.bhrBean[0].batchControlRecordBean[0].totalDebitEntryDollarAmount;
 				
 					
 					$("#customerName").append(companyName);
 					$("#batchNumber").append(batchNumber);
 					$("#fileName").append(fileName);
 					$("#backButton").append('<a href="file-process-detail.jsp?fileHeaderRecordId='+backButton+'" class="back-btn"><i class="fa fa-reply"> </i>Back to file process</a>');
 					$("#transactionSize").append(transactionSize);
 					$("#scanDocSize").append(scanDocSize);
 					
 			
 					var i=1;
 					for (var j = 0; j < response.bhrBean[0].entryDetailRecordBean.length; j++){
						
	 					  $("#first-level-list").append('<li class="items"><a href="javascript:;" onclick="getData('+j+')">Transaction '+i+'</a></li>');
	 					
	 					i++;
	 					
	 				}	
 					
 					$("#p_name").val(entryDetails.entryDetailRecordBean[0].individualIdentificationNumber);
 				     $("#traceNo").val(entryDetails.entryDetailRecordBean[0].traceNumber);
 				    $("#p_signature").val(entryDetails.entryDetailRecordBean[0].individualIdentificationNumber);
 				    $("#p_receivingDfiAccount").val(entryDetails.entryDetailRecordBean[0].receivingDFIIdentification);
 				 	 $("#p_dfiAccountNo").val(entryDetails.entryDetailRecordBean[0].dfiAccountNumber);
 				 	$("#totalCredit").append(totalCredit);
				     $("#totalDebit").append(totalDebit);
	 				 $("#p_signature").val(entryDetails.entryDetailRecordBean[0].individualIdentificationNumber);
						
	 				 $("#p_amount").val(entryDetails.entryDetailRecordBean[0].amount);
	 		    	   $("#r_amount").val(entryDetails.entryDetailRecordBean[0].amount);
	 		    	  $("#r_accountNo").val(entryDetails.entryDetailRecordBean[0].individualIdentificationNumber);
	 		    	 $("#r_name").val(entryDetails.entryDetailRecordBean[0].individualName);
						
	 		    	  
 					$("#memoNo").val(+entryDetails.entryDetailRecordBean[0].traceNumber);
 					$("#amountInWords").val(entryDetails.entryDetailRecordBean[0].amountInWords);
 		    	    $("#payTo").val(entryDetails.entryDetailRecordBean[0].individualName);
 		    	   
 	 				$("#recordTypeCode").val(entryDetails.entryDetailRecordBean[0].recordTypeCode);
					$("#transactionCode").val(entryDetails.entryDetailRecordBean[0].transactionCode);
					$("#receivingDFI").val(entryDetails.entryDetailRecordBean[0].receivingDFIIdentification);
					$("#checkDigit").val(entryDetails.entryDetailRecordBean[0].checkDigit);
					$("#dfiAccountNo").val(entryDetails.entryDetailRecordBean[0].dfiAccountNumber);
					$("#amount").val(entryDetails.entryDetailRecordBean[0].amount);
					$("#individualId").val(entryDetails.entryDetailRecordBean[0].individualIdentificationNumber);
					$("#individualName").val(entryDetails.entryDetailRecordBean[0].individualName);
					$("#addendaIndicator").val(entryDetails.entryDetailRecordBean[0].addendaRecordIndicator);
					$("#traceNumber").val(entryDetails.entryDetailRecordBean[0].traceNumber);
					$("#discretionaryData").val(entryDetails.entryDetailRecordBean[0].discretionaryData);
 					
 				//	for (var j = 0; j < entryDetails.entryDetailRecordBean.length; j++){
						
 						//$("#traceNumber").val(entryDetails.entryDetailRecordBean[0].traceNumber);
 						//$("#dv_bind_data").append(entryDetails.entryDetailRecordBean[j].traceNumber+'<br/>');
	 			//		
	 				//}		
					
 				
 				if(response.bhrBean[0].cCDAddendaRecordBean.length>0){
 				
					for (var j = 0; j < response.bhrBean[0].cCDAddendaRecordBean.length; j++){
						
	 					  $("#scanDoc p").append(response.bhrBean[0].cCDAddendaRecordBean[j].paymentRelatedInformation);
	 					
	 					i++;
	 					
	 				}		
 				}else{
 					
					  $("#scanDoc p").append('No Scan Content Available.');

 				}
 			
					
 					      
 						   
			},

		 error:function(response,statusTxt,error){
		 
			
		 }	
 		
 		});
    	 
    	 
    	 
    	 
    	});



	  function getData(entryId){
    	     var k = entryId; 					
    	     $("#p_individualId").val(entryDetails.entryDetailRecordBean[k].individualIdentificationNumber);
			$("#p_name").val(entryDetails.entryDetailRecordBean[k].individualIdentificationNumber);

    	     $("#traceNo").val(entryDetails.entryDetailRecordBean[k].traceNumber);
    	     $("#p_signature").val(entryDetails.entryDetailRecordBean[k].individualIdentificationNumber);
			 $("#p_receivingDfiAccount").val(entryDetails.entryDetailRecordBean[k].receivingDFIIdentification);
			$("#p_dfiAccountNo").val(entryDetails.entryDetailRecordBean[k].dfiAccountNumber);
			 $("#p_amount").val(entryDetails.entryDetailRecordBean[k].amount);
	    	 $("#r_amount").val(entryDetails.entryDetailRecordBean[k].amount);
	    	 $("#r_accountNo").val(entryDetails.entryDetailRecordBean[k].individualIdentificationNumber);
		    $("#r_name").val(entryDetails.entryDetailRecordBean[k].individualName);
			

    	     $("#amountInWords").val(entryDetails.entryDetailRecordBean[k].amountInWords);
     	    $("#payTo").val(entryDetails.entryDetailRecordBean[k].individualName);
     	    $("#amountInFigure").val(entryDetails.entryDetailRecordBean[k].amount);
     	   $("#memoNo").val(entryDetails.entryDetailRecordBean[k].traceNumber);
  			$("#recordTypeCode").val(entryDetails.entryDetailRecordBean[k].recordTypeCode);
  			$("#transactionCode").val(entryDetails.entryDetailRecordBean[k].transactionCode);
  			$("#receivingDFI").val(entryDetails.entryDetailRecordBean[k].receivingDFIIdentification);
  			$("#checkDigit").val(entryDetails.entryDetailRecordBean[k].checkDigit);
  			$("#dfiAccountNo").val(entryDetails.entryDetailRecordBean[k].dfiAccountNumber);
  			$("#amount").val(entryDetails.entryDetailRecordBean[k].amount);
  			$("#individualId").val(entryDetails.entryDetailRecordBean[k].individualIdentificationNumber);
  			$("#individualName").val(entryDetails.entryDetailRecordBean[k].individualName);
  			$("#addendaIndicator").val(entryDetails.entryDetailRecordBean[k].addendaRecordIndicator);
  			$("#traceNumber").val(entryDetails.entryDetailRecordBean[k].traceNumber);
  			$("#discretionaryData").val(entryDetails.entryDetailRecordBean[k].discretionaryData);
	     	}
	      
	   
      
     
     
     
     
     
     
     
</script>

</body>
<!-- END BODY -->
</html>