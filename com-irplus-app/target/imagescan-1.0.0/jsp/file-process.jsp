<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<head>


</head>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<!-- <div class="page-title">
				<h1>Batch Information</h1>
			</div> -->
			<!-- END PAGE TITLE -->
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container-fluid">
			<!-- BEGIN PAGE BREADCRUMB -->
		<!--<div class="col-md-6 nopad">
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="#">File Info</a> <i class="fa fa-circle"></i>
				</li>
				<li class="active">
					Batch Info
				</li>
			</ul>
			</div> -->
			<!-- <div class="col-md-6 nopad">
				<h1 class="file-name" id="fileName"><i class="fa fa-file-text"></i> </h1>
			</div> -->
			<div class=" margin-top-10">
				<div class="col-md-12 portlet light">
				<div class="pagestick-title">
								<span> Batch Info</span>
							</div>

                <div class="batch-info-block clearfix">

                   <!--<span class="batchtitle" >Batch Info</span>-->
			  <div class="batchinfo-table clearfix fileprocess-topdetail">
                   <table width="100%" border="0" cellspacing="0" cellpadding="0">
						<col width="10%"></col>
						<col width="10%"></col>
						<col width="10%"></col>
						<col width="10%"></col>
						<col width="10%"></col>
						<col width="10%"></col>
						<col width="10%"></col>
						<col width="10%"></col>	
						<col width="10%"></col>
						<col width="10%"></col>
					  
					  <thead>
						 <tr>
					      <th>
						  <div class="">Bank Name</div>
					      </th>
					       <th>
						   <div class="">Customer Name</div>
					      </th>
					      <th>
						  <div class="">File Name</div>  
					      </th>
					      <th>
						  <div class="">File Type</div>
					      </th>
					       <th>
						   <div class="">Payment Date</div> 
					    </th> 
						<th>
							<div class="">Amount</div>
					      </th>
					      <th>
						  <div class="">Transaction</div>
					      </th>
					       <th>
						   <div class="">Payment</div>
					      </th>
					       <th>
						   <div class="">Remittance</div>
					      </th>
					       <th>
						   <div class="">Scan Doc</div>
					      </th>
					    </tr>
					  </thead>
					  <tbody>
					 
					    <tr>
					    <td>
						  <div class=""> <span id="bankName" ></span></div>
						  </td>
					      <td>
						  <div class=""><span id="customerName"></span></div>
						  </td>
					       <td>
							<div class=""><span id="fileName"></span></div>
						  </td>
					       <td>
							<div class=""><span id="fileType" ></span></div>
						  </td>
					       <td>
						  <div class=""><span id="paymentDate"></span></div>
						  </td>
						  <td>
							
					      <div class=""><span>$</span> <span id="totalCredit"></span></div>
						  </td>
					      <td>
						  
					      <div class=""><span id="transactionCount"></span></div>
						  </td>
					       <td>
						   
					      <div class=""><span id="paymentCount"></span></div>
						  </td>
					       <td>
							<div class=""><span id="remittanceCount"></span></div>
						  </td>
					       <td>
						   
					      <div class=""><span id="scanCount"></span></div>
						  </td>
					     
					    </tr>
					    
					  </tbody>
					</table>

                   </div>


                    </div>

                  <div class="col-md-4 nopad-left">
                   <div class="batchtrans-leftbar clearfix">

							<table width="100%" border="0" cellspacing="0" cellpadding="0">

			                 <thead>
			                 <tr>
			                 <th  class="bg-blue" colspan="3" onclick="getBatch()">Batch <span id ="batchNo"></span></th>
			                 </tr>

			                 <tr class="bg-gray">
			                 <th>Transactions</th>
			                 <th class="text-center">Item Type</th>
			                 <th class="text-right">Amount</th>
			                 </tr>

			                 </thead>

										  <tbody id="transactionList"></tbody></table>

			<!-- 							        <table width="100%" border="0" cellspacing="0" cellpadding="0">

                 <thead>
                 <tr>
                 <th  class="bg-blue" colspan="3" >Batch <span id ="batchNumber"></span></th>
                 </tr>

                 <tr class="bg-gray">
                 <th>Transaction ID</th>
                 <th>Item Type</th>
                 <th align="right">Amount</th>
                 </tr>

                 </thead>

  <tbody >
    <tr  data-toggle="tooltip" data-placement="top" title="Bootstrap is a sleek, intuitive, and powerful front-end framework for faster1.">
      <td>1</td>
      <td><span class="remittance-icon" ><img src="../resources/assets/admin/layout3/img/remittance-icon.png"/></span></td>
      <td>130.00</td>
    </tr>
     <tr data-toggle="tooltip" data-placement="top" title="Bootstrap is a sleek, intuitive, and powerful front-end framework for faster2.">
       <td>&nbsp;</td>
       <td><span class="payment-icon" ><img src="../resources/assets/admin/layout3/img/payment-icon.png"/></span></td>
       <td>130.00</td>
     </tr>
     <tr data-toggle="tooltip" data-placement="top" title="Bootstrap is a sleek, intuitive, and powerful front-end framework for faster3.">
       <td>&nbsp;</td>
       <td><span class="remittance-icon" ><img src="../resources/assets/admin/layout3/img/remittance-icon.png"/></span></td>
       <td>130.00</td>
     </tr>
     <tr>
       <td>2</td>
       <td><span class="payment-icon" ><img src="../resources/assets/admin/layout3/img/payment-icon.png"/></span></td>
       <td>130.00</td>
     </tr>
     <tr>
       <td>&nbsp;</td>
       <td><span class="remittance-icon" ><img src="../resources/assets/admin/layout3/img/remittance-icon.png"/></span></td>
       <td>130.00</td>
     </tr>
     <tr>
       <td>&nbsp;</td>
       <td><span class="payment-icon" ><img src="../resources/assets/admin/layout3/img/payment-icon.png"/></span></td>
       <td>130.00</td>
     </tr>
     </tbody>
</table> -->

                   </div>
                  </div>


                   <div class="col-md-8 nopad-left">
				   
                   <div class="batch-trans-detail-right batchtrans-wraper clearfix">
				   
				   
				   <div class="fileprocess-tabwraper">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
	<a href="#firsttab" aria-controls="home" role="tab" data-toggle="tab" id="imageHeader">Payment</a>
	</li>
    <li role="presentation">
	<a href="#secondtab" aria-controls="profile" role="tab" data-toggle="tab">Item Information</a>
	</li>

  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
  					 
    <div role="tabpanel" class="tab-pane active" id="firsttab">
	<div class="hover-btns">
							<span class="download-wraper">
								<a href="javascript:void(0);"  class="download-icon downloadfront" id="downloadFront"> <i class="fa fa-download" aria-hidden="true"></i></a>
								<a href="#"  class="download-icon downloadrear" id="downloadRear"> <i class="fa fa-download" aria-hidden="true"></i></a>
							</span>
							<span>
								<a href="javascript:void(0);" class="flip-icon fliptrigger"><i class="fa fa-rotate-left" aria-hidden="true"></i></a>
							</span>
							<span>
								<a href="javascript:void(0);" class="zoom-icon"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
							</span>							
							
							</div>
		 <div class="col-md-12 cheque-wraper imageviewer">

						<div class="cheque-image">
						
							<div class="flipmain-wraper">
							
							
							<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
							<div class="flipper">
							<div class="front">
								<!-- front content -->
								<div class="img-magnifier-container"></div>
								<img id="frontImage" src=""  data-zoom-image="" class="img-responsive magnifier" alt=""/>
								
							</div>
							<div class="back">
							<!-- back content -->
							<img id="rearImage" src=""  data-zoom-image="" class="img-responsive magnifier"  alt=""/>
							
							</div>
							</div>
							</div>
							
						</div>
						</div>
								 
								 
								
					</div>
	</div>
    <div role="tabpanel" class="tab-pane" id="secondtab">
		<form action="">

								<div class="row">
								<fieldset class="col-md-6">
									<label class="col-md-12 control-label nopad">Record Type Code</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" id="recordTypeCode" placeholder="">
										</div>
								</fieldset>

								<fieldset class="col-md-6 ">
									<label class="col-md-12 control-label nopad">Transaction code</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="transactionCode">
										</div>
								</fieldset>



							</div>
							<div class="row">
								<fieldset class="col-md-6">
									<label class="col-md-12 control-label nopad">Receiving DFI Identification</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="receivingDFI">
										</div>
								</fieldset>
								<fieldset class="col-md-6">
									<label class="col-md-12 control-label nopad">DFI Account No</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="dfiAccountNo">
										</div>
								</fieldset>
							</div>
							<div class="row">

								<fieldset class="col-md-6 ">
									<label class="col-md-12 control-label nopad">Individual Identification No</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="individualId">
										</div>
								</fieldset>
								<fieldset class="col-md-6">
									<label class="col-md-12 control-label nopad">Individual Name</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="individualName">
										</div>
								</fieldset>
							</div>
							<div class="row">


								<fieldset class="col-md-6 ">
									<label class="col-md-12 control-label nopad">Check Digit</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="checkDigit">
										</div>
								</fieldset>
								<fieldset class="col-md-6">
									<label class="col-md-12 control-label nopad">Amount</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" class="amount" id="amount">
										</div>
								</fieldset>
							</div>
						</form>
							<form action="">

							<div class="row">
									<fieldset class="col-md-6 ">
										<label class="col-md-12 control-label nopad">Trace Number</label>
											<div class="col-md-12 nopad">
												<input type="text" class="form-control" placeholder="" id="traceNumber">
											</div>
									</fieldset>

									<fieldset class="col-md-6 ">
										<label class="col-md-12 control-label nopad">Format Code</label>
											<div class="col-md-12 nopad">
												<input type="text" class="form-control" placeholder="" id="discretionaryData">
											</div>
									</fieldset>

								</div>


							<div class="row">
									<fieldset class="col-md-6 ">
										<label class="col-md-12 control-label nopad">Addenda Record Indicator</label>
											<div class="col-md-12 nopad">
												<input type="text" class="form-control" placeholder="" id="addendaIndicator">
											</div>
									</fieldset>

									<fieldset class="col-md-6 ">
										<label class="col-md-12 control-label nopad">Discretionary Data</label>
											<div class="col-md-12 nopad">
												<input type="text" class="form-control" placeholder="" id="discretionaryData">
											</div>
									</fieldset>

								</div>




							</form>
	</div>
    
  </div>

</div>

       <!-- <div class="panel-group" id="accordion">
        <div class="panel panel-default" id="payment-wraper">
            <div class="panel-heading">
                <h4 class="panel-title">
               
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" id="imageHeader">
                    <span class="fa fa-chevron-up"></span>
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">

                     <div class="col-md-12 cheque-wraper imageviewer">
					 <div class="hover-btns">
							<span >
								<a href="../download/external"  class="download-icon"> <i class="fa fa-download"></i></a>
							</span>
							<span>
								<a href="javascript:void(0);" class="flip-icon fliptrigger"><i class="fa fa-rotate-left" ></i></a>
							</span>
							</div>
						<div class="cheque-image">
						
							<div class="flipmain-wraper">
							
							
							<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
							<div class="flipper">
							<div class="front">
								
								
								<img id="frontImage" src=""  data-zoom-image="" class="img-responsive magnifier" alt=""/>
								
							</div>
							<div class="back">
							
							<img id="rearImage" src=""  data-zoom-image="" class="img-responsive magnifier"  alt=""/>
							
							</div>
							</div>
							</div>
							
						</div>
						</div>
								 
								 
								
					</div>

                </div>
            </div>
        </div>
   
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Item Information
                    <span class="fa fa-chevron-down"></span>
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    	<form action="">

								<div class="row">
								<fieldset class="col-md-6">
									<label class="col-md-12 control-label nopad">Record Type Code</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" id="recordTypeCode" placeholder="">
										</div>
								</fieldset>

								<fieldset class="col-md-6 ">
									<label class="col-md-12 control-label nopad">Transaction code</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="transactionCode">
										</div>
								</fieldset>



							</div>
							<div class="row">
								<fieldset class="col-md-6">
									<label class="col-md-12 control-label nopad">Receiving DFI Identification</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="receivingDFI">
										</div>
								</fieldset>
								<fieldset class="col-md-6">
									<label class="col-md-12 control-label nopad">DFI Account No</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="dfiAccountNo">
										</div>
								</fieldset>
							</div>
							<div class="row">

								<fieldset class="col-md-6 ">
									<label class="col-md-12 control-label nopad">Individual Identification No</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="individualId">
										</div>
								</fieldset>
								<fieldset class="col-md-6">
									<label class="col-md-12 control-label nopad">Individual Name</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="individualName">
										</div>
								</fieldset>
							</div>
							<div class="row">


								<fieldset class="col-md-6 ">
									<label class="col-md-12 control-label nopad">Check Digit</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="checkDigit">
										</div>
								</fieldset>
								<fieldset class="col-md-6">
									<label class="col-md-12 control-label nopad">Amount</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" class="amount" id="amount">
										</div>
								</fieldset>
							</div>
						</form>
							<form action="">

							<div class="row">
									<fieldset class="col-md-6 ">
										<label class="col-md-12 control-label nopad">Trace Number</label>
											<div class="col-md-12 nopad">
												<input type="text" class="form-control" placeholder="" id="traceNumber">
											</div>
									</fieldset>

									<fieldset class="col-md-6 ">
										<label class="col-md-12 control-label nopad">Format Code</label>
											<div class="col-md-12 nopad">
												<input type="text" class="form-control" placeholder="" id="discretionaryData">
											</div>
									</fieldset>

								</div>


							<div class="row">
									<fieldset class="col-md-6 ">
										<label class="col-md-12 control-label nopad">Addenda Record Indicator</label>
											<div class="col-md-12 nopad">
												<input type="text" class="form-control" placeholder="" id="addendaIndicator">
											</div>
									</fieldset>

									<fieldset class="col-md-6 ">
										<label class="col-md-12 control-label nopad">Discretionary Data</label>
											<div class="col-md-12 nopad">
												<input type="text" class="form-control" placeholder="" id="discretionaryData">
											</div>
									</fieldset>

								</div>




							</form>

                </div>
            </div>
        </div>
    
    </div>-->


                   </div>
                   </div>




               <!--       <div class="col-md-3 nopad-left tree-list">
						<ul class="cd-accordion-menu animated">
							<li class="has-children">
								<input type="checkbox" name ="group-1" id="group-1" checked>
								<label for="group-1" class="active">Batch <span id ="batchNumber"></span></label>
								<ul id="first-level-list">
									<!-- <li class="first-level-list" >
										<input type="checkbox" name ="sub-group-1" id="sub-group-1" checked>
										<label for="sub-group-1">Item 1</label>

										<ul class="second-level-list" id="">

											<li><a href="#0">Scan Doc</a></li>
										</ul>
									</li>
								</ul>
							</li>
						</ul>
						<div class="batchform-wraper">
						<div class="medium-title">Batch - Info</div>
						<div class="form-group">
						<div class="row">
							<div class="col-md-6">Customer Name <span class="pull-right">:</span></div>
							<div class="col-md-6"><span id="customerName"></span></div>
						</div>
						</div>
						<div class="form-group">
						<div class="row">
							<div class="col-md-6">Transactions <span class="pull-right">:</span></div>
							<div class="col-md-6"><span id="transactionSize"></span></div>
						</div>
						</div>

						<div class="form-group">
						<div class="row">
							<div class="col-md-6">Scan Doc <span class="pull-right">:</span></div>
							<div class="col-md-6"><span id="scanDocSize"></span></div>
						</div>

						</div>
						<div class="form-group">
						<div class="row">
							<div class="col-md-6">Total Credit <span class="pull-right">:</span></div>
							<div class="col-md-6"><span id="totalCredit"></span></div>
						</div>

						</div>
						<div class="form-group">
						<div class="row">
							<div class="col-md-6">Total Debit <span class="pull-right">:</span></div>
							<div class="col-md-6"><span id="totalDebit"></span></div>
						</div>

						</div>

						</div>
					</div>
					<div class="col-md-9  tree-list-content">
						<div class="col-sm-12 nopad tree-border-bottom">
							 <div class="col-sm-6 nopad text-right pull-right" id="backButton">

							</div>
							<div class="col-sm-6 nopad">
								<ul>
									<li>
										<a href="">Batch</a>
									</li>
									<li>
										<span> > </span>
									</li>
									<li>
										<a href="">Transactions </a>
									</li>

								</ul>
							</div>
						</div>
						<div class="col-md-12 paymentremit-wraper">
						<div class="row">
							<div class="col-md-6 lightborder-right">
								<div class="medium-title">Payment</div>
								<div class="col-md-12 cheque-wraper">
								  <div class="col-md-12 nopad form-group">
								  <div class="row">
								     <div class="col-md-6">
										<div><input type ="text"  class="white-formfield text-left" id="p_name"></div>

								   	  </div>
								  	  <div class="col-md-6 text-right">
									  <div><input type ="text"  class="white-formfield text-right"></div>
								     </div>
								  </div>
								  </div>
								  <div class="col-md-12 nopad">
								  <div class="col-md-12 nopad form-group accname">
								     <p>PAY TO THE ORDER OF</p>
								    	<div class="row">
								    	<div class="col-md-7 nopad-right">
										<input type ="text" class="individualName form-control" id="payTo">
										</div>
								    	<div class="col-md-5">

										<div class="input-group">
											  <span class="input-group-addon">$</span>
											  <input type="text" class="form-control amount" id="p_amount">

										</div>
										</div>
										</div>
										</div>

								  </div>
								  <div class="col-md-12 form-group">
								      <div class="row">
									  <div class="col-md-10 nopad">
									  <input type ="text" id="amountInWords" class="amount-words">
									</div>
									<div class="col-md-2 nopad text-right">
									  <div class="amtword-caption">DOLLARS</div>
									  </div>
									  </div>
									  </div>
								  <div class="col-md-12 nopad">
								  <div class="row">
								     <div class="col-md-6">
										<div><input type ="text"  class="white-formfield text-left" id="memoNo"></div>
								   	     <h6>Memo</h6>

								   	  </div>
								  	  <div class="col-md-6 text-right">
									  <div><input type ="text"  class="white-formfield text-right" id="p_signature"></div>
								     </div>
								  </div>
								  <div class="row">
								     <div class="col-md-6">
										<div><input type ="text"  class="white-formfield text-left" id="p_receivingDfiAccount"></div>

								   	  </div>
								  	  <div class="col-md-6 text-right">
									  <div><input type ="text"  class="white-formfield text-right" id="p_dfiAccountNo"></div>
								     </div>
								  </div>
								  </div>
							  </div>

								<div class="col-md-12 form-file-process nopad">
							<form action="">

								<div class="row">
								<fieldset class="col-md-6">
									<label class="col-md-12 control-label nopad">Record Type Code</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" id="recordTypeCode" placeholder="">
										</div>
								</fieldset>

								<fieldset class="col-md-6 ">
									<label class="col-md-12 control-label nopad">Transaction code</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="transactionCode">
										</div>
								</fieldset>



							</div>
							<div class="row">
								<fieldset class="col-md-6">
									<label class="col-md-12 control-label nopad">Receiving DFI Identification</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="receivingDFI">
										</div>
								</fieldset>
								<fieldset class="col-md-6">
									<label class="col-md-12 control-label nopad">DFI Account No</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="dfiAccountNo">
										</div>
								</fieldset>
							</div>
							<div class="row">

								<fieldset class="col-md-6 ">
									<label class="col-md-12 control-label nopad">Individual Identification No</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="individualId">
										</div>
								</fieldset>
								<fieldset class="col-md-6">
									<label class="col-md-12 control-label nopad">Individual Name</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="individualName">
										</div>
								</fieldset>
							</div>
							<div class="row">


								<fieldset class="col-md-6 ">
									<label class="col-md-12 control-label nopad">Check Digit</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" id="checkDigit">
										</div>
								</fieldset>
								<fieldset class="col-md-6">
									<label class="col-md-12 control-label nopad">Amount</label>
										<div class="col-md-12 nopad">
											<input type="text" class="form-control" placeholder="" class="amount" id="amount">
										</div>
								</fieldset>
							</div>






							</form>
						</div>
							</div>
							<div class="col-md-6 ">
							<div class="medium-title">Remittance</div>
							<div class="col-md-12 cheque-wraper">
								  <div class="col-md-12 nopad form-group">
								  <div class="row">
								      <div class="col-md-6 ">
									      <h6></h6>
									  	</div>
									  	<div class="col-md-6 text-right">
									      <h6>DATE : </h6>

									 	</div>
								  </div>
								  </div>
								  <div class="col-md-12 nopad">
								  <div class="col-md-12 nopad form-group accname">
								     <p>ACCT #</p>
								    	<div class="row">
								    	<div class="col-md-7 nopad-right">
										<input type ="text" class="individualName form-control" id="r_name">
										</div>
								    	<div class="col-md-5">

										<div class="input-group">
											  <span class="input-group-addon">$</span>
											  <input type="text" class="form-control amount" id="r_amount">

											</div>
										</div>
										</div>
										</div>

								  </div>
								  <div class="col-md-12 form-group">
								      <div class="row">
									  <div class="col-md-10 nopad">
									  <input type ="text" class="amount-words" id="r_accountNo">
									</div>
								</div>
									  </div>
								   <div class="col-md-12 nopad">
								  <div class="row">
								     <div class="col-md-6">
										<div><input type ="text"  class="white-formfield text-left" id="traceNo"></div>
								   	     <h6>Trace Number</h6>

								   	  </div>
								  	  <div class="col-md-6 text-right">
									  <div><input type ="text"  class="white-formfield text-right"></div>
								         <h6></h6>
								   		</div>
								  </div>
								  </div>
							  </div>
							<div class="col-md-12 form-file-process">
							<form action="">

							<div class="row">
									<fieldset class="col-md-6 ">
										<label class="col-md-12 control-label nopad">Trace Number</label>
											<div class="col-md-12 nopad">
												<input type="text" class="form-control" placeholder="" id="traceNumber">
											</div>
									</fieldset>

									<fieldset class="col-md-6 ">
										<label class="col-md-12 control-label nopad">Format Code</label>
											<div class="col-md-12 nopad">
												<input type="text" class="form-control" placeholder="" id="discretionaryData">
											</div>
									</fieldset>

								</div>


							<div class="row">
									<fieldset class="col-md-6 ">
										<label class="col-md-12 control-label nopad">Addenda Record Indicator</label>
											<div class="col-md-12 nopad">
												<input type="text" class="form-control" placeholder="" id="addendaIndicator">
											</div>
									</fieldset>

									<fieldset class="col-md-6 ">
										<label class="col-md-12 control-label nopad">Discretionary Data</label>
											<div class="col-md-12 nopad">
												<input type="text" class="form-control" placeholder="" id="discretionaryData">
											</div>
									</fieldset>

								</div>




							</form>
						</div>
						</div>

						</div>
						<div class="col-md-12 nopad">
							<div class="medium-title">Scan Reports</div>
							<div class="col-md-12  nopad content-para" id="scanDoc">
							<p></p>
							</div>
						</div>

					<!--  	<div class="col-md-3 col-md-offset-9 col-sm-3 col-sm-offset-9 col-xs-12 text-right nopad">
									<div class="form-actions noborder form-group">
										<button type="button" class="btn default">Cancel</button>
										<button type="button" class="btn blue">Update</button>
									</div>
						</div>-->


						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<script>
     var batchHeaderRecordId = <%=request.getParameter("batchHeaderRecordId")%>;
     var entryDetails=null;
     $(document).ready(function()
    	{

 		$.ajax({

 			method:'get',
 			url:'../file-process/getEntry/'+batchHeaderRecordId,
 			contentType:'application/json',
 			dataType:'JSON',
 			crossDomain:'true',
 			success:function(response){
 						entryDetails=response.bhrBean[0]
 					var companyName=response.bhrBean[0].customerName;
 					var batchNumber=response.bhrBean[0].batchNumber;
 					var paymentDate=response.bhrBean[0].companyDescriptiveDate;
					var bankName=response.bhrBean[0].fileHeaderRecordBean.bankName;
 					var fileName=response.bhrBean[0].fileHeaderRecordBean.achFileName;
 					var fileType=response.bhrBean[0].fileHeaderRecordBean.fileType;
 					var backButton=response.bhrBean[0].fileHeaderRecordBean.fileHeaderRecordId;
 				
 					//var transactionSize=response.bhrBean[0].entryDetailRecordBean.length;
					var transactionSize=response.bhrBean[0].transactionCount;
					var paymentCount=response.bhrBean[0].paymentCount;
					var remittanceCount= response.bhrBean[0].remittanceCount;
 					var scanDocSize=response.bhrBean[0].scandocCount;
 					var itemCount=response.bhrBean[0].itemCount;
 					


 					var totalCredit=response.bhrBean[0].batchAmount;
 					

 					if(response.bhrBean[0].fileHeaderRecordBean.bankLogo==''){
 						$("#paymentcheq-logo").attr('src',"..");
 					}else{
 						$("#paymentcheq-logo").attr('src',".."+response.bhrBean[0].fileHeaderRecordBean.bankLogo);
 					}
 					$("#bankName").append(bankName);
 					$("#customerName").append(companyName);
 					$("#fileType").append(fileType);
 					$("#batchNumber").append(batchNumber);
 					$("#batchNo").append(batchNumber);
 					$("#fileName").append(fileName);
 					$("#backButton").append('<a href="file-process-detail.jsp?fileHeaderRecordId='+backButton+'" class="back-btn"><i class="fa fa-reply"> </i>Back to file process</a>');
 					$("#transactionCount").append(transactionSize);
 					$("#paymentCount").append(paymentCount);
 					$("#remittanceCount").append(remittanceCount);
 					$("#scanCount").append(scanDocSize);
 					

 					$("#paymentDate").append(paymentDate);
					

 					var i=1;
 					for (var j = 0; j < response.bhrBean[0].entryDetailRecordBean.length; j++){

	 					  $("#first-level-list").append('<li class="items"><a href="javascript:;" onclick="getData('+j+')">Transaction '+i+'</a></li>');

	 					i++;

	 				}
 					var count=1;
 					for (var z = 0;z<response.bhrBean[0].entryDetailRecordBean.length;z++){
 						
 						var p=parseInt(entryDetails.entryDetailRecordBean[z].paymentCount);
 						var r=parseInt(entryDetails.entryDetailRecordBean[z].remittanceCount);
 						var s=parseInt(entryDetails.entryDetailRecordBean[z].scandocCount);
 						var rowSize = (p+r+s+1);

	 					
 						$("#transactionList").append('<tr class="commontr-trig transaction-single"><td rowspan="'+rowSize+'" colspan="0"><span class="dropdown hover-dropdown"><a href="#" class="dropdown-toggle"><span class="remittance-icon" >Transaction</span></a><ul class="dropdown-menu"><li><span>Payment '+entryDetails.entryDetailRecordBean[z].paymentCount+'</span></li><li><span>Remittance'+entryDetails.entryDetailRecordBean[z].remittanceCount+'</span></li><li><span>Scan Document'+entryDetails.entryDetailRecordBean[z].scandocCount+'</span></li></ul></span>'+(z+1)+'</td><td><span class="dropdown hover-dropdown"><a href="#" class="dropdown-toggle"><span class="remittance-icon" ></span></a><ul class="dropdown-menu"><li><span class="custom-label"></span><span></span></li><li><span class="custom-label">Type :</span><span>Debit</span></li><li><span class="custom-label">Amount :</span><span>$5000</span></li></ul></span></td><td ><span></span></td></tr>');
 					
 				
 							 
 							for (var y = 0; y < entryDetails.entryDetailRecordBean[z].cCDAddendaRecordBean.length; y++){
			 					//  $("#transactionList").append('<tr onclick="getPayment('+z+','+y+')" class="commontr-trig"><td>'+count+'</td><td><span class="dropdown hover-dropdown"><a href="#" class="dropdown-toggle"><span class="remittance-icon" ><img src="../resources/assets/admin/layout3/img/remittance-icon.png"/></span></a><ul class="dropdown-menu"><li><span class="custom-label">Name :</span><span>pravin</span></li><li><span class="custom-label">Type :</span><span>Debit</span></li><li><span class="custom-label">Amount :</span><span>$5000</span></li></ul></span></td><td ><span>'+entryDetails.entryDetailRecordBean[z].cCDAddendaRecordBean[y].amount+'</span></td></tr>	<tr class="commontr-trig" onclick="getRemittance('+z+','+y+')">	 <td>&nbsp;</td><td><span class="dropdown hover-dropdown"><a href="#" class="dropdown-toggle"><span class="remittance-icon" ><img src="../resources/assets/admin/layout3/img/payment-icon.png"/></span></a><ul class="dropdown-menu"><li><span class="custom-label">Name :</span><span>pravin</span></li><li><span class="custom-label">Type :</span><span>Debit</span></li><li><span class="custom-label">Amount :</span><span>$5000</span></li></ul></span></td><td >'+entryDetails.entryDetailRecordBean[z].cCDAddendaRecordBean[y].amount+'</td></tr>');
		
 								if(entryDetails.entryDetailRecordBean[z].cCDAddendaRecordBean[y].entryType=='REMT'){
		 	 						$("#transactionList").append('<tr class="commontr-trig" onclick="getRemittance('+z+','+y+',this)"><td class="text-center"><span class="remittance-icon"  data-toggle="tooltip" data-placement="top" title="Remittance"><img src="../resources/assets/admin/layout3/img/payment-icon.png"/></span></td><td >'+entryDetails.entryDetailRecordBean[z].cCDAddendaRecordBean[y].amount+'</td></tr>');
		 	 						
		 	 				}else if(entryDetails.entryDetailRecordBean[z].cCDAddendaRecordBean[y].entryType=='PAYM'){
 								$("#transactionList").append('<tr onclick="getPayment('+z+','+y+',this)" class="commontr-trig"><td class="text-center"><span class="remittance-icon" data-toggle="tooltip" data-placement="top" title="Payment"><img src="../resources/assets/admin/layout3/img/credit-card.png"/></span></td><td ><span>'+entryDetails.entryDetailRecordBean[z].cCDAddendaRecordBean[y].amount+'</span></td></tr>');
 			 					
	 						}else if(entryDetails.entryDetailRecordBean[z].cCDAddendaRecordBean[y].entryType=='SCAN'){
			 	 						$("#transactionList").append('<tr id="scan_doc_magnifier" onclick="getScanDoc('+z+','+y+',this)" class="commontr-trig"><td class="text-center"><span class="remittance-icon"  data-toggle="tooltip" data-placement="top" title="Scan Document"><img src="../resources/assets/admin/layout3/img/remittance-icon.png"/></span></td><td>&nbsp;</td></tr>');
			 	 					
			 	 				} 
		 					} 
 							 
 						 }
 					

	 				
					//$("#scanDocSize").append(scanDocSize);
				/*	$(".batchtrans-leftbar").mCustomScrollbar({
										 theme:"dark",
										 setHeight: "200px"
						});*/
 					
						
						re_initialize();
						$("#frontImage").attr('src',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[0].cCDAddendaRecordBean[0].fciPng);
						
						$("#frontImage").attr('data-zoom-image', "http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[0].cCDAddendaRecordBean[0].fciPng); 
						$("#downloadFront").attr('href',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[0].cCDAddendaRecordBean[0].fciName);
						$("#downloadRear").attr('href',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[0].cCDAddendaRecordBean[0].rciName);
						$("#rearImage").attr('src',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[0].cCDAddendaRecordBean[0].rciPng);
						$("#rearImage").attr('data-zoom-image',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[0].cCDAddendaRecordBean[0].rciPng);
						$("#imageHeader").text("Payment");
					
					

 					 	$("#totalCredit").append(totalCredit);
				  

	 				

 					
 				//	for (var j = 0; j < entryDetails.entryDetailRecordBean.length; j++){

 						//$("#traceNumber").val(entryDetails.entryDetailRecordBean[0].traceNumber);
 						//$("#dv_bind_data").append(entryDetails.entryDetailRecordBean[j].traceNumber+'<br/>');
	 			//
	 				//}





			},

		 error:function(response,statusTxt,error){

		
		 }

 		});




    	});

     function getBatch(){
    	
			$("#frontImage").attr('src',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.batchPNG);
			$("#downloadFront").attr('href',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.frontImage);
			$("#imageHeader").text("Batch Header");
			$("#downloadTiff").attr("href",entryDetails.frontImage);

		  $('a[href="#firsttab"]').tab('show');
	  }

	  function getPayment(entryId,ccdId,thisval){
		  var k=entryId;
  	      var c=ccdId;
  	     
    	     $("#frontImage").attr('src',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[k].cCDAddendaRecordBean[c].fciPng);
			 $("#frontImage").attr('data-zoom-image',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[k].cCDAddendaRecordBean[c].fciPng);
    	     $("#rearImage").attr('src',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[k].cCDAddendaRecordBean[c].rciPng);
			 $("#rearImage").attr('data-zoom-image',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[k].cCDAddendaRecordBean[c].rciPng);
			 $("#downloadFront").attr('href',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[k].cCDAddendaRecordBean[c].fciName);
			$("#downloadRear").attr('href',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[k].cCDAddendaRecordBean[c].rciName);
				
 			$("#imageHeader").text("Payment");
  			paymentshow();
			$(".commontr-trig").removeClass('active');
			$(thisval).addClass('active');
	     	}



    	    //for(var a=0;a<response.bhrBean[0].entryDetailRecordBean[k].cCDAddendaRecordBean.length;a++){


    	    	//   $("#scanDocList").append('<tr><td>'+entryDetails.entryDetailRecordBean[k].cCDAddendaRecordBean[a].paymentRelatedInformation+'  '+entryDetails.entryDetailRecordBean[k].cCDAddendaRecordBean[a].addendaSequenceNumber+'   '+entryDetails.entryDetailRecordBean[k].cCDAddendaRecordBean[a].entryDetailSequenceNumber+'</td></tr>');
    	   // }


	     //}


	  function getRemittance(entryId,ccdId,thisval){
		  var k=entryId;
  	      var c=ccdId;

			$("#frontImage").attr('src',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[k].cCDAddendaRecordBean[c].fciPng);
			$("#frontImage").attr('data-zoom-image',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[k].cCDAddendaRecordBean[c].fciPng);
		    $("#rearImage").attr('src',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[k].cCDAddendaRecordBean[c].rciPng);
			$("#rearImage").attr('data-zoom-image',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[k].cCDAddendaRecordBean[c].rciPng);
			$("#downloadFront").attr('href',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[k].cCDAddendaRecordBean[c].fciName);
			$("#downloadRear").attr('href',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[k].cCDAddendaRecordBean[c].rciName);
			
			$("#imageHeader").text("Remittance");

			paymentshow();
			
			$(".commontr-trig").removeClass('active');
			$(thisval).addClass('active');
	     	}

	  function getScanDoc(entryId,ccdId,thisval){
		  
		  //alert(thisval);
		  
		  var k=entryId;
  	      var c=ccdId;	
  	    
  	    $("#frontImage").attr('src',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[k].cCDAddendaRecordBean[c].fciPng);
		$("#frontImage").attr('data-zoom-image',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[k].cCDAddendaRecordBean[c].fciPng);
  	    $("#rearImage").attr('src',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[k].cCDAddendaRecordBean[c].rciPng);
		$("#rearImage").attr('data-zoom-image',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[k].cCDAddendaRecordBean[c].rciPng);
		$("#downloadFront").attr('href',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[k].cCDAddendaRecordBean[c].fciName);
		$("#downloadRear").attr('href',"http://tcmuonline.com:8080/fileimages/BANKS/"+entryDetails.entryDetailRecordBean[k].cCDAddendaRecordBean[c].rciName);
		
		
		$("#imageHeader").text("Scan Document");
		//	$("#scanImage").attr('src',"data:image/png;base64,"+entryDetails.entryDetailRecordBean[k].imageurl);

		paymentshow();
		
		$(".commontr-trig").removeClass('active');
		$(thisval).addClass('active');
		}
		
		
		
		$("#frontImage").elevateZoom({
			zoomType : "lens",
			lensShape : "round",
			lensSize  : 200
		});
			
		
		 $(document).ready(function(){
		 // console.log("ready");
		});
 	$(".cheque-image").mCustomScrollbar({
				theme:"dark",
				 setHeight: "350px",
				axis:"y" // vertical and horizontal scrollbar
			});
		
		$(".fliptrigger").click(function(){
			$(".cheque-wraper").toggleClass("flipnow");
			$(".download-wraper").toggleClass("showrear");
			$(".cheque-image").mCustomScrollbar("scrollTo","top",{
				scrollInertia: 1200,
				scrollEasing:"easeInOut"
			});
			
		});
		$(".batchtrans-leftbar").mCustomScrollbar({
				theme:"dark",
				 setHeight: "450px",
				axis:"y" // vertical and horizontal scrollbar
			});


     function re_initialize(){


    	 $("[data-toggle='tooltip']").tooltip();
		
     }

     function getFormattedDate(date) {
    	  var year = date.getFullYear();

    	  var month = (1 + date.getMonth()).toString();
    	  month = month.length > 1 ? month : '0' + month;

    	  var day = date.getDate().toString();
    	  day = day.length > 1 ? day : '0' + day;

    	  return month + '/' + day + '/' + year;
    	}


	  function paymentshow(){
			$("#remittance-wraper").slideUp();
			$("#accordion .collapse").removeClass("in");
			$("#payment-wraper .collapse").addClass("in");
			$("#payment-wraper").slideDown();
			$('a[href="#firsttab"]').tab('show');
			$(".cheque-wraper").removeClass("flipnow");
			$(".cheque-image").mCustomScrollbar("scrollTo","top",{
				scrollInertia: 1200,
				scrollEasing:"easeInOut"
				
			});
			$(".zoom-icon").removeClass('in');
		$('.img-magnifier-glass').hide();
		
      }
	  function remitanceshow(){
			$("#payment-wraper").slideUp();
			$("#accordion .collapse").removeClass("in");
			$("#remittance-wraper .collapse").addClass("in");
			$("#remittance-wraper").slideDown();
			$('a[href="#firsttab"]').tab('show');
      }
	  function scandocshow(){
			$("#payment-wraper").slideUp();
			$("#remittance-wraper").slideUp();
			$("#accordion .collapse").removeClass("in");
			$("#scandoc-wraper .collapse").addClass("in");
			$('a[href="#firsttab"]').tab('show');
      }

	  
	 

	  
	  
	  
	  
	  
	 </script>


<script> 

function magnify(imgID, zoom) {
 	var img, glass, w, h, bw;
 	img = document.getElementById(imgID);
	img_src = img.getAttribute('src');
 	//alert(img_src);
 	/*create magnifier glass:*/
 	glass = document.createElement("DIV");
 	glass.setAttribute("class", "img-magnifier-glass");
 	/*insert magnifier glass:*/

		
		
 	/*execute a function when someone moves the magnifier glass over the image:*/
	
 	glass.addEventListener("mousemove", moveMagnifier);
 	img.addEventListener("mousemove",moveMagnifier);
	
	 

	
 	/*and also for touch screens:*/
 	glass.addEventListener("touchmove", moveMagnifier);
 	img.addEventListener("touchmove", moveMagnifier);

	 
		
 	function moveMagnifier(e) {
 	img.parentElement.insertBefore(glass, img);
 	/*set background properties for the magnifier glass:*/
 	glass.style.backgroundImage = "url('" + img.src + "')";
 	glass.style.backgroundRepeat = "no-repeat";
 	glass.style.backgroundSize = (img.width * zoom) + "px " + (img.height * zoom) + "px";
 	bw = 3;
 	w = glass.offsetWidth / 4;
 	h = glass.offsetHeight / 4;	


	
 		var pos, x, y;
 		/*prevent any other actions that may occur when moving over the image*/
 		//e.preventDefault();
 		/*get the cursor's x and y positions:*/
 		pos = getCursorPos(e);
 		x = pos.x;
 		y = pos.y;
 		/*prevent the magnifier glass from being positioned outside the image:*/
 		if (x > img.width - (w / zoom)) {
 			x = img.width - (w / zoom);
 		}
 		if (x < w / zoom) {
 			x = w / zoom;
 		}
 		if (y > img.height - (h / zoom)) {
 			y = img.height - (h / zoom);
 		}
 		if (y < h / zoom) {
 			y = h / zoom;
 		}
 		/*set the position of the magnifier glass:*/
 		glass.style.left = (x - w) + "px";
 		glass.style.top = (y - h) + "px";
 		/*display what the magnifier glass "sees":*/
 		glass.style.backgroundPosition = "-" + ((x * zoom) - w + bw) + "px -" + ((y * zoom) - h + bw) + "px"; 
 	}

 	function getCursorPos(e) {
 		var a, x = 0,
 			y = 0;
 		e = e || window.event;
 		/*get the x and y positions of the image:*/
 		a = img.getBoundingClientRect();
 
 		/*calculate the cursor's x and y coordinates, relative to the image:*/
 		x = e.pageX - a.left;
 		y = e.pageY - a.top;
 		/*consider any page scrolling:*/
 		x = x - window.pageXOffset;
 		y = y - window.pageYOffset;
 		return {
 			x: x,
 			y: y
 		};
 	}
 } 
 
	magnify('frontImage',3); 
	magnify('rearImage',3);
	
$('.zoom-icon').on('click',function(e){
	 e.preventDefault(); 
	 if($(this).hasClass('in')){
		 $(this).removeClass('in');
		$('.img-magnifier-glass').hide();
	 } else {
		 $(this).addClass('in'); 
		 $('.img-magnifier-glass').show();
	 }
});
</script>
<script> 
$('.batch-trans-detail-right .tab-content').on('mouseenter', function(e){
	 if($('.zoom-icon').hasClass('in')){
		$('.img-magnifier-glass').show();
	 }
}).on('mouseleave', function(e){
	$('.img-magnifier-glass').hide();
}); 
</script>
</body>
<!-- END BODY -->
</html>
