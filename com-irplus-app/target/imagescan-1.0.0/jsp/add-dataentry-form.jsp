<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<!-- <div class="page-title">
				<h1>Add Data Entry Form</h1>
			</div> -->
			<!-- END PAGE TITLE -->
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
		<!-- 	<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="index.jsp">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					 Add Data Entry Form
				</li>
			</ul> -->
			</div>
			</div>
			</div>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN PROFILE SIDEBAR -->
					<div class="profile-sidebar">
						<!-- PORTLET MAIN -->
						<div class="portlet light profile-sidebar-portlet">
							<!-- SIDEBAR USERPIC -->
						<div class="profile-userpic">
								<span class="userpic-inner">
									<img id="bnk-bnklogo" class="img-responsive" alt="" src="../resources/assets/admin/layout3/img/avatar9.jpg"/>
									<a class="edit-pic bank-img" data-toggle="modal" data-target="#imgupload">
										<i class="fa fa-edit"></i>
									</a>
								</span>	
							</div>
							<!-- END SIDEBAR USERPIC -->
							<!-- SIDEBAR USER TITLE -->
							<div class="profile-usertitle">
									<div id="customerName" class="profile-usertitle-name"></div>
							</div>
							<!-- END SIDEBAR USER TITLE -->
							<!-- SIDEBAR MENU -->
							<div class="profile-usermenu">
								<ul class="nav"></ul>
							</div>
							<!-- END MENU -->
						</div>
						<!-- END PORTLET MAIN -->
					</div>
					<!-- END BEGIN PROFILE SIDEBAR -->
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content boxwith-sidebar">
						<div class="row">
							<div class="col-md-12">
				
					
					<div class="portlet light col-md-12 col-sm-12 col-xs-12">
					<div class="pagestick-title">
										<span>Add Data Entry Form</span>
									</div>
						<div class="portlet-body">
							<form role="form" class="form-horizontal" name="submit_form" id="submit_form" method="POST">
							<div class="col-md-6 col-sm-6 col-xs-12">
														<div class="form-group">
															<label for="form_control_1">Form ID</label>
															<input type="text" class="form-control" id="form_id">
														</div>
													</div>
													<div class="col-md-6 col-sm-6 col-xs-12">
													<div class="form-group">
														<label for="form_control_1">Form Name</label>
														<input type="text" class="form-control" id="form_name">
													</div>
													</div>
													
													<div class="col-md-6 col-sm-6 col-xs-12">
														<div class="form-group">
															<label for="form_control_1">Batch Class</label>
															<input type="text" class="form-control" id="batch_class">
														</div>
													</div>
													<!-- <div class="col-md-6 col-sm-6 col-xs-12">
														<div class="form-group">
														<label for="form_control_1">Validation</label>
														<input type="text" class="form-control" id="form_control_1">
													</div>
													</div> -->
													<div class="col-md-6 col-sm-6 col-xs-12">
														<div class="form-group">
														<label for="form_control_1">Watch Folder</label>
														<input type="text" class="form-control" id="watch_folder">
														</div>
													</div>
													<!-- <div class="col-md-6 col-sm-6 col-xs-12">
														<div class="form-group">
														<label for="form_control_1">Validation</label>
														<input type="text" class="form-control" id="form_control_1">
													</div>
													</div> -->
													<div class="col-md-12 col-sm-12 col-xs-12">
														<div class="table-scrollable table-scrollable-borderless managebank-cont">
															<table class="table table-hover table-light table-bordered custstup-dataentry">
																<thead>
																	<tr class="uppercase">
																		<th width="20%" >Field Name</th>
																		<th width="20%">Field Type</th>
																		<th width="20%">Field Length</th>
																	<!-- 	<th width="20%">Validation</th> -->
																		<th width="20%"> Add/delete</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td width="20%">
																			<input type="text" class="form-control fieldName fieldName0" name="fieldName0" id="0" placeholder="Field Name"></td>
																		<td width="20%">
																			<select class="form-control select2 fieldtype fieldtype0" name="fieldtype0" id="0">
																				<option value="">select</option>
																				<option value="0">ACCT</option>
																				<option value="1">AMNT</option>
																				<option value="2">CHAR</option>
																				<option value="3">DATE</option>
																				<option value="4">NUMS</option>
																			</select>
 																		</td>
																		<td width="20%">
																			<input type="text" class="form-control fieldlength fieldlength0" name="fieldlength0" id="0"  placeholder="Field Length" />
																		</td>
																		<!-- <td width="20%">
																			<input type="checkbox"  class="icheck" data-checkbox="icheckbox_square-grey">
																		</td> -->
																		<td width="20%">
																			<button type="button" class="btn btn-xs green add-dataentry-filed"><i class="fa fa-plus"></i> Add Row</button>
																		</td>
																	</tr>
																</tbody>
															</table>
														</div>
														<div class="col-sm-12 col-md-12 col-xs-12 text-right">
											<div class="form-actions noborder nopad">
												<!-- <button type="button" class="btn default">Cancel</button>
												<input type="submit" class="btn blue" value="Save" /> -->
												
												 <button type="button"  onclick="cancelGroup()" class="btn default" >Cancel</button>
																<a href="javascript:submitUpdateCustDataForm()" class="btn blue button-submit" id="dv_update">
															Update
															</a>
															<a href="javascript:submitCustDataForm()" class="btn blue button-submit button-save" id="dv_save">
															Create 
															</a>
											</div>
											</div>
													</div>	
													</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->

<script type="text/javascript">


	var siteId=<%=session.getAttribute("siteId")%>;
	var userId=<%=session.getAttribute("userid")%>;
	var customerId =<%=request.getParameter("customerId")%>;
	$(document).ready(function() {  



		$("#dv_update").hide();
		$("#dv_save").show();
		
		
/* 		$.ajax({
			
			method:'get',
			
			
			url:'../customer/fieldtype/'+userId,
			contentType:'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success:function(response)
{
			
 				var htmlbankid="";
 				var htmlbankname= "<option value='' selected disabled>Select</option>";
 		
 				var branch =response.dashboardResponse.bankInfo.bankbranchinfo;
 				
 			if(response.dashboardResponse.bankInfo.length=='')	{
 				var	htmlbankname ="<OPTION>Select Bank</OPTION>"; 

					$(".groupbranchname0").html(htmlbankname);
 				
 			}
 			if(response.dashboardResponse.bankInfo.length!='')	{
 				for(var i=0;i<response.dashboardResponse.bankInfo.length;i++){
						
				 		htmlbankname +="<OPTION VALUE="+response.dashboardResponse.bankInfo[i].bankId+" >"+response.dashboardResponse.bankInfo[i].bankName+"</OPTION>"; 

 					$(".groupbranchname0").html(htmlbankname);	
 			
 				}
 			}
 		
	},
    error: function (e) {
        
        console.log(e.message);
     /*    alert("failed to get bank"); */
    
		
	});

		function submitCustDataForm()
		{
			
			
			var count=0;
			var numItems = $('.custstup-dataentry tbody tr').size();

			var url_data = ($("#submit_form").serialize());
			
			var result = queryStringToJSON(url_data);
			var mappingInfo="";
			var Editcustdetails = [];
		
		 
	var newcount=0;
	var countval=0;


	

				$(".fieldName").each(function()
						
						{
									  var k  = $(this).attr("id");
	
						           Editcustdetails[count]= 
										{
						        		   fieldName:result["fieldName"+k],
						        		   fieldtypid:result["fieldtype"+k],
						        		   fieldLength:result["fieldlength"+k],
										   userId:userId,
											isactive:'1'
												
										} 
					
					
									 mappingInfo={
						        		   customerid:customerId,
						        		   formCode: $("#form_id").val(),
						        		   formName: $("#form_name").val(),
						        		   batchClassDataentry : $("#batch_class").val(),
						        		   watchFolderDataentry: $("#watch_folder").val(),
						
											
						        		   dataEntryFormValidationDTO:Editcustdetails,
										 userid:userId,
										isactive:'1'
								}
						           
								count++;
						           
						        	   
						});
				
			alert(JSON.stringify(mappingInfo))
	 $.ajax({
				method:'post',
				url:'../customer/dataentry',
				data:JSON.stringify(mappingInfo),
				contentType:'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success: function(response) 
				{
					swal({title:"Done",text:"DataEntry Added Successfully",type:"success"},function(){window.location="manage-bank.jsp";});
				},
				
			 error:function(response,statusTxt,error){
			 
			/* 	alert("Failed to add record :"+response.responseJSON.errMsgs); */
			 }
		}); 
	}


	
	


		 var queryStringToJSON = function (url) {
			    if (url === '')
			        return '';
			    var pairs = (url || location.search).slice(1).split('&');
			    var result = {};
			    for (var idx in pairs) {
			        var pair = pairs[idx].split('=');
			        if (!!pair[0])
			            result[pair[0].toLowerCase()] = decodeURIComponent(pair[1] || '');
			    }
			    return result;
			}
	



</script>
		
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />


</body>
<!-- END BODY -->
</html>