<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>

<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
		
	
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	
		<div class="page-head">
		<div class="container">
			<!-- END PAGE TITLE -->
			<div class="col-md-6 col-sm-6 col-xs-12 text-right ">
				<div class="fixedbtn-container">
					<a href="javascript:void(0);" class="btn serach m-t-15 search-trigger">
					<i class="fa fa-search"></i> search
				</a>
				
				<a href="create-group.jsp" class="btn orange m-t-15"><i class="fa fa-plus"></i> Add Bank</a>
				
				
			</div>
			</div>
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			
			<!-- BEGIN PAGE BREADCRUMB -->
			<!--<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				
				<li class="active">
					Manage Customer Group
				</li>
			</ul>
			</div>
				<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<a href="create-group.jsp" class="btn blue">
								<i class="fa fa-plus"></i>Add Group</a>
				</div>
			</div>
			</div>-->
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
				<div class="col-md-12 nopad searchmain-wraper" id="search-wraper">
					<div class="portlet light">
						<span class="searchclose"> &times; </span>
						<form role="form">


									<div class="row">
								
<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">Group Name</label>
										<input type="text" class="form-control" id="filter-gname" placeholder="Group Name" List="grplist"  autocomplete="off">
									<datalist id="grplist"></datalist>
									</div>
									</div>
	
	<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">Bank Name</label>
										<input type="text" class="form-control" id="filter-bname" placeholder="Bank Name" List="list"  autocomplete="off">
									<datalist id="list"></datalist>
									</div>
									</div>
									
									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">Customer Id</label>
										<input type="text" class="form-control" id="filter-custId" placeholder="Customer Id"  List="customerCode" autocomplete="off">
									<datalist id="customerCode"></datalist>
									</div>
									</div>
									
									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">Customer Name</label>
										<input type="text" class="form-control" id="filter-cname" placeholder="Customer Name" List="customerlist" autocomplete="off">
										<datalist id="customerlist"></datalist>
									</div>
									</div>


									<div class="col-md-12 col-sm-12 col-xs-12 text-right">
									<label for="form_control_1">&nbsp;</label>
									<div class="form-md-line-input pb-15">
									<button type="button" class="btn blue" onClick="loadBanks('filter')">SEARCH</button>

									</div>
									</div>
							</div>
							</form>
							 </div>
							</div>
					
					<div class="portlet light col-md-12 col-sm-12 col-xs-12">
						<div class="pagestick-title">
								<span>Manage Customer Group</span>
							</div>
						<div class="portlet-body">
							<div class="table-scrollable table-scrollable-borderless managebank-cont">
				
							<div class="table-scrollable table-scrollable-borderless">
								<table  class="table table-striped" id="manage-customergrouping-id">
								<col width="15%"></col>
								<col width="10%"></col>

								<col width="10%"></col>
								<col width="10%"></col>
								<col width="10%"></col>
								<thead>
								<tr class="uppercase">
									<th>
										 GROUP NAME 
									</th>
									<th>
										 COMPANIES MAPPED
									</th>
									<th>
										 CREATED ON
									</th>
									<th>
										 CREATED TIME
									</th>
									<th>
										 Options
									</th>
									
								</tr>
								</thead>
						
								</table>
							</div>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
 
<script>
var siteId=<%=session.getAttribute("siteId")%>;
var userId=<%=session.getAttribute("userid")%>; 
$(document).ready(function(){
	loadBanks('list');
	
});


function loadBanks(operation)
{
	//var url ='../bank/list/'+siteId;
//	var method= 'get';
	//var data={};
	var url ='';
	var method = '';
	var data='';

	 if(operation=='list')
	{
		url="../custmrGrp/customer/grouping/showAll";
		data={};
		method= 'get';
	}
	if(operation=='filter')
	{
		url="../custmrGrp/group/filter";
		method =  'post';
		data =
		{
			siteId:siteId,
			customerGroupName: $('#filter-gname').val(),
			bankName: $('#filter-bname').val(),
			companyName: $('#filter-cname').val(),
			bankCustomercode: $('#filter-custId').val(),
		/* 	businessProcId:$('#busiprocessdd').val(), */
				userId:userId
			
		}
		
	}
	
	$('#manage-customergrouping-id').DataTable( {searching: false,lengthChange:false,pageLength:50,

	ajax : ({
				method: method,
				url: url,
				dataSrc: "custmrGrpngMngmntInfoList",
				data: data,
				crossDomain:'true',


			 error:function(response,statusTxt,error){


			 }
			}),

	"processing": true,
	"destroy": true,
	"ordering":true,
	"columns": [
		{ "data": "customerGroupName" },
		{ "data": "totalMappedBranch" },
	
		
		//{ "data": "createddate" },
	  {
		  data: null, render: function ( data, type, row )
						{
					
						if(data.createddate!=''){
							var src = data.createddate;
		    var d = new Date(src),
		        month = '' + (d.getMonth() + 1),
		        day = '' + d.getDate(),
		        year = d.getFullYear();
		    if (month.length < 2) month = '0' + month;
		    if (day.length < 2) day = '0' + day;
		     var groupdate=[month, day, year].join('-');
		     return groupdate;
						}
				}
					},
					
					{ "data":"createdtime"},				
					{ 
						data: null, render: function ( data, type, row ) 
						{
					 	
							
						
								/*  var password = 'password';
	  							var result = CryptoJS.AES.encrypt(strVal, password); */
	  							
	  						
	  						 
/* 							return  '<a href="view-grouping.jsp?customerGrpId='+result+'" class="btn btn-xs green-meadow" data-toggle="tooltip" data-placement="right" title="view!"><i class="fa fa-eye"></i> </a><a href="create-group.jsp?customerGrpId='+result+'" class="btn btn-xs blue" data-toggle="tooltip" data-placement="right" title="Edit!"><i class="fa fa-edit"></i></a><a  onclick="deleteGroup('+data.customerGrpId+',2);" class="delete-btn btn btn-danger btn-xs" data-toggle="tooltip" data-placement="right" title="Delete!"><i class="fa fa-trash"></i></a>'	
 */							
 
 return  '<form action="view-grouping.jsp" method="post"><input type="hidden" name="customerGrpId" value="'+data.customerGrpId+'"/><button type="submit" class="btn btn-xs green-meadow"><i class="fa fa-eye"></i></button></form><form action="create-group.jsp" method="post"><input type="hidden" name="customerGrpId" value="'+data.customerGrpId+'" /><button type="submit" class="btn btn-xs blue"><i class="fa fa-edit"></i></button></form><a  onclick="deleteGroup('+data.customerGrpId+',2);" class="delete-btn btn btn-danger btn-xs" data-toggle="tooltip" data-placement="right" title="Delete!"><i class="fa fa-trash"></i></a>'		
				}
						
					},            	
	
	],

// "initComplete": function (settings, json) {
    // $('.make-switch').bootstrapSwitch(
	// {
		 // size: 'mini'
	// });
    // },
   "fnDrawCallback": function( settings )
		{
    
	  $('.make-switch').bootstrapSwitch(
		{
			 size: 'mini'
		});
		},
	responsive: true
} ).fnDraw();
	
}
	
	
	
	
	


function deleteGroup(customerGrpId,isactive)
{
	 swal({
	title: "Want to delete?",
   text: "",
   type: "warning",
   showCancelButton: true,
   confirmButtonColor: "#DD6B55",
   confirmButtonText: "Yes, delete it!",
   closeOnConfirm: false,
	closeOnCancel: true
	},
		function () {
		var delcustdetails = [];
		
		 delcustdetails[0]=
				{
					
				 customerGrpId:customerGrpId,
					isactive:isactive
						
				}

		
		var groupInfo = {
				
				customerGrpId:customerGrpId,
				isactive:isactive,
				customergroupingDetails:delcustdetails
		}
	
	
		
			$.ajax({
				method: 'post',
				url: '../custmrGrp/CustGrp/delete',
				data: JSON.stringify(groupInfo),
				contentType: 'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success: function (response) {
					 swal({title: "Done",text: "Deleted Successfully",type: "success"},function(){window.location = "manage-custgroup.jsp";});
						
				},

			 error:function(response,statusTxt,error){
			 
				/* alert("Failed to add record :"+response.responseJSON); */
			 }
			});
	}
)}



$(function() {
	
	 $("#filter-bname").autocomplete({     

	    source : function(request, response) {
	    
	      $.ajax({
	           url : '../autocomplete/filter/bank',
	           type : 'get',
	           data : {
	        	   userid:userId,
	                  term : request.term
	           },
	           dataType : "json",
	           success : function(data) {
	        	 
	        	   var banklist="";
	        	   for(i=0;i<data.list.length;i++){
	        		   
	        		   banklist += "<option value='"+data.list[i].bankName +"'></option>";

	       
	        	   }
	        	   $("#list").html(banklist);
	          
	        	  
	           }
	    });
	 }
	});  
	});

	

$(function() {
		 $("#filter-cname").autocomplete({     

		    source : function(request, response) {
		    	
		      $.ajax({
		           url : '../autocomplete/filter/customer',
		           type : 'get',
		           data : {
		        	   userid:userId,
		                  term : request.term
		           },
		           dataType : "json",
		           success : function(data) {
		        	   
		        	   var customerlist="";
		        	   for(i=0;i<data.customerBean.length;i++){
		        		 
		        		   customerlist += "<option value='"+data.customerBean[i].companyName +"'></option>";

		        	   }
		        	   $("#customerlist").html(customerlist);
		          
		        	  
		           }
		    });
		 }
		}); 
		});
$(function() {
	 $("#filter-custId").autocomplete({     

	    source : function(request, response) {
	    	 autoFocus:true
	      $.ajax({
	    	     url : '../autocomplete/filter/customerCode',
	           type : 'get',
	           data : {
	        	   userid:userId,
	                  term : request.term
	           },
	           dataType : "json",
	           success : function(data) {
	        	   
	        	   var customercode="";
	        	   for(i=0;i<data.customerBean.length;i++){
	        		 
	        		   customercode += "<option value='"+data.customerBean[i].bankCustomerCode +"'></option>";

	        	   }
	        	   $("#customerCode").html(customercode);
	          
	        	  
	           }
	    });
	 }
	}); 
	}); 
$(function() {
	 $("#filter-gname").autocomplete({     

	    source : function(request, response) {
	    	 autoFocus:true
	      $.ajax({
	    	     url : '../custmrGrp/autocomplete/filter/grpname',
	           type : 'get',
	           data : {
	        	   userid:userId,
	                  term : request.term
	           },
	           dataType : "json",
	           success : function(data) {
	        	   
	        	   var grpname="";
	        	   for(i=0;i<data.list.length;i++){
	        		 
	        		  grpname += "<option value='"+data.list[i].groupname +"'></option>";

	        	   }
	        	   $("#grplist").html(grpname);
	          
	        	  
	           }
	    });
	 }
	}); 
	}); 
</script>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>