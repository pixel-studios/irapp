<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container-fluid">
			<div class="col-md-6 col-sm-6 col-xs-12 text-right">
			<div class="fixedbtn-container">
				<a href="javascript:void(0);" class="btn serach m-t-15 search-trigger">
					<i class="fa fa-search"></i> search
				</a>
			
			</div>
			</div>
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container-fluid">
			
			<!-- BEGIN PAGE BREADCRUMB -->
			<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<a href="#">Home</a><i class="fa fa-circle"></i>
					</li>
					
					<li class="active">
						 Manage Bank Locations
					</li>
				</ul>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 text-right">
				<a href="manage-bank.jsp" class="btn blue"><i class="fa fa-reply"></i> Back</a>
			</div>
			</div>	
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN PROFILE SIDEBAR -->
					<div class="profile-sidebar" style="width: 250px;">
						<!-- PORTLET MAIN -->
						<div class="portlet light profile-sidebar-portlet">
							<!-- SIDEBAR USERPIC -->
							<div class="profile-userpic">
								<img src="../resources/assets/admin/pages/img/banks/bankofamerica.jpg" class="img-responsive" alt="">
							</div>
							<!-- END SIDEBAR USERPIC -->
							<!-- SIDEBAR USER TITLE -->
							<div class="profile-usertitle">
								<div class="profile-usertitle-name">
									 Bank of America
								</div>
								<div class="profile-usertitle-job">
									 North Carolina, United States
								</div>
							</div>
							<!-- END SIDEBAR USER TITLE -->
							<!-- SIDEBAR MENU -->
							<div class="profile-usermenu">
								<ul class="nav">
									<!--<li class="active">
										<a href="extra_profile.html">
										<i class="icon-home"></i>
										Overview </a>
									</li>
									<li>
										<a href="extra_profile_account.html">
										<i class="icon-settings"></i>
										Account Settings </a>
									</li>
									<li>
										<a href="page_todo.html" target="_blank">
										<i class="icon-check"></i>
										Tasks </a>
									</li>
									<li>
										<a href="extra_profile_help.html">
										<i class="icon-info"></i>
										Help </a>
									</li>-->
								</ul>
							</div>
							<!-- END MENU -->
						</div>
						<!-- END PORTLET MAIN -->
						
					</div>
					<!-- END BEGIN PROFILE SIDEBAR -->
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
				
					
					<div class="portlet light col-md-12 col-sm-12 col-xs-12">
					
					
						<div class="portlet-body">
							<div class="col-md-12 col-sm-12 col-xs-12 nopad">
							<div class="table-scrollable table-scrollable-borderless">
								<table class="table table-hover table-light table-bordered">
								<thead>
								<tr class="uppercase">
									<th >
										 Location
									</th>
									<th>
										 Location id
									</th>
									<th>
										 Primary Contact
									</th>
									<th>
										 Email
									</th>
									<th>
										 Phone No
									</th>
									<th>
										 City
									</th>
									<th>
										 State
									</th>
									
									<th>
										Options
									</th>
									
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>
										Broadway, New York
									</td>
									<td>
										 IRPBID001
									</td>
									
									<td>
										 Steve
									</td>
									<td>
										info@bof.com
									</td>
									<td>
										+91 215468877
									</td>
									<td>
										New York
									</td>
									<td>
										New York
									</td>
									<td>
										<button type="button" class="btn grey btn-xs">Edit</button>
									</td>
								</tr>
								<tr>
									<td>
										Broadway, New York
									</td>
									<td>
										 IRPBID001
									</td>
									
									<td>
										 Steve
									</td>
									<td>
										info@bof.com
									</td>
									<td>
										+91 215468877
									</td>
									<td>
										New York
									</td>
									<td>
										New York
									</td>
									<td>
										<button type="button" class="btn grey btn-xs">Edit</button>
									</td>
								</tr>
								<tr>
									<td>
										Broadway, New York
									</td>
									<td>
										 IRPBID001
									</td>
									
									<td>
										 Steve
									</td>
									<td>
										info@bof.com
									</td>
									<td>
										+91 215468877
									</td>
									<td>
										New York
									</td>
									<td>
										New York
									</td>
									<td>
										<button type="button" class="btn grey btn-xs">Edit</button>
									</td>
								</tr>
								
							
								
								
								</tbody>
								</table>
							</div>
							</div>
							
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="container">
		 Image Scan &copy; 2017 ALL RIGHTS RESERVED.
	</div>
</div>
<div class="scroll-to-top">
	<i class="icon-arrow-up"></i>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../resources/assets/global/plugins/respond.min.js"></script>
<script src="../resources/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="../resources/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../resources/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../resources/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../resources/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../resources/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../resources/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../resources/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../resources/assets/global/plugins/icheck/icheck.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script src="../resources/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../resources/assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="../resources/assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="../resources/assets/admin/pages/scripts/profile.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {       
   	// initiate layout and plugins
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	Demo.init(); // init demo features\
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>