<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>


<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
		<!-- 
			<div class="col-md-6 col-sm-6 col-xs-12 text-right ">
				<div class="fixedbtn-container">
				<a href="javascript:void(0);" class="btn serach m-t-15 search-trigger">
					<i class="fa fa-search"></i> search
				</a>
				
				</div>
			</div> -->


		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content customergrp-section">
		<div class="container">

			<!-- BEGIN PAGE BREADCRUMB -->
		
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<div class="profile-content">
					<div class="col-md-12 nopad searchmain-wraper" id="search-wraper">
					<div class="portlet light">
						<span class="searchclose"> &times; </span>
						<form role="form">


									<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">From date</label>
										<input type="text" class="form-control fromdate1" id="Fromdatepicker"  placeholder="From date">
									</div>
									</div>

								

									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">To date</label>
										<input type="text" class="form-control todate1" id="Todatepicker"   placeholder="To date">
									</div>
									</div>

											
									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">Customer Id</label>
										<input type="text" class="form-control" id="filter-custId" placeholder="Customer Id"  >
															
									</div>
									</div>
									
									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">Customer Name</label>
										<input type="text" class="form-control" id="filter-cname" placeholder="Customer Name" List="customerlist">
											<datalist id="customerlist"></datalist>
									</div>
									</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">Bank Name</label>
										<input type="text" class="form-control" id="filter-bname" placeholder="Bank Name" List="list">
								<datalist id="list"></datalist>
									</div>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12 text-right">
									
									<div class="form-md-line-input pb-15">
									<button type="button"  class="btn blue" onClick="loadBanks('filter')">SEARCH</button>

									</div>
									</div>
							</div>
							</form>
							 </div>
							</div>

						<div class="row">
							<div class="col-md-12 custreport-wraper">
								
							
							
					<div class="portlet light col-md-12 col-sm-12 col-xs-12">
					<div class="pagestick-title">
								<span>Customer Report</span>
							</div>
						<div class="portlet-body example" style="display:block">
							<div class="">
								<table id="example" class="display table text-center" >
								<col width="5%"></col>
								<col width="12%"></col>
								<col width="22%"></col>
								<col width="20%"></col>
								<col width="10%"></col>
								<col width="10%"></col>
								<col width="10%"></col>
								<col width="10%"></col>
								<col width="10%"></col>
								
								
									<thead>
										<tr>
										
										<th></th>
										<th>Customer Id</th>
										<th>Company Name</th>
											<th>Bank Name</th>
										<th colspan="2">Batches</th>
										<th>Transactions</th>
										<th>Items</th>
										<th>Amount</th>	
																		
									</tr>
									<tr>
										<th></th>
										<th></th>
											<th></th>
										<th></th>
										
										<th>Ach</th>
										<th>Lbx</th>
										
										<th></th>
										<th></th>
										<th></th>
									
									</tr>
								</thead>
								
							</table>
									</div>
					</div>
							
						<div class="portlet-body example1" style="display:none">
										<div class="">
								<table id="example1" class="display table text-center" >
							
								<col width="12%"></col>
								<col width="22%"></col>
								<col width="10%"></col>
								<col width="10%"></col>
								<col width="10%"></col>
								<col width="10%"></col>
								<col width="10%"></col>
								
								
									<thead>
										<tr>
										
										<th>Customer Id</th>
										<th>Customer Name</th>
										<th colspan="2">Batches</th>
										<th>Transactions</th>
										<th>Items</th>
										<th>Amount</th>	
																		
									</tr>
									<tr>
										
										<th></th>
										<th></th>
										
										<th>Ach</th>
										<th>Lbx</th>
										
										<th></th>
										<th></th>
										<th></th>
									
									</tr>
								</thead>
								
							</table>
									</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />

<!-- BEGIN JAVASCRIPTS -->


<script>
var siteId=<%=session.getAttribute("siteId")%>;

var userId=<%=session.getAttribute("userid")%>;

$(document).ready(function(){
	
	
	 $('<script/>',{type:'text/javascript', src:'../resources/assets/global/plugins/jquery.base64.js'}).appendTo('head');

		
		var url_string = window.location.href;
		var url = new URL(url_string);
		
		var BetweendateValue = url.searchParams.get("Betweendate");
		var custId = url.searchParams.get("custId");
		var bankId = url.searchParams.get("bankId");
	
					
		var BetweenDate = $.base64.decode(BetweendateValue);
		var custIdval = $.base64.decode(custId);
		var bankIdval = $.base64.decode(bankId);
	
var url ='';
	var method = '';
	var data='';
if(BetweendateValue!=null){
	
	url="../custmrGrp/datewise/customerwise",
	data =
	{
			currentdate:BetweenDate,
			customerid:custIdval,
			bankwiseId:bankIdval,
			userid:userId,
		
	}
	method= 'post';



$.fn.dataTable.ext.errMode = 'throw'
	var table = $('#example').DataTable( {searching: false,lengthChange:false,pageLength:50,
			ajax : ({
				method: method,
				url: url,
				dataSrc: "custmrGrpngMngmntInfoList",
				data: data,
				crossDomain:'true',
			   

			 error:function(response,statusTxt,error){


			 }
			}),

	"processing": true,
	"destroy": true,
	"ordering": false,
	 "columns": [
 	      {
	                "className":      'details-control',
	      
	                "orderable":      false,
	                "data":           null,
	              
	                "defaultContent": ''
	              
	            },  

	            { "data": "bankCustomerCode" },
	            { "data": "companyName" },
	            { "data": "BankName" },
	            { "data": "AchfileCount" },
	            { "data": "lockboxfileCount" },
	         
	           
	            { "data": "totaltransactions" },
	            { "data": "totalitems" },
	            { "data": "BankwiseAmount" ,
	            	
	            	 "className": 'dt-right',
	            }
	           
	           
	        ],
	        
	        createdRow: function (row, data, indice) {

	            $(row).find("td:eq(0)").attr('id', data.bankCustomerCode);
	        
	        },

		    "fnDrawCallback": function( settings )
			{
	  
		  $('.make-switch').bootstrapSwitch(
			{
				 size: 'mini'
			});
			},
			responsive: true
	    } );
			



$('#example tbody').on('click', 'td.details-control', function () {
	
	
		var custIdval = $.base64.decode(custId);
		var bankIdval = $.base64.decode(bankId);
		
		var BetweenDate = $.base64.decode(BetweendateValue);
   var tr = $(this).closest('tr');
	data =
		{
			bankwiseId:bankIdval,
			currentdate:BetweenDate,
			userid:userId,
			customerid:custIdval
		}
	
	//console.log(tr);
   var row = table.row( tr );
   if ( $(this).parent("tr").hasClass("shown") ) {
   //if ( row.child.isShown() ) {
		console.log("if-condition");
		row.child.hide(500);
       tr.removeClass('shown');
   }
   else {
                
       
        	$.ajax({
		method: 'post',
data: data,
		"url": '../custmrGrp/customerwise/report/getdatewise',
		dataSrc: "custmrGrpngMngmntInfoList",
		
		crossDomain:'true',
       	success: function (response) {
			
				if(response.custmrGrpngMngmntInfoList.length>0){
					var tr_str='';    				

				for(var i=0;i<response.custmrGrpngMngmntInfoList.length;i++)
				{
					
					   tr_str += "<tr>"+ "<td>" +response.custmrGrpngMngmntInfoList[i].highestdate+ "</td>"+"<td>" +response.custmrGrpngMngmntInfoList[i].AchfileCount+ "</td>" + "<td>"+response.custmrGrpngMngmntInfoList[i].lockboxfileCount+"</td>" + "<td>" +response.custmrGrpngMngmntInfoList[i].totaltransactions+ "</td>" + "<td>" +response.custmrGrpngMngmntInfoList[i].totalitems+ "</td>" + "<td style='text-align:right'>" +response.custmrGrpngMngmntInfoList[i].totalbatchamt+ "</td>" ;
					"</tr>";

					}
					
					var repTable = "<table class='display table text-center innertable'><thead><tr><th><span class='arrowtop'></span>Date</th><th>ACH</th><th>LBX</th><th>Transactions</th><th>Items</th><th>Amount</th></tr></thead><tr>"+ tr_str +"</tr></table>";
				
					console.log("else-condition");
					$("tr.shown").next("tr").slideUp();
					$("tr").removeClass("shown");
					row.child(repTable).show(1500);
					tr.addClass('shown');
				}

				
			}
        	});

   }
} );

}
} );




	
	



</script>


<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
