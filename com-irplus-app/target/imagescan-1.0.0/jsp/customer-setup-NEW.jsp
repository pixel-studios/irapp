<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Customer Creation</h1>
			</div>
			<!-- END PAGE TITLE -->
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			
			<!-- BEGIN PAGE BREADCRUMB -->
			<!--<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="index.jsp">Home</a><i class="fa fa-circle"></i>
				</li>
				
				<li class="active">
					 Customer Creation
				</li>
			</ul>
			</div>
			</div>
			</div>-->
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN PROFILE SIDEBAR -->
					<div class="profile-sidebar">
						<!-- PORTLET MAIN -->
						<div class="portlet light profile-sidebar-portlet">
							<!-- SIDEBAR USERPIC -->
						<div class="profile-userpic">
								<span class="userpic-inner">
									<img id="bnk-bnklogo" class="img-responsive" alt="" src="../resources/assets/admin/layout3/img/avatar9.jpg"/>
									<a class="edit-pic bank-img" data-toggle="modal" data-target="#imgupload">
										<i class="fa fa-edit"></i>
									</a>
								</span>	
							</div>
							<!-- END SIDEBAR USERPIC -->
							<!-- SIDEBAR USER TITLE -->
							<div class="profile-usertitle">
									<div id="customerName" class="profile-usertitle-name"></div>
							</div>
							<!-- END SIDEBAR USER TITLE -->
							<!-- SIDEBAR MENU -->
							<div class="profile-usermenu">
								<ul class="nav"></ul>
							</div>
							<!-- END MENU -->
						</div>
						<!-- END PORTLET MAIN -->
					</div>
					<!-- END BEGIN PROFILE SIDEBAR -->
					<!-- BEGIN PROFILE CONTENT -->
				<div class="profile-content boxwith-sidebar">
					<div class="row">
						<div class="col-md-12">
							<div class="portlet light col-md-12 col-sm-12 col-xs-12" id="form_wizard_1">
									<div class="portlet-body form">
								<form action="#" class="form-horizontal" name="submit_form" id="submit_form" method="POST">	
										<div class="form-wizard">
											<div class="form-body">
												<ul class="nav nav-pills nav-justified steps">
													<li>
														<a href="#tab1" data-toggle="tab" class="step">
														<span class="number">
														1 </span>
														<span class="desc">
														<i class="fa fa-check"></i> Customer Information </span>
														</a>
													</li>
													<li>
														<a href="#tab2" data-toggle="tab" class="step">
														<span class="number">
														2 </span>
														<span class="desc">
														<i class="fa fa-check"></i> Contacts </span>
														</a>
													</li>
													<li>
														<a href="#tab3" data-toggle="tab" class="step active">
														<span class="number">
														3 </span>
														<span class="desc">
														<i class="fa fa-check"></i> Functions </span>
														</a>
													</li>
													<li>
														<a href="#tab5" data-toggle="tab" class="step active">
														<span class="number"> 4 </span>
														<span class="desc">
														<i class="fa fa-check"></i> ACH Form Setup </span>
														</a>
													</li>
													<li>
														<a href="#tab6" data-toggle="tab" class="step active">
														<span class="number"> 5 </span>
														<span class="desc">
														<i class="fa fa-check"></i> Auto Generated Report </span>
														</a>
													</li>
												</ul>
												<div id="bar" class="progress progress-striped" role="progressbar">
														<div class="progress-bar progress-bar-success"></div>
												</div>
												<div class="tab-content">
													<div class="tab-pane active" id="tab1">
														<input type="hidden" class="form-control" id="bnk-bankLogo" name="bnk-bankLogo" />
															
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label for="form_control_1">Bank Name <sup class="red-req">*</sup></label>
																<select class="form-control select2" id="bnk-name" name="bnk-name" required>
																</select>
															</fieldset>
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label for="form_control_1">Bank Location <sup class="red-req">*</sup></label>
																<select class="form-control select2" id="bnk-loc" name="bnk-loc" required>
																</select>
															</fieldset>
															
															
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label for="form_control_1">Company Name <sup class="red-req">*</sup></label>
																<input type="text" class="form-control" id="comp-name" name="comp-name" required />

														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
																<label for="form_control_1">Company Identification No. (ACH) <sup class="red-req">*</sup></label>
																<input type="text" class="form-control" id="comp-id-no" name="comp-id-no" required />
														</fieldset>
															
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Bank Customer Id<sup class="red-req">*</sup></label>
															<input type="text" class="form-control" id="bank-cust-id" name="bank-cust-id" required />
														</fieldset>	
															<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">DDA Bank A/C # <sup class="red-req">*</sup></label>
															<input type="text" class="form-control" id="dda-bank" name="dda-bank" required /> 
														</fieldset>	
														<fieldset class="portlet-title col-md-12 col-sm-12 col-xs-12 ">
															<span class="caption-subject font-green-sharp bold uppercase">Primary contact</span>
														</fieldset>
														
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">First Name <sup class="red-req">*</sup></label>
															<input type="text" class="form-control" id="fName-id" name="fName-id" required>
														</fieldset>
														
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Last Name<sup class="red-req">*</sup></label>
															<input type="text" class="form-control" id="lastName-id" name="lastName-id">
														</fieldset>
														
														
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Contact No<sup class="red-req">*</sup></label>
																<input type="text" class="form-control" id="contact-id" required>
														</fieldset>
														
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Email ID<sup class="red-req">*</sup></label>
															<input type="email" class="form-control" id="email-id" required>
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Address Line 1 <sup class="red-req">*</sup></label>
															<textarea class="form-control" rows="3" id="address1-id" ></textarea>	
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Address Line 2</label>
															<textarea class="form-control" rows="3" id="address2-id" ></textarea>
														</fieldset>
															
														
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">City <sup class="red-req">*</sup></label>
															<input type="text" class="form-control" id="city-id" name="city-id">
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">state <sup class="red-req">*</sup></label>
															<input type="text" class="form-control" id="state-id" name="state-id">
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Zip code <sup class="red-req">*</sup></label>
															<input type="text" class="form-control" id="zipcode-id" name="zipcode-id">
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Website</label>
															<input type="text" class="form-control" id="website-id" name="website-id">
														</fieldset>
													</div>
													<div class="tab-pane" id="tab2">
											<div id ="contact_form" class="col-md-12 col-sm-12 col-xs-12 bankaccount-listcont">
															<div class="form-group text-right">
																<button type="button" class="btn blue add-row-bank"><i class="fa fa-plus"></i> Add row</button>
															</div>
															<div class="col-md-12 col-sm-12 col-xs-12 bankaccount-single">
																	<div class="col-md-12 col-sm-12 col-xs-12 nopad">
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																			<div class="form-group">
																				<label class="control-label">First Name</label>
																			<input type="text" class="form-control" name="contact_fname1" id="contact_fname1">
																			</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Last Name</label>
																			<input type="text" class="form-control" id="contact_lname1" name="contact_lname1">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																			<div class="form-group">
																				<label class="control-label">Contact Role</label>
																				<input type="text" class="form-control" name="contact_crole1" id="contact_crole1">
																			</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Phone No.</label>
																			<input type="text" class="form-control" name="contact_fphoneno1" id="contact_fphoneno1">
																		</div>
																		</fieldset>
																	</div>
																	<div class="col-md-12 col-sm-12 col-xs-12 nopad">
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Email</label>
																			<input type="email" class="form-control " name="contact_femail1" id="contact_femail1" >
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Address 1</label>
																			<input type="text" class="form-control" name="contact_faddr11" id="contact_faddr11">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Address 2</label>
																			<input type="text" class="form-control" name="contact_faddr21" id="contact_faddr21">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">City</label>
																			<input type="text" class="form-control" name="contact_fcity1" id="contact_fcity1">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">State</label>
																			<input type="text" class="form-control" name="contact_fstate1" id="contact_fstate1">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Zip code</label>
																			<input type="text" class="form-control" name="contact_fzcode1" id="contact_fzcode1">
																		</div>
																		</fieldset>
																	</div>
															</div>
														</div>

													</div>
													<div class="tab-pane" id="tab3">
														<div class="col-md-12 col-sm-12 col-xs-12 nopad">
															<h3 class="medium-title">Licensing Functioning</h3>
														</div>
														<div class="form-group">
															
															<label class="col-md-4 control-label">File Processing : <sup class="red-req">*</sup></label>
																<div class="col-md-8">
																	<div class="input-group">
																		<div id="fileTypeChk" class="icheck-list">
																		</div>
																	</div>
																</div>
														</div>
														<div class="form-group">
															
															<label class="col-md-4 control-label">Business Processing : <sup class="red-req">*</sup></label>
																<div class="col-md-8">
																	<div class="input-group">
																		<div id="businessprocchk" class="icheck-list">
																		</div>
																	</div>
																</div>
														</div>
														<div class="exception-true col-sm-12 nopad"  id ="validation">
															<div class="form-group">
																<label class="col-md-4 control-label">Validations :</label>
																<div class="col-md-8">
																	<div class="icheck-list">
																		<label>
																			<input type="radio" id="valid-check-yes" data-radio="iradio_square-grey" name="valid-check" /> Yes 
																		</label>
																		<label>
																			<input type="radio" id="valid-check-no"  data-radio="iradio_square-grey" name="valid-check" /> No 
																		</label>
																	</div>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-4 control-label">Reports :</label>
																<div class="col-md-8">
																	<div class="icheck-list">
																		<label>
																			<input type="radio" class="icheck" id="rep-check-yes" data-radio="iradio_square-grey" name="rep-check" /> Yes 
																		</label>
																		<label>
																			<input type="radio" id="rep-check-no" class="icheck" data-radio="iradio_square-grey" name="rep-check" /> No 
																		</label>
																	</div>
																</div>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">
															Transmission File Format : <sup class="red-req">*</sup></label>
															<div class="col-md-8">
																<div class="input-group">
																	<div class="icheck-list">
																		<label>
																		<input type="checkbox" id="format-one" class="icheck" data-checkbox="icheckbox_square-grey"> Format 1 </label>
																		<label>
																		<input type="checkbox" id="format-two" class="icheck" data-checkbox="icheckbox_square-grey"> Format 2
																		</label>
																		<label>
																		<input type="checkbox" id="format-three" class="icheck" data-checkbox="icheckbox_square-grey"> Format 3
																		</label>
																		
																	</div>
																</div>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Notification : <sup class="red-req">*</sup></label>
															<div class="col-md-8">
																<div class="icheck-list">
																	<label>
																		<input type="radio" class="icheck" id="note-check-yes" data-radio="iradio_square-grey" name="notifi-check" /> Yes 
																	</label>
																	<label>
																		<input type="radio" id="note-check-no" class="icheck" data-radio="iradio_square-grey" name="notifi-check" /> No 
																	</label>
																</div>
															</div>
														</div>
													</div>
												<div class="tab-pane" id="tab5">
												<div class="col-md-12 col-sm-12 col-xs-12 nopad">
												<div class="medium-title">5 Record - Position : 05 to 25</div>
													<div class="col-md-12 col-sm-12 col-xs-12 nopad">
														<div class="table-scrollable table-scrollable-borderless managebank-cont custmap-tble">
														<table class="table table-hover table-bordered tble-custstup-5rec">
															<thead>
															<tr class="uppercase">
																<th>
																	 ACH Field
																</th>
																<th>
																	IR +  Field Match
																</th>
																<th> 
																	Check Digit
																</th>
																<th> 
																	Scheme
																</th>
																
																<th>
																	Options
																</th>
															</tr>
															</thead>
															<tbody>
															<tr>
																<td width="16.7%">
																	<input type="text" class="form-control" placeholder="Company name" id="compname" /> 
																</td>
																<td width="16.7%">
																	<select class="form-control select2">
																		<option value="">Company Name</option>
																		<option value="1">Bank of America</option>
																		<option value="2">HSBC</option>
																		<option value="3">BB1</option>
																		<option value="4">AT &amp; T</option>
																		<option value="5">NIC</option>
																	</select>
																</td>
																<td width="16.7%">
																	<div class="icheck-list">
																		<label>
																			<input type="radio" class="icheck" id="checkdig5rec-1" data-radio="iradio_square-grey" name="checkdig5rec-1" /> Yes 
																		</label>
																		<label>
																			<input type="radio" id="checkdig5rec-1" class="icheck" data-radio="iradio_square-grey" name="checkdig5rec-1" /> No 
																		</label>
																	</div>
																</td>
																<td width="16.7%">
																	<select class="form-control select2">
																		<option value="">7777</option>
																		<option value="1">Bank of America</option>
																		<option value="2">HSBC</option>
																		<option value="3">BB1</option>
																		<option value="4">AT &amp; T</option>
																		<option value="5">NIC</option>
																	</select>
																</td>
																
																<td width="16.7%">
																	<a class="btn btn-info add-custstup-5rec"><i class="fa fa-plus"></i> Add</a>
																</td>	
															</tr>
															</tbody>
														</table>
													</div>
												</div>
												<div class="medium-title">6 Record - Position : 05 to 50</div>
													<div class="col-md-12 col-sm-12 col-xs-12 nopad">
														<div class="table-scrollable table-scrollable-borderless managebank-cont custmap-tble">
														<table class="table table-hover table-bordered tble-custstup-6rec">
															<thead>
																<tr class="uppercase">
																	<th>
																		 ACH Field
																	</th>
																	<th>
																		IR +  Field Match
																	</th>
																	<th> 
																		Check Digit
																	</th>
																	<th> 
																		Scheme
																	</th>
																	<th>
																		Options
																	</th>
																</tr>
															</thead>
															<tbody>
															<tr>
																<td width="16.7%">
																	<input type="text" class="form-control" placeholder="Company name" /> 
																</td>
																<td width="16.7%">
																	<select class="form-control select2">
																		<option value="">Company Name</option>
																		<option value="1">Bank of America</option>
																		<option value="2">HSBC</option>
																		<option value="3">BB1</option>
																		<option value="4">AT &amp; T</option>
																		<option value="5">NIC</option>
																	</select>
																</td>
																<td width="16.7%">
																	<div class="icheck-list">
																		<label>
																			<input type="radio" class="icheck" id="checkdig6rec-1" data-radio="iradio_square-grey" name="checkdig6rec-1" /> Yes 
																		</label>
																		<label>
																			<input type="radio" id="checkdig6rec-1" class="icheck" data-radio="iradio_square-grey" name="checkdig6rec-1" /> No 
																		</label>
																	</div>
																</td>
																<td width="16.7%">
																	<select class="form-control select2">
																		<option value="">7777</option>
																		<option value="1">Bank of America</option>
																		<option value="2">HSBC</option>
																		<option value="3">BB1</option>
																		<option value="4">AT &amp; T</option>
																		<option value="5">NIC</option>
																	</select>
																</td>
																<td width="16.7%">
																	<a class="btn btn-info add-custstup-6rec"><i class="fa fa-plus"></i> Add</a>
																</td>	
															</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
														
														
														
														
														
														
														
														
													</div>
													<div class="tab-pane" id="tab6">
														
														
														
														<div class="row">
													<div class="col-md-6 col-sm-6 col-xs-6">
														<label><b>Choose Report</b></label>
													</div>
													<div class="col-md-6 col-sm-6 col-xs-6">
														<label><b>Set Time</b></label>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6 col-sm-6 col-xs-6">
													<select class="form-control select2" id="">
																<option value="">Select</option>
																<option value="1">Deposit Summary</option>
																<option value="2">Option 2</option>
																<option value="3">Option 3</option>
																<option value="4">Option 4</option>
															</select>
													</div>
													<div class="col-md-6 col-sm-6 col-xs-6">
														<div class="form-group">
															<input type="text" class="form-control timepicker" placeholder="Time" id="timepick" onBlur="javascript:t()">
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6 col-sm-6 col-xs-6">
													<select class="form-control select2" id="">
														<option value="">Select</option>
														<option value="1">Transaction Summary</option>
														<option value="2">Option 2</option>
														<option value="3">Option 3</option>
														<option value="4">Option 4</option>
													</select>
													</div>
													<div class="col-md-6 col-sm-6 col-xs-6">
														<div class="form-group">
															<input type="text" class="form-control timepicker" placeholder="Time" id="">
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6 col-sm-6 col-xs-6">
													<select class="form-control select2" id="">
														<option value="">Select</option>
														<option value="1">Account Summary</option>
														<option value="2">Option 2</option>
														<option value="3">Option 3</option>
														<option value="4">Option 4</option>
													</select>
													</div>
													<div class="col-md-6 col-sm-6 col-xs-6">
														<div class="form-group">
															<input type="text" class="form-control timepicker" placeholder="Time" id="">
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6 col-sm-6 col-xs-6">
													<select class="form-control select2" id="">
														<option value="">Select</option>
														<option value="1">Option 1</option>
														<option value="2">Option 2</option>
														<option value="3">Option 3</option>
														<option value="4">Option 4</option>
													</select>
													</div>
													<div class="col-md-6 col-sm-6 col-xs-6">
														<div class="form-group">
															<input type="text" class="form-control timepicker" placeholder="Time" id="">
														</div>
													</div>
												</div>
														
														
														
														
														
														
														
													</div>
												</div>
												<div class="form-actions">
													<div class="">
														<div class="col-md-offset-3 col-md-9 text-right">
															<a href="javascript:;" class="btn default button-previous">
															<i class="m-icon-swapleft"></i> Back </a>
															<a href="javascript:;" class="btn blue button-next">
															Continue <i class="m-icon-swapright m-icon-white"></i>
															</a>
															
															<a href="javascript:submitBankForm()" class="btn green button-submit">
															SAVE <i class="m-icon-swapright m-icon-white"></i>
															</a>
														</div>
													</div>
												</div>
											</div>
											</div>
										</form>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<!-- END PAGE CONTENT -->
</div>

<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<!-- END JAVASCRIPTS -->

<script src="../resources/assets/irplus/js/customerLogoUpload.js"></script>
<div class="modal fade" id="imgupload" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Choose a file to upload</h4>
        </div>
        <div class="modal-body">
			<form role="form" id="fileForm" class="form-horizontal" action="javascript: uploadCustomerLogo()">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<input id="bank-img-upload" name="customerLogo" type="file" class="file-loading">
				</div>
			</form>
        </div>
        <div class="modal-footer">
          
        </div>
      </div>
      
    </div>
  </div>
<script type="text/javascript">
	var siteId=<%=session.getAttribute("siteId")%>;
	var userId=<%=session.getAttribute("userid")%>;
	var customerid = <%=request.getParameter("customerId") %>;
	//getting all the active banks on pageload
	
	$(document).ready(function() {  
	
		
		
		if(customerid!=null){
			//update
					
			$.ajax({
			
				method:'get',
				url:'../customer/showOne/'+customerid,
				contentType:'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success:function(response){

				var customerInfo = response.customer;
				
				if(customerInfo.photofile==''){
					$("#bnk-bnklogo").attr('src',"../resources/assets/admin/layout3/img/avatar9.jpg");	
				}else{
					$("#bnk-bnklogo").attr('src',".."+customerInfo.photofile);
				}
					
				$("#bnk-bankLogo").val(customerInfo.photofile);
				$("#bank-cust-id").val(customerInfo.bankCustomerCode);
				$("#comp-name").val(customerInfo.companyName);
				$("#dda-bank").val(customerInfo.ddaBankAc);
				$("#comp-id-no").val(customerInfo.compIdentNo);
				
				var bankName = $("#bnk-name");
				bankName.append(
				        $('<option></option>').val(customerInfo.defaultBankName).html(customerInfo.defaultBankName)
				    );
				
				var branchName=$("#bnk-loc");
				branchName.append(
				        $('<option></option>').val(customerInfo.branchLocation).html(customerInfo.branchLocation)
			    );
				
				$("#website-id").val(customerInfo.website);
				     
									
				        
				
				var contact = customerInfo.customerContact[0];		
				
				$("#customerName").append(contact.firstName+'  '+contact.lastName);
				
				$("#fName-id").val(contact.firstName);
				$("#lastName-id").val(contact.lastName);
				$("#contact-id").val(contact.contactNo);
				$("#email-id").val(contact.emailId);
				$("#address1-id").val(contact.address1);
				$("#address2-id").val(contact.address2);
				$("#state-id").val(contact.state);
				$("#city-id").val(contact.city);
				$("#zipcode-id").val(contact.zipcode);
				
				
				var contacts= customerInfo.customerContact;
				//alert("contacts.length" +contacts.length);
				if(contacts.length>1)
				{
													
					for (var i=1;i<contacts.length ;i++ )
					{
						//alert("Calling value of i "+i);

						if (i<contacts.length-1)
						{
							$('.add-row-bank').click();
						}
						
						
						var bankCtct = contacts[i];
						//alert(JSON.stringify(bankCtct));
						$("#contact_id"+i).val(bankCtct.contactId);
						$("#contact_fname"+i).val(bankCtct.firstName);
						$("#contact_lname"+i).val(bankCtct.lastName);
						$("#contact_crole"+i).val(bankCtct.contactRole);
						$("#contact_fphoneno"+i).val(bankCtct.contactNo);
						$("#contact_femail"+i).val(bankCtct.emailId);
						$("#contact_faddr1"+i).val(bankCtct.address1);
						$("#contact_faddr2"+i).val(bankCtct.address2);
						$("#contact_fstate"+i).val(bankCtct.state);
						$("#contact_fcity"+i).val(bankCtct.city);
						$("#contact_fzcode"+i).val(bankCtct.zipcode);
						
					}
					
					setCustomerLicense(response);
					
				}
				
				
				
				
			//	var customerInfomation = JSON.stringify(response.customerBean.customerContactsBeans_array);
			//	alert("****************************+"+customerInfomation);
			
			var customerContacts_js = JSON.stringify(response.customer[0].customerContact);
		
		//	alert(customerContacts_js+"dddddddddddddddddddddddddd");
				
				var firstName ="";
				var lastName_t ="";
				var constaNo_t ="";
				var address1 ="";
				var address2 ="";
				var emailId ="";
			//$('.add-row-cust').click
			//alert(response.customerBean[0].customerContactsBeans_array.length)
			
			$.each(JSON.parse(customerContacts_js), function(idx, obj) {			
				//alert(obj.isdefault);					//isdefault
		/*		$("#fName-id").val(obj.firstName);		$("#lastName-id").val(obj.lastName);
				$("#contact-id").val(obj.contactNo);   	$("#email-id").val(obj.emailId);
				$("#address1-id").val(obj.address1);	$("#address2-id").val(obj.address2);	*/		
			});  
			
			var customerContacts_js1 = JSON.stringify(response.customer[0].customerContact);
			
			//alert(customerContacts_js1);
			
			var i= 1;
			
		//	alert(response.customerBean[0].customerContactsBeans_array.length)
			
			for (var j = 0; j < response.customer[0].customerContact.length; j++)
			{
					//alert('len='+response.customerBean[0].customerContactsBeans_array.length)
					///	alert(response.customerBean[0].customerContactsBeans_array[j].firstName+"  ****"+j);				
						
					if(response.customer[0].customerContact[j].isdefault)
					{					
					//	alert(response.customerBean[0].customerContactsBeans_array[j].isdefault+"  primary contact"+j);

						var contactsInfo= response.customer[0].customerContact[j];						
					
					//	alert(contactsInfo.firstName +" : primary");
						$("#fName-id").val(contactsInfo.firstName);
						$("#lastName-id").val(contactsInfo.lastName);
						$("#contact-id").val(contactsInfo.contactNo);			
						$("#email-id").val(contactsInfo.emailId);
						$("#address1-id").val(contactsInfo.address1);			
						$("#address2-id").val(contactsInfo.address2);
						
																						
					}else{
						
						
						var contactsInfo= response.customer[0].customerContact[j];	
						
					// Hi lincy..! below line  have to add the condition is defoult = false have to enter loop, 
					//array Contacts misorder come , if in array last one is primary key that time fails .	
						if(j<((response.customer[0].customerContact.length)-1))					
						$('.add-row-cust').trigger('click');
						//	alert(response.customerBean[0].customerContactsBeans_array[j].isdefault+"  **Arjun"+j);

						//	alert(contactsInfo.isdefault);
				
						$("#contact_crole"+j).val(contactsInfo.contactRole);
						$("#contact_fname"+j).val(contactsInfo.firstName);
						$("#contact_lname"+j).val(contactsInfo.lastName);
						$("#contact_fphoneno"+j).val(contactsInfo.contactNo);			
						$("#contact_femail"+j).val(contactsInfo.emailId);
						$("#contact_fstate"+j).val(contactsInfo.state);			
						$("#contact_fcity"+j).val(contactsInfo.city);
						$("#contact_fzcode"+j).val(contactsInfo.zipcode);
						$("#contact_faddr1"+j).val(contactsInfo.address1);
						$("#contact_faddr2"+j).val(contactsInfo.address1);
						//alert('in 1')
						//++i;
					}
					
				}							
				
				},
				error:function(response,statusTxt,error){
					 
						/* alert("Failed to add record :"+response.responseJSON); */
					 }
			});
			
		/*	
			$("#dv_save").hide();
			$("#dv_update").show();   */
			
		}else{
		
		//getting List of active banks
		$.ajax({
			
				method:'get',
				url:'../customer/show/banks/'+siteId,
				contentType:'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success:function(response)
				{
					htmlbnk = "";
					var htmlbnk= "<option value=''>Select Bank</option>";
					var bnkdet = JSON.stringify(response.parentBank.parentBank);
					obj = JSON.parse(bnkdet);
					
					for(var key in obj) {
						htmlbnk += "<option value=" + key  + ">" +obj[key] + "</option>"
					}
					
					$("#bnk-name").html('');
					$("#bnk-name").html(htmlbnk);	
					
			
				},
				error:function(response,statusTxt,error){
					 
						/* alert("Failed to add record :"+response.responseJSON); */
					 }
		});
		
		
		
		
		
		
		
		
		//getting the branches of selected bank
	
	$(function () {
	     var bankId = $(this).val();
	     $("#bnk-name").change(function () {
				var selectedText = $(this).find("option:selected").text();
				bankId = $(this).val();
				
	      $.ajax({
	        	 method: 'get',
	             url: '../customer/getBankBranch/'+bankId,
	             contentType: 'application/json',
				 dataType:'JSON',
				 crossDomain:'true',
	             success: function(response) {
	                var htmlbrnch= "<option value=''>Select Branch</option>";
	                	if(response.bankBranches.bankBranchList.length>0){
	                
						for (var j = 0; j < response.bankBranches.bankBranchList.length; j++)
						{					
							htmlbrnch += "<option value=" + response.bankBranches.bankBranchList[j].branchId + ">" + response.bankBranches.bankBranchList[j].branchLocation  + "</option>"
						} 
						
						$("#bnk-loc").html(htmlbrnch);
	                	}else{
	                	$("#bnk-loc").html('');
	                	}
	                	
						if(response.masterData!=undefined&&response.masterData!=null)
						{
							var buspros= response.masterData.busPros;

							for(j=0;j<buspros.length;j++)
							{
								//alert("In loop");
								//alert("buspros[j].processName"+JSON.stringify(buspros[j]));
								

								var chk = '<label><input type="checkbox" required name="bscheckbox" class="icheck" data-checkbox="icheckbox_square-grey" value='+buspros[j].businessProcessId+'>'+buspros[j].processName+'</label>';
			
								$('#businessprocchk').append(chk).find('.icheck').iCheck({checkboxClass: 'icheckbox_square-grey'});
								
								 
								 
			
								
							}
							var fileTypes= response.masterData.fileTypes;

							for(j=0;j<fileTypes.length;j++)
							{
								//alert("In loop");
								//alert("buspros[j].processName"+JSON.stringify(buspros[j]));
								
								var chk = '<label><input type="checkbox" name="flcheckbox" class="icheck" required data-checkbox="icheckbox_square-grey" value='+fileTypes[j].filetypeid+'>'+fileTypes[j].filetypename+'</label>';
			
								$('#fileTypeChk').append(chk).find('.icheck').iCheck({checkboxClass: 'icheckbox_square-grey'});
							}
							var bankSequence = response.masterData.bankFolderSequence[0];
							
							$("#bnk-folder").val(bankSequence.sequenceName+""+bankSequence.sequenceNumber);
							
						}
	                	
	                	
						
						re_intialize_icheckbox();
	                	
	                	
	                	
	             },
	             error: function (e) {
	                 //called when there is an error
	                 console.log(e.message);
	             /*     alert("failed to get bank branch"); */
	             }
	         });
	     });
	 });
		
		
		}
	});
	
	


//create customer
function submitBankForm()
	{

		
		var numItems = $('div.bankaccount-single').size();
		
		var url_data = ($("#submit_form").serialize());//.replace(/+/g,'%20');//replace(/ /g, '+');
		
		var result = queryStringToJSON(url_data);
		
		var contacts = [];

		contacts[0]=
		{
			firstName:$("#fName-id").val(),
			lastName:$("#lastName-id").val(),
			contactNo:$("#contact-id").val(),
			emailId:$("#email-id").val(),
			address1:$("#address1-id").val(),
			address2:$("#address2-id").val(),
			state:$("#state-id").val(),
			city:$("#city-id").val(),
			zipcode:$("#zipcode-id").val(),
			isdefault:'1',
			usersId:userId,
			isactive:'1'
		};
		 for(var k=1;k<=numItems;k++)
			{
			   
				contacts[k]=
				{
					firstName:result["contact_fname"+k],
					lastName:result["contact_lname"+k],
					contactRole:result["contact_crole"+k],
					contactNo:result["contact_fphoneno"+k],
					emailId: result["contact_femail"+k],
					address1: result["contact_faddr1"+k],
					address2: result["contact_faddr2"+k],
					isdefault:'0',
					state: result["contact_fstate"+k],
					city: result["contact_fcity"+k],
					zipcode:result["contact_fzcode"+k],
					usersId:userId,
					isactive:'1'
				}
			}
		 
			
		 	var bplist='';
			var ftypelist='';

			
			$('#businessprocchk input:checked').each(function() {
					bplist= bplist+$(this).val()+",";
				});
			$('#fileTypeChk input:checked').each(function() {
					ftypelist= ftypelist+$(this).val()+",";
				});
			//alert("bplist:"+bplist+" ftypelist:"+ftypelist);

			bplist = bplist.substring(0,bplist.length-1);

			ftypelist = ftypelist.substring(0,ftypelist.length-1);
			
			
		var customerInfo={
				
				userId:userId,
				BankBranchId:$("#bnk-loc option:selected" ).val(),
				irCustomerCode:'',
				companyName : $("#comp-name").val(),
				bankCustomerCode:$("#bank-cust-id").val(),
				ddaBankAc : $("#dda-bank").val(),
				compIdentNo : $("#comp-id-no").val(),
				photofile:$("#bnk-bankLogo").val(),
				website:$("#website-id").val(),
				customerContact:contacts,
				ftypelist: ftypelist,
				bplist:bplist,
				isactive:'1'
		}
	$.ajax({
			method:'post',
			url:'../customer/create',
			data:JSON.stringify(customerInfo),
			contentType:'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success: function(response) 
			{
				swal({title:"Done",text:"Customer Created Successfully",type:"success"},function(){window.location="manage-customer.jsp";});
			},
			
		 error:function(response,statusTxt,error){
		 
		/* 	alert("Failed to add record :"+response.responseJSON.errMsgs[0]); */
		 }
	});
		
		
}

	 var queryStringToJSON = function (url) {
		    if (url === '')
		        return '';
		    var pairs = (url || location.search).slice(1).split('&');
		    var result = {};
		    for (var idx in pairs) {
		        var pair = pairs[idx].split('=');
		        if (!!pair[0])
		            result[pair[0].toLowerCase()] = decodeURIComponent(pair[1] || '');
		    }
		    return result;
		}
	 
	

function re_intialize_icheckbox(){
	$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue',
		increaseArea: '20%' // optional
	});	
	
	$('input').on('ifClicked', function (event) { 
		var rbtn_id = $(this).attr('id');		
		if(rbtn_id=='expections-check-yes') {
		      $("#validation").fadeIn();
		  } else if(rbtn_id=='expections-check-no') {
		      $("#validation").fadeOut();
		  }
		
	});
}

function setCustomerLicense(response)
{
	var buspros= response.masterData.busPros;
	var bankInfo= response.customer;
	if(bankInfo.bplist!=undefined)
	{
		var bpList = bankInfo.bplist.split(",");
		for(j=0;j<buspros.length;j++)
		{
			var value = $.inArray(buspros[j].businessProcessId+"", bpList);
			//alert("value: "+value+" buspros[j].businessProcessId :"+buspros[j].businessProcessId +" bpList: "+bpList);
			var chk = '';

			if(value!=-1)
			{
				chk = '<label><input type="checkbox" required class="icheck" name="bcheckbox" checked="true" data-checkbox="icheckbox_square-grey" value='+buspros[j].businessProcessId+'>'+buspros[j].processName+'</label>';
				//alert("business process if  condirion  bankInfo.bplist!=undefined");
			}
			else
			{
				chk = '<label><input type="checkbox" required name="bcheckbox" class="icheck" data-checkbox="icheckbox_square-grey" value='+buspros[j].businessProcessId+'>'+buspros[j].processName+'</label>';
				//alert("business process else  bankInfo.bplist!=undefined condirion");
			}
			
			//alert("chk:"+chk+" val "+value);

			$('#businessprocchk').append(chk).find('.icheck').iCheck({checkboxClass: 'icheckbox_square-grey'});
		}
		
	}
	else
	{
		for(j=0;j<buspros.length;j++)
		{
			var chk = '<label><input type="checkbox" class="icheck"  data-checkbox="icheckbox_square-grey" checked="true" value='+buspros[j].businessProcessId+'>'+buspros[j].processName+'</label>';

			$('#businessprocchk').append(chk).find('.icheck').iCheck({checkboxClass: 'icheckbox_square-grey'});
			
			
		}
		
	}
	var fileTypes= response.masterData.fileTypes;
	if(bankInfo.ftypelist!=undefined)
	{
		var ftypeList = bankInfo.ftypelist.split(",");
		for(j=0;j<fileTypes.length;j++)
		{
			//alert("In loop");
			//alert("buspros[j].processName"+JSON.stringify(buspros[j]));
			var value = $.inArray(fileTypes[j].filetypeid+"", ftypeList);

			var chk ="";

			if(value!=-1)
			{
				chk = '<label><input type="checkbox" name="flcheckbox" checked="true" class="icheck" required value='+fileTypes[j].filetypeid+'>'+fileTypes[j].filetypename+'</label>';
			}
			else
			{
				chk = '<label><input type="checkbox" name="flcheckbox" class="icheck" required value='+fileTypes[j].filetypeid+'>'+fileTypes[j].filetypename+'</label>';
			}

			$('#fileTypeChk').append(chk).find('.icheck').iCheck({checkboxClass: 'icheckbox_square-grey'});
			
		}
		
	}
	else
	{
		for(j=0;j<fileTypes.length;j++)
		{
			var chk = '<label><input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey" checked="true" value='+fileTypes[j].filetypeid+'>'+fileTypes[j].filetypename+'</label>';

			$('#fileTypeChk').append(chk).find('.icheck').iCheck({checkboxClass: 'icheckbox_square-grey'});
		}
			
		
	}
	
} 


function re_intialize(){

	$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue',
		increaseArea: '20%' // optional
	});


}
</script>



















</body>
<!-- END BODY -->
</html>