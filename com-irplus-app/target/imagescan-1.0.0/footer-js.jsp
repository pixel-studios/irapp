<script src="../resources/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../resources/assets/global/plugins/jquery-validation/js/jquery.validate.js"></script>

<script src="../resources/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../resources/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../resources/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../resources/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../resources/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../resources/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../resources/assets/global/plugins/icheck/icheck.min.js"></script>
<script src="../resources/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="../resources/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="../resources/assets/global/plugins/bootstrap-fileinput/fileinput.min.js"></script>
<script src="../resources/assets/global/plugins/bootstrap-fileinput/theme.min.js"></script>
<script src="../resources/assets/global/plugins/sweet-alert/js/sweetalert-dev.js"></script>
<script src="../resources/assets/global/plugins/owl.carousel/owl.carousel.min.js"></script>


<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../resources/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../resources/assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="../resources/assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="../resources/assets/admin/layout3/scripts/form-wizard.js" type="text/javascript"></script>
<script src="../resources/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="../resources/assets/global/plugins/select2/select2.min.js"></script>
<script src="../resources/assets/global/plugins/timepicker/jquery.ui.timepicker.js"></script>
<script src="../resources/assets/global/plugins/multiselect/bootstrap-multiselect.min.js"></script>
<script src="../resources/assets/global/plugins/mcustom/jquery.mCustomScrollbar.concat.min.js"></script>
<!--<script type="text/javascript" src="../../assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>-->
<!-- END PAGE LEVEL SCRIPTS -->
<script>
function delfn()
	 {
		 swal({
		title: "Are you sure?",
        text: "You will not be able to recover this details!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false,
		closeOnCancel: true
		});
	}

	

jQuery(document).ready(function() { 

var username = '<%=session.getAttribute("username")%>';
var currentPage = window.location+"";
		//alert("username :"+currentPage.indexOf("login.jsp"));
		
		if (username==null||username=='null')
		{
			if (currentPage.indexOf("login.jsp")==-1&&currentPage.indexOf("file-upload.jsp")==-1)
			{
				showSessionExpiry();
			}
			
		}
$('#cust-setup').click(function(){
	  $("#jvalidate").valid();
});
$('#cust-cont').click(function()
{
	//alert();
	  $(".jvalidate").valid();
});
$('.add-dataentry-filed').click(function()
{
	var i =  $('.custstup-dataentry tr').size();
	
	//alert(i);
	var newDiv = $('<tr class="newdiv-'+i+'"><td><input type="text" class="form-control" id="fieldname-'+i+'" placeholder="Field Name" /></td><td><select class="form-control select2" id="fieldtype-'+i+'"><option value="">select</option><option value="1">Option 1</option><option value="2">Option 2</option><option value="3">Option 3</option><option value="4">Option 4</option></select></td><td><input type="text" class="form-control" id="fieldlength-'+i+'" placeholder="Field Length"></td><td><input type="checkbox"  class="icheck" data-checkbox="icheckbox_square-grey"></td><td><button type="button" class="btn btn-xs btn-danger add-dataentry-filed" onclick="deltecustbnk('+i+')";><i class="fa fa-trash"></i> Delete</button></td></tr>');
	
	$('.custstup-dataentry tbody').append(newDiv);
	$('.icheck').iCheck({checkboxClass: 'icheckbox_square-grey'});	
	$("#fieldtype-"+i+"").select2();
	i++;
	
})	
$('.add-dataentry').click(function()
{
	var i =  $('div.dataentry-wrapper').size();
	//alert("In method value of i "+i);
	i++;
	var newDiv = $('<div class="dataentry-single col-md-12 col-sm-12 col-xs-12"><div class="col-md-6 col-sm-6 col-xs-12"><div class="form-group"><label>IR Plus Form ID</label><input type="text" class="form-control" id="frmid-'+i+'"></div></div><div class="col-md-6 col-sm-6 col-xs-12"><div class="form-group"><label>Form Name</label><input type="text" class="form-control" id="frmname-'+i+'"></div></div><div class="portlet-title col-md-12 col-sm-12 col-xs-12"><div class="caption"><span class="caption-subject font-green-sharp bold uppercase">EPHESOFT Batch Class</span></div></div><div class="col-md-6 col-sm-6 col-xs-12"><div class="form-group"><label>Data Entry</label><input type="text" class="form-control"  id="btchclssde-'+i+'"></div></div><div class="col-md-6 col-sm-6 col-xs-12"><div class="form-group"><label>Validation</label><input type="text" class="form-control" id="btchclssval-'+i+'"></div></div><div class="portlet-title col-md-12 col-sm-12 col-xs-12"><div class="caption"><span class="caption-subject font-green-sharp bold uppercase">EPHESOFT Watch Folder</span></div></div><div class="col-md-6 col-sm-6 col-xs-12"><div class="form-group"><label>Data Entry</label><input type="text" class="form-control" id="wtchclssde-'+i+'"></div></div><div class="col-md-6 col-sm-6 col-xs-12"><div class="form-group"><label>Validation</label><input type="text" class="form-control" id="wtchclssval-'+i+'"></div></div><div class="col-md-12 col-sm-12 col-xs-12"><div class="table-scrollable"><table class="table table-bordered table-hover custstup-dataentry"><thead><tr class="uppercase"><th >Field Name</th><th>Field Type</th><th>Field Length</th><th>Validation</th><th> Add/delete</th></tr></thead><tbody><tr><td><input type="text" class="form-control" id="fieldname-'+i+'" placeholder="Field Name" /></td><td><select class="form-control select2" id="fieldtype-'+i+'"><option value="">select</option><option value="1">Option 1</option><option value="2">Option 2</option><option value="3">Option 3</option><option value="4">Option 4</option></select></td><td><input type="text" class="form-control" id="fieldlength-1" placeholder="Field Length"></td><td><input type="checkbox"  class="icheck" data-checkbox="icheckbox_square-grey"></td><td><button type="button" class="btn btn-xs btn-info add-dataentry-filed"><i class="fa fa-plus"></i> Add row</button></td></tr></tbody></table></div></div></div>');
	$('.dataentry-wrapper').append(newDiv);
});

$('.add-row-bank').click(function()
{
	var i =  $('div.bankaccount-single').size();
	//alert("In method value of i "+i);
	i++;
	var newDiv = $('<div class="col-md-12 col-sm-12 col-xs-12 bankaccount-single bankaccount-single-'+i+'"><a class="close-bnk " onclick="deltebnk('+i+');"><i class="fa fa-trash"></i></a><div class="col-md-12 col-sm-12 col-xs-12 nopad"><input type="hidden" class="form-control" id="contact_id'+i+'" name="contact_id'+i+'" /><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">First Name</label><input type="text" class="form-control" name="contact_fname'+i+'" id="contact_fname'+i+'"></div></fieldset><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">Last Name</label><input type="text" class="form-control" id="contact_lname'+i+'" name="contact_lname'+i+'"></div></fieldset><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">Contact Role</label><input type="text" class="form-control" name="contact_crole'+i+'" id="contact_crole'+i+'"></div></fieldset><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">Phone No.</label><input type="text" class="form-control" name="contact_fphoneno'+i+'" id="contact_fphoneno'+i+'"></div></fieldset></div><div class="col-md-12 col-sm-12 col-xs-12 nopad"><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">Email</label><input type="email" class="form-control " name="contact_femail'+i+'" id="contact_femail'+i+'"></div></fieldset><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">Address 1</label><input type="text" class="form-control" name="contact_faddr1'+i+'" id="contact_faddr1'+i+'"></div></fieldset><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">Address 2</label><input type="text" class="form-control" name="contact_faddr2'+i+'" id="contact_faddr2'+i+'"></div></fieldset><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">City</label><input type="text" class="form-control" name="contact_fcity'+i+'" id="contact_fcity'+i+'"></div></fieldset><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">State</label><input type="text" class="form-control" name="contact_fstate'+i+'" id="contact_fstate'+i+'"></div></fieldset><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">Zip code</label><input type="text" class="form-control" name="contact_fzcode'+i+'" id="contact_fzcode'+i+'"></div></fieldset></div></div>');
	$('.bankaccount-listcont').append(newDiv);
});

$('.add-row-cust').click(function()
{
	var i =  $('div.bankaccount-single').size();
	//alert("In method value of i "+i);
	i++;
	//var newDiv = $('<div class="col-md-12 col-sm-12 col-xs-12 bankaccount-single bankaccount-single-'+i+'"><a class="close-bnk " onclick="deltebnk('+i+');"><i class="fa fa-trash"></i></a><div class="col-md-12 col-sm-12 col-xs-12 nopad"><input type="hidden" class="form-control" id="contact_id'+i+'" name="contact_id'+i+'" required /><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">Contact Role <sup class="red-req">*</sup></label><input type="text" class="form-control" name="contact_crole'+i+'" id="contact_crole'+i+'" required /></div></fieldset><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">Contact Person <sup class="red-req">*</sup></label><input type="text" class="form-control" name="contact_prsn'+i+'" id="contact_prsn'+i+'" required /></div></fieldset><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">Phone No.</label><input type="text" class="form-control" name="contact_fphoneno'+i+'" id="contact_fphoneno'+i+'"></div></fieldset><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">Email ID</label><input type="email" class="form-control " name="contact_femail'+i+'" id="contact_femail'+i+'"></div></fieldset></div><div class="col-md-12 col-sm-12 col-xs-12 nopad"><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">Address</label><input type="text" class="form-control" name="contact_faddr1'+i+'" id="contact_faddr1'+i+'"></div></fieldset><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">State</label><input type="text" class="form-control" name="contact_fstate'+i+'" id="contact_fstate'+i+'"></div></fieldset><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">City</label><input type="text" class="form-control" name="contact_fcity'+i+'" id="contact_fcity'+i+'"></div></fieldset><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">Zip code</label><input type="text" class="form-control" name="contact_fzcode'+i+'" id="contact_fzcode'+i+'"></div></fieldset></div></div>');
	var newDiv = $('<div class="col-md-12 col-sm-12 col-xs-12 bankaccount-single bankaccount-single-'+i+'"><a class="close-bnk " onclick="deltebnk('+i+');"><i class="fa fa-trash"></i></a><div class="col-md-12 col-sm-12 col-xs-12 nopad"><input type="hidden" class="form-control" id="contact_id'+i+'" name="contact_id'+i+'" /><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">First Name<sup class="red-req">*</sup></label><input type="text" class="form-control" name="contact_fname'+i+'" id="contact_fname'+i+'" required /></div></fieldset><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">Last Name</label><input type="text" class="form-control" id="contact_lname'+i+'" name="contact_lname'+i+'"></div></fieldset><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">Contact Role</label><input type="text" class="form-control" name="contact_crole'+i+'" id="contact_crole'+i+'"></div></fieldset><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">Phone No.<sup class="red-req">*</sup></label><input type="text" class="form-control" name="contact_fphoneno'+i+'" id="contact_fphoneno'+i+'" required /></div></fieldset></div><div class="col-md-12 col-sm-12 col-xs-12 nopad"><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">Email</label><input type="email" class="form-control " name="contact_femail'+i+'" id="contact_femail'+i+'"></div></fieldset><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">Address 1</label><input type="text" class="form-control" name="contact_faddr1'+i+'" id="contact_faddr1'+i+'"></div></fieldset><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">Address 2</label><input type="text" class="form-control" name="contact_faddr2'+i+'" id="contact_faddr2'+i+'"></div></fieldset><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">City</label><input type="text" class="form-control" name="contact_fcity'+i+'" id="contact_fcity'+i+'"></div></fieldset><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">State</label><input type="text" class="form-control" name="contact_fstate'+i+'" id="contact_fstate'+i+'"></div></fieldset><fieldset class="col-md-3 col-sm-6 col-xs-12"><div class="form-group"><label class="control-label">Zip code</label><input type="text" class="form-control" name="contact_fzcode'+i+'" id="contact_fzcode'+i+'"></div></fieldset></div></div>');
	$('.jvalidate').append(newDiv);
	
});

$('.add-custstup-5rec').click(function()
{
	
	var i =  $('.tble-custstup-5rec tr').size();
	
	//alert(i);
	var newDiv = $('<tr class="newdiv-'+i+'"><td width="16.7%"><input type="text" class="form-control" placeholder="Company name"  id="compname-'+i+'"/></td><td width="16.7%"><select class="form-control" id="form-select-'+i+'"><option value="">Company Name</option><option value="1">Bank of America</option><option value="2">HSBC</option><option value="3">BB1</option><option value="4">AT &amp; T</option><option value="5">NIC</option></select></td><td width="16.7%"><div class="icheck-list"><label><input type="radio" class="icheck" id="checkdig5rec-'+i+'" data-radio="iradio_square-grey" name="checkdig5rec-'+i+'" /> Yes </label><label><input type="radio" id="checkdig5rec-'+i+'" class="icheck" data-radio="iradio_square-grey" name="checkdig5rec-'+i+'" /> No </label></div></td><td width="16.7%"><select class="form-control" id="form-select-comp5rec-'+i+'"><option value="">7777</option><option value="1">Bank of America</option><option value="2">HSBC</option><option value="3">BB1</option><option value="4">AT &amp; T</option><option value="5">NIC</option></select></td><td width="16.7%"><a class="btn btn-danger" onclick="deltecustbnk('+i+');"><i class="fa fa-trash"></i> Delete</a></td></tr>');
	$('table.tble-custstup-5rec tbody').append(newDiv);
	$('.icheck').iCheck({radioClass: 'iradio_square-grey'});	
	$("#form-select-"+i+"").select2();
	$("#form-select-comp5rec-"+i+"").select2();
	i++;
});
$('.add-custstup-6rec').click(function()
{
	
	var i =  $('.tble-custstup-6rec tr').size();
	
	//alert(i);
	var newDiv = $('<tr class="newdiv-rec6-'+i+'"><td width="16.7%"><input type="text" class="form-control" placeholder="Company name"  id="compname-'+i+'"/></td><td width="16.7%"><select class="form-control" id="form-select-'+i+'"><option value="">Company Name</option><option value="1">Bank of America</option><option value="2">HSBC</option><option value="3">BB1</option><option value="4">AT &amp; T</option><option value="5">NIC</option></select></td><td width="16.7%"><div class="icheck-list"><label><input type="radio" class="icheck" id="checkdig6rec-'+i+'" data-radio="iradio_square-grey" name="checkdig6rec-'+i+'" /> Yes </label><label><input type="radio" id="checkdig6rec-'+i+'" class="icheck" data-radio="iradio_square-grey" name="checkdig6rec-'+i+'" /> No </label></div></td><td width="16.7%"><select class="form-control" id="form-select-comp6rec-'+i+'"><option value="">7777</option><option value="1">Bank of America</option><option value="2">HSBC</option><option value="3">BB1</option><option value="4">AT &amp; T</option><option value="5">NIC</option></select></td><td width="16.7%"><a class="btn btn-danger" onclick="delterecord6('+i+');"><i class="fa fa-trash"></i> Delete</a></td></tr>');
	$('table.tble-custstup-6rec tbody').append(newDiv);
	$('.icheck').iCheck({  radioClass: 'iradio_square-grey'});	
	$("#form-select-"+i+"").select2();
	$("#form-select-comp6rec-"+i+"").select2();
	i++;
});


var k=0;
$('.add-custmap').click(function()
{
	k++;
	var newDiv = $('<tr class="newdiv-'+k+'"><td width="20%"><input type="text" class="form-control reference_data'+k+'"  id='+k+' name="reference_data'+k+'"/></td><td width="20%"><select class="form-control select2 natchafield'+k+'"  id='+k+' name="natchafield'+k+'"><option value="" selected disabled>select</option><option value="1">Receiving DFI ID</option><option value="2">DFI Account ID</option><option value="3">Individual Account ID</option><option value="4">Individual Account Name</option><option value="5">Trace No</option></select></td><td width="20%"><select class="form-control select2 branchname'+k+'"  id='+k+' name="branchname'+k+'" onchange="changeFunc.call(this);"></select></td><td width="20%"><select class="form-control select2 customername'+k+'"  id='+k+' name="customername'+k+'"></select></td><td width="20%"><a class="btn btn-info deltebnk" onclick="deltecustbnk('+k+');"><i class="fa fa-trash"></i> Delete</a></td></tr>');
	$('table.tble-custmap tbody').append(newDiv);
	var siteId=<%=session.getAttribute("siteId")%>;
	$.ajax({		
		method:'get',

		url:'../Customermapping/branch/'+siteId,
		contentType:'application/json',
		dataType:'JSON',
		crossDomain:'true',
		success:function(response)
{
	var appendbankname="<OPTION value='' selected disabled>Select Bank</OPTION>";
				
				for(var i=0;i<response.dashboardResponse.bankInfo.length;i++){

					appendbankname +="<OPTGROUP LABEL="+response.dashboardResponse.bankInfo[i].bankName+">";   
				
					for(var j=0;j<response.dashboardResponse.bankInfo[i].bankbranchinfo.length;j++){
						appendbankname +="<OPTION VALUE="+response.dashboardResponse.bankInfo[i].bankbranchinfo[j].branchId+">"+response.dashboardResponse.bankInfo[i].bankbranchinfo[j].branchLocation+" </OPTION>"; 

					}
		$(".branchname"+k).html(appendbankname);			
				} 
},
error:function(response,statusTxt,error){
	 
	alert("Failed to add record :"+response.responseJSON);
 }
 


	});

});


var g=0;
$('.add-custmapgroup').click(function()
{
	g++;
	var newDivgroup = $('<tr class="newdivgroup-'+g+'"><td width="20%"><select class="form-control select2 groupbranchname'+g+'"  id='+g+' name="groupbranchname'+g+'" onchange="changeFunc.call(this);"></select></td><td width="20%"><select class="form-control select2 groupcustomername'+g+'"  id='+g+' name="groupcustomername'+g+'"></select></td><td width="20%"><a class="btn btn-info deltebnk" onclick="deltecustbnkgroup('+g+');"><i class="fa fa-trash"></i> Delete</a></td></tr>');
	$('table.tble-custmapgroup tbody').append(newDivgroup);
	var siteId=<%=session.getAttribute("siteId")%>;
	$.ajax({		
		method:'get',

		url:'../Customermapping/branch/'+siteId,
		contentType:'application/json',
		dataType:'JSON',
		crossDomain:'true',
		success:function(response)
{
	var appendbankname="<OPTION value='' selected disabled>Select Bank</OPTION>";
				
				for(var i=0;i<response.dashboardResponse.bankInfo.length;i++){

					appendbankname +="<OPTGROUP LABEL="+response.dashboardResponse.bankInfo[i].bankName+">";   
				
					for(var j=0;j<response.dashboardResponse.bankInfo[i].bankbranchinfo.length;j++){
						appendbankname +="<OPTION VALUE="+response.dashboardResponse.bankInfo[i].bankbranchinfo[j].branchId+">"+response.dashboardResponse.bankInfo[i].bankbranchinfo[j].branchLocation+" </OPTION>"; 

					}
		$(".groupbranchname"+g).html(appendbankname);			
				} 
},
error:function(response,statusTxt,error){
	 
	alert("Failed to add record :"+response.responseJSON);
 }
 


	});

});
 	// initiate layout and plugins
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	Demo.init(); // init demo features
	FormWizard.init();
	
	$("#file-upload").fileinput();
	$("[data-toggle='tooltip']").tooltip();
	$(".filemng-upload").fileinput();
	$(".dtTable").dataTable();
	$("#bank-img-upload").fileinput();
	$("#user-img-upload").fileinput();
	//$(".select2").select2();
	


	$('#license-select').change(function()
	{
		if($('#license-select').val() == 2)
		{
			$("#val-period-div").fadeIn();
		}
		else
		{
			$("#val-period-div").fadeOut();
		}
		
	})
	
	
});

function deltebnk(i)
{
	var contactid = $("#contact_id"+i).val();
	$('.bankaccount-single-'+i+'').remove();
	
	deleteContact(contactid);
	
}
function deltecustbnk(i){
	//alert(i);
	$('.newdiv-'+i+'').remove();	
}

function deltecustbnkgroup(k){
	//alert(i);
	$('.newdivgroup-'+k+'').remove();	
}

function delterecord6(i){
	//alert(i);
	$('.newdiv-rec6-'+i+'').remove();	
}
function validfn()
{
	$("#jvalidate").validate();
}
$('.table-file').DataTable({
		
		"columnDefs": [
      { "width": "14%", "targets": 0 },
      { "width": "14%", "targets": 1 },
      { "width": "14%", "targets": 2 },
      { "width": "14%", "targets": 3 },
      { "width": "14%", "targets": 4 },
      { "width": "14%", "targets": 5 },
	  { "width": "14%", "targets": 6 }
    ],
	});
	var accordionsMenu = $('.cd-accordion-menu');

	if( accordionsMenu.length > 0 ) {
		
		accordionsMenu.each(function(){
			var accordion = $(this);
			//detect change in the input[type="checkbox"] value
			accordion.on('change', 'input[type="checkbox"]', function(){
				var checkbox = $(this);
				console.log(checkbox.prop('checked'));
				( checkbox.prop('checked') ) ? checkbox.siblings('ul').attr('style', 'display:none;').slideDown(500) : checkbox.siblings('ul').attr('style', 'display:block;').slideUp(500);
			});
		});
	}

function showSessionExpiry()
{
	swal({title: "Oops...",text: "Session Expired. Please login again",type: "error"},function(){window.location = "login.jsp";});
							
	//swal({'','Session Expired. Please login again','error'},function(){window.location = "manage-license.jsp";})		
	//alert("Session Expired. Please login again");
	//location.href='login.jsp';
}

 
// adding code 11 22 17




var sucmsg = 'has been added successfully';
var exsmsg = 'Already Exists';
var upmsg  = 'has been updated successfully';
var reqmsg = 'Required fields should not be empty';
var delmsg = 'Deleted successfully';
var statusmsg= 'Status updated successfully';
var exsmsg_email = 'Email ID already exists. Please try with other email ID to create account.';
var exsmsg_phone = 'Mobile number already exists. Please try with other mobile number create account.';
var exsmsg_reference = 'Reference exists with other data. Cannot be deleted.';
var oldpass_reference = 'Old Password doesn`t match. Please enter right password.';
var othmsg = 'Opps!!@ Server Error. Please retry again.';
var exsmsg_refstats = 'Reference exists with other data. Cannot Perform the action.';
var imgmsg = 'Error : image size must be 1000 x 1000 pixels.';

function funStats($frm,$par){

	swal({
		title: "Are you sure?",
        text: "You will not be able to recover this details!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    },
    function () {
		$.ajax({		
			url        : "../"+$frm,
			method     : 'GET',
			dataType   : 'json',
			data       : $par,
			beforeSend: function() {
				//loading();
 			},
			success: function(response){
			
				/*unloading();*/
				if(response.statusMsg == 'OK')
				{					
					swal("Success!", delmsg, "success");	
					//datatblCal(dataGridHdn);
					location.reload(true);
				}
				else if(response.statusMsg == '7')
				{					
					swal("Failure!", exsmsg_refstats, "warning");
					//datatblCal(dataGridHdn);	
				}
				else
				location.reload(true);
			}		
		});	
	 
    }); 	 
 }
//Delete data all page common function - END

//Cancel all page common function - START	 
function funCancel($frm,$acts,$stats,$lodlnk)
{  
	swal({
		title: "Are you sure?",
		text: "Are you sure to cancel?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ok",
		closeOnConfirm: true
	},
	function () {
		$("#"+$acts)[0].reset();
		$(location).attr('href', $lodlnk);
	});
}

	


$('.timepicker').timepicker({
   showPeriodLabels: false
});

/*----------*/
	
	
	/*date slider
	$(".homebanks-slider").owlCarousel({
		autoplay: false,
		items: 3,
		slideBy: 1,
		loop: $('.homebanks-slider').children().length > 1 ? true : false,
		infinite: false,
		smartSpeed: 900,
		nav: true,
		navText: [
		  "<i class='fa fa-angle-left'></i>",
		  "<i class='fa fa-angle-right'></i>"
		],

		dots: false
		
	});
	date slider*/


</script>