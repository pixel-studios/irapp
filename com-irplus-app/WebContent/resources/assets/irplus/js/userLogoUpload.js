
var isJpg = function(name) {
    return name.match(/jpg$/i)
};
    
var isPng = function(name) {
    return name.match(/png$/i)
};
	function uploadUserLogo()
	{
		//alert("Submitting file");
		//var file =  $("#user-img-upload");
	    var filename = $.trim($("#user-img-upload").val());
		//alert("filename: "+JSON.stringify(new FormData(document.getElementById("fileForm"))));
        
        if (!(isJpg(filename) || isPng(filename))) {
            alert('Please browse a JPG/PNG file to upload ...');
            return;
        }
        
        $.ajax({
            url: '../user/fileUpload',
            type: "POST",
            data: new FormData(document.getElementById("fileForm")),
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function(data) {
            //  alert("file uploaded");
			 // $("#usr-logo").attr('src',".."+data.userPhoto);
			//  $("#usr-userLogo").val(data.userPhoto);
			  $("#usr-logo").attr('src',"http://tcmuonline.com:8080/fileimages/Images/UserLogo/"+data.logoPath);
			  $("#usr-userLogo").val(data.logoPath);
			},
            failure : function(jqXHR, textStatus) {
              //alert(jqXHR.responseText);
              alert('File upload failed ...');
		   }
          });
		}
