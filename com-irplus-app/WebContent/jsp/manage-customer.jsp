<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container-fluid">
			<!-- BEGIN PAGE TITLE -->
			<!--<div class="col-md-6 col-sm-6 col-xs-12 page-title">
				<h1>Customers</h1>
			</div>-->
			<div class="col-md-6 col-sm-6 col-xs-12 text-right">
			<div class="fixedbtn-container">
				<a href="javascript:void(0);" class="btn serach m-t-15 search-trigger">
					<i class="fa fa-search"></i> search
				</a>
				<form action="customer-setup.jsp" method="post"><input type="hidden" name="bankId" value="<%=request.getParameter("bankId")%>" /><button type="submit" class="btn orange m-t-15"><i class="fa fa-plus"></i></button></form>
			</div>
			</div>
			<!-- END PAGE TITLE -->

		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container-fluid">

			<!-- BEGIN PAGE BREADCRUMB -->
			<!--<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>

				<li class="active">
					Manage Customers
				</li>

			</ul>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 text-right">
			<a href="customer-setup.jsp" class="btn blue"><i class="fa fa-plus"></i> Add Customer</a>
			</div>
			</div>
			</div>-->
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<div class="profile-content">
					<div class="col-md-12 nopad searchmain-wraper" id="search-wraper">
					<div class="portlet light">
					<span class="searchclose"> &times; </span>
						<form role="form">


									<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<!--<label for="form_control_1">Bank Customer id</label>-->
										<input type="text" class="form-control" id="form_control_1" placeholder="Customer ID">
									</div>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<!--<label for="form_control_1">Customer Name</label>-->
										<input type="text" class="form-control" id="form_control_1" placeholder="Customer Name">
									</div>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
									<!--	<label for="form_control_1">File Type</label>-->
										<select class="form-control select2" id="bnk-loc" name="bnk-loc">
											<option value="">Location</option>

										</select>

									</div>
									</div>
										
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
									<!--	<label for="form_control_1">File Type</label>-->
										<select class="form-control select2" id="fileprocessdd">
											<option value="">File Type</option>

										</select>

									</div>
									</div>
										
										
										
										
										
										
									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<!--<label for="form_control_1">Business Processing</label>-->
										<select class="form-control select2" id="busiprocessdd">
											<option value="">Business Process</option>

										</select>
									</div>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<!--<label for="form_control_1">Status</label>-->
										<select class="form-control select2" id="form_control_1">
											<option value="">Status</option>
											<option value="1">Active</option>
											<option value="0">Inactive</option>
										</select>
									</div>
									</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
								<!-- <label for="form_control_1">&nbsp;</label>-->
								<div class="form-group ">
								<button type="button" class="btn blue">Search</button>
								<!-- <button type="button" class="btn default">Reset</button> -->

								</div>
								</div>
								</div>
							</form>
							</div>
							</div>

						<div class="">
							<div class="col-md-12 nopad">
					<div class="portlet light portlet-border">
							<div class="pagestick-title">
								<span>Manage Customers</span>
							</div>
							<div class="pagestick-left">
								<span id ="bank-name"></span>
							</div>
						<div class="portlet-body">
						
							<div class="table-scrollable table-scrollable-borderless managebank-cont">
							<!--	<table id="manage-customer-id" class="table dtTable"> -->
								<table id="manage-customer-id" class="table">
									<col width="25%"></col>
									<col width="13%"></col>
									<col width="12%"></col>
									<col width="15%"></col>
									<col width="12%"></col>
									<col width="12%"></col>
									<col width="10%"></col>
									<col width="10%"></col>
									<col width="18%"></col>
								<thead>
								<tr class="uppercase">
									<th>Customer</th>
									
									<th>
										 Customer Id
									</th>
									<th>
										 Location
									</th>
									<th>
										 Primary Contact
									</th>

									<th>
										 Email
									</th>
									<th>
										 Phone
									</th>
									
									<!--<th>
										Created date
									</th>
									<th>
										Manage Forms
									</th>-->
									<th>
										Status
									</th>
									<th>
										Options
									</th>
								</tr>
								</thead>
				<!--			<tbody>
									<tr>
										<td class="fit">
											<img class="banklogo" src="../resources/assets/admin/layout3/img/IBM-logo-blue.png">
										</td>
										<td>
											<span class="bold">IBM</span>
										</td>
										<td>
											 Bank of America
										</td>
										<td>
											 IBM0923
										</td>
										<td>
											Kevin
										</td>
										<td>
											help.desk@ibm.com
										</td>
										<td>
											+91 215468764
										</td>
										<td>
											07/21/2016
										</td>
										<td>
											<a href="manage-forms.jsp" class="btn yellow btn-xs">view</a>
										</td>
										<td>
											<button type="button" class="btn green btn-xs">Active</button>
										</td>
										<td>
											<a class="edit-btn" data-toggle="tooltip" data-placement="right" title="Edit!">
												<i class="fa fa-edit"></i>
											</a>
										</td>
									</tr>
									<tr>
										<td class="fit">
											<img class="banklogo" src="../resources/assets/admin/layout3/img/at&t.png">
										</td>
										<td>
											<span class="bold"> AT&T </span>
										</td>
										<td>
											 Citi bank
										</td>
										<td>
											 AT&T001

										</td>

										<td>
											Supervisor
										</td>

										<td>
											info@at&t.com
										</td>
										<td>
											+91 215461124
										</td>
										<td>
											07/21/2016
										</td>
										<td>
											<a href="manage-forms.jsp" class="btn yellow btn-xs">view</a>
										</td>
										<td>
											<button type="button" class="btn red btn-xs">Inactive</button>
										</td>
										<td>
											<a class="edit-btn" data-toggle="tooltip" data-placement="right" title="Edit!">
												<i class="fa fa-edit"></i>
											</a>
										</td>
									</tr>
									<tr>
										<td class="fit">
											<img class="banklogo" src="../resources/assets/admin/layout3/img/A&F.png">
										</td>
										<td>
											<span class="bold">Abercrombie and Fitch</span>
										</td>
										<td>
											 Standard Chartered
										</td>
										<td>
											 AB&F091

										</td>

										<td>
											Steve
										</td>
										<td>
											support@a&f.com
										</td>
										<td>
											+91 215468764
										</td>
										<td>
											07/21/2016
										</td>
										<td>
											<a href="manage-forms.jsp" class="btn yellow btn-xs">view</a>
										</td>
										<td>
											<button type="button" class="btn bg-blue-hoki btn-xs">Deleted</button>
										</td>
										<td>
											<a class="edit-btn" data-toggle="tooltip" data-placement="right" title="Edit!">
												<i class="fa fa-edit"></i>
											</a>
										</td>
									</tr>
								</tbody>     // -->


						<!-- MINE-11-8-2017 -START		//

								<tbody>
									<tr>
										<td class="fit">
											<img class="banklogo" src="../resources/assets/admin/layout3/img/IBM-logo-blue.png">
										</td>
										<td>
											<span class="bold">IBM</span>
										</td>
										<td>
											 Bank of America
										</td>
										<td>
											 IBM0923
										</td>
										<td>
											Kevin
											<input type="text" id="primary-contact-Id" name="">
										</td>
										<td>
											help.desk@ibm.com
											<input type="text" id="email-Id" name="">
										</td>
										<td>
											+91 215468764
											<input type="text" id="phone-Id" name="">
										</td>
										<td>
											07/21/2016
										</td>
										<td>
											<a href="manage-forms.jsp" class="btn yellow btn-xs">view</a>
										</td>
										<td>
											<button type="button" class="btn red btn-xs">Inactive</button>
										</td>
										<td>
											<a class="edit-btn" data-toggle="tooltip" data-placement="right" title="Edit!">
												<i class="fa fa-edit"></i>
											</a>
										</td>

									</tr>
								</tbody>


					MINE-11-8-2017 - CLOSE	//			-->








							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />

<!-- BEGIN JAVASCRIPTS -->


<script>
var siteId=<%=session.getAttribute("siteId")%>;
var bankId=<%=request.getParameter("bankId") %>;
$(document).ready(function(){

	
	//getting List of active banks
	$.ajax({
		
		 method: 'get',
         url: '../customer/getBankBranch/'+bankId,
         contentType: 'application/json',
		 dataType:'JSON',
		 crossDomain:'true',
         success: function(response) {
        	 
        	 
        	 $("#bank-name").append(response.bankBranches.bankBranchList[0].parent_bankName);
        	 
        	 
            var htmlbrnch= "<option value=''>Select Branch</option>";
            	if(response.bankBranches.bankBranchList.length>0){
            
				for (var j = 0; j < response.bankBranches.bankBranchList.length; j++)
				{					
					htmlbrnch += "<option value="+response.bankBranches.bankBranchList[j].branchId + ">" + response.bankBranches.bankBranchList[j].branchLocation  + "</option>"
				} 
				
					$("#bnk-loc").html(htmlbrnch);
            	}else{
            		$("#bnk-loc").html('');
            	}
         	
         },
         error: function (e) {
             //called when there is an error
             console.log(e.message);
           /*   alert("failed to get bank branch"); */
         }

	
	});

	
	
	
	
//getting business process and fileType for search
	$.ajax({
		method: 'get',
		url: '../master/getData/'+siteId,
		contentType: 'application/json',
		dataType:'JSON',
		crossDomain:'true',
		success: function (response) {

				//alert("Added"+JSON.stringify(response));
				//alert("response.masterData "+response.masterData.busPros[0].processName);
				//location.href='manage-bank.jsp';
			if(response.masterData!=undefined&&response.masterData!=null)
			{
				var buspros= response.masterData.busPros;

				for(j=0;j<buspros.length;j++)
				{
					//alert("In loop");
					//alert("buspros[j].processName"+JSON.stringify(buspros[j]));

					var chk = '<option value='+buspros[j].businessProcessId+'>'+buspros[j].processName+'</option>';

					$('#busiprocessdd').append(chk);
				}
				var fileTypes= response.masterData.fileTypes;

				for(j=0;j<fileTypes.length;j++)
				{
					//alert("In loop");
					//alert("buspros[j].processName"+JSON.stringify(buspros[j]));

					var chk = '<option value='+fileTypes[j].filetypeid+'>'+fileTypes[j].filetypename+'</option>';

					$('#fileprocessdd').append(chk);


				}
			}


		},

	 error:function(response,statusTxt,error){


	 }
	});



	$.fn.dataTable.ext.errMode = 'throw'
            $("#manage-customer-id").dataTable({searching: false,lengthChange:false,pageLength:50,
			ajax :{
			"url":"../customer/showAll/"+bankId,
			"dataSrc":"customerBean"
			//	$("#bank-name").append(customerBean[0].defaultBankName);
		  		},
                //"aaData": arreglo,
                "ordering":true,
                "aoColumns": [
                
                            { //"mData":"irCustomerCode"
								"mData": null,
                                "mRender": function (data, type, full) {

							//photofile //customer_photos
								var src = "http://tcmuonline.com:8080/fileimages/Images/CustomerLogo/"+data.photofile;
								if(data.photofile!='')
									
								return "<img src="+src+" class='banklogo' alt=''>"+" "+full.companyName;
								else
								return "<img src='../resources/assets/admin/layout3/img/avatar9.jpg' class='banklogo' alt='bank-logo'>"+" "+full.companyName;
								}

							},
                           
							{ "mData":"bankCustomerCode"},
							{ "mData":"branchLocation"},
							{ //"mData":"firstName"
								"mData":null,
                                "mRender": function (data, type, full) {
								if(data.firstName != '')
									return full.firstName+" "+full.lastName;
									//return data = 'firstName' ? 'firstName' : ''
									},
							},
							{ "mData":"emailId"},
							{ "mData":"contactNo"},
							
							{
								"mData": null,
                                "mRender": function (data, type, full) {

								var urlval = 'customer/update/status/'+data.customerId;
								if(data.isactive=='1')
								return'<div class="btn-group" data-toggle="btn-toggle" ><button class="btn btn-success btn-xs active" type="button"><i class="fa fa-square text-green"></i> Active</button><button class="btn btn-default btn-xs" type="button" onclick="funchangestatus('+data.customerId+',0);"><i class="fa fa-square text-red"></i> &nbsp;</button></div>';
								else
								return'<div class="btn-group" data-toggle="btn-toggle"><button class="btn btn-default btn-xs" type="button" onclick="funchangestatus('+data.customerId+',1);"><i class="fa fa-square text-green"></i> &nbsp;</button><button class="btn btn-danger btn-xs active" type="button"><i class="fa fa-square text-red"></i> InActive</button></div>';
								}
							},
							{
								data: null, render: function (data, type, row )
								{
									
									
								// alert("data.moduleid"+data.moduleid);
									var urlval = 'customer/delete/'+data.customerId;
									return  '<form action="customer-setup.jsp" method="post"><input type="hidden" name="customerId" value="'+data.customerId+'"/><input type="hidden" name="bankId" value="'+bankId+'" /><button type="submit" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></button></form><a href="javascript:;" onclick="deleteCustomer('+data.customerId+',2);" class="delete-btn btn btn-danger btn-xs" data-toggle="tooltip" data-placement="right" title="Delete!"><i class="fa fa-trash"></i></a>'
					//<a href="#" class="delete-btn btn red btn-xs" data-toggle="tooltip" data-placement="right" title="Delete!" onclick="funStats(\''+urlval+'\',1)"><i class="fa fa-trash-o"></i></a>
								}

							},
                            /*{
                                "mData": null,
                                "mRender": function (data, type, full) {

								var firstName = full.customerContactsBeans_array.firstName;
								var secname = full.customerContactsBeans_array.lastName;
								return firstName+' '+secname;
								//alert(full.emailid)
		                            //var nombre =  full.product_email ;
                                    //var a2 = '<h3><a href="#">'+full.product_name+'</a></h3>';
                                    //var desc = '<p class="description">'+full.priduct_description+'</d>';
                                    //return '<a href="'+nombre+'" class="mail">Email Product</a> ' + a2 + desc;
                                },

                            },*/



                            ],
                "bDestroy": true
            }).fnDraw();
});
function funchangestatus(customerId,isactive)
{
	 swal({
	title: "Want to change status?",
   text: "",
   type: "warning",
   showCancelButton: true,
   confirmButtonColor: "#DD6B55",
   confirmButtonText: "Yes, Change it!",
   closeOnConfirm: false,
	closeOnCancel: true
	},
		function () {

			$.ajax({
				method: 'get',
				url: '../customer/update/status/'+customerId+'/'+isactive,
				contentType: 'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success: function (response) {
					 swal({title: "Done",text: "Updated",type: "success"},function(){window.location = "manage-customer.jsp";});
						// swal('Done!','Added','success'),function()
						// {
							 // window.location = 'manage-bank.jsp';
						// }
						//location.href='manage-bank.jsp';
				},

			 error:function(response,statusTxt,error){

				/* alert("Failed to add record :"+response.responseJSON); */
			 }
			});
	}
)}
function deleteCustomer(customerId,isactive)
{
	 swal({
	title: "Want to delete?",
   text: "",
   type: "warning",
   showCancelButton: true,
   confirmButtonColor: "#DD6B55",
   confirmButtonText: "Yes, delete it!",
   closeOnConfirm: false,
	closeOnCancel: true
	},
		function () {

			$.ajax({
				method: 'post',
				url: '../customer/delete/'+customerId,
				contentType: 'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success: function (response) {
					 swal({title: "Done",text: "Deleted Successfully",type: "success"},function(){window.location = "manage-customer.jsp";});
						// swal('Done!','Added','success'),function()
						// {
							 // window.location = 'manage-bank.jsp';
						// }
						//location.href='manage-bank.jsp';
				},

			 error:function(response,statusTxt,error){

				/* alert("Failed to add record :"+response.responseJSON); */
			 }
			});
	}
)}
$(document).ready(function()
{/*

	  $('#manage-customer-id').DataTable( {

		  ajax :{
			"url":"../customer/showAll",
			"dataSrc":"customerBean"
		  },

		"processing": true,
        //"serverSide": true,x
        "columns": [

			{
			"data":"companyName"

			},
			{"data":"companyName"

			},
			{"data":"defaultBankName"},
			{"data":"bankCustomerCode"},
			{"data": {"customerContactsBeans_array": [0] }

			},
			{"data":"bankCustomerCode"

			},
			//if(customerContactsBeans_array)
			{"data": [{"customerContactsBeans_array": [0]}]


				//"data":"customerContactsBeans_array.3"


			},
			{"data":"customerContactsBeans_array.0"

			},

			{"data":"bankCustomerCode"

			},


        ]
    } );
*/});



/*


		{ data: null, render: function ( data, type, row ) {
		   // alert(data.isactive);
	//		if(data.isactive=='1')
			return '<button type="button" class="btn green btn-xs">Active</button>';
			else
			return '<button type="button" class="btn red btn-xs">inactive</button>';

			var urlval = 'user/update/status/'+data.userId;
					if(data.isactive=='1')
					return '<button type="button" class="btn green btn-xs" onclick="funchangestatus(\''+urlval+'\',0)">Active</button>';
					else
					return '<button type="button" class="btn red btn-xs" onclick="funchangestatus(\''+urlval+'\',1)">inactive</button>';
	}},



		{ data: null, render: function ( data, type, row ){
              //  alert("data.moduleid"+data.moduleid);
			  var urlval = 'user/delete/'+data.userId;
                return  '<a href="profile.jsp?userid='+data.userId+'" class="edit-btn btn blue btn-xs" data-toggle="tooltip" data-placement="right" title="Edit!"><i class="fa fa-edit"></i></a><a href="#" class="delete-btn btn red btn-xs" data-toggle="tooltip" data-placement="right" title="Delete!" onclick="funStats(\''+urlval+'\',1)"><i class="fa fa-trash-o"></i></a>'
			}}

*/

</script>


<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
