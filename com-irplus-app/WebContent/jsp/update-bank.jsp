<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<jsp:include page="includes/style.jsp" />
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-menu-fixed" class to set the mega menu fixed  -->
<!-- DOC: Apply "page-header-top-fixed" class to set the top menu fixed  -->
<body class="page-md">
<!-- BEGIN HEADER -->
<div class="page-header">
	<!-- BEGIN HEADER TOP -->
	<jsp:include page="includes/header-top.jsp" />
	<!-- END HEADER TOP -->
	<!-- BEGIN HEADER MENU -->
	<jsp:include page="includes/header-menu.jsp" />
	<!-- END HEADER MENU -->
</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE 
			<div class="page-title">
				<h1>Bank Creation</h1>
			</div>
			 END PAGE TITLE -->
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<!--<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="index.jsp">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				
				<li class="active">
					 Bank Creation
				</li>
			</ul>
			</div>
			</div>
			</div>-->
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="bnk-setup-container">
				<div class="col-md-12 nopad">
					<!-- BEGIN PROFILE SIDEBAR -->
					<div class="profile-sidebar">
						<!-- PORTLET MAIN -->
						<div class="portlet light profile-sidebar-portlet">
							<!-- SIDEBAR USERPIC -->
							<div class="profile-userpic">
								<span class="userpic-inner">
									<img id="bnk-bnklogo" class="img-responsive" alt="">
									<a class="edit-pic bank-img" data-toggle="modal" data-target="#imgupload">
										<i class="fa fa-edit"></i>
									</a>
								</span>	
							</div>
							<!-- END SIDEBAR USERPIC -->
							<!-- SIDEBAR USER TITLE -->
							<div class="profile-usertitle">
								<div id="bnk-bname" class="profile-usertitle-name">
									
								</div>
								<div id="bnk-bcode" class="irplus-id">
								
								</div>
								
							</div>
							<!-- END SIDEBAR USER TITLE -->
							<!-- SIDEBAR MENU -->
							<div class="profile-usermenu">
								<ul class="nav">
									
								</ul>
							</div>
							<!-- END MENU -->
						</div>
						<!-- END PORTLET MAIN -->
						
					</div>
					<!-- END BEGIN PROFILE SIDEBAR -->
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content  boxwith-sidebar">
						<div class="row">
							<div class="col-md-12">
								<div class="portlet light col-md-12 col-sm-12 col-xs-12" id="form_wizard_1">
								<div class="pagestick-title">
								<span>Bank Setup</span>
							</div>
									<div class="portlet-body form">
									<form action="#" class="form-horizontal" id="submit_form" method="POST">	
										<div class="form-wizard">
											<div class="form-body">
												<ul class="nav nav-pills nav-justified steps">
													<li>
														<a href="#tab1" data-toggle="tab" class="step">
														<span class="number">
														1 </span>
														<span class="desc">
														<i class="fa fa-check"></i> Bank Information </span>
														</a>
													</li>
													<li>
														<a href="#tab2" data-toggle="tab" class="step">
														<span class="number">
														2 </span>
														<span class="desc">
														<i class="fa fa-check"></i> Business Process </span>
														</a>
													</li>
													<li>
														<a href="#tab3" data-toggle="tab" class="step active">
														<span class="number">
														3 </span>
														<span class="desc">
														<i class="fa fa-check"></i> Contacts </span>
														</a>
													</li>
												</ul>
												<div id="bar" class="progress progress-striped" role="progressbar">
														<div class="progress-bar progress-bar-success"></div>
												</div>
												<div class="tab-content">
													<div class="tab-pane active" id="tab1">
													
														<input type="hidden" class="form-control" id="bnk-id" name="bnk-id" />
														<input type="hidden" class="form-control" id="bnk-bankLogo" name="bnk-bankLogo" required />
														
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Bank Name <sup class="red-req">*</sup></label>
															<input type="text" class="form-control" id="bnk-name" name="bnk-name" required />
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Bank Id <sup class="red-req">*</sup></label>
															<input type="text" class="form-control" id="bnk-ownid" name="bnk-ownid" required />
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Location <sup class="red-req">*</sup></label>
															<input type="text" class="form-control" id="bnk-branchloc" name="bnk-branchloc" required />
														</fieldset>
														<input type="hidden" class="form-control" id="bnk-branchid" name="bnk-branchid" />
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Location Id</label>
															<input type="text" class="form-control" id="bnk-locid" name="bnk-locid"  />
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">RT (Routing Number) <sup class="red-req">*</sup></label>
															<input type="text" class="form-control" id="bnk-rtnum" name="bnk-rtnum" required />
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">DDA (Direct Deposit Account) <sup class="red-req">*</sup></label>
															<input type="text" class="form-control" id="bnk-dda" name="bnk-dda" required />
														</fieldset>
														<fieldset class="portlet-title col-md-12 col-sm-12 col-xs-12 ">
															<span class="caption-subject font-green-sharp bold uppercase">Primary contact</span>
														</fieldset>
														<input type="hidden" class="form-control" id="bnk-contactid" name="bnk-contactid" />
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">First Name <sup class="red-req">*</sup></label>
															<input type="text" class="form-control" id="bnksetup-fname" name="bnksetup-fname" required />
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Last Name</label>
															<input type="text" class="form-control" id="bnksetup-lname" name="bnksetup-lname" />
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Phone No <sup class="red-req">*</sup></label>
															<input type="text" class="form-control" id="bnksetup-ctct" name="bnksetup-ctct"  />
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Email </label>
															<input type="email" class="form-control" id="bnksetup-email" name="bnksetup-email"  />
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Address Line 1 </label>
															<textarea class="form-control" rows="3" id="bnksetup-addr1" name="bnksetup-addr1"  /></textarea>
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Address Line 2</label>
															<textarea class="form-control" rows="3" id="bnksetup-addr2" name="bnksetup-addr2" ></textarea>
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">City </label>
															<input type="text" class="form-control" id="bnksetup-cty" name="bnksetup-cty"  />
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">state </label>
															<input type="text" class="form-control" id="bnksetup-state" name="bnksetup-state"  />
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Zip code </label>
															<input type="text" class="form-control" id="bnksetup-zcode" name="bnksetup-zcode"  />
														</fieldset>
														<fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
															<label for="form_control_1">Website</label>
															<input type="text" class="form-control" id="bnksetup-web" name="bnksetup-web" >
														</fieldset>
														<input type="hidden" class="form-control" id="bnk-isdefault" name="bnk-isdefault" />
														<input type="hidden" class="form-control" id="bnk-isactive" name="bnk-isactive" />
															
													</div>
													<div class="tab-pane" id="tab2">
														<div class="col-md-12 col-sm-12 col-xs-12 nopad">
															<h3 class="medium-title">Licensing Functioning</h3>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">File Processing : <sup class="red-req">*</sup></label>
																<div class="col-md-8">
																	<div class="input-group">
																		<div id="fileTypeChk" class="icheck-list">
																		</div>
																	</div>
																</div>
														</div>
													<!-- 	<div class="form-group">
															<label class="col-md-4 control-label">Business Processing : </label>
																<div class="col-md-8">
																		<div class="input-group">
																			<div id="businessprocchk" class="icheck-list">
																			</div>
																		</div>
																</div>
														</div> -->
													</div>
													<div class="tab-pane" id="tab3">
														<div id ="contact_form" class="col-md-12 col-sm-12 col-xs-12 bankaccount-listcont">
															
															<div class="form-group text-right">
															<button type="button" class="btn blue add-row-bank"><i class="fa fa-plus"></i> Add row</button>
															</div>
															<div class="col-md-12 col-sm-12 col-xs-12 bankaccount-single">
																	<div class="col-md-12 col-sm-12 col-xs-12 nopad">
																		<input type="hidden" class="form-control" id="contact_id1" name="contact_id1" />
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																			<div class="form-group">
																				<label class="control-label">First Name</label>
																			<input type="text" class="form-control" name="contact_fname1" id="contact_fname1">
																			</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Last Name</label>
																			<input type="text" class="form-control" id="contact_lname1" name="contact_lname1">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																			<div class="form-group">
																				<label class="control-label">Contact Role</label>
																				<input type="text" class="form-control" name="contact_crole1" id="contact_crole1">
																			</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Phone No.</label>
																			<input type="text" class="form-control" name="contact_fphoneno1" id="contact_fphoneno1">
																		</div>
																		</fieldset>
																	</div>
																	<div class="col-md-12 col-sm-12 col-xs-12 nopad">
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Email</label>
																			<input type="email" class="form-control " name="contact_femail1" id="contact_femail1">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Address 1</label>
																			<input type="text" class="form-control" name="contact_faddr11" id="contact_faddr11">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Address 2</label>
																			<input type="text" class="form-control" name="contact_faddr21" id="contact_faddr21">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">City</label>
																			<input type="text" class="form-control" name="contact_fcity1" id="contact_fcity1">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">State</label>
																			<input type="text" class="form-control" name="contact_fstate1" id="contact_fstate1">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Zip code</label>
																			<input type="text" class="form-control" name="contact_fzcode1" id="contact_fzcode1">
																		</div>
																		</fieldset>
																	</div>															
															</div>
													</div>
												</div>
												<div class="form-actions">
													<div class="row">
														<div class="col-md-offset-3 col-md-9 text-right">
															<a href="javascript:;" class="btn default button-previous">
															<i class="m-icon-swapleft"></i> Back </a>
															<a href="javascript:;" class="btn blue button-next">
															Continue <i class="m-icon-swapright m-icon-white"></i>
															</a>
															<a href="javascript:submitUpdateBankForm()" class="btn green button-submit">
															Submit <i class="m-icon-swapright m-icon-white"></i>
															</a>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>	
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<script src="../resources/assets/irplus/js/bankLogoUpload.js"></script>
<!-- END JAVASCRIPTS -->
<div class="modal fade" id="imgupload" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Choose a file to upload</h4>
        </div>
        <div class="modal-body">
			<form role="form" id="fileForm" action="javascript:uploadBankLogo()" class="form-horizontal">
								<div class="col-md-12 col-sm-12 col-xs-12">
								
								<input id="bank-img-upload" name="bankLogo"  type="file" multiple class="file-loading">
								</div>
			</form>
        </div>
        <div class="modal-footer">
          
        </div>
      </div>
      
    </div>
  </div>
</body>
<script>
var bankId = <%= request.getParameter("bankId") %>;

var numItems = $('div.bankaccount-single').size();

//alert(numItems);

	
	$(document).ready(function() {  
	
		 $.ajax({
				method: 'get',
				url: '../bank/get/'+bankId,
				contentType: 'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success: function (response) {
					
						//alert("Added"+JSON.stringify(response));
						//alert("response.masterData "+response.masterData.busPros[0].processName);
						//location.href='manage-bank.jsp';
					if(response.bank!=undefined&&response.bank!=null)
					{
						var bankInfo= response.bank;
					$("#bnk-id").val(bankInfo.bankId);
					if(bankInfo.bankLogo==''){
						$("#bnk-bnklogo").attr('src',"../resources/assets/admin/layout3/img/avatar9.jpg");	
					}else{
						$("#bnk-bnklogo").attr('src',".."+bankInfo.bankLogo);
					}
					
					$("#bnk-bankLogo").val(bankInfo.bankLogo);
					$("#bnk-branchid").val(bankInfo.branchId);
					$("#bnk-bname").html(bankInfo.bankName);
					$("#bnk-bcode").html(bankInfo.bankCode);
					//alert("Value" +bankInfo.branchLocation);
					$("#bnk-branchloc").val(bankInfo.branchLocation);
					$("#bnk-blocationId").html(bankInfo.locationId);
					$("#bnk-name").val(bankInfo.bankName);
					$("#bnk-ownid").val(bankInfo.bankCode);
					$("#bnk-locid").val(bankInfo.locationId);
					$("#bnk-rtnum").val(bankInfo.rtNo);
					$("#bnk-dda").val(bankInfo.ddano);
					$("#bnk-isactive").val(bankInfo.bankStatus);
					
					var contact = bankInfo.contacts[0];
					$("#bnk-contactid").val(contact.contactId);
					$("#bnksetup-fname").val(contact.firstName);
					$("#bnksetup-lname").val(contact.lastName);
					$("#bnksetup-ctct").val(contact.contactNo);
					$("#bnksetup-email").val(contact.emailId);
					$("#bnksetup-addr1").val(contact.address1);
					$("#bnksetup-addr2").val(contact.address2);
					$("#bnk-isdefault").val(bankInfo.isdefault);
					$("#bnksetup-state").val(bankInfo.state);
					$("#bnksetup-cty").val(bankInfo.city);
					$("#bnksetup-zcode").val(bankInfo.zipcode);
					$("#bnksetup-web").val(bankInfo.website);
					//ftypelist: ftypelist,
					//bplist:bplist,
					
					var contacts= bankInfo.contacts;
					//alert("contacts.length" +contacts.length);
					if(contacts.length>1)
					{
														
						for (var i=1;i<contacts.length ;i++ )
						{
							//alert("Calling value of i "+i);

							if (i<contacts.length-1)
							{
								$('.add-row-bank').click();
							}
							
							
							var bankCtct = contacts[i];
							//alert(JSON.stringify(bankCtct));
							$("#contact_id"+i).val(bankCtct.contactId);
							$("#contact_fname"+i).val(bankCtct.firstName);
							$("#contact_lname"+i).val(bankCtct.lastName);
							$("#contact_crole"+i).val(bankCtct.contactRole);
							$("#contact_fphoneno"+i).val(bankCtct.contactNo);
							$("#contact_femail"+i).val(bankCtct.emailId);
							$("#contact_faddr1"+i).val(bankCtct.address1);
							$("#contact_faddr2"+i).val(bankCtct.address2);
							$("#contact_fstate"+i).val(bankCtct.state);
							$("#contact_fcity"+i).val(bankCtct.city);
							$("#contact_fzcode"+i).val(bankCtct.zipcode);
							
						}

					}
					
					setBankLicense(response);
					}
						
								   
				},

			 error:function(response,statusTxt,error){
			 
				/* alert("Failed to add record :"+response.responseJSON); */
			 }
			});
	});

	function submitUpdateBankForm()
	{
		
		//validfn();
		
		//alert($("#submit_form").serialize());

		var numItems = $('div.bankaccount-single').size();

		//alert(numItems);

		var url_data = ($("#submit_form").serialize());//.replace(/+/g,'%20');//replace(/ /g, '+');
	
		var result = queryStringToJSON(url_data);

		var contacts = [];

		contacts[0]=
		{
			contactId: $("#bnk-contactid").val(),
			branchId:  $("#bnk-branchid").val(),
			firstName: $("#bnksetup-fname").val(),
			lastName: $("#bnksetup-lname").val(),
			contactNo: $("#bnksetup-ctct").val(),
			emailId: $("#bnksetup-email").val(),
			address1: $("#bnksetup-addr1").val(),
			address2: $("#bnksetup-addr2").val(),
			isdefault:'1',
			state: $("#bnksetup-state").val(),
			city: $("#bnksetup-cty").val(),
			zipcode:$("#bnksetup-zcode").val(),
			isactive:'1'
		};

       for(var k=1;k<=numItems;k++)
		{
		   
			contacts[k]=
			{
				contactId:result["contact_id"+k],
				branchId:  $("#bnk-branchid").val(),
				firstName: result["contact_fname"+k],
				lastName: result["contact_lname"+k],
				contactRole: result["contact_crole"+k],
				contactNo: result["contact_fphoneno"+k],
				emailId: result["contact_femail"+k],
				address1: result["contact_faddr1"+k],
				address2: result["contact_faddr2"+k],
				isdefault:'0',
				state: result["contact_fstate"+k],
				city: result["contact_fcity"+k],
				zipcode:result["contact_fzcode"+k],
				isactive:'1'

			}
		}

		/* var bplist=''; */
		var ftypelist='';
		/* 
		$('#businessprocchk input:checked').each(function() {
				bplist= bplist+$(this).val()+",";
			}); */
		$('#fileTypeChk input:checked').each(function() {
				ftypelist= ftypelist+$(this).val()+",";
			});
		//alert("bplist:"+bplist+" ftypelist:"+ftypelist);

	    //alert("$(bnk-id).val() "+$("#bnk-id").val());

		/* bplist = bplist.substring(0,bplist.length-1); */

		ftypelist = ftypelist.substring(0,ftypelist.length-1);

		
			var bankInfo = {
					siteId:"12",
					bankName: $("#bnk-name").val(),
					bankCode: $("#bnk-ownid").val(),
					locationId:$("#bnk-locid").val(),
					branchLocation:$("#bnk-branchloc").val(),
					rtNo: $("#bnk-rtnum").val(),
					ddano: $("#bnk-dda").val(),
					bankId: $("#bnk-id").val(),
					branchId: $("#bnk-branchid").val(),
					bankLogo: $("#bnk-bankLogo").val(),
					bankStatus:$("#bnk-isactive").val(),
					
					contacts:contacts,
					isdefault:'1',
					state: $("#bnksetup-state").val(),
					city: $("#bnksetup-cty").val(),
					zipcode:$("#bnksetup-zcode").val(),
					website:$("#bnksetup-web").val(),
					ftypelist: ftypelist,
					/* bplist:bplist, */
					isactive:'1'
					
				}
		//alert("BankInfo :"+JSON.stringify(bankInfo));
		$.ajax({
					method: 'post',
					url: '../bank/update',
					data: JSON.stringify(bankInfo),
					contentType: 'application/json',
					dataType:'JSON',
					crossDomain:'true',
					success: function (response) {
						 swal({title: "Done",text: "Bank Updated Successfully",type: "success"},function(){window.location = "manage-bank.jsp";});
							// swal('Done!','Added','success'),function()
							// {
								 // window.location = 'manage-bank.jsp';
							// }
							//location.href='manage-bank.jsp';
					},

				 error:function(response,statusTxt,error){
				 
					/* alert("Failed to add record :"+response.responseJSON); */
				 }
				});
		
	}
	function setBankLicense(response)
	{
		//var buspros= response.masterData.busPros;
		var bankInfo= response.bank;
		/* if(bankInfo.bplist!=undefined)
		{
			var bpList = bankInfo.bplist.split(",");
			for(j=0;j<buspros.length;j++)
			{
				var value = $.inArray(buspros[j].businessProcessId+"", bpList);
				//alert("value: "+value+" buspros[j].businessProcessId :"+buspros[j].businessProcessId +" bpList: "+bpList);
				var chk = '';

				if(value!=-1)
				{
					chk = '<label><input type="checkbox" required class="icheck" name="bcheckbox" checked="true" data-checkbox="icheckbox_square-grey" value='+buspros[j].businessProcessId+'>'+buspros[j].processName+'</label>';

				}
				else
				{
					chk = '<label><input type="checkbox" required name="bcheckbox" class="icheck" data-checkbox="icheckbox_square-grey" value='+buspros[j].businessProcessId+'>'+buspros[j].processName+'</label>';

				}
				
				//alert("chk:"+chk+" val "+value);

				$('#businessprocchk').append(chk).find('.icheck').iCheck({checkboxClass: 'icheckbox_square-grey'});
			}
		}
		else
		{
			for(j=0;j<buspros.length;j++)
			{
				var chk = '<label><input type="checkbox" class="icheck"  data-checkbox="icheckbox_square-grey" value='+buspros[j].businessProcessId+'>'+buspros[j].processName+'</label>';

				$('#businessprocchk').append(chk).find('.icheck').iCheck({checkboxClass: 'icheckbox_square-grey'});
			}
		} */
		var fileTypes= response.masterData.fileTypes;
		if(bankInfo.ftypelist!=undefined)
		{
			var ftypeList = bankInfo.ftypelist.split(",");
			for(j=0;j<fileTypes.length;j++)
			{
				//alert("In loop");
				//alert("buspros[j].processName"+JSON.stringify(buspros[j]));
				var value = $.inArray(fileTypes[j].filetypeid+"", ftypeList);

				var chk ="";

				if(value!=-1)
				{
					chk = '<label><input type="checkbox" name="flcheckbox" checked="true" class="icheck" required value='+fileTypes[j].filetypeid+'>'+fileTypes[j].filetypename+'</label>';
				}
				else
				{
					chk = '<label><input type="checkbox" name="flcheckbox" class="icheck" required value='+fileTypes[j].filetypeid+'>'+fileTypes[j].filetypename+'</label>';
				}

				$('#fileTypeChk').append(chk).find('.icheck').iCheck({checkboxClass: 'icheckbox_square-grey'});
				
			}
		}
		else
		{
			for(j=0;j<fileTypes.length;j++)
			{
				var chk = '<label><input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey" value='+fileTypes[j].filetypeid+'>'+fileTypes[j].filetypename+'</label>';

				$('#fileTypeChk').append(chk).find('.icheck').iCheck({checkboxClass: 'icheckbox_square-grey'});
			}
				
			
		}
		
	}

  var queryStringToJSON = function (url) {
    if (url === '')
        return '';
    var pairs = (url || location.search).slice(1).split('&');
    var result = {};
    for (var idx in pairs) {
        var pair = pairs[idx].split('=');
        if (!!pair[0])
            result[pair[0].toLowerCase()] = decodeURIComponent(pair[1] || '');
    }
    return result;
}
	function deleteContact(contactId)
	{
		//alert("contactId : "+contactId);

		$.ajax({
				method: 'get',
				url: '../contact/delete/'+contactId,
				contentType: 'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success: function (response) {
											   
				},

			 error:function(response,statusTxt,error){
			 
			/* 	alert("Failed to delete contact :"+response.responseJSON); */
			 }
			});
	}


	
	
	</script>
<!-- END BODY -->
</html>