<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">

<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="col-md-6 col-sm-6 col-xs-12 page-title">
				<h1>Add module</h1>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<a href="manage-modulemenu.jsp" class="btn darkgrey m-t-15">
								<i class="fa fa-reply"></i> 
							Back 
						</a>
					</div>
			<!-- END PAGE TITLE -->
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<!--<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					 Edit Module menu
				</li>
			</ul>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<a href="manage-modulemenu.jsp" class="btn blue">
								<i class="fa fa-reply"></i> 
							Back 
						</a>
					</div>
			</div>
			</div>-->
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">				
					
					<div class="portlet light col-md-12 col-sm-12 col-xs-12">
						<div class="portlet-body">
							<div class="col-md-12 col-md-offset-0">
							<form id ="addModuleForm" class="form-horizontal " role="form" id="jvalidate">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-2 control-label">
										Module Name :</label>
										<div class="col-md-10">
											<input type="hidden" id= "modulemenuName-id" name="modulemenuName-id" class="form-control" placeholder="" />
											<input type="text" id= "modulemenuName" name="modulemenuName" class="form-control" placeholder="" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label">
										&nbsp; </label>
										<div class="col-md-10 nopad perm-wraper">
											<div class="col-md-12 m-b-15 box-header with-border">		
												<h3 class="medium-title">Module List</h3>
											</div>
											
											<div id="dv_dyn_module_list">
												
												
												
											</div>
											
											
										<!--	<div class="col-md-3">
												<label>
													<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey"  id="menu_md" name="menu_md"  /> Modules 
												</label>
											</div>
											<div class="col-md-3">
												<label>
													<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey" id="module_name-id" name="module_name-id" value="module_name-id" />
												</label>
											</div>
									
																
											<div class="col-md-3">
												<label>
													<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey" /> Admin Users  
												</label>
											</div>
											<div class="col-md-3">
												<label>
													<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey" /> Modules 
												</label>
											</div>
											<div class="col-md-3">
												<label>
													<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey" /> Roles  
												</label>
											</div>
											<div class="col-md-3">
												<label>
													<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey" /> Admin Users  
												</label>
											</div>
											<div class="col-md-3">
												<label>
													<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey" /> Modules 
												</label>
											</div>
											<div class="col-md-3">
												<label>
													<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey"  /> Roles  
												</label>
											</div>
											<div class="col-md-3">
												<label>
													<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey" /> Admin Users  
												</label>
											</div>
											<div class="col-md-3">
												<label>
													<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey" /> Modules 
												</label>
											</div>
											<div class="col-md-3">
												<label>
													<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey" /> Roles  
												</label>
											</div>
											<div class="col-md-3">
												<label>
													<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey" /> Admin Users  
												</label>
											</div>
											<div class="col-md-3">
												<label>
													<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey" /> Modules 
												</label>
											</div>
											<div class="col-md-3">
												<label>
													<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey" /> Roles  
												</label>
											</div>
											<div class="col-md-3">
												<label>
													<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey" /> Admin Users  
												</label>
											</div>
										</div> 
										-->
										
									</div>
									<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2  col-xs-12 m-t-15">
										<div class="form-actions noborder ">
							<input type="submit" class="btn blue" value="Update" onClick="moduleMenuUpdate()"/>
							&nbsp;				
							<button type="button" class="btn default" onclick="cancl()" >Cancel</button>
											
											
											<!-- <input type="button" class="btn blue" value="Add" onClick="validfn();menumoduleCreate()"/> -->
											
										</div>
									</div>
								</div>
							</form>
						</div>
							
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
	<!-- END PAGE CONTAINER -->
	<!-- BEGIN FOOTER -->

	<jsp:include page="includes/footer.jsp" />
	<jsp:include page="includes/footer-js.jsp" />

<script>
	
//menuid fetching	
var menuid_t = <%= request.getParameter("menuid") %>;
	
$(function(){
	
	$.ajax({		
			method:'get',
			url:'../menu/show/'+menuid_t,
			contentType:'application/json',
			dataType:'JSON',
			crossDomain:'true',
			
			success:function(response){
				
			var moduleInfo = JSON.stringify(response);
				
				$.each(JSON.parse(moduleInfo), function(idx, obj) {				
				 $("#modulemenuName").val(obj.menuName);
				 $("#modulemenuName-id").val(obj.menuid);
				});
			}
		});		
	
	//closed
		
		
// here listing all modulemenus 
// here getting "recordsTotal" of modulemenus size			
/**	
	$.ajax({		
			method:'get',
			
			url:'../menumodule/list',
			
			contentType:'application/json',
			dataType:'JSON',
			crossDomain:'true',
			
			success:function(response){
			var moduleInfo = JSON.stringify(response.module);	
				
				
				alert(moduleInfo);	
					$.each(JSON.parse(moduleInfo), function(idx, obj) {
						
		//			alert("obj.recordsTotal "+obj);	
		//		alert("MMMMMMMMMMMMMMMMMM obj.recordsTotal "+idx[4].recordsTotal);	
						
				});
			}
	});	
	
	
	
	*/
	
	

// dynamic modules append
// getting all active modules

	$.ajax({		
			method:'get',			
	//		url:'../module/show/list',			
			url:'../module/show_active_module_list',
			contentType:'application/json',
			dataType:'JSON',
			crossDomain:'true',			
			success:function(response){
			var mod_Info = JSON.stringify(response.module);	
					//alert("Entered list of all module"+mod_Info);	
					var module_list='';
					$.each(JSON.parse(mod_Info), function(idx, obj) {
					//alert("All module"+obj);
					//alert("active module list"+obj.modulename);						
					//alert(obj.moduleid);
					
					module_list +='<div class="col-sm-3"> <label> <input class="cls_module_'+obj.moduleid+'" type="checkbox" name="modulecheck_list[]" value="'+obj.moduleid+'" />'+obj.modulename+'</label> </div>'; 					
										
				});
				
				$("#dv_dyn_module_list").html(module_list);
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-blue',
					radioClass: 'iradio_square-blue',
					increaseArea: '20%' // optional
				});
				
				
			}
	});	//closed



//  getting menuid based  getting the all selected modulemenus


	$.ajax({		
			method:'get',			
	//		url:'../module/show/list',			
			url:'../menumodule/getModuleMenu_based_menuid/'+menuid_t,
			contentType:'application/json',
			dataType:'JSON',
			crossDomain:'true',			
			success:function(response){
			var mod_Info = JSON.stringify(response.moduleMenuBean);	
				//	alert("Entered list of all module"+mod_Info);	
					$.each(JSON.parse(mod_Info), function(idx, obj) {
					//alert("All module"+obj);
					//alert("active module list : obj.modulename : "+obj.moduleName +"obj.menuName : "+obj.menuName);	

					//alert(obj.moduleid);
					
					$(".cls_module_"+obj.moduleid).prop('checked', 'checked');
					$(".cls_module_"+obj.moduleid).parent('div').addClass('checked'); 
					
				});
			}
		});	
		
	
		
		//closed
	
// creating or adding modulemenuid
// single moduleid menuid adding single time ..... create loop is required

			
	});		
			


	
//update modulemenu
function moduleMenuUpdate(){

	var selecteditems = [];	
	
	//to get all the selected checkbox values
	
	//$('#checkboxes input:checked[name="c_n[]"]')
    //.map(function () { return $(this).val(); }).get()

    
    
    
    
    //$("#Div").find("input:checked").each(function (i, ob) { 
  //  selecteditems.push($(ob).val());
	//});
	
	var tasks= new Array();
$('input:checked[name^="modulecheck_list[]"]').each(function() 
{
tasks.push($(this).val());
});

    
    
    
//	if($("#jvalidate").valid())
	//{
	
/**			var chk_module_status =0;
			var chk_module_isreport ="0";
			
			if($("#module-status").is(':checked')){
				chk_module_status=1;
			}
			if($("#module-isreport").is(':checked')){ 
				chk_module_isreport="1";
			}	*/	

	var moduleInfo={	//modulemenuid : modulemenuid_t,
						//moduleid : $("#module-id").val(),
						menuid:menuid_t,	
						moduleIdArray:tasks
	}

			
	
	$.ajax({
		
		
			method:'post',
			url: '../modulemenu/update',
			data: JSON.stringify(moduleInfo),
			contentType: 'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success: function (response) {
				
					
					location.href='manage-modulemenu.jsp';
													   
			},error:function(response,statusTxt,error){
		 
			/* 	alert("Failed to add record :"+response.responseJSON); */
		    }
		});
	//} 
	
}
	

	function cancl(){
		location.href = 'manage-modulemenu.jsp';
	}
	
	
	
</script>
</body>
<!-- END BODY -->
</html>