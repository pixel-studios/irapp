<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container-fluid">
			<div class="fixedbtn-container">
				
				
				<!--<a href="edit-role.jsp" class="btn orange m-t-15"><i class="fa fa-plus"></i> Add Role</a>-->
				
				
			</div>
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container-fluid">
		
			<div class="row margin-top-10">
				<div class="col-md-12">
					
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
								<div class="portlet light col-md-12 col-sm-12 col-xs-12">
									<div class="pagestick-title">
									<span>Logs List</span>
									</div>
									<div class="portlet-body logtable-wraper">
										<div class="table-scrollable table-scrollable-borderless">
											<table class="table" id="fileLog">
											<col width="12%"></col>
											<col width="12%"></col>
											<col width="12%"></col>
											<col width="12%"></col>
											<col width="12%"></col>
											
											<thead>
													<tr class="uppercase">
														<th>File Name</th>
														<th>File Type</th> 
														<th>Processed Date & Time</th>
														<th>Status</th>
														<th>Note</th>
													</tr>
												</thead>
												<tbody>
													
												</tbody>
												
											</table>
										</div>
									</div>
								</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />

<script>
$(document).ready(function(){
	
	
	

	$.fn.dataTable.ext.errMode = 'throw'
            $("#fileLog").dataTable({searching: false,lengthChange:false,pageLength:50,
			ajax :{
			"url":"../file-audit/showAll",
			"dataSrc":"fileLogList"
			//	$("#bank-name").append(customerBean[0].defaultBankName);
		  		},
                //"aaData": arreglo,
                "ordering": true,
                "aoColumns": [
                
                            
							{ "mData":"fileName"},
							{ "mData":"fileType"},
							{ "mData":"processedDateTime"},
							{
								mData: null, render: function ( mData, type, row ) {
						 
							if(mData.isProcessed=='1')
							return '<button type="button" class="btn green btn-xs">Completed</button>';
							else
							return '<button type="button" class="btn red btn-xs">Error</button>';
							}}, 
						
							{ "mData":"description"},
							
                            ],
                "bDestroy": true
            }).fnDraw();
});

</script>
</body>
</html>