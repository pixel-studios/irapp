<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		
		<jsp:include page="includes/header-top.jsp" />
		
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		
		<jsp:include page="includes/header-menu.jsp" />
		
		<!-- END HEADER MENU -->
	</div>
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Add module</h1>
			</div>
			<!-- END PAGE TITLE -->
			
		</div>
	</div>
	
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					 Add module
				</li>
			</ul>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<a href="manage-module.jsp" class="btn blue">
								<i class="fa fa-reply"></i> 
							Back 
						</a>
					</div>
			</div>
			</div>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">				
					
					<div class="portlet light col-md-12 col-sm-12 col-xs-12">
						<div class="portlet-body">
							<div class="col-md-10 col-md-offset-1">
							<form id="jvalidate" class="form-horizontal " role="form" method="post" ><!--addModuleForm-->
								<div class="form-body">
								
									<input type="hidden" class="form-control" id="user-id" name="user-id" />
									<input type="hidden" class="form-control" id="module-id" name="module-id" />									
					
						<!--		<input type="hidden" class="form-control" id="creteddate-id" name="creteddate-id" />
							    	<input type="hidden" class="form-control" id="modidfieddate-id" name="modidfieddate-id" />  -->
																	
									<div class="form-group">
										<label class="col-md-4 control-label">
										Module Name : <sup class="red-req">*</sup></label>
										<div class="col-md-8">
											<input type="text" id= "module-name" name="module-name" class="form-control" placeholder="" required />
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label">
										Description :</label>
										<div class="col-md-8">
											<textarea class="form-control"  id="module-description" name="module-description" placeholder="" ></textarea>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-4 control-label">
										Module Path : <sup class="red-req">*</sup></label>
										<div class="col-md-8">
											<input type="text"  id ="module-path" name="module-path" class="form-control" placeholder="" required />
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-4 control-label">
										Module Icon Path :</label>
										<div class="col-md-8">
											<input type="text"  id = "module-icon" name="module-icon" class="form-control" placeholder="" />
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-4 control-label">
											Status
										</label>
										<div class="col-md-8">
											<label class="menu-checkbox">
												<input type="checkbox" class="icheck cls_status"  id="module-status" name="module-status" data-checkbox="icheckbox_square-grey">  
											</label>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-4 control-label">
											Is report
										</label>
										<div class="col-md-8">
											<label class="menu-checkbox">
												<input type="checkbox" class="icheck cls_isreport"  id="module-isreport" name="module-isreport" data-checkbox="icheckbox_square-grey">  
											</label>
										</div>
									</div>
									
									<div class="col-md-8 col-md-offset-4 col-sm-8 col-sm-offset-4  col-xs-12">
										<div class="form-actions noborder ">
										
																			
											<input id="dv_save" type="button" class="btn blue" value="Submit" onClick="moduleCreate();"/>
											
											
											<input id="dv_update" style="display:none;" type="button" class="btn blue" value="Update" onClick="moduleUpdate();"/>
											
											<button type="button"  onclick="cancelModule()" class="btn default" >Cancel</button>
											
										</div>
									</div>
								</div>
							</form>
						</div>
							
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />

<script>
//create module functionality

var user_id = <%= session.getAttribute("userid") %>;

	function moduleCreate(){
		
	
			
		if($("#jvalidate").valid())	
		{
				
			
			var chk_module_status =0;
			var chk_isreport ="0";
			
			if($("#module-status").is(':checked')){
				chk_module_status=1;
			}
			if($("#module-isreport").is(':checked')){
				chk_isreport="1";
			}		

			/*alert("status : "+$("#module-status").val());
			alert("ismodule	: "+$("#module-isreport").val());
			alert("ismodule	: "+$("#module-path").val());
			alert("ismodule	: "+$("#module-description").val());
			alert("modulepath :"+ $("#module-icon").val());
			*/
			
			var module_Info ={						
								modulename : $("#module-name").val(),
								modulepath : $("#module-path").val(),
								description : $("#module-description").val(),							 
								moduleicon : $("#module-icon").val(),								
								isreport : chk_isreport,
								isactive : chk_module_status,
								userid : user_id
				}
					//alert(JSON.stringify(module_Info));
					
					$.ajax({
					method: 'post',
					url: '../module/create/nodup',
					data: JSON.stringify(module_Info),
					contentType: 'application/json',
					dataType: 'JSON',
					crossDomain: 'true',
					success: function (response) {
						 swal({title: "Done",text: "Module Created",type: "success"},function(){window.location = "manage-module.jsp";});
							//alert("Added Module");
							//location.href='manage-module.jsp';
					
					},
					
				 error:function(response,statusTxt,error){
					//alert(Duplicated);
					/* alert("Failed to add record : "+response.responseJSON); */

					location.href='manage-module.jsp';
				
				 }
				});
				}
				
		}  

	
var moduleid = <%= request.getParameter("moduleid") %>;
//show update module functionality		

$(function(){		
	if(moduleid !=null){
		
	
		//update
		
		
	/**		var chk_module_status =0;
			var chk_isreport =0;
			
			if($("#module-status").is(':checked')){
				chk_module_status=1;
			}
			if($("#module-isreport").is(':checked')){
				chk_isreport=1;
			}
	*/	
		
		$.ajax({
		
			method:'get',
			url:'../module/show/'+moduleid,
			contentType:'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success:function(response){

			var moduleInfo = JSON.stringify(response.module);
			//alert(moduleInfo);
			
			$.each(JSON.parse(moduleInfo), function(idx, obj) {
				
			$("#module-id").val(obj.moduleid);
			$("#module-name").val(obj.modulename);
			$("#module-path").val(obj.modulepath);
	//		$("#menu-sortingorder").val(obj.sortingorder);
			$("#module-icon").val(obj.moduleicon);
			$("#module-isreport").val(obj.isreport);
			$("#creteddate-id").val(obj.createddate);
	//		$("#modidfieddate-id").val(obj.modifieddate);		
			$("#module-description").val(obj.description);
					
			if(obj.isactive == "1"){			
				$(".cls_status").prop('checked', 'checked');
				$(".cls_status").parent('div').addClass('checked');
			}else{
				$(".cls_status").prop('checked', false);
				$(".cls_status").parent('div').removeClass('checked');  
			}	

			if(obj.isreport == "1"){			
				$(".cls_isreport").prop('checked', 'checked');
				$(".cls_isreport").parent('div').addClass('checked');
			}else{
				$(".cls_isreport").prop('checked', false);
				$(".cls_isreport").parent('div').removeClass('checked');  
			}
			
	
			});
			
			},
			error:function(response,statusTxt,error){
				 
				/* 	alert("Failed to add record :"+response.responseJSON); */
				 }
		});
		
	$("#dv_save").hide();
	$("#dv_update").show();
	}	 
});


//update
function moduleUpdate(){
	
	if($("#jvalidate").valid())	
	{
			var chk_module_status =0;
			var chk_module_isreport ="0";
			
			if($("#module-status").is(':checked')){
				chk_module_status=1;
			}
			if($("#module-isreport").is(':checked')){ 
				chk_module_isreport="1";
			}	

	var moduleInfo ={
						moduleid : $("#module-id").val(),
						modulename : $("#module-name").val(),
						modulepath : $("#module-path").val(),
						
						isreport	: chk_module_isreport,
					//	createddate : $("#menu-createddate").val(),
			
						moduleicon : $("#module-icon").val(),
						
						description : $("#module-description").val(),						
						isactive : chk_module_status,
						userid : user_id
	}
		//alert(JSON.stringify(moduleInfo));
	
	$.ajax({
			method:'post',
			url: '../module/update',
			data: JSON.stringify(moduleInfo),
			contentType: 'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success: function (response) {
				
				 swal({title: "Done",text: "Module Updated",type: "success"},function(){window.location = "manage-module.jsp";});
				//	alert("Updated Module");
				//	location.href='manage-module.jsp';
													   
			},

		 error:function(response,statusTxt,error){
		 
		/* 	alert("Failed to add record :"+response.responseJSON); */
		 }
		});
	}
}

function cancelModule(){
	
	//alert("Cancelled Page");
	location.href='manage-module.jsp';
}



</script>
</body>
<!-- END BODY -->
</html>