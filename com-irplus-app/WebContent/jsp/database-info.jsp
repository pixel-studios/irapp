<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp" />
<!-- BEGIN BODY -->
<body class="page-md login">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<div class="logo">
	<a href="index.html">
	<img src="../resources/assets/admin/layout3/img/logo-default.png" alt="logo"/>
	</a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
	<!-- BEGIN LOGIN FORM -->
	<form class="db-form" action="index.html" method="post">
		<h3 class="form-title">Database Info</h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			<span>
			Enter any username and password. </span>
		</div>
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label">Hostname / IP</label>
			<input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" name="username"/>
		</div>
		<div class="form-group">
			<label class="control-label">Username</label>
			<input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" name="uname"/>
		</div>
		<div class="form-group">
			<label class="control-label">Password</label>
			<input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" name="password"/>
		</div>
		<div class="form-actions text-center">
			<button type="submit" class="btn btn-success uppercase">Continue</button>
		</div>
		
		
	</form>
	<!-- END LOGIN FORM -->
</div>
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer-js.jsp" />
</body>
<!-- END BODY -->
</html>