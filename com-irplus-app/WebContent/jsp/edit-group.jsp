<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			
			
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
				
				
					<div class="portlet light col-md-12 col-sm-12 col-xs-12">
						<div class="pagestick-title">
								<span>Update Group & Map</span>
							</div>
						<div class="portlet-body">
							<form role="form" class="form-horizontal" name="submit_form" id="submit_form" method="POST">
					 	<div class="col-md-12 col-sm-12 col-xs-12">
															<label class="col-md-5 control-label">
															Group Name :</label>
															<div class="col-md-7">
																<div class="input-group">
																	<input type="text" class="form-control" placeholder="" id="groupName" name="groupName">
																</div>
															</div>
															<!-- <div class="col-md-2" style="float:right;">
															<a class="btn btn-info " onclick="addeditcustmapgroup();"><i class="fa fa-plus"></i> Add</a>
														</div> -->
														</div> 
								<div class="col-md-12 col-sm-12 col-xs-12 nopad">
								<div class="table-scrollable table-scrollable-borderless managebank-cont custmap-tble">
								<table class="table table-hover table-bordered tble-custmapgroup">
								<thead>
								<tr class="uppercase">
								
									<th> 
										 Bank Name
									</th>
									<th> 
										Customer ID
									</th>
									<th>
										Options
									</th>
								</tr>
								</thead>
								
								<tbody>
								</tbody>
					
								</table>
							</div>
								</div>
									
								<div class="col-md-12 col-sm-12 col-xs-12 nopad">
								<div class="form-actions noborder text-right">
									<button type="button"  onclick="cancelGroup()" class="btn default">Cancel</button>
									<!-- <button type="button" class="btn blue">Create</button> -->
							
							<a href="javascript:submitUpdateCustgroupForm()" class="btn blue button-submit">Update</a>
														
															
							
								</div>
								</div>
							</form>
							
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>



<script>
var customerGrpId = <%= request.getParameter("customerGrpId") %>;
var userId=<%=session.getAttribute("userid")%>;
var isactive="2";
	$(document).ready(function() {  
		
		 $.ajax({
				method: 'get',
				url: '../custmrGrp/customergroup/get/'+customerGrpId,
				contentType: 'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success: function (response) {
					
					$("#groupName").val(response.custmrGrpngMngmntInfoList[0].customerGroupName);
	
		 				
		 					var newDivgroup = $('<tr class="newdivgroup-'+0+'"><td width="20%"><select class="form-control select2 groupbranchname groupbranchname'+0+'"  id='+0+' name="groupbranchname'+0+'" onchange="changeFunc.call(this);"></select></td><td width="20%"><select class="form-control select2 groupcustomername groupcustomername'+0+'"  id='+0+' name="groupcustomername'+0+'" onchange="changeCustFunc.call(this);"></select></td><td width="20%"><a class="btn btn-info " onclick="addeditcustmapgroup();"><i class="fa fa-plus"></i> Add</a></td></tr>');
		 					$('table.tble-custmapgroup tbody').append(newDivgroup);	
		 					
		 					for(g=1;g<response.custmrGrpngMngmntInfoList.length;g++){
			 					var newDivgroup = $('<tr class="newdivgroup-'+g+'"><td width="20%"><select class="form-control select2 groupbranchname groupbranchname'+g+'"  id='+g+' name="groupbranchname'+g+'" onchange="changeFunc.call(this);"></select></td><td width="20%"><select class="form-control select2 groupcustomername groupcustomername'+g+'"  id='+g+' name="groupcustomername'+g+'" onchange="changeCustFunc.call(this);"></select></td><td width="20%"><a class="btn btn-info deltebnk" onclick="deletecustgroup('+g+');"><i class="fa fa-trash"></i> Delete</a></td></tr>');
			 					$('table.tble-custmapgroup tbody').append(newDivgroup);	
			 					}			
		 			  m=response.custmrGrpngMngmntInfoList.length;
		 
			 		
				for(i=0;i<response.custmrGrpngMngmntInfoList.length;i++){

		 				 		var branch="<OPTION VALUE="+response.custmrGrpngMngmntInfoList[i].bankwiseId+">"+response.custmrGrpngMngmntInfoList[i].BankName+"</OPTION>"; 
		 				 				
		 				$(".groupbranchname"+i).html(branch);	
		 				
					} 
			 		getappendbranch(m);
				for(i=0;i<response.custmrGrpngMngmntInfoList.length;i++){

	 				 		var customer="<OPTION VALUE="+response.custmrGrpngMngmntInfoList[i].customerid+">"+response.custmrGrpngMngmntInfoList[i].companyName+"</OPTION>"; 
	 				 				
	 				$(".groupcustomername"+i).html(customer);	

				} 

									   
				},

			 error:function(response,statusTxt,error){
			 
			/* 	alert("Failed to add record :"+response.responseJSON); */
			 }
				
				
			});
		 

			
				function getappendbranch(m)
				{
				
					var row=m;
			
				$.ajax({
					
					method:'get',
					
					
					url:'../Customermapping/branch/'+siteId,
					contentType:'application/json',
					dataType:'JSON',
					crossDomain:'true',
					success:function(response)
	{
		 				var htmlbankid="";
		 				var htmlbankname= "<option value='' disabled>Select Bank</option>";
		 			
		 				var branch =response.dashboardResponse.bankInfo.bankbranchinfo;
		 				
							
		 				for(var i=0;i<response.dashboardResponse.bankInfo.length;i++){
		 			
		 					htmlbankname +="<OPTION VALUE="+response.dashboardResponse.bankInfo[i].bankId+" >"+response.dashboardResponse.bankInfo[i].bankName+"</OPTION>"; 

		 				}
		 				for(var j=0;j<row;j++){
		 				$(".groupbranchname"+j).append(htmlbankname);
		 				
		 				
		 				}
			},
			error:function(response,statusTxt,error){
				 
				/* alert("Failed to add record :"+response.responseJSON); */
			 }
			});		
				}
				
		
			
	

	});

	
	
	function addeditcustmapgroup()
	{

		m++;
	
		var newDivgroup = $('<tr class="newdivgroup-'+m+'"><td width="20%"><select class="form-control select2 groupbranchname groupbranchname'+m+'"  id='+m+' name="groupbranchname'+m+'" onchange="appendchangeFunc.call(this);"></select></td><td width="20%"><select class="form-control select2 groupcustomername groupcustomername'+m+'"  id='+m+' name="groupcustomername'+m+'" onchange="changeCustFunc.call(this);"></select></td><td width="20%"><a class="btn btn-info deltebnk" onclick="deltecustbnkgroup('+m+');"><i class="fa fa-trash"></i> Delete</a></td></tr>');
		$('table.tble-custmapgroup tbody').append(newDivgroup);
		var siteId=<%=session.getAttribute("siteId")%>;
		$.ajax({		
			method:'get',

			url:'../Customermapping/branch/'+siteId,
			contentType:'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success:function(response)
	{
				var htmlcustname= "<option  value='' selected disabled>Select Customer</option>";
				
		var appendbankname="<OPTION value='' selected disabled >Select Bank</OPTION>";
					
					for(var i=0;i<response.dashboardResponse.bankInfo.length;i++){

						appendbankname +="<OPTION VALUE="+response.dashboardResponse.bankInfo[i].bankId+" >"+response.dashboardResponse.bankInfo[i].bankName+"</OPTION>"; 


						
			$(".groupbranchname"+m).html(appendbankname);			
					} 
					$(".groupcustomername"+m).html(htmlcustname);	
	},
	error:function(response,statusTxt,error){
		 
		/* alert("Failed to add record :"+response.responseJSON); */
	 }
	 


		});
	}

	function appendchangeFunc() {
		
		  var rowid  = $(this).attr("id");
		 
		      var branchId  = $(".groupbranchname"+rowid).val();
		  
		      $.ajax({
		        	 method: 'get',
		             url: '../customer/getcustomer/'+branchId,
		             contentType: 'application/json',
					 dataType:'JSON',
					 crossDomain:'true',
		             success: function(response) {
		               
		             
		
		                	var appendcustomername= "<option value='' selected disabled>Select Customer</option>";
							
							for (var i = 0; i < response.bankBranches.bankCustomerList.length; i++){
							
							  appendcustomername += "<option value='" + response.bankBranches.bankCustomerList[i].customerid + "'>" + response.bankBranches.bankCustomerList[i].companyName + "</option>";
							}
			
							$(".groupcustomername"+rowid).html(appendcustomername);
			
		                	
		             },
		             error: function (e) {
		                
		                 console.log(e.message);
		               /*   alert("failed to get bank customer"); */
		             }
		         });
	}
		 
	function submitUpdateCustgroupForm()


	{
	
		var count=0;
		var numItems = $('.tble-custmapgroup tbody tr').size();

		var url_data = ($("#submit_form").serialize());
		
		var result = queryStringToJSON(url_data);
		var mappingInfo="";
		var Editcustdetails = [];
		
		var grpName=  $("#groupName").val();
		var newcount=0;
		var countval=0;


		if(grpName!=''){
			$(".groupcustomername").each(function()
					
					{
								  var k  = $(this).attr("id");
			
					           Editcustdetails[countval]= 
									{
										 customerId:result["groupcustomername"+k],
										 bankId:result["groupbranchname"+k],
											userId:userId,
										isactive:'1'
											
									} 
					           if(result["groupbranchname"+k]==null && result["groupcustomername"+k]==null)
					        	 {
					        	   newcount++;
					        	 swal("Select bank and customer")
					        	 }
					           
					         if(result["groupcustomername"+k]==null && result["groupbranchname"+k]!=null)
					        	 {
					        newcount++;
					        	 swal("Map the Customer")
					        
					        	 }
					
					           countval++;
					});

		if(newcount==0){
		
			InactivateGroup(customerGrpId,isactive);
 		$(".groupcustomername").each(function()
			
{

			  var k  = $(this).attr("id");

           Editcustdetails[count]= 
	
				{
					 customerId:result["groupcustomername"+k],
					 bankId:result["groupbranchname"+k],
						userId:userId,
					isactive:'1'
						
				} 

			 mappingInfo={

					customerGroupName : $("#groupName").val(),
					customergroupingDetails:Editcustdetails,
			 userid:userId,
				isactive:'1'
		}
          
		count++;
});

 		 $.ajax({
 			method:'post',
 			url:'../custmrGrp/customergroup/update/customer',
 			data:JSON.stringify(mappingInfo),
 			contentType:'application/json',
 			dataType:'JSON',
 			crossDomain:'true',
 			success: function(response) 
 			{
 				swal({title:"Done",text:"CustomerGroup Updated Successfully",type:"success"},function(){window.location="manage-custgroup.jsp";});
 			},
 			
 		 error:function(response,statusTxt,error){
 		 
 		/* 	alert("Failed to add record :"+response.responseJSON.errMsgs); */
 		 }
 	}); 
 		
		}


		}
		if(grpName==''){
			 swal("Pls fill the group name")
			
		}

		}
				
	
	function InactivateGroup(customerGrpId,isactive)
	{
	
		var delcustdetails = [];
		
	 delcustdetails[0]=
			{
				
			 customerGrpId:customerGrpId,
				isactive:isactive
					
			}
			 
		var groupInfo = {
				
					customerGrpId:customerGrpId,
					isactive:isactive,
					customergroupingDetails:delcustdetails
			}
		
				$.ajax({
					method: 'post',
					url: '../custmrGrp/CustGrp/delete',
					data: JSON.stringify(groupInfo),
					contentType: 'application/json',
					dataType:'JSON',
					crossDomain:'true',
					success: function (response) {
					
					},

				 error:function(response,statusTxt,error){
				 
				/* 	alert("Failed to add record :"+response.responseJSON); */
				 }
				});
		

	}
	
	 var queryStringToJSON = function (url) {
		    if (url === '')
		        return '';
		    var pairs = (url || location.search).slice(1).split('&');
		    var result = {};
		    for (var idx in pairs) {
		        var pair = pairs[idx].split('=');
		        if (!!pair[0])
		            result[pair[0].toLowerCase()] = decodeURIComponent(pair[1] || '');
		    }
		    return result;
		}
	 
	 function cancelGroup(){
		 	
		 
		 	location.href='manage-custgroup.jsp';
		 }
		
		function changeFunc() {
	  
			  var rowid  = $(this).attr("id");
			
			      var branchId  = $(".groupbranchname"+rowid).val();
			  
			      $.ajax({
			        	 method: 'get',
			             url: '../customer/getcustomer/'+branchId,
			             contentType: 'application/json',
						 dataType:'JSON',
						 crossDomain:'true',
			             success: function(response) {
			               
			             
			
			                	var appendcustomername= "<option value='' selected disabled>Select Customer</option>";
								
								for (var i = 0; i < response.bankBranches.bankCustomerList.length; i++){
								
								  appendcustomername += "<option value='" + response.bankBranches.bankCustomerList[i].customerid + "'>" + response.bankBranches.bankCustomerList[i].companyName + "</option>";
								}
				
								$(".groupcustomername"+rowid).html(appendcustomername);
				
			                	
			             },
			             error: function (e) {
			                
			                 console.log(e.message);
			              /*    alert("failed to get bank customer"); */
			             }
			         });
		}
		
		
		 
		function changeCustFunc() {
			 
			  var crowid  = $(this).attr("id");
			
			      var branchIdvalue  = $(".groupcustomername"+crowid).val();
			    
				$(".groupcustomername").each(function()
						{
										  var rowval  = $(this).attr("id");
	
				if(rowval!==crowid){
				
				var custid=$(".groupcustomername"+rowval).val();
					
				if(branchIdvalue==custid)
					{
					swal("customer already mapped")
						
					  
					  var branchId  = $(".groupbranchname"+crowid).val();
				      $.ajax({
				        	 method: 'get',
				             url: '../customer/getcustomer/'+branchId,
				             contentType: 'application/json',
							 dataType:'JSON',
							 crossDomain:'true',
						       success: function(response) {
					            
						   		
				                	var listItems= "<option value='' selected disabled>Select Customer</option>";
									for (var i = 0; i < response.bankBranches.bankCustomerList.length; i++){

									  listItems += "<option value='" + response.bankBranches.bankCustomerList[i].customerid + "'>" + response.bankBranches.bankCustomerList[i].companyName + "</option>";
									}
									$(".groupcustomername"+crowid).html('');
									$(".groupcustomername"+crowid).html(listItems);
				                	
				             },
				             error: function (e) {
				              
				                 console.log(e.message);
				               /*   alert("failed to get bank customer"); */
				             }
				         });
				
					}
				
				
			}	
						 });
		}
	</script>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>