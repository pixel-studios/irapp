<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp" />
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
	<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<div class="page-title">
				<h1> File Upload</h1>
			</div>
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<ul class="page-breadcrumb breadcrumb">
							<li>
								<a href="#">Home</a><i class="fa fa-circle"></i>
							</li>
							
							<li class="active">
								 File Upload
							</li>
						</ul>
					</div>	
				</div>
			</div>	
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					
					<div class="profile-content fileupload-container">
						<div class="row">
							<div class="portlet light col-md-12 col-sm-12 col-xs-12">
								<div class="portlet-body">
									<div id="license-validate" class="col-md-8 col-md-offset-2 ">
										<form role="form" id="fileForm" class="form-horizontal" action="javascript:uploadLicense()">
											<div class="col-md-12 col-sm-12 col-xs-12">
											<div class="medium-title">Validate your IR plus License</div>
											<input id="bank-img-upload" name="licFile" type="file" multiple class="file-loading">
											</div>
										</form>
									</div>
									<div class="col-md-6 col-md-offset-3">
										<div id="license-success" class="success-bg" style="display:none">
											<i class="fa fa-check-circle"></i>
											<h1>License is valid, Now you can login & access with IR+</h1>
											<a href="login.jsp">Login</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<script>

var isLicFile = function(name) {
    return name.match(/lic$/i)
};
    

	function uploadLicense()
	{
		//alert("uploading License file");
		//var file =  $("#bank-img-upload");
	    var filename = $.trim($("#bank-img-upload").val());
		//;/alert("filename: "+JSON.stringify(new FormData(document.getElementById("fileForm"))));
        
        if (!(isLicFile(filename) )) {
            alert('Please browse a lic file to upload ...');
            return;
        }
        
        $.ajax({
            url: '../license/upload',
            type: "POST",
            data: new FormData(document.getElementById("fileForm")),
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function(data) {
              //alert("file uploaded");
			  $("#license-success").show();
			   $("#license-validate").hide();
			},
           error:function(response,statusTxt,error){
				 
			//alert("Failed to add record :"+response.responseJSON.statusMsg);

			swal({title: "Oops...",text: response.responseJSON.statusMsg,type: "error"},function(){});
	
				 }
          });
		}
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>