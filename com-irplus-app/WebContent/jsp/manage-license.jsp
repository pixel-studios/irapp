<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>IR plus | Manage license</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="../resources/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../resources/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->

<link href="../resources/assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>
<link href="../resources/assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="../resources/assets/global/css/components-md.css" id="style_components" rel="stylesheet" type="text/css">
<link href="../resources/assets/global/css/plugins-md.css" rel="stylesheet" type="text/css">
<link href="../resources/assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="../resources/assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="../resources/assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-menu-fixed" class to set the mega menu fixed  -->
<!-- DOC: Apply "page-header-top-fixed" class to set the top menu fixed  -->
<body class="page-md">
<!-- BEGIN HEADER -->
<div class="page-header">
	<!-- BEGIN HEADER TOP -->
	<div class="page-header-top">
		<div class="container">
			<!-- BEGIN LOGO -->
			<div class="page-logo">
				<a href="index.jsp">
				<span>
				<img src="../resources/assets/admin/layout3/img/logo-default.png" alt="logo" class="logo-default"></span>
				<span class="logo-sub">AB Processing Center</span>
				</a>
			</div>
			<!-- END LOGO -->
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="menu-toggler"></a>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN TOP NAVIGATION MENU -->
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
					<!-- BEGIN NOTIFICATION DROPDOWN -->
					<li class="dropdown dropdown-extended dropdown-dark dropdown-notification" id="header_notification_bar">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<i class="fa fa-bell-o"></i>
						<span class="badge badge-default">7</span>
						</a>
						<ul class="dropdown-menu">
							
							<li>
								<ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
								
									<li>
										<a href="javascript:;">
										
										<span class="details">
										<span class="label label-sm label-icon label-warning">
										<i class="fa fa-bell-o"></i>
										</span>
										Notification 1 </span>
										</a>
									</li>
									<li>
										<a href="javascript:;">
										
										<span class="details">
										<span class="label label-sm label-icon label-warning">
										<i class="fa fa-bell-o"></i>
										</span>
										Notification 2 </span>
										</a>
									</li>
									<li>
										<a href="javascript:;">
										
										<span class="details">
										<span class="label label-sm label-icon label-warning">
										<i class="fa fa-bell-o"></i>
										</span>
										Notification 3 </span>
										</a>
									</li>
									<li>
										<a href="javascript:;">
										
										<span class="details">
										<span class="label label-sm label-icon label-warning">
										<i class="fa fa-bell-o"></i>
										</span>
										Notification 4 </span>
										</a>
									</li>
									<li>
										<a href="javascript:;">
																				<span class="details">
										<span class="label label-sm label-icon label-warning">
										<i class="fa fa-bell-o"></i>
										</span>
										Notification 5 </span>
										</a>
									</li>
									
									
									
								</ul>
							</li>
						</ul>
					</li>
					<!-- END NOTIFICATION DROPDOWN -->
					
					<li class="droddown dropdown-separator">
						<span class="separator"></span>
					</li>
					
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<li class="dropdown dropdown-user dropdown-dark">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<img alt="" class="img-circle" src="../resources/assets/admin/layout3/img/avatar9.jpg">
						<span class="username username-hide-mobile">Nick</span>
						</a>
						<ul class="dropdown-menu dropdown-menu-default">
							<li>
								<a href="profile.jsp">
								<i class="icon-user"></i> My Profile </a>
							</li>
							
							<li>
								<a href="login.jsp">
								<i class="icon-key"></i> Log Out </a>
							</li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
			</div>
			<!-- END TOP NAVIGATION MENU -->
		</div>
	</div>
	<!-- END HEADER TOP -->
	<!-- BEGIN HEADER MENU -->
	<div class="page-header-menu">
		<div class="container">
			
			<div class="hor-menu ">
				<ul class="nav navbar-nav">
					<li>
						<a href="index.jsp">Dashboard</a>
					</li>
					
				<li class="menu-dropdown classic-menu-dropdown ">
						<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
						Admin 
						</a>
						<ul class="dropdown-menu pull-left">
							<li>
								<a href="role-setup.jsp">
								
								Add Role </a>
								
							</li>
							<li>
								<a href="manage-roleperm.jsp">
								
								Manage Role </a>
								
							</li>
							<li>
								<a href="profile.jsp">
								
								Add User </a>
								
							</li>
							
							<li>
								<a href="manage-user.jsp">
								
								Manage Users </a>
								
							</li>
							
						</ul>
					</li>
					
					<li class="menu-dropdown classic-menu-dropdown ">
						<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
						Bank 
						</a>
						<ul class="dropdown-menu pull-left">
							<li>
								<a href="bank-setup.jsp">
								
								Bank Setup </a>
								
							</li>
							<li>
								<a href="manage-bank.jsp">
								
								Manage Bank </a>
								
							</li>
							<li>
								<a href="manage-bank-location.jsp">
								
								Manage Location </a>
								
							</li>
							
						</ul>
					</li>
					<li class="menu-dropdown classic-menu-dropdown ">
						<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
						Customer 
						</a>
						<ul class="dropdown-menu pull-left">
							<li>
								<a href="customer-setup.jsp">
								
								Create Customer </a>
								
							</li>
							<li>
								<a href="manage-customer.jsp">
								
								Manage Customer </a>
								
							</li>
							<li>
								<a href="create-group.jsp">
								
								Create Customer Group</a>
								
							</li>
							<li>
								<a href="manage-custgroup.jsp">
								
								Manage Customer Group</a>
								
							</li>
							
						</ul>
					</li>
					<li class="menu-dropdown classic-menu-dropdown ">
						<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
						Settings 
						</a>
						<ul class="dropdown-menu pull-left">
							<li>
								<a href="license-setup.jsp">
								
								License Setup </a>
								
							</li>
							<li>
								<a href="file-upload.jsp">
								
								Validate License</a>
								
							</li>
						
							
						</ul>
					</li>
					<li>
						<a href="#">File Process</a>
					</li>
					
					
					
					
				</ul>
			</div>
			<!-- END MEGA MENU -->
		</div>
	</div>
	<!-- END HEADER MENU -->
</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Manage License</h1>
			</div>
			<!-- END PAGE TITLE -->
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			
			<!-- BEGIN PAGE BREADCRUMB -->
			<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				
				<li class="active">
					Manage License
				</li>
				
			</ul>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 text-right">
				<a href="license-setup.jsp" class="btn blue"><i class="fa fa-plus"></i> ADD NEW SITE LICENSE</a>
			</div>
			</div>
			</div>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
							
					<!-- BEGIN SAMPLE FORM PORTLET-->
					
					<div class="portlet light col-md-12 col-sm-12 col-xs-12">
						<div class="portlet-body form">
							<form role="form">
							
								<div class="medium-title">Search By</div>
									
									<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="form-group form-md-line-input form-md-floating-label">
										<input type="text" class="form-control" id="form_control_1">
										<label for="form_control_1">Site Name</label>
									</div>
									</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="form-group form-md-line-input form-md-floating-label has-success">
										<input type="text" class="form-control" id="form_control_1">
										<label for="form_control_1">contact Name</label>
									</div>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="form-group form-md-line-input form-md-floating-label has-info">
										<select class="form-control" id="form_control_1">
											<option value=""></option>
											<option value="1">Option 1</option>
											<option value="2">Option 2</option>
											<option value="3">Option 3</option>
											<option value="4">Option 4</option>
										</select>
										<label for="form_control_1">License Type</label>
									</div>
									</div>
									
									<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="form-group form-md-line-input form-md-floating-label has-info">
										<select class="form-control" id="form_control_1">
											<option value=""></option>
											<option value="1">Option 1</option>
											<option value="2">Option 2</option>
											<option value="3">Option 3</option>
											<option value="4">Option 4</option>
										</select>
										<label for="form_control_1">File Processing</label>
									</div>
									</div>
									
									<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="form-group form-md-line-input form-md-floating-label has-info">
										<select class="form-control" id="form_control_1">
											<option value=""></option>
											<option value="1">Option 1</option>
											<option value="2">Option 2</option>
											<option value="3">Option 3</option>
											<option value="4">Option 4</option>
										</select>
										<label for="form_control_1">Business processing</label>
									</div>
									</div>
								
													
								
								<div class="col-md-4 col-sm-6 col-xs-12 ">
								<div class="form-group form-md-line-input">
									<button type="button" class="btn default">Reset</button>
									<button type="button" class="btn blue">Search</button>
								</div>
								</div>
							</form>
							
							<div class="col-md-12 col-sm-12 col-xs-12 nopad managebank-cont">
							<div class="table-scrollable table-scrollable-borderless">
								<table class="table table-hover table-light table-bordered">
								<thead>
								<tr class="uppercase">
									
									<th>
										 Serial No
									</th>
									
									<th>
										 Site ID
									</th>
									<th>
										 Site Name
									</th>
									<th>
										 Contact Name
									</th>
									<th>
										 License Type
									</th>
									<th>
										 Created Date
									</th>
									<th>
										 Created By
									</th>
									<th>
										Generate License
									</th>
									<th>
									    Edit / Download 
									</th>
									
								</tr>
								</thead>
								<tbody>
								<tr>
									
									<td>
										 #IR0001
									</td>
									<td>
										IRP0001
										
									</td>
									<td>
										AB Processing Center
									</td>
									<td>
										Steve
									</td>
									<td>
										Perpetual License
									</td>
									<td>
										12/21/2016
									</td>
									<td>
										Admin User
									</td>
									<td>
										<a href="javascript:;" class="primary-link"> Generate</a>
									</td>
									<td>
										<button type="button" class="btn grey btn-xs">Edit</button>
										<button type="button" class="btn yellow btn-xs">Download</button>
									</td>
								</tr>
								<tr>
									
									<td>
										 #IR0001
									</td>
									<td>
										IRP0001
										
									</td>
									<td>
										AB Processing Center
									</td>
									<td>
										Steve
									</td>
									<td>
										Perpetual License
									</td>
									<td>
										12/21/2016
									</td>
									<td>
										Admin User
									</td>
									<td>
										<a href="javascript:;" class="primary-link"> Generate</a>
									</td>
									<td>
										<button type="button" class="btn grey btn-xs">Edit</button>
										<button type="button" class="btn yellow btn-xs">Download</button>
									</td>
								</tr>
								<tr>
									
									<td>
										 #IR0001
									</td>
									<td>
										IRP0001
										
									</td>
									<td>
										AB Processing Center
									</td>
									<td>
										Steve
									</td>
									<td>
										Perpetual License
									</td>
									<td>
										12/21/2016
									</td>
									<td>
										Admin User
									</td>
									<td>
										<a href="javascript:;" class="primary-link"> Generate</a>
									</td>
									<td>
										<button type="button" class="btn grey btn-xs">Edit</button>
										<button type="button" class="btn yellow btn-xs">Download</button>
									</td>
								</tr>
								
								
								</tbody>
								</table>
							</div>
							</div>
						</div>
					</div>
					
					
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="container">
		 Image Scan &copy; 2017 ALL RIGHTS RESERVED.
	</div>
</div>
<div class="scroll-to-top">
	<i class="icon-arrow-up"></i>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../resources/assets/global/plugins/respond.min.js"></script>
<script src="../resources/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="../resources/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../resources/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../resources/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../resources/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../resources/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../resources/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../resources/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../resources/assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="../resources/assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="../resources/assets/admin/pages/scripts/profile.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {       
   	// initiate layout and plugins
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	Demo.init(); // init demo features\
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>