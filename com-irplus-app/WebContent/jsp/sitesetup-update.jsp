<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<jsp:include page="includes/style.jsp" />
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-menu-fixed" class to set the mega menu fixed  -->
<!-- DOC: Apply "page-header-top-fixed" class to set the top menu fixed  -->
<script src="../resources/assets/global/jquery.min.js" type="text/javascript"></script>
<script src="../resources/assets/global/jquery.validate.js"></script>
<body class="page-md">
<!-- BEGIN HEADER -->
<div class="page-header">
	<!-- BEGIN HEADER TOP -->
	<jsp:include page="includes/header-top.jsp" />
	<!-- END HEADER TOP -->
	<!-- BEGIN HEADER MENU -->
	<jsp:include page="includes/header-menu.jsp" />
	<!-- END HEADER MENU -->
</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="col-md-6 col-sm-6 col-xs-12 text-right ">
				<div class="fixedbtn-container">
					<a href="javascript:void(0);" class="btn green-meadow serach m-t-15 fileupload-trigger">
						<i class="fa fa-upload"></i> Upload
					</a>
							
				</div>
			</div>
			<!-- END PAGE TITLE -->

		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container-fluid">
			<!-- BEGIN PAGE BREADCRUMB -->
			<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">

			</div>
			</div>
			</div>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="bnk-setup-container">
				<div class="col-md-12 nopad">
					<!-- BEGIN PROFILE SIDEBAR -->
					<div class="profile-sidebar">
						<!-- PORTLET MAIN -->
						<div class="portlet light profile-sidebar-portlet">
							<!-- SIDEBAR USERPIC -->
							<div class="profile-userpic">
								<span class="userpic-inner">
									<img id="bnk-bnklogo" src="../resources/images/sample-logo.jpg" class="img-responsive" alt="">
									<a class="edit-pic bank-img" data-toggle="modal" data-target="#imgupload">
										<i class="fa fa-edit"></i>
									</a>
								</span>
							</div>
							<!-- END SIDEBAR USERPIC -->
							<!-- SIDEBAR USER TITLE -->
							<div class="profile-usertitle">
								<div id="siteName" class="profile-usertitle-name">

								</div>

							</div>
							<!-- END SIDEBAR USER TITLE -->
							<!-- SIDEBAR MENU -->
							<div class="profile-usermenu">
								<ul class="nav">

								</ul>
							</div>
							<!-- END MENU -->
						</div>
						<!-- END PORTLET MAIN -->

					</div>
					<!-- END BEGIN PROFILE SIDEBAR -->
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content boxwith-sidebar">
								
							<div class="col-md-12 nopad searchmain-wraper" id="upload-wraper">
					<div class="portlet light">
					
						<span class="searchclose"> &times; </span>
						<form role="form" id="fileForm" class="" action="javascript:uploadLicense()">
											<div class="col-md-12 col-sm-12 col-xs-12 nopad">
														<div class="medium-title">UPGRADE LICENSE</div>
											
														
															<fieldset class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
																<label for="form_control_1">License File<sup class="red-req"></sup></label>
																	<input id="bank-img-upload" name="licFile" type="file" multiple class="file-loading">

															</fieldset>
											
											
											
											
											</div>
										<!--	<div class="col-md-6 col-md-offset-3">
											<div id="license-success" class="success-bg" style="display:none">
											<i class="fa fa-check-circle"></i>
											<h1>License is valid, Now you can login & access with IR+</h1>
											<a href="login.jsp">Login</a>
										</div>
									</div>-->
											
										</form>
							 </div>
							</div>
						<div class="row">
							<div class="col-md-12">
								<div class="portlet light col-md-12 col-sm-12 col-xs-12" id="form_wizard_1">
								<div class="pagestick-title">
								<span>Site Setup</span>
							</div>
									<div class="portlet-body form">
									<form action="#" class="form-horizontal" name="submit_form" id="submit_form" method="POST">
										<div class="form-wizard">
											<div class="form-body">
												<ul class="nav nav-pills nav-justified steps">
													<li>
														<a href="#tab1" data-toggle="tab" class="step">
														<span class="number">
														1 </span>
														<span class="desc">
														<i class="fa fa-check"></i> Site Setup </span>
														</a>
													</li>
													<li>
														<a href="#tab2" data-toggle="tab" class="step">
														<span class="number">
														2 </span>
														<span class="desc">
														<i class="fa fa-check"></i> Manage File Path </span>
														</a>
													</li>
													<li>
														<a href="#tab3" data-toggle="tab" class="step active">
														<span class="number">
														3 </span>
														<span class="desc">
														<i class="fa fa-check"></i> Manage Database Info </span>
														</a>
													</li>
												</ul>
												<div id="bar" class="progress progress-striped" role="progressbar">
														<div class="progress-bar progress-bar-success"></div>
												</div>
												<div class="tab-content">
													<div class="tab-pane active" id="tab1">


															<input type="hidden" class="form-control" id="bnk-bankLogo" name="bnk-bankLogo"/>
															<input type="hidden" class="form-control" id="site-id" name="site-id"/>
															<fieldset class="portlet-title col-md-12 col-sm-12 col-xs-12 ">
															<span class="caption-subject font-green-sharp bold uppercase">Site Information</span>
														</fieldset>

															<fieldset class="col-md-6 col-sm-6 col-xs-12">
																<label>Site Name <sup class="red-req">*</sup></label>
																<input type="text" class="form-control" id="site-fname"  name="site-fname"required />
																</fieldset>
																<fieldset class="col-md-6 col-sm-6 col-xs-12">
																	<label>Site Code <i class="fa fa-info-circle info-head"></i></label>
																	<input type="text" class="form-control" id="site-code" name="site-code" readonly/>
																</fieldset>
																<fieldset class="col-md-6 col-sm-6 col-xs-12">
																	<label>Contact Person</label>
																	<input type="text" class="form-control" id="ct-prsn" name="ct-prsn" />
																</fieldset>
																<fieldset class="col-md-6 col-sm-6 col-xs-12">
																	<label>Contact No.</label>
																	<input type="text" class="form-control" id="sitect-no" name="sitect-no" />
																</fieldset>
																<fieldset class="col-md-12 col-sm-12 col-xs-12">
																	<label>Address</label>
																	<textarea class="form-control" id="address"></textarea>
																</fieldset>

																<div class="clearfix"></div>
																<div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 ">
																<hr></hr>
															</div>

																<fieldset class="portlet-title col-md-12 col-sm-12 col-xs-12 ">
															<span class="caption-subject font-green-sharp bold uppercase">License Information</span>
														</fieldset>
														<div class="row">
														<div class="col-md-3">
															<div class="col-md-12 nopad-right">
															<img src="../resources/images/license-icon.jpg" class="img-responsive" alt="ir-license">
															</div>
														</div>
														<div class="col-md-9 nopad">
														
														<fieldset class="col-md-12 col-sm-12 col-xs-12">
															<label class="col-md-4 control-label text-left nopad">Status<span class="pull-right">:</span> </label>
															<div class="col-md-6">
															<label class="p-t-8"><span id="licenseStatus"></span></label>
															</div>
														</fieldset>
														<fieldset class="col-md-12 col-sm-12 col-xs-12">
															<label class="col-md-4 control-label text-left nopad">Type <span class="pull-right">:</span> </label>
															<div class="col-md-6">
															<label class="p-t-8">
																<span id="licenseType"></span>
															</label>
															
														</div>
														</fieldset>
														<fieldset class="col-md-12 col-sm-12 col-xs-12">
															<label class="col-md-4 control-label text-left nopad">Activation Date<span class="pull-right">:</span> </label>
															<div class="col-md-6">
															<label class="p-t-8">
																<span id="fromDate"></span>
															</label>
															</div>
														</fieldset>
														<div id="valid" style="display:none;">
														<fieldset class="col-md-12 col-sm-12 col-xs-12">
															<label class="col-md-4 control-label text-left nopad">Expiration Date<span class="pull-right">:</span> </label>
															<div class="col-md-6">
															<label class="p-t-8">
															<span id="toDate"></span>
															
															</label>
															</div>
														</fieldset>
														<fieldset class="col-md-12 col-sm-12 col-xs-12">
															<label class="col-md-4 control-label text-left nopad">Remaining Days<span class="pull-right">:</span> </label>
															<div class="col-md-6">
															<label class="p-t-8">
															<span id="remainingDays"> </span> Days
															</label>
															</div>
														</fieldset>
														</div>
																<fieldset class="col-md-12 col-sm-12 col-xs-12">
																	<label class="col-md-4 control-label text-left nopad">
																		<b>Licensing Functioning</b>
																	</label>
																</fieldset>
																
																<fieldset class="col-md-12 col-sm-12 col-xs-12">
																	<label class="col-md-4 control-label text-left nopad"> File Processing <span class="pull-right">:</span> </label>
																<div class="col-md-8">
																	<input type="hidden" id="hidSelectedOptions" />
																	
																		<select class="form-control multiselect" multiple="multiple" id="fileTypeChk">
																		</select>
																		</div>
																</fieldset>
																
																<!-- <fieldset class="col-md-12 col-sm-12 col-xs-12">
																	<label class="col-md-4 control-label text-left nopad"> File Processing <span class="pull-right">:</span> </label>
																		<div class="col-md-8">
																			<div class="input-group">
																				<div id="fileTypeChk" class="icheck-list">
																			</div>
																		</div>
																		</div> -->

																	<!-- <label class="col-md-4 control-label text-left nopad">Business Processing <span class="pull-right">:</span></label>
																		<div class="col-md-8">

																				<div class="input-group">
																				<div id="businessprocchk" class="icheck-list">
																				</div>
																			</div>

																		</div> -->
																<!-- </fieldset> -->
															</div>
															</div>
													</div>
													<div class="tab-pane" id="tab2">

																	<fieldset class="col-md-12 col-sm-12 col-xs-12">
																		<label class="col-sm-4">IR Plus Root Folder</label>
																		<div class="col-sm-8">
																				<div class="input-group">
																			 <span class="input-group-addon" ><i class="fa fa-file"></i></span>
																				<input type="text" class="form-control" id="rootFolderPath" >
																			</div><!-- /input-group -->
																		</div>
																	</fieldset>
																	<!-- <fieldset class="col-md-12 col-sm-12 col-xs-12">
																		<label class="col-sm-4">Ephesoft XML & TIFF Process</label>
																		<div class="col-sm-8">
																				<div class="input-group">
																			 <span class="input-group-addon" ><i class="fa fa-file"></i></span>
																				<input type="text" class="form-control" id="ephesoftFolderPath" >
																			</div>/input-group
																		</div>
																	</fieldset>
																	<fieldset class="col-md-12 col-sm-12 col-xs-12">
																		<label class="col-sm-4">ImageHawk XML Process</label>
																		<div class="col-sm-8">
																		<div class="input-group">
																			 <span class="input-group-addon" ><i class="fa fa-file"></i></span>
																				<input type="text" class="form-control"  id="imagehawkXmlFolderPath">
																			</div>/input-group
																		</div>
																	</fieldset>
																	<fieldset class="col-md-12 col-sm-12 col-xs-12">
																		<label class="col-sm-4">Output Transmission</label>
																		<div class="col-sm-8">
																				<div class="input-group">
																			 <span class="input-group-addon" ><i class="fa fa-file"></i></span>
																				<input type="text" class="form-control" id="outputTransmissionFolderPath">
																			</div>/input-group
																		</div>
																	</fieldset>
																	<fieldset class="col-md-12 col-sm-12 col-xs-12">
																		<label class="col-sm-4">Archive Process Files</label>
																		<div class="col-sm-8">
																			<div class="input-group">
																			 <span class="input-group-addon" ><i class="fa fa-file"></i></span>
																				<input type="text" class="form-control" id="archiveProcessFilePath" >
																			</div>/input-group
																		</div>
																	</fieldset>
 -->

													</div>
													<div class="tab-pane" id="tab3">

															<fieldset class="col-md-12 col-sm-12 col-xs-12">
																<label class="col-sm-4 ">Host / IP</label>
																<div class="col-sm-7">
																	<input type="text"  class="form-control" id="host-name"/>
																</div>
															</fieldset>
															<fieldset class="col-md-12 col-sm-12 col-xs-12">
																<label class="col-sm-4">User Name</label>
																<div class="col-sm-7">
																	<input type="text"  class="form-control" id="host-username"/>
																</div>
															</fieldset>
															<fieldset class="col-md-12 col-sm-12 col-xs-12">
																<label class="col-sm-4">Password</label>
																<div class="col-sm-7">
																	<input type="password"  class="form-control" id="host-password"/>
																</div>
															</fieldset>


													</div>
												</div>
												<div class="form-actions">
													<div class="">
														<div class="col-md-offset-3 col-md-9 text-right">
															<a href="javascript:;" class="btn default button-previous">
															<i class="m-icon-swapleft"></i> Back </a>
															<a href="javascript:;" class="btn blue button-next">
															Continue <i class="m-icon-swapright m-icon-white"></i>
															</a>

															<a href="javascript:updateSite()" class="btn green button-submit">
															Update <i class="m-icon-swapright m-icon-white"></i>
															</a>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />

<script src="../resources/assets/irplus/js/siteLogo.js"></script>
<!-- END JAVASCRIPTS -->
<div class="modal fade" id="imgupload" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Choose a file to upload</h4>
        </div>
        <div class="modal-body">
			<form role="form" id="siteForm" class="form-horizontal" action="javascript:uploadSiteLogo()">
			<div class="col-md-12 col-sm-12 col-xs-12">
					<input id="site-img-upload" name="siteLogo" type="file" class="file-loading">
			</div>
			</form>
        </div>
        <div class="modal-footer">

        </div>
      </div>

    </div>
  </div>


<script>


var siteId=<%=session.getAttribute("siteId")%>;

$(document).ready(function(){

	$.ajax({
		method:'get',
		url:'../site/getSiteInfo/'+siteId,
		contentType:'application/json',
		dataType:'JSON',
		crossDomain:'true',
		success: function(response)
		{

			var site = response.siteInfo;

			$("#site-fname").val(site.siteName);
			$("#site-code").val(site.siteCode);
			$("#ct-prsn").val(site.contactPerson);
			$("#sitect-no").val(site.contactNo);

			$("#address").val(site.address1);
			$("#site-id").val(site.siteId);
			$("#siteName").append(site.siteName);
			$("#rootFolderPath").val(site.rootFolderPath);
			$("#ephesoftFolderPath").val(site.emphesoftXmlFolderPath);
			$("#imagehawkXmlFolderPath").val(site.imagehawkXmlFolderPath);
			$("#outputTransmissionFolderPath").val(site.outputTransmissionFolderPath);
			$("#archiveProcessFilePath").val(site.archiveProcessFilePath);


			if(site.licenseInfo.licType=='Validity'){
				$("#licenseType").append(site.licenseInfo.licType);
				$("#val-period-div").fadeIn();
				$("#fromDate").append(site.licenseInfo.createddate);
				$("#toDate").append(site.licenseInfo.validTo);
				$("#remainingDays").append(site.licenseInfo.remainingDays);
				if(site.licenseInfo.isactive=='1'){
					$("#licenseStatus").append("Active");
				}else{
					$("#licenseStatus").append("In Active");
				}
				$("#valid").fadeIn();
				
				}else{
				 $("#licenseType").append(site.licenseInfo.licType);
				 $("#fromDate").append(site.licenseInfo.createddate);
				 if(site.licenseInfo.isactive=='1'){
						$("#licenseStatus").append("Active");
					}else{
						$("#licenseStatus").append("In Active");
					}
				 
				 $("#valid").fadeOut();
				}
			if(site.siteLogo==''){
				$("#bnk-bnklogo").attr('src',"../resources/assets/admin/layout3/img/avatar9.jpg");
			}else{
				$("#bnk-bnklogo").attr('src',"http://tcmuonline.com:8080/fileimages/Images/SiteLogo/"+site.siteLogo);
			}

			$("#bnk-bankLogo").val(site.siteLogo);


			for(j=0;j<site.licenseInfo.businessProcessInfo.length;j++)
			{
				var chk = '<label><input type="checkbox"  class="icheck" name="bcheckbox" checked="true" data-checkbox="icheckbox_square-grey" value='+site.licenseInfo.businessProcessInfo[j].businessProcessId+'>'+site.licenseInfo.businessProcessInfo[j].processName+'</label>';


				$('#businessprocchk').append(chk).find('.icheck').iCheck({checkboxClass: 'icheckbox_square-grey'});
			}
			
			
			
			
			

 /* 	for(j=0;j<site.licenseInfo.fileTypeInfo.length;j++)
			{
				var chk = '<label><input type="checkbox"  class="icheck" name="bcheckbox" checked="true" data-checkbox="icheckbox_square-grey" value='+site.licenseInfo.fileTypeInfo[j].filetypeiId+'>'+site.licenseInfo.fileTypeInfo[j].filetypename+'</label>';


				$('#fileTypeChk').append(chk).find('.icheck').iCheck({checkboxClass:'icheckbox_square-grey'});
			}
 */ 
			
			
			
			var listBnkItems;
			//var jsonData = bankInfo;
			for (var j = 0; j <site.licenseInfo.fileTypeInfo.length; j++)
			{
			//alert(response.roleBeans[i].rolename)
			  listBnkItems += "<option value='"+site.licenseInfo.fileTypeInfo[j].filetypeid+"'>"+site.licenseInfo.fileTypeInfo[j].filetypename+"</option>";
			  
			  
			}
			
			
			$("#fileTypeChk").html('');
			$("#fileTypeChk").html(listBnkItems);
			$("#fileTypeChk").multiselect({
				 nonSelectedText:'select Process'
			});
			
			//$('.multiselect-container.dropdown-menu').slimScroll();
			$(".multiselect-container.dropdown-menu").mCustomScrollbar({
			theme:"dark"
			});
			
			
			
			var i=0;
			var otherBanks=[];
		var count=0;
			for(i;i<site.licenseInfo.fileTypeInfo.length;i++){
					
					otherBanks[count]=site.licenseInfo.fileTypeInfo[i].filetypeid;
					count++;
				
				
			}
			
			$("#hidSelectedOptions").val(otherBanks);
			
			
			 $("#fileTypeChk").multiselect({
			       selectedText: "# of # selected"
			    });
			    var hidValue = $("#hidSelectedOptions").val();
			     //alert(hidValue);
			    var selectedOptions = hidValue.split(",");
			    for(var i in selectedOptions) {
			        var optionVal = selectedOptions[i];
			        $("#fileTypeChk").find("option[value="+optionVal+"]").prop("selected","selected");
			    }
			    $("#fileTypeChk").multiselect('refresh');

			
			
			
			
			$("#host-name").val(site.dbInfo.hostName);
			$("#host-username").val(site.dbInfo.username);
			$("#host-password").val(site.dbInfo.password);

			re_intialize();


		},

	 error:function(response,statusTxt,error){

		/* alert("Failed to add record :"+response.responseJSON); */
	 }
	});



});



function updateSite(){
	
	

	var dbInfo={

			siteid:siteId,
			hostName:$("#host-name").val(),
			username:$("#host-username").val(),
			password:$("#host-password").val()

	}
	var siteInfo={

			siteId:siteId,
			siteName:$("#site-fname").val(),
			contactPerson:$("#ct-prsn").val(),
			contactNo:$("#sitect-no").val(),
			address1:$("#address").val(),
			siteLogo:$("#bnk-bankLogo").val(),
			rootFolderPath:$("#rootFolderPath").val(),
			emphesoftXmlFolderPath:$("#ephesoftFolderPath").val(),
			imagehawkXmlFolderPath:$("#imagehawkXmlFolderPath").val(),
			outputTransmissionFolderPath:$("#outputTransmissionFolderPath").val(),
			archiveProcessFilePath:$("#archiveProcessFilePath").val(),


			dbInfo:dbInfo
	}

				$.ajax({
					method: 'post',
					url: '../site/update',
					data: JSON.stringify(siteInfo),
					contentType: 'application/json',
					dataType:'JSON',
					crossDomain:'true',
					success: function (response) {
						 swal({title: "Done",text: "Updated",type: "success"},function(){window.location = "sitesetup-update.jsp";});
							// swal('Done!','Added','success'),function()
							// {
								 // window.location = 'manage-bank.jsp';
							// }
							//location.href='manage-bank.jsp';
					},

				 error:function(response,statusTxt,error){

				/* 	alert("Failed to add record :"+response.responseJSON); */
				 }
				});
}






function re_intialize(){

	$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue',
		increaseArea: '20%' // optional
	});


}


var isLicFile = function(name) {
    return name.match(/lic$/i)
};
    

	function uploadLicense()
	{
		//alert("uploading License file");
		//var file =  $("#bank-img-upload");
	    var filename = $.trim($("#bank-img-upload").val());
		//;/alert("filename: "+JSON.stringify(new FormData(document.getElementById("fileForm"))));
        
        if (!(isLicFile(filename) )) {
            alert('Please browse a lic file to upload ...');
            return;
        }
        
        $.ajax({
            url: '../license/upload',
            type: "POST",
            data: new FormData(document.getElementById("fileForm")),
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function(data) {
               swal({title: "Done",text: "License Upgraded",type: "success"},function(){window.location = "sitesetup-update.jsp";});
			},
           error:function(response,statusTxt,error){
				 
			//alert("Failed to add record :"+response.responseJSON.statusMsg);

			swal({title: "Oops...",text: response.responseJSON.statusMsg,type: "error"},function(){});
	
				 }
          });
		}

</script>





<!-- END BODY -->
</html>
