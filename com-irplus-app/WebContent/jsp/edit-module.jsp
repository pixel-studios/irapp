<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Add module</h1>
			</div>
			<!-- END PAGE TITLE -->
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					 Add module
				</li>
			</ul>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<a href="manage-module.jsp" class="btn blue">
								<i class="fa fa-reply"></i> 
							Back 
						</a>
					</div>
			</div>
			</div>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">				
					
					<div class="portlet light col-md-12 col-sm-12 col-xs-12">
						<div class="portlet-body">
							<div class="col-md-10 col-md-offset-1">
							<form id ="addModuleForm" class="form-horizontal " role="form" >
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-4 control-label">
										Module Name :</label>
										<div class="col-md-8">
											<input type="text" id= "modulename" name="modulename" class="form-control" placeholder="" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label">
										Description :</label>
										<div class="col-md-8">
											<textarea class="form-control"  id="description" name="description" placeholder=""></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label">
										Module Path :</label>
										<div class="col-md-8">
											<input type="text"  id = "modulepath" name="modulepath" class="form-control" placeholder="" />
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-4 control-label">
											Status
										</label>
										<div class="col-md-8">
											<label class="menu-checkbox">
												<input type="checkbox" class="icheck"  id="status" name="status" data-checkbox="icheckbox_square-grey">  
											</label>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label">
											Report
										</label>
										<div class="col-md-8">
											<label class="menu-checkbox">
												<input type="checkbox" class="icheck"  id="isreport" name="isreport" data-checkbox="icheckbox_square-grey">  
											</label>
										</div>
									</div>
									<div class="col-md-8 col-md-offset-4 col-sm-8 col-sm-offset-4  col-xs-12">
										<div class="form-actions noborder ">
											<button type="button" class="btn default" >Cancel</button>
											<button type="button" class="btn blue" onClick="submitForm()">Submit</button>
										</div>
									</div>
								</div>
							</form>
						</div>
							
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />

<script>

$('#example-module').DataTable( {
        "ajax": "../module/show/list",
		"processing": true,
        //"serverSide": true,x
        "columns": [
            { "data": "moduleid" },
            { "data": "modulename" },
            { "data": "description" },
            { "data": "modulepath" },
		    { "data": "isactive" },
     		{ "data": "isreport" }
        ]
    } );
	function submitForm()
{
	$("#addModuleForm").validate();	
	var person = {
            modulename: $("#modulename").val(),
            description:$("#description").val(),
            modulepath:$("#modulepath").val(),
			isactive:$("#isactive").val(),
			isreport:$("#isreport").val()
        }

		//alert("data "+jQuery("#addModuleForm").serialize());

	

    $.ajax({
            method: 'post',
            url: '../module/create',
			data: JSON.stringify(person),
			contentType: 'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success: function (response) {
				
					
					location.href='manage-module.jsp';
					//jQuery("#manageModule").click();
							   
            },

		 error:function(response,statusTxt,error){
			/* alert("Failed to add record :"+response.responseJSON.statusMsg); */
		 }
        });
		
	
}
</script>
</body>
<!-- END BODY -->
</html>l>