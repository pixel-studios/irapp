<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1> View Customer Mapping</h1>
			</div>
			<!-- END PAGE TITLE -->
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<ul class="page-breadcrumb breadcrumb">
							<li>
								<a href="#">Home</a><i class="fa fa-circle"></i>
							</li>
							
							<li class="active">
								 View Customer Mapping
							</li>
						</ul>
					</div>	
					<div class="col-md-6 col-sm-6 col-xs-12 text-right">
						<a href="manage-custgroup.jsp" class="btn blue"><i class="fa fa-reply"></i> Back </a>
					</div>
				</div>
			</div>	
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
				
					
					<div class="portlet light col-md-12 col-sm-12 col-xs-12">
						<div class="portlet-body">
							<form role="form" class="form-horizontal">
								<div class="col-md-12 col-sm-12 col-xs-12">
															<label class="col-md-5 control-label">
															Group Name :</label>
															<div class="col-md-6">
																<div class="input-group">
																	<input type="text" class="form-control" placeholder="">
																</div>
															</div>
														</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="table-scrollable table-scrollable-borderless managebank-cont">
								<table class="table table-hover table-light table-bordered">
								<thead>
								<tr class="uppercase">
									<th colspan="2">
										 Company Detail
									</th>
									<th>
										 IR Plus Id
									</th>
									<th colspan="2">
										 Bank Detail
									</th>									
									<th>
										 BANK ID & LOCATION
									</th>
									
									<th>
										BANK CUSTOMER ID
									</th>
								</tr>
								</thead>
								<tbody>
								 
									<tr>
									<td class="fit">
										<img class="banklogo" src="../resources/assets/admin/layout3/img/IBM-logo-blue.png">
									</td>
									<td>
										<span class="bold">IBM</span>
									</td>
									<td>
										 IRPCID001
									</td>
									<td class="fit">
										<img class="banklogo" src="../resources/assets/admin/pages/img/banks/bankofamerica.jpg">
									</td>
									<td>
										<span class="bold">Bank Of America</span>
									</td>
									
									<td>
										IRPBID0001 BOA-LCID0001
									</td>
									<td>
										IBM00034
									</td>
									
								</tr>
								<tr>
									<td class="fit">
										<img class="banklogo" src="../resources/assets/admin/layout3/img/IBM-logo-blue.png">
									</td>
									<td>
										<span class="bold">IBM Business Development</span>
									</td>
									<td>
										 IRPCID002
									</td>
									<td class="fit">
										<img class="banklogo" src="../resources/assets/admin/pages/img/banks/HSBC-logo.png">
									</td>
									<td>
										<span class="bold">HSBC </span>
									</td>
									<td>
										IRPBID0001 BOA-LCID0001
									</td>
									<td>
										IBM00034
									</td>
									
								</tr>
									<tr>
									<td class="fit">
										<img class="banklogo" src="../resources/assets/admin/layout3/img/IBM-logo-blue.png">
									</td>
									<td>
										<span class="bold">IBM Corporation</span>
									</td>
									<td>
										 IRPCID003
									</td>
									<td class="fit">
										<img class="banklogo" src="../resources/assets/admin/pages/img/banks/citibank.png">
									</td>
									<td>
										<span class="bold">Abercrombie and Fitch </span>
									</td>
									<td>
										IRPBID0001 BOA-LCID0001
									</td>
									<td>
										IBM00034
									</td>
								</tr>
								
								
								</tbody>
								</table>
							</div>
								</div>
									
								<div class="col-md-12 col-sm-12 col-xs-12 nopad">
								<div class="form-actions noborder text-right">
									<button type="button" class="btn default">Cancel</button>
									<button type="button" class="btn blue">Update</button>
								</div>
								</div>
							</form>
							
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>