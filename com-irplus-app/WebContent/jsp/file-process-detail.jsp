<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container-fluid">
			<!-- BEGIN PAGE TITLE
			<div class="page-title">
				<h1>File Processing Information</h1>
			</div>
			 END PAGE TITLE -->
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container-fluid">
		<div class="col-md-6 col-sm-6 col-xs-12 text-right ">
				<div class="fixedbtn-container">

				<a href="javascript:void(0);" class="btn serach m-t-15 search-trigger">
					<i class="fa fa-search"></i> search
				</a>
			</div>
			</div>
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
					
										<div class="col-md-12 nopad searchmain-wraper" id="search-wraper">
					<div class="portlet light">
						<span class="searchclose"> &times; </span>
						<form role="form">


									<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">From date</label>
										<input type="text" class="form-control fromdate" id="datepicker" placeholder="From date">
									</div>
									</div>

	<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">To date</label>
										<input type="text" class="form-control todate" id="datepicker2" placeholder="To date">
									</div>
									</div> 

	<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">Bank Name</label>
										<input type="text" class="form-control" id="filter-bname" placeholder="Bank Name">
									</div>
									</div>
									
									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">Customer Id</label>
										<input type="text" class="form-control" id="filter-custId" placeholder="Customer Id">
									</div>
									</div>
									
									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">Customer Name</label>
										<input type="text" class="form-control" id="filter-cname" placeholder="Customer Name">
									</div>
									</div>

							

									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">File Processing</label>
										<select class="form-control select2" id="fileprocessdd">
											<option value="">File Type</option>

										</select>

									</div>
									</div>

								<!-- 	<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">Business processing</label>
										<select class="form-control select2" id="busiprocessdd">
											<option value="">Business Process</option>

										</select>
									</div>
									</div> -->
							<!-- 		<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">Status</label>
										<select class="form-control select2" id="filter-bstatus">
											<option value="">ProcessedStatus</option>
											<option >Completed</option>
											<option >Error</option>
										</select>

									</div>
									</div>  -->
									<div class="col-md-12 col-sm-12 col-xs-12 text-right">
									<label for="form_control_1">&nbsp;</label>
									<div class="form-md-line-input pb-15">
									<button type="button" class="btn blue" onClick="loadBanks('filter')">SEARCH</button>

									</div>
									</div>
							</div>
							</form>
							 </div>
							</div>
					
					
					
					
						<div class="row">
							<div class="col-md-12">
								<!-- BEGIN SAMPLE FORM PORTLET-->
								<div class="portlet light col-md-12 col-sm-12 col-xs-12 file-process-asch">
								<div class="pagestick-title">
								<span>FILE PROCESSING INFORMATION</span>
							</div>
								      <!--<div class="batchinfo-table clearfix">
                   <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td><div class="batch-data-left batch-table-content clearfix">File Name</div>
      <div class="batch-data-right batch-table-content clearfix">: <i class="fa fa-file-text"></i> <span id="file-name"></span></div></td>
       <td><div class="batch-data-left batch-table-content clearfix">File Type</div>
      <div class="batch-data-right batch-table-content clearfix">:   <span id="fileType"></span></div></td>
      <td><div class="batch-data-left batch-table-content clearfix">Batch Date</div>  <div class="batch-data-right batch-table-content clearfix">:   <span id="batchDate"></span></div> </td>
    </tr>
    
  </tbody>
</table>

                   </div>-->
				    <div class="batchinfo-table clearfix fileprocess-topdetail">
                   <table width="100%" border="0" cellspacing="0" cellpadding="0">
						<col width="33%"></col>
						<col width="33%"></col>
						<col width="33%"></col>
						<thead>
						 <tr>
					      <th>
						  <div class="">File Name</div>
					      </th>
					       <th>
						   <div class="">File Type</div>
					      </th>
					      <th>
						  <div class="">Batch Date</div>  
					      </th>
					      
					    </tr>
					  </thead>
					  <tbody>
					 
					    <tr>
					    <td>
						  <div class=""> <span id="file-name"></span></div>
						  </td>
					      <td>
						  <div class=""> <span id="fileType"></span></div>
						  </td>
					       <td>
							<div class=""><span id="batchDate"></span></div>
						  </td>
					      
					     
					    </tr>
					    
					  </tbody>
					</table>

                   </div>
				   
				   
				   
				   
									<div class="portlet-body form">
										
										<!-- <div class="pull-right col-sm-6 nopad text-right fileprocess-rightcont">
											<a href="file-process-list.jsp">
												<i class="fa fa-reply"></i> Back
											</a>
											
										</div> -->
										<!-- <div class="col-sm-12 nopad">
											<ul class="list-inline list-batch">
												<li>
													<b><span class="batch-icon icon-common"><i class="fa fa-file"></i></span> No. of batch :</b> 4
												</li>
												<li>
													<b><span class="inprocess-icon icon icon-common"><i class="fa fa-cog"></i></span>In process : </b> 0
												</li>
												<li>
													<b><span class="readyprocess-icon icon icon-common"><i class="fa fa-repeat"></i></span>Ready to process :  </b> 0
												</li>
												<li>
													<b><span class="waitingaction-icon icon icon-common"><i class="fa fa-hourglass-half"></i></span>Waiting for action : </b> 2
												</li>
												<li>
													<b><span class="completedbatch-icon icon icon-common"><i class="fa fa-thumbs-up"></i></span>Batch Completed : </b> 2
												</li>
											</ul>
										</div>	 -->
										<div class="col-sm-12 nopad">
											<h3 class="medium-title">BATCHES COMPLETED</h3>
										</div>	
										<div class="table-scrollable table-scrollable-borderless material-table">
											<table class="table" id="batch-list">
												<thead>
													<tr class="uppercase">
														<th>
															Batch No.
														</th>
														<th>
															Customer Name
														</th>
														<!-- <th>
															Entry Class
														</th> -->
														<!-- <th>
															Form
														</th> -->
														
														<!-- <th>
															Data Entry
														</th> -->
														<!-- <th>
															Select Data Entry
														</th> -->
														<th>
															Transaction
														</th> 
														<th>
															Item
														</th> 
														<th>
															payment
														</th> 
														<th>
															Remittance
														</th> 
														 <th>
															Scan doc
														</th> 
														<th>
															batch amount
														</th> 
														<th>
															View
														</th>
														
													</tr>
												</thead>
												 <tbody>
													<!--<tr>
														<td>
															Batch 1
														</td>
														<td>
															Mckinsey
														</td>
														<td>
															Yes
														</td>
														<td>
															Default
														</td>
														<td>
															1
														</td>
														<td>
															-
														</td>
														<td>
															<button type="button" class="btn green btn-xs">completed</button>
														</td>
														
													</tr>
													<tr>
														<td>
															Batch 2
														</td>
														<td>
															AT & T
														</td>
														<td>
															No
														</td>
														<td>
															Not Required
														</td>
														<td>
															1
														</td>
														<td>
															<select class="form-control" id="form_control_1">
																<option value="">Form 1</option>
																<option value="1">Form 1</option>
																<option value="2">Option 2</option>
																<option value="3">Option 3</option>
																<option value="4">Option 4</option>
															</select>
															
														</td>
														<td>
															<button type="button" class="btn green btn-xs">completed</button>
														</td>
														
													</tr>	-->
												</tbody> 
											</table>
										</div>
										
									<!-- <h3 class="medium-title">BATCHES REQUIRES DATA ENTRY</h3>
										<div class="table-scrollable table-scrollable-borderless">
											<table class="table table-file">
												<thead>
													<tr class="uppercase">
														<th>
															Batch No.
														</th>
														<th>
															Customer Name
														</th>
														<th>
															Data Entry
														</th>
														<th>
															Form
														</th>
														<th>
															Items
														</th>
														<th>
															Select Data Entry
														</th>
														<th>
															Status
														</th>
														
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
															Batch 0
														</td>
														<td>
															IBM
														</td>
														<td>
															Yes
														</td>
														<td>
															No Default
														</td>
														<td>
															2
														</td>
														<td>
															<select class="form-control" id="form_control_1">
																<option value="">Form 4</option>
																<option value="1">Form 1</option>
																<option value="2">Option 2</option>
																<option value="3">Option 3</option>
																<option value="4">Option 4</option>
															</select>
															
														</td>
														<td>
															<button type="button" class="btn bg-blue-hoki btn-xs">Waiting for action</button>
														</td>
														
													</tr>
													<tr>
														<td>
															Batch 3
														</td>
														<td>
															CTS
														</td>
														<td>
															Yes
														</td>
														<td>
															No Default
														</td>
														<td>
															3
														</td>
														<td>
															<select class="form-control" id="form_control_1">
																<option value="">Form 3</option>
																<option value="1">Form 1</option>
																<option value="2">Option 2</option>
																<option value="3">Option 3</option>
																<option value="4">Option 4</option>
															</select>
															
														</td>
														<td>
															<button type="button" class="btn bg-blue-hoki btn-xs">Waiting for action</button>
														</td>
														
													</tr>	
												</tbody>
											</table>
										</div>-->
									</div>
								<!-- 	<div class="col-md-12 col-sm-12 col-xs-12 nopad">
										<div class="form-actions noborder text-right">
											<button type="button" class="btn default">Cancel</button>
											<button type="button" class="btn blue">Good to go</button>
										</div>
									</div>-->
								</div>
								<!-- END SAMPLE FORM PORTLET-->
							</div>
						</div>
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<script>
     var fileHeaderRecordId = <%=request.getParameter("fileHeaderRecordId")%>;
    
     $(document).ready(function()
    	{  	 	
 		$.ajax({
 	
 			method:'get',
 			url:'../file-process/getBatch/'+fileHeaderRecordId,
 			contentType:'application/json',
 			dataType:'JSON',
 			crossDomain:'true',
 			success:function(response){
 				
 				var len = response.length;
 				 filename = response.allFileList[0].achFileName;
 				$("#file-name").append(filename);
 				$("#fileType").append(response.allFileList[0].fileType);
 				$("#batchDate").append(response.allFileList[0].batchHeaderRecordBean[0].companyDescriptiveDate);
 	           
 				for(var i=0;i<response.allFileList[0].batchHeaderRecordBean.length;i++){
 	                var id = response.allFileList[0].batchHeaderRecordBean[i].batchNumber;
 	                var companyName = response.allFileList[0].batchHeaderRecordBean[i].companyName;
 	                var batchId=response.allFileList[0].batchHeaderRecordBean[i].batchHeaderRecordId;
 	                
 	              //var name = response[i].name;
 	             // var email = response[i].email;
 					
 	                var tr_str = "<tr>" +
 	                    "<td>" +response.allFileList[0].batchHeaderRecordBean[i].batchNumber+ "</td>" +
 	                   "<td>"+response.allFileList[0].batchHeaderRecordBean[i].customerName+ "</td>" +
 	            
 	                 "<td>"+response.allFileList[0].batchHeaderRecordBean[i].transactionCount+ "</td>" +
 	                "<td>"+response.allFileList[0].batchHeaderRecordBean[i].itemCount+ "</td>"+
					"<td>"+response.allFileList[0].batchHeaderRecordBean[i].paymentCount+ "</td>"+
					"<td>"+response.allFileList[0].batchHeaderRecordBean[i].remittanceCount+ "</td>"+
					"<td>"+response.allFileList[0].batchHeaderRecordBean[i].scandocCount+ "</td>"+
					"<td>"+response.allFileList[0].batchHeaderRecordBean[i].batchAmount+ "</td>"+
					"<td><form action='file-process.jsp' method='post'><input type='hidden' name='batchHeaderRecordId' value="+batchId+" /><button type=submit class='btn btn-xs green-meadow'><i class='fa fa-eye'></i></button></form></td>"
						
					"</tr>";

 	                $("#batch-list").append(tr_str);
 	            }
 		   
			},

		 error:function(response,statusTxt,error){
		 
		/* 	alert("im error :"+response.allFileList); */
		 }	
 		
 		});
    	 
    	 
    	
    	 
    	 
    	});


     $(function() {
    		
    	    $( "#datepicker" ).datepicker();
    	});
    	$(function() {
    		
    	    $( "#datepicker2" ).datepicker();
    	});
     
     
    

</script>
     


</body>
<!-- END BODY -->
</html>