package com.irplus.app;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.irplus.core.client.customer_reporting.ICustomerReportingCoreClient;
import com.irplus.dto.BankReportingsInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.customerreporting.CustomerRepotingBean;
import com.irplus.dto.customerreporting.CustomerRepotingResponseDetails;
import com.irplus.util.BusinessException;
import com.irplus.util.CustomerReportingValidationHelper;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;
import com.irplus.util.ValidationHelper;

@Controller
@RequestMapping(value="/customer")
public class CustomerReportingController {

	private static final Log log = LogFactory.getLog(CustomerReportingController.class);

	Gson gson = new Gson();
	
	@Autowired 
	ICustomerReportingCoreClient iCustomerReportingCoreClient;
	
	@RequestMapping(value="/reporting/create" , method= RequestMethod.POST , consumes="application/json" ,produces = "application/json") 
	
	public ResponseEntity<String> createCustomerReporting(@RequestBody String customerReportingData){
		
		log.info("Inside of CustomerContactsControll :: CreateCustomerContacts");
		
		HttpHeaders responseHeader = new HttpHeaders();
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		List<String> errMsgs = new ArrayList<String>();
 		 			
					try {
						CustomerRepotingBean customerRepotingBean= gson.fromJson(customerReportingData.toString(), CustomerRepotingBean.class);
						
						boolean isValidReq = CustomerReportingValidationHelper.isValidCustomerReportingReq(customerRepotingBean);
				
						if (isValidReq) {
					
							irPlusResponseDetails = iCustomerReportingCoreClient.createCustomerReport(customerRepotingBean);
							
						} else {
							
							errMsgs.add("Validation error :CustomerId is null");
							irPlusResponseDetails.setErrMsgs(errMsgs);
							irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
							irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
								
							log.error("ReportController :: Not a Valid report Object ");							
						}
						
					} catch (BusinessException e) {
						
						irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
						
						log.error("Exception raised in ReportController :: createCustomerReporting",e);
					}
															
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails),responseHeader ,HttpStatus.OK);
			} 
			else {
				if (!irPlusResponseDetails.isValidationSuccess()) {
			
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseHeader, HttpStatus.CONFLICT);
				
				} else {			
					return IRPlusRestUtil.getGenericErrorResponse();
				}
			}
	}
	
	//get
	@RequestMapping(value="/showreport/{customerReportId}",method=RequestMethod.GET,produces="application/json")
				
			public ResponseEntity<String> showOneCustomerReport(@PathVariable String customerReportId){
				
				log.info("customerReportId::" + customerReportId);
				List<String> errMsgs = new ArrayList<String>();
				
				HttpHeaders responseHeader = new HttpHeaders();
				
				IRPlusResponseDetails customerRepotingResponseDetails = new IRPlusResponseDetails();
		 			
				try{	
					boolean isValidRoleId = ValidationHelper.isValidReq(customerReportId);		
					if (isValidRoleId) {
						
					customerRepotingResponseDetails = iCustomerReportingCoreClient.getCustomerReportById(customerReportId);
					
					}else {
						errMsgs.add("Validation Error : Id is null");
						customerRepotingResponseDetails.setErrMsgs(errMsgs);
						customerRepotingResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
						customerRepotingResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
					}
					
				} catch (BusinessException e) {
					
					customerRepotingResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					customerRepotingResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);						
						
					log.error("Exception raised in customerReportingcontroller :: getCustomercontacts",e);
						
					}				
					
				if (customerRepotingResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				
					return new ResponseEntity<String>(gson.toJson(customerRepotingResponseDetails), responseHeader, HttpStatus.OK);
				} 
				else {
						if (!customerRepotingResponseDetails.isValidationSuccess()){
							return new ResponseEntity<String>(gson.toJson(customerRepotingResponseDetails),responseHeader, HttpStatus.CONFLICT);
						} else {
							return IRPlusRestUtil.getGenericErrorResponse();
						}
				}
			}
	
			//delte
	@RequestMapping(value="customerreport/delete/{customerReportId}",method=RequestMethod.GET,produces="application/json")
	public ResponseEntity<String> deleteCutomerContactsId(@PathVariable String customerReportId){
			
			HttpHeaders responseheaders = new HttpHeaders(); 

			CustomerRepotingResponseDetails customerRepotingResponseDetails = new CustomerRepotingResponseDetails();
	 		
			boolean isValidCustomId = ValidationHelper.isValidReq(customerReportId);
			
			if (isValidCustomId) {
							
						try {
							customerRepotingResponseDetails = iCustomerReportingCoreClient.deleteCustomerReportById(customerReportId);
							} catch (BusinessException e) {

								customerRepotingResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
								customerRepotingResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
								log.error("Exception raised in CutomerReportingController :: deleteCustomerReporting",e);
						
							}
			} else {
				
				customerRepotingResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				customerRepotingResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}
					
			if (customerRepotingResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(customerRepotingResponseDetails), responseheaders, HttpStatus.OK);
			}
			else {
					if (!customerRepotingResponseDetails.isValidationSuccess()){
						return new ResponseEntity<String>(gson.toJson(customerRepotingResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}			
	}

		@RequestMapping(value="/customerreporting/showAll",method=RequestMethod.GET,produces="application/json")
		public ResponseEntity<String> showAllCustomerContacts(){

			HttpHeaders responseheaders = new HttpHeaders(); 

			CustomerRepotingResponseDetails customerRepotingResponseDetails = new CustomerRepotingResponseDetails();
			
	 			try {
	 				customerRepotingResponseDetails = iCustomerReportingCoreClient.showAllCustomerReports();
					
				} catch (BusinessException e){
					
					customerRepotingResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					customerRepotingResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
					log.error("Exception raised in CustomerReportingsController :: showAllCustomersReporting",e);
				}
				
				if (customerRepotingResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)){
					
					return new ResponseEntity<String>(gson.toJson(customerRepotingResponseDetails), responseheaders, HttpStatus.OK);
				}
				else {
						if (!customerRepotingResponseDetails.isValidationSuccess()) {
							return new ResponseEntity<String>(gson.toJson(customerRepotingResponseDetails),responseheaders, HttpStatus.CONFLICT);
						} else {
							return IRPlusRestUtil.getGenericErrorResponse();
						}
				}		
		}
			
			
	@RequestMapping(value="/cutomerreport/update",method = RequestMethod.POST,consumes = "application/json" ,produces ="application/json")
	public ResponseEntity<String> updateCustomerContacts(@RequestBody String customerreportData){
			
			
			HttpHeaders responseheaders = new HttpHeaders(); 

			CustomerRepotingResponseDetails customerRepotingResponseDetails = new CustomerRepotingResponseDetails();
	 			
			CustomerRepotingBean customerRepotingBean = gson.fromJson(customerreportData.toString(), CustomerRepotingBean.class);
			
			boolean isValidReq = CustomerReportingValidationHelper.isValidCustomerReportingReq(customerRepotingBean);
			
			log.info("customerRepotingBeanid " +customerRepotingBean.getCustomerReportId());

					try {	
							
							if(isValidReq){
								
									customerRepotingResponseDetails = iCustomerReportingCoreClient.updateCustomerReports(customerRepotingBean);								
							} else {
								
								customerRepotingResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
								customerRepotingResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
							}
							
							}catch(BusinessException e){
									
									customerRepotingResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
									customerRepotingResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
									log.error("Exception raised in Customer Controller :: getCustomer",e);
						}
				
					if (customerRepotingResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
						return new ResponseEntity<String>(gson.toJson(customerRepotingResponseDetails), responseheaders, HttpStatus.OK);
					}
					else {
							if (!customerRepotingResponseDetails.isValidationSuccess()) {
								return new ResponseEntity<String>(gson.toJson(customerRepotingResponseDetails),responseheaders, HttpStatus.CONFLICT);
							} else {
								return IRPlusRestUtil.getGenericErrorResponse();
							}
					}		
		}	
	
	
	@RequestMapping(value="/dynamic/reports",method=RequestMethod.POST,consumes="application/json",produces="application/json")
	public ResponseEntity<String> dynamicReports(@RequestBody String bankReportingsInfoData){

		HttpHeaders responseheaders = new HttpHeaders(); 

			IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
 		
			try {
				
				BankReportingsInfo banksReportsInfo = gson.fromJson(bankReportingsInfoData.toString(), BankReportingsInfo.class);
							
				irPlusResponseDetails = iCustomerReportingCoreClient.dynamicCustomerReportsBybankId(banksReportsInfo);			
		
			}catch(BusinessException e){
				
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				log.error(" Exception raised in CustomerReportingsController :: dynamicCustomerReporting ",e);
			}
			
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)){
				
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			}
			else{
					if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}		
	}
	
}