package com.irplus.app;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import com.irplus.dao.hibernate.entities.IrFPCCDAddendaRecord;

public class RemittancePNG {
private ByteArrayOutputStream stream = null;
	
	public	RemittancePNG(){
	}

	public RemittancePNG(String[] text){
		 int yIndex = 30;
		 int fontSize=20;
		 ImageIcon printImage = new javax.swing.ImageIcon("C://Program Files/Apache Software Foundation/Tomcat 8.5/webapps/imagescan/resources/assets/irplus/sitelogo/imagescan.png");

		  BufferedImage bufferedImage = new BufferedImage(816,336,BufferedImage.TYPE_INT_RGB);
	        Graphics graphics = bufferedImage.getGraphics();
	        graphics.fillRect(0, 0, 816, 336);
	        graphics.setFont(new Font("Verdana",Font.PLAIN,15));
	        graphics.setColor(Color.BLACK);
	        //graphics.setColor(Color.BLACK);
	       // graphics.setFont(new Font("Arial Black", Font.BOLD, 5));
	        
	        
	        
	        graphics.drawImage(printImage.getImage(),0,0,100,30, null);
	        graphics.drawString("ImageScan Inc",5,45);
	        graphics.drawString(" | ACHTestBox",120,45);
	        graphics.drawString("4411 Forbes Blvd",5,60);
	        graphics.drawString("Lanham,MD.20708",5,75);
	        graphics.drawString(text[0],150,165);
            graphics.drawString("Payment Date : "+text[1],500,140);
            graphics.drawString("ACCT #: "+text[2],150,140);
            
            
            graphics.drawString("Amount: $"+text[3],550,165);
            graphics.drawString("ACH TRACE NUMBER: "+text[4],50,250);
		            
		            stream = new ByteArrayOutputStream();    
		            
		        
		        
	        //graphics.drawString(key, 10, 25);
	        try {
				ImageIO.write(bufferedImage,"png",stream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	
	
	
	public ByteArrayOutputStream getStream() {
		return stream;
	}

	public void setStream(ByteArrayOutputStream stream) {
		this.stream = stream;
	}


}
