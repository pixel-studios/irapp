package com.irplus.app;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.Gson;
import com.irplus.core.client.sitesetup.SiteSetupCoreClient;
import com.irplus.core.client.users.IUserCoreClient;
import com.irplus.dao.hibernate.entities.IRSSite;
import com.irplus.dao.users.IUserDao;
import com.irplus.dto.BankFilterInfo;
import com.irplus.dto.BankInfo;
import com.irplus.dto.ContactInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.UserFilterInfo;
import com.irplus.dto.users.UserBean;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;
import com.irplus.util.ValidationHelper;

@Controller
@Transactional
public class UserController {

	private static final Log log = LogFactory.getLog(UserController.class);

	Gson gson = new Gson();

	@Autowired
	IUserCoreClient userCoreClient;
	
	@Autowired
	IUserDao userDao;
	
	@Autowired
    SiteSetupCoreClient siteSetupCoreClient;
	
	
	@Autowired
	  private SessionFactory sessionFactory;
	  
	  public Session getMyCurrentSession()
	  {
	    Session session = null;
	    try
	    {
	      session = this.sessionFactory.getCurrentSession();
	    }
	    catch (HibernateException e)
	    {
	    	log.error("Exception raised DaoImpl :: At current session creation time ::" + e);
	    }
	    return session;
	  }
	  
	String rootFolderPath =null;
	
	String userLogoPath=null;
	
	
	
	
	
	
	@RequestMapping(value= "/user/create" , method = RequestMethod.POST, produces= "application/json", consumes= "application/json")
	@ResponseBody
	public ResponseEntity<String> createUser(@RequestBody String requestInputData) { 

		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	
		UserBean userBean = gson.fromJson(requestInputData.toString(),UserBean.class);
			log.info("User Info in user controller::" + userBean.getUsername());
		try
		{
			

		

			
				
				irPlusResponseDetails = userCoreClient.createUser(userBean);
			
			

		}
		catch (BusinessException e)
		{
			log.error("Exception in CreateUser", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}

		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}

	// -----------------------------------------

	/* create the user table and userbank table */

	@RequestMapping(value = "/user_ubank/create", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public ResponseEntity<String> createUserNoDuplicate(@RequestBody String userBeanData) {

		log.info("Inside of UserController :: createUser()");

		HttpHeaders responseHeader = new HttpHeaders();

		UserBean userBean = gson.fromJson(userBeanData.toString(),UserBean.class);

		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		try {

			usersResponseDetails = userCoreClient.createUserNoDup(userBean);

			// usersResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		} catch (BusinessException e) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception Raised At UserController :: Create User()", e);
		}

		if (usersResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {

			return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader, HttpStatus.OK);

		} else {

			if (!usersResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader,
						HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	
/* This method able to use 3 things
	1. This method act user update status 1 or 0
	2. This method act as user deleteNot Db status =2 
	3. delete meant isactive = 2 not permanent delete in DB
*/	
	
	
	@RequestMapping(value = "/user/update/status/{userid}/{status}", method = RequestMethod.GET,produces = "application/json")
	@ResponseBody

	public ResponseEntity<String> updateStatusUser(@PathVariable String userid, @PathVariable String status) {

		// UserBean userBean = gson.fromJson(userBeanData.toString(),
		// UserBean.class);
		
		UserBean userBean = new UserBean();
		List<String> errMsgs = new ArrayList<String>();
		
		HttpHeaders responseHeader = new HttpHeaders();
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		try {

			Integer id = Integer.parseInt(userid);
			Integer statusid = Integer.parseInt(status);

			boolean isValidId1 = ValidationHelper.isValidReq(userid);
			boolean isValidId2 = ValidationHelper.isValidReq(status);

			if (isValidId2 && isValidId1) {
				
				userBean.setUserid(id);
				userBean.setIsactive(statusid);
				usersResponseDetails = userCoreClient.updateStatusUser(userBean);	
				
			}else{
				errMsgs.add("Validation Error : Wrong Charecters Entered : Please Enter Validate no.s");
				usersResponseDetails.setErrMsgs(errMsgs);
				usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				usersResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);			
				
			}
			
			log.debug("UserController :: Not a Valid UserId");

		} catch (BusinessException e) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception Raised At UserController :: Create UserStatus()", e);
		}

		if (usersResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {

			return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader, HttpStatus.OK);

		} else {

			if (!usersResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader,
						HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}

	@RequestMapping(value = "/user/show/all_roles_banks/{siteId}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<String> getAll_Roles_Banks(@PathVariable String siteId) {

		HttpHeaders responseHeader = new HttpHeaders();

		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		try {
			usersResponseDetails = userCoreClient.getAll_Role_Banks(siteId);

			log.debug("UserController :: Shown All Users -Role ,Banks");

		} catch (BusinessException e) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception Raised At UserController :: get Role user()", e);
		}

		if (usersResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {

			return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader, HttpStatus.OK);

		} else {

			if (!usersResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader,
						HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}

	}
	
	/*Updated => user , userbanks tables*/
	
	@RequestMapping(value = "/user/update/user_bank", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	
	public ResponseEntity<String> updateUserAndBank(@RequestBody String userBeanData){



		HttpHeaders responseHeader = new HttpHeaders();
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		try {
			UserBean userBean = gson.fromJson(userBeanData.toString(),UserBean.class);
			
			usersResponseDetails = userCoreClient.updateUserAndBank(userBean);

		} catch (BusinessException e) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception Raised At UserController :: Create updateUser_Bank()", e);
		}

		if (usersResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {

			return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader, HttpStatus.OK);

		} else {

			if (!usersResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader,
						HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}

// delete meant isactive=2 not permanent delete in DB
	
	
// ----------------------------------------- close

	
	@RequestMapping(value = "/user/showOne/{userId}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> getUserById(@PathVariable String userId) {

		log.debug("Inside of User Controller : ");

		HttpHeaders responseHeader = new HttpHeaders();


		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		try {
			boolean isValidId = ValidationHelper.isValidReq(userId);
			if (isValidId) {
				usersResponseDetails = userCoreClient.getUserById(userId);
			} else {
				log.debug("UserController :: Not a Valid UserId");
			}

		} catch (BusinessException e) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception Raised At UserController :: Create User()", e);
		}

		if (usersResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {

			return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader, HttpStatus.OK);

		} else {

			if (!usersResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader,
						HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}

	@RequestMapping(value = "/user/delete/{userId}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<String> deleteUserById(@PathVariable String userId) {

		log.debug("Inside UserController :: DeleteUserById()");

		HttpHeaders responseHeader = new HttpHeaders();

		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		try {
			boolean isValidId = ValidationHelper.isValidReq(userId);
			if (isValidId) {
				usersResponseDetails = userCoreClient.deleteUserById(userId);
				usersResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			} else {
				log.debug("UserController :: Not a Valid UserId");
			}

		} catch (BusinessException e) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception Raised At UserController :: Create User()", e);
		}

		if (usersResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {

			return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader, HttpStatus.OK);

		} else {

			if (!usersResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader,
						HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}

	}

	@RequestMapping(value = "/user/show/all/{siteId}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<String> showAllUsers(@PathVariable String siteId) {
		log.debug("");

		HttpHeaders responseHeader = new HttpHeaders();

		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		try {
			usersResponseDetails = userCoreClient.findAllUsers(siteId);

			log.debug("UserController :: Shown All Users");

		} catch (BusinessException e) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception Raised At UserController :: Create User()", e);
		}

		if (usersResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {

			return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader, HttpStatus.OK);

		} else {

			if (!usersResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader,
						HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}

	}

	@RequestMapping(value = "/user/update", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> updateUser(@RequestBody String userBeanData) {

		UserBean userBean = gson.fromJson(userBeanData.toString(), UserBean.class);

		HttpHeaders responseHeader=new HttpHeaders();
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		try {
			usersResponseDetails = userCoreClient.updateUser(userBean);

			log.debug("UserController :: Not a Valid UserId");

		} catch (BusinessException e) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception Raised At UserController :: Create User()", e);
		}

		if (usersResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {

			return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader, HttpStatus.OK);

		} else {

			if (!usersResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader,
						HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
}
	
//filterUsers
	
//	@RequestMapping(value = "/user/filter", method = RequestMethod.POST,consumes = "application/json" , produces = "application/json")
//	@ResponseBody
//	public ResponseEntity<String> filterUser(@RequestBody String userBeanData){
//	log.info("ewwwfrf");
//	
//	
//	//	public ResponseEntity<String> filterUser(@ModelAttribute UserFilterInfo userFilterInfo) {
//	//	public ResponseEntity<String> filterUser() {
//		
//		UserFilterInfo userFilterInfo = gson.fromJson(userBeanData.toString(), UserFilterInfo.class);
//		
//		log.debug("Inside of UserController :: UserFilter ");
//		log.info("userFilterInfoqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"+userFilterInfo.getRoleId());
//		log.info("fdyrt76666666668888ytujgheydrfgdfffffffffffffffff"+userFilterInfo.getUsername());
//	//	UserFilterInfo userFilterInfo = new UserFilterInfo();
		@RequestMapping(value="/user/filter", method=RequestMethod.POST, produces= "application/json")
		@ResponseBody
		public ResponseEntity<String> filterUsers(@ModelAttribute UserFilterInfo userFilterInfo)
		{
		HttpHeaders responseHeader=new HttpHeaders();
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		try {
			
			usersResponseDetails = userCoreClient.filterUsers(userFilterInfo);

		} catch (BusinessException e) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception Raised At UserController :: UserFilter()", e);
		}

		if (usersResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {

			return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader, HttpStatus.OK);

		} else {

			if (!usersResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader,
						HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	
	@RequestMapping(value="/login",method=RequestMethod.POST,consumes = "application/json", produces="application/json")
	@ResponseBody
	public ResponseEntity<String> validateUser(@RequestBody String userBeanData,HttpSession session){

		UserBean userBean = gson.fromJson(userBeanData.toString(), UserBean.class);

		HttpHeaders responseHeader = new HttpHeaders();
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		try {
			usersResponseDetails = userCoreClient.validateUser(userBean);	

			log.debug("UserController :: Not a Valid UserId");

		} catch (BusinessException e) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception Raised At UserController :: Create User()",e);
		}

		if (usersResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {

			session.setAttribute("username", userBean.getUsername());

			log.debug("userBean.getUserid()kk "+userBean.getUserid());
			
			session.setAttribute("userid", userBean.getUserid());
			session.setAttribute("siteId", userBean.getSiteId());
			session.setAttribute("roleId", userBean.getRoleId());

			return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader, HttpStatus.OK);

		} else {

			if (!usersResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader, HttpStatus.CONFLICT);
			} else {	
				return IRPlusRestUtil.getGenericErrorResponse();				
			}
		}		
	}
 	
	
	@RequestMapping(value="/logout",method=RequestMethod.POST,consumes = "application/json", produces="application/json")
	@ResponseBody
	public ResponseEntity<String> doLogout(@RequestBody String userBeanData,HttpSession session){

		UserBean userBean = gson.fromJson(userBeanData.toString(), UserBean.class);


		HttpHeaders responseHeader = new HttpHeaders();
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		try {
			session.getAttribute("username");

			log.debug("UserController :: Logging out user :"+session.getAttribute("username"));
			
			
			session.invalidate();
			
			usersResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);

		} catch (Exception e) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception Raised At UserController :: logout User()",e);
		}

		if (usersResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {

			return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader, HttpStatus.OK);

		} else {

			if (!usersResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader, HttpStatus.CONFLICT);
			} else {	
				return IRPlusRestUtil.getGenericErrorResponse();				
			}
		}		
	}
 	
	@RequestMapping(value="/fetchMenu",method=RequestMethod.POST,consumes = "application/json", produces="application/json")
	@ResponseBody
	public ResponseEntity<String> fetchMenu(@RequestBody String userBeanData,HttpSession session){

		UserBean userBean = gson.fromJson(userBeanData.toString(),UserBean.class);


		HttpHeaders responseHeader = new HttpHeaders();
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		try {
			

			
			
			usersResponseDetails = userCoreClient.fetchMenu(userBean.getRoleId());
			
			if(usersResponseDetails.getStatusCode()!=IRPlusConstants.ERR_CODE)
			{
				usersResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				usersResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
			else
			{
				usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}

		} catch (Exception e) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception Raised At UserController :: fetchMenu",e);
		}

		if (usersResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {

			return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader, HttpStatus.OK);

		} else {

			if (!usersResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader, HttpStatus.CONFLICT);
			} else {	
				return IRPlusRestUtil.getGenericErrorResponse();				
			}
		}		
	}
 	
	@RequestMapping(value = "/user/fileUpload", method = RequestMethod.POST, headers = "content-type=multipart/*", produces = {"application/json"})
	 public ResponseEntity<String> imageUpload(MultipartHttpServletRequest request,
				@RequestParam(value="userPhoto",required = false) MultipartFile userPhoto) throws Exception {
			
			HttpHeaders responseHeaders = new HttpHeaders();

			IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
					
			try
			{
	    
				
				
				String hql = "from IRSSite site where site.isactive=?";
			      
			      Query query = getMyCurrentSession().createQuery(hql);
			      query.setParameter(0, Integer.valueOf(1));
			      
			      List<IRSSite> siteList = query.list();
			      if (siteList.size() > 0)
			      {
			        rootFolderPath = ((IRSSite)siteList.get(0)).getRootFolderPath();
				
			      }
				
				if(rootFolderPath!=null) {
					 File file = new File(rootFolderPath + File.separator + "Images");
			            if (!file.exists()) {
			              if (file.mkdir()) {
			                System.out.println("Directory is created!");
			              } else {
			                System.out.println("Failed to create directory!");
			              }
			            }
			            userLogoPath=rootFolderPath+File.separator + "Images"+File.separator+"UserLogo";
			            File file1 = new File(userLogoPath);
			            if (!file1.exists()) {
			              if (file1.mkdir()) {
			                System.out.println("Directory is created!");
			              } else {
			                System.out.println("Failed to create directory!");
			              }
			            }
	 				}	
			log.info("fileUploaded::" + userPhoto +" original File name : "+userPhoto.getOriginalFilename());
			
			String filePath = request.getRealPath("/");
			
			filePath = userLogoPath+File.separator+userPhoto.getOriginalFilename();
			
			log.info("fileUploaded::" + userPhoto +" original File name : "+userPhoto.getOriginalFilename()+"filepath : "+filePath);
			
			File newFile = new File(filePath);
			
			userPhoto.transferTo(newFile);
			
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			
			irPlusResponseDetails.setLogoPath(userPhoto.getOriginalFilename());
			}
			
			catch (Exception e) {
				log.error("Exception in ImageUpload in User", e);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
			{ 			  			// Successfully Request Creation

				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
			} 
			else 
			{
				if(!irPlusResponseDetails.isValidationSuccess())
				{
					return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
				}
				else
				{
					return IRPlusRestUtil.getGenericErrorResponse();
				}
			}
	    }
	@RequestMapping(value="/site",method=RequestMethod.GET,consumes = "application/json", produces="application/json")
	@ResponseBody
	public ResponseEntity<String> validateSite(){

		

		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		try{
			
			irPlusResponseDetails = this.siteSetupCoreClient.siteInfo();
		
	} catch (BusinessException e) {
		log.error("Exception in listBank",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
	}
	if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
	{ 			  			// Successfully Request Creation

		return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
	} 
	else 
	{
		if(!irPlusResponseDetails.isValidationSuccess())
		{
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
		}
		else
		{
			return IRPlusRestUtil.getGenericErrorResponse();
		}
	}
}
	@RequestMapping(value = "/user/show/all/", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<String> showAllUsers() {
		log.debug("");

		HttpHeaders responseHeader = new HttpHeaders();

		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		try {
			usersResponseDetails = userDao.showAllUsers();

			log.debug("UserController :: Shown All Users");

		} catch (BusinessException e) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception Raised At UserController :: Create User()", e);
		}

		if (usersResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {

			return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader, HttpStatus.OK);

		} else {

			if (!usersResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader,
						HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}

	}
	
	@RequestMapping(value = "/user/getOtherBank/{bankId}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<String> getOtherBank(@PathVariable String bankId) {

		HttpHeaders responseHeader = new HttpHeaders();

		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		try {
			usersResponseDetails = userCoreClient.getOtherBank(bankId) ;

			log.debug("UserController :: Shown All Users -Role ,Banks");

		} catch (BusinessException e) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception Raised At UserController :: get Role user()", e);
		}

		if (usersResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {

			return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader, HttpStatus.OK);

		} else {

			if (!usersResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader,
						HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}

	}
	
	@RequestMapping(value = "/user/getUserBank/{userId}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<String> getUserBank(@PathVariable String userId) {

		HttpHeaders responseHeader = new HttpHeaders();

		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		try {
			usersResponseDetails = userCoreClient.getUserBank(userId) ;

			log.debug("UserController :: Shown All Users -Role ,Banks");

		} catch (BusinessException e) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception Raised At UserController :: get Role user()", e);
		}

		if (usersResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {

			return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader, HttpStatus.OK);

		} else {

			if (!usersResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader,
						HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}

	}
	
	
	//user profile update
	
	@RequestMapping(value = "/profile/update", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> updateProfile(@RequestBody String userBeanData) {

		UserBean userBean = gson.fromJson(userBeanData.toString(), UserBean.class);

		HttpHeaders responseHeader=new HttpHeaders();
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		try {
			usersResponseDetails = userCoreClient.updateProfile(userBean);

			log.debug("UserController :: Not a Valid UserId");

		} catch (BusinessException e) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception Raised At UserController :: Create User()", e);
		}

		if (usersResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {

			return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader, HttpStatus.OK);

		} else {

			if (!usersResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(usersResponseDetails), responseHeader,
						HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
}

	@RequestMapping(value="/autocomplete/filter/username",method=RequestMethod.GET,produces="application/json")
	/*public ResponseEntity<String> Autocomplete(){*/
		public ResponseEntity<String> AutocompleteCustomerCode(@RequestParam("term") String term,@RequestParam("siteId") String siteId) {
		HttpHeaders responseheaders = new HttpHeaders(); 

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
			
			try {
				
				
				
			irPlusResponseDetails = userCoreClient.AutocompleteUsername(term,siteId);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			} catch (Exception e) {

				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				
			}
			
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.OK);
			}
			else {
					if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}	
			
	}
}