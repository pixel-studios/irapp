package com.irplus.app;

import java.io.File;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.Gson;
import com.irplus.core.client.IRPlusCoreClient;
import com.irplus.core.client.bankmgmt.IBankMgmtCoreClient;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.LicenseInfo;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;

@Controller
public class LicenseController {
    private static final Logger LOGGER = Logger.getLogger(IRPlusController.class);
    
    @Autowired
	IRPlusCoreClient irPlusCoreClient;
    
    Gson gson = new Gson();
    
    @RequestMapping(value = "/license/upload", method = RequestMethod.POST, headers = "content-type=multipart/*", produces = {"application/json"})
    
  	@ResponseBody
	public ResponseEntity<String> validateLicense(MultipartHttpServletRequest request,
			@RequestParam(value="licFile",required = false) MultipartFile licFile)
	{
    	HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = null;
				
		try
		{
    
		LOGGER.info("fileUploaded::" + licFile +" original File name : "+licFile.getOriginalFilename());
		
		String filePath = request.getRealPath("/");
		
		filePath = filePath+"/resources/assets/irplus/licenses/"+licFile.getOriginalFilename();
		
		LOGGER.info("fileUploaded::" + licFile +" original File name : "+licFile.getOriginalFilename()+"filepath : "+filePath);
		
		LicenseInfo licInfo = new LicenseInfo();
		
		licInfo.setLicPath(filePath);		
		
		File newFile = new File(filePath);
		
		licFile.transferTo(newFile);
		
		irPlusResponseDetails = irPlusCoreClient.validateLicense(licInfo);
		
		//irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		
		//irPlusResponseDetails.setLogoPath("/resources/assets/irplus/licenses/"+licFile.getOriginalFilename());
		}
		catch (BusinessException e) {
			LOGGER.error("Exception in fileUpload", e);
			if(irPlusResponseDetails==null)
			{
				irPlusResponseDetails = new IRPlusResponseDetails();
			}
			irPlusResponseDetails.setStatusMsg(e.getMessage());
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		
		catch (Exception e) {
			LOGGER.error("Exception in fileUpload", e);
			if(irPlusResponseDetails==null)
			{
				irPlusResponseDetails = new IRPlusResponseDetails();
			}
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
    
  
    
    
    
    
    
}
