package com.irplus.app;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.irplus.core.client.report_file_formats.IReportFileFormatsCoreClient;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.ReportFileFormatInfo;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;
import com.irplus.util.ValidationHelper;

@Controller
public class ReportFileFormatsController {
	private static final Logger log = Logger.getLogger(ReportFileFormatsController.class);
	Gson gson = new Gson();
	@Autowired
	IReportFileFormatsCoreClient iReportFileFormatsCoreClient;

@RequestMapping(value="/reportfileformat/create",method=RequestMethod.POST,consumes="application/json" , produces="application/json")
public ResponseEntity<String> createReportFileFormat(@RequestBody String rffData){
	
	log.info("Inside of Fieldtypecontroller");

	HttpHeaders responseHeader = new HttpHeaders();

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	ReportFileFormatInfo  reportFileFormatInfo=gson.fromJson(rffData.toString(), ReportFileFormatInfo.class);

	boolean isValidReq = ValidationHelper.isCreateReportFileFromatReqValid(reportFileFormatInfo);

	if (isValidReq) {

		try {
			irPlusResponseDetails = iReportFileFormatsCoreClient.createReportFileFormat(reportFileFormatInfo);
		} catch (BusinessException e){
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception raised in Fieldtypecontroller:: createReportFileFormatsCoreClient", e);
		}

	} else {

		irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
		log.error("Fieldtypecontroller :: Not a Valid ReportFileFormat Object ");
	}

	if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
		return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeader, HttpStatus.OK);
	} else {
		if (!irPlusResponseDetails.isValidationSuccess()) {

			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseHeader,
					HttpStatus.CONFLICT);
		} else {
			return IRPlusRestUtil.getGenericErrorResponse();
		}
	}
}


@RequestMapping(value = "/reportfileformat/update", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
public ResponseEntity<String> updatereportfileformat(@RequestBody String rffData) {

	log.info("Inside of Fieldtypecontroller");

	HttpHeaders responseHeader = new HttpHeaders();

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	ReportFileFormatInfo  reportFileFormatInfo=gson.fromJson(rffData.toString(), ReportFileFormatInfo.class);

	boolean isValidReq = ValidationHelper.isCreateReportFileFromatReqValid(reportFileFormatInfo);

	if (isValidReq) {

		try {
			irPlusResponseDetails = iReportFileFormatsCoreClient.updateReportFileFormat(reportFileFormatInfo);
		} catch (BusinessException e){
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception raised in Fieldtypecontroller:: createReportFileFormatsCoreClient", e);
		}

	} else {

		irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
		log.error("Fieldtypecontroller :: Not a Valid ReportFileFormat Object ");
	}

	if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
		return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeader, HttpStatus.OK);
	} else {
		if (!irPlusResponseDetails.isValidationSuccess()) {

			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseHeader,
					HttpStatus.CONFLICT);
		} else {
			return IRPlusRestUtil.getGenericErrorResponse();
		}
	}
}

@RequestMapping(value = "/reportfileformat/delete/{rssId}", method = RequestMethod.GET, produces = "application/json")
public ResponseEntity<String> deleteReportfileformat(@PathVariable String rssId) {

	log.info("Inside of fieldtypecontroll");

	HttpHeaders responseHeader = new HttpHeaders();

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	boolean isValidCustomId = ValidationHelper.isValidReq(rssId);

	if (isValidCustomId) {

		try {
			
			irPlusResponseDetails = iReportFileFormatsCoreClient.deleteReportFileFormatById(rssId);
		} catch (BusinessException e){
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception raised in ReportFileFormat :: deleteReportfileformat", e);
		}

	} else {

		irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
		log.error("Fieldtypecontroller :: Not a Valid ReportFileFormat Object ");
	}

	if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
		return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeader, HttpStatus.OK);
	} else {
		if (!irPlusResponseDetails.isValidationSuccess()) {

			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseHeader,
					HttpStatus.CONFLICT);
		} else {
			return IRPlusRestUtil.getGenericErrorResponse();
		}
	}
}

@RequestMapping(value = "/reportfileformat/showall", method = RequestMethod.GET, produces = "application/json")
public ResponseEntity<String> showallReportFileFormat() {

	log.info("Inside of ReportFileFormatcontroll");
	HttpHeaders responseheaders = new HttpHeaders();

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	try {
		irPlusResponseDetails = iReportFileFormatsCoreClient.showAllReportFileFormat();

	} catch (BusinessException e) {
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		log.error("Exception raised in ReportFileFormatcontroller:: showReportFileFormatInfo", e);
	}
	if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
		return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
	} else {
		if (!irPlusResponseDetails.isValidationSuccess()) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders,
					HttpStatus.CONFLICT);
		} else {
			return IRPlusRestUtil.getGenericErrorResponse();
		}
	}
}

@RequestMapping(value = "/reportfileformat/showone/{rsffId}", method = RequestMethod.GET, produces = "application/json")
public ResponseEntity<String> showOneReportFileFormatById(@PathVariable String rsffId) {

	log.info("Inside of fieldtypecontroll");
	HttpHeaders responseheaders = new HttpHeaders();

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	boolean isValidId = ValidationHelper.isValidReq(rsffId);
		if(isValidId) {
				try {
					irPlusResponseDetails = iReportFileFormatsCoreClient.getReportFileFormatById(rsffId);
				} catch (BusinessException e) {
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
					log.error("Exception raised in ReportFileFormatByIdcontroller:: showReportFileFormatByIdInfo", e);
				}
		} else {
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
		} else {
			if (!irPlusResponseDetails.isValidationSuccess()){
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders,
						HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
}