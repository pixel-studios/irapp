package com.irplus.app;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.irplus.core.client.rolepermissions.IRolePermissionCoreClient;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.RolePrmUpdateNCreateInfo;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.rolepermission.RolepermissionsBean;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;
import com.irplus.util.RolePermissionValidationHelper;
import com.irplus.util.ValidationHelper;

@Controller
public class RolePermissonsController {

	private static final Log log = LogFactory.getLog(RolePermissonsController.class);
	
	private Gson gson = new Gson();
	
	@Autowired
	IRolePermissionCoreClient iRolePermissionCoreClient;
		
	@RequestMapping(value="/rolepermission/create",method=RequestMethod.POST,consumes="application/json",produces="application/json")
	public ResponseEntity<String> createRolePermissions(@RequestBody String rolepermissionData){
		log.info("Inside Rolepermission Controller :: create rolepermissions");
		RolepermissionsBean rolepermissionsBean = gson.fromJson(rolepermissionData.toString(), RolepermissionsBean.class);
		
		HttpHeaders responseHeaders = new HttpHeaders();
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		try {
			boolean isValidReq = RolePermissionValidationHelper.isValidRolepermissionReq(rolepermissionsBean);
		
				if (isValidReq) {
				
					irPlusResponseDetails = iRolePermissionCoreClient.createRolePermission(rolepermissionsBean);
				
				} else {
				
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				}		
		} catch (BusinessException e) {
			
			log.error("Exception in Rolepermissions", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}	

	@RequestMapping(value="/rolepermission/update",method=RequestMethod.POST,consumes="application/json",produces="application/json")
	public ResponseEntity<String> updateRolePermissions(@RequestBody String rolepermissionData){
		
		log.info("Inside Rolepermission Controller :: update rolepermissions");
	
		RolepermissionsBean rolepermissionsBean = gson.fromJson(rolepermissionData.toString(), RolepermissionsBean.class);
		
		HttpHeaders responseHeaders = new HttpHeaders();
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		try {
			boolean isValidReq = RolePermissionValidationHelper.isValidRolepermissionReq(rolepermissionsBean);
		
				if (isValidReq) {
				
					irPlusResponseDetails = iRolePermissionCoreClient.updateRolePermission(rolepermissionsBean);
				
				} else {
				
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				}		
		} catch (BusinessException e) {
			
			log.error("Exception in Rolepermissions", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}	
	
	@RequestMapping(value="/rolepermission/showOne/{rolepermissionId}",method=RequestMethod.GET,produces="application/json")
	public ResponseEntity<String> showOneRolepermissionById(@PathVariable String rolepermissionId){
	
		HttpHeaders responseHeaders = new HttpHeaders();
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		log.info("Rolepermission Id ::"+rolepermissionId );
		
		try {
			boolean isValidId = RolePermissionValidationHelper.isValidRolepermissionId(rolepermissionId);
			
				if (isValidId) {
				irPlusResponseDetails =	iRolePermissionCoreClient.getRolePermissionById(rolepermissionId);	
				}else{
					irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				}		
	
		} catch (BusinessException e){
			log.error("Exception in get permission", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}		
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
			{ 				
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
			} 
			else 
			{
				if(!irPlusResponseDetails.isValidationSuccess())
				{
					return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
				}
				else
				{
					return IRPlusRestUtil.getGenericErrorResponse();
				}
			}	
	}	
	
	@RequestMapping(value="/rolepermission/status/{rolepermissionId}/{status}",method=RequestMethod.GET,produces="application/json")
	public ResponseEntity<String> updateStatusRolePrmn(@PathVariable String rolepermissionId ,@PathVariable String status){
	
		HttpHeaders responseHeaders = new HttpHeaders();
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		log.info("Rolepermission Id ::"+rolepermissionId);
		
		StatusIdInfo statusIdInfo = new StatusIdInfo();
		statusIdInfo.setId(Integer.parseInt(rolepermissionId));
		statusIdInfo.setStatus(Integer.parseInt(status));
		
		try {
			boolean isValidId1 = ValidationHelper.isValidReq(status);
			boolean isValidId2 = ValidationHelper.isValidReq(rolepermissionId);
			
				if (isValidId1 && isValidId2) {
				irPlusResponseDetails =	iRolePermissionCoreClient.updateStatusRolePrmn(statusIdInfo);	
				}else{
					irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				}		
	
		} catch (BusinessException e){
			log.error("Exception in get permission", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}		
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
			{ 				
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
			} 
			else 
			{
				if(!irPlusResponseDetails.isValidationSuccess())
				{
					return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
				}
				else
				{
					return IRPlusRestUtil.getGenericErrorResponse();
				}
			}	
	}	
	@RequestMapping(value="/rolepermission/showAll",method=RequestMethod.GET ,produces = "application/json" )
	public ResponseEntity<String> showAllPermissions(){
		
		log.debug("Show All Permissions");
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		HttpHeaders responseHeaders = new HttpHeaders();
		try {
			irPlusResponseDetails=iRolePermissionCoreClient.showAllRolePermission();
		} catch (BusinessException e) {
			log.error("Exception Raised Show All Permissions :: Exception ::"+e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		}
		
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseHeaders, HttpStatus.OK);
			
		} else {
			if (!irPlusResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	@RequestMapping(value="/rolepermission/delete/{rolepermissionId}",method=RequestMethod.GET ,produces = "application/json" )
	public ResponseEntity<String> deletePermissionId(@PathVariable String rolepermissionId){
		
		log.debug("Delete RolePermission"+rolepermissionId);
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		HttpHeaders responseHeaders = new HttpHeaders();
		try {
			boolean isValidId = RolePermissionValidationHelper.isValidRolepermissionId(rolepermissionId);
			if (isValidId) {
				irPlusResponseDetails=iRolePermissionCoreClient.deleteRolePermissionById(rolepermissionId);	
			} else {
				log.error("No Record found :: irpermissions ");				
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}
		
		} catch (BusinessException e) {
		
			log.error("Exception Raised RolePermissions :: Exception ::"+e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		}
		
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseHeaders, HttpStatus.OK);
			
		} else {
			if (!irPlusResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	
	
	// two methods
	
/*	
 * public IRPlusResponseDetails showAllRoleNMdl(StatusIdInfo statusIdInfo)	throws BusinessException ;
   public IRPlusResponseDetails updateRoleprmUpdNCrt(RolePrmUpdateNCreateInfo rolepermUpNCrte)	throws BusinessException ;
*/
	/*GetOne Role populate all modules*/
	
	@RequestMapping(value="/rolepermission/showOneRolePopup/{roleid}",method=RequestMethod.GET ,produces = "application/json" )
	public ResponseEntity<String> showAllRoleNMdl(@PathVariable String roleid){
		
		log.debug("Inside of Rolepermission Controller:: Show One RolieId :: showOneRolePopup");
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		HttpHeaders responseHeaders = new HttpHeaders();
		
		
		
		try {
			
			StatusIdInfo statusIdInfo = new StatusIdInfo();
			
			statusIdInfo.setId(Integer.parseInt(roleid));
						
			boolean isValidId =	ValidationHelper.isValidReq(roleid);
		
			log.debug(" Show One RolieId :: showOneRolePopup "+roleid);
			
			if(isValidId){
				
			statusIdInfo.setId(Integer.parseInt(roleid));
			
			irPlusResponseDetails = iRolePermissionCoreClient.showAllRoleNMdl(statusIdInfo);
			}
		} catch (BusinessException e) {
			log.error("Exception Raised Show All Permissions :: Exception ::"+e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		}
		
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseHeaders, HttpStatus.OK);
			
		} else {
			if (!irPlusResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}


	@RequestMapping(value="/rolepermission/update/crtupt",method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public ResponseEntity<String> updateRoleprmUpdNCrt(@RequestBody String  rolepermUpNCrteData){
		
		log.info("Inside Rolepermission Controller :: update rolepermissions");
	
		RolePrmUpdateNCreateInfo rolepermissionsBean = gson.fromJson(rolepermUpNCrteData.toString(), RolePrmUpdateNCreateInfo.class);
		
		HttpHeaders responseHeaders = new HttpHeaders();
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		boolean isValidReq = false;
				
		try{
			if(rolepermissionsBean != null ){
				isValidReq = true;
			}
				if (isValidReq) {
				
					irPlusResponseDetails = iRolePermissionCoreClient.updateRoleprmUpdNCrt(rolepermissionsBean);
				
				} else {
				
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				}		
		}catch (BusinessException e) {
			
			log.error("Exception in Rolepermissions", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}	

}