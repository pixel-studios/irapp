package com.irplus.app;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.irplus.core.client.roles.IRolesCoreClient;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.roles.RoleBean;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;
import com.irplus.util.RoleValidationHelper;
import com.irplus.util.ValidationHelper;

@Controller
public class RoleController {

	private static final Log log = LogFactory.getLog(RoleController.class);
	
	Gson gson = new Gson(); 
	
	@Autowired
	IRolesCoreClient rolesCoreClient;
	
	//createRole
	@RequestMapping(value="/role/create",method=RequestMethod.POST ,consumes="application/json" , produces="application/json" )
	public ResponseEntity<String> createRole(@RequestBody String roleBeanData){
		
		log.info(" Inside of RoleController :: roleCreate");
		
		HttpHeaders responseHeader = new HttpHeaders();
		
		IRPlusResponseDetails roleResponseDetails = new IRPlusResponseDetails();
				
		RoleBean roleBean = gson.fromJson(roleBeanData.toString(), RoleBean.class);
		
		
		boolean isValidReq = RoleValidationHelper.isValidRoleReq(roleBean);
		if (isValidReq) {

					try {
					
						roleResponseDetails = rolesCoreClient.createRole(roleBean);
		
					} catch (BusinessException e) {
						
						roleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
						roleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
						
						log.error("Exception raised in RoleController :: createRole",e);
					}
	
		} else {
			
			roleResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			roleResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);

			log.error("RoleController :: Not a Valid RoleBean Object ");
		}		
			
		if (roleResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<>(gson.toJson(roleResponseDetails),responseHeader ,HttpStatus.OK);
		} 
		else {
			if (!roleResponseDetails.isValidationSuccess()) {
		
				return new ResponseEntity<String>(gson.toJson(roleResponseDetails), responseHeader, HttpStatus.CONFLICT);
			
			} else {			
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}				
	}	
	
	
	//createRole without duplicate name
	
		@RequestMapping(value="/role/create_no_duplicate",method=RequestMethod.POST ,consumes="application/json" , produces="application/json" )
		public ResponseEntity<String> removeDuplicateRole(@RequestBody String roleBeanData){
			
			log.info(" Inside of RoleController :: roleCreate");
			
			HttpHeaders responseHeader = new HttpHeaders();
			
			IRPlusResponseDetails roleResponseDetails = new IRPlusResponseDetails();
					
			RoleBean roleBean = gson.fromJson(roleBeanData.toString(), RoleBean.class);
						
			boolean isValidReq = RoleValidationHelper.isValidRoleReq(roleBean);
			
			if (isValidReq) {
				
						try {						
							roleResponseDetails = rolesCoreClient.removeDuplicateRole(roleBean);
			
						} catch (BusinessException e) {
							
							roleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
							roleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
							
							log.error("Exception raised in RoleController :: createRole",e);
						}
		
			} else {
				
				roleResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				roleResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);

				log.error("RoleController :: Not a Valid RoleBean Object ");
			}		
				
			if (roleResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<>(gson.toJson(roleResponseDetails),responseHeader ,HttpStatus.OK);
			} 
			else {
				if (!roleResponseDetails.isValidationSuccess()) {
			
					return new ResponseEntity<String>(gson.toJson(roleResponseDetails), responseHeader, HttpStatus.CONFLICT);
				
				} else {			
					return IRPlusRestUtil.getGenericErrorResponse();
				}
			}				
		}	
		
	//getrole
	@RequestMapping(value="/role/showOne/{roleId}",method=RequestMethod.GET,produces="application/json")
	
	public ResponseEntity<String> getRoleById(@PathVariable String roleId){
		
		log.info("Role::" + roleId);
		HttpHeaders responseheaders = new HttpHeaders(); 
		IRPlusResponseDetails roleResponseDetails = new  IRPlusResponseDetails();
		
		boolean isValidRoleId = RoleValidationHelper.isValidRoleId(roleId);
		
		if (isValidRoleId) {							
				try{										
						roleResponseDetails = rolesCoreClient.getRoleById(roleId);				
					} catch (BusinessException e) {
						roleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
						roleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);						
						log.error("Exception raised in RoleController :: getRole",e);
					}
				
		} else {
			roleResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			roleResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}
		if (roleResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(roleResponseDetails), responseheaders, HttpStatus.OK);
		} else {
				if (!roleResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(roleResponseDetails),responseheaders, HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
		}
	}
	
	//update
	@RequestMapping(value="/role/update",method = RequestMethod.POST,consumes = "application/json" ,produces ="application/json")
	public ResponseEntity<String> updateRole(@RequestBody String roleBeanData){
		
		
		HttpHeaders responseheaders = new HttpHeaders(); 
		IRPlusResponseDetails roleResponseDetails = new  IRPlusResponseDetails();
		
		RoleBean roleBean = gson.fromJson(roleBeanData.toString(), RoleBean.class);
		
		boolean isValidReq = RoleValidationHelper.isValidRoleReq(roleBean);
		log.info("UpdateRole " +roleBean.getRoleid() );
		
				if (isValidReq) {
							try {
							
								roleResponseDetails = rolesCoreClient.updateRole(roleBean);
							
							} catch (BusinessException e) {
								
								roleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
								roleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
								log.error("Exception raised in RoleController :: getRole",e);
							}
				} else {
					roleResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
					roleResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				}
				
				if (roleResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
					return new ResponseEntity<String>(gson.toJson(roleResponseDetails), responseheaders, HttpStatus.OK);
				}
				else {
						if (!roleResponseDetails.isValidationSuccess()) {
							return new ResponseEntity<String>(gson.toJson(roleResponseDetails),responseheaders, HttpStatus.CONFLICT);
						} else {
							return IRPlusRestUtil.getGenericErrorResponse();
						}
				}		
	}
	//delte
	@RequestMapping(value="role/delete/{roleId}",method=RequestMethod.GET,produces="application/json")
	public ResponseEntity<String> deleteRoleId(@PathVariable String roleId){
		
		HttpHeaders responseheaders = new HttpHeaders(); 
		IRPlusResponseDetails roleResponseDetails = new  IRPlusResponseDetails();
		
		boolean isValidRoleId = RoleValidationHelper.isValidRoleId(roleId);
		
		if (isValidRoleId) {
						
					try {
							roleResponseDetails = rolesCoreClient.deleteRoleById(roleId);
						} catch (BusinessException e) {

							roleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
							roleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
							log.error("Exception raised in RoleController :: deleteRole",e);
					
						}
		} else {
			
			roleResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			roleResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
	
		}
				
		if (roleResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(roleResponseDetails), responseheaders, HttpStatus.OK);
		}
		else {
				if (!roleResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(roleResponseDetails),responseheaders, HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
		}			
}
	
	
		@RequestMapping(value="role/status/{roleId}/{status}",method=RequestMethod.GET,produces="application/json")
		public ResponseEntity<String> updateStatusRole(@PathVariable String roleId ,@PathVariable String status){
			
			HttpHeaders responseheaders = new HttpHeaders(); 
			
			IRPlusResponseDetails roleResponseDetails = new  IRPlusResponseDetails();
			
			boolean isValidRoleId = ValidationHelper.isValidReq(roleId);
			boolean isValidStatus = ValidationHelper.isValidReq(status);
			
			Integer status1 = Integer.parseInt(status);
			Integer roleid = Integer.parseInt(roleId);
			StatusIdInfo statusIdInfo = new StatusIdInfo();
			
			statusIdInfo.setId(roleid);
			statusIdInfo.setStatus(status1);
			
			if (isValidRoleId) {
							
						try {
								roleResponseDetails = rolesCoreClient.updateRoleStatus(statusIdInfo);
							} catch (BusinessException e) {

								roleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
								roleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
								log.error("Exception raised in RoleController :: Status update Role",e);
						
							}
			} else {
				
				roleResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				roleResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
		
			}
					
			if (roleResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(roleResponseDetails), responseheaders, HttpStatus.OK);
			}
			else {
					if (!roleResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(roleResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}			
	}
		
		/*This show roles for the Roles purpose 
		 * Here shows Active and Inactive also
		 * */
	
	@RequestMapping(value="/role/showAll",method=RequestMethod.GET,produces="application/json")
	public ResponseEntity<String> showAllRoles(){

		HttpHeaders responseheaders = new HttpHeaders(); 
		IRPlusResponseDetails roleResponseDetails = new  IRPlusResponseDetails();
		
			try {
				roleResponseDetails = rolesCoreClient.ShowAllRoles();
				
			} catch (BusinessException e) {

				roleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				roleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				log.error("Exception raised in RoleController :: deleteRole",e);
			}
			
			if (roleResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(roleResponseDetails), responseheaders, HttpStatus.OK);
			}
			else {
					if (!roleResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(roleResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}				
	}
	
	/*
	 * This show roles for the Rolepermission purpose 
	 * Here Active roles only shows
	 * 
	 * */
	
	@RequestMapping(value="/roleprm/showAll",method=RequestMethod.GET,produces="application/json")
	public ResponseEntity<String> showAllRoles1(){

		HttpHeaders responseheaders = new HttpHeaders(); 
		IRPlusResponseDetails roleResponseDetails = new  IRPlusResponseDetails();
		
			try {
				roleResponseDetails = rolesCoreClient.ShowAllRoles_roleprm();
				
			} catch (BusinessException e) {

				roleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				roleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				log.error("Exception raised in RoleController :: deleteRole",e);
			}
			
			if (roleResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(roleResponseDetails), responseheaders, HttpStatus.OK);
			}
			else {
					if (!roleResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(roleResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}				
	}
	
	
}