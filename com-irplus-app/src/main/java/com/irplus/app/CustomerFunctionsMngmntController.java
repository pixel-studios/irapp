package com.irplus.app;

import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.irplus.core.client.customerFunctionsMgnt.ICustomerFunctionsMngmntCoreClient;
import com.irplus.dto.CustomerFunctionInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;
import com.irplus.util.ValidationHelper;

@Controller
public class CustomerFunctionsMngmntController {

	private static final Logger log = Logger.getLogger(CustomerFunctionsMngmntController.class); 
	
	@Autowired
	ICustomerFunctionsMngmntCoreClient iCustomerFunctionsMngmntCoreClient;
	
	Gson gson = new Gson();
	
	@RequestMapping(value="/customer/funtions/create" ,method = RequestMethod.POST , consumes="application/json" ,produces="application/json")	
	public ResponseEntity<String> createCustomerFunction(@RequestBody String customerFunctionsData){
		
		log.debug("Inside of CustomerFunctionsMngmntController");
		
		HttpHeaders responseHeaders = new HttpHeaders();
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		List<String> errMsgs = new ArrayList<String>();
		
		CustomerFunctionInfo customerFunctionInfo = gson.fromJson(customerFunctionsData.toString(), CustomerFunctionInfo.class);
		
		System.out.println(customerFunctionInfo);
		
		log.debug("Arjun :"+customerFunctionInfo);
		
		boolean isValidReq = false;
		
		try {
			
			isValidReq = ValidationHelper.isValidCustomerFuntnReq(customerFunctionInfo);		
			
			if(isValidReq){
				irPlusResponseDetails	= iCustomerFunctionsMngmntCoreClient.createCustomerFunctions(customerFunctionInfo);
			}
			else{
				errMsgs.add("Validation Error : Please Check Availability of the UserId , CustomerId ,BankBranchId");
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				log.debug("Validation Error raised in CustomerFunctionsController :: CreateCustomerfunction");
					
			}
		} catch (BusinessException e) {
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception raised in CustomerFunctionsController :: CreateCustomerfunction",e);
			
		}
		
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails),responseHeaders ,HttpStatus.OK);
		
		} else {
			
				if (!irPlusResponseDetails.isValidationSuccess()) {
			
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
				
				} else {			
					return IRPlusRestUtil.getGenericErrorResponse();
				}
			
		}
		
	}

			
		@RequestMapping(value="/customerFunctn/showOne/{customerFunId}",method=RequestMethod.GET,produces="application/json")
			
		public ResponseEntity<String> showOneCustomerFunctnById(@PathVariable String customerFunId){
					
			log.debug("Inside CustomerFunctionsMngmntController : showOneCustomerFuntnById");
			
			HttpHeaders responseheaders = new HttpHeaders(); 
			
			IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
			
			try {
				
				log.info("Inside Customer Functions Controller :: customer/showOne() " + customerFunId);
							
				boolean isValidCustomerLicenceId = ValidationHelper.isValidReq(customerFunId);
				
				if (isValidCustomerLicenceId) {							
															
						irPlusResponseDetails = iCustomerFunctionsMngmntCoreClient.getCustomerFunctionsById(customerFunId);
						
				} else {
					
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				}
			
			} catch (BusinessException e) {

				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				
				log.error("Exception raised at Customer FunctionsMngmnt Controller :",e);
			}

			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			} else {

				if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}
		}
	
		@RequestMapping(value="/customer/funtions/update" ,method = RequestMethod.POST ,	consumes="application/json" ,produces="application/json")	
		public ResponseEntity<String> updateCustomerFunction(@RequestBody String customerFunctionsData){
			
			log.debug("Inside of CustomerFunctionsMngmntController :updatefunction()");
			
			HttpHeaders responseHeaders = new HttpHeaders();
			
			IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
			
			List<String> errMsgs = new ArrayList<String>();
			
				
		//	log.debug("Arjun :"+customerFunctionInfo);
			
			boolean isValidReq = false;
			
			try {
				CustomerFunctionInfo customerFunctionInfo = gson.fromJson(customerFunctionsData.toString(), CustomerFunctionInfo.class);
				
				System.out.println(customerFunctionInfo);
			
				
				isValidReq = ValidationHelper.isValidCustomerFuntnReq(customerFunctionInfo);		
				
				if(isValidReq){
					irPlusResponseDetails	= iCustomerFunctionsMngmntCoreClient.updateCustomerFunctions(customerFunctionInfo);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				}
				else{
					errMsgs.add("Validation Error : Please Check Availability of the UserId , CustomerId ,BankBranchId");
					irPlusResponseDetails.setErrMsgs(errMsgs);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
					log.debug("Validation Error raised in CustomerFunctionsController :: CreateCustomerfunction");
						
				}
			} catch(BusinessException e){
				
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				log.error("Exception raised in CustomerFunctionsController :: UpdateCustomerfunction",e);
				
			}
			
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails),responseHeaders ,HttpStatus.OK);
			
			} else {
				
					if (!irPlusResponseDetails.isValidationSuccess()) {
				
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
					
					} else {			
						return IRPlusRestUtil.getGenericErrorResponse();
					}				
			}
			
		}

		
}
