package com.irplus.app;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.irplus.core.client.module.IModuleCoreClient;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.module.ModuleBean;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;
import com.irplus.util.ModuleValidationHelper;
import com.irplus.util.ValidationHelper;

@Controller
public class ModuleController {
			
	private static final Logger log= Logger.getLogger(ModuleController.class);
	Gson gson = new Gson();
	
	@Autowired		
	IModuleCoreClient moduleCoreClient;
	
  	 /* Without Duplicate Create module  */
	
	@RequestMapping(value="/module/create/nodup",method=RequestMethod.POST, produces="application/json", consumes="application/json")
	@ResponseBody
	public ResponseEntity<String> createNoDupModule(@RequestBody String requestInputData){
		
		HttpHeaders responseHeaders = new HttpHeaders(); 
		
		IRPlusResponseDetails moduleResponseDetails = new IRPlusResponseDetails();
		
		ModuleBean moduleBean = gson.fromJson(requestInputData.toString(), ModuleBean.class);
		
		log.info("Inside Controller layer :: json object we got" +moduleBean.getModulename());
		boolean flag = false;
		
		try {
			
			boolean isValidReq = ValidationHelper.isValidModuleReq(moduleBean);
			if(isValidReq) {
	
				moduleResponseDetails=moduleCoreClient.createModuleNoDuplct(moduleBean);
						
			}else {
				moduleResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				moduleResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
		}
		} catch (BusinessException e) {
			moduleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception in Create Menu", e);
		}
	
		if(moduleResponseDetails.getStatusMsg().equalsIgnoreCase( IRPlusConstants.SUCCESS_MSG)) {

			return new ResponseEntity<>(gson.toJson(moduleResponseDetails), responseHeaders, HttpStatus.OK);
			
		}
		else
		{
			if(!moduleResponseDetails.isValidationSuccess()) 
			{
				return new ResponseEntity<>(gson.toJson(moduleResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}

	
	
	/*@RequestMapping(value="/module/create",method=RequestMethod.POST, produces="application/json", consumes="application/json")
	@ResponseBody
	public ResponseEntity<String> createModule(@RequestBody String requestInputData){
		
		HttpHeaders responseHeaders = new HttpHeaders(); 
		
		IRPlusResponseDetails moduleResponseDetails = new IRPlusResponseDetails();
		
		ModuleBean moduleBean = gson.fromJson(requestInputData.toString(), ModuleBean.class);
		
		log.info("Inside Controller layer :: json object we got" +moduleBean.getModulename());
		boolean flag = false;
		
		try {
			
			boolean isValidReq = ValidationHelper.isValidModuleReq(moduleBean);
			if(isValidReq) {
	
				moduleResponseDetails=moduleCoreClient.findAllModules();
				List<ModuleBean> listModule = moduleResponseDetails.getModule();
 				
					if(listModule.isEmpty()){
						moduleResponseDetails=moduleCoreClient.createModuleNoDuplct(moduleBean);
						moduleResponseDetails.setValidationSuccess(true);
						moduleResponseDetails.setRecordsTotal(1);
						moduleResponseDetails.setRecordsFiltered(1);
						moduleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
						moduleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
					}else{
						for (ModuleBean moduleBean2 : listModule) {
							if(moduleBean.getModulename().equalsIgnoreCase(moduleBean2.getModulename())){
							flag=true;
							break;
							}
						}
						if(flag==false){
							
							moduleResponseDetails=moduleCoreClient.createModule(moduleBean);
							
							moduleResponseDetails.setValidationSuccess(true);
							moduleResponseDetails.setRecordsTotal(1);
							moduleResponseDetails.setRecordsFiltered(1);
							moduleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
							moduleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
						}else{
							moduleResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
							moduleResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
						}
					}
				
			}else {
				moduleResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				moduleResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
		}
		} catch (BusinessException e) {
			moduleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception in Create Menu", e);
		}
	
		if(moduleResponseDetails.getStatusMsg().equalsIgnoreCase( IRPlusConstants.SUCCESS_MSG)) {

			return new ResponseEntity<>(gson.toJson(moduleResponseDetails), responseHeaders, HttpStatus.OK);
			
		}
		else
		{
			if(!moduleResponseDetails.isValidationSuccess()) 
			{
				return new ResponseEntity<>(gson.toJson(moduleResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}*/
// not working functionality
	
	/*@RequestMapping(value="/module/create/unique",method=RequestMethod.POST, produces="application/json", consumes="application/json")
	@ResponseBody
	public ResponseEntity<String> createModuleUnique(@RequestBody String requestInputData){
		
		HttpHeaders responseHeaders = new HttpHeaders();
		IRPlusResponseDetails moduleResponseDetails = new IRPlusResponseDetails();
		
		ModuleBean moduleBean = gson.fromJson(requestInputData.toString(), ModuleBean.class);
		
		log.info("Inside Controller layer :: json object we got" +moduleBean.getModulename());
		
		try {
			
			boolean isValidReq = ModuleValidationHelper.isValidModuleReq(moduleBean);
			if(isValidReq) {
				moduleResponseDetails=moduleCoreClient.createModuleNoDuplct(moduleBean);
				moduleResponseDetails.setValidationSuccess(true);
			}else {
				
				moduleResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				moduleResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}			
			
		} catch (BusinessException e) {
			moduleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception in Create Menu", e);
		}
	
		if(moduleResponseDetails.getStatusMsg().equalsIgnoreCase( IRPlusConstants.SUCCESS_MSG)) {

			return new ResponseEntity<>(gson.toJson(moduleResponseDetails), responseHeaders, HttpStatus.OK);
			
		}
		else
		{
			if(!moduleResponseDetails.isValidationSuccess()) 
			{
				return new ResponseEntity<>(gson.toJson(moduleResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}*/
	
	
	@RequestMapping(value="/module/delete/{moduleId}",method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> deleteModuleById(@PathVariable String moduleId){
	
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails moduleResponseDetails = new IRPlusResponseDetails();
		
		log.info("menuid::" + moduleId);
		try
		{
			boolean isValidReq = ModuleValidationHelper.isValidModuleId(moduleId);
		
		System.out.println("given no is isValidReq :" + isValidReq);
		
			if (isValidReq)
			{
				moduleResponseDetails=this.moduleCoreClient.deleteModuleById(moduleId);
				moduleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
			else
			{				
				moduleResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
		} catch (BusinessException e) {

			log.error("Exception in deleteModule", e);
			moduleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (moduleResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(moduleResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!moduleResponseDetails.isValidationSuccess()) //isvalidatioln is true , false ==> condition loop false
			{
				return new ResponseEntity<>(gson.toJson(moduleResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
		
	
	//Update or Edit
	@RequestMapping(value="/module/update",method=RequestMethod.POST,consumes="application/json",produces="application/json")
	@ResponseBody
	public ResponseEntity<String> updateModule(@RequestBody String requestInputData){
		
		HttpHeaders responseHeaders = new HttpHeaders();
		
		IRPlusResponseDetails moduleResponseDetails = new IRPlusResponseDetails();	
		ModuleBean moduleBean = gson.fromJson(requestInputData.toString(), ModuleBean.class);
		
		log.info(" moduleBean:: " + moduleBean);
		
		try {
		
			boolean isValidReq = ModuleValidationHelper.isValidModuleReq(moduleBean);
			
			if(isValidReq) {
				
				moduleResponseDetails=moduleCoreClient.updateModule(moduleBean);
				moduleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			}else {				
				moduleResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				moduleResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}			
			
		} catch (BusinessException e) {
			moduleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception in Create Menu", e);
		}

		if (moduleResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			// successfully Response create
			
			return new ResponseEntity<>(gson.toJson(moduleResponseDetails), responseHeaders, HttpStatus.OK);
			
		} else {
			
			if (!moduleResponseDetails.isValidationSuccess()) {
				
				return new ResponseEntity<>(gson.toJson(moduleResponseDetails), responseHeaders,HttpStatus.CONFLICT);
			} 
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}	
	}
	
	
	@RequestMapping(value="/module/show/list",method=RequestMethod.GET,produces="application/json")
	@ResponseBody
	public ResponseEntity<String> showModuleList(){
		
		HttpHeaders responseHeaders= new HttpHeaders();
		
		IRPlusResponseDetails moduleResponseDetails = new IRPlusResponseDetails();
		//moduleResponseDetails.
		
		log.info("");
		
		try {
			
			moduleResponseDetails=this.moduleCoreClient.findAllModules();
			
			if (!moduleResponseDetails.getModule().isEmpty()) {
				log.debug("Module list show :: inside controller");
				moduleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				moduleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			} 
			else {
				moduleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);			
			}
		
		} catch (BusinessException e) {			
			log.error("Exception in update Module",e);				
			moduleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		
		
		if (moduleResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			// successfully Response create
			
			return new ResponseEntity<>(gson.toJson(moduleResponseDetails), responseHeaders, HttpStatus.OK);
			
		} else {
			
			if (!moduleResponseDetails.isValidationSuccess()) {
				
				return new ResponseEntity<>(gson.toJson(moduleResponseDetails), responseHeaders,HttpStatus.CONFLICT);
			} 
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}

	@RequestMapping(value="/module/show/{moduleId}",method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<String> showOneModule(@PathVariable String moduleId){
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails moduleResponseDetails = new IRPlusResponseDetails();
		log.info("Inside of modulecontroll :: moduleid :: " + moduleId);
		try
		{
			boolean isValidReq = ModuleValidationHelper.isValidModuleId(moduleId);
		
			System.out.println("given no is isValidReq :" + isValidReq);
		
			if (isValidReq)
			{
				moduleResponseDetails=this.moduleCoreClient.getModuleById(moduleId);
//				moduleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
			else
			{
				
				moduleResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
		} catch (BusinessException e) {

			log.error("Exception in showOneModule", e);
			moduleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (moduleResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(moduleResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!moduleResponseDetails.isValidationSuccess()) //isvalidatioln is true , false ==> condition loop false
			{
				return new ResponseEntity<>(gson.toJson(moduleResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	
	@RequestMapping(value="/module/get_moduleid_bean/{moduleid}",method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<String> getActiveModule(@PathVariable String moduleid){
		HttpHeaders responseHeaders = new HttpHeaders();
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		IRPlusResponseDetails moduleResponseDetails = new IRPlusResponseDetails();
		
		log.info("Inside of modulecontroll :: moduleid :: " + moduleid);
		try
		{
			boolean isValidReq = ModuleValidationHelper.isValidModuleId(moduleid);
			
			System.out.println("given no is isValidReq :" + isValidReq);
			
			ModuleBean mbean=new ModuleBean();
			if (isValidReq)
			{
		
				moduleResponseDetails = moduleCoreClient.findAllModules();
				List<ModuleBean> listModule=moduleResponseDetails.getModule();

				for (ModuleBean moduleBean : listModule) {
					if(Integer.parseInt(moduleid)==(moduleBean.getModuleid())){
						mbean = moduleBean;
					}
				}
				if(mbean!=null){
					
					irPlusResponseDetails.setModuleBean(mbean);
					irPlusResponseDetails.setValidationSuccess(true);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
					}
				
			}
			else
			{
				
				moduleResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
		} catch (BusinessException e) {

			log.error("Exception in showOneModule", e);
			moduleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess()) //isvalidatioln is true , false ==> condition loop false
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}		

/*getting status active modules list only*/
	@RequestMapping(value="/module/show_active_module_list",method=RequestMethod.GET,produces="application/json")
	@ResponseBody
	public ResponseEntity<String> showActiveModuleList(){
		
		HttpHeaders responseHeaders= new HttpHeaders();
		IRPlusResponseDetails moduleResponseDetails1 = new IRPlusResponseDetails();
		IRPlusResponseDetails moduleResponseDetails = new IRPlusResponseDetails();
		//moduleResponseDetails.
		List<ModuleBean> mlist= null;
		List<ModuleBean> mlist1 = new ArrayList<ModuleBean>(); 
		log.info("inside showActiveModuleList");
		
		try {
			
			moduleResponseDetails=this.moduleCoreClient.findAllModules();
			mlist=moduleResponseDetails.getModule();
			
			for (ModuleBean modBean : mlist) {
				if(modBean!=null && modBean.getIsactive()!=null){
					if(modBean.getIsactive()==1){
						mlist1.add(modBean);
					}
				}
			}
			
			moduleResponseDetails=moduleResponseDetails1;
			
			if(mlist1 != null){
			moduleResponseDetails.setModule(mlist1);
			moduleResponseDetails.setRecordsTotal(mlist1.size());
			moduleResponseDetails.setRecordsFiltered(mlist1.size());
			}
			
			if (!moduleResponseDetails.getModule().isEmpty()) {
				log.debug("Module list show :: inside controller");
				moduleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				moduleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			} 
			else {
				moduleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);			
			}
		
		} catch (BusinessException e) {			
			log.error("Exception in update Module",e);				
			moduleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		
		
		if (moduleResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			// successfully Response create
			
			return new ResponseEntity<>(gson.toJson(moduleResponseDetails), responseHeaders, HttpStatus.OK);
			
		} else {
			
			if (!moduleResponseDetails.isValidationSuccess()) {
				
				return new ResponseEntity<>(gson.toJson(moduleResponseDetails), responseHeaders,HttpStatus.CONFLICT);
			} 
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}

		}
	}

/*isactive status update 	*/
	
/* Status Modifying Active and Inactive	*/
	
	@RequestMapping(value="/module/status/{moduleid}/{status}",method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> statusModule(@PathVariable String moduleid , @PathVariable String status)
	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails moduleResponseDetails=new IRPlusResponseDetails();
		log.info("menuid::" + moduleid);
		List<String> errMsg = new ArrayList<String>();
		
			Integer status1 = Integer.parseInt(status);
		
		StatusIdInfo sid = new StatusIdInfo();
		sid.setId(Integer.parseInt(moduleid));
		sid.setStatus(status1);
		try
		{
		boolean isValidReq1 = ValidationHelper.isValidReq( moduleid);
		boolean isValidReq2 = ValidationHelper.isValidReq(status);
		log.debug("given no is isValidReq :" + isValidReq1 );
			if (isValidReq1 && isValidReq2 )
			{
		//		menuResponseDetails=this.irMenuMgmntCoreClient.deleteMenu(menuid);
				moduleResponseDetails=this.moduleCoreClient.updateModuleStatus(sid);
				moduleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
			else
			{
				errMsg.add("Error : Id and Status Validation Error");
				moduleResponseDetails.setErrMsgs(errMsg);
				moduleResponseDetails.setValidationSuccess(false);
				moduleResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
		} catch (BusinessException e) {

			log.error("Exception in deleteMenu", e);
			moduleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (moduleResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(moduleResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!moduleResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(moduleResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	
//close
}
