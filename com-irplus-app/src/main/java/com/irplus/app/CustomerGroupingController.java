package com.irplus.app;

import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.irplus.core.client.customer_grouping.ICustomerGroupingMngmntCoreClient;
import com.irplus.dto.BankFilterInfo;
import com.irplus.dto.BankInfo;
import com.irplus.dto.CustomerGroupingDetails;
import com.irplus.dto.CustomerGroupingMngmntInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;
import com.irplus.util.ValidationHelper;

@Controller
@RequestMapping(value = "/custmrGrp")
public class CustomerGroupingController {
	private static final Logger LOGGER = Logger.getLogger(CustomerGroupingController.class);
	private static final Logger log = Logger.getLogger(CustomerGroupingController.class);
	
	@Autowired
	ICustomerGroupingMngmntCoreClient iCustomerGroupingMngmntCoreClient;	

	Gson gson = new Gson();

/*	@RequestMapping(value = "/create" , method=RequestMethod.POST ,produces = "application/json" , consumes = "application/json")
	public ResponseEntity<String> createGrouping(@RequestBody String CustomerGroupingMngmntInfoData){
		
	log.info("Inside of CustomerLicenceControll :: CreateCustomerLicence");
		
		HttpHeaders responseHeader = new HttpHeaders();
				
		List<String> errMsgs = new ArrayList<String>();
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 

		try{
			
			CustomerGroupingMngmntInfo customerGroupingMngmntInfo= gson.fromJson(CustomerGroupingMngmntInfoData.toString(), CustomerGroupingMngmntInfo.class);	
			
			
	//		if (customerMapSupportInfo.getCustomerMappingInfo()!= null) {
				
				irPlusResponseDetails = iCustomerGroupingMngmntCoreClient.createCustomerGrp(customerGroupingMngmntInfo);	
			} else {

				errMsgs.add("Validation Error : Please Check CustomerMappingInfo is Empty");
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				log.debug("Validation Error raised in CustomerMappingInfoDataController :: CreateCustomerMappingInfo");
			}
			
			
		} catch (BusinessException e) {
			
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			log.error("Exception raised in CustomerGroupingMngmntInfoController :: createCustomerGrp",e);
		}
	
		if(irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG))
		{
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeader, HttpStatus.OK);
		}else
		{
			if (!irPlusResponseDetails.isValidationSuccess()) {
	
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseHeader, HttpStatus.CONFLICT);
	
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
		
	@RequestMapping(value="/CustomrGrpDtalsViewbyId/{customerGroupingMngmntId}",method=RequestMethod.GET,produces="application/json")
		
		public ResponseEntity<String> getCustomrGrpDtalsViewById(@PathVariable String customerGroupingMngmntId){
					
			log.debug("Inside CustomerGroupingController : getCustomrGrpDtalsViewById");
			
			List<String> errMsgs = new ArrayList<String>();
			
			HttpHeaders responseheaders = new HttpHeaders(); 
			
			IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
			
			try {
				
				log.info("Inside Customer mapping Controller :: customer/showOne() " + customerGroupingMngmntId);
							
				boolean isValidCustomerId = ValidationHelper.isValidReq(customerGroupingMngmntId);
				
				if (isValidCustomerId) {							
															
						irPlusResponseDetails = iCustomerGroupingMngmntCoreClient.getCustomrGrpDtalsViewById(customerGroupingMngmntId);
						
				} else {
					errMsgs.add("Validation Error : Not valid customerGroupingMngmntId");
					irPlusResponseDetails.setErrMsgs(errMsgs);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
					}
			
			} catch (BusinessException e) {
	
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				
				log.error("Exception raised at CustomerGroupingController :",e);
			}
	
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			} else {
	
				if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}
		}
	
		@RequestMapping(value="/showall",method=RequestMethod.GET,produces="application/json")
		
		public ResponseEntity<String> showAllCustomerGrps(){
					
			log.debug("Inside CustomerGroupingController : showOneCustomerGrpById");
						
			HttpHeaders responseheaders = new HttpHeaders(); 
			
			IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
			
			try {
					irPlusResponseDetails = iCustomerGroupingMngmntCoreClient.showAllCustomerGrps();
				
			} catch (BusinessException e) {
	
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				
				log.error("Exception raised at Customer mapping Controller :",e);
			}
	
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			} else {
	
				if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}
		}
	
	 HERE CHECK THE VALIDATION OF THE ID	
		
		@RequestMapping(value="/update" ,method = RequestMethod.POST ,	consumes="application/json" ,produces="application/json")	
		public ResponseEntity<String> updateOneCustomerGrps(@RequestBody String customeGrpData){
			
			log.debug("Inside CustomerGroupingController : showOneCustomerGrpById");
						
			HttpHeaders responseHeaders = new HttpHeaders();
			
			IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
			
			List<String> errMsgs = new ArrayList<String>();			
				
		//	log.debug("Arjun :"+customerFunctionInfo);
			
			boolean isValidReq = false;
			
			try {
								
				CustomerGroupingMngmntInfo customerMappingInfo = gson.fromJson(customeGrpData.toString(), CustomerGroupingMngmntInfo.class);
				
				if(customerMappingInfo.getCustomerGrpId()!=null){
												
					irPlusResponseDetails = iCustomerGroupingMngmntCoreClient.updateOneCustomerGrps(customerMappingInfo);
					
				}
				else{
					errMsgs.add("Validation Error : Please Check id - CustomerGrouping Id null getting null");
					irPlusResponseDetails.setErrMsgs(errMsgs);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
					log.debug("Validation Error raised in CustomerFunctionsController :: CreateCustomerfunction");
						
				}
			} catch(BusinessException e){
				
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				log.error("Exception raised at Customer mapping Controller :",e);
				
			}
			
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				
				return new ResponseEntity<>(irPlusResponseDetails.toString(),responseHeaders ,HttpStatus.OK);
			
			} else {
				
					if (!irPlusResponseDetails.isValidationSuccess()) {
				
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
					
					} else {			
						return IRPlusRestUtil.getGenericErrorResponse();
					}				
			}			
		}		
		
		@RequestMapping(value="/updatestatus/{custmpid}/{status}",method=RequestMethod.GET, produces="application/json")
		@ResponseBody
		public ResponseEntity<String> statusUpdatCustmrGrpById(@PathVariable String custmpid , @PathVariable String status)
		{
			HttpHeaders responseHeaders = new HttpHeaders();
			List<String> errMsgs = new ArrayList<String>();			
			
			IRPlusResponseDetails menuResponseDetails=new IRPlusResponseDetails();
			log.info("menuid::" + custmpid);

			try
			{
				Integer status1 = Integer.parseInt(status);
				
				StatusIdInfo sid = new StatusIdInfo();
				
				sid.setId(Integer.parseInt(custmpid));
				sid.setStatus(status1);
		
				
			boolean isValidReq1 = ValidationHelper.isValidReq(custmpid);
			boolean isValidReq2 = ValidationHelper.isValidReq(status);
			log.debug("given no is isValidReq :" + isValidReq1 );
			
				if (isValidReq1 && isValidReq2 )
				{
					menuResponseDetails = iCustomerGroupingMngmntCoreClient.statusUpdatCustmrGrpById(sid);
					menuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				}
				else
				{
					errMsgs.add("Validation Error : NOt valid ids");
					menuResponseDetails.setErrMsgs(errMsgs);
					menuResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
					menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				}
			} catch (BusinessException e) {

				log.error("Exception in CustmrMapping", e);
				menuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
			if (menuResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
			{ 			  			// Successfully Request Creation

				return new ResponseEntity<>(gson.toJson(menuResponseDetails), responseHeaders, HttpStatus.OK);
			} 
			else 
			{
				if(!menuResponseDetails.isValidationSuccess())
				{
					return new ResponseEntity<>(gson.toJson(menuResponseDetails), responseHeaders, HttpStatus.CONFLICT);
				}
				else
				{
					return IRPlusRestUtil.getGenericErrorResponse();
				}
			}
		}

		@RequestMapping(value="/bankIdArrays",method=RequestMethod.GET,produces="application/json")
		
		public ResponseEntity<String> bankIdArrays(){
					
			log.debug("Inside CustomerGroupingController : showOneCustomerGrpById");
			
			
			HttpHeaders responseheaders = new HttpHeaders(); 
			
			IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
			
			try {
					irPlusResponseDetails = iCustomerGroupingMngmntCoreClient.bankIdArrays();
				
			} catch (BusinessException e) {
	
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				
				log.error("Exception raised at Customer mapping Controller :",e);
			}
	
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			} else {
	
				if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}
		}

	@RequestMapping(value="/showOne/{customerMapngId}",method=RequestMethod.GET,produces="application/json")
		
		public ResponseEntity<String> showOneCustomerGrpById(@PathVariable String customerMapngId){
					
			log.debug("Inside CustomerGroupingController : showOneCustomerGrpById");
			
			List<String> errMsgs = new ArrayList<String>();
			
			HttpHeaders responseheaders = new HttpHeaders(); 
			
			IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
			
			try {
				
				log.info("Inside Customer mapping Controller :: customer/showOne() " + customerMapngId);
							
				boolean isValidCustomerId = ValidationHelper.isValidReq(customerMapngId);
				
				if (isValidCustomerId) {							
															
						irPlusResponseDetails = iCustomerGroupingMngmntCoreClient.showOneCustomerGrpById(customerMapngId);
						
				} else {
					errMsgs.add("Validation Error : Not valid CustomerGroupingId");
					irPlusResponseDetails.setErrMsgs(errMsgs);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
					}
			
			} catch (BusinessException e) {
	
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				
				log.error("Exception raised at CustomerGroupingController :",e);
			}
	
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			} else {
	
				if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}
		}

	
	@RequestMapping(value="/showCustomers/{customerId}",method=RequestMethod.GET,produces="application/json")
	
	public ResponseEntity<String> showCustomerArray(@PathVariable String customerId){
				
		log.debug("Inside CustomerGroupingController : showOneCustomerGrpById");
		log.debug("Customers Id :"+customerId);
		
		List<String> errMsgs = new ArrayList<String>();
		
		HttpHeaders responseheaders = new HttpHeaders(); 
		
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
		
		try {
			
			log.info("Inside Customer mapping Controller :: customer/showOne() " + customerId);
						
			boolean isValidCustomerId = ValidationHelper.isValidReq(customerId);
			
			if (isValidCustomerId) {							
														
					irPlusResponseDetails = iCustomerGroupingMngmntCoreClient.custmrIdArrays(customerId);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
					
			} else {
				errMsgs.add("Validation Error : Not valid CustomerGroupingId");
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				}
		
		} catch (BusinessException e) {

			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			log.error("Exception raised at CustomerGroupingController :",e);
		}

		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
		} else {

			if (!irPlusResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
		}
	}
*/
	
	
	@RequestMapping(value="/customergroup/mapping/customer",method=RequestMethod.POST,produces="application/json",consumes= "application/json")
	@ResponseBody
	public ResponseEntity<String> createCustomerMappingGroup(@RequestBody String requestInputData) {

		HttpHeaders responseHeaders = new HttpHeaders();
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		CustomerGroupingMngmntInfo customergroupingMngmntInfo= gson.fromJson(requestInputData.toString(),CustomerGroupingMngmntInfo.class);
	

			List<String> errMsgs = new ArrayList<String>();
			try {
							/*boolean isValidReq = ValidationHelper.isValidCustomerReq(customergroupingMngmntInfo);
							boolean isValidContactReq = true;


							if (isValidReq&&isValidContactReq) {*/
								irPlusResponseDetails = this.iCustomerGroupingMngmntCoreClient.createCustomerMappingGroup(customergroupingMngmntInfo);
					
		
					 }catch (BusinessException e) {
								
								irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
								irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
								
								log.error("Exception raised in CustomerGroupingController :: createCustomerGroup",e);
					}
			
					if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
					{ 			  		

						return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
					} 
					else 
					{
						if(!irPlusResponseDetails.isValidationSuccess())
						{
							return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
						}
						else
						{
							return IRPlusRestUtil.getGenericErrorResponse();
						}
					}
	}
	
	
	@RequestMapping(value="/customer/grouping/showAll",method=RequestMethod.GET,produces="application/json")
	public ResponseEntity<String> showAllCustomers(){

		HttpHeaders responseheaders = new HttpHeaders(); 

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
 		
			try {
				irPlusResponseDetails = iCustomerGroupingMngmntCoreClient.showAllCustomerGroup();
				
			} catch (BusinessException e) {

				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				log.error("Exception raised in CustomerController :: showAllCustomers",e);
			}
			
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			}
			else {
					if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}	
			
	}
	
	
	
	@RequestMapping(value="/CustGrp/delete", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> deleteGroup(@RequestBody String requestInputData)
	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		CustomerGroupingMngmntInfo groupInfo = (CustomerGroupingMngmntInfo)this.gson.fromJson(requestInputData.toString(), CustomerGroupingMngmntInfo.class);

		try
		{
	
				irPlusResponseDetails = this.iCustomerGroupingMngmntCoreClient.deleteGroup(groupInfo);
	
		} catch (BusinessException e) {
			
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  		

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	

	
	@RequestMapping(value="/customergroup/get/{customerGrpId}", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> getcustomergroupInfo(@PathVariable Integer customerGrpId)
	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		try
		{
			/*boolean isValidReq = ValidationHelper.isValidReq(customerGrpId);

			if (isValidReq)
			{*/
				irPlusResponseDetails = this.iCustomerGroupingMngmntCoreClient.getcustomergroupInfo(customerGrpId);
			}
		/*	else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}*/
		 catch (BusinessException e) {
			log.error("Exception in listBank", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	

	@RequestMapping(value="/customergroup/update/customer",method=RequestMethod.POST,produces="application/json",consumes= "application/json")
	@ResponseBody
	public ResponseEntity<String> updateCustomerMappingGroup(@RequestBody String requestInputData) {	
		HttpHeaders responseHeaders = new HttpHeaders();
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		CustomerGroupingMngmntInfo customergroupingMngmntInfo= gson.fromJson(requestInputData.toString(),CustomerGroupingMngmntInfo.class);
	

		
			List<String> errMsgs = new ArrayList<String>();
			try {
							
								irPlusResponseDetails = this.iCustomerGroupingMngmntCoreClient.updateCustomerMappingGroup(customergroupingMngmntInfo);
							
		
					 }catch (BusinessException e) {
								
								irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
								irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
								
								log.error("Exception raised in CustomerController :: createCustomer",e);
					}
			
					if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
					{ 			  		

						return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
					} 
					else 
					{
						if(!irPlusResponseDetails.isValidationSuccess())
						{
							return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
						}
						else
						{
							return IRPlusRestUtil.getGenericErrorResponse();
						}
					}
	}
	
	
	@RequestMapping(value="/customer/groupingReport",method=RequestMethod.POST,produces="application/json")
	public ResponseEntity<String> showgroupingReport(@ModelAttribute CustomerGroupingMngmntInfo customerInfo){


		HttpHeaders responseheaders = new HttpHeaders(); 

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
 		
			try {
				irPlusResponseDetails = iCustomerGroupingMngmntCoreClient.showgroupingReport(customerInfo);
				
			} catch (BusinessException e) {

				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				log.error("Exception raised in CustomerController :: showAllCustomers",e);
			}
			
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			}
			else {
					if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}	
			
	}

	@RequestMapping(value="/groupwise/filter/report", method=RequestMethod.POST, produces= "application/json")
	
	public ResponseEntity<String> filterFiles(@ModelAttribute CustomerGroupingMngmntInfo grpdateinfo)
	{
	
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		

		
		try
		{
		
			irPlusResponseDetails = this.iCustomerGroupingMngmntCoreClient.filterGrpdatewise(grpdateinfo);
			
		} catch (Exception e) {
			log.error("Exception in listFiles", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	@RequestMapping(value="/customergroup/get/datewise", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> getcustomergroupDatewise(@ModelAttribute CustomerGroupingMngmntInfo customergrpdateinfo)
	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		
		try
		{
			

				irPlusResponseDetails = this.iCustomerGroupingMngmntCoreClient.getcustomergroupDatewise(customergrpdateinfo);
			}

		 catch (BusinessException e) {
			log.error("Exception in listBank", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	

	
	@RequestMapping(value="/customerwise/report",method=RequestMethod.GET,produces="application/json")
	public ResponseEntity<String> showcustomerWiseReport(){

		HttpHeaders responseheaders = new HttpHeaders(); 

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
 		
			try {
				irPlusResponseDetails = iCustomerGroupingMngmntCoreClient.showcustomerWiseReport();
				
			} catch (BusinessException e) {

				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				log.error("Exception raised in CustomerController :: showAllCustomers",e);
			}
			
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			}
			else {
					if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}	
			
	}
	
	
	@RequestMapping(value="/customerwise/report/getdatewise", method=RequestMethod.POST, produces="application/json")


	public ResponseEntity<String> getcustomerdateWise(@ModelAttribute CustomerGroupingMngmntInfo customerdateinfo)

	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	
		try
		{
			/*boolean isValidReq = ValidationHelper.isValidReq(customerGrpId);

			if (isValidReq)
			{*/
				irPlusResponseDetails = this.iCustomerGroupingMngmntCoreClient.getcustomerdateWise(customerdateinfo);
			}
		/*	else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}*/
		 catch (BusinessException e) {
			log.error("Exception in listBank", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	
	
	@RequestMapping(value="/customerwise/Groupreport/getdatewise", method=RequestMethod.POST, produces="application/json")


	public ResponseEntity<String> getGroupCustomerwise(@ModelAttribute CustomerGroupingMngmntInfo customerdateinfo)

	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	
		try
		{
			/*boolean isValidReq = ValidationHelper.isValidReq(customerGrpId);

			if (isValidReq)
			{*/
				irPlusResponseDetails = this.iCustomerGroupingMngmntCoreClient.getGroupCustomerwise(customerdateinfo);
			}
		/*	else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}*/
		 catch (BusinessException e) {
			log.error("Exception in listBank", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
@RequestMapping(value="/customerwise/filter/report", method=RequestMethod.POST, produces= "application/json")
	
	public ResponseEntity<String> filterReport(@ModelAttribute CustomerGroupingMngmntInfo customerdateinfo)
	{
	
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		

		
		try
		{
			
			irPlusResponseDetails = this.iCustomerGroupingMngmntCoreClient.filterCustomerdatewise(customerdateinfo);
			
		} catch (Exception e) {
			log.error("Exception in listFiles", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
@RequestMapping(value="/customer/filter/report/datewise", method=RequestMethod.POST, produces="application/json")
@ResponseBody
public ResponseEntity<String> getdatacustomerDatewise(@ModelAttribute CustomerGroupingMngmntInfo customerdatewiseinfo)
{
	HttpHeaders responseHeaders = new HttpHeaders();

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	
	try
	{
		

			irPlusResponseDetails = this.iCustomerGroupingMngmntCoreClient.getdatacustomerDatewise(customerdatewiseinfo);
		}

	 catch (BusinessException e) {
		log.error("Exception in listBank", e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
	}
	if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
	{ 			  			// Successfully Request Creation

		return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
	} 
	else 
	{
		if(!irPlusResponseDetails.isValidationSuccess())
		{
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
		}
		else
		{
			return IRPlusRestUtil.getGenericErrorResponse();
		}
	}
}

@RequestMapping(value="/volumetrend/report",method=RequestMethod.POST,produces="application/json")

	public ResponseEntity<String> showVolumeTrendReport(@ModelAttribute CustomerGroupingMngmntInfo currentDate){
	HttpHeaders responseheaders = new HttpHeaders(); 

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		try {
			irPlusResponseDetails = iCustomerGroupingMngmntCoreClient.showVolumeTrendReport(currentDate);
			
		} catch (BusinessException e) {

			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception raised in CustomerController :: showAllCustomers",e);
		}
		
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
		}
		else {
				if (!irPlusResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
		}	
		
}

@RequestMapping(value="/volumetrend/weekwise",method=RequestMethod.GET,produces="application/json")
public ResponseEntity<String> showVolumeTrendReportWeekwise(){

	HttpHeaders responseheaders = new HttpHeaders(); 

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		try {
			irPlusResponseDetails = iCustomerGroupingMngmntCoreClient.showVolumeTrendReportWeekwise();
			
		} catch (BusinessException e) {

			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception raised in CustomerController :: showAllCustomers",e);
		}
		
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
		}
		else {
				if (!irPlusResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
		}	
		
}
@RequestMapping(value="/volumetrend/rangewise/report", method=RequestMethod.POST, produces= "application/json")

public ResponseEntity<String> volumeTrendrangewise(@ModelAttribute CustomerGroupingMngmntInfo trendRange)
{
	
	HttpHeaders responseHeaders = new HttpHeaders();

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
	

	
	try
	{
	
	
		irPlusResponseDetails = this.iCustomerGroupingMngmntCoreClient.volumeTrendrangewise(trendRange);
		
	} catch (Exception e) {
		log.error("Exception in listFiles", e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
	}
	if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
	{ 			  			// Successfully Request Creation

		return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
	} 
	else 
	{
		if(!irPlusResponseDetails.isValidationSuccess())
		{
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
		}
		else
		{
			return IRPlusRestUtil.getGenericErrorResponse();
		}
	}
}

@RequestMapping(value="/volumetrend/bank/rangewise/report", method=RequestMethod.POST, produces= "application/json")

public ResponseEntity<String> volumeTrendBankrangewise(@ModelAttribute CustomerGroupingMngmntInfo BankRange)
{
	
	HttpHeaders responseHeaders = new HttpHeaders();

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
	

	
	try
	{
	
	
		irPlusResponseDetails = this.iCustomerGroupingMngmntCoreClient.volumeTrendBankrangewise(BankRange);
		
	} catch (Exception e) {
		log.error("Exception in listFiles", e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
	}
	if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
	{ 			  			

		return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
	} 
	else 
	{
		if(!irPlusResponseDetails.isValidationSuccess())
		{
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
		}
		else
		{
			return IRPlusRestUtil.getGenericErrorResponse();
		}
	}
}

@RequestMapping(value="/volumetrend/summary", method=RequestMethod.POST, produces= "application/json")

public ResponseEntity<String> volumeTrendSummary(@ModelAttribute CustomerGroupingMngmntInfo trendDateRange)
{
	
	HttpHeaders responseHeaders = new HttpHeaders();

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
	

	
	try
	{
	
	
		irPlusResponseDetails = this.iCustomerGroupingMngmntCoreClient.volumeTrendSummary(trendDateRange);
		
	} catch (Exception e) {
		log.error("Exception in listFiles", e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
	}
	if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
	{ 			  			// Successfully Request Creation

		return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
	} 
	else 
	{
		if(!irPlusResponseDetails.isValidationSuccess())
		{
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
		}
		else
		{
			return IRPlusRestUtil.getGenericErrorResponse();
		}
	}
}

@RequestMapping(value="/bankwise/volumeTrend/{bankid}", method=RequestMethod.GET, produces="application/json")
@ResponseBody
public ResponseEntity<String> getbankwiseReport(@PathVariable String bankid)
{
	HttpHeaders responseHeaders = new HttpHeaders();

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();


	try
	{
		/*boolean isValidReq = ValidationHelper.isValidReq(customerGrpId);

		if (isValidReq)
		{*/
			irPlusResponseDetails = this.iCustomerGroupingMngmntCoreClient.getbankwiseReport(bankid);
		}
	/*	else
		{
			irPlusResponseDetails.setValidationSuccess(false);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}*/
	 catch (BusinessException e) {
		log.error("Exception in listBank", e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
	}
	if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
	{ 			  			// Successfully Request Creation

		return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
	} 
	else 
	{
		if(!irPlusResponseDetails.isValidationSuccess())
		{
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
		}
		else
		{
			return IRPlusRestUtil.getGenericErrorResponse();
		}
	}
}

@RequestMapping(value="/bankwise/volumetrend/daywise/{bankid}", method=RequestMethod.GET, produces="application/json")
@ResponseBody
public ResponseEntity<String> getbankDaywiseReport(@PathVariable String bankid)
{
	HttpHeaders responseHeaders = new HttpHeaders();

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();


	try
	{
		/*boolean isValidReq = ValidationHelper.isValidReq(customerGrpId);

		if (isValidReq)
		{*/
			irPlusResponseDetails = this.iCustomerGroupingMngmntCoreClient.getbankDaywiseReport(bankid);
		}
	/*	else
		{
			irPlusResponseDetails.setValidationSuccess(false);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}*/
	 catch (BusinessException e) {
		log.error("Exception in listBank", e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
	}
	if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
	{ 			  			// Successfully Request Creation

		return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
	} 
	else 
	{
		if(!irPlusResponseDetails.isValidationSuccess())
		{
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
		}
		else
		{
			return IRPlusRestUtil.getGenericErrorResponse();
		}
	}
}






@RequestMapping(value="/customergrp/filter", method=RequestMethod.POST, produces= "application/json")
@ResponseBody
public ResponseEntity<String> CustomerGrpFilter(@ModelAttribute CustomerGroupingMngmntInfo GrpListInfo)
{
		
	HttpHeaders responseHeaders = new HttpHeaders();

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
	

	
	try
	{
		
		irPlusResponseDetails = this.iCustomerGroupingMngmntCoreClient.CustomerGrpFilter(GrpListInfo);
		
	} catch (BusinessException e) {
		
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
	}
	if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
	{ 			  			// Successfully Request Creation

		return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
	} 
	else 
	{
		if(!irPlusResponseDetails.isValidationSuccess())
		{
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
		}
		else
		{
			return IRPlusRestUtil.getGenericErrorResponse();
		}
	}
}



@RequestMapping(value="/report/Daywise/newreport",method=RequestMethod.POST,produces="application/json")
public ResponseEntity<String> showDayReport(@ModelAttribute CustomerGroupingMngmntInfo currentDate){
	
	HttpHeaders responseheaders = new HttpHeaders(); 

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		try {

			irPlusResponseDetails = iCustomerGroupingMngmntCoreClient.showDaywiseReport(currentDate);
			
		} catch (BusinessException e) {

			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception raised in CustomerController :: showAllCustomers",e);
		}
		
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
		}
		else {
				if (!irPlusResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
		}	
		
}







@RequestMapping(value="/Daywise/Customer/report",method=RequestMethod.POST,produces="application/json")
public ResponseEntity<String> showDaywiseCustomerReport(@ModelAttribute CustomerGroupingMngmntInfo customerInfo){

	HttpHeaders responseheaders = new HttpHeaders(); 

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		try {
		
			irPlusResponseDetails = iCustomerGroupingMngmntCoreClient.showDaywiseCustomerReport(customerInfo);
			
		} catch (BusinessException e) {

			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception raised in CustomerController :: showAllCustomers",e);
		}
		
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
		}
		else {
				if (!irPlusResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
		}	
		
}

@RequestMapping(value="/Daywise/GroupCustomer/report",method=RequestMethod.POST,produces="application/json")
public ResponseEntity<String> showDaywiseCustomerGroupReport(@ModelAttribute CustomerGroupingMngmntInfo customerInfo){

	HttpHeaders responseheaders = new HttpHeaders(); 

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		try {
		
			irPlusResponseDetails = iCustomerGroupingMngmntCoreClient.showDaywiseCustomerGroupReport(customerInfo);
			
		} catch (BusinessException e) {

			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception raised in CustomerController :: showAllCustomers",e);
		}
		
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
		}
		else {
				if (!irPlusResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
		}	
		
}

@RequestMapping(value="/datewise/customerwise",method=RequestMethod.POST,produces="application/json")
public ResponseEntity<String> DaycustomerReport(@ModelAttribute CustomerGroupingMngmntInfo customerInfo){

	HttpHeaders responseheaders = new HttpHeaders(); 

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		try {
	
			irPlusResponseDetails = iCustomerGroupingMngmntCoreClient.DaycustomerReport(customerInfo);
			
		} catch (BusinessException e) {

			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception raised in CustomerController :: showAllCustomers",e);
		}
		
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
		}
		else {
				if (!irPlusResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
		}	
		
}


@RequestMapping(value="/datewise/Group/customerwise",method=RequestMethod.POST,produces="application/json")
public ResponseEntity<String> DaycustomerGroupReport(@ModelAttribute CustomerGroupingMngmntInfo customerInfo){

	HttpHeaders responseheaders = new HttpHeaders(); 

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		try {
	
			irPlusResponseDetails = iCustomerGroupingMngmntCoreClient.DaycustomerGroupReport(customerInfo);
			
		} catch (BusinessException e) {

			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception raised in CustomerController :: showAllCustomers",e);
		}
		
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
		}
		else {
				if (!irPlusResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
		}	
		
}

@RequestMapping(value="/group/filter", method=RequestMethod.POST, produces= "application/json")
@ResponseBody
public ResponseEntity<String> filterGroups(@ModelAttribute CustomerGroupingMngmntInfo grpFilterInfo)
{
		
	HttpHeaders responseHeaders = new HttpHeaders();

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
	
	LOGGER.info("grpFilterInfo::" + grpFilterInfo);
	
	try
	{
		
		irPlusResponseDetails = this.iCustomerGroupingMngmntCoreClient.filterGroups(grpFilterInfo);
		
	} catch (BusinessException e) {
		LOGGER.error("Exception in listBank", e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
	}
	if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
	{ 			  			// Successfully Request Creation

		return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
	} 
	else 
	{
		if(!irPlusResponseDetails.isValidationSuccess())
		{
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
		}
		else
		{
			return IRPlusRestUtil.getGenericErrorResponse();
		}
	}
}
@RequestMapping(value="/autocomplete/filter/grpname",method=RequestMethod.GET,produces="application/json")
/*public ResponseEntity<String> Autocomplete(){*/
	public ResponseEntity<String> AutocompleteBank(@RequestParam("term") String term,@RequestParam("userid") String userid) {
	HttpHeaders responseheaders = new HttpHeaders(); 

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		try {
		
		irPlusResponseDetails = iCustomerGroupingMngmntCoreClient.AutocompleteGroup(term,userid);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		} catch (Exception e) {

			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
		
		}
		
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.OK);
		}
		else {
				if (!irPlusResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
		}	
		
}


}
