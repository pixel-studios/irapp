package com.irplus.app;

import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.irplus.core.client.customerMappingMngmnt.ICustomerMappingCoreClient;
import com.irplus.dto.CustomerGroupingMngmntInfo;
import com.irplus.dto.CustomerMapSupportInfo;
import com.irplus.dto.CustomerMappingInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;
import com.irplus.util.ValidationHelper;

@Controller
/*@RequestMapping(value = "/custmermpng")*/
public class CustomerMappingController {

	private static final Logger log = Logger.getLogger(CustomerMappingController.class);
	
	@Autowired
	ICustomerMappingCoreClient iCustomerMappingCoreClient;
	
	Gson gson = new Gson();

	@RequestMapping(value = "/create" , method=RequestMethod.POST ,produces = "application/json" , consumes = "application/json")
	public ResponseEntity<String> createCustomerMapping(@RequestBody String CustomerMappingInfoData){
		
	log.info("Inside of CustomerLicenceControll :: CreateCustomerLicence");
		
		HttpHeaders responseHeader = new HttpHeaders();
				
		List<String> errMsgs = new ArrayList<String>();
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 

		try{
			
			CustomerMapSupportInfo customerMapSupportInfo= gson.fromJson(CustomerMappingInfoData.toString(), CustomerMapSupportInfo.class);				
			
			if (customerMapSupportInfo.getCustomerMappingInfo()!= null) {
				irPlusResponseDetails = iCustomerMappingCoreClient.createCustomerMapping(customerMapSupportInfo) ;	
			} else {

				errMsgs.add("Validation Error : Please Check CustomerMappingInfo is Empty");
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				log.debug("Validation Error raised in CustomerMappingInfoDataController :: CreateCustomerMappingInfo");
			}						
		} catch (BusinessException e) {
			
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			log.error("Exception raised in CustomerMappingInfoDataController :: CreateCustomerMappingInfo",e);
		}
	
		if(irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG))
		{
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeader, HttpStatus.OK);
		}else
		{
			if (!irPlusResponseDetails.isValidationSuccess()) {
	
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseHeader, HttpStatus.CONFLICT);
	
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
}
		@RequestMapping(value="/showOne/{customerMapngId}",method=RequestMethod.GET,produces="application/json")
		
		public ResponseEntity<String> showOneCustomerMapngById(@PathVariable String customerMapngId){
					
			log.debug("Inside CustomerMappingMngmntController : showOneCustomerMappingById");
			List<String> errMsgs = new ArrayList<String>();
			
			HttpHeaders responseheaders = new HttpHeaders(); 
			
			IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
			
			try {
				
				log.info("Inside Customer mapping Controller :: customer/showOne() " + customerMapngId);
							
				boolean isValidCustomerId = ValidationHelper.isValidReq(customerMapngId);
				
				if (isValidCustomerId) {							
															
						irPlusResponseDetails = iCustomerMappingCoreClient.getCustomerMappingById(customerMapngId);
						
				} else {
					errMsgs.add("Validation Error : Not valid CustomerMappingId");
					irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				}
			
			} catch (BusinessException e) {
	
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				
				log.error("Exception raised at Customer mapping Controller :",e);
			}
	
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			} else {
	
				if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}
		}
	
		@RequestMapping(value="/showall",method=RequestMethod.GET,produces="application/json")
		
		public ResponseEntity<String> showAllCustomerMappings(){
					
			log.debug("Inside CustomerMappingMngmntController : showAll()");
		
			HttpHeaders responseheaders = new HttpHeaders(); 
			
			IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
			
			try {
					irPlusResponseDetails = iCustomerMappingCoreClient.showAllCustomerMapping();
				
			} catch (BusinessException e) {
	
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				
				log.error("Exception raised at Customer mapping Controller :",e);
			}
	
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			} else {
	
				if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}
		}
	
		@RequestMapping(value="/update" ,method = RequestMethod.POST ,	consumes="application/json" ,produces="application/json")	
		public ResponseEntity<String> updateCustomerFunction(@RequestBody String customeMappngData){
			
			log.debug("Inside CustomerMappingMngmntController : updateCustomerMapping()");
			
			HttpHeaders responseHeaders = new HttpHeaders();
			
			IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
			
			List<String> errMsgs = new ArrayList<String>();			
				
		//	log.debug("Arjun :"+customerFunctionInfo);
			
			boolean isValidReq = false;
			
			try {				
				
				CustomerMappingInfo customerMappingInfo = gson.fromJson(customeMappngData.toString(), CustomerMappingInfo.class);
				
				if(customerMappingInfo.getCustomerMapId()!=null){
												
					irPlusResponseDetails = iCustomerMappingCoreClient.updateCustomerMapping(customerMappingInfo);
					
				}
				else{
					errMsgs.add("Validation Error : Please Check id - CustomerMapping Id is null getting ");
					irPlusResponseDetails.setErrMsgs(errMsgs);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
					log.debug("Validation Error raised in CustomerFunctionsController :: CreateCustomerfunction");
						
				}
			} catch(BusinessException e){
				
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				log.error("Exception raised at Customer mapping Controller :",e);
				
			}
			
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails),responseHeaders ,HttpStatus.OK);
			
			} else {
				
					if (!irPlusResponseDetails.isValidationSuccess()) {
				
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
					
					} else {			
						return IRPlusRestUtil.getGenericErrorResponse();
					}				
			}			
		}		
		
		@RequestMapping(value="/updatestatus/{custmpid}/{status}",method=RequestMethod.GET, produces="application/json")
		@ResponseBody
		public ResponseEntity<String> updateStatusCustomrMpng(@PathVariable String custmpid , @PathVariable String status)
		{
			HttpHeaders responseHeaders = new HttpHeaders();
			List<String> errMsgs = new ArrayList<String>();			
			
			IRPlusResponseDetails menuResponseDetails=new IRPlusResponseDetails();
			log.info("menuid::" + custmpid);

			try
			{
				Integer status1 = Integer.parseInt(status);
				StatusIdInfo sid = new StatusIdInfo();
				sid.setId(Integer.parseInt(custmpid));
				sid.setStatus(status1);
		
				
			boolean isValidReq1 = ValidationHelper.isValidReq(custmpid);
			boolean isValidReq2 = ValidationHelper.isValidReq(status);
			log.debug("given no is isValidReq :" + isValidReq1 );
			
				if (isValidReq1 && isValidReq2 )
				{
					menuResponseDetails = iCustomerMappingCoreClient.statusUpdateCustmrMpng(sid);
					menuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				}
				else
				{
					errMsgs.add("Validation Error : Not valid ids");
					menuResponseDetails.setErrMsgs(errMsgs);
					menuResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
					menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				}
			} catch (BusinessException e) {

				log.error("Exception in CustmrMapping", e);
				menuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
			if (menuResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
			{ 			  			// Successfully Request Creation

				return new ResponseEntity<>(gson.toJson(menuResponseDetails), responseHeaders, HttpStatus.OK);
			} 
			else 
			{
				if(!menuResponseDetails.isValidationSuccess())
				{
					return new ResponseEntity<>(gson.toJson(menuResponseDetails), responseHeaders, HttpStatus.CONFLICT);
				}
				else
				{
					return IRPlusRestUtil.getGenericErrorResponse();
				}
			}
		}
		
		
		
		
		
		
		
		@RequestMapping(value="/Customermapping/branch/{userId}",method=RequestMethod.GET,produces="application/json")
		public ResponseEntity<String> CustomerBankingBanks(@PathVariable String userId){

			HttpHeaders responseheaders = new HttpHeaders(); 

			IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
				try {
				
					irPlusResponseDetails = iCustomerMappingCoreClient.CustomerBankingBanks(userId);
					
				} catch (BusinessException e) {

					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
					log.error("Exception raised in CustomerControllerMapping :: showAllBanksMapping",e);
				}
				
				if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
				}
				else {
						if (!irPlusResponseDetails.isValidationSuccess()) {
							return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
						} else {
							return IRPlusRestUtil.getGenericErrorResponse();
						}
				}	
				
		}

		
		
		
		@RequestMapping(value="/customer/getcustomer/{branchId}", method=RequestMethod.GET, produces="application/json")
		@ResponseBody
		public ResponseEntity<String> getBankCustomer(@PathVariable String branchId)
		{
			HttpHeaders responseHeaders = new HttpHeaders();

			IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

			log.info("siteId::" + branchId);
			try
			{
				boolean isValidReq = ValidationHelper.isValidReq(branchId);

				if (isValidReq)
				{
					irPlusResponseDetails = this.iCustomerMappingCoreClient.showBranchCustomer(branchId);
				}
				else
				{
					irPlusResponseDetails.setValidationSuccess(false);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				}
			} catch (BusinessException e) {
				log.error("Exception in listBank", e);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
			{ 			  			// Successfully Request Creation

				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
			} 
			else 
			{
				if(!irPlusResponseDetails.isValidationSuccess())
				{
					return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
				}
				else
				{
					return IRPlusRestUtil.getGenericErrorResponse();
				}
			}
		}
		
		
		
		@RequestMapping(value="/customer/mapping/Fields",method=RequestMethod.POST,produces="application/json",consumes= "application/json")
		@ResponseBody
		public ResponseEntity<String> createCustomerMappingFields(@RequestBody String requestInputData) {
			log.info("createCustomerMappingFieldscreateCustomerMappingFields");
			log.info("Inside of customergroups :: createCustomerMappingFields");
			
			HttpHeaders responseHeaders = new HttpHeaders();
			IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
			
			CustomerMappingInfo[] customerMappingInfo= gson.fromJson(requestInputData.toString(),CustomerMappingInfo[].class);
		
			
			
		//	CustomerGroupingDetails[] customergroupingDetails=customergroupingMngmntInfo.getCustomergroupingDetails();
			
		
			
				List<String> errMsgs = new ArrayList<String>();
	/*	try {
								boolean isValidReq = ValidationHelper.isValidCustomerReq(customerMappingInfo);
								boolean isValidContactReq = true;


								if (isValidReq&&isValidContactReq) {
									irPlusResponseDetails = this.iCustomerMappingCoreClient.createCustomerMappingFields(customerMappingInfo);
								}else{
									irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
									irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
								}
			
						 }*/
				try {
				irPlusResponseDetails = this.iCustomerMappingCoreClient.createCustomerMappingFields(customerMappingInfo);
				}
				catch (BusinessException e) {
									
									irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
									irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
									
									log.error("Exception raised in CustomerController :: createCustomer",e);
						}
				
						if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
						{ 			  			// Successfully Request Creation

							return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
						} 
						else 
						{
							if(!irPlusResponseDetails.isValidationSuccess())
							{
								return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
							}
							else
							{
								return IRPlusRestUtil.getGenericErrorResponse();
							}
						}
		}
		
		
		@RequestMapping(value="/customer/mapping/showAll",method=RequestMethod.GET,produces="application/json")
		public ResponseEntity<String> showAllCustomers(){

			HttpHeaders responseheaders = new HttpHeaders(); 

			IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
	 		
				try {
					irPlusResponseDetails = iCustomerMappingCoreClient.showAllCustomerMappedFields();
					
				} catch (BusinessException e) {

					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
					log.error("Exception raised in CustomerController :: showAllCustomers",e);
				}
				
				if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
				}
				else {
						if (!irPlusResponseDetails.isValidationSuccess()) {
							return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
						} else {
							return IRPlusRestUtil.getGenericErrorResponse();
						}
				}	
				
		}
		
		
		
		
}