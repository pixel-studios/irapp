package com.irplus.app;

import java.io.File;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.Gson;
import com.irplus.core.client.bankmgmt.IBankMgmtCoreClient;
import com.irplus.dao.hibernate.entities.IRSSite;
import com.irplus.dto.BankFilterInfo;
import com.irplus.dto.BankInfo;
import com.irplus.dto.ContactInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;
import com.irplus.util.ValidationHelper;
import org.hibernate.Query;
import org.hibernate.Session;
import java.util.List;
@Controller
@Transactional
public class BankController
{
	private static final Logger LOGGER = Logger.getLogger(BankController.class);

	Gson gson = new Gson();

	@Autowired
	IBankMgmtCoreClient iBankMgmtCoreClient;

	
	 @Autowired
	  private SessionFactory sessionFactory;
	  
	  public Session getMyCurrentSession()
	  {
	    Session session = null;
	    try
	    {
	      session = this.sessionFactory.getCurrentSession();
	    }
	    catch (HibernateException e)
	    {
	      LOGGER.error("Exception raised DaoImpl :: At current session creation time ::" + e);
	    }
	    return session;
	  }
	  
	String rootFolderPath =null;
	
	String branchLogoPath=null;
	
	
	
	@RequestMapping(value= "/bank/create" , method = RequestMethod.POST, produces= "application/json", consumes= "application/json")
	@ResponseBody
	public ResponseEntity<String> createBank(@RequestBody String requestInputData) { 

		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		BankInfo bankInfo = gson.fromJson(requestInputData.toString(), BankInfo.class);
		ContactInfo[] contacts = bankInfo.getContacts(); //gson.fromJson(requestInputData.toString(),ContactInfo[].class);
		LOGGER.info("Received contactArray "+contacts+" which length is :"+contacts.length);
		ContactInfo contactInfo = contacts[0];
		//bankInfo.setContact(contactArray[0]);

		LOGGER.info("bankInfo::" + bankInfo.getBankName()+"contactInfo :"+contactInfo);
		try
		{
			boolean isValidReq = ValidationHelper.isCreateBankReqValid(bankInfo);

			boolean isValidContactReq = true;

			for (int i = 0; i < contacts.length; i++)
			{
				if(isValidContactReq)
					isValidContactReq = ValidationHelper.isCreateContactReqValid(contactInfo);
				else
					break;

			}

			if (isValidReq&&isValidContactReq)
			{
				irPlusResponseDetails = this.iBankMgmtCoreClient.createBank(bankInfo);
			}
			else
			{
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}

		}
		catch (BusinessException e)
		{
			LOGGER.error("Exception in CreateBank", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}

		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}

	@RequestMapping(value="/bank/list/{userId}", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> listBanks(@PathVariable String userId)
	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		LOGGER.info("userId::" + userId);
		try
		{
			boolean isValidReq = ValidationHelper.isValidReq(userId);

			if (isValidReq)
			{
				irPlusResponseDetails = this.iBankMgmtCoreClient.listBanks(userId);
			}
			else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
		} catch (BusinessException e) {
			LOGGER.error("Exception in listBank", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	
	@RequestMapping(value="/bank/filter", method=RequestMethod.POST, produces= "application/json")
	@ResponseBody
	public ResponseEntity<String> filterBanks(@ModelAttribute BankFilterInfo bankFilterInfo)
	{
			
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		LOGGER.info("bankFilterInfo::" + bankFilterInfo);
		
		try
		{
			
			irPlusResponseDetails = this.iBankMgmtCoreClient.filterBanks(bankFilterInfo);
			
		} catch (BusinessException e) {
			LOGGER.error("Exception in listBank", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}


	@RequestMapping(value= "/bank/update", method=RequestMethod.POST, produces="application/json", consumes="application/json")
	@ResponseBody
	public ResponseEntity<String> updateBank(@RequestBody String requestInputData)
	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		BankInfo bankInfo = (BankInfo)this.gson.fromJson(requestInputData.toString(),BankInfo.class);

		LOGGER.info("Inside Update Bank bankInfo::" + bankInfo);
		try
		{
			boolean isValidReq = ValidationHelper.isUpdateBankReqValid(bankInfo);
			if (isValidReq)
			{
				irPlusResponseDetails = this.iBankMgmtCoreClient.updateBank(bankInfo);
			}
			else
			{
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}

		}
		catch (BusinessException e)
		{
			LOGGER.error("Exception in updateBank", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}

		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}


		}

	}
	
	@RequestMapping(value= "/bank/updatestatus", method=RequestMethod.POST, produces="application/json", consumes="application/json")
	@ResponseBody
	public ResponseEntity<String> updateBankStatus(@RequestBody String requestInputData)
	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		BankInfo bankInfo = (BankInfo)this.gson.fromJson(requestInputData.toString(), BankInfo.class);

		LOGGER.info("Inside updateBankStatus bankInfo::" + bankInfo);
		try
		{
			
			irPlusResponseDetails = this.iBankMgmtCoreClient.updateBankStatus(bankInfo);
			

		}
		catch (BusinessException e)
		{
			LOGGER.error("Exception in updateBank", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}

		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}


		}

	}
	
	@RequestMapping(value="/bank/get/{bankId}", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> getBankInfo(@PathVariable String bankId)
	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		LOGGER.info("siteId::" + bankId);
		try
		{
			boolean isValidReq = ValidationHelper.isValidReq(bankId);

			if (isValidReq)
			{
				irPlusResponseDetails = this.iBankMgmtCoreClient.getBankInfo(bankId);
			}
			else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
		} catch (BusinessException e) {
			LOGGER.error("Exception in listBank", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}


	@RequestMapping(value="/bank/delete", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> deleteBank(@RequestBody String requestInputData)
	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		BankInfo bankInfo = (BankInfo)this.gson.fromJson(requestInputData.toString(), BankInfo.class);

		LOGGER.info("bankId::");
		try
		{
		//	boolean isValidReq = ValidationHelper.isValidReq(bankId);
		//	if (isValidReq)
		//	{
				irPlusResponseDetails = this.iBankMgmtCoreClient.deleteBank(bankInfo);
		//	}
		//	else
		//	{
				//irPlusResponseDetails.setValidationSuccess(false);
			//	irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
			//	irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			//}
		} catch (BusinessException e) {
			LOGGER.error("Exception in deleteBank", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}

	@RequestMapping(value="/location/list/{bankId}", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> listLocations(@PathVariable String bankId)
	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		LOGGER.info("siteId::" + bankId);
		try
		{
			boolean isValidReq = ValidationHelper.isValidReq(bankId);

			if (isValidReq)
			{
				irPlusResponseDetails = this.iBankMgmtCoreClient.listLocations(bankId);
			}
			else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
		} catch (BusinessException e) {
			LOGGER.error("Exception in listBank", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}

	
	@RequestMapping(value= "/branch/create" , method = RequestMethod.POST, produces= "application/json", consumes= "application/json")
	@ResponseBody
	public ResponseEntity<String> createBankBranch(@RequestBody String requestInputData) { 

		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		BankInfo bankInfo = gson.fromJson(requestInputData.toString(), BankInfo.class);
		ContactInfo[] contacts = bankInfo.getContacts(); //gson.fromJson(requestInputData.toString(),ContactInfo[].class);
		LOGGER.info("Received contactArray "+contacts+" which length is :"+contacts.length);
		ContactInfo contactInfo = contacts[0];
		//bankInfo.setContact(contactArray[0]);

		LOGGER.info("bankInfo::" + bankInfo+"contactInfo :"+contactInfo);
		try
		{
	
				irPlusResponseDetails = this.iBankMgmtCoreClient.addLocation(bankInfo);
			
		}
		catch (BusinessException e)
		{
			LOGGER.error("Exception in CreateBank", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}

		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	
	@RequestMapping(value="/branch/get/{branchId}", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> getBranchInfo(@PathVariable String branchId)
	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		LOGGER.info("siteId::" + branchId);
		try
		{
			boolean isValidReq = ValidationHelper.isValidReq(branchId);

			if (isValidReq)
			{
				irPlusResponseDetails = this.iBankMgmtCoreClient.getBranchInfo(new Long(branchId));
			}
			else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
		} catch (BusinessException e) {
			LOGGER.error("Exception in listBank", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}

	@RequestMapping(value= "/branch/update" , method = RequestMethod.POST, produces= "application/json", consumes= "application/json")
	@ResponseBody
	public ResponseEntity<String> updateBranch(@RequestBody String requestInputData) { 

		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		BankInfo bankInfo = gson.fromJson(requestInputData.toString(),BankInfo.class);
		ContactInfo[] contacts = bankInfo.getContacts(); //gson.fromJson(requestInputData.toString(),ContactInfo[].class);
		LOGGER.info("Received contactArray "+contacts+" which length is :"+contacts.length);
		ContactInfo contactInfo = contacts[0];
		//bankInfo.setContact(contactArray[0]);

		LOGGER.info("bankInfo::" + bankInfo+"contactInfo :"+contactInfo);
		try
		{
		
				irPlusResponseDetails = this.iBankMgmtCoreClient.updateLocation(bankInfo);
			

		}
		catch (BusinessException e)
		{
			LOGGER.error("Exception in CreateBank", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}

		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	
	@RequestMapping(value="/branch/filter", method=RequestMethod.POST, produces= "application/json")
	@ResponseBody
	public ResponseEntity<String> filterBranches(@ModelAttribute BankFilterInfo bankFilterInfo)
	{
			
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		//BankFilterInfo bankInfo = gson.fromJson(requestInputData.toString(), BankFilterInfo.class);
				
		
		LOGGER.info("bankFilterInfo::" + bankFilterInfo);
		
		try
		{
			
			irPlusResponseDetails = this.iBankMgmtCoreClient.filterBranches(bankFilterInfo);
			
		} catch (BusinessException e) {
			LOGGER.error("Exception in filterBranches", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	
	@RequestMapping(value = "/bank/fileUpload", method = RequestMethod.POST, headers = "content-type=multipart/*", produces = {"application/json"})
    public ResponseEntity<String> fileUpload(MultipartHttpServletRequest request,
			@RequestParam(value="bankLogo",required = false) MultipartFile bankLogo) throws Exception {
		
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
	try
		{
    
			
			 String hql = "from IRSSite site where site.isactive=?";
		      
		      Query query = getMyCurrentSession().createQuery(hql);
		      query.setParameter(0, Integer.valueOf(1));
		      
		      List<IRSSite> siteList = query.list();
		      if (siteList.size() > 0)
		      {
		        rootFolderPath = ((IRSSite)siteList.get(0)).getRootFolderPath();
			
		      }
			
			if(rootFolderPath!=null) {
				 File file = new File(rootFolderPath + File.separator + "Images");
		            if (!file.exists()) {
		              if (file.mkdir()) {
		                System.out.println("Directory is created!");
		              } else {
		                System.out.println("Failed to create directory!");
		              }
		            }
		            branchLogoPath=rootFolderPath+File.separator + "Images"+File.separator+"BankLogo";
		            File file1 = new File(branchLogoPath);
		            if (!file1.exists()) {
		              if (file1.mkdir()) {
		                System.out.println("Directory is created!");
		              } else {
		                System.out.println("Failed to create directory!");
		              }
		            }
 
		            
		            
		            
			}
		LOGGER.info("fileUploaded::" + bankLogo +" original File name : "+bankLogo.getOriginalFilename());
		
		String filePath = request.getRealPath("/");
		
		filePath = branchLogoPath+File.separator+bankLogo.getOriginalFilename();
		
	
		
		LOGGER.info("fileUploaded::" + bankLogo +" original File name : "+bankLogo.getOriginalFilename()+"filepath : "+filePath);
		
		File newFile = new File(filePath);
		
		bankLogo.transferTo(newFile);
		
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		
	
		irPlusResponseDetails.setLogoPath(bankLogo.getOriginalFilename());
		}
		
		catch (Exception e) {
			LOGGER.error("Exception in fileUpload", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
    }
	

	@RequestMapping(value="/contact/delete/{contactId}", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> deleteContact(@PathVariable String contactId)
	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		LOGGER.info("bankId::" + contactId);
		try
		{
			boolean isValidReq = ValidationHelper.isValidReq(contactId);
			if (isValidReq)
			{
				irPlusResponseDetails = this.iBankMgmtCoreClient.deleteContact(contactId);
			}
			else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
		} catch (BusinessException e) {
			LOGGER.error("Exception in deleteContact", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}

	@RequestMapping(value = "/branch/fileUpload", method = RequestMethod.POST, headers = "content-type=multipart/*", produces = {"application/json"})
    public ResponseEntity<String> branchLogoUpload(MultipartHttpServletRequest request,
			@RequestParam(value="branchLogo",required = false) MultipartFile branchLogo) throws Exception {
		
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
				
		try
		{
    
		LOGGER.info("fileUploaded::" + branchLogo +" original File name : "+branchLogo.getOriginalFilename());
		
		String filePath = request.getRealPath("/");
		
		filePath = filePath+"/resources/assets/irplus/branchlogos/"+branchLogo.getOriginalFilename();
		
	//	filePath ="C://IRPLUS//images//bankBranchLogo//"+branchLogo.getOriginalFilename();
		
		LOGGER.info("fileUploaded::" + branchLogo +" original File name : "+branchLogo.getOriginalFilename()+"filepath : "+filePath);
		
		File newFile = new File(filePath);
		
		branchLogo.transferTo(newFile);
		
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		
		irPlusResponseDetails.setLogoPath("/resources/assets/irplus/branchlogos/"+branchLogo.getOriginalFilename());
		}
		
		catch (Exception e) {
			LOGGER.error("Exception in fileUpload", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
    }
	

	
	@RequestMapping(value= "/branch/updatestatus", method=RequestMethod.POST, produces="application/json", consumes="application/json")
	@ResponseBody
	public ResponseEntity<String> updateBranchStatus(@RequestBody String requestInputData)
	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		BankInfo bankInfo = (BankInfo)this.gson.fromJson(requestInputData.toString(), BankInfo.class);

		LOGGER.info("Inside updateBankStatus bankInfo::" + bankInfo);
		try
		{
			
			irPlusResponseDetails = this.iBankMgmtCoreClient.updateBranchStatus(bankInfo);
			

		}
		catch (BusinessException e)
		{
			LOGGER.error("Exception in updateBank", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}

		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}


		}

	}
	

	
	
	@RequestMapping(value= "/branch/delete", method=RequestMethod.POST, produces="application/json", consumes="application/json")
	@ResponseBody
	public ResponseEntity<String> deleteBranch(@RequestBody String requestInputData)
	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		BankInfo bankInfo = (BankInfo)this.gson.fromJson(requestInputData.toString(), BankInfo.class);

		LOGGER.info("Inside updateBankStatus bankInfo::" + bankInfo);
		try
		{
			
			irPlusResponseDetails = this.iBankMgmtCoreClient.deleteLocation(bankInfo);
			

		}
		catch (BusinessException e)
		{
			LOGGER.error("Exception in updateBank", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}

		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}


		}

	}
	
}