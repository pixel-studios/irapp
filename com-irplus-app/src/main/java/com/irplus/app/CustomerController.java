package com.irplus.app;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.Gson;
import com.irplus.core.client.customers.ICustomerCoreClient;
import com.irplus.dao.hibernate.entities.IRSSite;
import com.irplus.dto.CustomerBean;
import com.irplus.dto.CustomerContactsBean;
import com.irplus.dto.CustomerFilter;
import com.irplus.dto.CustomerGroupingMngmntInfo;
import com.irplus.dto.FormsManagementDTO;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;
import com.irplus.util.ValidationHelper;

@Controller
@Transactional
public class CustomerController {

	private static final Log log = LogFactory.getLog(CustomerController.class);
	
	Gson gson = new Gson();
	
	@Autowired 
	ICustomerCoreClient iCustomerCoreClient;
	
	@Autowired
	  private SessionFactory sessionFactory;
	  
	  public Session getMyCurrentSession()
	  {
	    Session session = null;
	    try
	    {
	      session = this.sessionFactory.getCurrentSession();
	    }
	    catch (HibernateException e)
	    {
	      log.error("Exception raised DaoImpl :: At current session creation time ::" + e);
	    }
	    return session;
	  }
	  
	String rootFolderPath =null;
	
	String customerLogoPath=null;

	@RequestMapping(value="/customer/create",method=RequestMethod.POST,produces="appliction/json",consumes= "application/json")
	@ResponseBody
	public ResponseEntity<String> createCustomer(@RequestBody String requestInputData) {
		
		log.info("Inside of CustomerControll :: CreateCustomer");
		
		HttpHeaders responseHeaders = new HttpHeaders();
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		CustomerBean customerBean= gson.fromJson(requestInputData.toString(),CustomerBean.class);
		CustomerContactsBean[] customerContact=customerBean.getCustomerContact();
		
		CustomerContactsBean contacts=customerContact[0];
		
		log.debug("insdie CustomerController : createCustomer() : Customer-companyName :"+customerBean.getCompanyName());
			List<String> errMsgs = new ArrayList<String>();
					try {
							boolean isValidReq = ValidationHelper.isValidCustomerReq(customerBean);
							boolean isValidContactReq = true;

							for (int i = 0; i < customerContact.length; i++)
							{
								if(isValidContactReq)
									isValidContactReq = ValidationHelper.isCreateCustomerContactReqValid(contacts);
								else
									break;

							}

							if (isValidReq&&isValidContactReq) {
								irPlusResponseDetails = this.iCustomerCoreClient.createCustomer(customerBean);
							}else{
								irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
								irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
							}
		
					 }catch (BusinessException e) {
								
								irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
								irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
								
								log.error("Exception raised in CustomerController :: createCustomer",e);
					}
			
					if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
					{ 			  			// Successfully Request Creation

						return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
					} 
					else 
					{
						if(!irPlusResponseDetails.isValidationSuccess())
						{
							return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
						}
						else
						{
							return IRPlusRestUtil.getGenericErrorResponse();
						}
					}
	}
	
	@RequestMapping(value="/customer/showAll/{bankId}",method=RequestMethod.GET,produces="application/json")
	public ResponseEntity<String> showAllCustomers(@PathVariable String bankId){

		HttpHeaders responseheaders = new HttpHeaders(); 

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
 		
			try {
				irPlusResponseDetails = iCustomerCoreClient.showAllCustomer(bankId);
				
			} catch (BusinessException e) {

				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				log.error("Exception raised in CustomerController :: showAllCustomers",e);
			}
			
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			}
			else {
					if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}	
			
	}
	
	/*  Show all banks - Branches  */ 
	
	@RequestMapping(value="/customer/show/banks/{siteId}",method=RequestMethod.GET,produces="application/json")
	public ResponseEntity<String> showAllBanks(@PathVariable String siteId){

		HttpHeaders responseheaders = new HttpHeaders(); 

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
 		
			try {
				irPlusResponseDetails = iCustomerCoreClient.showAllBanks(siteId);
				
			} catch (BusinessException e) {

				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				log.error("Exception raised in CustomerController :: showAllCustomers",e);
			}
			
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			}
			else {
					if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}	
			
	}
	
	//delte
	@RequestMapping(value="/customer/delete/{customerId}",method=RequestMethod.POST,produces="application/json")
	public ResponseEntity<String> deleteCutomerId(@PathVariable String customerId){
			
			HttpHeaders responseheaders = new HttpHeaders(); 

			IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
	 	
			boolean isValidCustomId = ValidationHelper.isValidReq(customerId);
			
			if (isValidCustomId) {
							
						try {
							irPlusResponseDetails = iCustomerCoreClient.deleteCustomerById(customerId);
							} 
							catch (BusinessException e){

								irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
								irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
								log.error("Exception raised in CutomerController :: deleteCustomer",e);
						
							}
			} else {
				
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}
					
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			}
			else {
					if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}			
	}
		
	//update
	@RequestMapping(value="/cutomer/update",method = RequestMethod.POST,consumes = "application/json" ,produces ="application/json")
	public ResponseEntity<String> updateCustomer(@RequestBody String customerBeanData){
			
			
			HttpHeaders responseheaders = new HttpHeaders(); 
			IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
			
			CustomerBean customerBean = gson.fromJson(customerBeanData.toString(), CustomerBean.class);
			
			boolean isValidReq = ValidationHelper.isValidCustomerReq(customerBean);
			log.info("UpdateCustomer " +customerBean.getCustomerId());
			
					if (isValidReq) {
								try {
								
									irPlusResponseDetails = iCustomerCoreClient.updateCustomer(customerBean);
								
								} catch (BusinessException e) {
									
									irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
									irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
									log.error("Exception raised in Customer Controller :: getCustomer",e);
								}
					} else {
						irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
					}
					
					if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
					}
					else {
							if (!irPlusResponseDetails.isValidationSuccess()) {
								return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
							} else {
								return IRPlusRestUtil.getGenericErrorResponse();
							}
					}		
		}

	//get
	@RequestMapping(value="/customer/showOne/{customerId}",method=RequestMethod.GET,consumes = "application/json",produces="application/json")
		
	public ResponseEntity<String> showOneCustomerById(@PathVariable String customerId){
		
		log.info("Role::" + customerId);
		
		HttpHeaders responseheaders = new HttpHeaders(); 
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
		List<String> errMsgs = new ArrayList<String>();
							
				try{	
					boolean isValidCustomerId=ValidationHelper.isValidReq(customerId);
					
					if (isValidCustomerId) {	
					
						CustomerBean customerBean =new CustomerBean();
						customerBean.setCustomerId(Integer.parseInt(customerId));
							
					irPlusResponseDetails = iCustomerCoreClient.getCustomerById(customerBean);				
				
					} else {
						errMsgs.add("Validation Error :: Not Valid CustomerID");
						irPlusResponseDetails.setErrMsgs(errMsgs);
						irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
						}
					
				} catch (BusinessException e) {
						
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);						
						log.error("Exception raised in CustomerController :: getCustomer",e);
					}
				
		
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
		} else {
				if (!irPlusResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
		}
	}
	
	
	@RequestMapping(value="/customer/update/status/{customerId}/{statusId}",method=RequestMethod.GET,produces="application/json")
	
	public ResponseEntity<String> updateCustomerStatus(@PathVariable String customerId , @PathVariable String statusId){
		
		log.info(" Inside of CustomerControll :: updateCustomerStatus" + customerId);
		
		HttpHeaders responseheaders = new HttpHeaders(); 
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
		List<String> errMsgs = new ArrayList<String>();
		
		StatusIdInfo statusIdInfo = new StatusIdInfo();
		boolean responseFlag = false;
		
		try{
		
		statusIdInfo.setId(Integer.parseInt(customerId));
		statusIdInfo.setStatus(Integer.parseInt(statusId));
		
		}catch (NumberFormatException e) {
			
			responseFlag = true;
			errMsgs.add(" Validation Error :: Not Valid statusId ");
			irPlusResponseDetails.setErrMsgs(errMsgs);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			log.error("Error : Exception raised "+e);
			
		}
		
		boolean isValidCustomerId = ValidationHelper.isValidReq(customerId);
		boolean isValidstatusId = ValidationHelper.isValidReq(statusId);
		boolean flag = true;
		
		if (isValidstatusId) {
			flag = false;
		}else{
			errMsgs.add(" Validation Error :: Not Valid statusId ");		
		} 
		
		if(isValidCustomerId){
			flag = false;
		}else{
			errMsgs.add(" Validation Error :: Not Valid CustomerId ");
		}		
		
		if (flag == false && responseFlag == false) {							
				try{										
					irPlusResponseDetails = iCustomerCoreClient.updateCutomerStaus(statusIdInfo);				
				
				} catch (BusinessException e) {
						
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);						
						log.error("Exception raised in CustomerController :: getCustomer",e);
					}
				
		} else {
		
			errMsgs.add(" Validation Error :: Not available  CustomerId or StatusId in DB ");
			irPlusResponseDetails.setErrMsgs(errMsgs);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
		
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
		} else {
				if (!irPlusResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
		}
	}

	@RequestMapping(value = "/custmr/fileUpload", method = RequestMethod.POST, headers = "content-type=multipart/*", produces = {"application/json"})
	 public ResponseEntity<String> imageUpload(MultipartHttpServletRequest request,
				@RequestParam(value="customerLogo",required = false) MultipartFile customerLogo) throws Exception {
			
			HttpHeaders responseHeaders = new HttpHeaders();

			IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
					
			try
			{
	    
				

				
				 String hql = "from IRSSite site where site.isactive=?";
			      
			      Query query = getMyCurrentSession().createQuery(hql);
			      query.setParameter(0, Integer.valueOf(1));
			      
			      List<IRSSite> siteList = query.list();
			      if (siteList.size() > 0)
			      {
			        rootFolderPath = ((IRSSite)siteList.get(0)).getRootFolderPath();
				
			      }
				
				if(rootFolderPath!=null) {
					 File file = new File(rootFolderPath + File.separator + "Images");
			            if (!file.exists()) {
			              if (file.mkdir()) {
			                System.out.println("Directory is created!");
			              } else {
			                System.out.println("Failed to create directory!");
			              }
			            }
			            customerLogoPath=rootFolderPath+File.separator + "Images"+File.separator+"CustomerLogo";
			            File file1 = new File(customerLogoPath);
			            if (!file1.exists()) {
			              if (file1.mkdir()) {
			                System.out.println("Directory is created!");
			              } else {
			                System.out.println("Failed to create directory!");
			              }
			            }
					}
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
			log.info("fileUploaded::" + customerLogo +" original File name : "+customerLogo.getOriginalFilename());
			
			String filePath = request.getRealPath("/");
			
			filePath = customerLogoPath+File.separator+customerLogo.getOriginalFilename();
			
			log.info("fileUploaded::" + customerLogo +" original File name : "+customerLogo.getOriginalFilename()+" filepath : "+ filePath);
			
			File newFile = new File(filePath);
			
			customerLogo.transferTo(newFile);
			
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			
			irPlusResponseDetails.setLogoPath(customerLogo.getOriginalFilename());
			}
			
			catch (Exception e) {
				log.error("Exception in ImageUpload", e);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
			{ 			  			// Successfully Request Creation

				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
			} 
			else 
			{
				if(!irPlusResponseDetails.isValidationSuccess())
				{
					return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
				}
				else
				{
					return IRPlusRestUtil.getGenericErrorResponse();
				}
			}
	    }
		

	@RequestMapping(value="/customer/filter",method=RequestMethod.POST,consumes="application/json",produces="appliction/json")
	public ResponseEntity<String> customerFilter(@RequestBody String customerData) {
		
				log.info("Inside of CustomerControll :: CreateCustomer");
				
				HttpHeaders responseHeader = new HttpHeaders();
				
				IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		 		 
				CustomerFilter customerBean= gson.fromJson(customerData.toString(), CustomerFilter.class);
				
				log.debug("insdie CustomerController : customerFilter() : Customer-companyName :"+customerBean.getCompanyName());
				List<String> errMsgs = new ArrayList<String>();
							try {
							
								irPlusResponseDetails = iCustomerCoreClient.customerFilter(customerBean);
								
								irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
							} catch (BusinessException e) {
								
								irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
								irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
								
								log.error("Exception raised in CustomerController :: createCustomer",e);
							}
			
					
					if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
						return new ResponseEntity<>(gson.toJson(irPlusResponseDetails),responseHeader ,HttpStatus.OK);
					} 
					else {
						if (!irPlusResponseDetails.isValidationSuccess()) {
					
							return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseHeader, HttpStatus.CONFLICT);
						
						} else {			
							return IRPlusRestUtil.getGenericErrorResponse();
						}
					}
		}

	
	@RequestMapping(value="/customer/showAllFileProcessing",method=RequestMethod.GET,produces="application/json")
	public ResponseEntity<String> showAllFileProcessing(){

		HttpHeaders responseheaders = new HttpHeaders(); 

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
 		
			try {
				irPlusResponseDetails = iCustomerCoreClient.showAllFileProcessing();
				
			} catch (BusinessException e) {

				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				log.error("Exception raised in CustomerController :: showAllFileProcessing",e);
			}
			
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			}
			else {
					if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}	
			
	}
	
	@RequestMapping(value="/customer/showAllBusinessProcessing",method=RequestMethod.GET,produces="application/json")
	public ResponseEntity<String> showAllBusinessProcessing(){

		HttpHeaders responseheaders = new HttpHeaders(); 

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
 		
			try {
				irPlusResponseDetails = iCustomerCoreClient.showAllBusinessProcessing();
				
			} catch (BusinessException e) {
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				log.error("Exception raised in CustomerController :: showAllBusinessProcessing",e);
			}
			
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			}
			else {
					if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}	
	}
	
	@RequestMapping(value="/customer/getBankBranch/{bankId}", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> getBankBranch(@PathVariable String bankId)
	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		log.info("siteId::" + bankId);
		try
		{
			boolean isValidReq = ValidationHelper.isValidReq(bankId);

			if (isValidReq)
			{
				irPlusResponseDetails = this.iCustomerCoreClient.showBankBranch(bankId);
			}
			else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
		} catch (BusinessException e) {
			log.error("Exception in listBank", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}


@RequestMapping(value="/autocomplete/filter",method=RequestMethod.GET,produces="application/json")
/*public ResponseEntity<String> Autocomplete(){*/
	public ResponseEntity<String> Autocomplete(@RequestParam("term") String term,@RequestParam("userid") String userid) {
	HttpHeaders responseheaders = new HttpHeaders(); 

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		try {
		
		irPlusResponseDetails = iCustomerCoreClient.Autocomplete(term,userid);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		} catch (Exception e) {

			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
		
		}
		
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.OK);
		}
		else {
				if (!irPlusResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
		}	
		
}

@RequestMapping(value="/autocomplete/filter/bank",method=RequestMethod.GET,produces="application/json")
/*public ResponseEntity<String> Autocomplete(){*/
	public ResponseEntity<String> AutocompleteBank(@RequestParam("term") String term,@RequestParam("userid") String userid) {
	HttpHeaders responseheaders = new HttpHeaders(); 

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		try {
		
		irPlusResponseDetails = iCustomerCoreClient.AutocompleteBank(term,userid);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		} catch (Exception e) {

			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
		
		}
		
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.OK);
		}
		else {
				if (!irPlusResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
		}	
		
}

@RequestMapping(value="/autocomplete/filter/customer",method=RequestMethod.GET,produces="application/json")
/*public ResponseEntity<String> Autocomplete(){*/
	public ResponseEntity<String> AutocompleteCustomer(@RequestParam("term") String term,@RequestParam("userid") String userid) {
	HttpHeaders responseheaders = new HttpHeaders(); 

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		try {
			
			
			
		irPlusResponseDetails = iCustomerCoreClient.AutocompleteCustomer(term,userid);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		} catch (Exception e) {

			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
		}
		
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.OK);
		}
		else {
				if (!irPlusResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
		}	
		
}


@RequestMapping(value="/autocomplete/filter/customerCode",method=RequestMethod.GET,produces="application/json")
/*public ResponseEntity<String> Autocomplete(){*/
	public ResponseEntity<String> AutocompleteCustomerCode(@RequestParam("term") String term,@RequestParam("userid") String userid) {
	HttpHeaders responseheaders = new HttpHeaders(); 

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		try {
			
			
			
		irPlusResponseDetails = iCustomerCoreClient.AutocompleteCustomerCode(term,userid);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		} catch (Exception e) {

			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
		}
		
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.OK);
		}
		else {
				if (!irPlusResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
		}	
		
}


@RequestMapping(value="/customer/dataentry",method=RequestMethod.POST,produces="application/json",consumes= "application/json")
@ResponseBody
public ResponseEntity<String> AddCustomerDataEntry(@RequestBody String requestInputData) {

	HttpHeaders responseHeaders = new HttpHeaders();
	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
	log.info("rtrtrtert");
	log.info("wwwwwwwwwwwwwweeeeeeeeeeeeeeeerrrrrrgggggggggggggdddddddddddddddddddddddddddddrrrrrr"+requestInputData);
	FormsManagementDTO formsManagement= gson.fromJson(requestInputData.toString(),FormsManagementDTO.class);


		List<String> errMsgs = new ArrayList<String>();
		try {
			
			
			log.info("fdgddddd");
			log.info("wwwwwwwwwwwwwweeeeeeeeeeeeeeeerrrrrrrrrrrr"+formsManagement.getFormCode());
			log.info("erwwwwwwwwwwwwwwwwwwwwwwwwww"+formsManagement.getFormName());
			log.info("erwwwwwwwwwwwwwwwwwwwwwwwwww"+formsManagement.getWatchFolderDataentry());
			log.info("erwwwwwwwwwwwwwwwwwwwwwwwwww"+formsManagement.getBatchClassDataentry());
			log.info("erwwwwwwwwwwwwwwwwwwwwwwwwww"+formsManagement.getUserid());
			log.info("erwwwwwwwwwwwwwwwwwwwwwwwwww"+formsManagement.getIsactive());
						/*boolean isValidReq = ValidationHelper.isValidCustomerReq(customergroupingMngmntInfo);
						boolean isValidContactReq = true;


						if (isValidReq&&isValidContactReq) {*/
							irPlusResponseDetails = this.iCustomerCoreClient.AddCustomerDataEntry(formsManagement);
				
	
				 }catch (BusinessException e) {
							
							irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
							irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
							
							log.error("Exception raised in CustomerGroupingController :: createCustomerGroup",e);
				}
		
				if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
				{ 			  		

					return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
				} 
				else 
				{
					if(!irPlusResponseDetails.isValidationSuccess())
					{
						return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
					}
					else
					{
						return IRPlusRestUtil.getGenericErrorResponse();
					}
				}
}







}