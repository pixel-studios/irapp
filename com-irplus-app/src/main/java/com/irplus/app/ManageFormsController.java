package com.irplus.app;

import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.irplus.core.client.manageForms.IManagmntFormsetupCoreClient;
import com.irplus.dto.FormsManagementDTO;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;
import com.irplus.util.ValidationHelper;

@Controller
@RequestMapping(value="/manageForms/")
public class ManageFormsController {

	private static final Logger log = Logger.getLogger(ManageFormsController.class);
	
	private Gson gson = new Gson();
	
	@Autowired
	IManagmntFormsetupCoreClient managmntFormsetupCoreClient;
	
	@RequestMapping(value = "/create" , method = RequestMethod.POST , consumes = "application/json" , produces = "application/json")
	public ResponseEntity<String> crateManagmntFormSetup(@RequestBody String crateManagmntFormSetupData){
		
		HttpHeaders responseHeaders = new HttpHeaders();
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		try {
			FormsManagementDTO formsManagementDTO =	gson.fromJson(crateManagmntFormSetupData.toString(), FormsManagementDTO.class);		
			irPlusResponseDetails = managmntFormsetupCoreClient.createMngmntformSetup(formsManagementDTO);
			
		}catch(BusinessException e){
		
			log.error("Exception in crateManagmntFormSetup", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		
		if(irPlusResponseDetails.getStatusMsg().equalsIgnoreCase( IRPlusConstants.SUCCESS_MSG)) {
			// Successfully Request Creation
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
			
		}
		else
		{
			if(!irPlusResponseDetails.isValidationSuccess()) 
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}				
	}
	

	@RequestMapping(value = "/update" , method = RequestMethod.POST , consumes = "application/json" , produces = "application/json")
	public ResponseEntity<String> updateManagmntFormSetup(@RequestBody String updateManagmntFormSetupData){
		
		HttpHeaders responseHeaders = new HttpHeaders();
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();	
		
		try {
			FormsManagementDTO formsManagementDTO =	gson.fromJson(updateManagmntFormSetupData.toString(), FormsManagementDTO.class);		
			irPlusResponseDetails = managmntFormsetupCoreClient.updateMngmntformSetup(formsManagementDTO);
			
		}catch(BusinessException e){
		
			log.error("Exception in updateManagmntFormSetup", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		
		if(irPlusResponseDetails.getStatusMsg().equalsIgnoreCase( IRPlusConstants.SUCCESS_MSG)) {
			// Successfully Request Creation
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
			
		}
		else
		{
			if(!irPlusResponseDetails.isValidationSuccess()) 
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}				
	}
	

	@RequestMapping(value = "/showOne/{formsetupid}" , method = RequestMethod.GET ,  produces = "application/json")
	public ResponseEntity<String> showOneMngmntformSetup(@PathVariable String formsetupid ){
		
		HttpHeaders responseHeaders = new HttpHeaders();
		
		List<String> errMsgs = new ArrayList<String>();
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();	
				
		boolean isValidReq = false;
		try {
			
			 isValidReq = ValidationHelper.isValidReq(formsetupid);
		
			 if(isValidReq){
				 
				irPlusResponseDetails = managmntFormsetupCoreClient.showOneMngmntformSetup(formsetupid);
				
			 }else{
				 errMsgs.add("Validatin Error : Please Enter Valid No");
				 irPlusResponseDetails.setErrMsgs(errMsgs);
				 irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				 irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			 }
				
		}catch(BusinessException e){
		
			log.error("Exception in showOneMngmntformSetup", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		
		if(irPlusResponseDetails.getStatusMsg().equalsIgnoreCase( IRPlusConstants.SUCCESS_MSG)) {
			// Successfully Request Creation
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
			
		}
		else
		{
			if(!irPlusResponseDetails.isValidationSuccess()) 
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}				
	}
	
	
	@RequestMapping(value = "/fieldTypes" , method = RequestMethod.GET ,  produces = "application/json")
	public ResponseEntity<String> dynamicSelectFieldTypes( ){
		
		HttpHeaders responseHeaders = new HttpHeaders();
		
		List<String> errMsgs = new ArrayList<String>();
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();	
				
		boolean isValidReq = false;
		try {
						 
				irPlusResponseDetails = managmntFormsetupCoreClient.dynamicSelectFieldTypes();
					
		}catch(BusinessException e){
		
			log.error("Exception in showOneMngmntformSetup", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		
		if(irPlusResponseDetails.getStatusMsg().equalsIgnoreCase( IRPlusConstants.SUCCESS_MSG)){
			// Successfully Request Creation
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
			
		}
		else
		{
			if(!irPlusResponseDetails.isValidationSuccess()) 
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}				
	}	
	

	@RequestMapping(value = "/statusUpdate/{formvalidid}/{stausid}" , method = RequestMethod.GET ,  produces = "application/json")
	public ResponseEntity<String>statusUpdate(@PathVariable String formvalidid ,@PathVariable String stausid  ){
		

		HttpHeaders responseHeaders = new HttpHeaders();

		List<String> errMsgs = new ArrayList<String>();
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();	

		StatusIdInfo statusIdInfo = new StatusIdInfo();
		boolean isValidReq1 = false;
		boolean isValidReq2 = false;

		try {
			
			 isValidReq1 = ValidationHelper.isValidReq(formvalidid);
			 isValidReq2 = ValidationHelper.isValidReq(stausid);			 			
			 
			 if(isValidReq1 &&  isValidReq2){
			
				 statusIdInfo.setId(Integer.parseInt(formvalidid));
				 statusIdInfo.setStatus(Integer.parseInt(stausid));
				 
			     irPlusResponseDetails = managmntFormsetupCoreClient.statusUpdate(statusIdInfo);
				
			 }else{
				 errMsgs.add("Validatin Error : Please Enter Valid Nos");
				 irPlusResponseDetails.setErrMsgs(errMsgs);
				 irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				 irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			 }
		
			
		}catch(BusinessException e){
		
			log.error("Exception in showOneMngmntformSetup", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		
		if(irPlusResponseDetails.getStatusMsg().equalsIgnoreCase( IRPlusConstants.SUCCESS_MSG)) {
			// Successfully Request Creation
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
			
		}
		else
		{
			if(!irPlusResponseDetails.isValidationSuccess()) 
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}				
	}
	
	
	@RequestMapping(value = "/showListofForms" , method = RequestMethod.GET ,  produces = "application/json")
	public ResponseEntity<String> showListofForms( ){
		
		HttpHeaders responseHeaders = new HttpHeaders();
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();	
				
		log.debug(" Inside of ManageForms Controller :: showListForms ");
		try {						 
				irPlusResponseDetails = managmntFormsetupCoreClient.showListofForms();
					
		}catch(BusinessException e){
		
			log.error("Exception in showOneMngmntformSetup", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		
		if(irPlusResponseDetails.getStatusMsg().equalsIgnoreCase( IRPlusConstants.SUCCESS_MSG)){
			// Successfully Request Creation
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
			
		}
		else
		{
			if(!irPlusResponseDetails.isValidationSuccess()) 
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}				
	}	
	
	
	@RequestMapping(value="/customer/manageform/showAll/{customerid}",method=RequestMethod.GET,produces="application/json")
	public ResponseEntity<String> showAllCustomerForms(@PathVariable Integer customerid){

		HttpHeaders responseheaders = new HttpHeaders(); 

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
 		
			try {
				irPlusResponseDetails = managmntFormsetupCoreClient.showAllCustomerForms(customerid);
				
			} catch (BusinessException e) {

				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				log.error("Exception raised in CustomerController :: showAllCustomers",e);
			}
			
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			}
			else {
					if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}	
			
	}
	
	
	
	
	
}
