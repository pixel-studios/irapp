package com.irplus.app;

import com.irplus.app.achreader.ACHBatch;
import com.irplus.app.achreader.ACHEntry;
import com.irplus.app.achreader.ACHFile;
import com.irplus.app.achreader.ACHRecordAddenda;
import com.irplus.dao.hibernate.entities.IRSBank;
import com.irplus.dao.hibernate.entities.IRSSite;
import com.irplus.dao.hibernate.entities.IrFFileAudit;
import com.irplus.dao.hibernate.entities.IrFPBatchHeaderRecord;
import com.irplus.dao.hibernate.entities.IrFPCCDAddendaRecord;
import com.irplus.dao.hibernate.entities.IrFPEntryDetailRecord;
import com.irplus.dao.hibernate.entities.IrFPFileHeaderRecord;
import com.irplus.dao.hibernate.entities.IrMFileTypes;
import com.irplus.dao.hibernate.entities.IrSBankBranch;
import com.irplus.dao.hibernate.entities.IrSCustomers;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Vector;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

@Controller
@Transactional
public class FileProcessController
{
  File error = null;
  File source = null;
  String todayDate = null;
  File src = null;
 
  private ACHFile achFile = null;
  String fileName = null;
  String achFileName = null;
  Long bankId;
  String bankFolder = null;
  int customerId;
  Date date = new Date();
  DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
  DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
  String currentDate = null;
  String currentTime = null;
  String systemTime = null;
  String numberAsString = null;
  String rootFolderPath = null;
  String imagePath = null;
  
  int totalFiles=0;
  int processedFiles=0;
  int errorFiles=0;
  int corruptedFiles=0;
  String description=null;
  String success="success";
  
  
  
  private static final Logger LOGGER = Logger.getLogger(FileProcessController.class);
  @Autowired
  private SessionFactory sessionFactory;
  
  public Session getMyCurrentSession()
  {
    Session session = null;
    try
    {
      session = this.sessionFactory.getCurrentSession();
    }
    catch (HibernateException e)
    {
      LOGGER.error("Exception raised DaoImpl :: At current session creation time ::" + e);
    }
    return session;
  }
  
  String[] folderList = null;
  
  public void doTask()
  {
    try
    {
      String[] folders = { "ACH-Files", "WIRE-Files", "LOCKBOX-Files", "BANKS", "ERROR-DOC", "temp" };
      
      String hql = "from IRSSite site where site.isactive=?";
      
      Query query = getMyCurrentSession().createQuery(hql);
      query.setParameter(0, Integer.valueOf(1));
      
      List<IRSSite> siteList = query.list();
      if (siteList.size() > 0)
      {
        this.rootFolderPath = ((IRSSite)siteList.get(0)).getRootFolderPath();
        
        this.source = new File(((IRSSite)siteList.get(0)).getRootFolderPath());
        if (this.source != null) {
          for (int i = 0; i < folders.length; i++)
          {
            File file = new File(this.source + File.separator + folders[i]);
            if (!file.exists()) {
              if (file.mkdir()) {
               // System.out.println("Directory is created!");
              } else {
               // System.out.println("Failed to create directory!");
              }
            }
          }
        }
        this.src = new File(this.source + File.separator + "ACH-Files");
        this.error = new File(this.source + File.separator + "ERROR-DOC");
       // copyFolder(this.src, this.dest);
        String fileName = null;
        File[] files = src.listFiles();
        totalFiles=src.listFiles().length;
        
        if (files.length != 0) {
        	
        
        	
          for (int z = 0; z < files.length; z++)
          {
            this.achFileName = files[z].getName();
            fileName = this.src + File.separator + files[z].getName();
            
            setAchFile(new ACHFile(fileName));
            
            this.todayDate = getMonthDay();
            this.systemTime = getTime();
            
            String bankQuery = "from IRSBank bank where bank.rtNumber=?";
            Query bnk = getMyCurrentSession().createQuery(bankQuery);
            bnk.setParameter(0,getAchFile().getFileHeader().getImmediateDestination().trim());
            List<IRSBank> bankList = bnk.list();
            if (bankList.size() > 0)
            {
              Vector<ACHBatch> achBatches = getAchFile().getBatches();
              
              String qry = "from IrSCustomers customer where customer.compIdentNo=?";
              Query cust = getMyCurrentSession().createQuery(qry);
              cust.setParameter(0, ((ACHBatch)achBatches.get(0)).getBatchHeader().getCompanyId());
              
              List<IrSCustomers> customerList = cust.list();
              if (customerList.size() > 0)
              {
                this.customerId = ((IrSCustomers)customerList.get(0)).getCustomerId().intValue();
                
                String bankIdFromdb = ((IRSBank)bankList.get(0)).getIRBankCode();
                this.bankId = Long.valueOf(((IRSBank)bankList.get(0)).getBankId());
                this.bankFolder = (this.rootFolderPath + File.separator + "BANKS" + File.separator + ((IRSBank)bankList.get(0)).getBankFolder() + File.separator + this.todayDate);
                
                this.imagePath = (((IRSBank)bankList.get(0)).getBankFolder() + File.separator + this.todayDate);
                File createBank = new File(this.bankFolder);
                if (!createBank.exists())
                {
                  createBank.mkdir();
                 // System.out.println("Directory created");
                }
                try
                {
              
                  IrFPFileHeaderRecord file = new IrFPFileHeaderRecord();
                  IrFPBatchHeaderRecord batch = null;
                  IrFPEntryDetailRecord transaction = null;
                  IrFPCCDAddendaRecord payment = null;
                  IrFPCCDAddendaRecord remittance = null;
                  IrFPCCDAddendaRecord scandoc = null;
                  IrFPCCDAddendaRecord car = null;
                  
                  IRSBank bank = new IRSBank();
                  
                  IRSBank fileBankId = new IRSBank();
                  
                  IrSCustomers customer = new IrSCustomers();
                  IrMFileTypes fileType = new IrMFileTypes();
                  
                  CreatePaymentTIFF image = null;
                  CreatePaymentTIFF batchImage = null;
                  CreatePNG pngImage = null;
                  CreateRemittanceTIFF remittanceTiff = null;
                  RemittancePNG remittanceImage = null;
                  CreateTIFF scanTiff = null;
                  CreatePNG scanImage = null;
                  currentDate=getCurrentDay();
				  currentTime=getCurrentTime();
				  fileBankId.setBankId(this.bankId.longValue());
                  file.setBank(fileBankId);
                  file.setProcessedDate(this.currentDate);
                  file.setProcessedTime(this.currentTime);
                  file.setAchFileName(this.achFileName);
                  file.setIsProcessed(1);
                  fileType.setFiletypeid(Long.valueOf(129L));
                  file.setFileType(fileType);
                  
                  String batchNo = null;
                  String batchDate = null;
                  for (int i = 0; i < achBatches.size(); i++)
                  {
                    batch = new IrFPBatchHeaderRecord();
                    
                    batch.setStandardEntryClass(((ACHBatch)achBatches.get(i)).getBatchHeader().getStandardEntryClassCode());
                    Integer value = Integer.valueOf(Integer.parseInt(((ACHBatch)achBatches.get(i)).getBatchHeader().getCompanyDescriptiveDate()));
                    SimpleDateFormat originalFormat = new SimpleDateFormat("yyMMdd");
                    Date date = originalFormat.parse(value.toString());
                    SimpleDateFormat newFormat = new SimpleDateFormat("MM-dd-yy");
                    batchDate = newFormat.format(date);
                    batch.setCompanyDescriptiveDate(batchDate);
                    batch.setEffectiveEntryDate(((ACHBatch)achBatches.get(i)).getBatchHeader().getEffectiveEntryDate());
                    customer.setCustomerId(Integer.valueOf(this.customerId));
                    batch.setCustomer(customer);
                    batch.setBatchNumber(((ACHBatch)achBatches.get(i)).getBatchHeader().getBatchNumber());
                    batchNo = ((ACHBatch)achBatches.get(i)).getBatchHeader().getBatchNumber();
                    
                    Vector<ACHEntry> achEntries = ((ACHBatch)achBatches.get(i)).getEntryRecs();
                    batch.setTransactionCount(Integer.toString(((ACHBatch)achBatches.get(i)).getEntryRecs().size()));
                    int paymentCount = 0;
                    int remittanceCount = 0;
                    int scanDocCount = 0;
                    int itemCount = 0;
                    for (int j = 0; j < achEntries.size(); j++)
                    {
                      int payCount = 0;
                      int remCount = 0;
                      int scanCount = 0;
                      int itmCount = 0;
                      
                      FileOutputStream fos = null;
                      transaction = new IrFPEntryDetailRecord();
                      String fileAmount = null;
                      
                      payment = new IrFPCCDAddendaRecord();
                      remittance = new IrFPCCDAddendaRecord();
                      scandoc = new IrFPCCDAddendaRecord();
                      fileAmount = ((ACHEntry)achEntries.get(j)).getEntryDetail().getAmount();
                      BigInteger amount = new BigInteger(fileAmount);
                      BigDecimal two = new BigDecimal(amount, 2);
                      
                      String dbAmount = two.toString();
                      
                      String[] convert = dbAmount.split("\\.");
                      
                      int a = Integer.parseInt(convert[0]);
                      String b = convert[1];
                      
                      int n = a;
                      String amountInWords = null;
                      NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
                      String numberAsString = numberFormat.format(n);
                      String commaSepaaratedNumber = null;
                      String receivingDFId=((ACHEntry)achEntries.get(j)).getEntryDetail().getReceivingDfiId();
                      String ticketType=null;
                      if (b.equals("00"))
                      {
                        commaSepaaratedNumber = numberAsString + ".00";
                        amountInWords = convert(n) + " Dollars and 0 / 100";
                      }
                      else
                      {
                        commaSepaaratedNumber = numberAsString + "." + b;
                        amountInWords = convert(n) + " Dollars and " + b + " / 100";
                      }
                      
                      if(receivingDFId.equals("00000000")) {
                    	  ticketType="CC PAYMENT";  
                      }else {
                    	  ticketType="ACH PAYMENT";
                      }
                      payment.setAmount(commaSepaaratedNumber);
                      payment.setEntryType("PAYM");
                      
                      String[] data = new String[8];
                      data[0] = ((ACHEntry)achEntries.get(j)).getEntryDetail().getIndividualIdNbr();
                      data[1] = batchDate;
                      data[2] = ((ACHEntry)achEntries.get(j)).getEntryDetail().getIndividualName();
                      data[3] = commaSepaaratedNumber;
                      data[4] = amountInWords;
                      data[5] = ticketType;
                      data[6] = ((ACHEntry)achEntries.get(j)).getEntryDetail().getDfiAcctNbr();
                      data[7] = ((ACHEntry)achEntries.get(j)).getEntryDetail().getTraceNumber();
                      
                      image = new CreatePaymentTIFF(data);
                      try
                      {
                        String paymentFile = this.bankFolder + File.separator + batchNo + "P" + j + ".tiff";
                        String tiffImagePath = this.imagePath + File.separator + batchNo + "P" + j + ".tiff";
                        fos = new FileOutputStream(paymentFile);
                        fos.write(image.getStream().toByteArray());
                        fos.close();
                        fos = null;
                        payment.setFciName(tiffImagePath);
                        payCount = 1;
                        paymentCount++;
                      }
                      catch (IOException ioExp)
                      {
                        ioExp.printStackTrace();
                        System.out.println("The error " + ioExp.toString());
                      }
                      pngImage = new CreatePNG(data);
                      try
                      {
                        String paymentFile = this.bankFolder + File.separator + batchNo + "P" + j + ".png";
                        String pngImagePath = this.imagePath + File.separator + batchNo + "P" + j + ".png";
                        fos = new FileOutputStream(paymentFile);
                        fos.write(pngImage.getStream().toByteArray());
                        fos.close();
                        fos = null;
                        payment.setFciPng(pngImagePath);
                      }
                      catch (IOException ioExp)
                      {
                        ioExp.printStackTrace();
                   //     System.out.println("The error " + ioExp.toString());
                      }
                      payment.setIrFPEntryDetailRecord(transaction);
                      transaction.getIrFPCCDAddendaRecord().add(payment);
                      payment.setIrFPFileHeaderRecord(file);
                      file.getIrFPCCDAddendaRecord().add(payment);
                      
                      remittance.setAmount(commaSepaaratedNumber);
                      remittance.setEntryType("REMT");
                      
                      String[] remi = new String[5];
                      remi[0] = ((ACHEntry)achEntries.get(j)).getEntryDetail().getIndividualIdNbr();
                      remi[1] = batchDate;
                      remi[2] = ((ACHEntry)achEntries.get(j)).getEntryDetail().getIndividualName();
                      remi[3] = commaSepaaratedNumber;
                      
                      remi[4] = ((ACHEntry)achEntries.get(j)).getEntryDetail().getTraceNumber();
                      
                      remittanceTiff = new CreateRemittanceTIFF(remi);
                      try
                      {
                        String remittanceFile = this.bankFolder + File.separator + batchNo + "R" + j + ".tiff";
                        String tiffImagePath = this.imagePath + File.separator + batchNo + "R" + j + ".tiff";
                        fos = new FileOutputStream(remittanceFile);
                        fos.write(remittanceTiff.getStream().toByteArray());
                        fos.close();
                        fos = null;
                        remittance.setFciName(tiffImagePath);
                        remCount = 1;
                        remittanceCount++;
                      }
                      catch (IOException ioExp)
                      {
                        ioExp.printStackTrace();
                      //  System.out.println("The error " + ioExp.toString());
                      }
                      remittanceImage = new RemittancePNG(remi);
                      try
                      {
                        String remittanceFile = this.bankFolder + File.separator + batchNo + "R" + j + ".png";
                        String pngImagePath = this.imagePath + File.separator + batchNo + "R" + j + ".png";
                        fos = new FileOutputStream(remittanceFile);
                        fos.write(remittanceImage.getStream().toByteArray());
                        fos.close();
                        fos = null;
                        remittance.setFciPng(pngImagePath);
                      }
                      catch (IOException ioExp)
                      {
                        ioExp.printStackTrace();
                      //  System.out.println("The error " + ioExp.toString());
                      }
                      remittance.setIrFPEntryDetailRecord(transaction);
                      transaction.getIrFPCCDAddendaRecord().add(remittance);
                      remittance.setIrFPFileHeaderRecord(file);
                      file.getIrFPCCDAddendaRecord().add(remittance);
                      
                      Vector<ACHRecordAddenda> achAddendas = ((ACHEntry)achEntries.get(j)).getAddendaRecs();
                      List<IrFPCCDAddendaRecord> list = new ArrayList();
                      int scanDocSize = achAddendas.size();
                      for (int k = 0; k < achAddendas.size(); k++)
                      {
                        car = new IrFPCCDAddendaRecord();
                        car.setPaymentRelatedInformation(((ACHRecordAddenda)achAddendas.get(k)).getPaymentRelatedInfo());
                        list.add(car);
                      }
                      if (list.size() > 0)
                      {
                        scanTiff = new CreateTIFF(list);
                        try
                        {
                          String scanDOC = this.bankFolder + File.separator + batchNo + "S" + j + ".tiff";
                          String tiffImagePath = this.imagePath + File.separator + batchNo + "S" + j + ".tiff";
                          fos = new FileOutputStream(scanDOC);
                          fos.write(scanTiff.getStream().toByteArray());
                          fos.close();
                          fos = null;
                          scandoc.setFciName(tiffImagePath);
                          scandoc.setEntryType("SCAN");
                          scanCount = 1;
                          scanDocCount++;
                        }
                        catch (IOException ioExp)
                        {
                          ioExp.printStackTrace();
                        //  System.out.println("The error " + ioExp.toString());
                        }
                        scanImage = new CreatePNG(list);
                        try
                        {
                          String scanDOC = this.bankFolder + File.separator + batchNo + "S" + j + ".png";
                          String pngImagePath = this.imagePath + File.separator + batchNo + "S" + j + ".png";
                          fos = new FileOutputStream(scanDOC);
                          fos.write(scanImage.getStream().toByteArray());
                          fos.close();
                          fos = null;
                          scandoc.setFciPng(pngImagePath);
                          scandoc.setIrFPEntryDetailRecord(transaction);
                          transaction.getIrFPCCDAddendaRecord().add(scandoc);
                          scandoc.setIrFPFileHeaderRecord(file);
                          file.getIrFPCCDAddendaRecord().add(scandoc);
                        }
                        catch (IOException ioExp)
                        {
                          ioExp.printStackTrace();
                      //    System.out.println("The error " + ioExp.toString());
                        }
                      }
                      if (payCount > 0) {
                        transaction.setPaymentCount(Integer.toString(payCount));
                      } else {
                        transaction.setPaymentCount(Integer.toString(payCount));
                      }
                      if (remCount > 0) {
                        transaction.setRemittanceCount(Integer.toString(remCount));
                      } else {
                        transaction.setRemittanceCount(Integer.toString(remCount));
                      }
                      if (scanCount > 0) {
                        transaction.setScandocCount(Integer.toString(scanCount));
                      } else {
                        transaction.setScandocCount(Integer.toString(scanCount));
                      }
                      transaction.setItemCount(Integer.toString(payCount + remCount));
                      transaction.setIrFPBatchHeaderRecord(batch);
                      batch.getIrFPEntryDetailRecord().add(transaction);
                      transaction.setIrFPFileHeaderRecord(file);
                      file.getIrFPEntryDetailRecord().add(transaction);
                      
                      batch.setScandocCount(Integer.toString(scanDocCount));
                    }
                    batchImage = new CreatePaymentTIFF(batchNo);
                    try
                    {
                      String paymentFile = this.bankFolder + File.separator + batchNo + "B" + i + ".tiff";
                      String tiffImagePath = this.imagePath + File.separator + batchNo + "B" + i + ".tiff";
                      FileOutputStream fos = new FileOutputStream(paymentFile);
                      fos.write(batchImage.getStream().toByteArray());
                      fos.close();
                      fos = null;
                      batch.setBatchImage(tiffImagePath);
                    }
                    catch (IOException ioExp)
                    {
                      ioExp.printStackTrace();
                    //  System.out.println("The error " + ioExp.toString());
                    }
                    pngImage = new CreatePNG(batchNo);
                    try
                    {
                      String paymentFile = this.bankFolder + File.separator + batchNo + "B" + i + ".png";
                      String pngImagePath = this.imagePath + File.separator + batchNo + "B" + i + ".png";
                      FileOutputStream fos = new FileOutputStream(paymentFile);
                      fos.write(pngImage.getStream().toByteArray());
                      fos.close();
                      fos = null;
                      batch.setBatchPNG(pngImagePath);
                    }
                    catch (IOException ioExp)
                    {
                      ioExp.printStackTrace();
                      System.out.println("The error " + ioExp.toString());
                    }
                    batch.setItemCount(Integer.toString(paymentCount + remittanceCount));
                    batch.setPaymentCount(Integer.toString(paymentCount));
                    batch.setRemittanceCount(Integer.toString(remittanceCount));
                    BigInteger amount = new BigInteger(((ACHBatch)achBatches.get(i)).getBatchControl().getTotCreditDollarAmt());
                    BigDecimal two = new BigDecimal(amount, 2);
                    String dbAmount = two.toString();
                    String[] convert = dbAmount.split("\\.");
                    
                    int a = Integer.parseInt(convert[0]);
                    String b = convert[1];
                    
                    int n = a;
                    
                    NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
                    String numberAsString = numberFormat.format(n);
                    String commaSepaaratedNumber = null;
                    if (b.equals("00")) {
                      commaSepaaratedNumber = numberAsString + ".00";
                    } else {
                      commaSepaaratedNumber = numberAsString + "." + b;
                    }
                    batch.setBatchAmount(commaSepaaratedNumber);
                    batch.setIrFPFileHeaderRecord(file);
                    file.getIrFPBatchHeaderRecord().add(batch);
                  }
                //  System.out.println(getAchFile().getFileControl());
                  
                  InputStream inStream = null;
                  OutputStream outStream = null;
                  try
                  {
                    String moveFile = fileName;
                    String moveTo = this.bankFolder + File.separator + this.achFileName;
                    File afile = new File(moveFile);
                    File bfile = new File(moveTo);
                    
                    inStream = new FileInputStream(afile);
                    outStream = new FileOutputStream(bfile);
                    
                    byte[] buffer = new byte['?'];
                    int length;
                    while ((length = inStream.read(buffer)) > 0) {
                      outStream.write(buffer, 0, length);
                    }
                    IrFFileAudit fileAudit = new IrFFileAudit();
                    currentDate=getCurrentDay();
                    fileAudit.setAuditDate(currentDate);
                    fileAudit.setFileType("ACH");
                    fileAudit.setProcessedDateTime(date);
                    fileAudit.setFileName(achFileName);
                    fileAudit.setStatus("success");
                    fileAudit.setIsProcessed(1);
                    fileAudit.setDescription("File Processed Successfully");
                    getMyCurrentSession().save(fileAudit);
                 
                    inStream.close();
                    outStream.flush();
                    outStream.close();
                    outStream = null;
                    
                    afile.delete();
                    
                  //  System.out.println("File is copied successful!");
                    
                    processedFiles++;
                    
                   
                  }
                  catch (IOException e)
                  {
                    e.printStackTrace();
                  }
                  Long flag = (Long)getMyCurrentSession().save(file);
               
                  
                 
                  
                  
                  files[z].delete();
                }
                catch (Exception localException1) {}
              }
              else
              {
                InputStream inStream = null;
                OutputStream outStream = null;
                try
                {
                	IrFFileAudit logAudit = new IrFFileAudit();	
                  String moveFile = fileName;
                  String moveTo = this.source + File.separator + "ERROR-DOC" + File.separator + this.achFileName;
                  File afile = new File(moveFile);
                  File bfile = new File(moveTo);
                  
                  inStream = new FileInputStream(afile);
                  outStream = new FileOutputStream(bfile);
                  
                  byte[] buffer = new byte['?'];
                  int length;
                  while ((length = inStream.read(buffer)) > 0) {
                    outStream.write(buffer, 0, length);
                  }
                  currentDate=getCurrentDay();
                  logAudit.setAuditDate(currentDate);
                  logAudit.setFileType("ACH");
                  logAudit.setProcessedDateTime(date);
                  logAudit.setFileName(achFileName);
                  logAudit.setStatus("failed");
                  logAudit.setIsProcessed(0);
                  logAudit.setDescription("Customer Bank ID Not Matched");
                  getMyCurrentSession().save(logAudit);
              
                  inStream.close();
                  outStream.flush();
                  outStream.close();
                  outStream = null;
               
                 
                  afile.delete();
                  
                //  System.out.println("File is copied successful!");
                }
                catch (IOException e)
                {
                  e.printStackTrace();
                }
                files[z].delete();
              }
             
            }
            else
            {
              InputStream inStream = null;
              OutputStream outStream = null;
              try
              {
            	  IrFFileAudit logAudit1 = new IrFFileAudit();
                String moveFile = fileName;
                String moveTo = this.source + File.separator + "ERROR-DOC" + File.separator + this.achFileName;
                File afile = new File(moveFile);
                File bfile = new File(moveTo);
                
                inStream = new FileInputStream(afile);
                outStream = new FileOutputStream(bfile);
                
                byte[] buffer = new byte['?'];
                int length;
                while ((length = inStream.read(buffer)) > 0) {
                  outStream.write(buffer, 0, length);
                }
                
                
                currentDate=getCurrentDay();
                logAudit1.setAuditDate(currentDate);
                logAudit1.setFileType("ACH");
                logAudit1.setProcessedDateTime(date);
                logAudit1.setFileName(achFileName);
                logAudit1.setStatus("failed");
                logAudit1.setIsProcessed(0);
                logAudit1.setDescription("Bank RT number Not Matched");
                getMyCurrentSession().save(logAudit1);
             
                
                
                inStream.close();
                outStream.flush();
                outStream.close();
                outStream = null;
                
                afile.delete();
                
              //  System.out.println("File is copied successful!");
                errorFiles++;
                
                
              }
              catch (IOException e)
              {
                e.printStackTrace();
              }
            files[z].delete();
            }
          }
         }
      }
      else
      {
       // System.out.println("Create Bank Folder");
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
	return;
  }
  
  public void copyFolder(File src, File dest)
    throws IOException
  {
    if (src.isDirectory())
    {
      if (!dest.exists())
      {
        dest.mkdir();
       // System.out.println("Directory copied from " + src + "  to " + dest);
      }
      String[] files = src.list();
      if (files.length != 0) {
        for (String file : files)
        {
          File srcFile = new File(src, file);
          File destFile = new File(dest, file);
          
          copyFolder(srcFile, destFile);
        }
      } else {
      //  System.out.println("No files Exist");
      }
    }
    else
    {
    //  System.out.println("Reading file name : " + src);
      InputStream in = new FileInputStream(src);
      Object out = new FileOutputStream(dest);
      
      byte[] buffer = new byte['?'];
      int length;
      while ((length = in.read(buffer)) > 0) {
        ((OutputStream)out).write(buffer, 0, length);
      }
      in.close();
      ((OutputStream)out).close();
    //  System.out.println("File copied from " + src + " to " + dest);
      boolean filedeleted = src.delete();
      if (filedeleted) {
  //      System.out.println("Files deleted successfully");
      } else {
    //    System.out.println("Files not deleted ");
      }
    }
  }
  
  public void checkFile(String filename)
  {
    File achFile = new File(filename);
    if (!achFile.exists()) {
    //  System.out.println("File " + achFile.getPath() + " does not exist");
    }
    if (!achFile.isFile()) {
   //   System.out.println("File " + achFile.getPath() + " is not a file");
    }
    if (!achFile.canRead()) {
  //    System.out.println("File " + achFile.getPath() + " cannot be read");
    }
    BufferedReader achReader = null;
    try
    {
      achReader = new BufferedReader(new FileReader(achFile.getPath()));
    }
    catch (FileNotFoundException ex)
    {
   //   System.out.println("File " + achFile.getPath() + " could not be opened. Reason " + ex.getMessage());
    }
    boolean foundFileControl = false;
    int rowCount = 0;
    try
    {
      String record = achReader.readLine();
      if (!record.substring(0, 1).equals("1"))
      {
      //  System.out.println("File " + achFile.getPath() + " is not an ACH file.  First character must be a \"1\"");
        record = null;
        achReader.close();
        achReader = null;
        moveFile(filename);
      }
      else
      {
        loadAchData(this.src);
        achFile.delete();
      }
    }
    catch (IOException ex)
    {
   //   System.out.println("File " + achFile.getPath() + " could not be processed. Reason " + ex.getMessage());
    }
    try
    {
      achReader.close();
    }
    catch (Exception localException) {}
  }
  
  private void moveFile(String fileName)
  {
    InputStream inStream = null;
    OutputStream outStream = null;
    try
    {
      File afile = new File(fileName);
      this.todayDate = getMonthDay();
      File errorDir = new File(this.error + File.separator + this.todayDate);
      if (!errorDir.exists())
      {
        errorDir.mkdir();
     //   System.out.println("Directory created for todays date");
      }
      String time = getTime();
      
      File bfile = new File(this.error + File.separator + this.todayDate + File.separator + this.todayDate + "_" + time + "_" + afile.getName());
      
      inStream = new FileInputStream(afile);
      outStream = new FileOutputStream(bfile);
      
      byte[] buffer = new byte['?'];
      int length;
      while ((length = inStream.read(buffer)) > 0) {
        outStream.write(buffer, 0, length);
      }
      inStream.close();
      outStream.close();
      inStream = null;
      outStream = null;
     // System.out.println("File is copied successful!");
      if ((inStream == null) && (outStream == null)) {
        try
        {
          Files.deleteIfExists(Paths.get(fileName, new String[0]));
        }
        catch (NoSuchFileException e)
        {
      //    System.out.println("No such file/directory exists");
        }
        catch (DirectoryNotEmptyException e)
        {
     //     System.out.println("Directory is not empty.");
        }
        catch (IOException e)
        {
          e.printStackTrace();
     //     System.out.println("Invalid permissions.");
        }
      }
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
  }
  
  private void loadAchData(File filePath)
  {
    Vector<String> errorMessages = null;
    try
    {
      File[] files = filePath.listFiles();
      if (files.length != 0) {
        for (int i = 0; i < files.length; i++)
        {
          this.achFileName = files[i].getName();
          this.fileName = (filePath + File.separator + files[i].getName());
          setAchFile(new ACHFile(this.fileName));
          
          this.todayDate = getMonthDay();
          this.systemTime = getTime();
          
          String hql = "from IrSBankBranch bank where bank.rtNumber=?";
          Query query = getMyCurrentSession().createQuery(hql);
          query.setParameter(0, getAchFile().getFileHeader().getImmediateDestination());
          List<IrSBankBranch> bankList = query.list();
          if (bankList.size() > 0)
          {
            Vector<ACHBatch> achBatches = getAchFile().getBatches();
            
            String qry = "from IrSCustomers customer where customer.compIdentNo=?";
            Query cust = getMyCurrentSession().createQuery(qry);
            cust.setParameter(0, ((ACHBatch)achBatches.get(0)).getBatchHeader().getCompanyId());
            
            List<IrSCustomers> customerList = cust.list();
            if (customerList.size() > 0)
            {
              this.customerId = ((IrSCustomers)customerList.get(0)).getCustomerId().intValue();
              
              String bankIdFromdb = ((IrSBankBranch)bankList.get(0)).getBank().getIRBankCode();
              this.bankId = Long.valueOf(((IrSBankBranch)bankList.get(0)).getBank().getBankId());
              this.bankFolder = (((IrSBankBranch)bankList.get(0)).getBank().getBankFolder() + File.separator + this.todayDate);
              File createBank = new File(this.bankFolder);
              if (!createBank.exists())
              {
                createBank.mkdir();
                System.out.println("Directory created");
              }
              loadAchDataRecords();
            }
            else
            {
              moveFile(this.fileName);
            }
          }
          else
          {
            moveFile(this.fileName);
          }
          files[i].delete();
        }
      }
    }
    catch (Exception ex)
    {
    //  System.err.println(ex.getMessage());
      ex.printStackTrace();
    }
  }
  
  public ACHFile getAchFile()
  {
    return this.achFile;
  }
  
  public void setAchFile(ACHFile achFile)
  {
    this.achFile = achFile;
  }
  
  private void loadAchDataRecords()
  {
    try
    {
      IrFPFileHeaderRecord file = new IrFPFileHeaderRecord();
      IrFPBatchHeaderRecord batch = null;
      IrFPEntryDetailRecord transaction = null;
      IrFPCCDAddendaRecord payment = null;
      IrFPCCDAddendaRecord remittance = null;
      IrFPCCDAddendaRecord scandoc = null;
      IrFPCCDAddendaRecord car = null;
      
      IRSBank bank = new IRSBank();
      IrSCustomers customer = new IrSCustomers();
      IrMFileTypes fileType = new IrMFileTypes();
      
      CreatePaymentTIFF image = null;
      CreatePaymentTIFF batchImage = null;
      CreatePNG pngImage = null;
      CreateRemittanceTIFF remittanceTiff = null;
      RemittancePNG remittanceImage = null;
      CreateTIFF scanTiff = null;
      CreatePNG scanImage = null;
      
      bank.setBankId(this.bankId.longValue());
      file.setBank(bank);
      file.setProcessedDate(this.currentDate);
      file.setProcessedTime(this.currentTime);
      file.setAchFileName(this.achFileName);
      file.setIsProcessed(1);
      fileType.setFiletypeid(Long.valueOf(127L));
      file.setFileType(fileType);
      
      String batchNo = null;
      String batchDate = null;
      
      Vector<ACHBatch> achBatches = getAchFile().getBatches();
      for (int i = 0; i < achBatches.size(); i++)
      {
        batch = new IrFPBatchHeaderRecord();
        
        batch.setStandardEntryClass(((ACHBatch)achBatches.get(i)).getBatchHeader().getStandardEntryClassCode());
        Integer value = Integer.valueOf(Integer.parseInt(((ACHBatch)achBatches.get(i)).getBatchHeader().getCompanyDescriptiveDate()));
        SimpleDateFormat originalFormat = new SimpleDateFormat("yyMMdd");
        Date date = originalFormat.parse(value.toString());
        SimpleDateFormat newFormat = new SimpleDateFormat("MM/dd/yy");
        batchDate = newFormat.format(date);
        batch.setCompanyDescriptiveDate(batchDate);
        batch.setEffectiveEntryDate(((ACHBatch)achBatches.get(i)).getBatchHeader().getEffectiveEntryDate());
        customer.setCustomerId(Integer.valueOf(this.customerId));
        batch.setCustomer(customer);
        batch.setBatchNumber(((ACHBatch)achBatches.get(i)).getBatchHeader().getBatchNumber());
        batchNo = ((ACHBatch)achBatches.get(i)).getBatchHeader().getBatchNumber();
        
        Vector<ACHEntry> achEntries = ((ACHBatch)achBatches.get(i)).getEntryRecs();
        batch.setTransactionCount(Integer.toString(((ACHBatch)achBatches.get(i)).getEntryRecs().size()));
        int paymentCount = 0;
        int remittanceCount = 0;
        int scanDocCount = 0;
        int itemCount = 0;
        for (int j = 0; j < achEntries.size(); j++)
        {
          int payCount = 0;
          int remCount = 0;
          int scanCount = 0;
          int itmCount = 0;
          
          FileOutputStream fos = null;
          transaction = new IrFPEntryDetailRecord();
          String fileAmount = null;
          
          payment = new IrFPCCDAddendaRecord();
          remittance = new IrFPCCDAddendaRecord();
          scandoc = new IrFPCCDAddendaRecord();
          fileAmount = ((ACHEntry)achEntries.get(j)).getEntryDetail().getAmount();
          BigInteger amount = new BigInteger(fileAmount);
          BigDecimal two = new BigDecimal(amount, 2);
          
          String dbAmount = two.toString();
          
          String[] convert = dbAmount.split("\\.");
          
          int a = Integer.parseInt(convert[0]);
          String b = convert[1];
          
          int n = a;
          String amountInWords = null;
          NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
          String numberAsString = numberFormat.format(n);
          String commaSepaaratedNumber = null;
          if (b.equals("00"))
          {
            commaSepaaratedNumber = numberAsString + ".00";
            amountInWords = convert(n) + " Dollars and 0 / 100";
          }
          else
          {
            commaSepaaratedNumber = numberAsString + "." + b;
            amountInWords = convert(n) + " Dollars and " + b + " / 100";
          }
          payment.setAmount(commaSepaaratedNumber);
          payment.setEntryType("PAYM");
          
          String[] data = new String[8];
          data[0] = ((ACHEntry)achEntries.get(j)).getEntryDetail().getIndividualIdNbr();
          data[1] = batchDate;
          data[2] = ((ACHEntry)achEntries.get(j)).getEntryDetail().getIndividualName();
          data[3] = commaSepaaratedNumber;
          data[4] = amountInWords;
          data[5] = ((ACHEntry)achEntries.get(j)).getEntryDetail().getReceivingDfiId();
          data[6] = ((ACHEntry)achEntries.get(j)).getEntryDetail().getDfiAcctNbr();
          data[7] = ((ACHEntry)achEntries.get(j)).getEntryDetail().getTraceNumber();
          
          image = new CreatePaymentTIFF(data);
          try
          {
            String paymentFile = this.bankFolder + File.separator + batchNo + "P" + j + ".tiff";
            fos = new FileOutputStream(paymentFile);
            fos.write(image.getStream().toByteArray());
            fos.close();
            fos = null;
            payment.setFciName(paymentFile);
            payCount = 1;
            paymentCount++;
          }
          catch (IOException ioExp)
          {
            ioExp.printStackTrace();
            System.out.println("The error " + ioExp.toString());
          }
          pngImage = new CreatePNG(data);
          try
          {
            String paymentFile = this.bankFolder + File.separator + batchNo + "P" + j + ".png";
            fos = new FileOutputStream(paymentFile);
            fos.write(pngImage.getStream().toByteArray());
            fos.close();
            fos = null;
            payment.setFciPng(paymentFile);
          }
          catch (IOException ioExp)
          {
            ioExp.printStackTrace();
        //    System.out.println("The error " + ioExp.toString());
          }
          payment.setIrFPEntryDetailRecord(transaction);
          transaction.getIrFPCCDAddendaRecord().add(payment);
          payment.setIrFPFileHeaderRecord(file);
          file.getIrFPCCDAddendaRecord().add(payment);
          
          remittance.setAmount(commaSepaaratedNumber);
          remittance.setEntryType("REMT");
          
          String[] remi = new String[5];
          remi[0] = ((ACHEntry)achEntries.get(j)).getEntryDetail().getIndividualIdNbr();
          remi[1] = batchDate;
          remi[2] = ((ACHEntry)achEntries.get(j)).getEntryDetail().getIndividualName();
          remi[3] = numberAsString;
          
          remi[4] = ((ACHEntry)achEntries.get(j)).getEntryDetail().getTraceNumber();
          
          remittanceTiff = new CreateRemittanceTIFF(remi);
          try
          {
            String remittanceFile = this.bankFolder + File.separator + batchNo + "R" + j + ".tiff";
            fos = new FileOutputStream(remittanceFile);
            fos.write(remittanceTiff.getStream().toByteArray());
            fos.close();
            fos = null;
            remittance.setFciName(remittanceFile);
            remCount = 1;
            remittanceCount++;
          }
          catch (IOException ioExp)
          {
            ioExp.printStackTrace();
      //      System.out.println("The error " + ioExp.toString());
          }
          remittanceImage = new RemittancePNG(remi);
          try
          {
            String remittanceFile = this.bankFolder + File.separator + batchNo + "R" + j + ".png";
            fos = new FileOutputStream(remittanceFile);
            fos.write(remittanceImage.getStream().toByteArray());
            fos.close();
            fos = null;
            remittance.setFciPng(remittanceFile);
          }
          catch (IOException ioExp)
          {
            ioExp.printStackTrace();
        //    System.out.println("The error " + ioExp.toString());
          }
          remittance.setIrFPEntryDetailRecord(transaction);
          transaction.getIrFPCCDAddendaRecord().add(remittance);
          remittance.setIrFPFileHeaderRecord(file);
          file.getIrFPCCDAddendaRecord().add(remittance);
          
          Vector<ACHRecordAddenda> achAddendas = ((ACHEntry)achEntries.get(j)).getAddendaRecs();
          List<IrFPCCDAddendaRecord> list = new ArrayList();
          int scanDocSize = achAddendas.size();
          for (int k = 0; k < achAddendas.size(); k++)
          {
      //      System.out.println(achAddendas.get(k));
            car = new IrFPCCDAddendaRecord();
            car.setPaymentRelatedInformation(((ACHRecordAddenda)achAddendas.get(k)).getPaymentRelatedInfo());
            list.add(car);
          }
          if (list.size() > 0)
          {
            scanTiff = new CreateTIFF(list);
            try
            {
              String scanDOC = this.bankFolder + File.separator + batchNo + "S" + j + ".tiff";
              fos = new FileOutputStream(scanDOC);
              fos.write(scanTiff.getStream().toByteArray());
              fos.close();
              fos = null;
              scandoc.setFciName(scanDOC);
              scandoc.setEntryType("SCAN");
              scanCount = 1;
              scanDocCount++;
            }
            catch (IOException ioExp)
            {
              ioExp.printStackTrace();
         //     System.out.println("The error " + ioExp.toString());
            }
            scanImage = new CreatePNG(list);
            try
            {
              String scanDOC = this.bankFolder + File.separator + batchNo + "S" + j + ".png";
              fos = new FileOutputStream(scanDOC);
              fos.write(scanImage.getStream().toByteArray());
              fos.close();
              fos = null;
              scandoc.setFciPng(scanDOC);
              scandoc.setIrFPEntryDetailRecord(transaction);
              transaction.getIrFPCCDAddendaRecord().add(scandoc);
              scandoc.setIrFPFileHeaderRecord(file);
              file.getIrFPCCDAddendaRecord().add(scandoc);
            }
            catch (IOException ioExp)
            {
              ioExp.printStackTrace();
         //     System.out.println("The error " + ioExp.toString());
            }
          }
          if (payCount > 0) {
            transaction.setPaymentCount(Integer.toString(payCount));
          } else {
            transaction.setPaymentCount(Integer.toString(payCount));
          }
          if (remCount > 0) {
            transaction.setRemittanceCount(Integer.toString(remCount));
          } else {
            transaction.setRemittanceCount(Integer.toString(remCount));
          }
          if (scanCount > 0) {
            transaction.setScandocCount(Integer.toString(scanCount));
          } else {
            transaction.setScandocCount(Integer.toString(scanCount));
          }
          transaction.setItemCount(Integer.toString(payCount + remCount));
          transaction.setIrFPBatchHeaderRecord(batch);
          batch.getIrFPEntryDetailRecord().add(transaction);
          transaction.setIrFPFileHeaderRecord(file);
          file.getIrFPEntryDetailRecord().add(transaction);
          
          batch.setScandocCount(Integer.toString(scanDocCount));
        }
        batchImage = new CreatePaymentTIFF(batchNo);
        try
        {
          String paymentFile = this.bankFolder + File.separator + batchNo + "B" + i + ".tiff";
          FileOutputStream fos = new FileOutputStream(paymentFile);
          fos.write(batchImage.getStream().toByteArray());
          fos.close();
          fos = null;
          batch.setBatchImage(paymentFile);
        }
        catch (IOException ioExp)
        {
          ioExp.printStackTrace();
      //    System.out.println("The error " + ioExp.toString());
        }
        pngImage = new CreatePNG(batchNo);
        try
        {
          String paymentFile = this.bankFolder + File.separator + batchNo + "B" + i + ".png";
          FileOutputStream fos = new FileOutputStream(paymentFile);
          fos.write(pngImage.getStream().toByteArray());
          fos.close();
          fos = null;
          batch.setBatchPNG(paymentFile);
        }
        catch (IOException ioExp)
        {
          ioExp.printStackTrace();
      //    System.out.println("The error " + ioExp.toString());
        }
        batch.setItemCount(Integer.toString(paymentCount + remittanceCount));
        batch.setPaymentCount(Integer.toString(paymentCount));
        batch.setRemittanceCount(Integer.toString(remittanceCount));
        BigInteger amount = new BigInteger(((ACHBatch)achBatches.get(i)).getBatchControl().getTotCreditDollarAmt());
        BigDecimal two = new BigDecimal(amount, 2);
        String dbAmount = two.toString();
        String[] convert = dbAmount.split("\\.");
        
        int a = Integer.parseInt(convert[0]);
        String b = convert[1];
        
        int n = a;
        
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
        String numberAsString = numberFormat.format(n);
        String commaSepaaratedNumber = null;
        if (b.equals("00")) {
          commaSepaaratedNumber = numberAsString + ".00";
        } else {
          commaSepaaratedNumber = numberAsString + "." + b;
        }
        batch.setBatchAmount(commaSepaaratedNumber);
        batch.setIrFPFileHeaderRecord(file);
        file.getIrFPBatchHeaderRecord().add(batch);
      }
      InputStream inStream = null;
      OutputStream outStream = null;
      try
      {
        String moveFile = this.fileName;
        String moveTo = this.bankFolder + File.separator + this.achFileName;
        File afile = new File(moveFile);
        File bfile = new File(moveTo);
        
        inStream = new FileInputStream(afile);
        outStream = new FileOutputStream(bfile);
        
        byte[] buffer = new byte['?'];
        int length;
        while ((length = inStream.read(buffer)) > 0) {
          outStream.write(buffer, 0, length);
        }
        inStream.close();
        outStream.flush();
        outStream.close();
        outStream = null;
        
        afile.delete();
        
     //   System.out.println("File is copied successful!");
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
      Long flag = (Long)getMyCurrentSession().save(file);
     // System.out.println("data inserted into fhr" + flag);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  public final String[] units = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
  public final String[] tens = { "", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };
  
  public String convert(int n)
  {
    if (n < 0) {
      return "minus " + convert(-n);
    }
    if (n < 20) {
      return this.units[n];
    }
    if (n < 100) {
      return this.tens[(n / 10)] + (n % 10 != 0 ? " " : "") + this.units[(n % 10)];
    }
    if (n < 1000) {
      return this.units[(n / 100)] + " Hundred" + (n % 100 != 0 ? " " : "") + convert(n % 100);
    }
    if (n < 1000000) {
      return convert(n / 1000) + " Thousand" + (n % 1000 != 0 ? " " : "") + convert(n % 1000);
    }
    if (n < 1000000000) {
      return convert(n / 1000000) + " Million" + (n % 1000000 != 0 ? " " : "") + convert(n % 1000000);
    }
    return convert(n / 1000000000) + " Billion" + (n % 1000000000 != 0 ? " " : "") + convert(n % 1000000000);
  }
  
  private String getMonthDay()
  {
    String dayMonth = "";
    Calendar ca1 = Calendar.getInstance();
    int iDay = ca1.get(5);
    Date date = new Date();
    
    SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyy");
    dayMonth = sdf.format(date);
    
    return dayMonth;
  }
  
  private String getTime()
  {
    String dayMonth = "";
    Calendar ca1 = Calendar.getInstance();
    int iDay = ca1.get(5);
    Date date = new Date();
    
    SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
    dayMonth = sdf.format(date);
    
    return dayMonth;
  }
   private String getCurrentDay()
  {
    String dayMonth = "";
    Calendar ca1 = Calendar.getInstance();
    int iDay = ca1.get(5);
    Date date = new Date();
    
    SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
    dayMonth = sdf.format(date);
    
    return dayMonth;
  }
  
   private String getCurrentTime()
  {
    String dayMonth = "";
    Calendar ca1 = Calendar.getInstance();
    int iDay = ca1.get(5);
    Date date = new Date();
    
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    dayMonth = sdf.format(date);
    
    return dayMonth;
  }
  
  
  
  private void processed(String moveFrom, String moveTo)
  {
   // System.out.println("im in proccessed folder");
    
    InputStream inStream = null;
    OutputStream outStream = null;
    try
    {
      File afile = new File(moveFrom);
      File bfile = new File(moveTo);
      
      inStream = new FileInputStream(afile);
      outStream = new FileOutputStream(bfile);
      
      byte[] buffer = new byte['?'];
      int length;
      while ((length = inStream.read(buffer)) > 0) {
        outStream.write(buffer, 0, length);
      }
      inStream.close();
      outStream.close();
      inStream = null;
      outStream = null;
    ///  System.out.println(afile.getName() + "File is moved to processed folder successful!");
      if ((inStream == null) && (outStream == null)) {
        try
        {
          Files.deleteIfExists(Paths.get(this.fileName, new String[0]));
        }
        catch (NoSuchFileException e)
        {
   //       System.out.println("No such file/directory exists");
        }
        catch (DirectoryNotEmptyException e)
        {
   //       System.out.println("Directory is not empty.");
        }
        catch (IOException e)
        {
          e.printStackTrace();
     //     System.out.println("Invalid permissions.");
        }
      }
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
  }
}
