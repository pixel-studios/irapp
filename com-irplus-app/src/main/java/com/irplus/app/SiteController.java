package com.irplus.app;



import java.io.File;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.Gson;
import com.irplus.dto.BankInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.SiteInfo;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;
import com.irplus.util.ValidationHelper;
import com.irplus.core.client.sitesetup.SiteSetupCoreClient;
import com.irplus.dao.hibernate.entities.IRSSite;

@Controller
@Transactional
public class SiteController {
	
	private static final Logger LOGGER = Logger.getLogger(SiteController.class);

	Gson gson = new Gson();

	@Autowired
     SiteSetupCoreClient siteSetupCoreClient;
	
	@Autowired
	  private SessionFactory sessionFactory;
	  
	  public Session getMyCurrentSession()
	  {
	    Session session = null;
	    try
	    {
	      session = this.sessionFactory.getCurrentSession();
	    }
	    catch (HibernateException e)
	    {
	    	LOGGER.error("Exception raised DaoImpl :: At current session creation time ::" + e);
	    }
	    return session;
	  }
	  
	String rootFolderPath =null;
	
	String siteLogoPath=null;
	
	
	@RequestMapping(value="/site/getSiteInfo/{siteId}", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> getSiteInfo(@PathVariable String siteId)
	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		try{
		
				irPlusResponseDetails = this.siteSetupCoreClient.getSiteInfo(siteId);
			
		} catch (BusinessException e) {
			LOGGER.error("Exception in listBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	
	@RequestMapping(value= "/site/update", method=RequestMethod.POST, produces="application/json", consumes="application/json")
	@ResponseBody
	public ResponseEntity<String> updateSite(@RequestBody String requestInputData)
	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		SiteInfo siteInfo = (SiteInfo)this.gson.fromJson(requestInputData.toString(),SiteInfo.class);

		LOGGER.info("Inside Update Bank bankInfo::" + siteInfo);
		try
		{
			//boolean isValidReq = ValidationHelper.isUpdateBankReqValid(bankInfo);
			//if (isValidReq)
			//{
				irPlusResponseDetails = this.siteSetupCoreClient.updateSite(siteInfo);
			//}
			//else
			//{
				//irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				//irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			//}

		}
		catch (BusinessException e)
		{
			LOGGER.error("Exception in updateSite", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}

		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}


		}

	}
	
	@RequestMapping(value= "/site/folder/update", method=RequestMethod.POST, produces="application/json", consumes="application/json")
	@ResponseBody
	public ResponseEntity<String> updateFolder(@RequestBody String requestInputData)
	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		SiteInfo siteInfo = (SiteInfo)this.gson.fromJson(requestInputData.toString(),SiteInfo.class);

		LOGGER.info("Inside Update Bank bankInfo::" + siteInfo);
		try
		{
			//boolean isValidReq = ValidationHelper.isUpdateBankReqValid(bankInfo);
			//if (isValidReq)
			//{
				irPlusResponseDetails = this.siteSetupCoreClient.updateFolder(siteInfo);
			//}
			//else
			//{
				//irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				//irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			//}

		}
		catch (BusinessException e)
		{
			LOGGER.error("Exception in updateSite", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}

		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}


		}

	}
	@RequestMapping(value="/site/verifyLicense",method=RequestMethod.GET,produces="application/json")
	public ResponseEntity<String> showAllCustomers(){

		HttpHeaders responseheaders = new HttpHeaders(); 

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
			
			try {
				irPlusResponseDetails = siteSetupCoreClient.licenseValidity();
				
			} catch (BusinessException e) {

				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				LOGGER.error("Exception raised in CustomerController :: showAllCustomers",e);
			}
			
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.OK);
			}
			else {
					if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}	
			
	}
	
	  @RequestMapping(value = "/site/fileUpload", method = RequestMethod.POST, headers = "content-type=multipart/*", produces = {"application/json"})
	    public ResponseEntity<String> fileUpload(MultipartHttpServletRequest request,
				@RequestParam(value="siteLogo",required = false) MultipartFile siteLogo) throws Exception {
			
			HttpHeaders responseHeaders = new HttpHeaders();

			IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
					
			try
			{
				
				

				String hql = "from IRSSite site where site.isactive=?";
			      
			      Query query = getMyCurrentSession().createQuery(hql);
			      query.setParameter(0, Integer.valueOf(1));
			      
			      List<IRSSite> siteList = query.list();
			      if (siteList.size() > 0)
			      {
			        rootFolderPath = ((IRSSite)siteList.get(0)).getRootFolderPath();
				
			      }
				
				if(rootFolderPath!=null) {
					 File file = new File(rootFolderPath + File.separator + "Images");
			            if (!file.exists()) {
			              if (file.mkdir()) {
			                System.out.println("Directory is created!");
			              } else {
			                System.out.println("Failed to create directory!");
			              }
			            }
			            siteLogoPath=rootFolderPath+File.separator + "Images"+File.separator+"SiteLogo";
			            File file1 = new File(siteLogoPath);
			            if (!file1.exists()) {
			              if (file1.mkdir()) {
			                System.out.println("Directory is created!");
			              } else {
			                System.out.println("Failed to create directory!");
			              }
			            }
	 				}	
			
	    
			LOGGER.info("fileUploaded::" + siteLogo +" original File name : "+siteLogo.getOriginalFilename());
			
			String filePath = request.getRealPath("/");
			
			filePath =siteLogoPath+File.separator+siteLogo.getOriginalFilename();
			
			LOGGER.info("fileUploaded::" + siteLogo +" original File name : "+siteLogo.getOriginalFilename()+"filepath : "+filePath);
			
			File newFile = new File(filePath);
			
			siteLogo.transferTo(newFile);
			
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			
			irPlusResponseDetails.setLogoPath(siteLogo.getOriginalFilename());
			}
			
			catch (Exception e) {
				LOGGER.error("Exception in fileUpload", e);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
			{ 			  			// Successfully Request Creation

				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
			} 
			else 
			{
				if(!irPlusResponseDetails.isValidationSuccess())
				{
					return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
				}
				else
				{
					return IRPlusRestUtil.getGenericErrorResponse();
				}
			}
	    }
		


}
