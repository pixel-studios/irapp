package com.irplus.app;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.irplus.core.client.fieldtypes.IFieldTypesCoreClient;
import com.irplus.dto.FieldTypesInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;
import com.irplus.util.ValidationHelper;

@Controller
public class FieldTypesController {

	private static final Logger log = Logger.getLogger(FieldTypesController.class);

	Gson gson = new Gson();

	@Autowired
	IFieldTypesCoreClient iFieldTypesCoreClient;

	@RequestMapping(value = "/fieldtype/create", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<String> createFieldType(@RequestBody String fieldtypesData) {

		log.info("Inside of Fieldtypecontroller");

		HttpHeaders responseHeader = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		FieldTypesInfo fieldTypesInfo = gson.fromJson(fieldtypesData.toString(), FieldTypesInfo.class);

		boolean isValidReq = ValidationHelper.isCreateFieldReqValid(fieldTypesInfo);

		if (isValidReq) {

			try {
				irPlusResponseDetails = iFieldTypesCoreClient.createFieldTypes(fieldTypesInfo);
			} catch (BusinessException e) {
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				log.error("Exception raised in Fieldtypecontroller:: createFieldTypesInfo", e);
			}

		} else {

			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			log.error("Fieldtypecontroller :: Not a Valid Fieldtype Object ");
		}

		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeader, HttpStatus.OK);
		} else {
			if (!irPlusResponseDetails.isValidationSuccess()) {

				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseHeader,
						HttpStatus.CONFLICT);

			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}

	}

	@RequestMapping(value = "/fieldtype/update", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<String> updateFieldType(@RequestBody String fieldtypeData) {

		log.info("Inside of fieldtypecontroll");

		HttpHeaders responseHeader = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		FieldTypesInfo fieldTypesInfo = gson.fromJson(fieldtypeData.toString(), FieldTypesInfo.class);

		boolean isValidReq = ValidationHelper.isCreateFieldReqValid(fieldTypesInfo);
		if (isValidReq) {
			try {
				irPlusResponseDetails = iFieldTypesCoreClient.updateFieldTypes(fieldTypesInfo);
			} catch (BusinessException e) {

				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				log.error("Exception raised in Customer Controller :: getCustomer", e);
			}
		} else {
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
		}

		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseHeader, HttpStatus.OK);
		} else {
			if (!irPlusResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseHeader,HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}

	@RequestMapping(value = "/fieldtype/delete/{fieldId}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<String> deleteFieldType(@PathVariable String fieldId) {

		log.info("Inside of fieldtypecontroll");

		HttpHeaders responseheaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		boolean isValidCustomId = ValidationHelper.isValidReq(fieldId);

		if (isValidCustomId) {

			try {
				irPlusResponseDetails = iFieldTypesCoreClient.deleteFieldTypesById(fieldId);
			} catch (BusinessException e) {
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				log.error("Exception raised in fieldtypecontroll :: fieldtype", e);
			}
		} else {
			
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
		} else {
			if (!irPlusResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders,
						HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}

	@RequestMapping(value = "/fieldtype/showall", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<String> showallFieldType() {

		log.info("Inside of fieldtypecontroll");
		HttpHeaders responseheaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		try {
			irPlusResponseDetails = iFieldTypesCoreClient.showAllFieldTypes();

		} catch (BusinessException e) {
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception raised in Fieldtypecontroller:: showFieldTypesInfo", e);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {

			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
		} else {
			if (!irPlusResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders,
						HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	@RequestMapping(value = "/fieldtype/showone/{filetypeId}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<String> showOneFieldTypeById(@PathVariable String filetypeId) {

		log.info("Inside of fieldtypecontroll");
		HttpHeaders responseheaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		boolean isValidId = ValidationHelper.isValidReq(filetypeId);
			if(isValidId) {
					try {
						irPlusResponseDetails = iFieldTypesCoreClient.getFieldTypesById(filetypeId);
			
					} catch (BusinessException e) {
						irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
						log.error("Exception raised in Fieldtypecontroller:: showFieldTypesInfo", e);
					}
			} else {
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}
		
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
	
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			} else {
				if (!irPlusResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders,
							HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
			}
	}
		
}