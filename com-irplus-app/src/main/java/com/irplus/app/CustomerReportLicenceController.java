package com.irplus.app;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.irplus.core.client.customer_report_licence.ICustomerReportLicenceCoreClient;
import com.irplus.dto.CustomerReportLicenceInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;
import com.irplus.util.ValidationHelper;

@Controller
public class CustomerReportLicenceController {

	private static final Log log = LogFactory.getLog(CustomerReportLicenceController.class);

	Gson gson = new Gson();
	
	@Autowired 
	ICustomerReportLicenceCoreClient iCustomerReportingCoreClient;
	
	@RequestMapping(value="/customer_report_licence/create" , method= RequestMethod.POST , consumes="application/json" ,produces = "application/json") 
	
	public ResponseEntity<String> createCustomerReporting(@RequestBody String customerReportLicenceData){
		log.info("Inside of CustomerReportLicenceControll :: CustomerReportLicence");
		
		HttpHeaders responseHeader = new HttpHeaders();
		
		IRPlusResponseDetails responseDetails = new IRPlusResponseDetails();
 		 
		CustomerReportLicenceInfo customerRepotingBean= gson.fromJson(customerReportLicenceData.toString(), CustomerReportLicenceInfo.class);
		
		boolean isValidReq = ValidationHelper.isCustomerReportLicenceReqValid(customerRepotingBean);
		
			if (isValidReq) {
					try {					
						responseDetails = iCustomerReportingCoreClient.createCustomerReportLicence(customerRepotingBean);
		
					} catch (BusinessException e) {
						
						responseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
						responseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
						
						log.error("Exception raised in CustomerReportLicenceController :: createCustomerReportLicence",e);
					}
				} else {
			
					responseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
					responseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
						
					log.error("CustomerReportLicenceController :: Not a Valid CustomerReportLicence Object ");							
				}						
						
			if (responseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<>(gson.toJson(responseDetails),responseHeader ,HttpStatus.OK);
			} 
			else {
				if (!responseDetails.isValidationSuccess()) {
			
					return new ResponseEntity<String>(gson.toJson(responseDetails), responseHeader, HttpStatus.CONFLICT);
				
				} else {			
					return IRPlusRestUtil.getGenericErrorResponse();
				}
			}
	}

	
	//get
			@RequestMapping(value="//customer_report_licence/get/{cureliId}",method=RequestMethod.GET,produces="application/json")
				
			public ResponseEntity<String> showOneCustomerReport(@PathVariable String cureliId){
				
				log.info("customerReportId::" + cureliId);
				
				HttpHeaders responseHeader = new HttpHeaders();
				
				IRPlusResponseDetails responseDetails = new IRPlusResponseDetails();
		 			
				boolean isValidRoleId = ValidationHelper.isValidReq(cureliId);
				
				if (isValidRoleId) {							
						try{										
				
							responseDetails = iCustomerReportingCoreClient.getCustomerReportLicenceById(cureliId);
						} catch (BusinessException e) {
							
							responseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
							responseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);						
								
							log.error("Exception raised in customerReportingcontroller :: getCustomercontacts",e);
								
							}
						
				}
				else {
					responseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
					responseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
					}			
				if (responseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				
					return new ResponseEntity<String>(gson.toJson(responseDetails), responseHeader, HttpStatus.OK);
				} 
				else {
						if (!responseDetails.isValidationSuccess()) {
							return new ResponseEntity<String>(gson.toJson(responseDetails),responseHeader, HttpStatus.CONFLICT);
						} else {
							return IRPlusRestUtil.getGenericErrorResponse();
						}
				}
			}
	
			//delte
	@RequestMapping(value="/customer_report_licence/delete/{customerRlcId}",method=RequestMethod.GET,produces="application/json")
	public ResponseEntity<String> deleteCutomerContactsId(@PathVariable String customerRlcId){
			
			HttpHeaders responseheaders = new HttpHeaders(); 

			IRPlusResponseDetails responseDetails = new IRPlusResponseDetails();
		 	
			boolean isValidCustomId = ValidationHelper.isValidReq(customerRlcId);
			
			if (isValidCustomId) {
							
						try {
							responseDetails = iCustomerReportingCoreClient.deleteCustomerReportLicenceById(customerRlcId);
							} catch (BusinessException e) {

								responseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
								responseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
								log.error("Exception raised in CutomerReportingController :: deleteCustomerReporting",e);
						
							}
			} else {
				
				responseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				responseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}
					
			if (responseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(responseDetails), responseheaders, HttpStatus.OK);
			}
			else {
					if (!responseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(responseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}			
	}

		@RequestMapping(value="/customer_report_licence/showall",method=RequestMethod.GET,produces="application/json")
		public ResponseEntity<String> showAllCustomerContacts(){

			HttpHeaders responseheaders = new HttpHeaders(); 

			IRPlusResponseDetails responseDetails = new IRPlusResponseDetails();
				try {
					responseDetails = iCustomerReportingCoreClient.showAllCustomerReportLicences();
					
				} catch (BusinessException e) {

					responseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					responseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
					log.error("Exception raised in CustomerReportingsController :: showAllCustomersReporting",e);
				}
				
				if (responseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
					
					return new ResponseEntity<String>(gson.toJson(responseDetails), responseheaders, HttpStatus.OK);
				}
				else {
						if (!responseDetails.isValidationSuccess()) {
							return new ResponseEntity<String>(gson.toJson(responseDetails),responseheaders, HttpStatus.CONFLICT);
						} else {
							return IRPlusRestUtil.getGenericErrorResponse();
						}
				}		
		}
			
			
	@RequestMapping(value="/customer_report_licence/update",method = RequestMethod.POST,consumes = "application/json" ,produces ="application/json")
	public ResponseEntity<String> updateCustomerContacts(@RequestBody String customerreportData){
			
			
			HttpHeaders responseheaders = new HttpHeaders(); 

			IRPlusResponseDetails responseDetails = new IRPlusResponseDetails();
				
			CustomerReportLicenceInfo customerRepotingBean = gson.fromJson(customerreportData.toString(), CustomerReportLicenceInfo.class);
			
			boolean isValidReq = ValidationHelper.isCustomerReportLicenceReqValid(customerRepotingBean);
			
			log.info("customerRepotingBeanid " +customerRepotingBean.getReportLicensingId());
			
					if (isValidReq) {
								try {
								
									responseDetails = iCustomerReportingCoreClient.updateCustomerReportLicence(customerRepotingBean);
								
								} catch (BusinessException e) {
									
									responseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
									responseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
									log.error("Exception raised in Customer Controller :: getCustomer",e);
								}
					} else {
						responseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
						responseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
					}
					
					if (responseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
						return new ResponseEntity<String>(gson.toJson(responseDetails), responseheaders, HttpStatus.OK);
					}
					else {
							if (!responseDetails.isValidationSuccess()) {
								return new ResponseEntity<String>(gson.toJson(responseDetails),responseheaders, HttpStatus.CONFLICT);
							} else {
								return IRPlusRestUtil.getGenericErrorResponse();
							}
					}		
		}	

}
