package com.irplus.app;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.MultiPixelPackedSampleModel;
import java.awt.image.SampleModel;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;
import javax.media.jai.JAI;
import javax.media.jai.TiledImage;

import com.irplus.dao.hibernate.entities.IrFPCCDAddendaRecord;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.TIFFEncodeParam;

public class CreatePNG {
	private ByteArrayOutputStream stream = null;
	
	public	CreatePNG(){
	}

	public CreatePNG(String[] text){
		 int yIndex = 30;
		 int fontSize=20;
		 
		  BufferedImage bufferedImage = new BufferedImage(816,336,BufferedImage.TYPE_INT_RGB);
	        Graphics graphics = bufferedImage.getGraphics();
	        graphics.fillRect(0, 0, 816, 336);
	        graphics.setFont(new Font("Verdana",Font.PLAIN,12));
	        graphics.setColor(Color.BLACK);
	        //graphics.setColor(Color.BLACK);
	       // graphics.setFont(new Font("Arial Black", Font.BOLD, 5));
	        
	        
	        
	        graphics.drawString(text[0],150,20);
            graphics.drawString("Date : "+text[1],650,35);
            
            graphics.drawString("Pay to the",15,100);
            graphics.drawString("Order of",15,120);
            
            graphics.drawString(text[2],80,120);
            
            graphics.drawRect(650,130,140,40);
            graphics.drawString("$ "+text[3],660,155);
            graphics.drawString(text[4],80,155);
            graphics.setFont(new Font("Verdana",Font.BOLD,15));
            graphics.drawString(text[5],90,220);
            graphics.drawString("Memo : "+text[7],90,250);
            graphics.drawString(text[0],580,250);
            graphics.drawLine(550,260,750,260); //(x1,y1,x2,y2)
            graphics.setFont(new Font("MICR Encoding",Font.BOLD,25));
            graphics.drawString(text[5],100,300);
            graphics.drawString(text[6],250,300);
		            
		            stream = new ByteArrayOutputStream();    
		            
		        
		        
	        //graphics.drawString(key, 10, 25);
	        try {
				ImageIO.write(bufferedImage,"png",stream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	
	
	 public CreatePNG(List<IrFPCCDAddendaRecord> text)
	    {
		 int yIndex = 70;
		 int fontSize=25;
	         
	        BufferedImage bufferedImage = new BufferedImage(2480,3508,BufferedImage.TYPE_INT_RGB);
	        Graphics graphics = bufferedImage.getGraphics();
	        graphics.fillRect(0, 0, 2480,3508);
	        graphics.setFont(new Font("Verdana",Font.PLAIN,35));
	        graphics.setColor(Color.BLACK);
	        
	        
	       for(int i = 0; i < text.size(); i++)
	      {
	            graphics.drawString(text.get(i).getPaymentRelatedInformation(),150,yIndex);
	            
	            
	            yIndex+=fontSize+5;
	        }
	                
	       stream = new ByteArrayOutputStream();    
           
	        
	        
	        //graphics.drawString(key, 10, 25);
	        try {
				ImageIO.write(bufferedImage,"png",stream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       
	        
	    }
	 public CreatePNG(String text)
	    {
		 int yIndex = 30;
		 int fontSize=20;
	         
	        BufferedImage bufferedImage = new BufferedImage(816,336,BufferedImage.TYPE_INT_RGB);
	        Graphics graphics = bufferedImage.getGraphics();
	        graphics.fillRect(0, 0, 816,336);
	        graphics.setFont(new Font("Verdana",Font.PLAIN,20));
	        graphics.setColor(Color.BLACK);
	        

            graphics.drawString("BATCH HEADER  "+text,250,160);
         
	            
	            
	             
	       stream = new ByteArrayOutputStream();    
        
	        
	        
	        //graphics.drawString(key, 10, 25);
	        try {
				ImageIO.write(bufferedImage,"png",stream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       
	        
	    }
	 
	public ByteArrayOutputStream getStream() {
		return stream;
	}

	public void setStream(ByteArrayOutputStream stream) {
		this.stream = stream;
	}

}
