package com.irplus.app;

import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.irplus.core.client.dbinfo.IDBPropertiesCoreClient;
import com.irplus.dto.DBDTO;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;

@Controller
@RequestMapping(value="/db/")
public class DBPropertiesController {

	private static final Logger log = Logger.getLogger(DBPropertiesController.class);

	Gson gson = new Gson();

	@Autowired
	IDBPropertiesCoreClient idbPropertiesCoreClient;
	
	@RequestMapping(value="/create" , method = RequestMethod.POST , consumes = "application/json" ,produces ="application/json" )
	public ResponseEntity createDB(@RequestBody String dbData){
		
		log.info("Inside of DBPropertiesController : createDB");

		HttpHeaders responseheaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		DBDTO dbdto = gson.fromJson(dbData.toString(), DBDTO.class);
		
		log.debug("user name :"+dbdto.getUsername()+"user pwd :"+dbdto.getPassword());
		try {
		
			irPlusResponseDetails = idbPropertiesCoreClient.createDBProperties(dbdto);
		
		} catch (BusinessException e) {
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception raised in DBPropertiesController:: createFieldTypesInfo", e);
		}

		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
		} else {
				if (!irPlusResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
		}				
	}
	
		
	@RequestMapping(value="/showDBProperties" , method = RequestMethod.GET  ,produces ="application/json" )
	public ResponseEntity showDBProperties(){
		
		log.info("Inside of DBPropertiesController : showDBProperties");

		HttpHeaders responseheaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	//	DBDTO dbdto = gson.fromJson(dbData.toString(), DBDTO.class);
		
		try {		
			irPlusResponseDetails = idbPropertiesCoreClient.showDBProperties();
		
		} catch (BusinessException e) {
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception raised in DBPropertiesController:: showDBProperties", e);
		}

		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
		} else {
				if (!irPlusResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
		}			
	}
	
	
	@RequestMapping(value="/showDBProperties/{id}" , method = RequestMethod.GET  ,produces ="application/json" )
	public ResponseEntity showOneDBProperties(@PathVariable("id") Integer dbData){
		
		log.info("Inside of DBPropertiesController : showDBProperties");

		HttpHeaders responseheaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	//	DBDTO dbdto = gson.fromJson(dbData.toString(), DBDTO.class);
		List<String> errMsgs = new ArrayList<String>();
		log.debug("@Pathvaraiable "+dbData);
		errMsgs.add("@Pathvaraiable "+dbData);
		irPlusResponseDetails.setErrMsgs(errMsgs);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		
		/*
		 * try {		
			irPlusResponseDetails = idbPropertiesCoreClient.showDBProperties();				
			} catch (BusinessException e) {
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				log.error("Exception raised in DBPropertiesController:: showDBProperties", e);
			}*/

		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
		}else{
				if (!irPlusResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
		}
	}
	
	@RequestMapping(value="/update" , method = RequestMethod.POST , consumes = "application/json" ,produces ="application/json" )
	public ResponseEntity updateDB(@RequestBody String dbData){
		
		log.info("Inside of DBPropertiesController : updateDB");

		HttpHeaders responseheaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		DBDTO dbdto = gson.fromJson(dbData.toString(), DBDTO.class);
		
		log.debug("user name :"+dbdto.getUsername()+"user pwd :"+dbdto.getPassword());
		
		try{
		
			irPlusResponseDetails = idbPropertiesCoreClient.updateDBProperties(dbdto);
		
		}catch (BusinessException e) {
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception raised in DBPropertiesController:: updateDB" , e);
		}

		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
		} else {
				if (!irPlusResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
		}				
	}
}