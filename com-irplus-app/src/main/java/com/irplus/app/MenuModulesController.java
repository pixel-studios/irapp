package com.irplus.app;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.irplus.core.client.module_menu.IModuleMenusCoreClient;
import com.irplus.dto.BankInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.menu_modules_association.ModuleMenuBean;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;
import com.irplus.util.ModuleMenuValidationHelper;

@Controller
public class MenuModulesController {
	
	private static final Logger log= Logger.getLogger(MenuModulesController.class);

	Gson gson = new Gson();
	
	@Autowired
	IModuleMenusCoreClient moduleMenusCoreClient;
			
	@RequestMapping(value= "/menu_module/create" , method = RequestMethod.POST, produces= "application/json", consumes= "application/json")
	@ResponseBody
	public ResponseEntity<String> createMenuModule(@RequestBody String requestInputData) { 
	
		HttpHeaders responseHeaders = new HttpHeaders();
	
		IRPlusResponseDetails menuModuleResponseDetails=new IRPlusResponseDetails();

		ModuleMenuBean mmInfo = gson.fromJson(requestInputData.toString(), ModuleMenuBean.class);
		
		try {
		
			boolean isValidReq = ModuleMenuValidationHelper.isValidModuleMenuReq(mmInfo);
		
			if(mmInfo.getSortingorder()!=null) {
				menuModuleResponseDetails=this.moduleMenusCoreClient.createMenuModule(mmInfo);
			}
			else {
				
				menuModuleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				menuModuleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
			
		} catch (BusinessException e) {
			log.error("Exception in Create Menu", e);
			menuModuleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			menuModuleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		
		if(menuModuleResponseDetails.getStatusMsg().equalsIgnoreCase( IRPlusConstants.SUCCESS_MSG)) {
			// Successfully Request Creation
			return new ResponseEntity<>(gson.toJson(menuModuleResponseDetails), responseHeaders, HttpStatus.OK);
			
		}
		else
		{
			if(!menuModuleResponseDetails.isValidationSuccess()) 
			{
				return new ResponseEntity<>(gson.toJson(menuModuleResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	
	// delete modulemenu ok
	@RequestMapping(value="/menumodule/delete/{menumoduleid}",method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> deleteMenuModule(@PathVariable String menumoduleid)
	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails menuModuleResponseDetails=new IRPlusResponseDetails();
		
		log.info("menumoduleid::" + menumoduleid);
		try
		{
		boolean isValidReq=ModuleMenuValidationHelper.isValidModuleMenuId(menumoduleid);
			
		System.out.println("given ModuleMenu no is isValidReq :" + isValidReq);
			if (isValidReq)
			{
				menuModuleResponseDetails=this.moduleMenusCoreClient.deleteMenuModuleById(menumoduleid);
				menuModuleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
			else
			{
				menuModuleResponseDetails.setValidationSuccess(false);
				menuModuleResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				menuModuleResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}
		} catch (BusinessException e) {

			log.error("Exception in deleteMenu", e);
			menuModuleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			menuModuleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (menuModuleResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(menuModuleResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!menuModuleResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(menuModuleResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}

	//updated module menu ok
	@RequestMapping(value= "/modulemenu/update", method=RequestMethod.POST, produces="application/json", consumes="application/json")
	@ResponseBody
	public ResponseEntity<String> updateMenuModule(@RequestBody String requestInputData)
	{
		log.debug("Inside of ModuleMenuController : UpdateMenuModule()");
		
		HttpHeaders responseHeaders = new HttpHeaders();
		List<String> errMsgs = new ArrayList<String>();
		
		IRPlusResponseDetails menuModuleResponseDetails=new IRPlusResponseDetails();
	
		try {
			
			//BankInfo bankInfo = gson.fromJson(requestInputData.toString(), BankInfo.class);
			
			ModuleMenuBean moduleMenuInfo= gson.fromJson(requestInputData.toString(),ModuleMenuBean.class);	
			
			//	boolean isValidReq = ModuleMenuValidationHelper.isValidModuleMenuReq(moduleMenuInfo);	
				
			//	if (isValidReq) {				
					menuModuleResponseDetails=this.moduleMenusCoreClient.updateMenuModule(moduleMenuInfo);
				/*} 
				else {
					errMsgs.add("Validation Error : ");
					menuModuleResponseDetails.setErrMsgs(errMsgs);
					
					menuModuleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
					menuModuleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);			
				}*/
				
			} catch (BusinessException e) {
				
				log.error("Exception in update Menu",e);				
				menuModuleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				menuModuleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
		if (menuModuleResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {		
			
			return new ResponseEntity<>(gson.toJson(menuModuleResponseDetails), responseHeaders, HttpStatus.OK);
			
		} else {
			
			if (!menuModuleResponseDetails.isValidationSuccess()) {
				
				return new ResponseEntity<>(gson.toJson(menuModuleResponseDetails), responseHeaders,HttpStatus.CONFLICT);
			} 
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}		
	}
	
	//modul menu list ok
	@RequestMapping(value="/menumodule/list",method=RequestMethod.GET,produces="application/json")
	@ResponseBody
	public ResponseEntity<String> showMenuModuleList()
	{	
		HttpHeaders responseHeaders = new HttpHeaders();
		
		IRPlusResponseDetails menuModuleResponseDetails=new IRPlusResponseDetails();
		log.debug("Inside MenuModuleController :: menumodule show list");
	
		try {
									
			menuModuleResponseDetails=this.moduleMenusCoreClient.findAllMenuModule();
			if (!menuModuleResponseDetails.getModuleMenuBean().isEmpty()) {
				log.debug("Menu list show :: inside controller");
				menuModuleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				menuModuleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			} 
			else {
				menuModuleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				menuModuleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);			
			}
		
		} catch (BusinessException e) {
			
			log.error("Exception in update Menu",e);				
			menuModuleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			menuModuleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception Raised At MenuModuleController :: MenuModule List",e);
		}
		
		if (menuModuleResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			// successfully Response create			
			return new ResponseEntity<>(gson.toJson(menuModuleResponseDetails), responseHeaders, HttpStatus.OK);
			
		} else {
			
			if (!menuModuleResponseDetails.isValidationSuccess()) {
				
				return new ResponseEntity<>(gson.toJson(menuModuleResponseDetails), responseHeaders,HttpStatus.CONFLICT);
			} 
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}

		}
	}
	
	//menu module show menuid ok
	
	@RequestMapping(value="/menumodule/show/{menumoduleid}", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> showOneMenu(@PathVariable String menumoduleid){
		
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails menuModuleResponseDetails=new IRPlusResponseDetails();
		
		log.debug("Inside MenuModuleController :: menumodule show list");
		try
		{
		boolean isValidReq = ModuleMenuValidationHelper.isValidModuleMenuId(menumoduleid);
			if (isValidReq)
			{				
				menuModuleResponseDetails=this.moduleMenusCoreClient.getMenuModuleById(menumoduleid);
				menuModuleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
			else
			{
				menuModuleResponseDetails.setValidationSuccess(false);
				menuModuleResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				menuModuleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
		}
		catch (BusinessException e) {
			
			log.error("Exception in deleteMenu", e);
			menuModuleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			menuModuleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception Raised At MenuModuleController :: MenuModule showOneMenu",e);
		}		
		if (menuModuleResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation
			
			return new ResponseEntity<>(gson.toJson(menuModuleResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!menuModuleResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(menuModuleResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	
/*	 list out the menuid same menumoduleid*/
	
	@RequestMapping(value="/menumodule/getModuleMenu_based_menuid/{menuid}", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> getMenuModule_bymenuid(@PathVariable String menuid){
		
		HttpHeaders responseHeaders = new HttpHeaders();
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		IRPlusResponseDetails menuModuleResponseDetails = new IRPlusResponseDetails();
		
		List<ModuleMenuBean> mmlist = new ArrayList<ModuleMenuBean>();
		
		List<String> errMsg = new ArrayList<String>();
		
		log.debug("Inside MenuModuleController :: menumodule show list");
		try
		{
		boolean isValidReq = ModuleMenuValidationHelper.isValidModuleMenuId(menuid);
			if (isValidReq)
			{				
				menuModuleResponseDetails=this.moduleMenusCoreClient.findAllMenuModule();
				List<ModuleMenuBean> listMMbean = menuModuleResponseDetails.getModuleMenuBean();
				
				for (ModuleMenuBean moduleMenuBean : listMMbean) {
							if(Integer.parseInt(menuid)==moduleMenuBean.getMenuid()){
								mmlist.add(moduleMenuBean);			
							}
				 }
				if(mmlist.isEmpty()){
					irPlusResponseDetails.setValidationSuccess(false);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				}else{
					irPlusResponseDetails.setModuleMenuBean(mmlist);
					irPlusResponseDetails.setValidationSuccess(true);
					errMsg.add("Success : Module List :"+mmlist.size());
					irPlusResponseDetails.setErrMsgs(errMsg);
					irPlusResponseDetails.setRecordsTotal(mmlist.size());
					irPlusResponseDetails.setRecordsFiltered(mmlist.size());
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				}

			}
			else
			{ 
				errMsg.add("Not Matched id : Modules list Not available");
				irPlusResponseDetails.setErrMsgs(errMsg);
				menuModuleResponseDetails.setValidationSuccess(false);
				menuModuleResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				menuModuleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
		}
		catch (BusinessException e) {
			
			log.error("Exception in getMenuModule_bymenuid");
			menuModuleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			menuModuleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception Raised At MenuModuleController :: MenuModule getMenuModule_bymenuid",e);
		}	
		
		// here write jump code
		
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation
			
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	

	/*new ui code services added*/
	
/* Update Status if match the menu id*/
		@RequestMapping(value="/menumodule/update/status/{menumoduleid}/{status}", method=RequestMethod.GET, produces="application/json")
		@ResponseBody
		public ResponseEntity<String> UpdateStatus(@PathVariable String menumoduleid , @PathVariable String status){
			
			HttpHeaders responseHeaders = new HttpHeaders();

			IRPlusResponseDetails menuModuleResponseDetails=new IRPlusResponseDetails();
			
			StatusIdInfo statId = new StatusIdInfo();
			
			statId.setId(Integer.parseInt(menumoduleid)); 
			statId.setStatus(Integer.parseInt(status));
			
			log.debug("Inside MenuModuleController :: menumodule show list");
			
			try
			{
			boolean isValidReq = ModuleMenuValidationHelper.isValidModuleMenuId(menumoduleid);
				if (isValidReq)
				{				
					menuModuleResponseDetails=this.moduleMenusCoreClient.UpdateStatus(statId);
					menuModuleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				}
				else
				{
					menuModuleResponseDetails.setValidationSuccess(false);
					menuModuleResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
					menuModuleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				}
			}
			catch (BusinessException e) {
				
				log.error("Exception in UpdateStatus");
				menuModuleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				menuModuleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				log.error("Exception Raised At MenuModuleController :: MenuModule UpdateStatus",e);
			}		
			if (menuModuleResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
			{ 			  			// Successfully Request Creation
				
				return new ResponseEntity<>(gson.toJson(menuModuleResponseDetails), responseHeaders, HttpStatus.OK);
			} 
			else 
			{
				if(!menuModuleResponseDetails.isValidationSuccess())
				{
					return new ResponseEntity<>(gson.toJson(menuModuleResponseDetails), responseHeaders, HttpStatus.CONFLICT);
				}
				else
				{
					return IRPlusRestUtil.getGenericErrorResponse();
				}
			}
		}
	
	/*Deleting menumodule is here updating status =2 only no permanent delete*/
		
		@RequestMapping(value="/menumodule/Delete/update/status/{menumoduleid}/{status}", method=RequestMethod.GET, produces="application/json")
		@ResponseBody
		public ResponseEntity<String> DeleteUpdateStatus(@PathVariable String menumoduleid , @PathVariable String status){
			
			HttpHeaders responseHeaders = new HttpHeaders();

			IRPlusResponseDetails menuModuleResponseDetails=new IRPlusResponseDetails();
			
			StatusIdInfo statId = new StatusIdInfo();
			
			statId.setId(Integer.parseInt(menumoduleid)); 
			statId.setStatus(Integer.parseInt(status));
			
			log.debug("Inside MenuModuleController :: menumodule show list");
			
			try
			{
			boolean isValidReq = ModuleMenuValidationHelper.isValidModuleMenuId(menumoduleid);
				if (isValidReq)
				{				
					menuModuleResponseDetails=this.moduleMenusCoreClient.DeleteUpdateStatus(statId);
					menuModuleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				}
				else
				{
					menuModuleResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
					menuModuleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				}
			}
			catch (BusinessException e) {
				
				log.error("Exception in DeleteUpdateStatus");
				menuModuleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				menuModuleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				log.error("Exception Raised At MenuModuleController :: MenuModule DeleteUpdateStatus",e);
			}		
			if (menuModuleResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
			{ 			  			// Successfully Request Creation
				
				return new ResponseEntity<>(gson.toJson(menuModuleResponseDetails), responseHeaders, HttpStatus.OK);
			} 
			else 
			{
				if(!menuModuleResponseDetails.isValidationSuccess())
				{
					return new ResponseEntity<>(gson.toJson(menuModuleResponseDetails), responseHeaders, HttpStatus.CONFLICT);
				}
				else
				{
					return IRPlusRestUtil.getGenericErrorResponse();
				}
			}
		}

	/*	List outing only the Status is 0 and 1*/
		
		@RequestMapping(value="/menumodule/status/list",method=RequestMethod.GET,produces="application/json")
		@ResponseBody
		public ResponseEntity<String>  findAllByStatus() 
		{	
			HttpHeaders responseHeaders = new HttpHeaders();
			
			IRPlusResponseDetails menuModuleResponseDetails=new IRPlusResponseDetails();
			log.debug("Inside MenuModuleController :: menumodule show list");
		
			try {										
				menuModuleResponseDetails=this.moduleMenusCoreClient.findAllByStatus();
				menuModuleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			} catch (BusinessException e) {
							
				menuModuleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				menuModuleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				log.error("Exception in findAllByStatus : ",e);	
			}
			
			if (menuModuleResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				// successfully Response create			
				return new ResponseEntity<>(gson.toJson(menuModuleResponseDetails), responseHeaders, HttpStatus.OK);
				
			} else {
				
				if (!menuModuleResponseDetails.isValidationSuccess()) {
					
					return new ResponseEntity<>(gson.toJson(menuModuleResponseDetails), responseHeaders,HttpStatus.CONFLICT);
				} 
				else
				{
					return IRPlusRestUtil.getGenericErrorResponse();
				}
			}
		}

		@RequestMapping(value= "/menu_module/create/nodup" , method = RequestMethod.POST, produces= "application/json", consumes= "application/json")
		@ResponseBody
		public ResponseEntity<String> createMenuModuleNoDuplicate(@RequestBody String requestInputData) { 
		
			HttpHeaders responseHeaders = new HttpHeaders();
		
			IRPlusResponseDetails menuModuleResponseDetails=new IRPlusResponseDetails();

			ModuleMenuBean mmInfo = gson.fromJson(requestInputData.toString(), ModuleMenuBean.class);
			
			try {
			
				boolean isValidReq = ModuleMenuValidationHelper.isValidModuleMenuReq(mmInfo);
			
				if(mmInfo.getSortingorder()!=null) {
					menuModuleResponseDetails=this.moduleMenusCoreClient.createMenuModuleNoDuplicate(mmInfo);
					menuModuleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				}
				else {
					menuModuleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
					menuModuleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				}
				
			} catch (BusinessException e) {
				
				menuModuleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				menuModuleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				log.error("Exception in createMenuModuleNoDuplicate", e);
			}
			
			if(menuModuleResponseDetails.getStatusMsg().equalsIgnoreCase( IRPlusConstants.SUCCESS_MSG)) {
				// Successfully Request Creation
				return new ResponseEntity<>(gson.toJson(menuModuleResponseDetails), responseHeaders, HttpStatus.OK);
				
			}
			else
			{
				if(!menuModuleResponseDetails.isValidationSuccess()) 
				{
					return new ResponseEntity<>(gson.toJson(menuModuleResponseDetails), responseHeaders, HttpStatus.CONFLICT);
				}
				else
				{
					return IRPlusRestUtil.getGenericErrorResponse();
				}
			}
		}		
}