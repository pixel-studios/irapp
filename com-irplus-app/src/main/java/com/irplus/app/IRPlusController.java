package com.irplus.app;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.irplus.core.client.IRPlusCoreClient;
import com.irplus.core.client.bankmgmt.IBankMgmtCoreClient;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;

@Controller
public class IRPlusController {
    private static final Logger LOGGER = Logger.getLogger(IRPlusController.class);
    
    @Autowired
	IRPlusCoreClient irPlusCoreClient;
    
    Gson gson = new Gson();

    @RequestMapping(value="/master/getData/{siteId}", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> getMasterData(@PathVariable String siteId)
	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		try
		{
			irPlusResponseDetails = irPlusCoreClient.getMasterData(siteId);
			irPlusResponseDetails.setValidationSuccess(false);
			

		} catch (BusinessException e) {
			LOGGER.error("Exception in listBank", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!irPlusResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
}
