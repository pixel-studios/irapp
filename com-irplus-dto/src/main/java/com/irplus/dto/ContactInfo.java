package com.irplus.dto;

public class ContactInfo {
	 private static final long serialVersionUID = -3176074231815769436L;
	 //private Long contactId;
	 private String contactId;
	 private Long branchId;
	 private String firstName;
	 private String lastName;
	 private String contactRole;
	 private String contactNo;
	 private String emailId;
	 private String address1;
	 private String address2;
	 private Character isdefault;
	
	private String country;
	 private String state;
	 private String city;
	 private String zipcode;
	 private int isActive;
	 private String website;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public Character getIsdefault() {
		return isdefault;
	}
	public void setIsdefault(Character isdefault) {
		this.isdefault = isdefault;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public int getIsActive() {
		return isActive;
	}
	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}
	public String getContactRole() {
		return contactRole;
	}
	public void setContactRole(String contactRole) {
		this.contactRole = contactRole;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	public String getContactId() {
		return contactId;
	}
	public void setContactId(String contactId) {
		this.contactId = contactId;
	}
	public Long getBranchId() {
		return branchId;
	}
	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ContactInfo [contactId=").append(contactId).append(", branchId=").append(branchId)
				.append(", firstName=").append(firstName).append(", lastName=").append(lastName)
				.append(", contactRole=").append(contactRole).append(", contactNo=").append(contactNo)
				.append(", emailId=").append(emailId).append(", address1=").append(address1).append(", address2=")
				.append(address2).append(", isdefault=").append(isdefault).append(", country=").append(country)
				.append(", state=").append(state).append(", city=").append(city).append(", zipcode=").append(zipcode)
				.append(", isActive=").append(isActive).append(", website=").append(website).append("]");
		return builder.toString();
	}
	
	 

	   

}
