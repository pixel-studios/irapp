package com.irplus.dto;

import java.util.Date;

public class UserBankBean {
	
	private int userBankId;
	private Long branchId;
	private byte isDefault;
	private int isactive;
	private int userId;
	private int adminId;
	private Date createddate;
	private Date modifieddate;
	
	public UserBankBean(){
		
	}
	public int getUserBankId() {
		return userBankId;
	}
	public void setUserBankId(int userBankId) {
		this.userBankId = userBankId;
	}
	public Long getBranchId() {
		return branchId;
	}
	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}
	public byte getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(byte isDefault) {
		this.isDefault = isDefault;
	}
	public int getIsactive() {
		return isactive;
	}
	public void setIsactive(int isactive) {
		this.isactive = isactive;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getAdminId() {
		return adminId;
	}
	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}
	public Date getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}
	public Date getModifieddate() {
		return modifieddate;
	}
	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserBankBean [userBankId=").append(userBankId).append(", branchId=").append(branchId)
			    .append(", isDefault=").append(isDefault).append(", userId=").append(userId)
				.append(", adminId=").append(adminId).append(", createddate=").append(createddate)
				.append(", modifieddate=").append(modifieddate).append(", isactive=").append(isactive).append("]");
		return builder.toString();
	}

}
