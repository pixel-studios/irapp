package com.irplus.dto;

public class FileTypeInfo {
	
	    private static final long serialVersionUID = -3176074231815769436L;

	    private Long filetypeid;
		private String filetypename;
	//	private int licenseId;
		private int isactive;
		private Long licenseId;
		 private String MonthTransactions;
		 private String customername;
			private String bankname;
			private String fromDate;
			private String toDate;
			public int getUserId() {
				return userId;
			}
			public void setUserId(int userId) {
				this.userId = userId;
			}
			private int userId;
			
			public String getBankCustCode() {
				return bankCustCode;
			}
			public void setBankCustCode(String bankCustCode) {
				this.bankCustCode = bankCustCode;
			}
			private String bankCustCode;
			private Integer customerId;	
		 	public String getCustomername() {
			return customername;
		}
		public void setCustomername(String customername) {
			this.customername = customername;
		}
		public String getBankname() {
			return bankname;
		}
		public void setBankname(String bankname) {
			this.bankname = bankname;
		}
		public String getFromDate() {
			return fromDate;
		}
		public void setFromDate(String fromDate) {
			this.fromDate = fromDate;
		}
		public String getToDate() {
			return toDate;
		}
		public void setToDate(String toDate) {
			this.toDate = toDate;
		}
		public Integer getCustomerId() {
			return customerId;
		}
		public void setCustomerId(Integer customerId) {
			this.customerId = customerId;
		}
			 
		 
		 
		 
		 public int getMonthlyTrans() {
			return MonthlyTrans;
		}
		public void setMonthlyTrans(int monthlyTrans) {
			MonthlyTrans = monthlyTrans;
		}
		private int MonthlyTrans;
		 public int getMonthvalue() {
			return Monthvalue;
		}
		public void setMonthvalue(int monthvalue) {
			Monthvalue = monthvalue;
		}
		private int Monthvalue;
	    public String getMonthTransactions() {
			return MonthTransactions;
		}
		public void setMonthTransactions(String monthTransactions) {
			MonthTransactions = monthTransactions;
		}
		private String createddate;
	    private String modifieddate;
	    	    
		public Long getFiletypeid() {
			return filetypeid;
		}
		public void setFiletypeid(Long filetypeid) {
			this.filetypeid = filetypeid;
		}
		public String getFiletypename() {
			return filetypename;
		}
		public void setFiletypename(String filetypename) {
			this.filetypename = filetypename;
		}
		
/*		public int getLicenseId() {
			return licenseId;
		}
		public void setLicenseId(int licenseId) {
			this.licenseId = licenseId;
		}*/
		
		
		public int getIsactive() {
			return isactive;
		}
		public Long getLicenseId() {
			return licenseId;
		}
		public void setLicenseId(Long licenseId) {
			this.licenseId = licenseId;
		}
		public void setIsactive(int isactive) {
			this.isactive = isactive;
		}
		public String getCreateddate() {
			return createddate;
		}
		public void setCreateddate(String createddate) {
			this.createddate = createddate;
		}
		public String getModifieddate() {
			return modifieddate;
		}
		public void setModifieddate(String modifieddate) {
			this.modifieddate = modifieddate;
		}
		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("FileTypeInfo [filetypeid=").append(filetypeid).append(", filetypename=")
					.append(filetypename).append(", licenseId=").append(licenseId).append(", isactive=")
					.append(isactive).append(", createddate=").append(createddate).append(", modifieddate=")
					.append(modifieddate).append("]");
			return builder.toString();
		}
		
	    
	   
		
		
	}



