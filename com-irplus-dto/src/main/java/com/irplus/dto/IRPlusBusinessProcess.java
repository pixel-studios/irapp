package com.irplus.dto;

import java.sql.Timestamp;

public class IRPlusBusinessProcess {

	private Long businessProcessId;
	//private BankBranch bankBranch;
	private String processName;
	private Long licenseId;
	private Character hasVisibility;
	private Character isparent;
	private int isactive;
	//private IrMUsers user;
	private Timestamp createddate;
    private Timestamp modifieddate;
    public IRPlusBusinessProcess() {
		
	}
	public Long getBusinessProcessId() {
		return businessProcessId;
	}
	public void setBusinessProcessId(Long businessProcessId) {
		this.businessProcessId = businessProcessId;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	
	
	public Long getLicenseId() {
		return licenseId;
	}
	public void setLicenseId(Long licenseId) {
		this.licenseId = licenseId;
	}
	public Character getHasVisibility() {
		return hasVisibility;
	}
	public void setHasVisibility(Character hasVisibility) {
		this.hasVisibility = hasVisibility;
	}
	public Character getIsparent() {
		return isparent;
	}
	public void setIsparent(Character isparent) {
		this.isparent = isparent;
	}
	public int getIsactive() {
		return isactive;
	}
	public void setIsactive(int isactive) {
		this.isactive = isactive;
	}
	/*public IrMUsers getUser() {
		return user;
	}
	public void setUser(IrMUsers user) {
		this.user = user;
	}*/
	public Timestamp getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Timestamp createddate) {
		this.createddate = createddate;
	}
	public Timestamp getModifieddate() {
		return modifieddate;
	}
	public void setModifieddate(Timestamp modifieddate) {
		this.modifieddate = modifieddate;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IrSBusinessProcess [businessProcessId=").append(businessProcessId).append(", processName=")
				.append(processName).append(", licenseId=").append(licenseId).append(", hasVisibility=")
				.append(hasVisibility).append(", isparent=").append(isparent).append(", isactive=").append(isactive)
				.append(", createddate=").append(createddate).append(", modifieddate=")
				.append(modifieddate).append("]");
		return builder.toString();
	}
	
	
}
