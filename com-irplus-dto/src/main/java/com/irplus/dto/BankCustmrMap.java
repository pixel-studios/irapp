package com.irplus.dto;

public class BankCustmrMap {

	private Long bankId;
	private String bankName;
	private Integer customerId;
	private Integer customrGrpId;
	private Integer customrGrpDtlsId;
	private Integer userId;
	private Integer adminId;
	private String customerGroupName;
	private Integer isactive;
	
	 
	
	public Long getBankId() {
		return bankId;
	}	
	public void setBankId(Long bankId) {
		this.bankId = bankId;
	}	
	public Integer getCustomerId() {
		return customerId;
	}	
	public void setCustomerId(Integer customerId){
		this.customerId = customerId;
	}
	public Integer getCustomrGrpId() {
		return customrGrpId;
	}
	public void setCustomrGrpId(Integer customrGrpId) {
		this.customrGrpId = customrGrpId;
	}
	public Integer getCustomrGrpDtlsId() {
		return customrGrpDtlsId;
	}
	public String getCustomerGroupName() {
		return customerGroupName;
	}
	public void setCustomerGroupName(String customerGroupName) {
		this.customerGroupName = customerGroupName;
	}
	public Integer getIsactive() {
		return isactive;
	}
	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}
	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public void setCustomrGrpDtlsId(Integer customrGrpDtlsId) {
		this.customrGrpDtlsId = customrGrpDtlsId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getAdminId() {
		return adminId;
	}
	public void setAdminId(Integer adminId) {
		this.adminId = adminId;
	}
	
}