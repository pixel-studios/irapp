package com.irplus.dto.menu;

import java.util.List;

public class MenuResponseDetails {

	private boolean isValidationSuccess;
    private int statusCode;
    private String statusMsg;
    private List<String> errMsgs;
    private List<AddMenu> menus;
    
    public boolean isValidationSuccess() {
		return isValidationSuccess;
	}
	public void setValidationSuccess(boolean isValidationSuccess) {
		this.isValidationSuccess = isValidationSuccess;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
	public List<String> getErrMsgs() {
		return errMsgs;
	}
	public void setErrMsgs(List<String> errMsgs) {
		this.errMsgs = errMsgs;
	}
	
	public List<AddMenu> getMenus() {
		return menus;
	}
	public void setMenus(List<AddMenu> menus) {
		this.menus = menus;
	}
	@Override
	public String toString() {
		return "MenuResponseDetails [isValidationSuccess=" + isValidationSuccess + ", statusCode=" + statusCode
				+ ", statusMsg=" + statusMsg + ", errMsgs=" + errMsgs + ", menus=" + menus + "]";
	}
	
	
    
/*	@Override
	public String toString() {
		return "IRPlusResponseDetails [isValidationSuccess=" + isValidationSuccess + ", statusCode=" + statusCode
				+ ", statusMsg=" + statusMsg + ", errMsgs=" + errMsgs;
	}
	public List<BankInfo> getBanks() {
		return banks;
	}
	public void setBanks(List<BankInfo> banks) {
		this.banks = banks;
	}
      
*/

	
	
	
}