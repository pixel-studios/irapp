package com.irplus.dto.menu;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.irplus.dto.module.ModuleBean;

public class MenuInfo {
	
private Integer menuid;

private String menuName;
private String description;
private Integer status;

private String menuicon;
private String modulepath;
private Integer sortingorder;
private List<ModuleBean> subMenu = new ArrayList<ModuleBean>();

public Integer getMenuid() {
	return menuid;
}

public void setMenuid(Integer menuid) {
	this.menuid = menuid;
}

public String getMenuName() {
	return menuName;
}

public void setMenuName(String menuName) {
	this.menuName = menuName;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

public Integer getStatus() {
	return status;
}

public void setStatus(Integer status) {
	this.status = status;
}

public String getMenuicon() {
	return menuicon;
}

public void setMenuicon(String menuicon) {
	this.menuicon = menuicon;
}

public String getModulepath() {
	return modulepath;
}

public void setModulepath(String modulepath) {
	this.modulepath = modulepath;
}

public Integer getSortingorder() {
	return sortingorder;
}

public void setSortingorder(Integer sortingorder) {
	this.sortingorder = sortingorder;
}

public List<ModuleBean> getSubMenu() {
	return subMenu;
}

public void setSubMenu(List<ModuleBean> subMenu) {
	this.subMenu = subMenu;
}

@Override
public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append("MenuInfo [menuid=").append(menuid).append(", menuName=").append(menuName).append(", description=")
			.append(description).append(", status=").append(status).append(", menuicon=").append(menuicon)
			.append(", modulepath=").append(modulepath).append(", sortingorder=").append(sortingorder)
			.append(", subMenu=").append(subMenu).append("]");
	return builder.toString();
}




}
