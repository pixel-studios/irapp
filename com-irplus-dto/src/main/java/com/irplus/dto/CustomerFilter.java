package com.irplus.dto;

public class CustomerFilter {

	private String contactId;
	
	private Long branchId;
	
	private String firstName;
	private String lastName;

	private Long filetypeid;
	private String filetypename;
	
	private String irCustomerCode;
	private String companyName;
	private String bankCustomerCode;
	
	private Long businessProcessId;
	private String processName;
	private Integer isactive;
	

	public Long getFiletypeid() {
		return filetypeid;
	}
	public void setFiletypeid(Long filetypeid) {
		this.filetypeid = filetypeid;
	}
	public String getIrCustomerCode() {
		return irCustomerCode;
	}
	public void setIrCustomerCode(String irCustomerCode) {
		this.irCustomerCode = irCustomerCode;
	}
	public String getCompanyName() {
		return companyName;
	}
	public String getBankCustomerCode() {
		return bankCustomerCode;
	}
	public void setBankCustomerCode(String bankCustomerCode) {
		this.bankCustomerCode = bankCustomerCode;
	}
	public Integer getIsactive() {
		return isactive;
	}
	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Long getBusinessProcessId() {
		return businessProcessId;
	}
	public void setBusinessProcessId(Long businessProcessId) {
		this.businessProcessId = businessProcessId;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getFiletypename() {
		return filetypename;
	}
	public void setFiletypename(String filetypename) {
		this.filetypename = filetypename;
	}
	public String getContactId() {
		return contactId;
	}
	public void setContactId(String contactId) {
		this.contactId = contactId;
	}
	public Long getBranchId() {
		return branchId;
	}
	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}