package com.irplus.dto.users;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import com.irplus.dto.BankBranchInfo;
import com.irplus.dto.BankInfo;
import com.irplus.dto.UserBankBean;
import com.irplus.dto.roles.RoleBean;

public class UserBean {

	private Integer zipcode;
	private Integer userid;
	private String internalUserCode;
	private Integer isactive;
	private String siteId;
	private String userCode;
	private String firstName;
	private String lastName;

	private String username;
	private String password;

	private int roleId;
	private String permissionRoleName;

	private Integer adminId;

	private String emailAddress;
	private String contactno;
	private String userPhoto;
	private Date createdDate;
	private Date modifiedDate;
	private UserBankBean[] userBank;

	public UserBankBean[] getUserBank() {
		return userBank;
	}

	public void setUserBank(UserBankBean[] userBank) {
		this.userBank = userBank;
	}

	private BankBranchInfo bankBranchInfo;
	
	
	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	private String defaultBankName;
	
	private Long ParentBankId;

	private Long bankBranchId;
	
	private Long bankBranchId_array[];
	
	private Map<Long, String> branch_ids_Locations;
	
	private Integer OtherBanks;
	
	private String bankBranchLocation;
	//userbank creation  	
	
	/*this two variables .... required for  "update profile()"
	 *  
	 *  ...... parent_bank_defoult
	 *  ...... other_bank_defoult
	 *  
	 *  *  */
	
	private Byte parent_bank_defoult;

	private Byte other_bank_defoult;
		
	private ArrayList<RoleBean> roleArrayList;
	
	private ArrayList<BankInfo> bankList;	
	
	private ArrayList<BankInfo> OtherbankList;
	public ArrayList<BankInfo> getOtherbankList() {
		return OtherbankList;
	}

	public void setOtherbankList(ArrayList<BankInfo> otherbankList) {
		OtherbankList = otherbankList;
	}

	private String roleName;
	
	public UserBean() {

	}
	
	public String getRoleName(){
		return roleName;
	}
	
	public void setRoleName(String roleNames){
		 this.roleName = roleNames;
	}

	public Integer getZipcode() {
		return zipcode;
	}

	public void setZipcode(Integer zipcode) {
		this.zipcode = zipcode;
	}

	public Integer getUserid() {
		return this.userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public String getInternalUserCode() {
		return this.internalUserCode;
	}

	public void setInternalUserCode(String internalUserCode) {
		this.internalUserCode = internalUserCode;
	}

	public String getUserCode() {
		return this.userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getFirstName() {
		return this.firstName;
	}
	
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Byte getParent_bank_defoult() {
		return parent_bank_defoult;
	}

	public void setParent_bank_defoult(Byte parent_bank_defoult) {
		this.parent_bank_defoult = parent_bank_defoult;
	}

	public Byte getOther_bank_defoult() {
		return other_bank_defoult;
	}

	public void setOther_bank_defoult(Byte other_bank_defoult) {
		this.other_bank_defoult = other_bank_defoult;
	}

	public String getDefaultBankName() {
		return defaultBankName;
	}

	public void setDefaultBankName(String defaultBankName) {
		this.defaultBankName = defaultBankName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public Integer getOtherBanks() {
		return OtherBanks;
	}

	public void setOtherBanks(Integer otherBanks) {
		OtherBanks = otherBanks;
	}

	public BankBranchInfo getBankBranchInfo() {
		return bankBranchInfo;
	}

	public void setBankBranchInfo(BankBranchInfo bankBranchInfo) {
		this.bankBranchInfo = bankBranchInfo;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return this.username;
	}

	public Map<Long, String> getBranch_ids_Locations() {
		return branch_ids_Locations;
	}

	public void setBranch_ids_Locations(Map<Long, String> branch_ids_Locations) {
		this.branch_ids_Locations = branch_ids_Locations;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getRoleId() {
		return this.roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}


	public Long getParentBankId() {
		return ParentBankId;
	}

	public void setParentBankId(Long parentBankId) {
		ParentBankId = parentBankId;
	}

	public Long getBankBranchId() {
		return bankBranchId;
	}

	public void setBankBranchId(Long bankBranchId) {
		this.bankBranchId = bankBranchId;
	}

	public String getBankBranchLocation() {
		return bankBranchLocation;
	}

	public void setBankBranchLocation(String bankBranchLocation) {
		this.bankBranchLocation = bankBranchLocation;
	}

	public Integer getAdminId() {
		return this.adminId;
	}

	public void setAdminId(Integer adminId) {
		this.adminId = adminId;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getContactno() {
		return this.contactno;
	}

	public void setContactno(String contactno) {
		this.contactno = contactno;
	}

	public Long[] getBankBranchId_array() {
		return bankBranchId_array;
	}

	public void setBankBranchId_array(Long[] bankBranchId_array) {
		this.bankBranchId_array = bankBranchId_array;
	}

	public String getUserPhoto() {
		return this.userPhoto;
	}

	public void setUserPhoto(String userPhoto) {
		this.userPhoto = userPhoto;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Integer getIsactive() {
		return isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public ArrayList<BankInfo> getBankList() {
		return bankList;
	}

	public void setBankList(ArrayList<BankInfo> bankList) {
		this.bankList = bankList;
	}

	public ArrayList<RoleBean> getRoleArrayList() {
		return roleArrayList;
	}

	public void setRoleArrayList(ArrayList<RoleBean> roleArrayList) {
		this.roleArrayList = roleArrayList;
	}

	public String getPermissionRoleName() {
		return permissionRoleName;
	}

	public void setPermissionRoleName(String permissionRoleName) {
		this.permissionRoleName = permissionRoleName;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		builder.append("UserBean [zipcode=").append(zipcode).append(", userid=").append(userid)
		.append(", siteId=").append(siteId).append(", internalUserCode=").append(internalUserCode)
		.append(", isactive=").append(isactive).append(", userCode=").append(userCode).append(", firstName=")
		.append(firstName).append(", lastName=").append(lastName).append(", username=").append(username)
		.append(", password=").append(password).append(", userBank=").append(Arrays.toString(userBank))
		.append(", roleId=").append(roleId).append(", permissionRoleName=").append(permissionRoleName).append(", adminId=")
		.append(adminId).append(", emailAddress=").append(emailAddress).append(", contactno=")
		.append(contactno).append(", userPhoto=").append(userPhoto).append(", createdDate=").append(createdDate)
		.append(", modifiedDate=").append(modifiedDate).append("]");
return builder.toString();
	}
	
}
