package com.irplus.dto;

import java.util.Date;

public class FileAudit {

	private int auditId;
	private String auditDate;
	private int totalFiles;
	private int processedFiles;
	private int errorFiles;
	private int corruptedFiles;
	private String fileType;
	private Date processedDateTime;
	
	private String fileName;
	private String description;
	private String status;
	private int isProcessed;

	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getIsProcessed() {
		return isProcessed;
	}
	public void setIsProcessed(int isProcessed) {
		this.isProcessed = isProcessed;
	}
	public int getAuditId() {
		return auditId;
	}
	public void setAuditId(int auditId) {
		this.auditId = auditId;
	}
	public String getAuditDate() {
		return auditDate;
	}
	public void setAuditDate(String auditDate) {
		this.auditDate = auditDate;
	}
	public int getTotalFiles() {
		return totalFiles;
	}
	public void setTotalFiles(int totalFiles) {
		this.totalFiles = totalFiles;
	}
	public int getProcessedFiles() {
		return processedFiles;
	}
	public void setProcessedFiles(int processedFiles) {
		this.processedFiles = processedFiles;
	}
	public int getErrorFiles() {
		return errorFiles;
	}
	public void setErrorFiles(int errorFiles) {
		this.errorFiles = errorFiles;
	}
	public int getCorruptedFiles() {
		return corruptedFiles;
	}
	public void setCorruptedFiles(int corruptedFiles) {
		this.corruptedFiles = corruptedFiles;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public Date getProcessedDateTime() {
		return processedDateTime;
	}
	public void setProcessedDateTime(Date processedDateTime) {
		this.processedDateTime = processedDateTime;
	}
}
