package com.irplus.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class ReportFileFormatInfo {

	private Integer fileFormatId;
	private Long FiletypeId;
	private Integer userId;
	private String fileformatName;
	private Integer isactive;
	private Date createddate;
	private Date modifieddate;

	private Set irSCustomerReportlicensings = new HashSet(0);

	public ReportFileFormatInfo() {
	}

	public Integer getFileFormatId() {
		return this.fileFormatId;
	}

	public void setFileFormatId(Integer fileFormatId) {
		this.fileFormatId = fileFormatId;
	}

	public String getFileformatName() {
		return this.fileformatName;
	}

	public void setFileformatName(String fileformatName) {
		this.fileformatName = fileformatName;
	}

	public Integer getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public Long getFiletypeId() {
		return FiletypeId;
	}

	public void setFiletypeId(Long filetypeId) {
		FiletypeId = filetypeId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

	public Set getIrSCustomerReportlicensings() {
		return this.irSCustomerReportlicensings;
	}

	public void setIrSCustomerReportlicensings(Set irSCustomerReportlicensings) {
		this.irSCustomerReportlicensings = irSCustomerReportlicensings;
	}

}
