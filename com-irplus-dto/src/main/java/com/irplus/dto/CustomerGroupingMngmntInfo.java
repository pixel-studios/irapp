package com.irplus.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public class CustomerGroupingMngmntInfo {
	
	private Integer customerGrpId ;
	private String customerGroupName ;
	private Integer isactive ;
	private String createddate;
	private Date modifieddate;
	private Integer userid ;
	private Object highdate ;
	private String totalbatchamt ;
	private String achfiletype ;
	private String createdtime;
	
	
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	private String day;
	private String month;
	private String year;
	
	public String getFileTypeId() {
		return fileTypeId;
	}
	public void setFileTypeId(String fileTypeId) {
		this.fileTypeId = fileTypeId;
	}
	private String fileTypeId ;
	
	public String getCreatedtime() {
		return createdtime;
	}
	public void setCreatedtime(String createdtime) {
		this.createdtime = createdtime;
	}
	public String getCurrentdate() {
		return currentdate;
	}
	public void setCurrentdate(String currentdate) {
		this.currentdate = currentdate;
	}
	public int getTotalFiles() {
		return totalFiles;
	}
	public void setTotalFiles(int totalFiles) {
		this.totalFiles = totalFiles;
	}
	public int getTotalBanks() {
		return totalBanks;
	}
	public void setTotalBanks(int totalBanks) {
		this.totalBanks = totalBanks;
	}
	public int getTotalCustomers() {
		return totalCustomers;
	}
	public void setTotalCustomers(int totalCustomers) {
		this.totalCustomers = totalCustomers;
	}
	public long getBankwiseId() {
		return bankwiseId;
	}
	public void setBankwiseId(long bankwiseId) {
		this.bankwiseId = bankwiseId;
	}
	public Object getFileTypeName() {
		return fileTypeName;
	}
	public void setFileTypeName(Object fileTypeName) {
		this.fileTypeName = fileTypeName;
	}
	public String getBankwiseAmount() {
		return BankwiseAmount;
	}
	public void setBankwiseAmount(String bankwiseAmount) {
		BankwiseAmount = bankwiseAmount;
	}
	public long getBankAchfileCount() {
		return BankAchfileCount;
	}
	public void setBankAchfileCount(long bankAchfileCount) {
		BankAchfileCount = bankAchfileCount;
	}
	public long getBankLockboxfileCount() {
		return BankLockboxfileCount;
	}
	public void setBankLockboxfileCount(long bankLockboxfileCount) {
		BankLockboxfileCount = bankLockboxfileCount;
	}
	public long getSiteId() {
		return siteId;
	}
	public void setSiteId(long siteId) {
		this.siteId = siteId;
	}
	public String getTotSummaryItems() {
		return TotSummaryItems;
	}
	public void setTotSummaryItems(String totSummaryItems) {
		TotSummaryItems = totSummaryItems;
	}
	public String getTotSummaryAmount() {
		return TotSummaryAmount;
	}
	public void setTotSummaryAmount(String totSummaryAmount) {
		TotSummaryAmount = totSummaryAmount;
	}
	public String getTotSummaryTrans() {
		return TotSummaryTrans;
	}
	public void setTotSummaryTrans(String totSummaryTrans) {
		TotSummaryTrans = totSummaryTrans;
	}
	private String currentdate;
	
	private String Todatevalue;
	
	public String getTodatevalue() {
		return Todatevalue;
	}
	public void setTodatevalue(String todatevalue) {
		Todatevalue = todatevalue;
	}
	private int totalFiles ;
	private int totalBanks ;
	private int totalCustomers ;
	private long bankwiseId ;
	private Object fileTypeName ;
	private String BankwiseAmount ;
	private long  BankAchfileCount ;
	private long  BankLockboxfileCount ;
	private long  siteId ;
	private String TotSummaryItems ;
	private String TotSummaryAmount ;
	private String TotSummaryTrans ;
	
	
	public String getBankCustomercode() {
		return bankCustomercode;
	}
	public void setBankCustomercode(String bankCustomercode) {
		this.bankCustomercode = bankCustomercode;
	}
	private String bankCustomercode ;
	
	private int weekbatches ;
	public int getWeekbatches() {
		return weekbatches;
	}
	public void setWeekbatches(int weekbatches) {
		this.weekbatches = weekbatches;
	}
	public String getWeektransactions() {
		return weektransactions;
	}
	public void setWeektransactions(String weektransactions) {
		this.weektransactions = weektransactions;
	}
	public String getWeekbatchamount() {
		return weekbatchamount;
	}
	public void setWeekbatchamount(String weekbatchamount) {
		this.weekbatchamount = weekbatchamount;
	}
	private String weektransactions ;
	private String weekbatchamount ;
	public String getAchfiletype() {
		return achfiletype;
	}
	public void setAchfiletype(String achfiletype) {
		this.achfiletype = achfiletype;
	}
	private long  AchfileCount ;
	private String highestdate ;
	public String getHighestdate() {
		return highestdate;
	}
	public void setHighestdate(String highestdate) {
		this.highestdate = highestdate;
	}
	public long getAchfileCount() {
		return AchfileCount;
	}
	public void setAchfileCount(long achfileCount) {
		AchfileCount = achfileCount;
	}
	public long getLockboxfileCount() {
		return lockboxfileCount;
	}
	public void setLockboxfileCount(long lockboxfileCount) {
		this.lockboxfileCount = lockboxfileCount;
	}
	private long  lockboxfileCount ;
	public String getTotalitems() {
		return totalitems;
	}
	public void setTotalitems(String totalitems) {
		this.totalitems = totalitems;
	}
	public String getTotaltransactions() {
		return totaltransactions;
	}
	public void setTotaltransactions(String totaltransactions) {
		this.totaltransactions = totaltransactions;
	}
	private String totalitems ;
	private String totaltransactions;
	public String getTotalbatchamt() {
		return totalbatchamt;
	}
	public void setTotalbatchamt(String totalbatchamt) {
		this.totalbatchamt = totalbatchamt;
	}
	public Object getHighdate() {
		return highdate;
	}
	public void setHighdate(Object highdate) {
		this.highdate = highdate;
	}
	private CustomerGroupingDetails[] customergroupingDetails;
	
	public Integer getTotalMappedBranch() {
		return totalMappedBranch;
	}
	public void setTotalMappedBranch(Integer totalMappedBranch) {
		this.totalMappedBranch = totalMappedBranch;
	}
	public String getBranchLocation() {
		return branchLocation;
	}
	public void setBranchLocation(String branchLocation) {
		this.branchLocation = branchLocation;
	}
	private String branchLocation;



	public String getBankId() {
		return bankId;
	}
	public void setBankId(String bankId) {
		this.bankId = bankId;
	}
	private String bankId ;

	private String BankName ;

	public String getBankName() {
		return BankName;
	}
	public void setBankName(String bankName) {
		BankName = bankName;
	}
	private Integer totalMappedBranch;
	
	
	private Integer customrGrpDetailsId;
	
	
	private Integer adminid;
	private String customerCompanyIdentNo;
	
	
	public String getCreateddate() {
		return createddate;
	}
	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}
	public Date getModifieddate() {
		return modifieddate;
	}
	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

	
	
	private String companyName;
	private String parentBankName;
	private String bankCustomerCode;
	private String bankBranchLocation;
	
	private Integer usercode ;
	
	
	private Long branchId;
	private Integer customerId[] ;
	private Integer customerid;
	
	
	private String todate ;
	public String getTodate() {
		return todate;
	}
	public void setTodate(String todate) {
		this.todate = todate;
	}
	public String getFromdate() {
		return fromdate;
	}
	public void setFromdate(String fromdate) {
		this.fromdate = fromdate;
	}
	private String fromdate ;
	
	public CustomerGroupingDetails[] getCustomergroupingDetails() {
		return customergroupingDetails;
	}
	public void setCustomergroupingDetails(CustomerGroupingDetails[] customergroupingDetails) {
		this.customergroupingDetails = customergroupingDetails;
	}

	private Object CustId;
	
	public Object getCustId() {
		return CustId;
	}
	public void setCustId(Object custId) {
		CustId = custId;
	}
	private Map<Integer , Integer> mapBankCustmrIds;	
	private Map<String , Integer> mapGrpNameMapedCmpnsNos;
  //private Map<String , Integer> mapBankNameMapedCustmrId;
	private Map<String , String> mapBankNameMapedCustmrId;	
	private ArrayList<BankCustmrMap> bankCustmrMapArray;
	
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	public ArrayList<BankCustmrMap> getBankCustmrMapArray() {
		return bankCustmrMapArray;
	}
	public void setBankCustmrMapArray(ArrayList<BankCustmrMap> bankCustmrMapArray) {
		this.bankCustmrMapArray = bankCustmrMapArray;
	}
	public Integer getCustomerGrpId(){
		return customerGrpId;
	}
	public void setCustomerGrpId(Integer customerGrpId) {
		this.customerGrpId = customerGrpId;
	}
	public Integer getCustomrGrpDetailsId() {
		return customrGrpDetailsId;
	}
	public void setCustomrGrpDetailsId(Integer customrGrpDetailsId) {
		this.customrGrpDetailsId = customrGrpDetailsId;
	}

	public String getBankBranchLocation() {
		return bankBranchLocation;
	}
	public void setBankBranchLocation(String bankBranchLocation) {
		this.bankBranchLocation = bankBranchLocation;
	}
	public Integer getUsercode() {
		return usercode;
	}
	public void setUsercode(Integer usercode) {
		this.usercode = usercode;
	}
	public Integer getCustomerid() {
		return customerid;
	}
	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}
	public Map<String, String> getMapBankNameMapedCustmrId() {
		return mapBankNameMapedCustmrId;
	}
	public Map<Integer, Integer> getMapBankCustmrIds() {
		return mapBankCustmrIds;
	}
	public void setMapBankCustmrIds(Map<Integer, Integer> mapBankCustmrIds) {
		this.mapBankCustmrIds = mapBankCustmrIds;
	}
	public void setMapBankNameMapedCustmrId(Map<String, String> mapBankNameMapedCustmrId) {
		this.mapBankNameMapedCustmrId = mapBankNameMapedCustmrId;
	}
	public String getCustomerGroupName() {
		return customerGroupName;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getParentBankName() {
		return parentBankName;
	}
	public void setParentBankName(String parentBankName) {
		this.parentBankName = parentBankName;
	}
	public void setCustomerGroupName(String customerGroupName) {
		this.customerGroupName = customerGroupName;
	}
	public Map<String, Integer> getMapGrpNameMapedCmpnsNos() {
		return mapGrpNameMapedCmpnsNos;
	}
	public void setMapGrpNameMapedCmpnsNos(Map<String, Integer> mapGrpNameMapedCmpnsNos) {
		this.mapGrpNameMapedCmpnsNos = mapGrpNameMapedCmpnsNos;
	}
	public Integer getAdminid() {
		return adminid;
	}
	public void setAdminid(Integer adminid) {
		this.adminid = adminid;
	}
	public Integer getIsactive() {
		return isactive;
	}
	public String getCustomerCompanyIdentNo() {
		return customerCompanyIdentNo;
	}
	public void setCustomerCompanyIdentNo(String customerCompanyIdentNo) {
		this.customerCompanyIdentNo = customerCompanyIdentNo;
	}
	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}
	public String getBankCustomerCode() {
		return bankCustomerCode;
	}
	public void setBankCustomerCode(String bankCustomerCode) {
		this.bankCustomerCode = bankCustomerCode;
	}


	
	public Integer getUserid() {
		return userid;
	}
	
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	
	public Long getBranchId() {
		return branchId;
	}
	
	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}
	
	public Integer[] getCustomerId() {
		return customerId;
	}
	
	public void setCustomerId(Integer[] customerId) {
		this.customerId = customerId;
	}
	
/*	public Map<Integer, Integer> getMapBankCustmrIds() {
		return mapBankCustmrIds;
	}
	public void setMapBankCustmrIds(Map<Integer, Integer> mapBankCustmrIds) {
		this.mapBankCustmrIds = mapBankCustmrIds;
	}	
	*/
}