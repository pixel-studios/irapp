package com.irplus.dto;

public class BankViewInfo {
	
	    private static final long serialVersionUID = -3176074231815769436L;

	    private long bankId;
	    private Character isdefault;
	    private int isactive;
	    private String siteId;
	    private String bankName;
	    private String bankCode;
	    private String bankLogo;
	    private String createddate;
	    private String modifieddate;
	    private String firstname;
	    private String lastName;
	    public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		private String email;
		private String contactno;
		public long getBankId() {
			return bankId;
		}
		public void setBankId(long bankId) {
			this.bankId = bankId;
		}
		public Character getIsdefault() {
			return isdefault;
		}
		public void setIsdefault(Character isdefault) {
			this.isdefault = isdefault;
		}
		public int getIsactive() {
			return isactive;
		}
		public void setIsactive(int isactive) {
			this.isactive = isactive;
		}
		public String getSiteId() {
			return siteId;
		}
		public void setSiteId(String siteId) {
			this.siteId = siteId;
		}
		public String getBankName() {
			return bankName;
		}
		public void setBankName(String bankName) {
			this.bankName = bankName;
		}
		public String getBankCode() {
			return bankCode;
		}
		public void setBankCode(String bankCode) {
			this.bankCode = bankCode;
		}
		public String getBankLogo() {
			return bankLogo;
		}
		public void setBankLogo(String bankLogo) {
			this.bankLogo = bankLogo;
		}
		public String getCreateddate() {
			return createddate;
		}
		public void setCreateddate(String createddate) {
			this.createddate = createddate;
		}
		public String getModifieddate() {
			return modifieddate;
		}
		public void setModifieddate(String modifieddate) {
			this.modifieddate = modifieddate;
		}
		public String getFirstname() {
			return firstname;
		}
		public void setFirstname(String firstname) {
			this.firstname = firstname;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getContactno() {
			return contactno;
		}
		public void setContactno(String contactno) {
			this.contactno = contactno;
		}
		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("BankViewInfo [bankId=").append(bankId).append(", isdefault=").append(isdefault)
					.append(", isactive=").append(isactive).append(", siteId=").append(siteId).append(", bankName=")
					.append(bankName).append(", bankCode=").append(bankCode).append(", bankLogo=").append(bankLogo)
					.append(", createddate=").append(createddate).append(", modifieddate=").append(modifieddate)
					.append(", firstname=").append(firstname).append(", email=").append(email).append(", contactno=")
					.append(contactno).append("]");
			return builder.toString();
		}
		
		
	}



