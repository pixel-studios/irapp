package com.irplus.dto;
 
import com.irplus.dto.users.UserBean;

public class BankBranchInfo {

	private Long branchId;
	private String branchLocation;
	private String branchOwnCode;
	private Character isdefault;
	private String locationId;
	private String rtNumber;
	private String ddaNumber;
	private String website;
	private String country;
	private String state;
	private String city;
	private String zipcode;
	private int isactive;
	
	private UserBean user;
	private BankInfo bank;
   
	private Long parent_bankId;
	private String parent_bankName;
	
	public Long getBranchId() {
		return branchId;
	}
	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}
	public String getBranchLocation() {
		return branchLocation;
	}
	public void setBranchLocation(String branchLocation) {
		this.branchLocation = branchLocation;
	}
	public String getBranchOwnCode() {
		return branchOwnCode;
	}
	public void setBranchOwnCode(String branchOwnCode) {
		this.branchOwnCode = branchOwnCode;
	}
	public Character getIsdefault() {
		return isdefault;
	}
	public void setIsdefault(Character isdefault) {
		this.isdefault = isdefault;
	}
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	public String getRtNumber() {
		return rtNumber;
	}
	public void setRtNumber(String rtNumber) {
		this.rtNumber = rtNumber;
	}
	public String getDdaNumber() {
		return ddaNumber;
	}
	public Long getParent_bankId() {
		return parent_bankId;
	}
	public String getParent_bankName() {
		return parent_bankName;
	}
	public void setParent_bankName(String parent_bankName) {
		this.parent_bankName = parent_bankName;
	}
	public void setParent_bankId(Long parent_bankId) {
		this.parent_bankId = parent_bankId;
	}
	public void setDdaNumber(String ddaNumber) {
		this.ddaNumber = ddaNumber;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public int getIsactive() {
		return isactive;
	}
	public void setIsactive(int isactive) {
		this.isactive = isactive;
	}
	public UserBean getUser() {
		return user;
	}
	public void setUser(UserBean user) {
		this.user = user;
	}
	public BankInfo getBank() {
		return bank;
	}
	public void setBank(BankInfo bank) {
		this.bank = bank;
	}
	
}
