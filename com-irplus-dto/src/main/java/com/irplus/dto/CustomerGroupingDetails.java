package com.irplus.dto;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;


public class CustomerGroupingDetails {
	private static final long serialVersionUID = -3176074231815769436L;
	
	private Integer customrGrpDetailsId;
	private Integer customerGrpId;
	private Long branchId;
	private Integer customerId;
	private Integer isactive;
	private Integer userId;
	private String createddate;
	public Long getBankId() {
		return bankId;
	}
	public void setBankId(Long bankId) {
		this.bankId = bankId;
	}


	private Long bankId;


	

	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	
	public String getCreateddate() {
		return createddate;
	}
	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}
	

	
	
	
	public Integer getCustomerGrpId() {
		return customerGrpId;
	}
	public void setCustomerGrpId(Integer customerGrpId) {
		this.customerGrpId = customerGrpId;
	}
	public Integer getCustomrGrpDetailsId() {
		return customrGrpDetailsId;
	}
	public void setCustomrGrpDetailsId(Integer customrGrpDetailsId) {
		this.customrGrpDetailsId = customrGrpDetailsId;
	}

	public Long getBranchId() {
		return branchId;
	}
	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getIsactive() {
		return isactive;
	}
	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	
/*	public CustomerGroupingDetails() {
	}
*/

/*	public CustomerGroupingDetails(int customrGrpDetailsId) {
		this.customrGrpDetailsId = customrGrpDetailsId;
	}
	public CustomerGroupingDetails(Integer customerGrpId, Integer customrGrpDetailsId, Integer userid, Long branchId,
			Integer customerId, Date createddate, Date modifieddate, Integer isactive
		) {
		super();
		this.customerGrpId = customerGrpId;
		this.customrGrpDetailsId = customrGrpDetailsId;
		this.userid = userid;
		this.branchId = branchId;
		this.customerId = customerId;
		this.createddate = createddate;
		this.modifieddate = modifieddate;
		this.isactive = isactive;
	
	}*/
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CustomerGroupingDetails [customerGrpId=").append(customerGrpId).append(", customrGrpDetailsId=").append(customrGrpDetailsId)
				.append(", userId=").append(userId).append(", branchId=").append(branchId)
				.append(", customerId=").append(customerId).append(", createddate=").append(createddate)
				.append(", isactive=").append(isactive)
		
				.append("]");
		return builder.toString();
	}
	
	

}