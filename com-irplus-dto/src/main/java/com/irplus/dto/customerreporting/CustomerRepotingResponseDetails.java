package com.irplus.dto.customerreporting;

import java.util.List;

import com.irplus.dto.BankInfo;

public class CustomerRepotingResponseDetails {

	private boolean isValidationSuccess;
    private int statusCode;
    private String statusMsg;
    private List<String> errMsgs;
    private List<CustomerRepotingBean> customerRepotingBeans;
    
    public boolean isValidationSuccess() {
		return isValidationSuccess;
	}
	public void setValidationSuccess(boolean isValidationSuccess) {
		this.isValidationSuccess = isValidationSuccess;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public List<CustomerRepotingBean> getCustomerRepotingBeans() {
		return customerRepotingBeans;
	}
	public void setCustomerRepotingBeans(List<CustomerRepotingBean> customerRepotingBeans) {
		this.customerRepotingBeans = customerRepotingBeans;
	}
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
	public List<String> getErrMsgs() {
		return errMsgs;
	}
	public void setErrMsgs(List<String> errMsgs) {
		this.errMsgs = errMsgs;
	}
}