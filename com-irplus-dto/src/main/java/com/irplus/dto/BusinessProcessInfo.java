package com.irplus.dto;

public class BusinessProcessInfo {
	
	    private static final long serialVersionUID = -3176074231815769436L;

	    private Long businessProcessId;
		private String processName;
		private int licenseId;
		private Character hasVisibility;
		private Character isparent;
		private int isactive;		
	    private String createddate;
	    private String modifieddate;
	    
		public Long getBusinessProcessId() {
			return businessProcessId;
		}
		public void setBusinessProcessId(Long businessProcessId) {
			this.businessProcessId = businessProcessId;
		}
		public String getProcessName() {
			return processName;
		}
		public void setProcessName(String processName) {
			this.processName = processName;
		}
		public int getLicenseId() {
			return licenseId;
		}
		public void setLicenseId(int licenseId) {
			this.licenseId = licenseId;
		}
		public Character getHasVisibility() {
			return hasVisibility;
		}
		public void setHasVisibility(Character hasVisibility) {
			this.hasVisibility = hasVisibility;
		}
		public Character getIsparent() {
			return isparent;
		}
		public void setIsparent(Character isparent) {
			this.isparent = isparent;
		}
		public int getIsactive() {
			return isactive;
		}
		public void setIsactive(int isactive) {
			this.isactive = isactive;
		}
		public String getCreateddate() {
			return createddate;
		}
		public void setCreateddate(String createddate) {
			this.createddate = createddate;
		}
		public String getModifieddate() {
			return modifieddate;
		}
		public void setModifieddate(String modifieddate) {
			this.modifieddate = modifieddate;
		}
		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("BusinessProcessInfo [businessProcessId=").append(businessProcessId).append(", processName=")
					.append(processName).append(", licenseId=").append(licenseId).append(", hasVisibility=")
					.append(hasVisibility).append(", isparent=").append(isparent).append(", isactive=").append(isactive)
					.append(", createddate=").append(createddate).append(", modifieddate=").append(modifieddate)
					.append("]");
			return builder.toString();
		}
	   
		
		
	}



