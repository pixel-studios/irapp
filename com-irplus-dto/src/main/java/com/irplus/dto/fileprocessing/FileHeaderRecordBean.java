package com.irplus.dto.fileprocessing;

import java.util.ArrayList;
import java.util.List;

public class FileHeaderRecordBean{

	private int fileHeaderRecordId;
	private String achFileName;
	private char recordTypeCode;
	private String priorityCode;
	private String immediateDestination;
	private String immediateOrigin;
	private String fileCreationDate;
	private String fileCreationTime;
	private String fileIdModifier;
	private int isProcessed;
	private String processedDate;
	private String processedTime;
	private String recordSize;
	private String blockingFactor;
	private String formatCode;
	private String immediateDestinationName;
	private String immediateOriginName;
	private String referenceCode;
	private Long bankId;
	private String bankName;
	private String bankLogo;
	private String fileType;
	private String filetypeId;
	
	private long fileTypeId;
	public long getFileTypeId() {
		return fileTypeId;
	}

	public void setFileTypeId(long fileTypeId) {
		this.fileTypeId = fileTypeId;
	}

	public String getFiletypeId() {
		return filetypeId;
	}

	public void setFiletypeId(String filetypeId) {
		this.filetypeId = filetypeId;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankLogo() {
		return bankLogo;
	}

	public void setBankLogo(String bankLogo) {
		this.bankLogo = bankLogo;
	}
	private int customerId;
	
	
	public Long getBankId() {
		return bankId;
	}

	public void setBankId(Long bankId) {
		this.bankId = bankId;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	private List<BatchHeaderRecordBean> batchHeaderRecordBean = new ArrayList<BatchHeaderRecordBean>();
	private List<WEBEntryDetailRecordBean> entryDetailRecordBean= new ArrayList<WEBEntryDetailRecordBean>();
	
	public List<WEBEntryDetailRecordBean> getEntryDetailRecordBean() {
		return entryDetailRecordBean;
	}

	public void setEntryDetailRecordBean(List<WEBEntryDetailRecordBean> entryDetailRecordBean) {
		this.entryDetailRecordBean = entryDetailRecordBean;
	}
	private List<BatchControlRecordBean> batchControlRecordBean= new ArrayList<BatchControlRecordBean>();
	private List<FileControlRecordBean> fileControlRecordBean= new ArrayList<FileControlRecordBean>();
	private List<CCDAddendaRecordBean> cCDAddendaRecordBean= new ArrayList<CCDAddendaRecordBean>();

	public int getIsProcessed() {
		return isProcessed;
	}

	public void setIsProcessed(int isProcessed) {
		this.isProcessed = isProcessed;
	}

	public String getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(String processedDate) {
		this.processedDate = processedDate;
	}

	public String getProcessedTime() {
		return processedTime;
	}

	public void setProcessedTime(String processedTime) {
		this.processedTime = processedTime;
	}
	
	
	

	

	public List<BatchControlRecordBean> getBatchControlRecordBean() {
		return batchControlRecordBean;
	}

	public void setBatchControlRecordBean(List<BatchControlRecordBean> batchControlRecordBean) {
		this.batchControlRecordBean = batchControlRecordBean;
	}

	public List<FileControlRecordBean> getFileControlRecordBean() {
		return fileControlRecordBean;
	}

	public void setFileControlRecordBean(List<FileControlRecordBean> fileControlRecordBean) {
		this.fileControlRecordBean = fileControlRecordBean;
	}

	public List<CCDAddendaRecordBean> getcCDAddendaRecordBean() {
		return cCDAddendaRecordBean;
	}

	public void setcCDAddendaRecordBean(List<CCDAddendaRecordBean> cCDAddendaRecordBean) {
		this.cCDAddendaRecordBean = cCDAddendaRecordBean;
	}

	public FileHeaderRecordBean() {
		
	}
	
	public List<BatchHeaderRecordBean> getBatchHeaderRecordBean() {
		return batchHeaderRecordBean;
	}

	public void setBatchHeaderRecordBean(List<BatchHeaderRecordBean> batchHeaderRecordBean) {
		this.batchHeaderRecordBean = batchHeaderRecordBean;
	}

	public int getFileHeaderRecordId() {
		return fileHeaderRecordId;
	}
	public void setFileHeaderRecordId(int fileHeaderRecordId) {
		this.fileHeaderRecordId = fileHeaderRecordId;
	}
	public String getAchFileName() {
		return achFileName;
	}
	public void setAchFileName(String achFileName) {
		this.achFileName = achFileName;
	}
	public char getRecordTypeCode() {
		return recordTypeCode;
	}
	public void setRecordTypeCode(char recordTypeCode) {
		this.recordTypeCode = recordTypeCode;
	}
	public String getPriorityCode() {
		return priorityCode;
	}
	public void setPriorityCode(String priorityCode) {
		this.priorityCode = priorityCode;
	}
	public String getImmediateDestination() {
		return immediateDestination;
	}
	public void setImmediateDestination(String immediateDestination) {
		this.immediateDestination = immediateDestination;
	}
	public String getImmediateOrigin() {
		return immediateOrigin;
	}
	public void setImmediateOrigin(String immediateOrigin) {
		this.immediateOrigin = immediateOrigin;
	}
	public String getFileCreationDate() {
		return fileCreationDate;
	}
	public void setFileCreationDate(String fileCreationDate) {
		this.fileCreationDate = fileCreationDate;
	}
	public String getFileCreationTime() {
		return fileCreationTime;
	}
	public void setFileCreationTime(String fileCreationTime) {
		this.fileCreationTime = fileCreationTime;
	}
	public String getFileIdModifier() {
		return fileIdModifier;
	}
	public void setFileIdModifier(String fileIdModifier) {
		this.fileIdModifier = fileIdModifier;
	}
	public String getRecordSize() {
		return recordSize;
	}
	public void setRecordSize(String recordSize) {
		this.recordSize = recordSize;
	}
	public String getBlockingFactor() {
		return blockingFactor;
	}
	public void setBlockingFactor(String blockingFactor) {
		this.blockingFactor = blockingFactor;
	}
	public String getFormatCode() {
		return formatCode;
	}
	public void setFormatCode(String formatCode) {
		this.formatCode = formatCode;
	}
	public String getImmediateDestinationName() {
		return immediateDestinationName;
	}
	public void setImmediateDestinationName(String immediateDestinationName) {
		this.immediateDestinationName = immediateDestinationName;
	}
	public String getImmediateOriginName() {
		return immediateOriginName;
	}
	public void setImmediateOriginName(String immediateOriginName) {
		this.immediateOriginName = immediateOriginName;
	}
	public String getReferenceCode() {
		return referenceCode;
	}
	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}
}
