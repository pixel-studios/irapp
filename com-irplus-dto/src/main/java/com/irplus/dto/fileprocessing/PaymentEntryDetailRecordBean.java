package com.irplus.dto.fileprocessing;

public class PaymentEntryDetailRecordBean {
	
	private int paymentId;
	private String recordTypeCode;
	private String transactionCode;
	private String receivingDFIIdentification;
	private String checkDigit;
	private String dfiAccountNumber;
	private String amount;
	private String individualIdentificationNumber;
	private FileHeaderRecordBean fileHeaderRecordBean;
	private BatchHeaderRecordBean batchHeaderRecordBean;
	
	public int getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}

	public String getRecordTypeCode() {
		return recordTypeCode;
	}

	public void setRecordTypeCode(String recordTypeCode) {
		this.recordTypeCode = recordTypeCode;
	}

	public String getTransactionCode() {
		return transactionCode;
	}

	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

	public String getReceivingDFIIdentification() {
		return receivingDFIIdentification;
	}

	public void setReceivingDFIIdentification(String receivingDFIIdentification) {
		this.receivingDFIIdentification = receivingDFIIdentification;
	}

	public String getCheckDigit() {
		return checkDigit;
	}

	public void setCheckDigit(String checkDigit) {
		this.checkDigit = checkDigit;
	}

	public String getDfiAccountNumber() {
		return dfiAccountNumber;
	}

	public void setDfiAccountNumber(String dfiAccountNumber) {
		this.dfiAccountNumber = dfiAccountNumber;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getIndividualIdentificationNumber() {
		return individualIdentificationNumber;
	}

	public void setIndividualIdentificationNumber(String individualIdentificationNumber) {
		this.individualIdentificationNumber = individualIdentificationNumber;
	}

	public FileHeaderRecordBean getFileHeaderRecordBean() {
		return fileHeaderRecordBean;
	}

	public void setFileHeaderRecordBean(FileHeaderRecordBean fileHeaderRecordBean) {
		this.fileHeaderRecordBean = fileHeaderRecordBean;
	}

	public BatchHeaderRecordBean getBatchHeaderRecordBean() {
		return batchHeaderRecordBean;
	}

	public void setBatchHeaderRecordBean(BatchHeaderRecordBean batchHeaderRecordBean) {
		this.batchHeaderRecordBean = batchHeaderRecordBean;
	}

	public PaymentEntryDetailRecordBean() {
		
	}
	
	

}
