package com.irplus.dto.fileprocessing;

import java.util.ArrayList;
import java.util.List;

public class BatchHeaderRecordBean {
	
	private int batchHeaderRecordId;
	private char recordTypeCode;
	private String serviceClassCode;
	private String companyName;
	private String discretionaryData;
	private String companyIdentification;
	private String standardEntryClass;
	private String companyEntryDescription;
	private String companyDescriptiveDate;
	private String effectiveEntryDate;
	private String reserved;
	private String orginatorStatusCode;
	private String orginatingFinancialInstitution;
	private String batchNumber;
	private String customerName;
	private String batchAmount;
	private String batchPNG;
	private String transactionCount;
	private String itemCount;
	private String paymentCount;
	private String remittanceCount;
	private String scandocCount;
	private String frontImage;
	private String rearImage;
	
	public String getFrontImage() {
		return frontImage;
	}

	public void setFrontImage(String frontImage) {
		this.frontImage = frontImage;
	}

	public String getRearImage() {
		return rearImage;
	}

	public void setRearImage(String rearImage) {
		this.rearImage = rearImage;
	}

	public String getBatchPNG() {
		return batchPNG;
	}

	public void setBatchPNG(String batchPNG) {
		this.batchPNG = batchPNG;
	}

	public String getBatchAmount() {
		return batchAmount;
	}

	public void setBatchAmount(String batchAmount) {
		this.batchAmount = batchAmount;
	}

	public String getTransactionCount() {
		return transactionCount;
	}

	public void setTransactionCount(String transactionCount) {
		this.transactionCount = transactionCount;
	}

	public String getItemCount() {
		return itemCount;
	}

	public void setItemCount(String itemCount) {
		this.itemCount = itemCount;
	}

	public String getPaymentCount() {
		return paymentCount;
	}

	public void setPaymentCount(String paymentCount) {
		this.paymentCount = paymentCount;
	}

	public String getRemittanceCount() {
		return remittanceCount;
	}

	public void setRemittanceCount(String remittanceCount) {
		this.remittanceCount = remittanceCount;
	}

	public String getScandocCount() {
		return scandocCount;
	}

	public void setScandocCount(String scandocCount) {
		this.scandocCount = scandocCount;
	}


	
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	private FileHeaderRecordBean fileHeaderRecordBean;
	private List<WEBEntryDetailRecordBean> entryDetailRecordBean= new ArrayList<WEBEntryDetailRecordBean>();

	

	
	public List<WEBEntryDetailRecordBean> getEntryDetailRecordBean() {
		return entryDetailRecordBean;
	}

	public void setEntryDetailRecordBean(List<WEBEntryDetailRecordBean> entryDetailRecordBean) {
		this.entryDetailRecordBean = entryDetailRecordBean;
	}

	

	public BatchHeaderRecordBean() {
		
	}

	public int getBatchHeaderRecordId() {
		return batchHeaderRecordId;
	}

	public void setBatchHeaderRecordId(int batchHeaderRecordId) {
		this.batchHeaderRecordId = batchHeaderRecordId;
	}

	public char getRecordTypeCode() {
		return recordTypeCode;
	}

	public void setRecordTypeCode(char recordTypeCode) {
		this.recordTypeCode = recordTypeCode;
	}

	public String getServiceClassCode() {
		return serviceClassCode;
	}

	public void setServiceClassCode(String serviceClassCode) {
		this.serviceClassCode = serviceClassCode;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getDiscretionaryData() {
		return discretionaryData;
	}

	public void setDiscretionaryData(String discretionaryData) {
		this.discretionaryData = discretionaryData;
	}

	public String getCompanyIdentification() {
		return companyIdentification;
	}

	public void setCompanyIdentification(String companyIdentification) {
		this.companyIdentification = companyIdentification;
	}

	public String getStandardEntryClass() {
		return standardEntryClass;
	}

	public void setStandardEntryClass(String standardEntryClass) {
		this.standardEntryClass = standardEntryClass;
	}

	public String getCompanyEntryDescription() {
		return companyEntryDescription;
	}

	public void setCompanyEntryDescription(String companyEntryDescription) {
		this.companyEntryDescription = companyEntryDescription;
	}

	public String getCompanyDescriptiveDate() {
		return companyDescriptiveDate;
	}

	public void setCompanyDescriptiveDate(String companyDescriptiveDate) {
		this.companyDescriptiveDate = companyDescriptiveDate;
	}

	public String getEffectiveEntryDate() {
		return effectiveEntryDate;
	}

	public void setEffectiveEntryDate(String effectiveEntryDate) {
		this.effectiveEntryDate = effectiveEntryDate;
	}

	public String getReserved() {
		return reserved;
	}

	public void setReserved(String reserved) {
		this.reserved = reserved;
	}

	public String getOrginatorStatusCode() {
		return orginatorStatusCode;
	}

	public void setOrginatorStatusCode(String orginatorStatusCode) {
		this.orginatorStatusCode = orginatorStatusCode;
	}

	public String getOrginatingFinancialInstitution() {
		return orginatingFinancialInstitution;
	}

	public void setOrginatingFinancialInstitution(String orginatingFinancialInstitution) {
		this.orginatingFinancialInstitution = orginatingFinancialInstitution;
	}

	public String getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	public FileHeaderRecordBean getFileHeaderRecordBean() {
		return fileHeaderRecordBean;
	}

	public void setFileHeaderRecordBean(FileHeaderRecordBean fileHeaderRecordBean) {
		this.fileHeaderRecordBean = fileHeaderRecordBean;
	}

}
