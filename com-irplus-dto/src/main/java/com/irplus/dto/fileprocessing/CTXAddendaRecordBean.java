package com.irplus.dto.fileprocessing;

public class CTXAddendaRecordBean {
	
	private int ctxAddendaRecordId;
	private String recordType;
	private String addendaTypeCode;
	private String paymentRelatedInformation;
	private String addendaSequenceNumber;
	private String entryDetailSequenceNumber;

	public int getCtxAddendaRecordId() {
		return ctxAddendaRecordId;
	}

	public void setCtxAddendaRecordId(int ctxAddendaRecordId) {
		this.ctxAddendaRecordId = ctxAddendaRecordId;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	public String getAddendaTypeCode() {
		return addendaTypeCode;
	}

	public void setAddendaTypeCode(String addendaTypeCode) {
		this.addendaTypeCode = addendaTypeCode;
	}

	public String getPaymentRelatedInformation() {
		return paymentRelatedInformation;
	}

	public void setPaymentRelatedInformation(String paymentRelatedInformation) {
		this.paymentRelatedInformation = paymentRelatedInformation;
	}

	public String getAddendaSequenceNumber() {
		return addendaSequenceNumber;
	}

	public void setAddendaSequenceNumber(String addendaSequenceNumber) {
		this.addendaSequenceNumber = addendaSequenceNumber;
	}

	public String getEntryDetailSequenceNumber() {
		return entryDetailSequenceNumber;
	}

	public void setEntryDetailSequenceNumber(String entryDetailSequenceNumber) {
		this.entryDetailSequenceNumber = entryDetailSequenceNumber;
	}

}
