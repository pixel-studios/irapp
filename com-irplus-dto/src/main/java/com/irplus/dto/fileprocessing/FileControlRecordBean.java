package com.irplus.dto.fileprocessing;

public class FileControlRecordBean {
	
	private int fileControlRecordId;
	private char recordTypeCode;
	private int batchCount;
	private String blockCount;
	private int entryAddendaCount;
	private String entryHash;
	private double totalDebitEntryDollarAmountInFile;
	private double totalcreditEntryDollarAmountInFile;
	private String reserved;
	private FileHeaderRecordBean fileHeaderRecordBean;
	private BatchHeaderRecordBean batchHeaderRecordBean;
	
	
	
	public int getFileControlRecordId() {
		return fileControlRecordId;
	}
	public void setFileControlRecordId(int fileControlRecordId) {
		this.fileControlRecordId = fileControlRecordId;
	}
	public char getRecordTypeCode() {
		return recordTypeCode;
	}
	public void setRecordTypeCode(char recordTypeCode) {
		this.recordTypeCode = recordTypeCode;
	}
	public int getBatchCount() {
		return batchCount;
	}
	public void setBatchCount(int batchCount) {
		this.batchCount = batchCount;
	}
	public String getBlockCount() {
		return blockCount;
	}
	public void setBlockCount(String blockCount) {
		this.blockCount = blockCount;
	}
	public int getEntryAddendaCount() {
		return entryAddendaCount;
	}
	public void setEntryAddendaCount(int entryAddendaCount) {
		this.entryAddendaCount = entryAddendaCount;
	}
	public String getEntryHash() {
		return entryHash;
	}
	public void setEntryHash(String entryHash) {
		this.entryHash = entryHash;
	}
	public double getTotalDebitEntryDollarAmountInFile() {
		return totalDebitEntryDollarAmountInFile;
	}
	public void setTotalDebitEntryDollarAmountInFile(double totalDebitEntryDollarAmountInFile) {
		this.totalDebitEntryDollarAmountInFile = totalDebitEntryDollarAmountInFile;
	}
	public double getTotalcreditEntryDollarAmountInFile() {
		return totalcreditEntryDollarAmountInFile;
	}
	public void setTotalcreditEntryDollarAmountInFile(double totalcreditEntryDollarAmountInFile) {
		this.totalcreditEntryDollarAmountInFile = totalcreditEntryDollarAmountInFile;
	}
	public String getReserved() {
		return reserved;
	}
	public void setReserved(String reserved) {
		this.reserved = reserved;
	}
	public FileHeaderRecordBean getFileHeaderRecordBean() {
		return fileHeaderRecordBean;
	}
	public void setFileHeaderRecordBean(FileHeaderRecordBean fileHeaderRecordBean) {
		this.fileHeaderRecordBean = fileHeaderRecordBean;
	}
	public BatchHeaderRecordBean getBatchHeaderRecordBean() {
		return batchHeaderRecordBean;
	}
	public void setBatchHeaderRecordBean(BatchHeaderRecordBean batchHeaderRecordBean) {
		this.batchHeaderRecordBean = batchHeaderRecordBean;
	}
	
	
	
	

}
