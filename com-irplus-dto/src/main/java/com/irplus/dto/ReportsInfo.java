package com.irplus.dto;

public class ReportsInfo {

	private Integer moduleId; 	
	private String reportName;
	private String reportGenTime;
	private Integer bankReportId;
	
	
	public String getReportGenTime() {
		return reportGenTime;
	}
	public void setReportGenTime(String reportGenTime) {
		this.reportGenTime = reportGenTime;
	}
	public Integer getBankReportId() {
		return bankReportId;
	}
	public void setBankReportId(Integer bankReportId) {
		this.bankReportId = bankReportId;
	}
	public Integer getModuleId() {
		return moduleId;
	}
	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}
	public String getReportName() {
		return reportName;
	}
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	
	
}
