package com.irplus.dto.roles;

import java.util.List;

public class RoleResponseDetails {

	private boolean isValidationSuccess;
    private int statusCode;
    private String statusMsg;
    private List<String> errMsgs;
    private List<RoleBean> roleBean;
	private List<RoleBean> data;
    private int recordsTotal;
    private int recordsFiltered;
    
    
    public List<RoleBean> getData() {
		return data;
	}
	public void setData(List<RoleBean> data) {
		this.data = data;
	}
	public int getRecordsTotal() {
		return recordsTotal;
	}
	public void setRecordsTotal(int recordsTotal) {
		this.recordsTotal = recordsTotal;
	}
	public int getRecordsFiltered() {
		return recordsFiltered;
	}
	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}
  
    public boolean isValidationSuccess() {
		return isValidationSuccess;
	}
	public void setValidationSuccess(boolean isValidationSuccess) {
		this.isValidationSuccess = isValidationSuccess;
	}	
	
	public int getStatusCode() {
		return statusCode;
	}
	
	public List<RoleBean> getRoleBean() {
		return roleBean;
	}
	
	public void setRoleBean(List<RoleBean> roleBean) {
		roleBean = roleBean;
	}
	
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getStatusMsg() {
		return statusMsg;
	}
	
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
	
	public List<String> getErrMsgs() {
		return errMsgs;
	}
	
	public void setErrMsgs(List<String> errMsgs) {
		this.errMsgs = errMsgs;
	}
	
}