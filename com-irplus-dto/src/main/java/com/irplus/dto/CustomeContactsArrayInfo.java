package com.irplus.dto;

public class CustomeContactsArrayInfo {

	CustomerContactsBean[] customerContactsBean;

	public CustomerContactsBean[] getCustomerContactsBean(){
		return customerContactsBean;
	}

	public void setCustomerContactsBean(CustomerContactsBean[] customerContactsBean) {
		this.customerContactsBean = customerContactsBean;
	}
		
}
