package com.irplus.dto;

import com.irplus.dto.menu_modules_association.ModuleMenuBean;

	public class ModuleMenuArrayDTO {
	
	private ModuleMenuBean[] moduleMenuBeans;
	
	public ModuleMenuArrayDTO(ModuleMenuBean[] moduleMenuBeans) {
		super();
		this.moduleMenuBeans = moduleMenuBeans;
	}
	
	public ModuleMenuArrayDTO() {
		super();
	}

	public ModuleMenuBean[] getModuleMenuBeans() {
		return moduleMenuBeans;
	}

	public void setModuleMenuBeans(ModuleMenuBean[] moduleMenuBeans) {
		this.moduleMenuBeans = moduleMenuBeans;
	} 
	
	
}
