package com.irplus.dto.menu_modules_association;

import java.util.Date;

public class ModuleMenuBean {
	
	private int modulemenuid;
	private Integer sortingorder;
	private Date createddate;
	private Date modifieddate;		
	private Integer menuid;   //association of relations
	private Integer moduleid; //association of relations
	private Integer status;
	private Integer userId;
	private String menuName;
	private String moduleName;
	
//	private Set<IrMRolepermissions> irMRolepermissionses = new HashSet<IrMRolepermissions>(0);
	
	private Integer[] moduleIdArray;
	
	public Integer getMenuid() {
		return menuid;
	}

	public void setMenuid(Integer menuid) {
		this.menuid = menuid;
	}

	public Integer getModuleid() {
		return moduleid;
	}

	public void setModuleid(Integer moduleid) {
		this.moduleid = moduleid;
	}

	public ModuleMenuBean() {
	}

	public ModuleMenuBean(int modulemenuid) {
		this.modulemenuid = modulemenuid;
	}
	
	public ModuleMenuBean(int modulemenuid, Integer sortingorder, Date createddate, Date modifieddate, Integer menuid,
			Integer moduleid) {
		super();
		this.modulemenuid = modulemenuid;
		this.sortingorder = sortingorder;
		this.createddate = createddate;
		this.modifieddate = modifieddate;
		this.menuid = menuid;
		this.moduleid = moduleid;
	}

	public int getModulemenuid() {
		return this.modulemenuid;
	}

	public void setModulemenuid(int modulemenuid) {
		this.modulemenuid = modulemenuid;
	}

	public Integer getSortingorder() {
		return this.sortingorder;
	}

	public String getMenuName() {
		return menuName;
	}

	public Integer[] getModuleIdArray() {
		return moduleIdArray;
	}

	public void setModuleIdArray(Integer[] moduleIdArray) {
		this.moduleIdArray = moduleIdArray;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public void setSortingorder(Integer sortingorder) {
		this.sortingorder = sortingorder;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

	@Override
	public String toString() {
		return "ModuleMenuBean [modulemenuid=" + modulemenuid + ", sortingorder=" + sortingorder + ", createddate="
				+ createddate + ", modifieddate=" + modifieddate + ", menuid=" + menuid + ", moduleid=" + moduleid
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((createddate == null) ? 0 : createddate.hashCode());
		result = prime * result + ((menuid == null) ? 0 : menuid.hashCode());
		result = prime * result + ((modifieddate == null) ? 0 : modifieddate.hashCode());
		result = prime * result + ((moduleid == null) ? 0 : moduleid.hashCode());
		result = prime * result + modulemenuid;
		result = prime * result + ((sortingorder == null) ? 0 : sortingorder.hashCode());
		return result;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModuleMenuBean other = (ModuleMenuBean) obj;
		if (createddate == null) {
			if (other.createddate != null)
				return false;
		} else if (!createddate.equals(other.createddate))
			return false;
		if (menuid == null) {
			if (other.menuid != null)
				return false;
		} else if (!menuid.equals(other.menuid))
			return false;
		if (modifieddate == null) {
			if (other.modifieddate != null)
				return false;
		} else if (!modifieddate.equals(other.modifieddate))
			return false;
		if (moduleid == null) {
			if (other.moduleid != null)
				return false;
		} else if (!moduleid.equals(other.moduleid))
			return false;
		if (modulemenuid != other.modulemenuid)
			return false;
		if (sortingorder == null) {
			if (other.sortingorder != null)
				return false;
		} else if (!sortingorder.equals(other.sortingorder))
			return false;
		return true;
	}
}
