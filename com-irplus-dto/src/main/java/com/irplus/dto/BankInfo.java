package com.irplus.dto;

import java.util.Arrays;
import java.util.List;

public class BankInfo {
	
	    private static final long serialVersionUID = -3176074231815769436L;

	    private long bankId;
	    private Long branchId;
	    
	    public String getGroupname() {
			return groupname;
		}
		public void setGroupname(String groupname) {
			this.groupname = groupname;
		}
		private String groupname;
	    private String branchLocation;
	    private String branchOwnCode;
	    private Character isdefault;
	    private int isactive;
	    private int totalBranches;
	    private int totalCustomers;
	  	private int bankStatus;
		private int userId;
	    private String siteId;
	    private String locationId;
	    private String rtNo;
	    private String ddano;
	    private ContactInfo[] contacts;
	    private String bankName;
	    private String bankCode;
	    private String bankLogo;
	    private String branchLogo;
	    private String bankFolder;
	   	private String createddate;
	    private String modifieddate;
	    private String country;
	    private String state;
		private String city;
		private String zipcode;
		private String website;
		private String bplist;
		private String ftypelist;
		   public Object getUserName() {
			return userName;
		}
		public void setUserName(Object userName) {
			this.userName = userName;
		}
		private Object userName;
		private String parentBankName;
		private String branchBankLocationName;
		
		private int totalCustomerscount;
		
		public int getTotalCustomerscount() {
			return totalCustomerscount;
		}
		public void setTotalCustomerscount(int totalCustomerscount) {
			this.totalCustomerscount = totalCustomerscount;
		}
		private List<BankBranchInfo>bankbranchinfo;
		
		public List<BankBranchInfo> getBankbranchinfo() {
			return bankbranchinfo;
		}
		public void setBankbranchinfo(List<BankBranchInfo> bankbranchinfo) {
			this.bankbranchinfo = bankbranchinfo;
		}
		public int getTotalCustomers() {
			return totalCustomers;
		}
		public void setTotalCustomers(int totalCustomers) {
			this.totalCustomers = totalCustomers;
		}
		public int getTotalBranches() {
			return totalBranches;
		}
		public void setTotalBranches(int totalBranches) {
			this.totalBranches = totalBranches;
		}
	    public int getBankStatus() {
			return bankStatus;
		}
		public void setBankStatus(int bankStatus) {
			this.bankStatus = bankStatus;
		}
		
		public String getBankFolder() {
			return bankFolder;
		}
		public void setBankFolder(String bankFolder) {
			this.bankFolder = bankFolder;
		}

		public long getBankId() {
			return bankId;
		}
		
		
		
		
		public int getUserId() {
			return userId;
		}




		public void setUserId(int userId) {
			this.userId = userId;
		}




		public static long getSerialversionuid() {
			return serialVersionUID;
		}




		public void setBankId(long bankId) {
			this.bankId = bankId;
		}
		public Long getBranchId() {
			return branchId;
		}
		public void setBranchId(Long branchId) {
			this.branchId = branchId;
		}
		public String getBranchLocation() {
			return branchLocation;
		}
		public void setBranchLocation(String branchLocation) {
			this.branchLocation = branchLocation;
		}
		public String getBranchOwnCode() {
			return branchOwnCode;
		}
		public void setBranchOwnCode(String branchOwnCode) {
			this.branchOwnCode = branchOwnCode;
		}
		public Character getIsdefault() {
			return isdefault;
		}
		public String getParentBankName() {
			return parentBankName;
		}
		public void setParentBankName(String parentBankName) {
			this.parentBankName = parentBankName;
		}
		public String getBranchBankLocationName() {
			return branchBankLocationName;
		}
		public void setBranchBankLocationName(String branchBankLocationName) {
			this.branchBankLocationName = branchBankLocationName;
		}
		public void setIsdefault(Character isdefault) {
			this.isdefault = isdefault;
		}
		public int getIsactive() {
			return isactive;
		}
		public void setIsactive(int isactive) {
			this.isactive = isactive;
		}
		public String getSiteId() {
			return siteId;
		}
		public void setSiteId(String siteId) {
			this.siteId = siteId;
		}
		public String getLocationId() {
			return locationId;
		}
		public void setLocationId(String locationId) {
			this.locationId = locationId;
		}
		public String getRtNo() {
			return rtNo;
		}
		public void setRtNo(String rtNo) {
			this.rtNo = rtNo;
		}
		public String getDdano() {
			return ddano;
		}
		public void setDdano(String ddano) {
			this.ddano = ddano;
		}
		public ContactInfo[] getContacts() {
			return contacts;
		}
		public void setContacts(ContactInfo[] contacts) {
			this.contacts = contacts;
		}
		public String getBankName() {
			return bankName;
		}
		public void setBankName(String bankName) {
			this.bankName = bankName;
		}
		public String getBankCode() {
			return bankCode;
		}
		public void setBankCode(String bankCode) {
			this.bankCode = bankCode;
		}
		public String getBankLogo() {
			return bankLogo;
		}
		public void setBankLogo(String bankLogo) {
			this.bankLogo = bankLogo;
		}
		public String getCreateddate() {
			return createddate;
		}
		public void setCreateddate(String createddate) {
			this.createddate = createddate;
		}
		public String getModifieddate() {
			return modifieddate;
		}
		public void setModifieddate(String modifieddate) {
			this.modifieddate = modifieddate;
		}
		public String getCountry() {
			return country;
		}
		public void setCountry(String country) {
			this.country = country;
		}
		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public String getZipcode() {
			return zipcode;
		}
		public void setZipcode(String zipcode) {
			this.zipcode = zipcode;
		}
		public String getWebsite() {
			return website;
		}
		public void setWebsite(String website) {
			this.website = website;
		}
		public String getBplist() {
			return bplist;
		}
		public void setBplist(String bplist) {
			this.bplist = bplist;
		}
		public String getFtypelist() {
			return ftypelist;
		}
		public void setFtypelist(String ftypelist) {
			this.ftypelist = ftypelist;
		}
		public String getBranchLogo() {
			return branchLogo;
		}
		public void setBranchLogo(String branchLogo) {
			this.branchLogo = branchLogo;
		}
		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("BankInfo [bankId=").append(bankId).append(", branchId=").append(branchId)
					.append(", branchLocation=").append(branchLocation).append(", branchOwnCode=").append(branchOwnCode)
					.append(", isdefault=").append(isdefault).append(", isactive=").append(isactive).append(", siteId=")
					.append(siteId).append(", locationId=").append(locationId).append(", rtNo=").append(rtNo)
					.append(", ddano=").append(ddano).append(", contacts=").append(Arrays.toString(contacts))
					.append(", bankName=").append(bankName).append(", bankCode=").append(bankCode).append(", bankLogo=")
					.append(bankLogo).append(", createddate=").append(createddate).append(", modifieddate=")
					.append(modifieddate).append(", country=").append(country).append(", state=").append(state)
					.append(", city=").append(city).append(", zipcode=").append(zipcode).append(", bankStatus=").append(bankStatus).append(", website=")
					.append(website).append(", bplist=").append(bplist).append(", totalBranches=").append(totalBranches).append(", ftypelist=").append(ftypelist)
					.append(", branchLogo=").append(branchLogo).append(", bankFolder=").append(bankFolder).append(", userId=").append(userId)
					.append("]");
			return builder.toString();
		}
			
	}