package com.irplus.dto.module;

import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class ModuleBean {	
	
	private Integer moduleid;

	private String modulename;
	private String moduleicon;
	private String description;
	private String modulepath;
	private Integer isreport;
	private Integer userid;
	private Date createddate;
	private Date modifieddate;
	private Integer isactive;
	
	private Set irMModulemenuses = new HashSet(0);
//	private Set irSBankreportings = new HashSet(0);

	private static final long serialVersionUID =1L;
	
	
	
	
	
	
	
	
	
	
	
	/*Comparator for sorting the list by roll no*/
    public static Comparator<ModuleBean> StuRollno = new Comparator<ModuleBean>() {

	public int compare(ModuleBean s1, ModuleBean s2) {

	   int rollno1 = s1.getModuleid();
	   int rollno2 = s2.getModuleid();

	   /*For ascending order*/
	   return rollno1-rollno2;

	   /*For descending order*/
	   //rollno2-rollno1;
   }};
	public ModuleBean() {
	}

	public ModuleBean(//IrMStatus irMStatus, 
			String modulename, String moduleicon, String description,
			String modulepath, Integer isreport, Integer userid, Date createddate, Date modifieddate
	//		Set irMModulemenuses, Set irSBankreportings
			) {
	//	this.irMStatus = irMStatus;
		this.modulename = modulename;
		this.moduleicon = moduleicon;
		this.description = description;
		this.modulepath = modulepath;
		this.isreport = isreport;
		this.userid = userid;
		this.createddate = createddate;
		this.modifieddate = modifieddate;
//		this.irMModulemenuses = irMModulemenuses;
//		this.irSBankreportings = irSBankreportings;
	}

	public Integer getModuleid() {
		return this.moduleid;
	}

	public void setModuleid(Integer moduleid) {
		this.moduleid = moduleid;
	}

	/*
	public IrMStatus getIrMStatus() {
		return this.irMStatus;
	}

	public void setIrMStatus(IrMStatus irMStatus) {
		this.irMStatus = irMStatus;
	}
	*/

	public String getModulename() {
		return this.modulename;
	}

	public void setModulename(String modulename) {
		this.modulename = modulename;
	}

	public String getModuleicon() {
		return this.moduleicon;
	}

	public void setModuleicon(String moduleicon) {
		this.moduleicon = moduleicon;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getModulepath() {
		return this.modulepath;
	}

	public void setModulepath(String modulepath) {
		this.modulepath = modulepath;
	}

	public Integer getIsreport() {
		return this.isreport;
	}

	public void setIsreport(Integer isreport) {
		this.isreport = isreport;
	}

	public Integer getUserid() {
		return this.userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

	public Integer getIsactive() {
		return isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	
	/*@Override
	public String toString() {
		return "ModulesBean [moduleid=" + moduleid + ", modulename=" + modulename + ", moduleicon=" + moduleicon
				+ ", description=" + description + ", modulepath=" + modulepath + ", isreport=" + isreport + ", userid="
				+ userid + ", createddate=" + createddate + ", modifieddate=" + modifieddate + "]";
	}*/

/*	public Set getIrMModulemenuses() {
		return this.irMModulemenuses;
	}

	public void setIrMModulemenuses(Set irMModulemenuses) {
		this.irMModulemenuses = irMModulemenuses;
	}

	public Set getIrSBankreportings() {
		return this.irSBankreportings;
	}

	public void setIrSBankreportings(Set irSBankreportings) {
		this.irSBankreportings = irSBankreportings;
	}
*/
}
