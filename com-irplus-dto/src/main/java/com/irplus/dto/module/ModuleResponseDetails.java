package com.irplus.dto.module;

import java.util.List;

public class ModuleResponseDetails {

	private boolean isValidationSuccess;
/*	private boolean isInputValidationSuccess;
    private boolean isOutPutValidationSuccess;
*/	private int statusCode;
    private String statusMsg;
    private List<String> errMsgs;
    private List<ModuleBean> module;
  	
	private List<ModuleBean> data;
    private int recordsTotal;
    private int recordsFiltered;
    

    
    public int getStatusCode() {
		return statusCode;
	}
	public boolean isValidationSuccess() {
		return isValidationSuccess;
	}
	public void setValidationSuccess(boolean isValidationSuccess) {
		this.isValidationSuccess = isValidationSuccess;
	}
	@Override
	public String toString() {
		return "ModuleResponseDetails [isValidationSuccess=" + isValidationSuccess + ", statusCode=" + statusCode
				+ ", statusMsg=" + statusMsg + ", errMsgs=" + errMsgs + ", module=" + module + "]";
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
	public List<String> getErrMsgs() {
		return errMsgs;
	}
	public void setErrMsgs(List<String> errMsgs) {
		this.errMsgs = errMsgs;
	}
	
	
	public List<ModuleBean> getModule() {
			return module;
		}
	public void setModule(List<ModuleBean> module) {
			this.module = module;
		}
	
	public int getRecordsTotal() {
		return recordsTotal;
	}
	public void setRecordsTotal(int recordsTotal) {
		this.recordsTotal = recordsTotal;
	}
	public List<ModuleBean> getData() {
		return data;
	}
	public void setData(List<ModuleBean> data) {
		this.data = data;
	}
	public int getRecordsFiltered() {
		return recordsFiltered;
	}
	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}
	
	
}