package com.irplus.dto;

import java.util.List;

import com.irplus.dto.customerreporting.CustomerRepotingBean;
import com.irplus.dto.fileprocessing.BatchHeaderRecordBean;
import com.irplus.dto.fileprocessing.FileHeaderRecordBean;
import com.irplus.dto.fileprocessing.WEBEntryDetailRecordBean;
import com.irplus.dto.menu.AddMenu;
import com.irplus.dto.menu.MenuInfo;
import com.irplus.dto.menu_modules_association.ModuleMenuBean;
import com.irplus.dto.module.ModuleBean;
import com.irplus.dto.rolepermission.RolepermissionsBean;
import com.irplus.dto.roles.RoleBean;
import com.irplus.dto.users.UserBean;

public class IRPlusResponseDetails {

	
	private boolean isValidationSuccess;
	private int statusCode;
	private LicenseInfo licenseInfo;
	private MenuInfo menuInfo;
	private List<MenuInfo> userMenus;

	private String statusMsg;
	private List<String> errMsgs;
	private List<BankInfo> banks;
	private BankInfo bank;
	private List<FileAudit> fileLogList;
	public List<FileAudit> getFileLogList() {
		return fileLogList;
	}

	public void setFileLogList(List<FileAudit> fileLogList) {
		this.fileLogList = fileLogList;
	}

	public List<BankInfo> getList() {
		return list;
	}

	public List<CustomerGroupingMngmntInfo> getFiletypeSummary() {
		return FiletypeSummary;
	}



	public void setFiletypeSummary(List<CustomerGroupingMngmntInfo> filetypeSummary) {
		FiletypeSummary = filetypeSummary;
	}

	private List<CustomerGroupingMngmntInfo> FiletypeSummary;

	
	private CustomerGroupingMngmntInfo customerGroupingMngmntResponse;
	public CustomerGroupingMngmntInfo getCustomerGroupingMngmntResponse() {
		return customerGroupingMngmntResponse;
	}

	public void setCustomerGroupingMngmntResponse(CustomerGroupingMngmntInfo customerGroupingMngmntResponse) {
		this.customerGroupingMngmntResponse = customerGroupingMngmntResponse;
	}

	public void setList(List<BankInfo> list) {
		this.list = list;
	}

	private List<BankInfo> list;
	private SiteInfo siteInfo;

	private IRMasterDataDao masterData;
	
	private LocationViewInfo bankBranches;
	
	private List<BankwiseFileprocess> bankWiseFileProcess;
	
	public List<BankwiseFileprocess> getBankWiseFileProcess() {
		return bankWiseFileProcess;
	}



	public void setBankWiseFileProcess(List<BankwiseFileprocess> bankWiseFileProcess) {
		this.bankWiseFileProcess = bankWiseFileProcess;
	}



	public LocationViewInfo getBankBranches() {
		return bankBranches;
	}
	


	public void setBankBranches(LocationViewInfo bankBranches) {
		this.bankBranches = bankBranches;
	}

	//module bean
	private List<ModuleBean> module;
	private ModuleBean moduleBean;
	private int recordsTotal;
	private int recordsFiltered;
	private String logoPath;

	private UserBean userBean;
	
//	added 28 11 17
	
	private UserBean oneUserBean;	
	
	private List<BankViewInfo> bankViewInfo;

	private List<LocationViewInfo> locations;

	//rolepermissions

	private List<RolepermissionsBean> rolePermissonsBean;

	private RoleBean roleBean;
	private List<FieldTypesInfo> fieldsInfo;
	
	private List<RoleBean> roleBeans;

	private List<UserBean> userBeans;

	private List<AddMenu> menus;
	
	private AddMenu menu;

	private List<ModuleMenuBean> moduleMenuBean;
	private List<CustomerBean> customerBean;
	private List<CustomerContactsBean> customerContactsBean;

	private List<ReportFileFormatInfo> reportFileFormatInfos;
	private List<CustomerLicenceInfo> customerLicenceInfo;
	private List<CustomerReportLicenceInfo> customerReportLicenceInfos;
	
	private UserFilterInfo userFilterInfo;
	

	
	private List<UserBean> listUserFilter;
	
	private ParentBank parentBank;
	 private BankBranchInfo branchbankinfo;
	 
	public BankBranchInfo getBranchbankinfo() {
		return branchbankinfo;
	}

	public void setBranchbankinfo(BankBranchInfo branchbankinfo) {
		this.branchbankinfo = branchbankinfo;
	}

	// 11 23 17	
	private CustomerFunctionInfo customerFunctionInfo;	

	// 1 12 17	
	private List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList;
	
	private CustomerGroupingMngmntInfo oneCustmrGrp;
	// 2 12 17
	
	private BankIdCustmrIdArraysInfo bankIdCustmrIdArrays ;
	// 4 12 17	
	private CustomerMappingInfo customerMappingInfo;
	
	private List<CustomerMappingInfo> customerMpngInfoList;
	
	// 6 12 17
	private BankReportingsInfo  bankReportingsInfo;
	
	// 7 12 17	
	private List<CustomerRepotingBean> ListcustomerRepotingBean;
	
	// 8 12 17	
	private List<BankBranchInfo> bankBranchList;
	
	// 12 14 17 
	private CustomeContactsArrayInfo customeContactsArrayInfo;
	
	// 12 15 17
	private ModuleMenuArrayDTO moduleMenuArrayDTO;
	
	// 12 18 17
	private List<BusinessProcessInfo> businessProcessList;
	
	// 12 18 17
	private List<FileTypeInfo> fileTypeInfoList;
	
	// 12 21 17
	
	private List<FieldTypesInfo> listFieldTypes ;
	private FormsManagementDTO   formsManagementDTO;
	
	//12 22 17
	private List<FormsManagementDTO> formMgntList ;
 	private CustomerBean customer;	
 	
 	public CustomerBean getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerBean customer) {
		this.customer = customer;
	}

	// 12 22 17
 	private List<DBDTO> DbdtoList;	
 	
	
 	//FileProcessing all processed file list
 	private List<FileHeaderRecordBean> allFileList;
	private List<BatchHeaderRecordBean> bhrBean;
	private FileHeaderRecordBean fhrBean;
	
	private WEBEntryDetailRecordBean entryDetailRecordBean;
 	
 	private Dashboard dashboardResponse;
	
	
	
	
	
	
	
	
	
 	
	public Dashboard getDashboardResponse() {
		return dashboardResponse;
	}

	public void setDashboardResponse(Dashboard dashboardResponse) {
		this.dashboardResponse = dashboardResponse;
	}

	public WEBEntryDetailRecordBean getEntryDetailRecordBean() {
		return entryDetailRecordBean;
	}

	public void setEntryDetailRecordBean(WEBEntryDetailRecordBean entryDetailRecordBean) {
		this.entryDetailRecordBean = entryDetailRecordBean;
	}

	public List<BatchHeaderRecordBean> getBhrBean() {
		return bhrBean;
	}

	public void setBhrBean(List<BatchHeaderRecordBean> bhrBean) {
		this.bhrBean = bhrBean;
	}

	public FileHeaderRecordBean getFhrBean() {
		return fhrBean;
	}

	public void setFhrBean(FileHeaderRecordBean fhrBean) {
		this.fhrBean = fhrBean;
	}


	
	

	public List<FileHeaderRecordBean> getAllFileList() {
		return allFileList;
	}

	public void setAllFileList(List<FileHeaderRecordBean> allFileList) {
		this.allFileList = allFileList;
	}

	public boolean isValidationSuccess(){
		return isValidationSuccess;
	}

	public void setValidationSuccess(boolean isValidationSuccess) {
		this.isValidationSuccess = isValidationSuccess;
	}

	public FormsManagementDTO getFormsManagementDTO() {
		return formsManagementDTO;
	}

	public void setFormsManagementDTO(FormsManagementDTO formsManagementDTO) {
		this.formsManagementDTO = formsManagementDTO;
	}

	public List<DBDTO> getDbdtoList() {
		return DbdtoList;
	}

	public void setDbdtoList(List<DBDTO> dbdtoList) {
		DbdtoList = dbdtoList;
	}

	public BankIdCustmrIdArraysInfo getBankIdCustmrIdArrays() {
		return bankIdCustmrIdArrays;
	}

	public List<FormsManagementDTO> getFormMgntList() {
		return formMgntList;
	}

	public void setFormMgntList(List<FormsManagementDTO> formMgntList) {
		this.formMgntList = formMgntList;
	}

	public void setBankIdCustmrIdArrays(BankIdCustmrIdArraysInfo bankIdCustmrIdArrays) {
		this.bankIdCustmrIdArrays = bankIdCustmrIdArrays;
	}

	public CustomeContactsArrayInfo getCustomeContactsArrayInfo() {
		return customeContactsArrayInfo;
	}

	public void setCustomeContactsArrayInfo(CustomeContactsArrayInfo customeContactsArrayInfo) {
		this.customeContactsArrayInfo = customeContactsArrayInfo;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public List<FileTypeInfo> getFileTypeInfoList() {
		return fileTypeInfoList;
	}

	public void setFileTypeInfoList(List<FileTypeInfo> fileTypeInfoList) {
		this.fileTypeInfoList = fileTypeInfoList;
	}

	public ModuleMenuArrayDTO getModuleMenuArrayDTO() {
		return moduleMenuArrayDTO;
	}

	public void setModuleMenuArrayDTO(ModuleMenuArrayDTO moduleMenuArrayDTO) {
		this.moduleMenuArrayDTO = moduleMenuArrayDTO;
	}

	public BankReportingsInfo getBankReportingsInfo() {
		return bankReportingsInfo;
	}

	public void setBankReportingsInfo(BankReportingsInfo bankReportingsInfo) {
		this.bankReportingsInfo = bankReportingsInfo;
	}

	public void setStatusCode(int statusCode){
		this.statusCode = statusCode;
	}

	public CustomerMappingInfo getCustomerMappingInfo() {
		return customerMappingInfo;
	}

	public List<CustomerRepotingBean> getListcustomerRepotingBean() {
		return ListcustomerRepotingBean;
	}

	public void setListcustomerRepotingBean(List<CustomerRepotingBean> listcustomerRepotingBean) {
		ListcustomerRepotingBean = listcustomerRepotingBean;
	}

	public List<CustomerMappingInfo> getCustomerMpngInfoList() {
		return customerMpngInfoList;
	}

	public void setCustomerMpngInfoList(List<CustomerMappingInfo> customerMpngInfoList) {
		this.customerMpngInfoList = customerMpngInfoList;
	}

	public List<BusinessProcessInfo> getBusinessProcessList() {
		return businessProcessList;
	}

	public List<FieldTypesInfo> getListFieldTypes() {
		return listFieldTypes;
	}

	public void setListFieldTypes(List<FieldTypesInfo> listFieldTypes) {
		this.listFieldTypes = listFieldTypes;
	}

	public void setBusinessProcessList(List<BusinessProcessInfo> businessProcessList) {
		this.businessProcessList = businessProcessList;
	}

	public List<BankBranchInfo> getBankBranchList() {
		return bankBranchList;
	}

	public void setBankBranchList(List<BankBranchInfo> bankBranchList) {
		this.bankBranchList = bankBranchList;
	}

	public void setCustomerMappingInfo(CustomerMappingInfo customerMappingInfo) {
		this.customerMappingInfo = customerMappingInfo;
	}

	public List<CustomerGroupingMngmntInfo> getCustmrGrpngMngmntInfoList() {
		return custmrGrpngMngmntInfoList;
	}

	public void setCustmrGrpngMngmntInfoList(List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList) {
		this.custmrGrpngMngmntInfoList = custmrGrpngMngmntInfoList;
	}

	public CustomerGroupingMngmntInfo getOneCustmrGrp() {
		return oneCustmrGrp;
	}

	public void setOneCustmrGrp(CustomerGroupingMngmntInfo oneCustmrGrp) {
		this.oneCustmrGrp = oneCustmrGrp;
	}

	public LicenseInfo getLicenseInfo() {
		return licenseInfo;
	}

	public void setLicenseInfo(LicenseInfo licenseInfo) {
		this.licenseInfo = licenseInfo;
	}

	public MenuInfo getMenuInfo() {
		return menuInfo;
	}

	public void setMenuInfo(MenuInfo menuInfo) {
		this.menuInfo = menuInfo;
	}

	public List<MenuInfo> getUserMenus() {
		return userMenus;
	}

	public void setUserMenus(List<MenuInfo> userMenus) {
		this.userMenus = userMenus;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	public CustomerFunctionInfo getCustomerFunctionInfo() {
		return customerFunctionInfo;
	}

	public void setCustomerFunctionInfo(CustomerFunctionInfo customerFunctionInfo) {
		this.customerFunctionInfo = customerFunctionInfo;
	}

	public List<String> getErrMsgs() {
		return errMsgs;
	}

	public UserBean getOneUserBean() {
		return oneUserBean;
	}

	public void setOneUserBean(UserBean oneUserBean) {
		this.oneUserBean = oneUserBean;
	}

	public void setErrMsgs(List<String> errMsgs) {
		this.errMsgs = errMsgs;
	}

	public List<BankInfo> getBanks() {
		return banks;
	}

	public void setBanks(List<BankInfo> banks) {
		this.banks = banks;
	}

	public BankInfo getBank() {
		return bank;
	}

	public void setBank(BankInfo bank) {
		this.bank = bank;
	}

	public IRMasterDataDao getMasterData() {
		return masterData;
	}

	public void setMasterData(IRMasterDataDao masterData) {
		this.masterData = masterData;
	}

	public List<ModuleBean> getModule() {
		return module;
	}

	public void setModule(List<ModuleBean> module) {
		this.module = module;
	}

	public int getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(int recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public int getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public String getLogoPath() {
		return logoPath;
	}

	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}

	public UserBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserBean userBean) {
		this.userBean = userBean;
	}

	public List<BankViewInfo> getBankViewInfo() {
		return bankViewInfo;
	}

	public void setBankViewInfo(List<BankViewInfo> bankViewInfo) {
		this.bankViewInfo = bankViewInfo;
	}

	public List<LocationViewInfo> getLocations() {
		return locations;
	}

	public void setLocations(List<LocationViewInfo> locations) {
		this.locations = locations;
	}

	public List<RolepermissionsBean> getRolePermissonsBean() {
		return rolePermissonsBean;
	}

	public void setRolePermissonsBean(List<RolepermissionsBean> rolePermissonsBean) {
		this.rolePermissonsBean = rolePermissonsBean;
	}

	public List<FieldTypesInfo> getFieldsInfo() {
		return fieldsInfo;
	}

	public void setFieldsInfo(List<FieldTypesInfo> fieldsInfo) {
		this.fieldsInfo = fieldsInfo;
	}

	public List<RoleBean> getRoleBeans() {
		return roleBeans;
	}

	public void setRoleBeans(List<RoleBean> roleBeans) {
		this.roleBeans = roleBeans;
	}

	public List<UserBean> getUserBeans() {
		return userBeans;
	}

	public void setUserBeans(List<UserBean> userBeans) {
		this.userBeans = userBeans;
	}

	public List<AddMenu> getMenus() {
		return menus;
	}

	public void setMenus(List<AddMenu> menus) {
		this.menus = menus;
	}

	public List<ModuleMenuBean> getModuleMenuBean() {
		return moduleMenuBean;
	}

	public void setModuleMenuBean(List<ModuleMenuBean> moduleMenuBean) {
		this.moduleMenuBean = moduleMenuBean;
	}

	public List<CustomerBean> getCustomerBean() {
		return customerBean;
	}

	public void setCustomerBean(List<CustomerBean> customerBean) {
		this.customerBean = customerBean;
	}

	public List<CustomerContactsBean> getCustomerContactsBean() {
		return customerContactsBean;
	}

	public void setCustomerContactsBean(List<CustomerContactsBean> customerContactsBean) {
		this.customerContactsBean = customerContactsBean;
	}

	public List<ReportFileFormatInfo> getReportFileFormatInfos() {
		return reportFileFormatInfos;
	}

	public void setReportFileFormatInfos(List<ReportFileFormatInfo> reportFileFormatInfos) {
		this.reportFileFormatInfos = reportFileFormatInfos;
	}

	public List<CustomerLicenceInfo> getCustomerLicenceInfo() {
		return customerLicenceInfo;
	}

	public void setCustomerLicenceInfo(List<CustomerLicenceInfo> customerLicenceInfo) {
		this.customerLicenceInfo = customerLicenceInfo;
	}

	public List<CustomerReportLicenceInfo> getCustomerReportLicenceInfos() {
		return customerReportLicenceInfos;
	}

	public void setCustomerReportLicenceInfos(List<CustomerReportLicenceInfo> customerReportLicenceInfos) {
		this.customerReportLicenceInfos = customerReportLicenceInfos;
	}

	public UserFilterInfo getUserFilterInfo() {
		return userFilterInfo;
	}

	public void setUserFilterInfo(UserFilterInfo userFilterInfo) {
		this.userFilterInfo = userFilterInfo;
	}

	public List<UserBean> getListUserFilter() {
		return listUserFilter;
	}

	public void setListUserFilter(List<UserBean> listUserFilter) {
		this.listUserFilter = listUserFilter;
	}

	public ParentBank getParentBank() {
		return parentBank;
	}

	public void setParentBank(ParentBank parentBank) {
		this.parentBank = parentBank;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IRPlusResponseDetails [isValidationSuccess=").append(isValidationSuccess)
				.append(", statusCode=").append(statusCode).append(", licenseInfo=").append(licenseInfo).append(", siteInfo=").append(siteInfo)
				.append(", menuInfo=").append(menuInfo).append(", userMenus=").append(userMenus).append(", statusMsg=")
				.append(statusMsg).append(", errMsgs=").append(errMsgs).append(", banks=").append(banks)
				.append(", bank=").append(bank).append(", masterData=").append(masterData).append(", module=")
				.append(module).append(", recordsTotal=").append(recordsTotal).append(", recordsFiltered=")
				.append(recordsFiltered).append(", logoPath=").append(logoPath).append(", userBean=").append(userBean)
				.append(", bankViewInfo=").append(bankViewInfo).append(", locations=").append(locations)
				.append(", rolePermissonsBean=").append(rolePermissonsBean).append(", roleBean=").append(getRoleBean())
				.append(", fieldsInfo=").append(fieldsInfo).append(", roleBeans=").append(roleBeans).append(", customer=").append(customer)
				.append(", userBeans=").append(userBeans).append(", menus=").append(menus).append(", moduleMenuBean=")
				.append(moduleMenuBean).append(", customerBean=").append(customerBean).append(", customerContactsBean=")
				.append(customerContactsBean).append(", reportFileFormatInfos=").append(reportFileFormatInfos)
				.append(", customerLicenceInfo=").append(customerLicenceInfo).append(", customerReportLicenceInfos=")
				.append(customerReportLicenceInfos).append(", userFilterInfo=").append(userFilterInfo).append(", branchbankinfo=").append(branchbankinfo)
				.append(", listUserFilter=").append(listUserFilter).append(", parentBank=").append(parentBank)
				.append(", allFileList=").append(allFileList).append(", dashboardResponse=").append(dashboardResponse)
				.append(",fileLogList=").append(fileLogList).append(", fhrBean=").append(fhrBean).append(", bankWiseFileProcess=").append(bankWiseFileProcess).append(", bhrBean=").append(bhrBean).append(", entryDetailRecordBean=").append(entryDetailRecordBean)
				.append("]");
		return builder.toString();
	}

	
	
	
	
	public SiteInfo getSiteInfo() {
		return siteInfo;
	}

	public void setSiteInfo(SiteInfo siteInfo) {
		this.siteInfo = siteInfo;
	}

	public AddMenu getMenu() {
		return menu;  
	}

	public void setMenu(AddMenu menu) {
		this.menu = menu;
	} 

	public RoleBean getRoleBean() {
		return roleBean;
	}

	public void setRoleBean(RoleBean roleBean) {
		this.roleBean = roleBean;
	}

	public ModuleBean getModuleBean() {
		return moduleBean;
	}

	public void setModuleBean(ModuleBean moduleBean) {
		this.moduleBean = moduleBean;
	}

	public List<CustomerGroupingMngmntInfo> getFileListDetails() {
		return FileListDetails;
	}



	public void setFileListDetails(List<CustomerGroupingMngmntInfo> fileListDetails) {
		FileListDetails = fileListDetails;
	}

	private List<CustomerGroupingMngmntInfo> FileListDetails;
}