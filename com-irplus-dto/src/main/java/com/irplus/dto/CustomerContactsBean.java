package com.irplus.dto;

import java.util.Date;

public class CustomerContactsBean {
	private static final long serialVersionUID = -3176074231815769436L;

	private Integer customerContactId;
	private Integer usersId;
	
	private Integer customerId;
	
	private String firstName;
	private String lastName;
	private String contactRole;
	private String contactNo;
	private String emailId;
	private String address1;
	private String address2;
	private String country;
	private String state;
	private String city;
	private String zipcode;
	private Character isdefault;
	
	private Integer isactive;
	
	private Date createddate;
	private Date modifieddate;

	public CustomerContactsBean() {
	}

	public CustomerContactsBean(int customerContactId) {
		this.customerContactId = customerContactId;
	}

	public CustomerContactsBean(int customerContactId, Integer usersId, Integer customerId, String firstName,
			String lastName, String contactRole, String contactNo, String emailId,
			String address1, String address2, String country, String state, String city,
			String zipcode, Character isdefault, Integer isactive, Date createddate, Date modifieddate) {
		super();
		this.customerContactId = customerContactId;
		this.usersId = usersId;
		this.customerId = customerId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.contactRole = contactRole;
		this.contactNo = contactNo;
		this.emailId = emailId;
		this.address1 = address1;
		this.address2 = address2;
		this.country = country;
		this.state = state;
		this.city = city;
		this.zipcode = zipcode;
		this.isdefault = isdefault;
		this.isactive = isactive;
		this.createddate = createddate;
		this.modifieddate = modifieddate;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Integer getCustomerContactId() {
		return customerContactId;
	}

	public void setCustomerContactId(Integer customerContactId) {
		this.customerContactId = customerContactId;
	}

	public Integer getUsersId() {
		return usersId;
	}

	public void setUsersId(Integer usersId) {
		this.usersId = usersId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getContactRole() {
		return this.contactRole;
	}

	public void setContactRole(String contactRole) {
		this.contactRole = contactRole;
	}

	public String getContactNo() {
		return this.contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getEmailId() {
		return this.emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipcode() {
		return this.zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public Character getIsdefault() {
		return this.isdefault;
	}

	public void setIsdefault(Character isdefault){
		this.isdefault = isdefault;
	}

	public Integer getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CustomerContactsBean [customerContactId=").append(customerContactId).append(", customerId=").append(customerId)
				.append(", firstName=").append(firstName).append(", lastName=").append(lastName)
				.append(", contactRole=").append(contactRole).append(", contactNo=").append(contactNo)
				.append(", emailId=").append(emailId).append(", address1=").append(address1).append(", address2=")
				.append(address2).append(", isdefault=").append(isdefault).append(", country=").append(country)
				.append(", state=").append(state).append(", city=").append(city).append(", zipcode=").append(zipcode)
				.append(", isactive=").append(isactive).append("]");
		return builder.toString();
	}
}