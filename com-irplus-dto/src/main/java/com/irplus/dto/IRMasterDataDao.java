package com.irplus.dto;

import java.util.List;

import com.irplus.dto.menu.AddMenu;
import com.irplus.dto.menu_modules_association.ModuleMenuBean;
import com.irplus.dto.module.ModuleBean;
import com.irplus.dto.rolepermission.RolepermissionsBean;
import com.irplus.dto.roles.RoleBean;
import com.irplus.dto.users.UserBean;

public class IRMasterDataDao {

	private boolean isValidationSuccess;
   
    //rolepermissions
    
    private List<BusinessProcessInfo> busPros;
    
    private List<Sequence> bankFolderSequence;

    //private List<ModuleBean> data;
    private List<FileTypeInfo> LockfileTypesInfo;
    public List<FileTypeInfo> getLockfileTypesInfo() {
		return LockfileTypesInfo;
	}

	public void setLockfileTypesInfo(List<FileTypeInfo> lockfileTypesInfo) {
		LockfileTypesInfo = lockfileTypesInfo;
	}

	public List<FileTypeInfo> getFileTypesInfo() {
		return fileTypesInfo;
	}

	public void setFileTypesInfo(List<FileTypeInfo> fileTypesInfo) {
		this.fileTypesInfo = fileTypesInfo;
	}

	private List<FileTypeInfo> fileTypesInfo;
	private List<FileTypeInfo> fileTypes;

	public boolean isValidationSuccess() {
		return isValidationSuccess;
	}

	public void setValidationSuccess(boolean isValidationSuccess) {
		this.isValidationSuccess = isValidationSuccess;
	}

	public List<BusinessProcessInfo> getBusPros() {
		return busPros;
	}

	public void setBusPros(List<BusinessProcessInfo> busPros) {
		this.busPros = busPros;
	}

	public List<FileTypeInfo> getFileTypes() {
		return fileTypes;
	}

	public void setFileTypes(List<FileTypeInfo> fileTypes) {
		this.fileTypes = fileTypes;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IRMasterDataDao [isValidationSuccess=").append(isValidationSuccess).append(", busPros=")
				.append(busPros).append(", fileTypes=").append(fileTypes).append(", bankFolderSequence=").append(bankFolderSequence).append("]");
		return builder.toString();
	}

	public List<Sequence> getBankFolderSequence() {
		return bankFolderSequence;
	}

	public void setBankFolderSequence(List<Sequence> bankFolderSequence) {
		this.bankFolderSequence = bankFolderSequence;
	}

	
}
