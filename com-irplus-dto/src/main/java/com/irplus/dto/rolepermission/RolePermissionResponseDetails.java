package com.irplus.dto.rolepermission;

import java.util.List;

public class RolePermissionResponseDetails {

	private boolean isValidationSuccess;
/*	private boolean isInputValidationSuccess;
    private boolean isOutPutValidationSuccess;
*/	private int statusCode;
    private String statusMsg;
    private List<String> errMsgs;
    private List<RolepermissionsBean> rolePermissionBean;
    
    
	public int getStatusCode() {
		return statusCode;
	}
	public boolean isValidationSuccess() {
		return isValidationSuccess;
	}
	public void setValidationSuccess(boolean isValidationSuccess) {
		this.isValidationSuccess = isValidationSuccess;
	}
	
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
	public List<String> getErrMsgs() {
		return errMsgs;
	}
	public void setErrMsgs(List<String> errMsgs) {
		this.errMsgs = errMsgs;
	}
	public List<RolepermissionsBean> getRolePermissionBean() {
		return rolePermissionBean;
	}
	public void setRolePermissionBean(List<RolepermissionsBean> rolePermissionBean) {
		this.rolePermissionBean = rolePermissionBean;
	}
	
}