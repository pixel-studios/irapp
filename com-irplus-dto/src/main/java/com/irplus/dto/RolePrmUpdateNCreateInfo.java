package com.irplus.dto;

import com.irplus.dto.rolepermission.RolepermissionsBean;

public class RolePrmUpdateNCreateInfo {

	private Integer rolesId;
	private RolepermissionsBean[] roleprmArray;

	//	private Integer userId;
	
	public Integer getRolesId() {
		return rolesId;
	}
	public void setRolesId(Integer rolesId) {
		this.rolesId = rolesId;
	}
	public RolepermissionsBean[] getRoleprmArray() {
		return roleprmArray;
	}
	public void setRoleprmArray(RolepermissionsBean[] roleprmArray) {
		this.roleprmArray = roleprmArray;
	}
		
}
