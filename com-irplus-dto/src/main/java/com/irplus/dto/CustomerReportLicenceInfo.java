package com.irplus.dto;

import java.sql.Timestamp;
import java.util.Date;

public class CustomerReportLicenceInfo {
	
	private Integer reportLicensingId;
	private Integer irMReportfileFormatsId;
	private Integer irMUsersId;
	private Long irSBankbranchId;
	private Integer irSCustomersId;
	private Integer isactive;
	private Date createddate;
	private Date modifieddate;

	private  Timestamp createddat;
	private Timestamp modifieddat;

	
	public CustomerReportLicenceInfo() {
	}

	public CustomerReportLicenceInfo(Integer irMReportfileFormats, Integer irMUsers,
			Long irSBankbranch, Integer irSCustomers, Integer isactive, Date createddate,
			Date modifieddate) {
		this.irMReportfileFormatsId = irMReportfileFormats;
		this.irMUsersId = irMUsers;
		this.irSBankbranchId = irSBankbranch;
		this.irSCustomersId = irSCustomers;
		this.isactive = isactive;
		this.createddate = createddate;
		this.modifieddate = modifieddate;
	}

	public Integer getReportLicensingId() {
		return this.reportLicensingId;
	}

	public void setReportLicensingId(Integer reportLicensingId) {
		this.reportLicensingId = reportLicensingId;
	}

	public Integer getIrMReportfileFormatsId() {
		return irMReportfileFormatsId;
	}

	public void setIrMReportfileFormatsId(Integer irMReportfileFormatsId) {
		this.irMReportfileFormatsId = irMReportfileFormatsId;
	}

	public Integer getIrMUsersId() {
		return irMUsersId;
	}

	public void setIrMUsersId(Integer irMUsersId) {
		this.irMUsersId = irMUsersId;
	}

	public Long getIrSBankbranchId() {
		return irSBankbranchId;
	}

	public void setIrSBankbranchId(Long irSBankbranchId) {
		this.irSBankbranchId = irSBankbranchId;
	}

	public Integer getIrSCustomersId() {
		return irSCustomersId;
	}

	public void setIrSCustomersId(Integer irSCustomersId) {
		this.irSCustomersId = irSCustomersId;
	}

	public Integer getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public Timestamp getCreateddat() {
		return createddat;
	}

	public void setCreateddat(Timestamp createddat) {
		this.createddat = createddat;
	}

	public Timestamp getModifieddat() {
		return modifieddat;
	}

	public void setModifieddat(Timestamp modifieddat) {
		this.modifieddat = modifieddat;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

}
