package com.irplus.dto;

public class BankFilterInfo {

	private static final long serialVersionUID = -3196074231815769436L;

	private String siteId;
	
	private Long bankId;

	private String bankName;

	private String licType;

	private String businessProcId;

	private String fileTypeId;

	private Integer isactive;
	
	private String locationId;
	
	private String branchLocation;
	
	private Integer branchStatus;
	
	private Integer userId;
	
	private String customerName;
	
	private String customerCode;
	
	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getBranchLocation() {
		return branchLocation;
	}

	public void setBranchLocation(String branchLocation) {
		this.branchLocation = branchLocation;
	}

	
	
	public BankFilterInfo()
	{
		
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getLicType() {
		return licType;
	}

	public void setLicType(String licType) {
		this.licType = licType;
	}

	public String getBusinessProcId() {
		return businessProcId;
	}

	public void setBusinessProcId(String businessProcId) {
		this.businessProcId = businessProcId;
	}

	public String getFileTypeId() {
		return fileTypeId;
	}

	public void setFileTypeId(String fileTypeId) {
		this.fileTypeId = fileTypeId;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public Integer getIsactive() {
		return isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	
	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public Integer getBranchStatus() {
		return branchStatus;
	}

	public void setBranchStatus(Integer branchStatus) {
		this.branchStatus = branchStatus;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BankFilterInfo [siteId=").append(siteId).append(", bankName=").append(bankName)
				.append(", licType=").append(licType).append(", businessProcId=").append(businessProcId)
				.append(", fileTypeId=").append(fileTypeId).append(", isactive=").append(isactive)
				.append(", locationId=").append(locationId).append(", branchLocation=").append(branchLocation)
				.append(", branchStatus=").append(branchStatus).append("]");
		return builder.toString();
	}

	public Long getBankId() {
		return bankId;
	}

	public void setBankId(Long bankId) {
		this.bankId = bankId;
	}

	

}
