package com.irplus.dto;

import java.util.List;

public class LocationViewInfo {
	
	    private static final long serialVersionUID = -3176074231815769436L;

	    private long bankId;
	    private Long branchId;
	    private Character isdefault;
	    private int isactive;
	    private String siteId;
	    private String bankName;
	    private String bankLogo;
	    private String branchLogo;
	    private int totalBranch;
	    private int totalCustomers;
		
		public int getTotalCustomers() {
			return totalCustomers;
		}
		public void setTotalCustomers(int totalCustomers) {
			this.totalCustomers = totalCustomers;
		}
		private String branchLocation;
	    private String branchOwnCode;
	    private String locationId;
	    private String createddate;
	    private String modifieddate;
	    private String firstname;
	    private String email;
		private String contactno;
		private String city;
		private String state;
		
		private List<BankBranchInfo> bankBranchList;
		private List<CustomerGroupingMngmntInfo> bankCustomerList;
		
		public List<CustomerGroupingMngmntInfo> getBankCustomerList() {
			return bankCustomerList;
		}
		public void setBankCustomerList(List<CustomerGroupingMngmntInfo> bankCustomerList) {
			this.bankCustomerList = bankCustomerList;
		}
		public List<BankBranchInfo> getBankBranchList() {
			return bankBranchList;
		}
		public void setBankBranchList(List<BankBranchInfo> bankBranchList) {
			this.bankBranchList = bankBranchList;
		}
		private List<BusinessProcessInfo> bankBusinessProcess;
		
		private List<FileTypeInfo> bankFileTypeInfo;
		
		
		public int getTotalBranch() {
			return totalBranch;
		}
		public void setTotalBranch(int totalBranch) {
			this.totalBranch = totalBranch;
		}
		
		
		public static long getSerialversionuid() {
			return serialVersionUID;
		}
		
		
		public List<BusinessProcessInfo> getBankBusinessProcess() {
			return bankBusinessProcess;
		}
		public void setBankBusinessProcess(List<BusinessProcessInfo> bankBusinessProcess) {
			this.bankBusinessProcess = bankBusinessProcess;
		}
		public List<FileTypeInfo> getBankFileTypeInfo() {
			return bankFileTypeInfo;
		}
		public void setBankFileTypeInfo(List<FileTypeInfo> bankFileTypeInfo) {
			this.bankFileTypeInfo = bankFileTypeInfo;
		}
		public long getBankId() {
			return bankId;
		}
		public void setBankId(long bankId) {
			this.bankId = bankId;
		}
		public Character getIsdefault() {
			return isdefault;
		}
		public void setIsdefault(Character isdefault) {
			this.isdefault = isdefault;
		}
		public int getIsactive() {
			return isactive;
		}
		public void setIsactive(int isactive) {
			this.isactive = isactive;
		}
		public String getSiteId() {
			return siteId;
		}
		public void setSiteId(String siteId) {
			this.siteId = siteId;
		}
		public String getBankName() {
			return bankName;
		}
		public void setBankName(String bankName) {
			this.bankName = bankName;
		}
		public String getBankLogo() {
			return bankLogo;
		}
		public void setBankLogo(String bankLogo) {
			this.bankLogo = bankLogo;
		}
		public String getBranchLocation() {
			return branchLocation;
		}
		public void setBranchLocation(String branchLocation) {
			this.branchLocation = branchLocation;
		}
		public String getBranchOwnCode() {
			return branchOwnCode;
		}
		public void setBranchOwnCode(String branchOwnCode) {
			this.branchOwnCode = branchOwnCode;
		}
		public String getLocationId() {
			return locationId;
		}
		public void setLocationId(String locationId) {
			this.locationId = locationId;
		}
		public String getCreateddate() {
			return createddate;
		}
		public void setCreateddate(String createddate) {
			this.createddate = createddate;
		}
		public String getModifieddate() {
			return modifieddate;
		}
		public void setModifieddate(String modifieddate) {
			this.modifieddate = modifieddate;
		}
		public String getFirstname() {
			return firstname;
		}
		public void setFirstname(String firstname) {
			this.firstname = firstname;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getContactno() {
			return contactno;
		}
		public void setContactno(String contactno) {
			this.contactno = contactno;
		}
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}
		 public String getBranchLogo() {
				return branchLogo;
			}
			public void setBranchLogo(String branchLogo) {
				this.branchLogo = branchLogo;
			}
		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("LocationViewInfo [bankId=").append(bankId).append(", isdefault=").append(isdefault)
					.append(", isactive=").append(isactive).append(", siteId=").append(siteId).append(", bankName=")
					.append(bankName).append(", bankLogo=").append(bankLogo).append(", branchLocation=")
					.append(branchLocation).append(", branchOwnCode=").append(branchOwnCode).append(", locationId=")
					.append(locationId).append(", createddate=").append(createddate).append(", modifieddate=")
					.append(modifieddate).append(", firstname=").append(firstname).append(", email=").append(email)
					.append(", contactno=").append(contactno).append(", bankCustomerList=").append(bankCustomerList).append(", branchLogo=").append(branchLogo).append(", city=").append(city).append(", state=")
					.append(state).append("]");
			return builder.toString();
		}
		public Long getBranchId() {
			return branchId;
		}
		public void setBranchId(Long branchId) {
			this.branchId = branchId;
		}
		
			    
	}



