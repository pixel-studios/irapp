package com.irplus.core.client.dbinfo;

import com.irplus.dto.DBDTO;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

public interface IDBPropertiesCoreClient {
	
	public IRPlusResponseDetails createDBProperties(DBDTO dbdto) throws BusinessException;
		
	public IRPlusResponseDetails updateDBProperties(DBDTO dbdto) throws BusinessException;

	public IRPlusResponseDetails showDBProperties() throws BusinessException;
	
	public IRPlusResponseDetails deleteDBProperties(DBDTO dbdto) throws BusinessException;
}
