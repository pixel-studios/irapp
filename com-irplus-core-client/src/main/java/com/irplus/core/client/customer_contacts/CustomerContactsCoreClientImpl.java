package com.irplus.core.client.customer_contacts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.core.customer_contacts.ICustomerContactsCore;
import com.irplus.dao.client.customercontacts.ICustomerContactsDaoClient;
import com.irplus.dto.CustomeContactsArrayInfo;
import com.irplus.dto.CustomerBean;
import com.irplus.dto.CustomerContactsBean;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

@Component
public class CustomerContactsCoreClientImpl implements ICustomerContactsCoreClient {

	@Autowired
	ICustomerContactsDaoClient iCustomerContactsDao;
	
	@Override
	public IRPlusResponseDetails createCustomerContacts(CustomerContactsBean customerContactsBean)throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerContactsDao.createCustomerContacts(customerContactsBean);
		
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getCustomerContactsById(CustomerBean customerBean) throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerContactsDao.getCustomerContactsById(customerBean);
		
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateCustomerConstatnts(CustomerContactsBean customerContactsBean)
			throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerContactsDao.updateCustomerConstatnts(customerContactsBean);
		
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails deleteCustomerContactsById(String customerContactsId) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerContactsDao.deleteCustomerContactsById(customerContactsId);
		
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails showAllCustomerContacts() throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerContactsDao.showAllCustomerContacts();
		
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateCustomerConstactsOnly(CustomeContactsArrayInfo customeContactsArrayInfo)
			throws BusinessException {
IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerContactsDao.updateCustomerConstactsOnly(customeContactsArrayInfo);
		
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails addContacts(CustomerContactsBean[] customerContactsBean) throws BusinessException {
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerContactsDao.addContacts(customerContactsBean);
		
		return irPlusResponseDetails;
	}
	
}