package com.irplus.core.client.customer_licence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.client.customer_licence.ICustomerLicencingDaoClient;
import com.irplus.dto.CustomerLicenceInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

@Component
public class CustomerLicenceCoreClientImpl implements ICustomerLicencingCoreClient{

	@Autowired
	ICustomerLicencingDaoClient iCustomerLicencingDaoClient;
	
	@Override
	public IRPlusResponseDetails createCustomerLicence(CustomerLicenceInfo customerLicenceInfo)
			throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		irPlusResponseDetails = iCustomerLicencingDaoClient.createCustomerLicence(customerLicenceInfo);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getCustomerLicenceById(String customerLicenceId) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		irPlusResponseDetails = iCustomerLicencingDaoClient.getCustomerLicenceById(customerLicenceId);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateCustomerLicence(CustomerLicenceInfo customerLicenceInfo)
			throws BusinessException {
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		irPlusResponseDetails = iCustomerLicencingDaoClient.updateCustomerLicence(customerLicenceInfo);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails deleteCustomerLicenceById(String customerLicenceId) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		irPlusResponseDetails = iCustomerLicencingDaoClient.deleteCustomerLicenceById(customerLicenceId);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails showAllCustomerLicences() throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		irPlusResponseDetails = iCustomerLicencingDaoClient.showAllCustomerLicences();
		return irPlusResponseDetails;
	}

}
