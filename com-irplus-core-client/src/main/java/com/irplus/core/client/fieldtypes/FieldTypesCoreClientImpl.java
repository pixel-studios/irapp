package com.irplus.core.client.fieldtypes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.core.fieldtypes.IFieldTypesCore;
import com.irplus.dto.FieldTypesInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

@Component
public class FieldTypesCoreClientImpl implements IFieldTypesCoreClient{

	@Autowired 
	IFieldTypesCore iFieldTypesCore;
	
	@Override
	public IRPlusResponseDetails createFieldTypes(FieldTypesInfo fieldTypesInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		irPlusResponseDetails =iFieldTypesCore.createFieldTypes(fieldTypesInfo); 
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getFieldTypesById(String fieldTypesInfoId) throws BusinessException {
	
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		irPlusResponseDetails =iFieldTypesCore.getFieldTypesById(fieldTypesInfoId);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateFieldTypes(FieldTypesInfo fieldTypesInfo) throws BusinessException {
	
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		irPlusResponseDetails =iFieldTypesCore.updateFieldTypes(fieldTypesInfo);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails deleteFieldTypesById(String fieldTypesInfoId) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		irPlusResponseDetails =iFieldTypesCore.deleteFieldTypesById(fieldTypesInfoId); 
		return irPlusResponseDetails;

	}

	@Override
	public IRPlusResponseDetails showAllFieldTypes() throws BusinessException {
	
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		irPlusResponseDetails =iFieldTypesCore.showAllFieldTypes(); 
		return irPlusResponseDetails;

	}

}
