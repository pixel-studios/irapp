package com.irplus.core.client.dashboard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.core.dashboard.DashboardCore;
import com.irplus.dto.CustomerGroupingMngmntInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

@Component
public class DashboardCoreClientImpl implements DashboardCoreClient{

@Autowired 
DashboardCore dashboardCore;

@Override
public IRPlusResponseDetails getDashboard(String siteId) throws BusinessException {
return dashboardCore.getDashboard(siteId);
}

@Override
public IRPlusResponseDetails listAllFiles(Integer userId) throws BusinessException {
return dashboardCore.listAllFiles(userId);
}

@Override
public IRPlusResponseDetails BankwiseFile(CustomerGroupingMngmntInfo user,Long branchId) throws BusinessException {
return dashboardCore.BankwiseFile(user,branchId);
}


}
