package com.irplus.core.client.customer_grouping;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.core.customer_grouping.ICustomerGroupingMngmntCore;
import com.irplus.dto.CustomerGroupingDetails;
import com.irplus.dto.CustomerGroupingMngmntInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.util.BusinessException;

@Component
public class CustomerGroupingMngmntCoreClientImpl  implements ICustomerGroupingMngmntCoreClient{

	private static final Logger log = Logger.getLogger(CustomerGroupingMngmntCoreClientImpl.class);
	
	@Autowired
	ICustomerGroupingMngmntCore iCustomerGroupingMngmntCore;

	@Override
	public IRPlusResponseDetails createCustomerGrp(CustomerGroupingMngmntInfo customerGroupingMngmntInfo)
			throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerGroupingMngmntCore.createCustomerGrp(customerGroupingMngmntInfo);
		return irPlusResponseDetails;

	}
	
	

	@Override
	public IRPlusResponseDetails showOneCustomerGrpById(String customerGroupingMngmntId) throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerGroupingMngmntCore.showOneCustomerGrpById(customerGroupingMngmntId);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateOneCustomerGrps(CustomerGroupingMngmntInfo customerGroupingMngmntInfo)
			throws BusinessException {
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerGroupingMngmntCore.updateOneCustomerGrps(customerGroupingMngmntInfo);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails statusUpdatCustmrGrpById(StatusIdInfo statusIdInfo) throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerGroupingMngmntCore.statusUpdatCustmrGrpById(statusIdInfo);
		
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getCustomrGrpDtalsViewById(String customerGroupingMngmntId) throws BusinessException {
	
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerGroupingMngmntCore.getCustomrGrpDtalsViewById(customerGroupingMngmntId);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails showAllCustomerGrps() throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntCore.showAllCustomerGrps();
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails bankIdArrays() throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iCustomerGroupingMngmntCore.bankIdArrays();
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails custmrIdArrays(String barnchId) throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();		
		irPlusResponseDetails = iCustomerGroupingMngmntCore.custmrIdArrays(barnchId);
		return irPlusResponseDetails;
	}
	
	
	@Override
	public IRPlusResponseDetails showAllCustomerGroup() throws BusinessException {
	
		return iCustomerGroupingMngmntCore.showAllCustomerGroup();
	}
	
	@Override
	public IRPlusResponseDetails createCustomerMappingGroup(CustomerGroupingMngmntInfo customergroupingMngmntInfo)
			throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerGroupingMngmntCore.createCustomerMappingGroup(customergroupingMngmntInfo);
		return irPlusResponseDetails;

	}

	@Override
	public IRPlusResponseDetails updateCustomerMappingGroup(CustomerGroupingMngmntInfo customergroupingMngmntInfo)
			throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerGroupingMngmntCore.updateCustomerMappingGroup(customergroupingMngmntInfo);
		return irPlusResponseDetails;

	}

	@Override
	public IRPlusResponseDetails deleteGroup(CustomerGroupingMngmntInfo groupInfo) throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerGroupingMngmntCore.deleteGroup(groupInfo);
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails volumeTrendrangewise(CustomerGroupingMngmntInfo trendRange) throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerGroupingMngmntCore.volumeTrendrangewise(trendRange);
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails volumeTrendSummary(CustomerGroupingMngmntInfo trendDateRange) throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerGroupingMngmntCore.volumeTrendSummary(trendDateRange);
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails getcustomergroupInfo(Integer customerGrpId) throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();		
		irPlusResponseDetails = iCustomerGroupingMngmntCore.getcustomergroupInfo(customerGrpId);
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails getcustomerdateWise(CustomerGroupingMngmntInfo customerdateinfo) throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();		
		irPlusResponseDetails = iCustomerGroupingMngmntCore.getcustomerdateWise(customerdateinfo);
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails showcustomerWiseReport() throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntCore.showcustomerWiseReport();
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails filterGrpdatewise(CustomerGroupingMngmntInfo grpdateinfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntCore.filterGrpdatewise(grpdateinfo);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails filterCustomerdatewise(CustomerGroupingMngmntInfo customerdateinfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntCore.filterCustomerdatewise(customerdateinfo);
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails getcustomergroupDatewise(CustomerGroupingMngmntInfo customergrpdateinfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntCore.getcustomergroupDatewise(customergrpdateinfo);
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails getdatacustomerDatewise(CustomerGroupingMngmntInfo customerdatewiseinfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntCore.getdatacustomerDatewise(customerdatewiseinfo);
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails showVolumeTrendReportWeekwise() throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntCore.showVolumeTrendReportWeekwise();
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails showVolumeTrendReport(CustomerGroupingMngmntInfo currentDate) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntCore.showVolumeTrendReport(currentDate);
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails getbankwiseReport(String bankid) throws BusinessException {
	
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerGroupingMngmntCore.getbankwiseReport(bankid);
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails getbankDaywiseReport(String bankid) throws BusinessException {
	
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =  iCustomerGroupingMngmntCore.getbankDaywiseReport(bankid);
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails volumeTrendBankrangewise(CustomerGroupingMngmntInfo BankRange) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntCore.volumeTrendBankrangewise(BankRange);
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails showgroupingReport(CustomerGroupingMngmntInfo customerInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntCore.showgroupingReport(customerInfo);
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails CustomerGrpFilter(CustomerGroupingMngmntInfo GrpListInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntCore.CustomerGrpFilter(GrpListInfo);
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails showDaywiseReport(CustomerGroupingMngmntInfo currentDate) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntCore.showDaywiseReport(currentDate);
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails showDaywiseCustomerReport(CustomerGroupingMngmntInfo customerInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntCore.showDaywiseCustomerReport(customerInfo);
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails showDaywiseCustomerGroupReport(CustomerGroupingMngmntInfo customerInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntCore.showDaywiseCustomerGroupReport(customerInfo);
		return irPlusResponseDetails;
	}
	
	
	@Override
	public IRPlusResponseDetails DaycustomerReport(CustomerGroupingMngmntInfo customerInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntCore.DaycustomerReport(customerInfo);
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails DaycustomerGroupReport(CustomerGroupingMngmntInfo customerInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntCore.DaycustomerGroupReport(customerInfo);
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails getGroupCustomerwise(CustomerGroupingMngmntInfo customerInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntCore.getGroupCustomerwise(customerInfo);
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails filterGroups(CustomerGroupingMngmntInfo grpFilterInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntCore.filterGroups(grpFilterInfo);
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails AutocompleteGroup(String term,String userid) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails =   iCustomerGroupingMngmntCore.AutocompleteGroup(term,userid);
		return irPlusResponseDetails;
	}
}