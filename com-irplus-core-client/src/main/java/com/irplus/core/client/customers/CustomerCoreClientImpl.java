package com.irplus.core.client.customers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.core.customers.ICustomerCore;
import com.irplus.dto.CustomerBean;
import com.irplus.dto.CustomerFilter;
import com.irplus.dto.FormsManagementDTO;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.util.BusinessException;

@Component
public class CustomerCoreClientImpl  implements ICustomerCoreClient{

	@Autowired
	ICustomerCore iCustomerCore;
	
	@Override
	public IRPlusResponseDetails createCustomer(CustomerBean customerBean) throws BusinessException {

		return  iCustomerCore.createCustomer(customerBean);
	}

	@Override
	public IRPlusResponseDetails getCustomerById(CustomerBean customerBean) throws BusinessException {
	
		return iCustomerCore.getCustomerById(customerBean);
		
	}

	@Override
	public IRPlusResponseDetails updateCustomer(CustomerBean customersBean) throws BusinessException {
				
		return iCustomerCore.updateCustomer(customersBean);
		}
	
	@Override
	public IRPlusResponseDetails showAllCustomer(String siteId) throws BusinessException{
	
		return iCustomerCore.showAllCustomer(siteId);
	}
	
	@Override
	public IRPlusResponseDetails deleteCustomerById(String custommerId) throws BusinessException {
		
		return iCustomerCore.deleteCustomerById(custommerId);
	}

	@Override
	public IRPlusResponseDetails showAllBanks(String SiteId) throws BusinessException {
	
		return iCustomerCore.showAllBanks(SiteId);
	}

	@Override
	public IRPlusResponseDetails updateCutomerStaus(StatusIdInfo statusIdInfo) throws BusinessException {

		return iCustomerCore.updateCutomerStaus(statusIdInfo);
	}

	@Override
	public IRPlusResponseDetails customerFilter(CustomerFilter customerFilter) throws BusinessException {
		return iCustomerCore.customerFilter(customerFilter);
	}

	@Override
	public IRPlusResponseDetails showAllFileProcessing() throws BusinessException {
		// TODO Auto-generated method stub
		return iCustomerCore.showAllFileProcessing();
	}

	@Override
	public IRPlusResponseDetails showAllBusinessProcessing() throws BusinessException {
		// TODO Auto-generated method stub
		return iCustomerCore.showAllBusinessProcessing();
	}

	@Override
	public IRPlusResponseDetails showBankBranch(String bankId) throws BusinessException {
		// TODO Auto-generated method stub
		return iCustomerCore.showBankBranch(bankId);
	}
	@Override
	public IRPlusResponseDetails Autocomplete(String term,String userid) throws BusinessException
	{
		
		return iCustomerCore.Autocomplete(term,userid);
	}
	
	@Override
	public IRPlusResponseDetails AutocompleteCustomer(String term,String userid) throws BusinessException
	{
		
		return iCustomerCore.AutocompleteCustomer(term,userid);
	}
	
	@Override
	public IRPlusResponseDetails AutocompleteCustomerCode(String term,String userid) throws BusinessException
	{
		
		return iCustomerCore.AutocompleteCustomerCode(term,userid);
	}
	
	@Override
	public IRPlusResponseDetails AutocompleteBank(String term,String userid) throws BusinessException
	{
		
		return iCustomerCore.AutocompleteBank(term,userid);
	}
	
	@Override
	public IRPlusResponseDetails AddCustomerDataEntry(FormsManagementDTO formsManagement) throws BusinessException
	{
		
		return iCustomerCore.AddCustomerDataEntry(formsManagement);
	}
}