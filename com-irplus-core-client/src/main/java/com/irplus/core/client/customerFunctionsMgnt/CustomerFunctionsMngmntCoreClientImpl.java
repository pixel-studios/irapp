package com.irplus.core.client.customerFunctionsMgnt;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.core.customerFunctionsMgnt.ICustomerFunctionsMngmntCore;
import com.irplus.dto.CustomerFunctionInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

@Component
public class CustomerFunctionsMngmntCoreClientImpl implements ICustomerFunctionsMngmntCoreClient {

	private static final Logger log = Logger.getLogger(CustomerFunctionsMngmntCoreClientImpl.class);
	
	@Autowired 
	ICustomerFunctionsMngmntCore iCustomerFunctionsMngmntCore;

	@Override
	public IRPlusResponseDetails createCustomerFunctions(CustomerFunctionInfo customerFunctionInfo)
			throws BusinessException {

		log.debug("Inside of CustomerFunctionsMngmntCoreClientImpl");
			
		return iCustomerFunctionsMngmntCore.createCustomerFunctions(customerFunctionInfo);
	}

	@Override
	public IRPlusResponseDetails getCustomerFunctionsById(String customerFunctionInfoId) throws BusinessException {
		log.debug("Inside of CustomerFunctionsMngmntCoreClientImpl");
		
		return iCustomerFunctionsMngmntCore.getCustomerFunctionsById(customerFunctionInfoId);
	}

	@Override
	public IRPlusResponseDetails updateCustomerFunctions(CustomerFunctionInfo customerFunctionInfo)
			throws BusinessException {

		return iCustomerFunctionsMngmntCore.updateCustomerFunctions(customerFunctionInfo);
	}

	@Override
	public IRPlusResponseDetails statusUpdateCustmrFunctnsById(String customerFunctionInfoId) throws BusinessException {

		return null;
	}

	@Override
	public IRPlusResponseDetails showAllCustmrFunctions() throws BusinessException {

		return null;
	}

	
	
}
