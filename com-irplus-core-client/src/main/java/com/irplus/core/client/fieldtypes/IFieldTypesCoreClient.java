package com.irplus.core.client.fieldtypes;

import com.irplus.dto.FieldTypesInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;


public interface IFieldTypesCoreClient {
		
	    public IRPlusResponseDetails createFieldTypes(FieldTypesInfo fieldTypesInfo) throws BusinessException;

		public IRPlusResponseDetails getFieldTypesById(String fieldTypesInfoId) throws BusinessException;
		
		public IRPlusResponseDetails updateFieldTypes(FieldTypesInfo fieldTypesInfo) throws BusinessException;

		public IRPlusResponseDetails deleteFieldTypesById(String fieldTypesInfoId) throws BusinessException;
		
		public IRPlusResponseDetails showAllFieldTypes() throws BusinessException;
		
	}