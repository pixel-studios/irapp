package com.irplus.core.client.fileprocessing.fileheaderrecord;

import java.util.List;

import com.irplus.dto.CustomerGroupingMngmntInfo;
import com.irplus.dto.FileTypeInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.fileprocessing.FileHeaderRecordBean;
import com.irplus.util.BusinessException;

public interface FileHeaderRecordCoreClient {
	public Boolean insertRecord(FileHeaderRecordBean fileHeaderRecord);
	public Boolean deleteRecord(FileHeaderRecordBean fileHeaderRecord);
	public IRPlusResponseDetails listAllRecords(String userId)throws BusinessException;
	public IRPlusResponseDetails getRecordByBatchId(Integer fileId) throws BusinessException; 
    public Boolean updateRecord(FileHeaderRecordBean fileHeaderRecord);
    public IRPlusResponseDetails getMasterDataChart(Integer userId,String siteId) throws BusinessException; 
    public IRPlusResponseDetails listfileLog()throws BusinessException;
    public IRPlusResponseDetails DaywiseFiles(CustomerGroupingMngmntInfo fileheadrec) throws BusinessException; 
    public IRPlusResponseDetails DaywiseFilesGroup(CustomerGroupingMngmntInfo fileheadrec) throws BusinessException; 
    public IRPlusResponseDetails BankwiseFileList(CustomerGroupingMngmntInfo fileinfo) throws BusinessException; 
	public IRPlusResponseDetails filterFiles(FileTypeInfo fileListInfo) throws BusinessException; 
    public IRPlusResponseDetails GroupwiseFileList(CustomerGroupingMngmntInfo Grpinfo) throws BusinessException; 
}
