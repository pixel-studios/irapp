package com.irplus.core.client.module;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.core.module.IModuleCore;
import com.irplus.core.module.ModuleCoreImpl;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.module.ModuleBean;
import com.irplus.util.BusinessException;

@Component
public class ModuleCoreClientImpl implements IModuleCoreClient{
	
	private static final Logger log = Logger.getLogger(ModuleCoreImpl.class);
	
	@Autowired
	IModuleCore moduleCore;

	@Override
	public IRPlusResponseDetails createModule(ModuleBean moduleInfo) throws BusinessException {

		return   moduleCore.createModule(moduleInfo);
	}

	@Override
	public IRPlusResponseDetails getModuleById(String moduleId) throws BusinessException {

		return moduleCore.getModuleById(moduleId);
	}

	@Override
	public IRPlusResponseDetails updateModule(ModuleBean moduleInfo) throws BusinessException {

		return moduleCore.updateModule(moduleInfo);
	}

	@Override
	public IRPlusResponseDetails deleteModuleById(String moduleId) throws BusinessException {

		return moduleCore.deleteModuleById(moduleId);
	}

	@Override
	public IRPlusResponseDetails findAllModules() throws BusinessException {

		return moduleCore.findAllModules();
	}

	@Override
	public IRPlusResponseDetails createByConditionModule(ModuleBean moduleInfo) throws BusinessException {

		return moduleCore.createByConditionModule(moduleInfo);
	}

	@Override
	public IRPlusResponseDetails createModuleNoDuplct(ModuleBean moduleInfo) throws BusinessException {

		return moduleCore.createModuleNoDuplct(moduleInfo);
	}

	@Override
	public IRPlusResponseDetails updateModuleStatus(StatusIdInfo siInfo) throws BusinessException {

		return moduleCore.updateModuleStatus(siInfo);
	}
}
