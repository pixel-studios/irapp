package com.irplus.core.client.rolepermissions;


import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.RolePrmUpdateNCreateInfo;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.rolepermission.RolepermissionsBean;
import com.irplus.util.BusinessException;

public interface IRolePermissionCoreClient {
		
    public IRPlusResponseDetails createRolePermission(RolepermissionsBean rolepermissionsBean) throws BusinessException;

	public IRPlusResponseDetails getRolePermissionById(String RolepermissionsBeanId) throws BusinessException;		

	public IRPlusResponseDetails updateRolePermission(RolepermissionsBean  rolepermissionsBean) throws BusinessException;

	public IRPlusResponseDetails deleteRolePermissionById(String rolePermissionId) throws BusinessException;
		
	public IRPlusResponseDetails showAllRolePermission() throws BusinessException;
	
	public IRPlusResponseDetails updateStatusRolePrmn(StatusIdInfo statusIdInfo)throws BusinessException;
	
	// new two
	
	public IRPlusResponseDetails showAllRoleNMdl(StatusIdInfo statusIdInfo)	throws BusinessException ;

	public IRPlusResponseDetails updateRoleprmUpdNCrt(RolePrmUpdateNCreateInfo rolepermUpNCrte)	throws BusinessException ;
}
