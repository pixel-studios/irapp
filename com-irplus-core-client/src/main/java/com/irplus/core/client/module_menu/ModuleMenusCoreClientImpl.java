package com.irplus.core.client.module_menu;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.core.module_menu.IModuleMenusCore;
import com.irplus.dao.client.module_menu.ModuleMenusDaoClientImpl;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.menu_modules_association.ModuleMenuBean;
import com.irplus.util.BusinessException;

@Component
public class ModuleMenusCoreClientImpl implements IModuleMenusCoreClient {

	private static final Log log = LogFactory.getLog(ModuleMenusDaoClientImpl.class);
	
	@Autowired
	IModuleMenusCore moduleMenusCore;
		
	@Override
	public IRPlusResponseDetails createMenuModule(ModuleMenuBean moduleMenuInfo) throws BusinessException {
		return moduleMenusCore.createMenuModule(moduleMenuInfo);
	}

	@Override
	public IRPlusResponseDetails getMenuModuleById(String moduleMenuId) throws BusinessException {

		return moduleMenusCore.getMenuModuleById(moduleMenuId);
	}

	@Override
	public IRPlusResponseDetails updateMenuModule(ModuleMenuBean moduleMenuInfo) throws BusinessException {

		return moduleMenusCore.updateMenuModule(moduleMenuInfo);
	}

	@Override
	public IRPlusResponseDetails deleteMenuModuleById(String moduleMenuId) throws BusinessException {

		return moduleMenusCore.deleteMenuModuleById(moduleMenuId);
	}

	@Override
	public IRPlusResponseDetails findAllMenuModule() throws BusinessException {

		return moduleMenusCore.findAllMenuModule();
	}
	
	@Override
	public IRPlusResponseDetails findAllByStatus() throws BusinessException {		
		return  moduleMenusCore.findAllByStatus();
	}

	@Override
	public IRPlusResponseDetails UpdateStatus(StatusIdInfo sidInfo) throws BusinessException {		
		return  moduleMenusCore.UpdateStatus(sidInfo);
	}

	@Override
	public IRPlusResponseDetails DeleteUpdateStatus(StatusIdInfo sidInfo) throws BusinessException {		
		return  moduleMenusCore.DeleteUpdateStatus(sidInfo);
	}

	@Override
	public IRPlusResponseDetails createMenuModuleNoDuplicate(ModuleMenuBean moduleMenuInfo) throws BusinessException {

		return  moduleMenusCore.createMenuModuleNoDuplicate(moduleMenuInfo);
	}

	
}
