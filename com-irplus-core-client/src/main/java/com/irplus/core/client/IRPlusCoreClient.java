package com.irplus.core.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dto.BankInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.LicenseInfo;
import com.irplus.util.BusinessException;
import com.irplus.core.IRPlusCore;


@Component
public class IRPlusCoreClient {
	@Autowired
	IRPlusCore irpluscore;
	
	public IRPlusResponseDetails getMasterData(String siteId) throws BusinessException {

		return irpluscore.getMasterData(siteId);
	}

	public IRPlusResponseDetails validateLicense(LicenseInfo licInfo) throws BusinessException{
		// TODO Auto-generated method stub
		return irpluscore.validateLicense(licInfo);
	}

}
