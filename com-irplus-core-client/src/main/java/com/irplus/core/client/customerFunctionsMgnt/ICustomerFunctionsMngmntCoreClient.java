package com.irplus.core.client.customerFunctionsMgnt;

import com.irplus.dto.CustomerFunctionInfo;
import com.irplus.dto.CustomerReportLicenceInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

public interface ICustomerFunctionsMngmntCoreClient {

	 	public IRPlusResponseDetails createCustomerFunctions(CustomerFunctionInfo customerFunctionInfo) throws BusinessException;

		public IRPlusResponseDetails getCustomerFunctionsById(String customerFunctionInfoId) throws BusinessException;
		
		public IRPlusResponseDetails updateCustomerFunctions(CustomerFunctionInfo  customerFunctionInfo) throws BusinessException;

		public IRPlusResponseDetails statusUpdateCustmrFunctnsById(String customerFunctionInfoId) throws BusinessException;
		
		public IRPlusResponseDetails showAllCustmrFunctions() throws BusinessException;
		
}
