package com.irplus.core.client.customer_report_licence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.core.customer_report_licence.ICustomerReportLicenceCore;
import com.irplus.dto.CustomerReportLicenceInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

@Component
public class CustomerReportLicenceCoreClientImpl implements ICustomerReportLicenceCoreClient{

	@Autowired
	ICustomerReportLicenceCore iCustomerReportLicenceDao; 
	
	@Override
	public IRPlusResponseDetails createCustomerReportLicence(CustomerReportLicenceInfo customerReportLicenceInfo)
			throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails  = new IRPlusResponseDetails();
		irPlusResponseDetails  = iCustomerReportLicenceDao.createCustomerReportLicence(customerReportLicenceInfo);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getCustomerReportLicenceById(String customerReportLicenceInfoId)
			throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails  = new IRPlusResponseDetails();
		irPlusResponseDetails  = iCustomerReportLicenceDao.getCustomerReportLicenceById(customerReportLicenceInfoId);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateCustomerReportLicence(CustomerReportLicenceInfo customerReportLicenceInfo)
			throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails  = new IRPlusResponseDetails();
		irPlusResponseDetails  = iCustomerReportLicenceDao.updateCustomerReportLicence(customerReportLicenceInfo);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails deleteCustomerReportLicenceById(String ReportLicenceInfoId) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails  = new IRPlusResponseDetails();
		irPlusResponseDetails  = iCustomerReportLicenceDao.deleteCustomerReportLicenceById(ReportLicenceInfoId);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails showAllCustomerReportLicences() throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails  = new IRPlusResponseDetails();
		irPlusResponseDetails  = iCustomerReportLicenceDao.showAllCustomerReportLicences();
		return irPlusResponseDetails;
	}

}
