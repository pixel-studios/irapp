package com.irplus.core.client.fileprocessing.batchheaderrecord;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.core.fileprocessing.batchheaderrecord.BatchHeaderRecordCore;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.fileprocessing.BatchHeaderRecordBean;
import com.irplus.util.BusinessException;

@Component
public class BatchHeaderRecordCoreClientImpl implements BatchHeaderRecordCoreClient{
	
	@Autowired
	private BatchHeaderRecordCore batchHeaderRecordCore;

	@Override
	public Boolean insertRecord(BatchHeaderRecordBean batchHeaderRecord) {
		return batchHeaderRecordCore.insertRecord(batchHeaderRecord);
	}

	@Override
	public Boolean deleteRecord(BatchHeaderRecordBean batchHeaderRecord) {
		return batchHeaderRecordCore.deleteRecord(batchHeaderRecord);
	}

	@Override
	public List<BatchHeaderRecordBean> listAllRecords() {
		return batchHeaderRecordCore.listAllRecords();
	}

	
	@Override
	public Boolean updateRecord(BatchHeaderRecordBean batchHeaderRecord) {
		return batchHeaderRecordCore.updateRecord(batchHeaderRecord);
	}

	@Override
	public List<BatchHeaderRecordBean> getByQuery(Integer Id) {
		return batchHeaderRecordCore.getByQuery(Id);
	}

	@Override
	public IRPlusResponseDetails getRecordByBatchId(Integer fileId) throws BusinessException {
		return batchHeaderRecordCore.getRecordByBatchId(fileId);
	}

}
