package com.irplus.core.client.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.core.users.IUserCore;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.UserFilterInfo;
import com.irplus.dto.users.UserBean;
import com.irplus.util.BusinessException;

@Component
public class UserCoreClientImpl implements IUserCoreClient{

	@Autowired 
	IUserCore userCore;
	
	@Override
	public IRPlusResponseDetails createUser(UserBean userBeanInfo) throws BusinessException {
		
		return userCore.createUser(userBeanInfo);
	}

	@Override
	public IRPlusResponseDetails getUserById(String userId) throws BusinessException {

		return userCore.getUserById(userId);
	}

	@Override
	public IRPlusResponseDetails updateUser(UserBean userBeanInfo) throws BusinessException {

		return userCore.updateUser(userBeanInfo);
	}

	@Override
	public IRPlusResponseDetails deleteUserById(String userId) throws BusinessException {

		return userCore.deleteUserById(userId);
	}

	@Override
	public IRPlusResponseDetails findAllUsers(String siteId) throws BusinessException {

		return userCore.findAllUsers(siteId);
	}

	@Override
	public IRPlusResponseDetails createUserNoDup(UserBean userBean) throws BusinessException {

		return userCore.createUserNoDup(userBean);
	}

	@Override
	public IRPlusResponseDetails getAll_Role_Banks(String siteId) throws BusinessException {

		return userCore.getAll_Role_Banks(siteId);				
	}

	@Override
	public IRPlusResponseDetails updateUserAndBank(UserBean userBeanInfo) throws BusinessException {

		return userCore.updateUserAndBank(userBeanInfo);
	}

	@Override
	public IRPlusResponseDetails updateStatusUser(UserBean userBeanInfo) throws BusinessException {

		return userCore.updateStatusUser(userBeanInfo);
	}

	@Override
	public IRPlusResponseDetails filterUsers(UserFilterInfo userFilterInfo) throws BusinessException {

		return userCore.filterUsers(userFilterInfo);
	}

	@Override
	public IRPlusResponseDetails validateUser(UserBean userBean) throws BusinessException{
		return userCore.validateUser(userBean);
	}

	@Override
	public IRPlusResponseDetails fetchMenu(int roleId)  throws BusinessException{
		return userCore.fetchMenu(roleId);
	}

	@Override
	public IRPlusResponseDetails getOtherBank(String bankId) throws BusinessException {
		return userCore.getOtherBank(bankId);
	}

	@Override
	public IRPlusResponseDetails getUserBank(String userId) throws BusinessException {
		return userCore.getUserBank(userId);
	}

	@Override
	public IRPlusResponseDetails updateProfile(UserBean userBeanInfo) throws BusinessException {
		return userCore.updateProfile(userBeanInfo);
	}
	@Override
	public IRPlusResponseDetails AutocompleteUsername(String term,String siteId) throws BusinessException
	{
		
		return userCore.AutocompleteUsername(term,siteId);
	}
}