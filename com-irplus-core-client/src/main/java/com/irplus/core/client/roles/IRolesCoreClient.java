package com.irplus.core.client.roles;

import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.roles.RoleBean;
import com.irplus.util.BusinessException;

public interface IRolesCoreClient{
		
    public IRPlusResponseDetails createRole(RoleBean roleBean) throws BusinessException;

	public IRPlusResponseDetails getRoleById(String roleId) throws BusinessException;		

	public IRPlusResponseDetails updateRole(RoleBean roleBean) throws BusinessException;

	public IRPlusResponseDetails deleteRoleById(String roleId) throws BusinessException;
		
	public IRPlusResponseDetails ShowAllRoles() throws BusinessException;
	
	public IRPlusResponseDetails removeDuplicateRole(RoleBean roleBean) throws BusinessException;
	
	public IRPlusResponseDetails updateRoleStatus(StatusIdInfo statusIbInfo) throws BusinessException ;
	
	public IRPlusResponseDetails ShowAllRoles_roleprm() throws BusinessException ;
	
}
