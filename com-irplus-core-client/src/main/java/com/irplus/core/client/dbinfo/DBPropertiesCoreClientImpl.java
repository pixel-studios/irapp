/**
 * 
 */
package com.irplus.core.client.dbinfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.core.dbinfo.IDBPropertiesCore;
import com.irplus.dto.DBDTO;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

/**
 * @author arjun
 *
 */

@Component
public class DBPropertiesCoreClientImpl implements IDBPropertiesCoreClient{

	@Autowired
	IDBPropertiesCore iDBPropertiesCore;
	
	@Override
	public IRPlusResponseDetails createDBProperties(DBDTO dbdto) throws BusinessException {

		return iDBPropertiesCore.createDBProperties(dbdto);
	}

	@Override
	public IRPlusResponseDetails updateDBProperties(DBDTO dbdto) throws BusinessException {

		return iDBPropertiesCore.updateDBProperties(dbdto);
	}

	@Override
	public IRPlusResponseDetails showDBProperties() throws BusinessException {

		return iDBPropertiesCore.showDBProperties();
	}

	@Override
	public IRPlusResponseDetails deleteDBProperties(DBDTO dbdto) throws BusinessException {

		return iDBPropertiesCore.deleteDBProperties(dbdto);
	}

	
}
