<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Bank Wise File Process List</h1>
			</div>
			<!-- END PAGE TITLE -->
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB 
			<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<ul class="page-breadcrumb breadcrumb">
						<li>
							<a href="#">Home</a><i class="fa fa-circle"></i>
						</li>
						
						<li class="active">
							File Process List
						</li>
						</ul>
					</div>
				</div>
			</div>
			 END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
								<div class="portlet light col-md-12 col-sm-12 col-xs-12">
									<div class="portlet-body">
										<div class="table-scrollable table-scrollable-borderless">
											<table id="file-list" class="table">
												<thead>
													<tr class="uppercase">
														<th>
															Bank Name
														</th>
													<!--<th>
															Bank Name
														</th> -->
														<th>
															Total Files
														</th>
														<th>
															Total Batches
														</th>
														<th>
															Total Entry
														</th>
														<th>
															Total Credit
														</th>
														<th>
															Total Debit
														</th>
																						
																										
														<th>
															 view
														</th>
													</tr>
												</thead>
											<!--	<tbody>
													<tr>
														<td>
															Bof_100617
														</td>
														<td>
															Bank of America
														</td>
														<td>
															06/21/2016
														</td>
														<td>
															10
														</td>
														<td>
															4
														</td>
														<td>
															6
														</td>
														<td>
															<a href="file-process" class="edit-btn" data-toggle="tooltip" data-placement="right" title="Batch View">
																<i class="fa fa-eye"></i>
															</a>
														</td>
														<td>
															<button type="button" class="btn red btn-xs">In progress</button>
														</td>
														<td>
															<a href="file-process-detail.jsp" class="edit-btn" data-toggle="tooltip" data-placement="right" title="View">
																<i class="fa fa-eye"></i>
															</a>
														</td>
													</tr>	
													<tr>
														<td>
															Bof_100618
														</td>
														<td>
															Citi Bank
														</td>
														<td>
															06/23/2016
														</td>
														<td>
															100
														</td>
														<td>
															100
														</td>
														<td>
															0
														</td>
														<td>
															<a href="file-process-detail.jsp" class="edit-btn" data-toggle="tooltip" data-placement="right" title="Batch View">
																<i class="fa fa-eye"></i>
															</a>
														</td>
														<td>
															<button type="button" class="btn red btn-xs">In Progress</button>
														</td>
														<td>
															<a href="file-process-detail.jsp" class="edit-btn" data-toggle="tooltip" data-placement="right" title="View">
																<i class="fa fa-eye"></i>
															</a>
														</td>
													</tr>
												</tbody>-->
											</table>
										</div>
									</div>
								</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<script>
$(document).ready(function()
{  	 
	$.fn.dataTable.ext.errMode = 'throw'
	  $('#file-list').DataTable( {
		  
		  ajax :{
			"url":"../bank-file-process/showAll",  
			"dataSrc":"bankWiseFileProcess"
		  },
		//"ajax": "../role/showAll",
		"processing": true,
        //"serverSide": true,x
        "columns": [
			{"data":"bankName"},
			{"data":"totalFiles"},
			{"data":"batchCount"},
		
			{"data":"entryCount"},
			{"data":"totalCredit"},
			{"data":"totalDebit"},
			 /* {"data":"bankVisibility"},*/
			
			{ 
				data: null, render: function ( data, type, row ) 
				{
				//alert("data.bankId"+data.bankId);
                return '<a href="file-process-detail.jsp?fileHeaderRecordId='+data.bankId+'" class="edit-btn" data-toggle="tooltip" data-placement="right" title="View"><i class="fa fa-eye"></i></a>'
				}
			},	
				
        ]
    } );
});
</script>


<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>