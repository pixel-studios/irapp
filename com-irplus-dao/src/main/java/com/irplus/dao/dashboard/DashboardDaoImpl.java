package com.irplus.dao.dashboard;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.Cookie;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.hibernate.entities.IRSBank;
import com.irplus.dao.hibernate.entities.IrFPBatchHeaderRecord;
import com.irplus.dao.hibernate.entities.IrFPFileHeaderRecord;
import com.irplus.dao.hibernate.entities.IrMRoles;
import com.irplus.dao.hibernate.entities.IrMUsers;
import com.irplus.dao.hibernate.entities.IrMUsersBank;
import com.irplus.dao.hibernate.entities.IrSBankBranch;
import com.irplus.dao.hibernate.entities.IrSBankContact;
import com.irplus.dao.hibernate.entities.IrSCustomers;
import com.irplus.dto.BankBranchInfo;
import com.irplus.dto.BankInfo;
import com.irplus.dto.CustomerGroupingMngmntInfo;
import com.irplus.dto.Dashboard;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.LocationViewInfo;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusUtil;

@Component
@Transactional
public class DashboardDaoImpl implements DashboardDao {

	private static final Logger LOGGER = Logger.getLogger(DashboardDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	

	
	public Session getMyCurrentSession(){
		
		Session session =null;
	
		try {	
		
			session=sessionFactory.getCurrentSession();
		
		}catch (HibernateException e) {
			LOGGER.error("Exception raised DaoImpl :: At current session creation time ::"+e);
		}		
		return session; 
	}
		
	
	
	@Override
	public IRPlusResponseDetails getDashboard(String userId) throws BusinessException {
		LOGGER.info("Inside Dashboard Dao Impl : ");
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		Dashboard dashboard = new Dashboard();
		BankInfo bankDetails=null;
		
		
		try {
			
		/*	Cookie cookie = requestContext.getCookies().get("JSESSIONID");
			String value = cookie.getValue();
			*/
			
			int userid = Integer.parseInt(userId);
			//total active users
			Query users = getMyCurrentSession().createQuery("from IrMUsers users where users.site.siteId=? and users.isactive=?");
			users.setParameter(0,new Long(71));
			users.setParameter(1,1);
			List<IrMUsers> usersList=users.list();
			
		
			if(usersList.size()>0){
				dashboard.setTotalUsers(usersList.size());
			}else{
				dashboard.setTotalUsers(0);	
			}
			
			
			//total active roles
			
			Query roles = getMyCurrentSession().createQuery("from IrMRoles irMRoles where irMRoles.isrestricted=? and irMRoles.isactive=?");
			roles.setParameter(0,1);
			roles.setParameter(1,1);
			List<IrMRoles> rolesList =roles.list();
			
		
			if(rolesList.size()>0){
				dashboard.setTotalRoles(rolesList.size());
			}else{
				dashboard.setTotalRoles(0);
			}
		
		
			
	Query banks = getMyCurrentSession().createQuery("from IrMUsersBank userBank where userBank.irMUsers.userid=? and userBank.isactive=?");
			banks.setParameter(0,userid);
			banks.setParameter(1,1);
			List<IrMUsersBank> userBankList=banks.list();
			dashboard.setTotalBanks(userBankList.size());
		int count=0;
		if(userBankList.size()>0){
				for(IrMUsersBank irMUsersBank:userBankList){
			    bankDetails = new BankInfo();
					bankDetails.setBankLogo(irMUsersBank.getBank().getBankLogo());
					bankDetails.setBankName(irMUsersBank.getBank().getBankName());
					bankDetails.setBankId(irMUsersBank.getBank().getBankId());
					//bankDetails.setBranchId(irMUsersBank.getIrSBankbranch().getBranchId());
/*					
	branch location 	*/			
					
					Query totcustomer = getMyCurrentSession().createQuery("from IrSCustomers customer where customer.isactive=? and customer.irSBankBranch.bank.bankId=?");


					totcustomer.setParameter(0,1);

					totcustomer.setParameter(1,irMUsersBank.getBank().getBankId());
					List<IrSCustomers> totalcustList =totcustomer.list();
					
					
				if(totalcustList.size()>0){	
					
				count+=totalcustList.size();
					
				}
			
				
			List <BankBranchInfo> branchbanklist = new ArrayList<BankBranchInfo>();
			
	Query branch = getMyCurrentSession().createQuery("from IrSBankBranch branch where branch.isactive=? and branch.bank.bankId=?");
	branch.setParameter(0,1);
	BankBranchInfo branchDetails=null;
	branch.setParameter(1,irMUsersBank.getBank().getBankId());
	List<IrSBankBranch> branchList =branch.list();			

	if(branchList.size()>0){
		int countcust=0;
		for(int j=0;j<branchList.size();j++){
		
		Query cust = getMyCurrentSession().createQuery("from IrSCustomers customer where customer.isactive=? and customer.irSBankBranch.branchId=?");


			cust.setParameter(0,1);

			cust.setParameter(1,branchList.get(j).getBranchId());
			List<IrSCustomers> custList =cust.list();
			
			
		if(custList.size()>0){	
			
				 countcust+=custList.size();
				 
			}
		
	
			bankDetails.setTotalCustomers(countcust);
		}
	
		bankDetails.setTotalBranches(branchList.size());
	}

	dashboard.getBankInfo().add(bankDetails);
				}
				
				/*bankDetails.setTotalCustomerscount(count);*/
				dashboard.setTotalCustomerCount(count);
		}
		

			irPlusResponseDetails.setDashboardResponse(dashboard);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		} catch (Exception e) {
		
			LOGGER.error("Exception in listBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

			throw new HibernateException(e);

		}
		
		
		
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails listAllFiles(Integer userId) throws BusinessException {
		LOGGER.info("Inside Dashboard Dao Impl : file processing");
	
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		Dashboard dashboard = new Dashboard();
		IrFPBatchHeaderRecord FileDetails=null;
		
		try {

			Date date=new Date();
			DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");

			String procesdate=dateFormat.format(date);
			
			

			
			 String fromDate="";
			 String ToDate="";
			   String[] monthnames = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

			 

			   String[] dateParts = procesdate.split("-");
			   String month = dateParts[0]; 
			   String day = dateParts[1]; 
			   String year = dateParts[2]; 
			  
			   String monthName =  monthnames[Integer.parseInt(month)-1];
			
			   dashboard.setMonth(monthName);
			   dashboard.setYear(year);
			   dashboard.setDay(day);

			Query totalfile = getMyCurrentSession().createQuery("from IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr where fhr.processedDate=? and user.irMUsers.userid=? and user.isactive=?");
			totalfile.setParameter(0,procesdate);
			totalfile.setParameter(1,userId);
			totalfile.setParameter(2,1);
			List<IrFPFileHeaderRecord> totalfilelist = totalfile.list();
			if(totalfilelist.size()>0){
				dashboard.setTotalFiles(totalfilelist.size());
			}
			
			Query batch = getMyCurrentSession().createQuery(" from IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr  inner join fhr.irFPBatchHeaderRecord as fbr where fhr.processedDate=? and user.irMUsers.userid=? and user.isactive=?");

			batch.setParameter(0,procesdate);
			batch.setParameter(1,userId);
			batch.setParameter(2,1);


	List<?> list = batch.list();
	if(list.size()>0){	
		BigDecimal batchamt = BigDecimal.ZERO;
		//BigDecimal batchamt = new BigDecimal(0);
		int transcount=0;
		int count=0;
		int debitamount=0;
		int creditamount=0;
		int filecount=0;
	for(int i=0; i<list.size(); i++) {
		FileDetails = new IrFPBatchHeaderRecord();
		
		
		Object[] row = (Object[]) list.get(i);
		IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)row[2];
		IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)row[3];

		if(list.size()>0){	
			dashboard.setTotalBatches(list.size());
		}
		
		else
		{
			dashboard.setTotalBatches(0);
		}
		
		
		transcount+=Integer.parseInt(irBcr.getTransactionCount());
		
		
		String[] convert = (irBcr.getBatchAmount()).split("\\.");
		String a=convert[0];	
	     String b = convert[1];
		a = a.replaceAll(",", "");
		String total =a+"."+b; 
		String value=String.valueOf(total);
		
		 batchamt = batchamt.add(new BigDecimal(value));
		 


	}
	  String dbAmount = batchamt.toString();
			String[] convert = dbAmount.split("\\.");
	        int a = Integer.parseInt(convert[0]);
	        String b = convert[1];

			NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
	     		String	batchAmount = numberFormat.format(a);
	     		
	     	String amountWithComma=null;
	       if(b.equals("00")){
		    	 amountWithComma=batchAmount+".00";
		      }else{
		    	amountWithComma=batchAmount+"."+b;  
		      }
dashboard.setTotalBatchamt(String.valueOf(amountWithComma));
dashboard.setTotalTransactions(String.valueOf(transcount));
	
	
String[] date1 = procesdate.split("-");

		dashboard.setFirstdate(procesdate);

	
	}else{
		String[] date1 = procesdate.split("-");

/*String month  = date1[0];
String day  = date1[1];
String year = date1[2];*/

		dashboard.setTotalBatchamt("0.00");
		dashboard.setTotalTransactions("0");
		dashboard.setTotalBatches(0);

		dashboard.setFirstdate(procesdate);
	}

Calendar cal = Calendar.getInstance();        
cal = Calendar.getInstance();
cal.add(Calendar.DATE, -1);
String secprocesdate=dateFormat.format(cal.getTime());


Query sectotalfile = getMyCurrentSession().createQuery("from IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr where fhr.processedDate=? and user.irMUsers.userid=? and user.isactive=?");
sectotalfile.setParameter(0,secprocesdate);
sectotalfile.setParameter(1,userId);
sectotalfile.setParameter(2,1);

	
	List<IrFPFileHeaderRecord> sectotalfilelist = sectotalfile.list();
	if(sectotalfilelist.size()>0){
		dashboard.setSectotalFiles(sectotalfilelist.size());
	}

    
	Query query = getMyCurrentSession().createQuery(" from IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr  inner join fhr.irFPBatchHeaderRecord as fbr where fhr.processedDate=? and user.irMUsers.userid=? and user.isactive=?");

	query.setParameter(0,secprocesdate);
	query.setParameter(1,userId);
	query.setParameter(2,1);
    
   
	List<?> querylist = query.list();
	if(querylist.size()>0){	
		BigDecimal batchamt = BigDecimal.ZERO;
		int transcount=0;
		int count=0;
		int debitamount=0;
		int creditamount=0;
		int filecount=0;
	     for(int i=0; i<querylist.size(); i++) {
		FileDetails = new IrFPBatchHeaderRecord();
		
		Object[] row = (Object[]) querylist.get(i);
		IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)row[2];
		IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)row[3];

		if(querylist.size()>0){	
			dashboard.setSectotalBatches(querylist.size());
		}
		
		else
		{
			dashboard.setSectotalBatches(0);
		}

		transcount+=Integer.parseInt(irBcr.getTransactionCount());
		String[] convert = (irBcr.getBatchAmount()).split("\\.");
		String a=convert[0];	
	     String b = convert[1];
		a = a.replaceAll(",", "");

		String total =a+"."+b; 
		String value=String.valueOf(total);	
		 batchamt = batchamt.add(new BigDecimal(value));
		 
		dashboard.setSecdate(secprocesdate);
		
	}
	     String dbAmount = batchamt.toString();
			String[] convert = dbAmount.split("\\.");
	        int a = Integer.parseInt(convert[0]);
	        String b = convert[1];

			NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
	     		String	batchAmount = numberFormat.format(a);
	     		
	     	String amountWithComma=null;
	       if(b.equals("00")){
		    	 amountWithComma=batchAmount+".00";
		      }else{
		    	amountWithComma=batchAmount+"."+b;  
		      }
		 dashboard.setSectotalCreditAmt(String.valueOf(amountWithComma));
	dashboard.setSectotalTransactions(String.valueOf(transcount));
	
	

	}else{

		dashboard.setSectotalTransactions("0");

		dashboard.setSectotalCreditAmt("0.00");
		dashboard.setSecdate(secprocesdate);
	}
	Calendar scal = Calendar.getInstance();        
    scal = Calendar.getInstance();
    scal.add(Calendar.DATE, -2);
    
    String thirdprocesdate=dateFormat.format(scal.getTime());
   
    Query thirdtotalfile = getMyCurrentSession().createQuery("from IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr where fhr.processedDate=? and user.irMUsers.userid=? and user.isactive=?");
    thirdtotalfile.setParameter(0,thirdprocesdate);
    thirdtotalfile.setParameter(1,userId);
    thirdtotalfile.setParameter(2,1);
    
 
	List<IrFPFileHeaderRecord> thirdtotalfilelist = thirdtotalfile.list();
	if(thirdtotalfilelist.size()>0){
		dashboard.setThirdtotalFiles(thirdtotalfilelist.size());
	}
    
	Query third = getMyCurrentSession().createQuery(" from IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr  inner join fhr.irFPBatchHeaderRecord as fbr where fhr.processedDate=? and user.irMUsers.userid=? and user.isactive=?");

	third.setParameter(0,thirdprocesdate);
	third.setParameter(1,userId);
	third.setParameter(2,1);


	List<?> thirdlist = third.list();
	if(thirdlist.size()>0){	
		
		BigDecimal batchamt = BigDecimal.ZERO;
		int transcount=0;
		int count=0;
		int debitamount=0;
		int creditamount=0;
		int filecount=0;
	for(int i=0; i<thirdlist.size(); i++) {
		FileDetails = new IrFPBatchHeaderRecord();
		
		Object[] row = (Object[]) thirdlist.get(i);
		IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)row[2];
		IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)row[3];
	

		if(thirdlist.size()>0){	
			dashboard.setThirdtotalBatches(thirdlist.size());
		}
		
		else
		{
			dashboard.setThirdtotalBatches(0);
		}
		transcount+=Integer.parseInt(irBcr.getTransactionCount());
		String[] convert = (irBcr.getBatchAmount()).split("\\.");
		String a=convert[0];	
	     String b = convert[1];
		a = a.replaceAll(",", "");
		String total =a+"."+b; 
		String value=String.valueOf(total);
		 batchamt = batchamt.add(new BigDecimal(value));

		dashboard.setThirddate(thirdprocesdate);
	}

	String dbAmount = batchamt.toString();
	String[] convert = dbAmount.split("\\.");
    int a = Integer.parseInt(convert[0]);
    String b = convert[1];

	NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
 		String	batchAmount = numberFormat.format(a);
 		
 	String amountWithComma=null;
   if(b.equals("00")){
    	 amountWithComma=batchAmount+".00";
      }else{
    	amountWithComma=batchAmount+"."+b;  
      }

	 dashboard.setThirdtotalCreditAmt(String.valueOf(amountWithComma));
	dashboard.setThirdtotalTransactions(String.valueOf(transcount));


	}else{
	
		dashboard.setThirdtotalTransactions("0");

		dashboard.setThirdtotalCreditAmt("0.00");
		dashboard.setThirddate(thirdprocesdate);
	}
	

			irPlusResponseDetails.setDashboardResponse(dashboard);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		} catch (Exception e) {
		
			LOGGER.error("Exception in listBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

			throw new HibernateException(e);

		}
		
		
		
		return irPlusResponseDetails;
	}
	
	
	
	@Override
	public IRPlusResponseDetails BankwiseFile(CustomerGroupingMngmntInfo user,Long branchId) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		Dashboard dashboard = new Dashboard();
		BankInfo bankDetails=null;
		IrFPBatchHeaderRecord FileDetails=null;
		
		try {
		
			Date date=new Date();
			DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");

			String procesdate=dateFormat.format(date);
			
			Query totalfile = getMyCurrentSession().createQuery("from IrMUsersBank as user inner join user.bank as bank inner join bank.irFPFileHeaderRecord as fhr where fhr.processedDate=? and fhr.bank.bankId=? and user.irMUsers.userid=? and user.isactive=?");
			totalfile.setParameter(0,procesdate);
			totalfile.setParameter(1,branchId);
			totalfile.setParameter(2,user.getUserid());
			totalfile.setParameter(3,1);
			List<IrFPFileHeaderRecord> totalfilelist = totalfile.list();
			
		
			if(totalfilelist.size()>0){
				dashboard.setTotalFiles(totalfilelist.size());
			}
			
			Query batch = getMyCurrentSession().createQuery("  from IrMUsersBank as user inner join user.bank as bank inner join bank.irFPFileHeaderRecord as fhr inner join fhr.irFPBatchHeaderRecord as fbr  where fhr.processedDate=? and fhr.bank.bankId=? and user.irMUsers.userid=? and user.isactive=?");

			batch.setParameter(0,procesdate);
			batch.setParameter(1,branchId);
			batch.setParameter(2,user.getUserid());
			batch.setParameter(3,1);

	List<?> list = batch.list();
	if(list.size()>0){	
	
		BigDecimal batchamt = BigDecimal.ZERO;
		int transcount=0;
		int count=0;
		int debitamount=0;
		int creditamount=0;
		int filecount=0;
		int itemcount=0;
	for(int i=0; i<list.size(); i++) {
		FileDetails = new IrFPBatchHeaderRecord();
		
		
		Object[] row = (Object[]) list.get(i);
		IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)row[2];
		IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)row[3];

		if(list.size()>0){	
			dashboard.setTotalBatches(list.size());
		}
		
		else
		{
			dashboard.setTotalBatches(0);
		}
		
		
		transcount+=Integer.parseInt(irBcr.getTransactionCount());
		String[] convert = (irBcr.getBatchAmount()).split("\\.");
		String a=convert[0];	
	     String b = convert[1];
		a = a.replaceAll(",", "");
		String total =a+"."+b; 
		String value=String.valueOf(total);
		 batchamt = batchamt.add(new BigDecimal(value));

		itemcount+=Double.parseDouble(irBcr.getItemCount());

		
		
	}
	String dbAmount = batchamt.toString();
	String[] convert = dbAmount.split("\\.");
    int a = Integer.parseInt(convert[0]);
    String b = convert[1];

	NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
 		String	batchAmount = numberFormat.format(a);
 		
 	String amountWithComma=null;
   if(b.equals("00")){
    	 amountWithComma=batchAmount+".00";
      }else{
    	amountWithComma=batchAmount+"."+b;  
      }


	 dashboard.setTotalBatchamt(String.valueOf(amountWithComma));
	dashboard.setTotalItemcount(String.valueOf(itemcount));
	dashboard.setTotalTransactions(String.valueOf(transcount));
		
		dashboard.setFirstdate(procesdate);

	
	}else{
		dashboard.setTotalItemcount("0");
		dashboard.setTotalBatchamt("0.00");
		dashboard.setTotalTransactions("0");
		dashboard.setTotalBatches(0);

		dashboard.setFirstdate(procesdate);
	}
			
			
			
	
Calendar cal = Calendar.getInstance();        
cal = Calendar.getInstance();
cal.add(Calendar.DATE, -1);
String secprocesdate=dateFormat.format(cal.getTime());


Query sectotalfile = getMyCurrentSession().createQuery(" from IrMUsersBank as user inner join user.bank as bank inner join bank.irFPFileHeaderRecord as fhr where fhr.processedDate=? and fhr.bank.bankId=? and user.irMUsers.userid=? and user.isactive=?");
sectotalfile.setParameter(0,secprocesdate);
sectotalfile.setParameter(1,branchId);
sectotalfile.setParameter(2,user.getUserid());
sectotalfile.setParameter(3,1);

List<IrFPFileHeaderRecord> sectotalfilelist = sectotalfile.list();
if(sectotalfilelist.size()>0){
	dashboard.setSectotalFiles(sectotalfilelist.size());
}
	
Query secbatch = getMyCurrentSession().createQuery("  from IrMUsersBank as user inner join user.bank as bank inner join bank.irFPFileHeaderRecord as fhr inner join fhr.irFPBatchHeaderRecord as fbr  where fhr.processedDate=? and fhr.bank.bankId=? and user.irMUsers.userid=? and user.isactive=?");

secbatch.setParameter(0,secprocesdate);
secbatch.setParameter(1,branchId);
secbatch.setParameter(2,user.getUserid());
secbatch.setParameter(3,1);

	List<?> querylist = secbatch.list();
	if(querylist.size()>0){	
		BigDecimal batchamt = BigDecimal.ZERO;
		int transcount=0;
		int count=0;
		int debitamount=0;
		int creditamount=0;
		int filecount=0;
		int itemcount=0;
	     for(int i=0; i<querylist.size(); i++) {
		FileDetails = new IrFPBatchHeaderRecord();
		
		Object[] row = (Object[]) querylist.get(i);
		IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)row[2];
		IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)row[3];

		if(querylist.size()>0){	
			dashboard.setSectotalBatches(querylist.size());
		}
		
		else
		{
			dashboard.setSectotalBatches(0);
		}

		
		
		itemcount+=Double.parseDouble(irBcr.getItemCount());
		transcount+=Integer.parseInt(irBcr.getTransactionCount());
		String[] convert = (irBcr.getBatchAmount()).split("\\.");
		String a=convert[0];	
	     String b = convert[1];
		a = a.replaceAll(",", "");
		String total =a+"."+b; 
		String value=String.valueOf(total);
		 batchamt = batchamt.add(new BigDecimal(value));
		 
		dashboard.setSecdate(secprocesdate);
		
	}
	     String dbAmount = batchamt.toString();
			String[] convert = dbAmount.split("\\.");
	        int a = Integer.parseInt(convert[0]);
	        String b = convert[1];

			NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
	     		String	batchAmount = numberFormat.format(a);
	     		
	     	String amountWithComma=null;
	       if(b.equals("00")){
		    	 amountWithComma=batchAmount+".00";
		      }else{
		    	amountWithComma=batchAmount+"."+b;  
		      }


		 dashboard.setSectotalCreditAmt(String.valueOf(amountWithComma));
		 
	dashboard.setSectotalTransactions(String.valueOf(transcount));
	dashboard.setSecitemcount(String.valueOf(itemcount));
	

	}else{
		dashboard.setSecitemcount("0");
		dashboard.setSectotalTransactions("0");
		dashboard.setSectotalCreditAmt("0.00");
		dashboard.setSecdate(secprocesdate);
	}
	Calendar scal = Calendar.getInstance();        
    scal = Calendar.getInstance();
    scal.add(Calendar.DATE, -2);
    
    String thirdprocesdate=dateFormat.format(scal.getTime());

	Query thirdtotalfile = getMyCurrentSession().createQuery(" from IrMUsersBank as user inner join user.bank as bank inner join bank.irFPFileHeaderRecord as fhr where fhr.processedDate=? and fhr.bank.bankId=? and user.irMUsers.userid=? and user.isactive=?");
	thirdtotalfile.setParameter(0,thirdprocesdate);
	thirdtotalfile.setParameter(1,branchId);
	thirdtotalfile.setParameter(2,user.getUserid());
	thirdtotalfile.setParameter(3,1);

	List<IrFPFileHeaderRecord> thirdtotalfilelist = thirdtotalfile.list();
	if(thirdtotalfilelist.size()>0){
		dashboard.setThirdtotalFiles(thirdtotalfilelist.size());
	}
	
	
Query thirdbatch = getMyCurrentSession().createQuery("  from IrMUsersBank as user inner join user.bank as bank inner join bank.irFPFileHeaderRecord as fhr inner join fhr.irFPBatchHeaderRecord as fbr  where fhr.processedDate=? and fhr.bank.bankId=? and user.irMUsers.userid=? and user.isactive=?");

thirdbatch.setParameter(0,thirdprocesdate);
thirdbatch.setParameter(1,branchId);
thirdbatch.setParameter(2,user.getUserid());
thirdbatch.setParameter(3,1);

	List<?> thirdlist = thirdbatch.list();
	if(thirdlist.size()>0){	
		int itemcount=0;
		BigDecimal batchamt = BigDecimal.ZERO;
		int transcount=0;
		int count=0;
		int debitamount=0;
		int creditamount=0;
		int filecount=0;
	for(int i=0; i<thirdlist.size(); i++) {
		FileDetails = new IrFPBatchHeaderRecord();
		
		Object[] row = (Object[]) thirdlist.get(i);
		IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)row[2];
		IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)row[3];
	

		if(thirdlist.size()>0){	
			dashboard.setThirdtotalBatches(thirdlist.size());
		}
		
		else
		{
			dashboard.setThirdtotalBatches(0);
		}
		transcount+=Integer.parseInt(irBcr.getTransactionCount());
		String[] convert = (irBcr.getBatchAmount()).split("\\.");
		String a=convert[0];	
	     String b = convert[1];
		a = a.replaceAll(",", "");
		String total =a+"."+b; 
		String value=String.valueOf(total);
		 batchamt = batchamt.add(new BigDecimal(value));
		 
		itemcount+=Double.parseDouble(irBcr.getItemCount());
	
	
		dashboard.setThirddate(thirdprocesdate);
	}
	String dbAmount = batchamt.toString();
	String[] convert = dbAmount.split("\\.");
    int a = Integer.parseInt(convert[0]);
    String b = convert[1];

	NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
 		String	batchAmount = numberFormat.format(a);
 		
 	String amountWithComma=null;
   if(b.equals("00")){
    	 amountWithComma=batchAmount+".00";
      }else{
    	amountWithComma=batchAmount+"."+b;  
      }

	 dashboard.setThirdtotalCreditAmt(String.valueOf(amountWithComma));
	 
	dashboard.setThirditemcount(String.valueOf(itemcount));
	dashboard.setThirdtotalTransactions(String.valueOf(transcount));
	

	}else{
		dashboard.setThirditemcount("0");
		dashboard.setThirdtotalTransactions("0");
		dashboard.setThirdtotalCreditAmt("0.00");
		dashboard.setThirddate(thirdprocesdate);
	}	

	irPlusResponseDetails.setDashboardResponse(dashboard);
	irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		} catch (Exception e) {
		
			LOGGER.error("Exception in listBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

			throw new HibernateException(e);

		}
		
		
		
		return irPlusResponseDetails;
	}
}
