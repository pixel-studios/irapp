package com.irplus.dao;

import de.schlichtherle.license.CipherParam;

/**
 *
 * @author sujay
 */
public class CipherParamImpl implements CipherParam{
    private String keyPwd;
    
    public CipherParamImpl(final String keyPwd){
        this.keyPwd = keyPwd;
    }

    public String getKeyPwd() {
        return this.keyPwd;
    }

}