package com.irplus.dao.roles;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.hibernate.entities.IrMRoles;
import com.irplus.dao.hibernate.entities.IrMUsers;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.roles.RoleBean;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;

@Transactional
@Component

public class RoleDaoImpl implements IRolesDao {

	private static final Log log = LogFactory.getLog(RoleDaoImpl.class);
	
	@Autowired
	SessionFactory sessionFactory;
	
	public Session getMyCurrentSession() {		
		Session session =null;
		
		try {	
		session=sessionFactory.getCurrentSession();
		}catch (HibernateException e) {
		log.error("Exception raised DaoImpl :: At current session creation time ::"+e);
		}		
		return session; 
	}

	@Override
	public IRPlusResponseDetails createRole(RoleBean roleBean) throws BusinessException {

		IRPlusResponseDetails roleResponseDetails = new IRPlusResponseDetails();
		
		log.info("Inside RoleDaoImpl :: RoleCreate");
		
		IrMRoles irMRoles = new IrMRoles();
		
		irMRoles.setRolename(roleBean.getRolename());
		irMRoles.setBankVisibility(roleBean.getBankVisibility());
		irMRoles.setIsrestricted(roleBean.getIsrestricted());
		irMRoles.setIsactive(roleBean.getStatusId());

		/*	if(roleBean.getStatusId()!=null) {		
		IrMStatus irMStatus = (IrMStatus)getMyCurrentSession().get(IrMStatus.class, roleBean.getStatusId());		
		log.debug("Inside RoleDaoImpl :: RoleCreate :: roleBean.getStatusId()"+roleBean.getStatusId());	
		irMRoles.setIrMStatus(irMStatus);	
		}	else {			
			log.debug("Inside RoleDaoImpl :: RoleCreate :: roleBean.getStatusId() is null :"+roleBean.getStatusId());			
		}
		 */	
		
		if(roleBean.getUserId()!=null) {
			
		IrMUsers irMUsers  = (IrMUsers)getMyCurrentSession().get(IrMUsers.class, roleBean.getUserId());
		
		log.debug("Inside RoleDaoImpl :: RoleCreate :: available getUserId()"+roleBean.getUserId());
		
		irMRoles.setIrMUsers(irMUsers);
		
		}else {
		
			log.debug("Inside RoleDaoImpl :: RoleCreate :: not available getUserId()"+roleBean.getUserId());
		}
		getMyCurrentSession().save(irMRoles);
		
		roleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
		roleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		
		return roleResponseDetails;
	}
	
      /* Create Without Duplicate Role, Remove the Duplicate  Role */
	
	public IRPlusResponseDetails removeDuplicateRole(RoleBean roleBean) throws BusinessException {

		IRPlusResponseDetails roleResponseDetails = new IRPlusResponseDetails();
		List<String> errMsg = new ArrayList<String>();	
		
		log.info("Inside RoleDaoImpl :: RoleCreate");
		List<String> errorString = new ArrayList<String>();
		boolean flag=false;
		IrMRoles irMRoles = new IrMRoles();
		
		try{
			
		Criteria criteria = getMyCurrentSession().createCriteria(IrMRoles.class);
		List<IrMRoles> list = criteria.list();
		
			if(list != null && (!list.isEmpty())){
				
						for(IrMRoles irMRoles1 : list){			
								if(roleBean.getRolename().equalsIgnoreCase(irMRoles1.getRolename())){			
									flag=true;
									break;
								}			
						}						
			}else{
				// create role  ..... list is empty
				
							if(roleBean.getUserId()!=null) {
								
								IrMUsers irMUsers  = (IrMUsers)getMyCurrentSession().get(IrMUsers.class, roleBean.getUserId());
								
								log.debug("Inside RoleDaoImpl :: RoleCreate :: available getUserId()"+roleBean.getUserId());
								
								irMRoles.setIrMUsers(irMUsers);
								
								}else {
								
									log.debug("Inside RoleDaoImpl :: RoleCreate :: not available getUserId()"+roleBean.getUserId());
									
									log.debug("Userid not entered");
									
									errMsg.add("Error :: In Role :"+roleBean.getUserId()+" :UserId not included");
									roleResponseDetails.setErrMsgs(errMsg);
									roleResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
									roleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
									errorString.add("Error :: Userid not available");
									roleResponseDetails.setErrMsgs(errorString);
								}
				
						log.info("Inside RoleDaoImpl :: RoleCreate");
						
						irMRoles.setRolename(roleBean.getRolename());
						irMRoles.setBankVisibility(roleBean.getBankVisibility());
						irMRoles.setIsrestricted(roleBean.getIsrestricted());
						irMRoles.setIsactive(roleBean.getStatusId());

						getMyCurrentSession().save(irMRoles);
							
							log.debug("Role Data saved");
							
							errMsg.add("Success :: Role successfully saved in Db");
							roleResponseDetails.setErrMsgs(errMsg);
							roleResponseDetails.setValidationSuccess(true);
							roleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
							roleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
							errorString.add("Success :: Data saved DB");
							roleResponseDetails.setErrMsgs(errorString);
			}
			if(flag){
			
				log.debug("Duplicate data not entered");
				roleResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				roleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				errorString.add("Error :: Duplicate name entered");
				roleResponseDetails.setErrMsgs(errorString);
			
			}else{
				// create role  ..... unique role
				
							if(roleBean.getUserId()!=null) {
								
								IrMUsers irMUsers  = (IrMUsers)getMyCurrentSession().get(IrMUsers.class, roleBean.getUserId());
								
								log.debug("Inside RoleDaoImpl :: RoleCreate :: available getUserId()"+roleBean.getUserId());
								
								irMRoles.setIrMUsers(irMUsers);
								
								}else {
								
									log.debug("Inside RoleDaoImpl :: Role Create :: not available getUserId()"+roleBean.getUserId());
									log.debug("Userid not entered");
									
									errMsg.add("Error :: In Role :"+roleBean.getUserId()+" :UserId not included");
									roleResponseDetails.setErrMsgs(errMsg);
									
									roleResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
									roleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
									errorString.add("Error :: Userid not available");
									roleResponseDetails.setErrMsgs(errorString);
								}
				
						log.info("Inside RoleDaoImpl :: RoleCreate");
						
						irMRoles.setRolename(roleBean.getRolename());
						irMRoles.setBankVisibility(roleBean.getBankVisibility());
						irMRoles.setIsrestricted(roleBean.getIsrestricted());
						irMRoles.setIsactive(roleBean.getStatusId());
			
						getMyCurrentSession().save(irMRoles);
							
							log.debug("Role Data saved");

							errMsg.add("Success :: Role successfully saved in Db");
							roleResponseDetails.setErrMsgs(errMsg);
							
							roleResponseDetails.setValidationSuccess(true);
							roleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
							roleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
							errorString.add("Success :: Data saved DB");
							roleResponseDetails.setErrMsgs(errorString);												
				}
			
		}catch(Exception e){
			roleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			roleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception Raised inside RoldeDaoImpl :: getRoleId : "+e);
			throw new HibernateException(e);
		}
		
		return roleResponseDetails;
	}
	
	
	@Override
	public IRPlusResponseDetails getRoleById(String roleId) throws BusinessException {


		log.debug("Inside RoleDaoImpl :: getRoleById");
		
		IRPlusResponseDetails roleResponseDetails = new IRPlusResponseDetails();
		IrMRoles irMRoles =null;
		RoleBean roleBean = new RoleBean();
		List<String> errMsg = new ArrayList<String>();	
		List<RoleBean> listRoleBean = new ArrayList<RoleBean>();
		
		try {
			
			irMRoles =(IrMRoles) getMyCurrentSession().get(IrMRoles.class, Integer.parseInt(roleId));
			
			if (irMRoles!=null) {
				
				roleBean.setUserId(irMRoles.getRoleid());
				roleBean.setRolename(irMRoles.getRolename());
				roleBean.setBankVisibility(irMRoles.getBankVisibility());
				roleBean.setStatusId(irMRoles.getIsactive());
				roleBean.setIsrestricted(irMRoles.getIsrestricted());
				roleBean.setCreateddate(irMRoles.getCreateddate());
				roleBean.setModifieddate(irMRoles.getModifieddate());
				roleBean.setRoleid(irMRoles.getRoleid());
				
				listRoleBean.add(roleBean);
				
				roleResponseDetails.setRoleBean(roleBean);

				errMsg.add("Success : "+roleBean.getRoleid()+" : role Available in Db ");
				roleResponseDetails.setErrMsgs(errMsg);
				roleResponseDetails.setValidationSuccess(true);
				roleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				roleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				
			} else {
				errMsg.add("Error : "+roleBean.getRoleid()+" : role not Available in Db ");
				roleResponseDetails.setErrMsgs(errMsg);
				roleResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				roleResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);				
			}
			
		} catch (Exception e) {

			roleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			roleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception Raised inside RoldeDaoImpl :: getRoleId : "+e);
			throw new HibernateException(e);
		}
				
		return roleResponseDetails;
	}	

	
/*Without duplicate update role*/
	@Override
	public IRPlusResponseDetails updateRole(RoleBean roleBean) throws BusinessException {
		log.info("Inside RoleDaoImpl ");
		log.debug("Update Role instance ");
		List<String> errMsg = new ArrayList<String>();					
		
		IRPlusResponseDetails roleResponseDetails = new IRPlusResponseDetails();
		boolean flag = false;
		try{
			//
			Criteria criteria = getMyCurrentSession().createCriteria(IrMRoles.class);
			List<IrMRoles> list =criteria.list();
			
			for (IrMRoles irMRoles : list) {
				if(irMRoles.getRoleid()!=roleBean.getRoleid()){
				
					if(irMRoles.getRolename().equalsIgnoreCase(roleBean.getRolename())){
						flag=true;
						break;
					}
				}				
			}
			
			  if(flag){
					errMsg.add("Error : "+roleBean.getRolename()+" : role name is Duplicated  :: Please Enter Unique name ");
					roleResponseDetails.setErrMsgs(errMsg);
					roleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					roleResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				  
			  }else{
					IrMRoles irMRoles = (IrMRoles) getMyCurrentSession().get(IrMRoles.class, roleBean.getRoleid());
					
					irMRoles.setIrMUsers((IrMUsers)getMyCurrentSession().get(IrMUsers.class,roleBean.getUserId()));
							
					irMRoles.setBankVisibility(roleBean.getBankVisibility());
					irMRoles.setCreateddate(irMRoles.getCreateddate());
					irMRoles.setRolename(roleBean.getRolename());
					irMRoles.setIsrestricted(roleBean.getIsrestricted());
					irMRoles.setIsactive(roleBean.getStatusId());
					
					getMyCurrentSession().update(irMRoles);
					
					errMsg.add("Success : "+roleBean.getRoleid()+" : role updated ");
					roleResponseDetails.setErrMsgs(errMsg);
					roleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
					roleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
					roleResponseDetails.setValidationSuccess(true);
			  }
		
		}catch (Exception e) {

			roleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			roleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception Raised inside RoldeDaoImpl :: updateRoleId : "+e);
		
			throw new HibernateException(e);
		}
		return roleResponseDetails;		
	}
	
 /* Role Status update*/
	
	public IRPlusResponseDetails updateRoleStatus(StatusIdInfo statusIbInfo) throws BusinessException {
		log.info("Inside RoleDaoImpl ");
		log.debug("Update Role instance ");
		List<String> errMsg = new ArrayList<String>();					
		
		IRPlusResponseDetails roleResponseDetails = new IRPlusResponseDetails();
		
		try{
			//
		
			  if(statusIbInfo.getId()!=null && statusIbInfo.getStatus()!=null){
			
				  Criteria criteria = getMyCurrentSession().createCriteria(IrMRoles.class);
					List<IrMRoles> list = criteria.list();
					
				  
				  
				  IrMRoles irMRoles = (IrMRoles) getMyCurrentSession().get(IrMRoles.class, statusIbInfo.getId());
					
					irMRoles.setIsactive(statusIbInfo.getStatus());
					
					getMyCurrentSession().update(irMRoles);
					
					errMsg.add("Success : Role Status updated : status is :" +statusIbInfo.getStatus());
					roleResponseDetails.setErrMsgs(errMsg);
					roleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
					roleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
					roleResponseDetails.setValidationSuccess(true);
			  }else{
					errMsg.add("Error :  role id and status id not available ");
					roleResponseDetails.setErrMsgs(errMsg);
					roleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					roleResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
			
			  }
		
		}catch (Exception e) {

			roleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			roleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception Raised inside RoldeDaoImpl :: updateRoleId : "+e);
		
			throw new HibernateException(e);
		}
		return roleResponseDetails;		
	}
	
	
	
	@Override
	public IRPlusResponseDetails deleteRoleById(String roleId) throws BusinessException {

		log.info("Inside RoleDaoImpl");
		log.debug("deleting Role instance");
		List<String> errMsg = new ArrayList<String>();			
		
		IRPlusResponseDetails roleResponseDetails = new IRPlusResponseDetails();
		try{	
			Query query = sessionFactory.getCurrentSession().createQuery("delete IrMRoles d where d.roleid =:role_Id");
			query.setParameter("role_Id", Integer.parseInt(roleId));
			
			int result=query.executeUpdate();
			if(result==0){
				
				log.debug("delete country failed to delete");
				
				errMsg.add("Error : Failed to Delete role :"+roleId);
				roleResponseDetails.setErrMsgs(errMsg);
				roleResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				roleResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				
			}else {
				log.debug("succcessfully deleted role :: roleId"+roleId);
				
				errMsg.add("Success : Deleted role :"+roleId);
				roleResponseDetails.setErrMsgs(errMsg);
				roleResponseDetails.setValidationSuccess(true);
				roleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				roleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
			
			
		}catch (Exception e) {
			log.error("Exception raised in delete role",e);
			roleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			roleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			throw new HibernateException(e);
		}
		return roleResponseDetails;
	}

	@Override
	public IRPlusResponseDetails ShowAllRoles() throws BusinessException {

		log.info("Inside RoleDaoImpl");
		log.debug("Show all role instances");
		List<String> errMsg = new ArrayList<String>();	
		IRPlusResponseDetails roleResponseDetails = new IRPlusResponseDetails();
		List<RoleBean> listRoleBean = new ArrayList<RoleBean>();
		
		try {
			
			//Criteria criteria = getMyCurrentSession().createCriteria(IrMRoles.class);
			Query roles = getMyCurrentSession().createQuery("from IrMRoles irMRoles where irMRoles.isrestricted=?");
			roles.setParameter(0,1);
			
			List<IrMRoles> list = roles.list();
			
			if (!list.isEmpty()) {
							
				for (IrMRoles irMRoles : list) {
			
					RoleBean roleBean = new RoleBean();
					
					roleBean.setRoleid(irMRoles.getRoleid());
					roleBean.setStatusId(irMRoles.getIsactive());
					roleBean.setUserId(irMRoles.getIrMUsers().getUserid());
					roleBean.setRolename(irMRoles.getRolename());
					roleBean.setBankVisibility(irMRoles.getBankVisibility());
					roleBean.setCreateddate(irMRoles.getCreateddate());
					roleBean.setModifieddate(irMRoles.getModifieddate());
					roleBean.setIsrestricted(irMRoles.getIsrestricted());
					
					listRoleBean.add(roleBean);
				}
				roleResponseDetails.setRoleBeans(listRoleBean);
				
				roleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				roleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				errMsg.add("Success : All roles :"+list.size());
				roleResponseDetails.setErrMsgs(errMsg);
				roleResponseDetails.setRecordsFiltered(list.size());
				roleResponseDetails.setRecordsTotal(list.size());
				
			} else {
				log.debug("Roles not available :: Empty Data :: Data required to show roles");
				
				errMsg.add("Error :No roles available ");
				roleResponseDetails.setErrMsgs(errMsg);
				roleResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				roleResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				
			}
			
		} catch (Exception e) {
			log.error("Exception raised in show list roles",e);
			roleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			roleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			throw new HibernateException(e);
		}
		
		return roleResponseDetails;
	}
	
	

	public IRPlusResponseDetails ShowAllRoles_roleprm() throws BusinessException {

		log.info("Inside RoleDaoImpl");
		log.debug("Show all role instances");
		List<String> errMsg = new ArrayList<String>();	
		IRPlusResponseDetails roleResponseDetails = new IRPlusResponseDetails();
		List<RoleBean> listRoleBean = new ArrayList<RoleBean>();
		
		try {
			
			//Criteria criteria = getMyCurrentSession().createCriteria(IrMRoles.class);
			Query roles = getMyCurrentSession().createQuery("from IrMRoles irMRoles where irMRoles.isrestricted=?");
			roles.setParameter(0,1);
			
			List<IrMRoles> list = roles.list();
			
			if (!list.isEmpty()) {
							
				for (IrMRoles irMRoles : list) {
					
					if(irMRoles.getIsactive()!=0 && irMRoles.getIsactive()!=2 ){
					RoleBean roleBean = new RoleBean();
					
					roleBean.setRoleid(irMRoles.getRoleid());
					roleBean.setStatusId(irMRoles.getIsactive());
					roleBean.setUserId(irMRoles.getIrMUsers().getUserid());
					roleBean.setRolename(irMRoles.getRolename());
					roleBean.setBankVisibility(irMRoles.getBankVisibility());
					roleBean.setCreateddate(irMRoles.getCreateddate());
					roleBean.setModifieddate(irMRoles.getModifieddate());
					roleBean.setIsrestricted(irMRoles.getIsrestricted());
					
					listRoleBean.add(roleBean);
					}
				}
			
				if(!listRoleBean.isEmpty()){
				
					roleResponseDetails.setRoleBeans(listRoleBean);
				roleResponseDetails.setValidationSuccess(true);
			//	roleResponseDetails.set
				roleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				roleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				errMsg.add("Success : All roles :"+listRoleBean.size());
				roleResponseDetails.setErrMsgs(errMsg);
				roleResponseDetails.setRecordsFiltered(listRoleBean.size());
				roleResponseDetails.setRecordsTotal(listRoleBean.size());
				}
				else{
					errMsg.add("Message Alert : Roles are Empty ");
					roleResponseDetails.setErrMsgs(errMsg);
					roleResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
					roleResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				}
				
			} else {
				log.debug("Roles not available :: Empty Data :: Data required to show roles");
				
				errMsg.add("Error :No roles available ");
				roleResponseDetails.setErrMsgs(errMsg);
				roleResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				roleResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				
			}
			
		} catch (Exception e) {
			log.error("Exception raised in show list roles",e);
			roleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			roleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			throw new HibernateException(e);
		}
		
		return roleResponseDetails;
	}
		
}
