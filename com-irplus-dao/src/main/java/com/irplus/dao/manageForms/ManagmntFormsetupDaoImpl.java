package com.irplus.dao.manageForms;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.hibernate.entities.IrMFieldtypes;
import com.irplus.dao.hibernate.entities.IrMUsers;
import com.irplus.dao.hibernate.entities.IrSBankBranch;
import com.irplus.dao.hibernate.entities.IrSCustomerOtherlicensing;
import com.irplus.dao.hibernate.entities.IrSCustomercontacts;
import com.irplus.dao.hibernate.entities.IrSCustomers;
import com.irplus.dao.hibernate.entities.IrSFormsetup;
import com.irplus.dao.hibernate.entities.IrSFormvalidation;
import com.irplus.dao.hibernate.entities.IrsCustomerGroupingDetails;
import com.irplus.dto.BusinessProcessInfo;
import com.irplus.dto.CustomerBean;
import com.irplus.dto.CustomerContactsBean;
import com.irplus.dto.CustomerGroupingMngmntInfo;
import com.irplus.dto.DataEntryFormValidationDTO;
import com.irplus.dto.FieldTypesInfo;
import com.irplus.dto.FormsManagementDTO;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;


// started 12 20 17 

@Transactional
@Component
public class ManagmntFormsetupDaoImpl implements IManagmntFormsetupDao{
	
	private static final Logger log = Logger.getLogger(ManagmntFormsetupDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public IRPlusResponseDetails createMngmntformSetup(FormsManagementDTO formsManagementDTO) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		List<String> errMsgs = new ArrayList<String>();
		Session session = sessionFactory.getCurrentSession();
		
		log.debug(" Inside of the ManagmntFormsetupDaoImpl :: createMngmntformSetup ");
	
		try{
			boolean flag = true;
			
			if (formsManagementDTO.getFormCode()==null) {				
				flag = false;				
				errMsgs.add("Validation Error : FormCode not entered ");				
			}
			if (formsManagementDTO.getFormName()==null) {
				flag = false;
				errMsgs.add("Validation Error : FormName not entered ");
			}
			if (formsManagementDTO.getBatchClassDataentry()==null) {
				flag = false;
				errMsgs.add("Validation Error : BatchClassDataEntry not entered ");
			}
			if (formsManagementDTO.getBatchClassValidation()==null) {
				flag = false;
				errMsgs.add("Validation Error : BatchClassValidation not entered ");
			}
			if (formsManagementDTO.getWatchFolderDataentry()==null) {
				flag = false;
				errMsgs.add("Validation Error : WatchFolderDataentry not entered ");
			}
			if (formsManagementDTO.getWatchFolderValidation()==null) {
				flag = false;
				errMsgs.add("Validation Error : WatchFolderValidation not entered ");
			}
			if(formsManagementDTO.getUserid() == null){
				flag = false;
				errMsgs.add("Validation Error : Userid not entered ");
			}
			if(formsManagementDTO.getCustomerid() == null){
				flag = false;
				errMsgs.add("Validation Error : Customerid not entered ");
			}
			if(formsManagementDTO.getDataEntryFormValidationDTO().length == 0){
				flag = false;
				errMsgs.add("Validation Error : DataEntry Form Validation not entered ");
			}else{
				DataEntryFormValidationDTO[] dtt = formsManagementDTO.getDataEntryFormValidationDTO();
				
				if(dtt[0].getFieldName()==null){
					flag = false;
					errMsgs.add("Validation Error : Field Name not entered ");
				}
				if(dtt[0].getFieldLength() == null){
					flag = false;
					errMsgs.add("Validation Error : Field Length not entered ");
				}
			
			}
			
			Integer id = null;
			
			if (flag) {
				
				IrSFormsetup irs = new IrSFormsetup();
				
				irs.setFormCode(formsManagementDTO.getFormCode());
				irs.setFormName(formsManagementDTO.getFormName());
				irs.setBatchClassDataentry(formsManagementDTO.getBatchClassDataentry());
				irs.setBatchClassValidation(formsManagementDTO.getBatchClassValidation());
				
				irs.setIrMUsers((IrMUsers)session.get(IrMUsers.class, formsManagementDTO.getUserid()));
				irs.setIrSBankbranch((IrSBankBranch)session.get(IrSBankBranch.class , formsManagementDTO.getBankbranchId()));
				irs.setIrSCustomers((IrSCustomers)session.get(IrSCustomers.class, formsManagementDTO.getCustomerid()));
				
				irs.setWatchFolderDataentry(formsManagementDTO.getWatchFolderDataentry());
				irs.setWatchFolderValidation(formsManagementDTO.getWatchFolderValidation());
				
				irs.setIsactive(1);
				irs.setIsdefault(formsManagementDTO.getIsdefault());
				
				 id =(Integer) session.save(irs);
				session.flush();
				
				log.debug(" formsetupid :" + id);
				
				if(id!=null){
					Integer id1 = null;
					DataEntryFormValidationDTO[] defv = formsManagementDTO.getDataEntryFormValidationDTO();
					
					for(int i=0; i<defv.length; i++){
					
					IrSFormvalidation irsf = new IrSFormvalidation();
									
					irsf.setFieldName(defv[i].getFieldName());
					irsf.setFieldLength(defv[i].getFieldLength());					
					irsf.setHasValidation(defv[i].getHasValidation());				
					irsf.setIrMFieldtypes((IrMFieldtypes)session.get(IrMFieldtypes.class, defv[i].getFieldtypid()));
					
					irsf.setIrMUsers((IrMUsers)session.get(IrMUsers.class, formsManagementDTO.getUserid()));
					irsf.setIrSFormsetup((IrSFormsetup)session.get(IrSFormsetup.class, id));
					irsf.setIsactive(1);
					
					id1 = (Integer)session.save(irsf);
									
					}
					if(id1!=null){
						
						errMsgs.add("Success :: Saved Data ");
						irPlusResponseDetails.setErrMsgs(errMsgs);				
						irPlusResponseDetails.setValidationSuccess(true);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
						irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
					}
					
				}else{
					errMsgs.add("Validation Error : IrSFormsetup id null ");
					irPlusResponseDetails.setErrMsgs(errMsgs);				
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				}
				
				
			} else {
				irPlusResponseDetails.setErrMsgs(errMsgs);				
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				
			}
			
			
		}catch (Exception e) {
			log.error("Exception in create createMngmntformSetup",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		
		return irPlusResponseDetails;
	}

	public IRPlusResponseDetails updateMngmntformSetup(FormsManagementDTO formsManagementDTO) throws BusinessException {
	
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		List<String> errMsgs = new ArrayList<String>();
		Session session = sessionFactory.getCurrentSession();
		
		log.debug(" Inside of the ManagmntFormsetupDaoImpl :: updateMngmntformSetup ");
		IrSFormsetup fs = null;
		boolean validFlag = true;
		IrMUsers user = null;
		IrSCustomers customer = null;
		IrSBankBranch  bankbranch = null;
		try{
			
				if(formsManagementDTO.getFormSetupId()!=null){
					fs =(IrSFormsetup) session.createCriteria(IrSFormsetup.class , "formsetup")
						.add(Restrictions.eq("formSetupId", formsManagementDTO.getFormSetupId())).uniqueResult();									
				}else{
					errMsgs.add("Validation Error : Please Enter formSetupId");
					validFlag = false;
				}
				
				if(formsManagementDTO.getUserid()!=null){
					user = (IrMUsers) session.get(IrMUsers.class, formsManagementDTO.getUserid());
				}else{
					errMsgs.add("Validation Error : Please Enter UserId");
					validFlag = false;
				}
				
				if(formsManagementDTO.getCustomerid()!=null){
					customer =(IrSCustomers) session.get(IrSCustomers.class, formsManagementDTO.getCustomerid());
				}else{
					errMsgs.add("Validation Error :: Please Enter CustomerId");
					validFlag = false;
				}
				
				if(formsManagementDTO.getBankbranchId()!=null){
					bankbranch =(IrSBankBranch) session.get(IrSBankBranch.class, formsManagementDTO.getBankbranchId());
				}else{
					errMsgs.add("Validation Error :: Please Enter BankbranchId");
					validFlag = false;
				}
				if(formsManagementDTO.getDataEntryFormValidationDTO().length==0){
					errMsgs.add("Validation Error :: Please Enter Data Entry Form Validation Fields");
					validFlag = false;
				}
			
			if(fs!=null && validFlag){
															
				fs.setIrMUsers(user);
				fs.setIrSBankbranch(bankbranch);
				fs.setIrSCustomers(customer);
				
				fs.setBatchClassDataentry(formsManagementDTO.getBatchClassDataentry());
				fs.setBatchClassValidation(formsManagementDTO.getBatchClassValidation());
				fs.setFormCode(formsManagementDTO.getFormCode());
				fs.setFormName(formsManagementDTO.getFormName());
				
				fs.setIsdefault(formsManagementDTO.getIsdefault());
				fs.setWatchFolderDataentry(formsManagementDTO.getWatchFolderDataentry());
				fs.setWatchFolderValidation(formsManagementDTO.getWatchFolderValidation());
				
				session.update(fs);
			//	fs.setIrSFormvalidations(irSFormvalidations);
								
				List<IrSFormvalidation> irslist = session.createCriteria(IrSFormvalidation.class , "formvald")
				.add(Restrictions.like("formvald.irMFieldtypes.fieldTypeId", formsManagementDTO.getFormSetupId())).list();
				
				
				DataEntryFormValidationDTO[] defv  =  formsManagementDTO.getDataEntryFormValidationDTO();
				
				boolean flag = false;
				
				for (DataEntryFormValidationDTO dataEntryFormValidationDTO : defv){
					
				flag = true;
				
							for (IrSFormvalidation irSFormvalidation : irslist) {
								
							//	if(irSFormvalidation.getIsactive()!=2){
								
								if (dataEntryFormValidationDTO.getValidationId()==irSFormvalidation.getValidationId() ){
												
									irSFormvalidation.setFieldLength(dataEntryFormValidationDTO.getFieldLength());
									irSFormvalidation.setFieldName(dataEntryFormValidationDTO.getFieldName());
									irSFormvalidation.setHasValidation(dataEntryFormValidationDTO.getHasValidation());									
									irSFormvalidation.setIrMFieldtypes((IrMFieldtypes)session.get(IrMFieldtypes.class,dataEntryFormValidationDTO.getFieldtypid()));
									
									irSFormvalidation.setIsactive(1);
									session.update(irSFormvalidation);
									flag = false;
								} 
								//}
								
							}
							
					if(flag){
						
						IrSFormvalidation irSFormvalidation = new IrSFormvalidation();  
								
						irSFormvalidation.setFieldLength(dataEntryFormValidationDTO.getFieldLength());
						irSFormvalidation.setFieldName(dataEntryFormValidationDTO.getFieldName());
						irSFormvalidation.setHasValidation(dataEntryFormValidationDTO.getHasValidation());									
						irSFormvalidation.setIrMFieldtypes((IrMFieldtypes)session.get(IrMFieldtypes.class,dataEntryFormValidationDTO.getFieldtypid()));
			
						irSFormvalidation.setIrMUsers(user);
						irSFormvalidation.setIrSFormsetup(fs);
						irSFormvalidation.setIsactive(1);
						
						session.save(irSFormvalidation);
						
					}		
				
				}
				
				errMsgs.add("Success : Updated Data ");
				irPlusResponseDetails.setErrMsgs(errMsgs);				
				irPlusResponseDetails.setValidationSuccess(true);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
								
			}/*else{
				
				errMsgs.add("Validation Error : IrSFormsetup id null or Please Enter Valid Fields");
				irPlusResponseDetails.setErrMsgs(errMsgs);				
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);				
			}*/
			
		}catch (Exception e) {
			log.error("Exception in create createMngmntformSetup",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
	
	return irPlusResponseDetails;
	}	
	
	
	// Over
	
	public IRPlusResponseDetails showOneMngmntformSetup(String id) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		List<String> errMsgs = new ArrayList<String>();
		Session session = sessionFactory.getCurrentSession();
		
		log.debug(" Inside of the ManagmntFormsetupDaoImpl :: showOneMngmntformSetup ");
		
		try{
			
			Integer idno = Integer.parseInt(id);
			IrSFormsetup fstp = null;
			
			fstp = (IrSFormsetup) session.createCriteria(IrSFormsetup.class)
					.add(Restrictions.eq("formSetupId", idno)).uniqueResult();
			
			if(fstp!=null){
			
			List<IrSFormvalidation> lst = session.createCriteria(IrSFormvalidation.class , "irsform").add(Restrictions.like("irsform.irSFormsetup.formSetupId" ,idno)).list();
			
			FormsManagementDTO fmdto = new FormsManagementDTO();

			fmdto.setFormSetupId(fstp.getFormSetupId());
			
			
			fmdto.setBatchClassDataentry(fstp.getBatchClassDataentry());
			fmdto.setBatchClassValidation(fstp.getBatchClassValidation());
			
			
		//	fmdto.setDataEntryFormValidationDTO(fstp.getd);

			fmdto.setWatchFolderValidation(fstp.getWatchFolderValidation());
			fmdto.setWatchFolderDataentry(fstp.getWatchFolderDataentry());
			fmdto.setIsdefault(fstp.getIsdefault());
			
			fmdto.setFormCode(fstp.getFormCode());
			fmdto.setFormName(fstp.getFormName());

			fmdto.setIsactive(fstp.getIsactive());
			fmdto.setUserid(fstp.getIrMUsers().getUserid());
			fmdto.setCustomerid(fstp.getIrSCustomers().getCustomerId());
			fmdto.setBankbranchId(fstp.getIrSBankbranch().getBranchId());
					
			DataEntryFormValidationDTO[] dtoarray = new DataEntryFormValidationDTO[lst.size()];
		
			int i = 0;
			
			for (IrSFormvalidation irSFormvalidation : lst){
				
				DataEntryFormValidationDTO dto = new DataEntryFormValidationDTO();
				
				if(irSFormvalidation.getFieldLength()!=null){
				dto.setFieldLength(irSFormvalidation.getFieldLength()); }else{dto.setFieldLength(0);}
				
				if(irSFormvalidation.getFieldLength()!=null){
				dto.setFieldName(irSFormvalidation.getFieldName());}else{dto.setFieldName("");}
				
				if(irSFormvalidation.getFieldLength()!=null){
				dto.setFieldtypid(irSFormvalidation.getIrMFieldtypes().getFieldTypeId());}else{dto.setFieldtypid(0);}
											
				if(irSFormvalidation.getFieldLength()!=null){
				dto.setHasValidation(irSFormvalidation.getHasValidation());}else{dto.setHasValidation(false);}
				
				if(irSFormvalidation.getFieldLength()!=null){
				dto.setFieldLength(irSFormvalidation.getFieldLength());}else{dto.setFieldLength(0);}
				
				dtoarray[i]=dto;
				++i;
				
			}
			fmdto.setDataEntryFormValidationDTO(dtoarray);
						
			irPlusResponseDetails.setFormsManagementDTO(fmdto);
			errMsgs.add("Success :: Get the Data ");
			irPlusResponseDetails.setErrMsgs(errMsgs);				
			irPlusResponseDetails.setValidationSuccess(true);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
									
			}else{
				errMsgs.add("Validation Error : IrSFormsetup id null ");
				irPlusResponseDetails.setErrMsgs(errMsgs);				
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}
			
		}catch(Exception e){
			log.error("Exception in create createMngmntformSetup",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
	
		return irPlusResponseDetails;
	}
	
	
	public IRPlusResponseDetails dynamicSelectFieldTypes()throws BusinessException{
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		List<String> errMsgs = new ArrayList<String>();
		Session session = sessionFactory.getCurrentSession();
		
		List<FieldTypesInfo> listFieldTypes = new ArrayList<FieldTypesInfo>();
		
		log.debug(" Inside of the ManagmntFormsetupDaoImpl :: dynamicSelectFieldTypes ");
		
		try{
			
			List<IrMFieldtypes>  irsfieldLst = session.createCriteria(IrMFieldtypes.class).add(Restrictions.like("isactive", 1)).list();
		
			if(!irsfieldLst.isEmpty()){
				
				for (IrMFieldtypes irMFieldtypes : irsfieldLst) {
					
					if(irMFieldtypes.getIsactive()==1 ){
						
					FieldTypesInfo fieldTypesInfo = new FieldTypesInfo();
										
					if(irMFieldtypes.getDefaultLength()!=0){
						
					fieldTypesInfo.setDefaultLength(irMFieldtypes.getDefaultLength());}
					
					if(irMFieldtypes.getFieldFormat()!=null){
						
					fieldTypesInfo.setFieldFormat(irMFieldtypes.getFieldFormat());}
				
					fieldTypesInfo.setFieldTypeId(irMFieldtypes.getFieldTypeId());
					
					fieldTypesInfo.setIsactive(irMFieldtypes.getIsactive());
					
					listFieldTypes.add(fieldTypesInfo);
					}
				}
								
				if(listFieldTypes.isEmpty()){
					
					errMsgs.add("Validation Error : No Data Available ");
					irPlusResponseDetails.setErrMsgs(errMsgs);				
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				
				}else{
					irPlusResponseDetails.setListFieldTypes(listFieldTypes);
					errMsgs.add("Success :: Got the Data ");
					irPlusResponseDetails.setErrMsgs(errMsgs);				
					irPlusResponseDetails.setValidationSuccess(true);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				}
				
			}else{
				errMsgs.add("Validation Error : No Data Available ");
				irPlusResponseDetails.setErrMsgs(errMsgs);				
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}
			
			
		}catch(Exception e){
			log.error("Exception in list dynamicSelectFieldTypes",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

	return irPlusResponseDetails;
	}
	
public IRPlusResponseDetails statusUpdate(StatusIdInfo statusIdInfo)throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		List<String> errMsgs = new ArrayList<String>();
		Session session = sessionFactory.getCurrentSession();
		boolean flag = true;
		
		if(statusIdInfo.getId() == null){
			flag = false;
			errMsgs.add("Validation Error : status Id is null");
		}
		if(statusIdInfo.getStatus() == null){
			flag = false;
			errMsgs.add("Validation Error : status Id is null");
		}
		
		try{
		
			IrSFormvalidation irsf = (IrSFormvalidation) session.get(IrSFormvalidation.class, statusIdInfo.getId());
			
			irsf.setIsactive(statusIdInfo.getStatus());
			session.update(irsf);
			
			errMsgs.add("Success : Deleted - updated Status as :"+statusIdInfo.getStatus());
			
			irPlusResponseDetails.setErrMsgs(errMsgs);				
			irPlusResponseDetails.setValidationSuccess(true);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			
		}catch(Exception e){
			
			log.error("Exception in list dynamicSelectFieldTypes",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}
public IRPlusResponseDetails showListofForms()throws BusinessException {
	
	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
	List<String> errMsgs = new ArrayList<String>();
	Session session = sessionFactory.getCurrentSession();
	boolean flag = true;
	List<FormsManagementDTO> formMgntList = new ArrayList<FormsManagementDTO>();
		
	try{		
				List<IrSFormsetup> irsformList = session.createCriteria(IrSFormsetup.class)
				.add(Restrictions.like("isactive", 1)).list();
		
				Set<IrSFormvalidation> irsfSet =null;
				
				Integer i = null;
				FormsManagementDTO fmdto = null;
				if (!irsformList.isEmpty()){
					
					for (IrSFormsetup irSFormsetup : irsformList) {
												
						if(irSFormsetup.getIsactive()!=2){
						
						 fmdto = new FormsManagementDTO();
			//			fmdto.setFormValidatnsLength(irSFormsetup.getIrSFormvalidations().size());
						
						fmdto.setFormCode(irSFormsetup.getFormCode());  // formcode =formId
						fmdto.setFormName(irSFormsetup.getFormName());
						fmdto.setFormSetupId(irSFormsetup.getFormSetupId());
						
						fmdto.setBankbranchId(irSFormsetup.getIrSBankbranch().getBranchId());
						fmdto.setUserid(irSFormsetup.getIrMUsers().getUserid());
						
						fmdto.setBatchClassDataentry(irSFormsetup.getBatchClassDataentry());
						fmdto.setBatchClassValidation(irSFormsetup.getBatchClassValidation());						
						
						fmdto.setWatchFolderDataentry(irSFormsetup.getWatchFolderDataentry());
						fmdto.setWatchFolderValidation(irSFormsetup.getWatchFolderValidation());
												
						fmdto.setCustomerid(irSFormsetup.getIrSCustomers().getCustomerId());
				//		fmdto.setDataEntryFormValidationDTO(irSFormsetup.get);
					
			   //		fmdto.setFormValidatnsLength(irSFormsetup.get);
						
						{   i=0;
							
							irsfSet = irSFormsetup.getIrSFormvalidations();
						
						for(IrSFormvalidation irSFormvalidation : irsfSet){
							
							if(irSFormvalidation.getIsactive()==1){
								++i;
							}							
						}
						fmdto.setFormValidatnsLength(i);
						
						}						
						
						fmdto.setIsactive(irSFormsetup.getIsactive());
						fmdto.setIsdefault(irSFormsetup.getIsdefault());
						
						formMgntList.add(fmdto);
						}											
					}
					if (!formMgntList.isEmpty()) {
						errMsgs.add(" Success  : List of the forms ");
						irPlusResponseDetails.setFormMgntList(formMgntList);
						irPlusResponseDetails.setErrMsgs(errMsgs);
						irPlusResponseDetails.setRecordsFiltered(formMgntList.size());
						irPlusResponseDetails.setRecordsTotal(formMgntList.size());
						irPlusResponseDetails.setValidationSuccess(true);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
						irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
						
					} else {
						errMsgs.add(" Message Alert : No Active Data Available ");
						irPlusResponseDetails.setErrMsgs(errMsgs);				
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
						irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
					}
				
					
				} else {
					errMsgs.add("Validation Error : No Data Available ");
					irPlusResponseDetails.setErrMsgs(errMsgs);				
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				}								
		
		irPlusResponseDetails.setErrMsgs(errMsgs);				
		irPlusResponseDetails.setValidationSuccess(true);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
		
	}catch(Exception e){
		
		log.error("Exception in list dynamicSelectFieldTypes",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}

	return irPlusResponseDetails;
}



@Override
public IRPlusResponseDetails showAllCustomerForms(Integer customerid) throws BusinessException {
	
	
	IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
	List<FormsManagementDTO> listformBean = new ArrayList<FormsManagementDTO>();
	Session session = null;
	FormsManagementDTO fmdto=null;
	session = sessionFactory.getCurrentSession();
	List<String> errMsgs = new ArrayList<String>();
	try {
		
		
		Query customersQueryform = session.createQuery("from IrSFormsetup form where form.irSCustomers.customerId=?");
		customersQueryform.setParameter(0,customerid);
		

		List<IrSFormsetup> list = customersQueryform.list();
		if(list.size()>0){
			for(int i=0;i<list.size();i++){
				 fmdto = new FormsManagementDTO();
				 fmdto.setCreateddate(list.get(i).getCreateddate());
				 fmdto.setBatchClassDataentry(list.get(i).getBatchClassDataentry());
				 fmdto.setWatchFolderDataentry(list.get(i).getWatchFolderDataentry());
				 fmdto.setFormCode(list.get(i).getFormCode());
		   fmdto.setFormName(list.get(i).getFormName());
		   fmdto.setFormSetupId(list.get(i).getFormSetupId());
		   fmdto.setCompanyName(list.get(i).getIrSCustomers().getCompanyName());
		   
			Query query = session.createQuery("from IrSFormvalidation formval where formval.irSFormsetup.formSetupId=?");
			
			
			query.setParameter(0,list.get(i).getFormSetupId());
			List<IrSFormvalidation> formvalid =query.list();		

			if(formvalid.size()>0){
				
				fmdto.setTotalfields(formvalid.size());
			
				listformBean.add(fmdto);

			}
			else
			{
			
				fmdto.setTotalfields(0);
				listformBean.add(fmdto);
			} 
		   		   
			}
			
			irPlusResponseDetails.setFormMgntList(listformBean);

			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			
		} else {
	
			log.debug("Customers not available :: Empty Data :: Data required to show roles");
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
	
		}
		   
		   
		   
		 
		
	} catch (Exception e) {
		log.error("Exception raised in show list customers",e);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		
		throw new HibernateException(e);
	}
	
	return irPlusResponseDetails;
	}	












}