package com.irplus.dao.contactmgmt;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.hibernate.entities.IrSBankBranch;
import com.irplus.dao.hibernate.entities.IrSBankContact;
import com.irplus.dao.hibernate.entities.IrSCustomercontacts;
import com.irplus.dao.hibernate.entities.IrSCustomers;
import com.irplus.dto.BankInfo;
import com.irplus.dto.ContactInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;

@Component

@Transactional
public class ContactMgmtDaoImpl implements IContactMgmtDao {

	private static final Logger LOGGER = Logger.getLogger(ContactMgmtDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public IRPlusResponseDetails addContact(ContactInfo contactInfo) throws BusinessException {

		LOGGER.info("Inside ContactMgmtDaoImpl");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		session = sessionFactory.getCurrentSession();

		try
		{
			IrSBankContact bankContact = new IrSBankContact(contactInfo);
			IrSBankBranch bankBranch = (IrSBankBranch) session.get(IrSBankBranch.class, contactInfo.getBranchId());
			LOGGER.debug("Received bankBranch Info :"+bankBranch);
			bankContact.setBranch(bankBranch);
			session.save(bankContact);
			session.flush();
			LOGGER.debug("Added bankContact Info :"+bankContact);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}
		catch(Exception e)
		{
			LOGGER.error("Exception in addContact",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails addContacts(ContactInfo[] contacts) throws BusinessException {

		LOGGER.info("Inside ContactMgmtDaoImpl");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		session = sessionFactory.getCurrentSession();

		IrSBankBranch bankBranch = null;
	

		try
		{
			if(contacts.length>0)
			{
				bankBranch = (IrSBankBranch) session.get(IrSBankBranch.class,contacts[0].getBranchId());
				LOGGER.debug("Received bankBranch Info :"+bankBranch);

			}
			for(int i=0;i<contacts.length;i++)
			{
				if("".equals(contacts[i].getFirstName())) {
				
					System.out.println("First Name Empty::");
				}else {
					
					IrSBankContact bankContact = new IrSBankContact(contacts[i]);
					bankContact.setBranch(bankBranch);
					session.save(bankContact);
					session.flush();
					LOGGER.debug("Added bankContact Info :"+bankContact);
				}
			}
		
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		
		}catch(Exception e)
		{
			LOGGER.error("Exception in addContact",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	

	}


	@Override
	public IRPlusResponseDetails listContacts(String bankId) throws BusinessException {

		LOGGER.info("Inside listBanks");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		/*Session session = null;

		List<BankInfo>banks = new ArrayList<BankInfo> ();

		try
		{
			session = sessionFactory.getCurrentSession();

			Query query = session.createQuery("from IRSBank bank where bank.siteId =:siteId");

			query.setParameter("siteId", siteId);

//			List<IRSBank> bankList = (List<IRSBank>)query.list();
			List<IRSBank> bankList = (List)query.list();

			Iterator <IRSBank> bankListIt = bankList.iterator();

			IRSBank irsBank = null;

			BankInfo bankInfo = null;

			while (bankListIt.hasNext()) 
			{
				irsBank = bankListIt.next();

				bankInfo = new BankInfo();

				bankInfo.setSiteId(irsBank.getSiteId());

				bankInfo.setBankId(irsBank.getBankId());

				bankInfo.setBankLogo(irsBank.getBankLogo());

				bankInfo.setBankName(irsBank.getBankName());

				bankInfo.setBankCode(irsBank.getIRBankCode());

				bankInfo.setCreateddate(IRPlusUtil.convertTimestampToStringFormat(irsBank.getCreateddate()));

				bankInfo.setModifieddate(IRPlusUtil.convertTimestampToStringFormat(irsBank.getModifieddate()));


				banks.add(bankInfo);

			}
			irPlusResponseDetails.setBanks(banks);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}
		catch(Exception e)
		{
			LOGGER.error("Exception in listBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

			throw new HibernateException(e);
		}*/

		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateContact(ContactInfo contactInfo) throws BusinessException {

		LOGGER.info("Inside updateContact ContactInfo :"+contactInfo);

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;
		session = sessionFactory.getCurrentSession();

		IrSBankBranch bankBranch = null;

		IrSBankContact bankContact = null;

		try
		{
			bankContact = (IrSBankContact) session.get(IrSBankContact.class, contactInfo.getContactId());
			bankContact.setFirstName(contactInfo.getFirstName());
			bankContact.setLastName(contactInfo.getLastName());
			bankContact.setContactRole(contactInfo.getContactRole());
			bankContact.setContactNo(contactInfo.getContactNo());
			bankContact.setEmailId(contactInfo.getEmailId());
			bankContact.setAddress1(contactInfo.getAddress1());
			bankContact.setAddress2(contactInfo.getAddress2());
			bankContact.setIsdefault(contactInfo.getIsdefault());
			bankContact.setCountry(contactInfo.getCountry());
			bankContact.setState(contactInfo.getState());
			bankContact.setCity(contactInfo.getCity());
			bankContact.setZipcode(contactInfo.getZipcode());
			bankContact.setIsactive(contactInfo.getIsActive());
			bankContact.setBranch(bankBranch);
			session.update(bankContact);
			session.flush();
			LOGGER.debug("updated bankContact Info :"+bankContact);

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}


		catch(Exception e)
		{
			LOGGER.error("Exception in updateBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateContacts(ContactInfo[] contacts) throws BusinessException {

		LOGGER.info("Inside updateContact ContactInfo :"+contacts);
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		session = sessionFactory.getCurrentSession();

		IrSBankBranch bankBranch = null;

	/*	IrSBankContact bankContact = null;*/

		ContactInfo contactInfo = null;

		try
		{
			
			contactInfo = contacts[0];
			
			Long branchId=contactInfo.getBranchId();
			deleteExistingContacts(branchId);
			
			
			
			if(contacts.length>0)
			{
				bankBranch = (IrSBankBranch) session.get(IrSBankBranch.class,contacts[0].getBranchId());
				LOGGER.debug("Received bankBranch Info :"+bankBranch);

			}
			for(int i=0;i<contacts.length;i++)
			{
				IrSBankContact bankContact = new IrSBankContact(contacts[i]);
				bankContact.setBranch(bankBranch);
				session.save(bankContact);
				session.flush();
				LOGGER.debug("Added bankContact Info :"+bankContact);
			}
			
			
/*			
			for(int i=0;i<contacts.length;i++)
			{
				
				LOGGER.info("eyyyyyyyywwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwe"+contacts.length);
				contactInfo = contacts[i];
				
				LOGGER.info("Inside updateContact ContactInfo :"+contactInfo);
				
				LOGGER.info("wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwe"+contactInfo.getContactId());

				
				if(contactInfo.getContactId()!=null&&!contactInfo.getContactId().isEmpty())
				{
					bankContact = (IrSBankContact) session.createQuery("from IrSBankContact contact where contact.contactId=:contactId").setParameter("contactId", new Long(contactInfo.getContactId())).uniqueResult();
					LOGGER.info("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"+bankContact);
				}
				
				if(bankContact==null || contactInfo.getContactId()==null)
				{
					LOGGER.info("ytytryrtrtrtrtrtrtrtrtrtrtrtrtrtrtrtrtrtrtrtrtrtrtrt");
					bankContact = new IrSBankContact();
					bankBranch = (IrSBankBranch) session.get(IrSBankBranch.class, contacts[0].getBranchId());
					LOGGER.info("rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr"+contacts[0].getBranchId());
					LOGGER.info("uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu"+bankBranch);
					bankContact.setBranch(bankBranch);
					bankContact.setFirstName(contactInfo.getFirstName());
					bankContact.setLastName(contactInfo.getLastName());
					bankContact.setContactRole(contactInfo.getContactRole());
					bankContact.setContactNo(contactInfo.getContactNo());
					bankContact.setEmailId(contactInfo.getEmailId());
					bankContact.setAddress1(contactInfo.getAddress1());
					bankContact.setAddress2(contactInfo.getAddress2());
					bankContact.setIsdefault(contactInfo.getIsdefault());
					bankContact.setCountry(contactInfo.getCountry());
					bankContact.setState(contactInfo.getState());
					bankContact.setCity(contactInfo.getCity());
					bankContact.setZipcode(contactInfo.getZipcode());
					bankContact.setIsactive(contactInfo.getIsActive());
					LOGGER.info("InsideUpdate contacts Adding new ContactInfo :"+contacts);
					session.save(bankContact);
				}
				else
				{
					bankContact.setFirstName(contactInfo.getFirstName());
					bankContact.setLastName(contactInfo.getLastName());
					bankContact.setContactRole(contactInfo.getContactRole());
					bankContact.setContactNo(contactInfo.getContactNo());
					bankContact.setEmailId(contactInfo.getEmailId());
					bankContact.setAddress1(contactInfo.getAddress1());
					bankContact.setAddress2(contactInfo.getAddress2());
					bankContact.setIsdefault(contactInfo.getIsdefault());
					bankContact.setCountry(contactInfo.getCountry());
					bankContact.setState(contactInfo.getState());
					bankContact.setCity(contactInfo.getCity());
					bankContact.setZipcode(contactInfo.getZipcode());
					bankContact.setIsactive(contactInfo.getIsActive());
					session.update(bankContact);
				}
	
				session.flush();
				LOGGER.debug("updated bankContact Info :"+bankContact);
			}*/
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}


		catch(Exception e)
		{
			LOGGER.error("Exception in updateBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}
	
	private void deleteExistingContacts(Long branchId) 
	{
		

		Session session = null;

		try
		{
		
			session = sessionFactory.getCurrentSession();

			Query query = session.createQuery("delete from IrSBankContact cnct where cnct.branch.branchId=:branchId");

			query.setParameter("branchId", branchId);

			query.executeUpdate();
			
		
			
			
		}
		catch(Exception e)
		{
			LOGGER.error("Exception in deleteExistingContacts",e);

			throw new HibernateException(e);
		}	

	}
	@Override
	public IRPlusResponseDetails deleteContact(String contactId) throws BusinessException {

		LOGGER.info("Inside deleteContact contactId :"+ contactId);

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;
		
		IrSBankContact bankContact = null;

		
		try
		{
			session = sessionFactory.getCurrentSession();

			bankContact = (IrSBankContact) session.get(IrSBankContact.class, new Long(contactId));

			if(bankContact!=null)
			{
				session.delete(bankContact);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
			else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}	

		}
		catch(Exception e)
		{
			LOGGER.error("Exception in deleteContact",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}
	@Override
	public IrSBankContact findPrimaryContact(Long bankId)
	{
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		session = sessionFactory.getCurrentSession();

		IrSBankContact bankContact = null;

		try
		{
			Query query = session.createQuery("from IrSBankContact contact where contact.isdefault='1' and  contact.branch.isdefault='1' and contact.branch.bank.bankId=:bankId" );

			query.setParameter("bankId", bankId);
			bankContact = (IrSBankContact) query.uniqueResult();

		}

		catch(Exception e)
		{
			LOGGER.error("Exception in findPrimaryContact",e);
		}

		return bankContact;
	}
	@Override
	public IrSBankContact findPrimaryContactByBranchId(Long branchId)
	{
		Session session = null;

		session = sessionFactory.getCurrentSession();

		IrSBankContact bankContact = null;

		try
		{
			Query query = session.createQuery("from IrSBankContact contact where contact.isdefault='1' and  contact.branch.branchId=:branchId" );

			query.setParameter("branchId", branchId);
			bankContact = (IrSBankContact) query.uniqueResult();

		}

		catch(Exception e)
		{
			LOGGER.error("Exception in findPrimaryContactByBranchId",e);
		}

		return bankContact;
	}

	@Override
	public IRPlusResponseDetails getContacts(BankInfo bankInfo) throws BusinessException
	{
		LOGGER.info("Inside getContacts bankInfo :"+bankInfo);

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		session = sessionFactory.getCurrentSession();

		IrSBankBranch bankBranch = null;

		IrSBankContact bankContact = null;

		ContactInfo contactInfo = null;

		List<IrSBankContact> bankContactList = null;

		List<ContactInfo> contacts = new ArrayList<ContactInfo>();


		try
		{
			Query query = session.createQuery("from IrSBankContact contact where contact.branch.branchId=:branchId");

			query.setParameter("branchId", bankInfo.getBranchId());

			bankContactList = (List<IrSBankContact>) query.list();

			Iterator<IrSBankContact> bankCtctItr = bankContactList.iterator();
			
			LOGGER.debug("Contact Size:"+bankContactList.size());

			while (bankCtctItr.hasNext()) {
				bankContact =  bankCtctItr.next();

				contactInfo = new ContactInfo();
				
				contactInfo.setContactId(bankContact.getContactId()+"");
				contactInfo.setFirstName(bankContact.getFirstName());
				contactInfo.setLastName(bankContact.getLastName());
				contactInfo.setContactRole(bankContact.getContactRole());
				contactInfo.setContactNo(bankContact.getContactNo());
				contactInfo.setEmailId(bankContact.getEmailId());
				contactInfo.setAddress1(bankContact.getAddress1());
				contactInfo.setAddress2(bankContact.getAddress2());
				contactInfo.setIsdefault(bankContact.getIsdefault());
				contactInfo.setCountry(bankContact.getCountry());
				contactInfo.setState(bankContact.getState());
				contactInfo.setCity(bankContact.getCity());
				contactInfo.setZipcode(bankContact.getZipcode());
				contactInfo.setIsActive(bankContact.getIsactive());
				contactInfo.setBranchId(bankContact.getBranch().getBranchId());

				contacts.add(contactInfo);

			}
			ContactInfo[] contactArray = new ContactInfo[contacts.size()];
			bankInfo.setContacts(contacts.toArray(contactArray));
			irPlusResponseDetails.setBank(bankInfo);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}


		catch(Exception e)
		{
			LOGGER.error("Exception in getContacts",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		return irPlusResponseDetails;

	}

}
