package com.irplus.dao.contactmgmt;

import com.irplus.dao.hibernate.entities.IrSBankContact;
import com.irplus.dto.BankInfo;
import com.irplus.dto.ContactInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

public interface IContactMgmtDao {

	public IRPlusResponseDetails addContact(ContactInfo contactInfo)
	            throws BusinessException;
	public IRPlusResponseDetails addContacts(ContactInfo[] contactInfo)
	            throws BusinessException;

	public IRPlusResponseDetails listContacts(String bankId) throws BusinessException;

	public IRPlusResponseDetails updateContact(ContactInfo contactInfo) throws BusinessException;
	
	public IRPlusResponseDetails updateContacts(ContactInfo[] contacts) throws BusinessException;

	public IRPlusResponseDetails deleteContact(String contactId) throws BusinessException;
	
	public IrSBankContact findPrimaryContact(Long bankId) throws BusinessException;
	
	public IrSBankContact findPrimaryContactByBranchId(Long bankId) throws BusinessException;
	
	public IRPlusResponseDetails getContacts(BankInfo bankInfo) throws BusinessException;
    
}
