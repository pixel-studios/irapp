package com.irplus.dao;


import de.schlichtherle.license.KeyStoreParam;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import com.irplus.util.IRPlusConstants;

/**
 *
 * @author sujay
 */
public class KeyStoreParamImpl implements KeyStoreParam {

    private String keyStore;
    private String alias;
    private String storePwd;
    private String keyPwd;

    public KeyStoreParamImpl(String keyStore, String alias, String storePwd, String keyPwd) {
        this.keyStore = keyStore;
        this.alias = alias;
        this.storePwd = storePwd;
        this.keyPwd = keyPwd;
    }

    public KeyStoreParamImpl(String alias, String storePwd, String keyPwd) {
        this.alias = alias;
        this.storePwd = storePwd;
        this.keyPwd = keyPwd;
    }

    public InputStream getStream() throws IOException {
        
        final String resourceName = this.keyStore == null ? "privateKeyStore" : this.keyStore;
        String directory = IRPlusConstants.LIC_DIRECTORY;
        //String directory = "D:\\Dhina\\License Generator\\src";
        File keyFile = new File(directory+"\\"+resourceName);
        final InputStream in = new FileInputStream(keyFile); //getClass().getResourceAsStream(resourceName);
        //getClass().getClassLoader().getResourceAsStream(resourceName);
       
        return in;
    }

    public String getAlias() {
        return this.alias;
    }

    public String getStorePwd() {
        return this.storePwd;
    }

    public String getKeyPwd() {
        return this.keyPwd;
    }

    /**
     * @param alias the alias to set
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    /**
     * @param storePwd the storePwd to set
     */
    public void setStorePwd(String storePwd) {
        this.storePwd = storePwd;
    }

    /**
     * @param keyPwd the keyPwd to set
     */
    public void setKeyPwd(String keyPwd) {
        this.keyPwd = keyPwd;
    }
}
