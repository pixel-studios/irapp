package com.irplus.dao.bankbranchdao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.contactmgmt.IContactMgmtDao;
import com.irplus.dao.hibernate.entities.IRSBank;
import com.irplus.dao.hibernate.entities.IrMUsersBank;
import com.irplus.dao.hibernate.entities.IrSBankBranch;
import com.irplus.dao.hibernate.entities.IrSBankContact;
import com.irplus.dao.hibernate.entities.IrSCustomers;
import com.irplus.dto.BankInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.LocationViewInfo;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusUtil;

@Component

@Transactional
public class BankBranchMgmtDaoImpl implements IBankBranchMgmtDao {

	private static final Logger LOGGER = Logger.getLogger(BankBranchMgmtDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	IContactMgmtDao iContactMgmtDao;

	@Override
	public IRPlusResponseDetails createBankBranch(BankInfo bankInfo) throws BusinessException {

		LOGGER.info("Inside BankMgmtDaoImpl");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		session = sessionFactory.getCurrentSession();

		try
		{
IrSBankBranch bankBranch = new IrSBankBranch();
			
			IRSBank bank = (IRSBank) session.get(IRSBank.class,bankInfo.getBankId());
			bankBranch.setBank(bank);
			
		
			bankBranch.setLocationId(bankInfo.getLocationId());
			bankBranch.setBranchLocation(bankInfo.getBranchLocation());
			bankBranch.setWebsite(bankInfo.getWebsite());
			bankBranch.setIsdefault(bankInfo.getIsdefault());
			bankBranch.setCountry(bankInfo.getCountry());
			bankBranch.setState(bankInfo.getState());
			bankBranch.setCity(bankInfo.getCity());
			bankBranch.setZipcode(bankInfo.getZipcode());
			bankBranch.setIsactive(bankInfo.getIsactive());
			
			Long branchId = (Long) session.save(bankBranch);
			LOGGER.debug("Added bankBranch Info :"+branchId);

			bankInfo.getContacts()[0].setBranchId(branchId);
			session.flush();
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}
		catch(Exception e)
		{
			LOGGER.error("Exception in createBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}


	@Override
	public IRPlusResponseDetails listBankBranches(String bankId) throws BusinessException {

		LOGGER.info("Inside listBankBranches");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		List<LocationViewInfo>locations = new ArrayList<LocationViewInfo> ();

		try
		{
			session = sessionFactory.getCurrentSession();
			
			/*isActive Status Code
			
			0-inactive
			1-active
			2-deleted*/	
			

			
			
			String hql = "from IrSBankBranch branch where branch.bank.bankId=? and branch.isactive!=?";
			
			Query query = session.createQuery(hql);

			query.setParameter(0,new Long(bankId));
			query.setParameter(1,2);
			

		//List<IrSBankBranch> bankList = (List<IRSBank>)query.list();
			List<IrSBankBranch> branchList = (List)query.list();

			Iterator<IrSBankBranch> bankListIt = branchList.iterator();

			IrSBankBranch bankBranch = null;

			IrSBankContact bankContact = null;

			LocationViewInfo locationViewInfo = null;

			while (bankListIt.hasNext()) 
			{
				bankBranch = bankListIt.next();

				locationViewInfo = new LocationViewInfo();

				locationViewInfo.setSiteId(bankBranch.getBank().getSiteId());

				locationViewInfo.setBankId(bankBranch.getBank().getBankId());

				locationViewInfo.setBankLogo(bankBranch.getBank().getBankLogo());
				locationViewInfo.setIsdefault(bankBranch.getIsdefault());

				locationViewInfo.setBankName(bankBranch.getBank().getBankName());
				
				locationViewInfo.setBranchId(bankBranch.getBranchId());

				locationViewInfo.setBranchLocation(bankBranch.getBranchLocation());

				locationViewInfo.setBranchOwnCode(bankBranch.getBranchOwnCode());
				
				locationViewInfo.setIsactive(bankBranch.getIsactive());

				locationViewInfo.setLocationId(bankBranch.getLocationId());

				locationViewInfo.setCreateddate(IRPlusUtil.convertTimestampToStringFormat(bankBranch.getCreateddate()));

				locationViewInfo.setModifieddate(IRPlusUtil.convertTimestampToStringFormat(bankBranch.getModifieddate()));
				
				
				
				if(branchList.size()>0){
				
					
					
					Query cust =session.createQuery("from IrSCustomers customer where customer.isactive=? and customer.irSBankBranch.branchId=?");


						cust.setParameter(0,1);

						cust.setParameter(1,bankBranch.getBranchId());
						List<IrSCustomers> custList =cust.list();
						
						
					if(custList.size()>0){	
						
						locationViewInfo.setTotalCustomers(custList.size());
							 
						}
					
				
				}	
					
				
				
				
				
				
				
				
				

				bankContact = iContactMgmtDao.findPrimaryContactByBranchId(bankBranch.getBranchId());

				if(bankContact!=null)
				{
					locationViewInfo.setFirstname(bankContact.getFirstName());
					locationViewInfo.setEmail(bankContact.getEmailId());
					locationViewInfo.setContactno(bankContact.getContactNo());
				}

				locationViewInfo.setCity(bankBranch.getCity());
				locationViewInfo.setState(bankBranch.getState());
				
				locations.add(locationViewInfo);
			}
			irPlusResponseDetails.setLocations(locations);
			irPlusResponseDetails.setRecordsTotal(locations.size());
			irPlusResponseDetails.setRecordsFiltered(locations.size());
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}
		catch(Exception e)
		{
			LOGGER.error("Exception in listBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateBranch(BankInfo bankInfo) throws BusinessException {

		LOGGER.info("Inside updateBank bankInfo :"+bankInfo);
		LOGGER.info("Insideentereeebrannnnnnnnnnnnnnnnnncjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		IrSBankBranch bankBranch = null;

		try
		{
			
			System.out.println("bank Id ::::: "+bankInfo.getBankId());
			session = sessionFactory.getCurrentSession();
			
			Query banks = session.createQuery("from IrSBankBranch branch where branch.bank.bankId=?");
			banks.setParameter(0,bankInfo.getBankId());

			List<IrSBankBranch> branchList = banks.list();
			
			
			if(branchList.size()>0) {
				
				for(int a=0;a<branchList.size();a++) {
					Long branchId=new Long(branchList.get(a).getBranchId());
					Long uiBranchId= new Long(bankInfo.getBranchId());
					
					if(bankInfo.getIsdefault()=='0') {
						

						bankBranch = (IrSBankBranch) session.get(IrSBankBranch.class,bankInfo.getBranchId());

						if(bankBranch!=null)
						{
							LOGGER.info("Inside : "+bankBranch);

							bankBranch.setBranchLocation(bankInfo.getBranchLocation());
							bankBranch.setBranchOwnCode(bankInfo.getBranchOwnCode());
							bankBranch.setLocationId(bankInfo.getLocationId());
							bankBranch.setWebsite(bankInfo.getWebsite());
							bankBranch.setBranchLogo(bankInfo.getBranchLogo());
							bankBranch.setIsdefault(bankInfo.getIsdefault());
							bankBranch.setCountry(bankInfo.getCountry());
							bankBranch.setState(bankInfo.getState());
							bankBranch.setCity(bankInfo.getCity());
							bankBranch.setZipcode(bankInfo.getZipcode());
							bankBranch.setIsactive(bankInfo.getIsactive());
							session.update(bankBranch);
							irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
						}
					}else {
					if(uiBranchId.equals(branchId)) {
						bankBranch = (IrSBankBranch) session.get(IrSBankBranch.class,bankInfo.getBranchId());

						if(bankBranch!=null)
						{
							LOGGER.info("Inside : "+bankBranch);

							bankBranch.setBranchLocation(bankInfo.getBranchLocation());
							bankBranch.setBranchOwnCode(bankInfo.getBranchOwnCode());
							bankBranch.setLocationId(bankInfo.getLocationId());
							bankBranch.setWebsite(bankInfo.getWebsite());
							bankBranch.setBranchLogo(bankInfo.getBranchLogo());
							bankBranch.setIsdefault(bankInfo.getIsdefault());
							bankBranch.setCountry(bankInfo.getCountry());
							bankBranch.setState(bankInfo.getState());
							bankBranch.setCity(bankInfo.getCity());
							bankBranch.setZipcode(bankInfo.getZipcode());
							bankBranch.setIsactive(bankInfo.getIsactive());
							session.update(bankBranch);
							irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
						}
						else
						{
							irPlusResponseDetails.setValidationSuccess(false);
							irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
							irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
						}				

					}else {
					
						char isDefault1='0';
						Query q = session.createQuery("UPDATE IrSBankBranch irSBankBranch set irSBankBranch.isdefault=? WHERE irSBankBranch.branchId=?");
					    q.setParameter(0,isDefault1);
						 q.setParameter(1,branchList.get(a).getBranchId());
						 q.executeUpdate();
					}
				}
				}
				
					
				
			}
			
			
			
			

		}
		catch(Exception e)
		{
			LOGGER.error("Exception in updateBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getBranchInfo(BankInfo bankInfo)  throws BusinessException
	{
		LOGGER.info("Inside getBranchInfo");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		IrSBankBranch bankBranch = null;

		try
		{
			session = sessionFactory.getCurrentSession();

			Query query = session.createQuery("from IrSBankBranch branch where branch.isdefault='1' and branch.bank.bankId=:bankId");

			query.setParameter("bankId", bankInfo.getBankId());

			bankBranch = (IrSBankBranch) query.uniqueResult();

			if(bankBranch!=null)
			{
				bankInfo.setBranchId(bankBranch.getBranchId());
				bankInfo.setBranchLocation(bankBranch.getBranchLocation());
				bankInfo.setBranchOwnCode(bankBranch.getBranchOwnCode());
				bankInfo.setLocationId(bankBranch.getLocationId());
				bankInfo.setWebsite(bankBranch.getWebsite());
				bankInfo.setIsdefault(bankBranch.getIsdefault());
				bankInfo.setCountry(bankBranch.getCountry());
				bankInfo.setState(bankBranch.getState());
				bankInfo.setCity(bankBranch.getCity());
				bankInfo.setZipcode(bankBranch.getZipcode());
				bankInfo.setBankStatus(bankBranch.getIsactive());
				bankInfo.setBranchLogo(bankBranch.getBranchLogo());
				irPlusResponseDetails.setBank(bankInfo);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
			else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}


		}
		catch(Exception e)
		{
			LOGGER.error("Exception in updateBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		return irPlusResponseDetails;

	}
	
	@Override
	public IRPlusResponseDetails getBranchInfo(Long branchId)  throws BusinessException
	{
		LOGGER.info("Inside getBranchInfo : branchId : " +branchId);

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		IrSBankBranch bankBranch = null;

		try
		{
			session = sessionFactory.getCurrentSession();

			bankBranch = (IrSBankBranch) session.get(IrSBankBranch.class,branchId);
			
			BankInfo bankInfo = new BankInfo();
			
			if(bankBranch!=null)
			{
				bankInfo.setBankId(bankBranch.getBank().getBankId());
				bankInfo.setBankName(bankBranch.getBank().getBankName());
				bankInfo.setBranchId(bankBranch.getBranchId());
				bankInfo.setBranchLocation(bankBranch.getBranchLocation());
				bankInfo.setBranchOwnCode(bankBranch.getBranchOwnCode());
				bankInfo.setBankLogo(bankBranch.getBank().getBankLogo());
				bankInfo.setLocationId(bankBranch.getLocationId());
				bankInfo.setWebsite(bankBranch.getWebsite());
				bankInfo.setIsdefault(bankBranch.getIsdefault());
				bankInfo.setCountry(bankBranch.getCountry());
				bankInfo.setState(bankBranch.getState());
				bankInfo.setCity(bankBranch.getCity());
				bankInfo.setBranchLogo(bankBranch.getBranchLogo());
				bankInfo.setZipcode(bankBranch.getZipcode());
				bankInfo.setBankStatus(bankBranch.getIsactive());
				
				irPlusResponseDetails.setBank(bankInfo);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
			else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}


		}
		catch(Exception e)
		{
			LOGGER.error("Exception in updateBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		return irPlusResponseDetails;

	}


	@Override
	public IRPlusResponseDetails deleteBranch(BankInfo bankInfo) throws BusinessException {

		LOGGER.info("Inside deleteBranch branchId :");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		IrSBankBranch irSBankBranch = null;

				

		try
		{
			session = sessionFactory.getCurrentSession();

			irSBankBranch = (IrSBankBranch) session.get(IrSBankBranch.class,bankInfo.getBranchId());

			if(irSBankBranch!=null)
			{
				irSBankBranch.setIsactive(bankInfo.getIsactive());
				session.update(irSBankBranch);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
			else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}	

		}
		catch(Exception e)
		{
			LOGGER.error("Exception in deleteBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}


/*	@Override
	public IRPlusResponseDetails deleteBranch(String bankId) throws BusinessException {

		LOGGER.info("Inside deleteBank bankId :"+ bankId);

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		IRSBank irsBank = null;

				 Need to add code to validate dependency of data on this bank details

		try
		{
			session = sessionFactory.getCurrentSession();

			irsBank = (IRSBank) session.get(IRSBank.class, new Long(bankId));

			if(irsBank!=null)
			{
				session.delete(irsBank);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
			else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}	

		}
		catch(Exception e)
		{
			LOGGER.error("Exception in deleteBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}
*/
	
	
	
}
