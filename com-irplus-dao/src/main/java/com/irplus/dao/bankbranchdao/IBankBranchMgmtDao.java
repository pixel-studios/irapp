package com.irplus.dao.bankbranchdao;

import com.irplus.dto.BankInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

public interface IBankBranchMgmtDao {


    public IRPlusResponseDetails createBankBranch(BankInfo bankInfo) throws BusinessException;

	public IRPlusResponseDetails listBankBranches(String bankId) throws BusinessException;

	public IRPlusResponseDetails updateBranch(BankInfo bankInfo) throws BusinessException;

	public IRPlusResponseDetails deleteBranch(BankInfo bankInfo) throws BusinessException;
	
	public IRPlusResponseDetails getBranchInfo(BankInfo bankInfo)  throws BusinessException;
	
	public IRPlusResponseDetails getBranchInfo(Long branchId)  throws BusinessException;
	
    
}
