package com.irplus.dao.dbinfo;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.hibernate.entities.IrMDbinfo;
import com.irplus.dao.hibernate.entities.IrSCustomers;
import com.irplus.dto.DBDTO;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;

/**
 * This Dao Class - DBProperties tables curd activities impacted here
 * @author arjun
 *
 **/

@Transactional
@Component
public class DBPropertiesDaoImpl implements IDBPropertiesDao{

	public static final Logger log = Logger.getLogger(DBPropertiesDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	Session session =null;

	/*
	 * public Session getMyCurrentSession() { Session session =null;
	 * 
	 * try { session=sessionFactory.getCurrentSession(); }catch
	 * (HibernateException e) { log.
	 * error("Exception raised DaoImpl :: At current session creation time ::"+e
	 * ); throw new HibernateException(e); } return session; }
	 */

	
	@Override
	public IRPlusResponseDetails createDBProperties(DBDTO dbdto) throws BusinessException {
		
	log.info("Inside DBPropertiesDaoImpl : createDBProperties");
		
	log.debug("user name :"+dbdto.getUsername()+"user pwd :"+dbdto.getPassword());
	
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		List<String> errMsgs = new ArrayList<String>();
		
		session = sessionFactory.getCurrentSession();
		
		Integer id = null;
	try{
		boolean flag = false;
		if(dbdto.getUsername()!=null && dbdto.getHostName()!=null && dbdto.getPassword()!=null){
		
			IrMDbinfo irMDbinfo = new IrMDbinfo();				
			
			irMDbinfo.setHostName(dbdto.getHostName());
			irMDbinfo.setPassword(dbdto.getPassword());
			irMDbinfo.setUsername(dbdto.getUsername());
						
		 id = (Integer)	session.save(irMDbinfo);
		 flag = true;
		}else{
			errMsgs.add("Message Alert :Please Enter Valid fields");
			irPlusResponseDetails.setErrMsgs(errMsgs);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
		}
		
		if(id!=null && flag){
			errMsgs.add("Success : Saved DB data");
			irPlusResponseDetails.setErrMsgs(errMsgs);
			irPlusResponseDetails.setValidationSuccess(true);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}else{
			errMsgs.add("Message Alert :Not Saved DB data");
			irPlusResponseDetails.setErrMsgs(errMsgs);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
		}
	 } catch (Exception e) {

		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		log.error("Exception Raised inside createDBProperties :: "+e);
		throw new HibernateException(e);
	}		
	return irPlusResponseDetails;
	}

	
	@Override
	public IRPlusResponseDetails updateDBProperties(DBDTO dbdto) throws BusinessException {
	    
		log.info("Inside DBPropertiesDaoImpl : updateDBProperties");
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();		
		List<String> errMsgs = new ArrayList<String>();
		session=sessionFactory.getCurrentSession();
		IrMDbinfo irMDbinfo =null;
		Integer id = null;
		
		try{				
				if(dbdto.getUsername()!=null && dbdto.getHostName()!=null && dbdto.getPassword()!=null){					
					
				//	irMDbinfo = (IrMDbinfo)session.get(IrMDbinfo.class, dbdto.getDbInfoId());
					
					if(irMDbinfo !=null){						
						
					//irMDbinfo.setDbInfoId(dbdto.getDbInfoId());
					irMDbinfo.setHostName(dbdto.getHostName());
					irMDbinfo.setPassword(dbdto.getPassword());
					irMDbinfo.setUsername(dbdto.getUsername());
								
				 	session.update(irMDbinfo);
				 	
					errMsgs.add("Success : Saved DB data");
					irPlusResponseDetails.setErrMsgs(errMsgs);
					irPlusResponseDetails.setValidationSuccess(true);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
					
					}else{
						errMsgs.add("Message Alert : Not available that data in Db");
						irPlusResponseDetails.setErrMsgs(errMsgs);
						irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);					
					}
				}else{
					errMsgs.add("Message Alert :Please Enter Valid fields");
					irPlusResponseDetails.setErrMsgs(errMsgs);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				}
				
			} catch (Exception e){		
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				log.error("Exception Raised inside createDBProperties :: "+e);
				throw new HibernateException(e);
			}		
			
	return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails showDBProperties() throws BusinessException {
	log.info("Inside DBPropertiesDaoImpl : showDBProperties");
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		List<String> errMsgs = new ArrayList<String>();
		session = sessionFactory.getCurrentSession();
		List<DBDTO> lstdbdto = new ArrayList<DBDTO>();
		try{
			List<IrMDbinfo> lst = session.createCriteria(IrMDbinfo.class).list();
			if(!lst.isEmpty()){
				
				for (IrMDbinfo irMDbinfo : lst) {
					DBDTO dt = new DBDTO();
					
					//dt.setDbInfoId(irMDbinfo.getDbInfoId());
					dt.setHostName((String)irMDbinfo.getHostName());
					dt.setPassword((String)irMDbinfo.getPassword());
					dt.setUsername((String)irMDbinfo.getUsername());
					
					lstdbdto.add(dt);
				}
				irPlusResponseDetails.setDbdtoList(lstdbdto);
			}else{				
				errMsgs.add("Message Alert : No Data Available ");
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
			}						
		} catch (Exception e) {
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception Raised inside customer DaoImple :: "+e);
			throw new HibernateException(e);
		}
			
	return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails deleteDBProperties(DBDTO dbdto) throws BusinessException {
	log.info("Inside DBPropertiesDaoImpl : deleteDBProperties");
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		List<String> errMsgs = new ArrayList<String>();
		
		try{
			
		
	} catch (Exception e) {
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		log.error("Exception Raised inside customer DaoImple :: "+e);
		throw new HibernateException(e);
	}
			
	return irPlusResponseDetails;	
	}
	
	
	public IRPlusResponseDetails showOneDb(int id) throws BusinessException {
	
		log.info("Inside DBPropertiesDaoImpl : updateDBProperties");
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();		
		List<String> errMsgs = new ArrayList<String>();
		session=sessionFactory.getCurrentSession();
		IrMDbinfo irMDbinfo =null;
	//	Integer id1 = null;
		
		DBDTO dbdto =null;
		
		List<DBDTO> dbdtoList = new ArrayList<DBDTO>();
		
		try{		
			irMDbinfo =(IrMDbinfo) session.get(IrMDbinfo.class, id);
			
			if(irMDbinfo!=null){
						
					dbdto = new DBDTO();
					
					if(irMDbinfo.getUsername()!=null){dbdto.setUsername(irMDbinfo.getUsername());}	
					
					//if(irMDbinfo.getDbInfoId()!=null){dbdto.setDbInfoId(irMDbinfo.getDbInfoId());}
					if(irMDbinfo.getHostName()!=null){dbdto.setHostName(irMDbinfo.getHostName());}
					if(irMDbinfo.getPassword()!=null){dbdto.setPassword(irMDbinfo.getPassword());}
												 
					errMsgs.add("Success : Saved DB data");
					
					irPlusResponseDetails.setErrMsgs(errMsgs);
					irPlusResponseDetails.setValidationSuccess(true);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
					
					/* 
					 * }else{
						errMsgs.add("Message Alert : Not available that data in Db");
						irPlusResponseDetails.setErrMsgs(errMsgs);
						irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);					
					} */
					
				}else{
					errMsgs.add("Message Alert :Please Enter Valid fields");
					irPlusResponseDetails.setErrMsgs(errMsgs);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				}
				
			} catch (Exception e){		
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				log.error("Exception Raised inside createDBProperties :: "+e);
				throw new HibernateException(e);
			}		
			
	return irPlusResponseDetails;
	}

}
