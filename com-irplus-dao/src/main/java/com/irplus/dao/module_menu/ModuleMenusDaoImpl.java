package com.irplus.dao.module_menu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.hibernate.entities.IrMMenus;
import com.irplus.dao.hibernate.entities.IrMModulemenus;
import com.irplus.dao.hibernate.entities.IrMModules;
import com.irplus.dao.hibernate.entities.IrMRolepermissions;
import com.irplus.dao.hibernate.entities.IrMUsers;

import com.irplus.dao.menu.IrMMenusDao;
import com.irplus.dao.module.IModulesDao;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.menu_modules_association.ModuleMenuBean;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;

@Component

@Transactional
public class ModuleMenusDaoImpl implements IModuleMenusDao{

	private static final Log log = LogFactory.getLog(ModuleMenusDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	IrMMenusDao irMMenusDao;
	
	@Autowired
	IModulesDao iModulesDao ;
	
	public Session getMyCurrentSession(){
		 Session session = null;
		 try 
		 {   
		     session = sessionFactory.getCurrentSession();
		 } 
		 catch (HibernateException e) 
		 {    
			 log.error("Exception Raised At ModuleMenuDaoImpl"+e);
		 }
		 return session;
	}
	
	@Override
	public IRPlusResponseDetails createMenuModule(ModuleMenuBean moduleMenuInfo) throws BusinessException {
		
		log.info("Inside of IrMMenuModulesDaoImpl");
		
		List<String> errMsg = new ArrayList<String>();
		
		IRPlusResponseDetails moduleMenuResponseDetails = new IRPlusResponseDetails();
		IrMModulemenus mmInfo = null;
		
		try {			
			
			mmInfo = new IrMModulemenus();  
					
			IrMMenus menuref = (IrMMenus)sessionFactory.getCurrentSession().get(IrMMenus.class , moduleMenuInfo.getMenuid());
						
			IrMModules moduleref =(IrMModules)sessionFactory.getCurrentSession().get(IrMModules.class ,moduleMenuInfo.getModuleid());
		
			IrMUsers userinfo = (IrMUsers)sessionFactory.getCurrentSession().get(IrMUsers.class , moduleMenuInfo.getUserId());
			
			if(moduleref!=null && menuref !=null && userinfo != null) {
			
				errMsg.add("Success : Available userId ,Moduleid ,Userid :"+moduleMenuInfo.getMenuid()+" "
				+moduleMenuInfo.getModuleid()+" "+moduleMenuInfo.getUserId()+"" );
				moduleMenuResponseDetails.setErrMsgs(errMsg);
				

				mmInfo.setIrMMenus(menuref);
				mmInfo.setIrMModules(moduleref);
				
				moduleref.setModuleid(moduleMenuInfo.getModuleid());
				mmInfo.setSortingorder(moduleMenuInfo.getSortingorder());
				
				getMyCurrentSession().save(mmInfo);
				
				moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				moduleMenuResponseDetails.setValidationSuccess(true);
				log.debug("Inside of ModuleMenuDaoImpl class :: createModuleMenu() calling");
			
			}else {
				
				
				log.error("moduleMenuInfo.getMenuid() not available "+moduleMenuInfo.getMenuid()+""
						+ " moduleMenuInfo.getModuleid() not available "+moduleMenuInfo.getModuleid());
				
				errMsg.add("Success : Available userId ,Moduleid ,Userid :");
				
				moduleMenuResponseDetails.setErrMsgs(errMsg);
			}			

		}
		catch (Exception e) {
			log.error("Exception in create MenuModule",e);
			moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			moduleMenuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		return moduleMenuResponseDetails;
	}
	//----------------------------------------------------------------------------------
	
// modulemeanus update of status = 1 or 0 ..........Which are matched modules reflect status also 0 and 1
		
public IRPlusResponseDetails UpdateStatus(StatusIdInfo sidInfo) throws BusinessException {
		
		IRPlusResponseDetails moduleMenuResponseDetails = new IRPlusResponseDetails();
		
		log.debug("Inside of ModuleMenus ");
		
		List<String> errMsg = new ArrayList<String>();
		
		boolean flag = false;
		try {
			
		/*	Criteria criteria=sessionFactory.getCurrentSession().createCriteria(IrMModulemenus.class);
			List<IrMModulemenus> list = criteria.list();
			
			for (IrMModulemenus irMModulemenus2 : list) {
				
				if(irMModulemenus2.getIrMMenus().getMenuid()==sidInfo.getId()){
				
					flag=true;
					irMModulemenus2.setIsactive(sidInfo.getStatus());
					
					
					getMyCurrentSession().update(irMModulemenus2);
				}
			}*/
			
			IrMModulemenus irmm =(IrMModulemenus) getMyCurrentSession().get(IrMModulemenus.class ,  sidInfo.getId());
			if( irmm !=null){
				irmm.setIsactive(sidInfo.getStatus());
				getMyCurrentSession().update(irmm);
				flag = true;
			}
			
			if(flag){
			errMsg.add("Success :: Status updated :"+sidInfo.getStatus());
			moduleMenuResponseDetails.setErrMsgs(errMsg);
			moduleMenuResponseDetails.setValidationSuccess(true);		
			moduleMenuResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			
			}else{
				
				errMsg.add("Validation Error :: This id is null");
				moduleMenuResponseDetails.setErrMsgs(errMsg);				
				moduleMenuResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}		
		} 
		catch (Exception e) 
		{			
			log.error("Update MenuModule Exceprion risen"+e);
			
			moduleMenuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
	
			throw new HibernateException(e);
		}		
		return moduleMenuResponseDetails;			
	}
	
//close delete

//----------------------------------------------------------------

/*delete meanus update of status = 2*/

public IRPlusResponseDetails DeleteUpdateStatus(StatusIdInfo sidInfo) throws BusinessException {
		
		IRPlusResponseDetails moduleMenuResponseDetails = new IRPlusResponseDetails();
		
		log.debug("Inside of ModuleMenus ");
		
		List<String> errMsg = new ArrayList<String>();
		IrMModulemenus irMModulemenus = null;				
		
		try {
			irMModulemenus =(IrMModulemenus)getMyCurrentSession().get(IrMModulemenus.class,sidInfo.getId());	
			
			irMModulemenus.setIsactive(sidInfo.getStatus());
			
			getMyCurrentSession().update(irMModulemenus);
			
			errMsg.add("Success :: Status updated :"+sidInfo.getStatus());
			moduleMenuResponseDetails.setErrMsgs(errMsg);
			moduleMenuResponseDetails.setValidationSuccess(true);
			
			moduleMenuResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
						
		} 
		catch (Exception e) 
		{			
			log.error("Update MenuModule Exceprion risen"+e);
			
			moduleMenuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
	
			throw new HibernateException(e);
		}		
		return moduleMenuResponseDetails;			
	}

/*list outing the modulemenus which are not status =2*/ 

public IRPlusResponseDetails findAllByStatus() throws BusinessException {
	
	IRPlusResponseDetails moduleMenuResponseDetails = new IRPlusResponseDetails();
			
	log.debug("Inside of ModuleMenus ");
	
	IrMModulemenus irMModulemenus = new IrMModulemenus();
	List<String> errMsg = new ArrayList<String>();
	List<ModuleMenuBean> listShow = new ArrayList<ModuleMenuBean>();				
	
	try {
			Criteria criteria=sessionFactory.getCurrentSession().createCriteria(IrMModulemenus.class);
			List<IrMModulemenus> list = criteria.list();
								
				Iterator<IrMModulemenus> iterator = list.iterator();
								
				for (IrMModulemenus mModule : list) {
				
				ModuleMenuBean mmBean = new ModuleMenuBean();
			
							if(mModule.getIsactive()!=2){
								mmBean.setModulemenuid(mModule.getModulemenuid());
								mmBean.setSortingorder(mModule.getSortingorder());
								mmBean.setModuleid(mModule.getIrMModules().getModuleid());
								mmBean.setMenuid(mModule.getIrMMenus().getMenuid());
								mmBean.setCreateddate(mModule.getCreateddate());
								mmBean.setModifieddate(mModule.getModifieddate());
								mmBean.setStatus(mModule.getIsactive());
								
								mmBean.setMenuName(mModule.getIrMMenus().getMenuname());
								mmBean.setModuleName(mModule.getIrMModules().getModulename());
								
								listShow.add(mmBean);
									
							} // if loop
				} //for loop
						if((listShow!=null) && listShow.size()!= 0){
						
						errMsg.add(" Success : ModuleMenus List Size :"+listShow.size());
						moduleMenuResponseDetails.setErrMsgs(errMsg);
						moduleMenuResponseDetails.setRecordsFiltered(listShow.size());
						moduleMenuResponseDetails.setRecordsTotal(listShow.size());
						moduleMenuResponseDetails.setModuleMenuBean(listShow);
						moduleMenuResponseDetails.setValidationSuccess(true);						
						moduleMenuResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
						moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);													
						}else{
							errMsg.add(" Error :: No ModuleMenus List");
							moduleMenuResponseDetails.setErrMsgs(errMsg);						
							moduleMenuResponseDetails.setModuleMenuBean(listShow);							
							moduleMenuResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
							moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);													
						}
		} catch (Exception e) {

			log.error("	Exception raisen  in ModuleMenu	"+e);
			moduleMenuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			throw new HibernateException(e);
			
		}
	
	return moduleMenuResponseDetails;
}

//----------------------------------------------------------------
	@Override
	public IRPlusResponseDetails getMenuModuleById(String moduleMenuId) throws BusinessException {
	
		IRPlusResponseDetails moduleMenuResponseDetails = new IRPlusResponseDetails();
		log.debug("Inside of ModuleMenus :: getMenuModuleById");
		
		IrMModulemenus mmInfo = null;
		
		ModuleMenuBean mmbean = new ModuleMenuBean();
		
		List<ModuleMenuBean> listmmbean = new ArrayList<ModuleMenuBean>();
		
		try {
			
//			mmInfo.setModulemenuid(Integer.parseInt(moduleMenuId));
			
			// Get the data 
			mmInfo = (IrMModulemenus)getMyCurrentSession().get(IrMModulemenus.class, Integer.parseInt(moduleMenuId));
			
			if(mmInfo!=null){
			
			mmbean.setCreateddate(mmInfo.getCreateddate());
			mmbean.setModifieddate(mmInfo.getModifieddate());
			mmbean.setMenuid(mmInfo.getIrMMenus().getMenuid());
			mmbean.setModuleid(mmInfo.getIrMModules().getModuleid());
			mmbean.setSortingorder(mmInfo.getSortingorder());			
			mmbean.setModulemenuid(mmInfo.getModulemenuid());
			
			mmbean.setMenuName(mmInfo.getIrMMenus().getMenuname());
			mmbean.setModuleName(mmInfo.getIrMModules().getModulename());
			
			listmmbean.add(mmbean);
			moduleMenuResponseDetails.setModuleMenuBean(listmmbean);
			moduleMenuResponseDetails.setValidationSuccess(true);
			moduleMenuResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			
			}else{
				moduleMenuResponseDetails.setValidationSuccess(false);
				moduleMenuResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);				
			}
			
		} catch (Exception e) {
			log.error("Exception risen in modulemenugetId ");
			moduleMenuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			throw new HibernateException(e);
		}
		
		return moduleMenuResponseDetails;
	}

	// update old
	
	
	public IRPlusResponseDetails updateMenuModule1(ModuleMenuBean moduleMenuInfo) throws BusinessException {
		
		IRPlusResponseDetails moduleMenuResponseDetails = new IRPlusResponseDetails();
		
		log.debug("Inside of ModuleMenus ");
		
		IrMModulemenus irMModulemenus = new IrMModulemenus();				
		
		try {
			
			IrMMenus irMenus = new IrMMenus();
			irMenus.setMenuid(moduleMenuInfo.getMenuid());
			
			IrMModules irMModules = new IrMModules();
			irMModules.setModuleid(moduleMenuInfo.getModuleid());
			
			irMModulemenus.setSortingorder(moduleMenuInfo.getSortingorder());
			irMModulemenus.setIrMMenus(irMenus);
			irMModulemenus.setIrMModules(irMModules);
			irMModulemenus.setModulemenuid(moduleMenuInfo.getModulemenuid());
			
			getMyCurrentSession().update(irMModulemenus);
		
			moduleMenuResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
						
		} 
		catch (Exception e) 
		{			
			log.error("Update MenuModule Exceprion risen"+e);
			moduleMenuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			throw new HibernateException(e);
		}		
		return moduleMenuResponseDetails;			
	}

	
	@Override
	public IRPlusResponseDetails updateMenuModule(ModuleMenuBean moduleMenuInfo) throws BusinessException {
		
		IRPlusResponseDetails moduleMenuResponseDetails = new IRPlusResponseDetails();		
		
		List<String>  errMsgs = new ArrayList<String>();
		
		log.debug("Inside of updateMenuModule ");
		
		boolean myFlag = true;
		
		try {
						
			if(moduleMenuInfo.getUserId()==null){ 
			myFlag = false; 
			errMsgs.add("Validation Error :: UserId null ");
			moduleMenuResponseDetails.setErrMsgs(errMsgs);
			moduleMenuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}
			if(moduleMenuInfo.getMenuid()==null){
				myFlag = false; 
				errMsgs.add("Validation Error :: Menuid is null");
				moduleMenuResponseDetails.setErrMsgs(errMsgs);
				moduleMenuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);	
			}
			
			if(myFlag){
				
						IrMMenus irMenus = null;						
						irMenus =(IrMMenus) getMyCurrentSession().get(IrMMenus.class ,moduleMenuInfo.getMenuid());
							
							if(irMenus!=null && moduleMenuInfo.getModuleIdArray()!=null){
								
							//	if(moduleMenuInfo.getModulemenuid()){
									Criteria criteria = getMyCurrentSession().createCriteria(IrMModulemenus.class ,"modulmenu" );
									 
								if(!criteria.list().isEmpty()){
										 
									criteria.createAlias("modulmenu.irMMenus", "menu");
									
									List<IrMModulemenus> menuList = criteria.add(Restrictions.eq( "menu.menuid",moduleMenuInfo.getMenuid() )).list();
									
									if(!menuList.isEmpty()){
										
									for (IrMModulemenus irMModulemenus2 : menuList) {
										irMModulemenus2.setIsactive(0); // while updation time all contains are inactive after it will get active
										getMyCurrentSession().update(irMModulemenus2);
										getMyCurrentSession().flush();
									}
									
									 Integer[] moduleid = moduleMenuInfo.getModuleIdArray();
									 
									 Integer[] moduleid_db = new Integer[moduleid.length]; 
									 int i=0;
									for (IrMModulemenus irMModul : menuList){
																	
							//			Collections.frequency(Arrays.asList(moduleid),irMModul.getm );
										
										moduleid_db[i] = irMModul.getIrMModules().getModuleid();
										++i;
										for (Integer id : moduleid) {
											
											if(irMModul.getIrMModules().getModuleid() == id){
												 
												irMModul.setIsactive(1);
												getMyCurrentSession().update(irMModul);
												getMyCurrentSession().flush();
											}
											
										}										
									}
									
									for (Integer id : moduleid) {																				
										
										if( Collections.frequency(Arrays.asList(moduleid_db),id ) == 0 ){
										
											IrMModulemenus mm = new IrMModulemenus();
											
											mm.setIrMMenus((IrMMenus)getMyCurrentSession().get(IrMMenus.class,moduleMenuInfo.getMenuid()));
											mm.setIrMModules((IrMModules)getMyCurrentSession().get(IrMModules.class, id));										
											mm.setIrMUsers((IrMUsers)getMyCurrentSession().get(IrMUsers.class, moduleMenuInfo.getUserId()));
											mm.setIsactive(1);
											mm.setSortingorder(0);
									
											getMyCurrentSession().save(mm);
											getMyCurrentSession().flush();
										}
										
									}
									moduleMenuResponseDetails.setValidationSuccess(true);
									moduleMenuResponseDetails.setErrMsgs(errMsgs);
									moduleMenuResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
									moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);

									}else{
										errMsgs.add("Modulemenu empty");
										moduleMenuResponseDetails.setErrMsgs(errMsgs);
										moduleMenuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
										moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
									}
									
									}																		
							}else{
							errMsgs.add("Validation Error :: Modulemenus is null");
							moduleMenuResponseDetails.setErrMsgs(errMsgs);
							moduleMenuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
							moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
							}
			}
			
			
		} 
		catch(Exception e){
			
			log.error(" Update MenuModule Exceprion risen  "+e);
			moduleMenuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			throw new HibernateException(e);
		}		
		return moduleMenuResponseDetails;			
	}
	
	
	
	@Override
	public IRPlusResponseDetails deleteMenuModuleById(String moduleMenuId) throws BusinessException {
		
		IRPlusResponseDetails moduleMenuResponseDetails = new IRPlusResponseDetails();
		List<String> errMsg = new ArrayList<String>();
		log.debug("	Inside of ModuleMenus ");
		
		IrMModulemenus irMModulemenus = null;
		
		try {
			irMModulemenus	= (IrMModulemenus)getMyCurrentSession().get(IrMModulemenus.class, Integer.parseInt(moduleMenuId));

			if(irMModulemenus ==null){
			
					errMsg.add("Validation Error : Given Id Not available in DB");
					moduleMenuResponseDetails.setErrMsgs(errMsg);
					moduleMenuResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
					moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
		
			}else{			    
				
				getMyCurrentSession().delete(irMModulemenus);

				errMsg.add(" Success : Given Id Deleted in DB");
				moduleMenuResponseDetails.setErrMsgs(errMsg);
				moduleMenuResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				}
				
		} catch (Exception e) {
			log.error("Exception raised in Delete Menumodule "+e);
			moduleMenuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			throw new HibernateException(e);			
		}
		
		return moduleMenuResponseDetails;
}

	
	@Override
	public IRPlusResponseDetails findAllMenuModule() throws BusinessException {
		
		IRPlusResponseDetails moduleMenuResponseDetails = new IRPlusResponseDetails();
				
		log.debug("Inside of ModuleMenus ");
		
		IrMModulemenus irMModulemenus = new IrMModulemenus();
		
		List<ModuleMenuBean> listShow = new ArrayList<ModuleMenuBean>();				
		
		try {
				Criteria criteria=sessionFactory.getCurrentSession().createCriteria(IrMModulemenus.class);
				List<IrMModulemenus> list = criteria.list();
									
					Iterator<IrMModulemenus> iterator = list.iterator();
									
					for (IrMModulemenus mModule : list) {
					
					ModuleMenuBean mmBean = new ModuleMenuBean();
						
					mmBean.setModulemenuid(mModule.getModulemenuid());
					mmBean.setSortingorder(mModule.getSortingorder());
					mmBean.setModuleid(mModule.getIrMModules().getModuleid());
					mmBean.setMenuid(mModule.getIrMMenus().getMenuid());
					mmBean.setCreateddate(mModule.getCreateddate());
					mmBean.setModifieddate(mModule.getModifieddate());
					mmBean.setStatus(mModule.getIsactive());
					
					mmBean.setMenuName(mModule.getIrMMenus().getMenuname());
					mmBean.setModuleName(mModule.getIrMModules().getModulename());
					
					listShow.add(mmBean);
					}
					moduleMenuResponseDetails.setRecordsFiltered(listShow.size());
					moduleMenuResponseDetails.setRecordsTotal(listShow.size());
					moduleMenuResponseDetails.setModuleMenuBean(listShow);
					
					moduleMenuResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
					moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);													
				
			} catch (Exception e) {

				log.error("	Exception raisen  in ModuleMenu	"+e);
				moduleMenuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				throw new HibernateException(e);
				
			}
		
		return moduleMenuResponseDetails;
	}
	
	//============================================
	
	public IRPlusResponseDetails createMenuModuleNoDuplicate(ModuleMenuBean moduleMenuInfo) throws BusinessException {
		
		log.info("Inside of IrMMenuModulesDaoImpl");
		
		List<String> errMsg = new ArrayList<String>();
		
		IRPlusResponseDetails moduleMenuResponseDetails = new IRPlusResponseDetails();
		IrMModulemenus mmInfo = null;
		
		IrMMenus menuref = null;
		IrMModules moduleref =	null;			
		IrMUsers userinfo =  null;		
		boolean flag = false;
		try {			
			mmInfo = new IrMModulemenus();  
			
			if(moduleMenuInfo.getMenuid()==null){				
				errMsg.add("Validation Error : Not Selected ModuleId : Not Available ModuleId");
				moduleMenuResponseDetails.setErrMsgs(errMsg);
				flag = true;
			}
			if(moduleMenuInfo.getModuleid()==null){
				errMsg.add("Validation Error : Not Selected MenuId : Not Available MenuId");
				moduleMenuResponseDetails.setErrMsgs(errMsg);				
				flag = true;
			}
			if(moduleMenuInfo.getUserId() == null){
				errMsg.add("Validation Error : Not Available UserId ");
				moduleMenuResponseDetails.setErrMsgs(errMsg);
				flag = true;
			}
			if(moduleMenuInfo.getStatus() == null){
				errMsg.add("Validation Error : Not Available status");
				moduleMenuResponseDetails.setErrMsgs(errMsg);
				flag = true;
			}
			if(moduleMenuInfo.getSortingorder() == null){
				errMsg.add("Validation Error : Not Available sortingorder");
				moduleMenuResponseDetails.setErrMsgs(errMsg);
				flag = true;
			}
			
					if(flag==false){
										
					menuref = (IrMMenus)sessionFactory.getCurrentSession().get(IrMMenus.class , moduleMenuInfo.getMenuid());
								
					moduleref =(IrMModules)sessionFactory.getCurrentSession().get(IrMModules.class ,moduleMenuInfo.getModuleid());
				
					userinfo = (IrMUsers)sessionFactory.getCurrentSession().get(IrMUsers.class , moduleMenuInfo.getUserId());
			
					if(moduleref!=null && menuref !=null && userinfo != null) { // 3 val strt
						
						errMsg.add("Success : Available userId ,Moduleid ,Userid :"+moduleMenuInfo.getMenuid()+" "
						+moduleMenuInfo.getModuleid()+" "+moduleMenuInfo.getUserId()+"" );
						moduleMenuResponseDetails.setErrMsgs(errMsg);
						
						mmInfo.setIrMMenus(menuref);
						mmInfo.setIrMModules(moduleref);
						moduleref.setModuleid(moduleMenuInfo.getModuleid());
						mmInfo.setSortingorder(moduleMenuInfo.getSortingorder());
						mmInfo.setIrMUsers(userinfo);
						mmInfo.setIsactive(moduleMenuInfo.getStatus());
						
						Criteria criteria=sessionFactory.getCurrentSession().createCriteria(IrMModulemenus.class);
						List<IrMModulemenus> list = criteria.list();
											
							Iterator<IrMModulemenus> iterator = list.iterator();
							boolean flash=false;				
							for (IrMModulemenus mModule : list) {
						
								if(mModule.getIsactive()==2){
										
									if(moduleMenuInfo.getMenuName().equalsIgnoreCase(mModule.getIrMMenus().getMenuname())){
										
										flash=true;
										errMsg.add("Validaton Error : Duplicate Name Entered ");
										moduleMenuResponseDetails.setErrMsgs(errMsg);
										break;
										}
									}else if(moduleMenuInfo.getModuleid()==mModule.getIrMModules().getModuleid()){
										flash=true;
										errMsg.add("Validation Error : Duplicate ModuleId Entered ");
										moduleMenuResponseDetails.setErrMsgs(errMsg);
										break;
									}
							}
						
										if(flash==false){
												
										getMyCurrentSession().save(mmInfo);
										
										errMsg.add("Success : Name Entered :"+moduleMenuInfo.getMenuName()+" : Is saved in DB ");
										moduleMenuResponseDetails.setErrMsgs(errMsg);
										moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
										moduleMenuResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
										moduleMenuResponseDetails.setValidationSuccess(true);
										log.debug("Inside of ModuleMenuDaoImpl class :: createModuleMenu() calling");
										}
							}else {
								
								
								log.error("moduleMenuInfo.getMenuid() not available "+moduleMenuInfo.getMenuid()+""
										+ " moduleMenuInfo.getModuleid() not available "+moduleMenuInfo.getModuleid());
								
								errMsg.add("Error :Not Available userId ,Moduleid ,Userid  : please check those ids not available");
								
								moduleMenuResponseDetails.setErrMsgs(errMsg);
								moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
								moduleMenuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
							}	 // 3 val close		
					}//close flag
					else{
						errMsg.add("Error :Not Available userId ,Moduleid ,Userid  : please check those ids not available");
						
						moduleMenuResponseDetails.setErrMsgs(errMsg);
						moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
						moduleMenuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					}
		}
		catch (Exception e) {
			log.error("Exception in create MenuModule",e);
			moduleMenuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			moduleMenuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		return moduleMenuResponseDetails;
	}
	
}