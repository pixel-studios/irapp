package com.irplus.dao.sitesetup;




import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.hibernate.entities.IRSBank;
import com.irplus.dao.hibernate.entities.IRSLicense;
import com.irplus.dao.hibernate.entities.IRSSite;
import com.irplus.dao.hibernate.entities.IrMBusinessProcess;
import com.irplus.dao.hibernate.entities.IrMDbinfo;
import com.irplus.dao.hibernate.entities.IrMFileTypes;
import com.irplus.dao.hibernate.entities.IrSBusinessProcess;
import com.irplus.dao.hibernate.entities.IrSCustomercontacts;
import com.irplus.dto.BusinessProcessInfo;
import com.irplus.dto.DBDTO;
import com.irplus.dto.FileTypeInfo;
import com.irplus.dto.IRPlusBusinessProcess;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.LicenseInfo;
import com.irplus.dto.SiteInfo;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusUtil;


@Component
@Transactional
public class SiteSetupDaoImpl implements SiteSetupDao {

	private static final Logger LOGGER = Logger.getLogger(SiteSetupDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public IRPlusResponseDetails getSiteInfo(String siteId) throws BusinessException {
		LOGGER.info("Inside SiteSetupDaoImpl  :  getSiteInfo function");
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		
		IRSSite iRSSite=null;
		IRSLicense iRSLicense=null;
		SiteInfo site=null;
		DBDTO dbInfo=null;
		BusinessProcessInfo businessProcessInfo=null;
		
		List<BusinessProcessInfo> iRPlusBusinessProcess = new ArrayList<BusinessProcessInfo>();
		List<FileTypeInfo> fileTypeInfoList = new ArrayList<FileTypeInfo>();
		
		LicenseInfo licenseInfo=null;
		FileTypeInfo fileTypeInfo=null;
		List<FileTypeInfo> fileTypeLists=null;
		IrMFileTypes fileTypesData=null;
		Long licenseId = null;
		try
		{
			Session session = null;
			session = sessionFactory.getCurrentSession();
			
	
			iRSSite = (IRSSite) session.get(IRSSite.class,new Long(siteId));
			
			
			//Criteria criteria = session.createCriteria(IRSSite.class);
			
			
			if(iRSSite!=null){
				 site = new SiteInfo();
				// dbInfo= new DBDTO();
							
				site.setSiteId(iRSSite.getSiteId());
				site.setSiteCode(iRSSite.getSiteCode());
				site.setSiteName(iRSSite.getSiteName());
				site.setContactNo(iRSSite.getContactNo());
				site.setContactPerson(iRSSite.getContactPerson());
				site.setAddress1(iRSSite.getAddress1());
				site.setSiteLogo(iRSSite.getSiteLogo());
				site.setRootFolderPath(iRSSite.getRootFolderPath());
				site.setEmphesoftXmlFolderPath(iRSSite.getEmphesoftXmlFolderPath());
				site.setImagehawkXmlFolderPath(iRSSite.getImagehawkXmlFolderPath());
				site.setOutputTransmissionFolderPath(iRSSite.getOutputTransmissionFolderPath());
				site.setArchiveProcessFilePath(iRSSite.getArchiveProcessFilePath());
				
				licenseId = iRSSite.getLicense().getLicenseId();
				
				if(licenseId!=null){
					licenseInfo=new LicenseInfo();
					String licenseType=	IRPlusUtil.irplusDecrypt(iRSSite.getLicense().getLicensetype());
					String validTo=IRPlusUtil.irplusDecrypt(iRSSite.getLicense().getEnddate());
					
					
					if(licenseType.equals("Validity")){
						
						DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
						String dd=IRPlusUtil.irplusDecrypt(iRSSite.getLicense().getEnddate());
						Date date1 = dateFormat.parse(dd);
						Date date2 = new Date();
						
						
						Long days=getDifferenceDays(date2,date1);
						
						licenseInfo.setLicType(IRPlusUtil.irplusDecrypt(iRSSite.getLicense().getLicensetype()));
						licenseInfo.setValidFrom(IRPlusUtil.irplusDecrypt(iRSSite.getLicense().getStartdate()));
						licenseInfo.setValidTo(IRPlusUtil.irplusDecrypt(iRSSite.getLicense().getEnddate()));
						licenseInfo.setCreateddate(iRSSite.getLicense().getCreateddate());
						licenseInfo.setRemainingDays(days);
						licenseInfo.setIsactive(iRSSite.getLicense().getIsactive());
						site.setLicenseInfo(licenseInfo);
						
				}	else{
					Long days=1L;
					licenseInfo.setLicType(IRPlusUtil.irplusDecrypt(iRSSite.getLicense().getLicensetype()));
					licenseInfo.setCreateddate(iRSSite.getLicense().getCreateddate());
					licenseInfo.setRemainingDays(days);
					licenseInfo.setIsactive(iRSSite.getLicense().getIsactive());
					site.setLicenseInfo(licenseInfo);
					
					
				}
					
				}
				
				Query query = session.createQuery("FROM IrMDbinfo dbinfo WHERE dbinfo.site.siteId=?");
				query.setParameter(0,new Long(siteId));
				List<IrMDbinfo> dbList = query.list();
				
				if(dbList.size()>0){
					for(IrMDbinfo iDbinfo:dbList){
						dbInfo= new DBDTO();
						dbInfo.setHostName(iDbinfo.getHostName());
						dbInfo.setUsername(iDbinfo.getUsername());
						dbInfo.setPassword(iDbinfo.getPassword());
						site.setDbInfo(dbInfo);
					}
				}
				
				
				
				Query bp = session.createQuery("FROM IrMBusinessProcess businessProcess WHERE businessProcess.iRSLicense.licenseId=?");
				bp.setParameter(0,licenseId);
				
				List<IrMBusinessProcess> bpList = bp.list();
				
				if(bpList.size()>0){
					
					for(IrMBusinessProcess bProcess:bpList){
						businessProcessInfo=new BusinessProcessInfo();
						businessProcessInfo.setBusinessProcessId(bProcess.getBusinessProcessId());
						businessProcessInfo.setProcessName(bProcess.getProcessName());
						iRPlusBusinessProcess.add(businessProcessInfo);
						licenseInfo.setBusinessProcessInfo(iRPlusBusinessProcess);
						site.setLicenseInfo(licenseInfo);
					}
					
					
				}
				
				Query fileTypes = session.createQuery("FROM IrMFileTypes fileTypes WHERE fileTypes.iRSLicense.licenseId=?");
				fileTypes.setParameter(0,licenseId);
				
				List<IrMFileTypes> fileTypeList = fileTypes.list();
				
				if(fileTypeList.size()>0){
					
					for(IrMFileTypes irMFileTypes:fileTypeList){
						
						fileTypeInfo = new FileTypeInfo();
						fileTypeInfo.setFiletypeid(irMFileTypes.getFiletypeid());
						fileTypeInfo.setFiletypename(irMFileTypes.getFiletypename());
						fileTypeInfoList.add(fileTypeInfo);
						
						licenseInfo.setFileTypeInfo(fileTypeInfoList);
						site.setLicenseInfo(licenseInfo);
					}
					
					
				}
				
				/*dbInfo.setHostName(iRSSite.getIrMDbinfo().getHostName());
				dbInfo.setUsername(iRSSite.getIrMDbinfo().getUsername());
				dbInfo.setPassword(iRSSite.getIrMDbinfo().getPassword());
				
				site.setDbInfo(dbInfo);*/
				
				/*if(licenseId!=null){
					iRSLicense = (IRSLicense) session.get(IRSLicense.class,licenseId);
					
					if(iRSLicense!=null){
						licenseInfo=new LicenseInfo();
						
						licenseInfo.setLicType(IRPlusUtil.irplusDecrypt(iRSLicense.getLicensetype()));
						//licenseInfo.setValidFrom(IRPlusUtil.irplusDecrypt(iRSLicense.getStartdate()));
						//licenseInfo.setValidTo(IRPlusUtil.irplusDecrypt(iRSLicense.getEnddate()));
						site.setLicenseInfo(licenseInfo);
					}
					
					
				}*/
				
			
				
			}
			
		
			
			
			

			irPlusResponseDetails.setSiteInfo(site);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			
	
		}
		catch(Exception e)
		{
			LOGGER.error("Exception in createBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		
		
		
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateSite(SiteInfo siteInfo) throws BusinessException {
		LOGGER.info("Inside SiteSetupDaoImpl  :  Update Site function");
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		Session session = null;
		IRSSite iRSSite=null;
		IrMDbinfo dbInfo=null;
		try
		{
			session = sessionFactory.getCurrentSession();

			iRSSite = (IRSSite) session.get(IRSSite.class,siteInfo.getSiteId());
		
			if(iRSSite!=null){
				
				iRSSite.setSiteName(siteInfo.getSiteName());
				iRSSite.setContactPerson(siteInfo.getContactPerson());
				iRSSite.setContactNo(siteInfo.getContactNo());
				iRSSite.setAddress1(siteInfo.getAddress1());
				iRSSite.setSiteLogo(siteInfo.getSiteLogo());
				iRSSite.setRootFolderPath(siteInfo.getRootFolderPath());
				iRSSite.setEmphesoftXmlFolderPath(siteInfo.getEmphesoftXmlFolderPath());
				iRSSite.setImagehawkXmlFolderPath(siteInfo.getImagehawkXmlFolderPath());
				iRSSite.setOutputTransmissionFolderPath(siteInfo.getOutputTransmissionFolderPath());
				iRSSite.setArchiveProcessFilePath(siteInfo.getArchiveProcessFilePath());
				
				
				
				
				Query query = session.createQuery("FROM IrMDbinfo dbinfo WHERE dbinfo.site.siteId=?");
				query.setParameter(0,siteInfo.getSiteId());
				List<IrMDbinfo> dbList = query.list();
				
				if(dbList.size()>0){
					
					for(IrMDbinfo irMDbinfo:dbList){
						session.delete(irMDbinfo);
					}
					dbInfo= new IrMDbinfo();
					iRSSite.setSiteId(siteInfo.getSiteId());
					dbInfo.setSite(iRSSite);
					dbInfo.setHostName(siteInfo.getDbInfo().getHostName());
					dbInfo.setUsername(siteInfo.getDbInfo().getUsername());
					dbInfo.setPassword(siteInfo.getDbInfo().getPassword());
					
					session.save(dbInfo);
					
					
				}else{
					dbInfo= new IrMDbinfo();
					iRSSite.setSiteId(siteInfo.getSiteId());
					dbInfo.setSite(iRSSite);
					dbInfo.setHostName(siteInfo.getDbInfo().getHostName());
					dbInfo.setUsername(siteInfo.getDbInfo().getUsername());
					dbInfo.setPassword(siteInfo.getDbInfo().getPassword());
					
					session.save(dbInfo);
				}
				
				
				session.update(iRSSite);
				
				
				
				
				
				
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				
				
			}else{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}
		
		
		}catch(Exception e)
		{
			LOGGER.error("Exception in updateBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		
		
		
		
		
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateFolder(SiteInfo siteInfo) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IRPlusResponseDetails licenseValidity() throws BusinessException {
		
		LOGGER.info("Inside SiteSetupDaoImpl  :  Update Site function");
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		try
		{
			IRSSite iRSSite=null;
			LicenseInfo licenseInfo=null;
			
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery("FROM IRSSite site WHERE site.isactive=?");
			query.setParameter(0,1);
			
			iRSSite=(IRSSite) query.uniqueResult();

			if(iRSSite!=null){
				IRSLicense iRSLicense=null;
				Query license = session.createQuery("FROM IRSLicense license WHERE license.licenseId=? and license.isactive=?");
				license.setParameter(0,iRSSite.getLicense().getLicenseId());
				license.setParameter(1,1);
				iRSLicense=(IRSLicense) license.uniqueResult();
				
				if(iRSLicense!=null){
					
					 licenseInfo = new LicenseInfo();
					
					licenseInfo.setLicType(IRPlusUtil.irplusDecrypt(iRSSite.getLicense().getLicensetype()));
					licenseInfo.setValidFrom(IRPlusUtil.irplusDecrypt(iRSSite.getLicense().getStartdate()));
					licenseInfo.setValidTo(IRPlusUtil.irplusDecrypt(iRSSite.getLicense().getEnddate()));
					
					
				}

				
				irPlusResponseDetails.setLicenseInfo(licenseInfo);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);	
			}else{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}
			
			
			
			
			
		}catch(Exception e){
			LOGGER.error("Exception in updateBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails siteInfo() throws BusinessException {
		LOGGER.info("Inside SiteSetupDaoImpl  :  siteInfo function");
			IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
			
			
			IRSSite iRSSite=null;
			
			SiteInfo site=null;
			LicenseInfo licenseInfo=null;
			Long licenseId = null;
			
			try
			{
				Session session = null;
				session = sessionFactory.getCurrentSession();
				
				
				Query query = session.createQuery("FROM IRSSite site WHERE site.isactive=?");
				query.setParameter(0,1);
				List<IRSSite> siteList=query.list();
				iRSSite=(IRSSite)query.uniqueResult();
				
				
				if(iRSSite!=null){
					 site = new SiteInfo();
					
								
					site.setSiteId(iRSSite.getSiteId());
					site.setSiteCode(iRSSite.getSiteCode());
					site.setSiteName(iRSSite.getSiteName());
					site.setContactNo(iRSSite.getContactNo());
					site.setContactPerson(iRSSite.getContactPerson());
					site.setAddress1(iRSSite.getAddress1());
					site.setSiteLogo(iRSSite.getSiteLogo());
					
					
					licenseId = iRSSite.getLicense().getLicenseId();
					
					if(licenseId!=null){
						licenseInfo=new LicenseInfo();
						String licenseType=	IRPlusUtil.irplusDecrypt(iRSSite.getLicense().getLicensetype());
						
						
					if(licenseType.equals("Validity")){
							
							DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
							String dd=IRPlusUtil.irplusDecrypt(iRSSite.getLicense().getEnddate());
							Date date1 = dateFormat.parse(dd);
							Date date2 = new Date();
							
							
							Long days=getDifferenceDays(date2,date1);
							
							licenseInfo.setLicType(IRPlusUtil.irplusDecrypt(iRSSite.getLicense().getLicensetype()));
							licenseInfo.setValidFrom(IRPlusUtil.irplusDecrypt(iRSSite.getLicense().getStartdate()));
							licenseInfo.setValidTo(IRPlusUtil.irplusDecrypt(iRSSite.getLicense().getEnddate()));
							licenseInfo.setCreateddate(iRSSite.getLicense().getCreateddate());
							licenseInfo.setRemainingDays(days);
							licenseInfo.setIsactive(iRSSite.getLicense().getIsactive());
							site.setLicenseInfo(licenseInfo);
							
					}	else{
						Long days=1L;
						licenseInfo.setLicType(IRPlusUtil.irplusDecrypt(iRSSite.getLicense().getLicensetype()));
						licenseInfo.setCreateddate(iRSSite.getLicense().getCreateddate());
						licenseInfo.setRemainingDays(days);
						licenseInfo.setIsactive(iRSSite.getLicense().getIsactive());
						site.setLicenseInfo(licenseInfo);
						
						
					}



					}
				
					
			
				
				
				

				irPlusResponseDetails.setSiteInfo(site);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				
				}else{
					site = new SiteInfo();
					licenseInfo=new LicenseInfo();
					licenseInfo.setLicType("Expired");
					site.setLicenseInfo(licenseInfo);

					irPlusResponseDetails.setSiteInfo(site);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
					
				}
			}
			catch(Exception e)
			{
				LOGGER.error("Exception in createBank",e);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				throw new HibernateException(e);
			}
			
			
			
			return irPlusResponseDetails;
	}

	
	private long getDifferenceDays(Date d1, Date d2) {
	    long diff = d2.getTime() - d1.getTime();
	    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}
}
