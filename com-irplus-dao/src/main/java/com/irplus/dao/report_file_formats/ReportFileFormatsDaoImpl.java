package com.irplus.dao.report_file_formats;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.hibernate.entities.IrMFieldtypes;
import com.irplus.dao.hibernate.entities.IrMFileTypes;
import com.irplus.dao.hibernate.entities.IrMReportfileFormats;
import com.irplus.dao.hibernate.entities.IrMUsers;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.ReportFileFormatInfo;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;

@Transactional
@Component
public class ReportFileFormatsDaoImpl implements IReportFileFormatsDao{

	private static final Logger log =  Logger.getLogger(ReportFileFormatsDaoImpl.class);
	
	@Autowired
	SessionFactory sessionFactory;

	public Session getMyCurrentSession() {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
			log.error("Exception raised DaoImpl :: At current session creation time ::" + e);
			throw new HibernateException(e);
		}
		return session;
	}
	
	
	@Override
	public IRPlusResponseDetails createReportFileFormat(ReportFileFormatInfo reportFileFormatInfo)
			throws BusinessException {
		
		log.info("Inside of IrMReportfileFormatsDaoImpl");
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		IrMFieldtypes irMFieldtypes = new IrMFieldtypes();

		IrMReportfileFormats irMReportfileFormats = new IrMReportfileFormats();
		try {
			irMReportfileFormats.setFileformatName(reportFileFormatInfo.getFileformatName());
			irMReportfileFormats.setIrMFiletypes((IrMFileTypes)getMyCurrentSession().get(IrMFileTypes.class ,reportFileFormatInfo.getFiletypeId()));
			irMReportfileFormats.setIrMUsers((IrMUsers)getMyCurrentSession().get(IrMUsers.class, reportFileFormatInfo.getUserId()));
			irMReportfileFormats.setIrSCustomerReportlicensings(reportFileFormatInfo.getIrSCustomerReportlicensings());
			irMReportfileFormats.setIsactive(reportFileFormatInfo.getIsactive());
	
			getMyCurrentSession().save(irMReportfileFormats);

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);

		} catch (Exception e) {
			log.error("Exception in create IrMReportfileFormats", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}
	
	
	@Override
	public IRPlusResponseDetails getReportFileFormatById(String reportFileFormatInfoId) throws BusinessException {
		
		log.info("Inside of getReportFileFormat :: ReportFileFormatdaoimpl");
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		ReportFileFormatInfo reportFileFormatInfo = new ReportFileFormatInfo();
		
		List<ReportFileFormatInfo> listReportFileFormatInfo = new ArrayList<ReportFileFormatInfo>();
		
		IrMReportfileFormats irMReportfileFormats;
		try {
			irMReportfileFormats = (IrMReportfileFormats) getMyCurrentSession().get(IrMReportfileFormats.class,Integer.parseInt(reportFileFormatInfoId));

			if (irMReportfileFormats != null) {

				reportFileFormatInfo.setFileFormatId(irMReportfileFormats.getFileFormatId());
				reportFileFormatInfo.setFileformatName(irMReportfileFormats.getFileformatName());
				reportFileFormatInfo.setFiletypeId(irMReportfileFormats.getIrMFiletypes().getFiletypeid());
				reportFileFormatInfo.setIrSCustomerReportlicensings(irMReportfileFormats.getIrSCustomerReportlicensings());
				reportFileFormatInfo.setIsactive(irMReportfileFormats.getIsactive());
				reportFileFormatInfo.setModifieddate(irMReportfileFormats.getModifieddate());
				reportFileFormatInfo.setUserId(irMReportfileFormats.getIrMUsers().getUserid());
				reportFileFormatInfo.setCreateddate(irMReportfileFormats.getCreateddate());
			
				listReportFileFormatInfo.add(reportFileFormatInfo);

				irPlusResponseDetails.setReportFileFormatInfos(listReportFileFormatInfo);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			} else {
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}

		} catch (Exception e) {

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception Raised inside listReportFileFormatInfo DaoImple :: " + e);
			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}
	
	
	@Override
	public IRPlusResponseDetails updateReportFileFormat(ReportFileFormatInfo reportFileFormatInfo)
			throws BusinessException {

		log.info("Inside of updateReportFileFormat");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		IrMFieldtypes irMFieldtypes = null;
		IrMReportfileFormats irMReportfileFormats = null;

		try {
			irMReportfileFormats = (IrMReportfileFormats) getMyCurrentSession().get(IrMReportfileFormats.class,
					reportFileFormatInfo.getFileFormatId());

			irMReportfileFormats.setFileFormatId(reportFileFormatInfo.getFileFormatId());
			irMReportfileFormats.setFileformatName(reportFileFormatInfo.getFileformatName());
			irMReportfileFormats.setIrMUsers((IrMUsers)getMyCurrentSession().get(IrMUsers.class,reportFileFormatInfo.getUserId()));
	//		irMReportfileFormats.setIrSCustomerReportlicensings();
			irMReportfileFormats.setIsactive(reportFileFormatInfo.getIsactive());
			irMReportfileFormats.setIrMFiletypes((IrMFileTypes)getMyCurrentSession().get(IrMFileTypes.class ,reportFileFormatInfo.getFiletypeId()));
			irMReportfileFormats.setCreateddate(reportFileFormatInfo.getCreateddate());
			
			getMyCurrentSession().update(irMReportfileFormats);

			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			irPlusResponseDetails.setValidationSuccess(true);

		} catch (Exception e) {

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception Raised inside ReportfileFormatsDaoImpl :: updateMReportfileFormatsId : " + e);

			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails deleteReportFileFormatById(String reportFileFormatInfoId) throws BusinessException {
		
		log.info("inside of delete ReportFileFormat");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		try {
			Query query = getMyCurrentSession().createQuery("delete IrMReportfileFormats i  where i.fileFormatId =:fileFormat_Id");
			query.setParameter("fileFormat_Id", Integer.parseInt(reportFileFormatInfoId));

			int result = query.executeUpdate();

			if (result == 0) {
				log.debug("deleted ReportFileFormat failed to delete");
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);

			} else {
				log.debug("succcessfully deleted ReportFileFormat :: ReportFileFormat" + reportFileFormatInfoId);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
		} catch (Exception e) {
			log.error("Exception raised in deleted ReportFileFormat", e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}
	
	
	@Override
	public IRPlusResponseDetails showAllReportFileFormat() throws BusinessException {
		log.info("Inside reportFileFormatInfoDaoimpl :: showAllReportFileFormat");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		List<ReportFileFormatInfo> listReportFileFormatInfo = new ArrayList<ReportFileFormatInfo>();

		try {

			Criteria criteria = getMyCurrentSession().createCriteria(IrMReportfileFormats.class);

			List<IrMReportfileFormats> listReportFormats = (List<IrMReportfileFormats>)criteria.list();

			if (!listReportFormats.isEmpty()) {

				for (IrMReportfileFormats irMReportfileFormats : listReportFormats) {

					ReportFileFormatInfo reportFileFormatInfo = new ReportFileFormatInfo();
									
					reportFileFormatInfo.setFileFormatId(irMReportfileFormats.getFileFormatId());
					reportFileFormatInfo.setFileformatName(irMReportfileFormats.getFileformatName());				
					reportFileFormatInfo.setFiletypeId(irMReportfileFormats.getIrMFiletypes().getFiletypeid());
		//			reportFileFormatInfo.setIrSCustomerReportlicensings(irMReportfileFormats.getIrSCustomerReportlicensings());
					reportFileFormatInfo.setIsactive(irMReportfileFormats.getIsactive());
					reportFileFormatInfo.setModifieddate(irMReportfileFormats.getModifieddate());
					reportFileFormatInfo.setUserId(irMReportfileFormats.getIrMUsers().getUserid());
					reportFileFormatInfo.setCreateddate(irMReportfileFormats.getCreateddate());
					
					listReportFileFormatInfo.add(reportFileFormatInfo);
				}

				irPlusResponseDetails.setReportFileFormatInfos(listReportFileFormatInfo);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);

			} else {
				log.debug("reportFileFormatInfo not available :: Empty Data :: Data required to showAllreportFileFormatInfo");
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}

		} catch (Exception e) {
			log.error("Exception raised in show listreportFileFormatInfo ", e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);

			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}

}