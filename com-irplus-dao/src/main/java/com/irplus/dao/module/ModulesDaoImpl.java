package com.irplus.dao.module;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.hibernate.entities.IrMModules;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.module.ModuleBean;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;

@Component
@Transactional
public class ModulesDaoImpl implements IModulesDao{

	private static final Log log = LogFactory.getLog(ModulesDaoImpl.class);

	@Autowired 
	private SessionFactory sessionFactory;
	
// create module
	@Override
	public IRPlusResponseDetails createModule(ModuleBean moduleInfo) throws BusinessException {
		
	log.info("Inside ModulesDaoImpl");
		
	IRPlusResponseDetails moduleResponseDetails=new IRPlusResponseDetails();
		
		try {
			
			IrMModules irMModules = new IrMModules();			
			
			irMModules.setModulename(moduleInfo.getModulename());
			irMModules.setDescription(moduleInfo.getDescription());
			irMModules.setModulepath(moduleInfo.getModulepath());
			irMModules.setModuleicon(moduleInfo.getModuleicon());
			irMModules.setIsreport(moduleInfo.getIsreport());			
			irMModules.setIsactive(moduleInfo.getIsactive());
			irMModules.setUserid(moduleInfo.getUserid());		
		
			sessionFactory.getCurrentSession().save(irMModules);
			
			moduleResponseDetails.setValidationSuccess(true);
			moduleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			moduleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			log.debug("Inside ModueldaoImpl :: success fully saved Module in db");
		}
		catch (Exception e) {
			log.error("Exception in create Menu",e);
			moduleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		
		return moduleResponseDetails;
		
	}

	// Create module if there not create
// un complete functionality	
//	@override
	public IRPlusResponseDetails createByConditionModule(ModuleBean moduleInfo) throws BusinessException {
		
		log.info("Inside ModulesDaoImpl");
		
		List<String> errMsgs = new ArrayList<String>();
		
		Session session = sessionFactory.getCurrentSession();
			
		IRPlusResponseDetails moduleResponseDetails=new IRPlusResponseDetails();
			
			try {
				
			/* Removing the duplicates	*/
				
				Criteria criteria= session.createCriteria(IrMModules.class);
				List<IrMModules> list =criteria.list();
					
				Iterator<IrMModules> modulelist=(Iterator<IrMModules>) list.iterator();
				 
					if(!list.isEmpty()) {
						
						IrMModules irMModules2 = null;
						boolean flag= false;
//						HashSet<IrMModules> hlist = new HashSet<IrMModules>();
								while (modulelist.hasNext()) {
									
									irMModules2 = (IrMModules) modulelist.next();
									 if(moduleInfo.getModulename().equalsIgnoreCase(irMModules2.getModulename())){
										
									 }else{
//										 hlist.add(irMModules2);
									 } 
										
																			
								}
								if(moduleInfo.getModulename().equalsIgnoreCase(irMModules2.getModulename())){
									
								}else{
									flag=true;
								}
								if(flag){
									try {
										
										IrMModules irMModules = new IrMModules();			
										
										irMModules.setModulename(moduleInfo.getModulename());
										irMModules.setDescription(moduleInfo.getDescription());
										irMModules.setModulepath(moduleInfo.getModulepath());
										irMModules.setModuleicon(moduleInfo.getModuleicon());
										irMModules.setIsreport(moduleInfo.getIsreport());			
										irMModules.setIsactive(moduleInfo.getIsactive());
										irMModules.setUserid(moduleInfo.getUserid());		

										session.save(irMModules);
										
										moduleResponseDetails.setValidationSuccess(true);
										moduleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
										moduleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
										log.debug("Inside ModueldaoImpl :: success fully saved Module in db");
									}
									catch (Exception e) {
										log.error("Exception in create Menu",e);
										moduleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
										moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
										throw new HibernateException(e);
									}
									
								}
									
					}else{

						try {
							
							IrMModules irMModules = new IrMModules();			
							
							irMModules.setModulename(moduleInfo.getModulename());
							irMModules.setDescription(moduleInfo.getDescription());
							irMModules.setModulepath(moduleInfo.getModulepath());
							irMModules.setModuleicon(moduleInfo.getModuleicon());
							irMModules.setIsreport(moduleInfo.getIsreport());			
							irMModules.setIsactive(moduleInfo.getIsactive());
							irMModules.setUserid(moduleInfo.getUserid());		
						
							sessionFactory.getCurrentSession().save(irMModules);
							moduleResponseDetails.setValidationSuccess(true);
							moduleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
							moduleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
							log.debug("Inside ModueldaoImpl :: success fully saved Module in db");
						}
						catch (Exception e) {
							log.error("Exception in create Menu",e);
							moduleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
							moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
							throw new HibernateException(e);
						}												
					
					}					
			}
			catch (Exception e) {
				log.error("Exception in create Menu",e);
				moduleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				throw new HibernateException(e);
			}
			
			return moduleResponseDetails;
			
		}

/*	Create module without duplicate moduleName */
	
	public IRPlusResponseDetails createModuleNoDuplct(ModuleBean moduleInfo) throws BusinessException {
		
		log.info("Inside ModulesDaoImpl");
		
		IRPlusResponseDetails moduleResponseDetails=new IRPlusResponseDetails();
		
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(IrMModules.class);
		
		List<ModuleBean> listInfo = new ArrayList<ModuleBean>();
		
		List<IrMModules> list = criteria.list();
				
		List<String> errorMsg = new ArrayList<String>(); 
 		
		boolean flag = false;
 		
		IrMModules irMModules=new IrMModules();
	
 		try{
		
 			if(!list.isEmpty()){
		
					for (IrMModules irMModules1 : list){
						
						if(moduleInfo.getModulename().equalsIgnoreCase(irMModules1.getModulename())){
							flag = true;
							break;
						}
					}
						if(flag){
							
							moduleResponseDetails.setValidationSuccess(false);											
							errorMsg.add("Validation Error: Duplicate Name Entered");							
							moduleResponseDetails.setErrMsgs(errorMsg);				
							moduleResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
							moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
						}else{													
							
							irMModules.setModulename(moduleInfo.getModulename());
							irMModules.setDescription(moduleInfo.getDescription());
							irMModules.setIsreport(moduleInfo.getIsreport());
							irMModules.setIsactive(moduleInfo.getIsactive());
							irMModules.setIsreport(moduleInfo.getIsreport());
							irMModules.setModuleicon(moduleInfo.getModuleicon());

							irMModules.setModulepath(moduleInfo.getModulepath());
							irMModules.setUserid(moduleInfo.getUserid());
							
							sessionFactory.getCurrentSession().save(irMModules);
							
							errorMsg.add(" Success :: Module successfully saved in DB ");							
							moduleResponseDetails.setErrMsgs(errorMsg);
							moduleResponseDetails.setValidationSuccess(true);
							moduleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
							moduleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
						
						}					
		}else{
			
			irMModules.setModulename(moduleInfo.getModulename());
			irMModules.setDescription(moduleInfo.getDescription());
			irMModules.setIsreport(moduleInfo.getIsreport());
			irMModules.setIsactive(moduleInfo.getIsactive());
			irMModules.setIsreport(moduleInfo.getIsreport());
			irMModules.setModuleicon(moduleInfo.getModuleicon());

			irMModules.setModulepath(moduleInfo.getModulepath());
			irMModules.setUserid(moduleInfo.getUserid());

			sessionFactory.getCurrentSession().save(irMModules);
			
			log.debug(" Success :: Module successfully saved in DB");
			
			errorMsg.add(" Success :: Module successfully saved in DB ");							
			moduleResponseDetails.setErrMsgs(errorMsg);
			moduleResponseDetails.setValidationSuccess(true);
			moduleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			moduleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			
		}
		}catch (Exception e) {
			log.error("Exception in create Module",e);
			moduleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		return moduleResponseDetails;
	}
	
	
	/*	isactive status updating by id based */
	
	public IRPlusResponseDetails updateModuleStatus(StatusIdInfo siInfo) throws BusinessException {

		log.info("Inside ModulesDaoImpl");
		
		IRPlusResponseDetails menuResponseDetails=new IRPlusResponseDetails();
		
		Integer moduleInfoId=siInfo.getId();

		IrMModules irmoduleOrg=null;
		List<String> errMsg = new ArrayList<String>();
								
					try {
						
					irmoduleOrg=(IrMModules) sessionFactory.getCurrentSession().get(IrMModules.class, moduleInfoId);
					
						if(irmoduleOrg != null){			
							log.debug("Successfully updated Status  ");
							
								irmoduleOrg.setIsactive(siInfo.getStatus());								

							sessionFactory.getCurrentSession().update(irmoduleOrg);    //update the items
							menuResponseDetails.setValidationSuccess(true);
							errMsg.add(" Success : Successfully updated Status ");
							menuResponseDetails.setErrMsgs(errMsg);
							menuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
							menuResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
							}
							else
							{
							errMsg.add(" Error : Not updated Status **");
							menuResponseDetails.setErrMsgs(errMsg);
							menuResponseDetails.setValidationSuccess(false);
							menuResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
							menuResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
						}
						
							} catch (Exception e) {
					log.error("Exception in update Module",e);
					menuResponseDetails.setValidationSuccess(false);
					menuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
					menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					throw new HibernateException(e);
				}
		return menuResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateModule(ModuleBean moduleInfo) throws BusinessException {
				
	log.info("Inside IrMMenusDaoImpl");
		
	IRPlusResponseDetails moduleResponseDetails=new IRPlusResponseDetails();
		
		Integer menuInfoId=moduleInfo.getModuleid();
		
		IrMModules irMModules=null;
		List<String> errorMsg = new ArrayList<String>();
		
		List<ModuleBean> list = new ArrayList<ModuleBean>();
		List<String> errMsg = new ArrayList<String>();
		boolean flag = false;
		
		try {
	
		Session	session = sessionFactory.getCurrentSession();
		irMModules=(IrMModules) session.get(IrMModules.class, moduleInfo.getModuleid());
    
		/* Duplicate module name remove	*/
		
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(IrMModules.class);
		
		List<IrMModules> list_all = criteria.list();
			
	//		Iterator<IrMModules> modulelist=(Iterator<IrMModules>) list.iterator();
		
	//	
		if (irMModules!=null) {
			
			for (IrMModules moduleBean : list_all){
				
				if(moduleBean.getModuleid()== moduleInfo.getModuleid()){
					if(moduleBean.getModulename().equalsIgnoreCase(moduleInfo.getModulename())){
						flag=false;
					}
				}
				if(moduleBean.getModuleid()!= moduleInfo.getModuleid()){
					if(moduleBean.getModulename().equalsIgnoreCase(moduleInfo.getModulename())){
						flag=true;
						break;
					}
				}
			}	
				
				if(flag){
					
					moduleResponseDetails.setValidationSuccess(false);
					errorMsg.add(" Error :: Module Name Duplicated Not updated : "+moduleInfo.getModulename());							
					moduleResponseDetails.setErrMsgs(errorMsg);
					moduleResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
					moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					
				}else{
					
					irMModules.setModulename(moduleInfo.getModulename());
					irMModules.setModuleicon(moduleInfo.getModuleicon());
					irMModules.setModulepath(moduleInfo.getModulepath());
					irMModules.setDescription(moduleInfo.getDescription());
					irMModules.setIsreport(moduleInfo.getIsreport());				
					irMModules.setUserid(moduleInfo.getUserid());
					irMModules.setIsactive(moduleInfo.getIsactive());
					
					session.update(irMModules);
					
					moduleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
					moduleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
					moduleResponseDetails.setValidationSuccess(true);
					errorMsg.add(" Success :: Module successfully updated in DB ");							
					moduleResponseDetails.setErrMsgs(errorMsg);
				}
			
			
		} else {
			moduleResponseDetails.setValidationSuccess(false);
			errorMsg.add(" Error :: Module not updated in DB ");							
			moduleResponseDetails.setErrMsgs(errorMsg);
			moduleResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			moduleResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
		}
		}
			catch(Exception e)
			{
			moduleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception in update Module",e);
			throw new HibernateException(e);
		}
		
		return moduleResponseDetails;
	}


	@Override
	public IRPlusResponseDetails findAllModules() throws BusinessException {
		
		IRPlusResponseDetails moduleResponseDetails=new IRPlusResponseDetails();
		IrMModules irMModules2=null;
		
		List<ModuleBean> listBean=new ArrayList<ModuleBean>();
		System.out.println("find all modules (");
		
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(IrMModules.class);
		List<IrMModules> list =criteria.list();
			
			Iterator<IrMModules> modulelist=(Iterator<IrMModules>) list.iterator();
		 
			if(!list.isEmpty()) {
			try {

						while (modulelist.hasNext()) {
							
						irMModules2 = (IrMModules) modulelist.next();					
							
						ModuleBean mBean=new ModuleBean();
						
						mBean.setModulename(irMModules2.getModulename());
						mBean.setModuleicon(irMModules2.getModuleicon());
						mBean.setModuleid(irMModules2.getModuleid());
						mBean.setModulepath(irMModules2.getModulepath());
						mBean.setUserid(irMModules2.getUserid());
						mBean.setCreateddate(irMModules2.getCreateddate());
						mBean.setModifieddate(irMModules2.getModifieddate());
						mBean.setIsreport(irMModules2.getIsreport());
						mBean.setDescription(irMModules2.getDescription());
						mBean.setIsactive(irMModules2.getIsactive());
						
						listBean.add(mBean);
					}
					if (!listBean.isEmpty()) {
					
						log.debug("Get the All Modules");
						moduleResponseDetails.setModule(listBean);
						moduleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
						moduleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
						
						moduleResponseDetails.setRecordsTotal(listBean.size());				
						moduleResponseDetails.setRecordsFiltered(listBean.size());
						
						moduleResponseDetails.setValidationSuccess(true);
					} else {
						log.debug("NOt available Modules");
						moduleResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
						moduleResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
						moduleResponseDetails.setValidationSuccess(false);
					}						
		}catch (Exception e) {
			moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			moduleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception in Find all Modules",e);
		}
			}
			else {
				log.debug("NOt available Modules");
				moduleResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				moduleResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				moduleResponseDetails.setValidationSuccess(false);
			}
			
		return moduleResponseDetails;
	}
	

	@Override
	public IRPlusResponseDetails getModuleById(String moduleid) throws BusinessException {
	
		IRPlusResponseDetails moduleResponseDetails=new IRPlusResponseDetails();	
		
		List<ModuleBean> addModuleList = new ArrayList<ModuleBean>();
		
		ModuleBean moduleInfo=null;
		
		IrMModules irMModules=null;
		
		Session session=sessionFactory.getCurrentSession();
		irMModules=(IrMModules) session.get(IrMModules.class,Integer.parseInt(moduleid));
		
		try {
		
			if(irMModules!=null) {
			
				moduleInfo=new ModuleBean();
			 
			 moduleInfo.setModulename(irMModules.getModulename());
			 moduleInfo.setModuleicon(irMModules.getModuleicon());
			 moduleInfo.setModulepath( irMModules.getModulepath());
			 moduleInfo.setDescription(irMModules.getDescription());
			 moduleInfo.setIsreport(irMModules.getIsreport());
			 moduleInfo.setModulepath(irMModules.getModulepath());
  			 moduleInfo.setUserid(irMModules.getUserid());
			 moduleInfo.setModifieddate(irMModules.getModifieddate());
			 moduleInfo.setCreateddate(irMModules.getCreateddate());
			 moduleInfo.setModuleid(irMModules.getModuleid());
			 moduleInfo.setIsactive(irMModules.getIsactive());
			
			addModuleList.add(moduleInfo);
			
			log.debug("get successful, module instance found");
			moduleResponseDetails.setValidationSuccess(true);
			 moduleResponseDetails.setModule(addModuleList);
			 moduleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			 moduleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);

		}
		else {
			log.debug("get successful, no menu instance found");
			moduleResponseDetails.setValidationSuccess(false);
			moduleResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			moduleResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
		}
		}catch (Exception e) {
			
			log.error("Exception in find Menu",e);
			moduleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		return moduleResponseDetails;
	}

//checking
	@Override
	public IRPlusResponseDetails deleteModuleById(String moduleId) throws BusinessException {
	
		IRPlusResponseDetails moduleResponseDetails=new IRPlusResponseDetails();
		
		log.info("Inside IrmodulesDaoImpl");
		log.debug("deleting IrMModules instance");
		
		List<String> errMsg = new ArrayList<String>();
		IrMModules irMModules=null;
		Integer id= Integer.parseInt(moduleId);
		try{	
			
			irMModules =(IrMModules)sessionFactory.getCurrentSession().get(IrMModules.class,id);
			
			
			if(irMModules!=null){			
				sessionFactory.getCurrentSession().delete(irMModules);
				log.debug("delete module");
						
				errMsg.add("Success :: Deleted the Module :"+id);
				moduleResponseDetails.setErrMsgs(errMsg);
				moduleResponseDetails.setValidationSuccess(true);
				moduleResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				moduleResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}else {

				log.debug("Not delete module failed to delete");
				errMsg.add("Error :: ModuleId not available :: Not Deleted the Module ");
				moduleResponseDetails.setErrMsgs(errMsg);
				moduleResponseDetails.setValidationSuccess(false);
				moduleResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				moduleResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				
			}			
			
		} catch (Exception e) {
			log.error("Exception in delete Module",e);
			moduleResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			moduleResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.debug("delete module failed", e);
		}
		return moduleResponseDetails;
	}		
}