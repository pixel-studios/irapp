package com.irplus.dao.bankdao;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.contactmgmt.IContactMgmtDao;
import com.irplus.dao.hibernate.entities.IRSBank;
import com.irplus.dao.hibernate.entities.IRSBankLicensing;
import com.irplus.dao.hibernate.entities.IRSSite;
import com.irplus.dao.hibernate.entities.IrFPBatchHeaderRecord;
import com.irplus.dao.hibernate.entities.IrFPFileHeaderRecord;
import com.irplus.dao.hibernate.entities.IrSBankBranch;
import com.irplus.dao.hibernate.entities.IrSBankContact;
import com.irplus.dao.hibernate.entities.IrSSequence;
import com.irplus.dao.hibernate.entities.IrMBusinessProcess;
import com.irplus.dao.hibernate.entities.IrMFileTypes;
import com.irplus.dao.hibernate.entities.IrMUsers;
import com.irplus.dao.hibernate.entities.IrMUsersBank;
import com.irplus.dto.BankFilterInfo;
import com.irplus.dto.BankInfo;
import com.irplus.dto.BankViewInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.LocationViewInfo;
import com.irplus.dto.fileprocessing.FileHeaderRecordBean;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusUtil;

@Component

@Transactional
public class BankMgmtDaoImpl implements IBankMgmtDao {

	private static final Logger LOGGER = Logger.getLogger(BankMgmtDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	IContactMgmtDao iContactMgmtDao;
	
	

	@Override
	public IRPlusResponseDetails createBank(BankInfo bankInfo) throws BusinessException {

		LOGGER.info("Inside BankMgmtDaoImpl");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;
		
		session = sessionFactory.getCurrentSession();
		IrMUsers user = new IrMUsers();
		IrSSequence sequence = null;
		IRSSite iRSSite=null;
		String filePath=null;
		String bankFolder=null;
		try
		{
		 iRSSite = (IRSSite) session.get(IRSSite.class, new Long(bankInfo.getSiteId()));
		
		if(iRSSite!=null){
			String rootFolder = iRSSite.getRootFolderPath();	
		
		
		
		
			 filePath =rootFolder+File.separator+"BANKS"+File.separator+bankInfo.getBankFolder();
			bankFolder=bankInfo.getBankFolder();;
			 File files = new File(filePath);
		        if (!files.exists()) {
		            if (files.mkdirs()) {
		                System.out.println("Multiple directories are created!");
		            } else {
		                System.out.println("Failed to create multiple directories!");
		            }
		        }
			
			
		 }
			
			IRSBank irsBank = new IRSBank();

			irsBank.setBankName(bankInfo.getBankName());
			irsBank.setBankLogo(bankInfo.getBankLogo());
			irsBank.setBankFolder(bankFolder);
			irsBank.setIRBankCode(bankInfo.getBankCode());
			irsBank.setSiteId(bankInfo.getSiteId());
			irsBank.setIsactive(bankInfo.getIsactive());
			irsBank.setRtNumber(bankInfo.getRtNo().trim());
			irsBank.setDdaNumber(bankInfo.getDdano());
			
			user.setUserid(bankInfo.getUserId());
			irsBank.setIrMUsers_id(user);
			
			
			
			

			Long bankId = (Long) session.save(irsBank);
			
			if(bankId>0){
				
				sequence = (IrSSequence) session.get(IrSSequence.class,1);
				int sequenceNo=sequence.getSequenceNumber();
				sequence.setSequenceNumber(sequenceNo+1);
				
				session.update(sequence);
			}
			
			LOGGER.debug("Bank Info is saved :"+bankId);
			session.flush();
			bankInfo.setBankId(bankId);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}
		catch(Exception e)
		{
			LOGGER.error("Exception in createBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}


	@Override
	public IRPlusResponseDetails listBanks(String userId) throws BusinessException {

		LOGGER.info("Inside listBanks");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		List<BankViewInfo>banks = new ArrayList<BankViewInfo> ();

		try
		{
			
		
			session = sessionFactory.getCurrentSession();
			
		
			
			
			
			
			//Query query = session.createQuery("from IRSBank bank where bank.siteId =:siteId");

			//query.setParameter("siteId", siteId);
			
			/*isActive Status Code
			
			0-inactive
			1-active
			2-deleted*/
		int userid = Integer.parseInt(userId);
		
		
		
		
		
		//	String hql = "from IRSBank bank where bank.irMUsers_id.userid=? and bank.isactive!=?";
		
			String hql = "from IrMUsersBank userBank where userBank.irMUsers.userid=? and userBank.isactive!=?";
			Query query = session.createQuery(hql);

			query.setParameter(0,userid);
			query.setParameter(1,2);

			//			List<IRSBank> bankList = (List<IRSBank>)query.list();
			
			List<IrMUsersBank> userBankList = (List)query.list();
			
			//List<IRSBank> bankList = (List)query.list();

			Iterator <IrMUsersBank> bankListIt = userBankList.iterator();

			IrMUsersBank irMUsersBank = null;

			BankViewInfo bankViewInfo = null;

			IrSBankContact bankContact = null;

			while (bankListIt.hasNext()) 
			{
				irMUsersBank = bankListIt.next();

				bankViewInfo = new BankViewInfo();

				bankViewInfo.setSiteId(irMUsersBank.getBank().getSiteId());

				bankViewInfo.setBankId(irMUsersBank.getBank().getBankId());

				bankViewInfo.setBankLogo(irMUsersBank.getBank().getBankLogo());

				bankViewInfo.setBankName(irMUsersBank.getBank().getBankName());

				bankViewInfo.setBankCode(irMUsersBank.getBank().getIRBankCode());
				
				bankViewInfo.setIsactive(irMUsersBank.getBank().getIsactive());

				bankViewInfo.setCreateddate(IRPlusUtil.convertTimestampToStringFormat(irMUsersBank.getBank().getCreateddate()));

				bankViewInfo.setModifieddate(IRPlusUtil.convertTimestampToStringFormat(irMUsersBank.getBank().getModifieddate()));

				bankContact = iContactMgmtDao.findPrimaryContact(irMUsersBank.getBank().getBankId());

				if(bankContact!=null)
				{
					bankViewInfo.setFirstname(bankContact.getFirstName());
					bankViewInfo.setLastName(bankContact.getLastName());
					bankViewInfo.setEmail(bankContact.getEmailId());
					bankViewInfo.setContactno(bankContact.getContactNo());
				}

				banks.add(bankViewInfo);

			}
			irPlusResponseDetails.setBankViewInfo(banks);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}
		catch(Exception e)
		{
			LOGGER.error("Exception in listBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails filterBanks(BankFilterInfo bankFilterInfo) throws BusinessException
	{
		LOGGER.info("Inside filterBanks : "+bankFilterInfo);
	
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		List<BankViewInfo>banks = new ArrayList<BankViewInfo> ();

		try
		{
	
			session = sessionFactory.getCurrentSession();

			Criteria criteria = session.createCriteria(IRSBankLicensing.class,"bankLicense");
			criteria.setProjection(Projections.groupProperty("bankLicense.bank.bankId"));

			criteria.createAlias("bankLicense.bank", "bank");
			criteria.createAlias("bank.irSBankBranch", "branch");
			criteria.createAlias("bank.irMUsersBank", "usersBank");
			criteria.createAlias("usersBank.irMUsers", "users");
			criteria.createAlias("users.site", "site");
			criteria.createAlias("bankLicense.fileType", "filetype");
			criteria.createAlias("branch.irSCustomers", "customers");
	
			if(bankFilterInfo.getFileTypeId()!=null&&!bankFilterInfo.getFileTypeId().isEmpty())
			{
				criteria.add(Restrictions.eq("filetype.filetypeid",new Long(bankFilterInfo.getFileTypeId())));

			}
			if(bankFilterInfo.getIsactive()!=null)
			{
				criteria.add(Restrictions.eq("bank.isactive",bankFilterInfo.getIsactive()));
			}
			if(bankFilterInfo.getBankName()!=null&&!bankFilterInfo.getBankName().isEmpty())
			{
				criteria.add(Restrictions.eq("bank.bankName",bankFilterInfo.getBankName()));
			}
			if(bankFilterInfo.getUserId()!=null)
			{
				criteria.add(Restrictions.eq("usersBank.irMUsers.userid",bankFilterInfo.getUserId()));
			}
			if(bankFilterInfo.getSiteId()!=null&&!bankFilterInfo.getSiteId().isEmpty())
			{
				criteria.add(Restrictions.eq("users.site.siteId",new Long(bankFilterInfo.getSiteId())));
			}
			if(bankFilterInfo.getCustomerName()!=null&&!bankFilterInfo.getCustomerName().isEmpty())
			{
				criteria.add(Restrictions.eq("customers.companyName",bankFilterInfo.getCustomerName()));
			}
			if(bankFilterInfo.getCustomerCode()!=null&&!bankFilterInfo.getCustomerCode().isEmpty())
			{
				criteria.add(Restrictions.eq("customers.bankCustomerCode",bankFilterInfo.getCustomerCode()));
			}

			List<IRSBankLicensing> banklist = criteria.list();
			
			if(banklist.size()>0){
				for(int j=0; j<banklist.size(); j++) {
					Object bankId = (Object) banklist.get(j);
			
			Query bankprocess= session.createQuery("from IRSBank as bank  where bank.bankId=:bankId");			

			bankprocess.setParameter("bankId",bankId);
		
			BankViewInfo bankViewInfo = null;
			IrSBankContact bankContact = null;
			List<IRSBank> rowval = bankprocess.list();
			if(rowval.size()>0){
			for(int i=0; i<rowval.size(); i++) {
		
				bankViewInfo = new BankViewInfo();
		
				bankViewInfo.setSiteId(rowval.get(i).getSiteId());

				bankViewInfo.setBankId(rowval.get(i).getBankId());

				bankViewInfo.setBankLogo(rowval.get(i).getBankLogo());

				bankViewInfo.setBankName(rowval.get(i).getBankName());

				bankViewInfo.setBankCode(rowval.get(i).getIRBankCode());
				
				bankViewInfo.setIsactive(rowval.get(i).getIsactive());

				bankViewInfo.setCreateddate(IRPlusUtil.convertTimestampToStringFormat(rowval.get(i).getCreateddate()));

				bankViewInfo.setModifieddate(IRPlusUtil.convertTimestampToStringFormat(rowval.get(i).getModifieddate()));

				bankContact = iContactMgmtDao.findPrimaryContact(rowval.get(i).getBankId());
				if(bankContact!=null)
				{
					bankViewInfo.setFirstname(bankContact.getFirstName());
					bankViewInfo.setEmail(bankContact.getEmailId());
					bankViewInfo.setContactno(bankContact.getContactNo());
				}

				banks.add(bankViewInfo);
				
		
			}
			}
				}
			}

			irPlusResponseDetails.setBankViewInfo(banks);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}
		catch(Exception e)
		{
			LOGGER.error("Exception in listBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}

	public IRPlusResponseDetails filterBranches(BankFilterInfo bankFilterInfo) throws BusinessException
	{
		LOGGER.info("Inside listBankBranches");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		List<LocationViewInfo>locations = new ArrayList<LocationViewInfo> ();

		try
		{
			session = sessionFactory.getCurrentSession();

			Criteria criteria = session.createCriteria(IrSBankBranch.class,"bankBranch");
			criteria.createAlias("bankBranch.bank", "bank");
			//criteria.add(Restrictions.eq("bank.bankId",bankFilterInfo.getBankId()));
		
			if(bankFilterInfo.getLocationId()!=null&&!bankFilterInfo.getLocationId().isEmpty())
			{
				criteria.add(Restrictions.like("bankBranch.locationId",bankFilterInfo.getLocationId(),MatchMode.ANYWHERE));
			}
			if(bankFilterInfo.getBranchLocation()!=null&&!bankFilterInfo.getBranchLocation().isEmpty())
			{
				criteria.add(Restrictions.like("bankBranch.branchLocation",bankFilterInfo.getBranchLocation(),MatchMode.ANYWHERE));
			}
			
			if(bankFilterInfo.getBranchStatus()!=null)
			{
				criteria.add(Restrictions.eq("bankBranch.isactive",bankFilterInfo.getIsactive()));
			}
			
	
			List<IrSBankBranch> branchList = (List)criteria.list();
			
			Iterator<IrSBankBranch> bankListIt = branchList.iterator();

			IrSBankBranch bankBranch = null;

			IrSBankContact bankContact = null;

			LocationViewInfo locationViewInfo = null;

			while (bankListIt.hasNext()) 
			{
				bankBranch = bankListIt.next();

				locationViewInfo = new LocationViewInfo();

				locationViewInfo.setSiteId(bankBranch.getBank().getSiteId());

				locationViewInfo.setBankId(bankBranch.getBank().getBankId());

				locationViewInfo.setBankLogo(bankBranch.getBank().getBankLogo());
				

				locationViewInfo.setBankName(bankBranch.getBank().getBankName());
				
				locationViewInfo.setBranchId(bankBranch.getBranchId());

				locationViewInfo.setBranchLocation(bankBranch.getBranchLocation());

				locationViewInfo.setBranchOwnCode(bankBranch.getBranchOwnCode());

				locationViewInfo.setLocationId(bankBranch.getLocationId());

				locationViewInfo.setCreateddate(IRPlusUtil.convertTimestampToStringFormat(bankBranch.getCreateddate()));

				locationViewInfo.setModifieddate(IRPlusUtil.convertTimestampToStringFormat(bankBranch.getModifieddate()));

				bankContact = iContactMgmtDao.findPrimaryContactByBranchId(bankBranch.getBranchId());

				if(bankContact!=null)
				{
					locationViewInfo.setFirstname(bankContact.getFirstName());
					locationViewInfo.setEmail(bankContact.getEmailId());
					locationViewInfo.setContactno(bankContact.getContactNo());
				}

				locationViewInfo.setCity(bankBranch.getCity());
				locationViewInfo.setState(bankBranch.getState());


				locations.add(locationViewInfo);

			}
			irPlusResponseDetails.setLocations(locations);
			irPlusResponseDetails.setRecordsTotal(locations.size());
			irPlusResponseDetails.setRecordsFiltered(locations.size());
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}
		catch(Exception e)
		{
			LOGGER.error("Exception in filterBranches ",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails updateBank(BankInfo bankInfo) throws BusinessException {

		LOGGER.info("Inside updateBank bankInfo :"+bankInfo);

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		IRSBank irsBank = null;

		try
		{
			session = sessionFactory.getCurrentSession();
			irsBank = (IRSBank) session.get(IRSBank.class, bankInfo.getBankId());

			if(irsBank!=null)
			{

				irsBank.setBankName(bankInfo.getBankName());
				irsBank.setBankLogo(bankInfo.getBankLogo());
				irsBank.setIRBankCode(bankInfo.getBankCode());
				irsBank.setIsactive(bankInfo.getBankStatus());
				irsBank.setRtNumber(bankInfo.getRtNo().trim());
				irsBank.setDdaNumber(bankInfo.getDdano());
				session.update(irsBank);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
			else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}				

		}
		catch(Exception e)
		{
			LOGGER.error("Exception in updateBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}
	
	
	@Override
	public IRPlusResponseDetails updateBankStatus(BankInfo bankInfo) throws BusinessException {

		LOGGER.info("Inside updateBank bankInfo :"+bankInfo);

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;
		IrMUsers user = new IrMUsers();
		IRSBank irsBank = null;
		IRSBankLicensing iRSBankLicensing =new IRSBankLicensing();
		ArrayList<IRSBankLicensing> bankLicenses = new ArrayList<IRSBankLicensing>();
		
		try
		{
			session = sessionFactory.getCurrentSession();

			irsBank = (IRSBank) session.get(IRSBank.class, bankInfo.getBankId());
			
			

			if(irsBank!=null)
			{
				irsBank.setIsactive(bankInfo.getIsactive());

				
				session.update(irsBank);
				
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
			else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}				

		}
		catch(Exception e)
		{
			LOGGER.error("Exception in updateBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}


	@Override
	public IRPlusResponseDetails getBankInfo(String bankId) throws BusinessException
	{
		LOGGER.info("Inside getBankInfo bankId :"+ bankId);

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		IRSBank irsBank = null;

		BankInfo bankInfo = null;

		try
		{
			session = sessionFactory.getCurrentSession();

			irsBank = (IRSBank) session.get(IRSBank.class, new Long(bankId));

			if(irsBank!=null)
			{
				bankInfo = new BankInfo();

				bankInfo.setBankId(irsBank.getBankId());

				bankInfo.setBankLogo(irsBank.getBankLogo());

				bankInfo.setBankName(irsBank.getBankName());
				bankInfo.setRtNo(irsBank.getRtNumber());
				bankInfo.setDdano(irsBank.getDdaNumber());
				bankInfo.setBankCode(irsBank.getIRBankCode());
				bankInfo.setBankStatus(irsBank.getIsactive());
				bankInfo.setSiteId(irsBank.getSiteId());

				irPlusResponseDetails.setBank(bankInfo);

				getBankLicenses(bankInfo);

				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
			else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}				

		}
		catch(Exception e)
		{
			LOGGER.error("Exception in updateBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails deleteBank(BankInfo bankInfo) throws BusinessException {

		LOGGER.info("Inside deleteBank bankId :");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		IRSBank irsBank = null;

		/*		 Need to add code to validate dependency of data on this bank details*/
		
		/*status code
		0-inactive
		1-active
		2-delete
		 */		

		try
		{
			session = sessionFactory.getCurrentSession();

			irsBank = (IRSBank) session.get(IRSBank.class, bankInfo.getBankId());

			if(irsBank!=null)
			{
				irsBank.setIsactive(bankInfo.getIsactive());
				session.update(irsBank);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
			else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}	

		}
		catch(Exception e)
		{
			LOGGER.error("Exception in deleteBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}

	public IRPlusResponseDetails addBankLicense(BankInfo bankInfo) throws BusinessException
	{
		LOGGER.info("Inside addBankLicense bankInfo :"+bankInfo);

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		IRSBank irsBank = null;

		IRSBankLicensing bankLicensing = null;

		IrMFileTypes fileType = null;

		IrMBusinessProcess businessProcess = null;
		

		try
		{

			deleteExistingLicenses(bankInfo.getBankId());

			session = sessionFactory.getCurrentSession();

			bankLicensing = new IRSBankLicensing();

			irsBank = (IRSBank) session.get(IRSBank.class, bankInfo.getBankId());

			ArrayList<IRSBankLicensing> bankLicenses = new ArrayList<IRSBankLicensing>();

			String ftypeList = bankInfo.getFtypelist();


			if(ftypeList!=null&&!ftypeList.isEmpty())
			{
				String[] ftype = ftypeList.split(",");

				for (int i = 0; i < ftype.length; i++) 
				{
					fileType = (IrMFileTypes) session.get(IrMFileTypes.class, new Long(ftype[i]));

					bankLicensing = new IRSBankLicensing();
					
					IrMUsers user = new IrMUsers();
					user.setUserid(bankInfo.getUserId());
					bankLicensing.setUser(user);

					bankLicensing.setFileType(fileType);

					bankLicensing.setBank(irsBank);

					bankLicenses.add(bankLicensing);

				}
			}

			/*String bpList = bankInfo.getBplist();

			if(bpList!=null&&!bpList.isEmpty())
			{
				String[] bpType = bpList.split(",");

				bankLicensing = null;

				for (int i = 0; i < bpType.length; i++) 
				{
					businessProcess = (IrMBusinessProcess) session.get(IrMBusinessProcess.class, new Long(bpType[i]));

					try
					{
						bankLicensing = bankLicenses.get(i); 
					}
					catch(ArrayIndexOutOfBoundsException abe)
					{
						bankLicensing = new IRSBankLicensing();

						bankLicenses.add(bankLicensing);
					}

					bankLicensing.setBusinessProcess(businessProcess);

					bankLicensing.setBank(irsBank);

					bankLicenses.add(bankLicensing);

				}

			}
*/
			for (Iterator iterator = bankLicenses.iterator(); iterator.hasNext();) {
				IRSBankLicensing irsBankLicensing = (IRSBankLicensing) iterator.next();

				session.save(irsBankLicensing);
			}

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);

		}
		catch(Exception e)
		{
			LOGGER.error("Exception in deleteBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}


	private void deleteExistingLicenses(long bankId) 
	{
		LOGGER.info("Inside deleteExistingLicenses bankId :"+bankId);

		Session session = null;

		try
		{
			session = sessionFactory.getCurrentSession();

			Query query = session.createQuery("delete from IRSBankLicensing bankLic where bankLic.bank.bankId=:bankId");

			query.setParameter("bankId", bankId);

			query.executeUpdate();
		}
		catch(Exception e)
		{
			LOGGER.error("Exception in deleteExistingLicenses",e);

			throw new HibernateException(e);
		}	

	}

	private void getBankLicenses(BankInfo bankInfo)
	{
		LOGGER.info("Inside getBankLicenses bankId :"+bankInfo.getBankId());

		Session session = null;

		List<IRSBankLicensing> licenseList = null;

		IRSBankLicensing bankLicense = null;

		try
		{
			session = sessionFactory.getCurrentSession();

			Query query = session.createQuery("from IRSBankLicensing bankLic where bankLic.bank.bankId=:bankId");

			query.setParameter("bankId", bankInfo.getBankId());

			licenseList = query.list();

			Iterator <IRSBankLicensing> licenseListIt = licenseList.iterator();

			String bpList="";

			String ftypeList="";

			while (licenseListIt.hasNext()) {
				bankLicense = licenseListIt.next();
				if(bankLicense.getBusinessProcess()!=null)
				{
					bpList +=bpList+bankLicense.getBusinessProcess().getBusinessProcessId()+",";
				}
				if(bankLicense.getFileType()!=null)
				{
					ftypeList +=ftypeList+bankLicense.getFileType().getFiletypeid()+",";
				}

			}
			if(!bpList.isEmpty())
			{
				bpList = bpList.substring(0, bpList.length()-1);
				bankInfo.setBplist(bpList);
			}

			if(!ftypeList.isEmpty())
			{
				ftypeList = ftypeList.substring(0, ftypeList.length()-1);
				bankInfo.setFtypelist(ftypeList);
			}

		}
		catch(Exception e)
		{
			LOGGER.error("Exception in deleteExistingLicenses",e);

			throw new HibernateException(e);
		}	
	}


	@Override
	public IRPlusResponseDetails updateBranchStatus(BankInfo bankInfo) throws BusinessException {

		LOGGER.info("Inside updateBank bankInfo :"+bankInfo);

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		IrSBankBranch bankBranch = null;

		try
		{
			session = sessionFactory.getCurrentSession();

			bankBranch = (IrSBankBranch) session.get(IrSBankBranch.class,bankInfo.getBranchId());

			if(bankBranch!=null)
			{
				bankBranch.setIsactive(bankInfo.getIsactive());
				session.update(bankBranch);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
			else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}				

		}
		catch(Exception e)
		{
			LOGGER.error("Exception in updateBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}
}