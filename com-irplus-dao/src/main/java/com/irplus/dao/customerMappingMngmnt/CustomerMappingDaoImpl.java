package com.irplus.dao.customerMappingMngmnt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.hibernate.entities.IRSBank;
import com.irplus.dao.hibernate.entities.IrMUsers;
import com.irplus.dao.hibernate.entities.IrMUsersBank;
import com.irplus.dao.hibernate.entities.IrSBankBranch;
import com.irplus.dao.hibernate.entities.IrSCustomermapping;
import com.irplus.dao.hibernate.entities.IrSCustomers;
import com.irplus.dao.hibernate.entities.IrSNatchaField;
import com.irplus.dao.hibernate.entities.IrsCustomerGrouping;
import com.irplus.dao.hibernate.entities.IrsCustomerGroupingDetails;
import com.irplus.dto.BankBranchInfo;
import com.irplus.dto.BankInfo;
import com.irplus.dto.BusinessProcessInfo;
import com.irplus.dto.CustomerGroupingDetails;
import com.irplus.dto.CustomerGroupingMngmntInfo;
import com.irplus.dto.CustomerMapSupportInfo;
import com.irplus.dto.CustomerMappingInfo;
import com.irplus.dto.Dashboard;
import com.irplus.dto.FileTypeInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.LocationViewInfo;
import com.irplus.dto.ParentBank;
import com.irplus.dto.StatusIdInfo;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;

@Transactional
@Component
public class CustomerMappingDaoImpl implements ICustomerMappingDao{

	private static final Logger log = Logger.getLogger(CustomerMappingDaoImpl.class);
	
	@Autowired
	SessionFactory sessionFactory;
	
	/*@Override
	public IRPlusResponseDetails createCustomerMapping(CustomerMapSupportInfo customerMapSupportInfo)
			throws BusinessException {
		
		CustomerMappingInfo customerMappingInfo[] = customerMapSupportInfo.getCustomerMappingInfo();
		
		List<String> errMsgs = new ArrayList<String>();		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();		
		Session session = sessionFactory.getCurrentSession();		
	
		try{	
		
			boolean nullFlag = true;

			for (int i = 0; i < customerMappingInfo.length; i++) {
				
				CustomerMappingInfo customerMappingInfo2 = customerMappingInfo[i];	
					
				if(customerMappingInfo2==null){
					nullFlag = false;
				}	
				
				if(customerMappingInfo2!=null){
					
				if (customerMappingInfo[i].getNachaFields()== null && customerMappingInfo[i].getReferenceData()==null&&
							customerMappingInfo[i].getBankbranchid()== null &&	customerMappingInfo[i].getCustomersId() ==null) {
					
						nullFlag = false;
					}
				}				
			}
				
			if(nullFlag){
				
			Duplicate remove	
				
				boolean dup = false;
			
						for (int i = 0; i < customerMappingInfo.length; i++) {
							
							CustomerMappingInfo one = customerMappingInfo[i];
							
							for (int j = i + 1 ; j < customerMappingInfo.length; j++) {
								CustomerMappingInfo two = customerMappingInfo[j];
								
								if (one.getNachaFields().equalsIgnoreCase(two.getReferenceData()) &&
									one.getReferenceData().equalsIgnoreCase(two.getReferenceData())&&
									(one.getBankbranchid()==two.getBankbranchid() )&&
									one.getCustomersId()==two.getCustomersId()	
									) {
									
									dup = true;
								
								}
							}
						}
				
					if(dup==false){
						
						boolean db_flag = false;
						
						Criteria criteria = session.createCriteria(IrSCustomermapping.class);
						
						List<IrSCustomermapping> lst = criteria .list();
												
						for (IrSCustomermapping Cstmrmpg : lst) {

							for (int i = 0; i < customerMappingInfo.length; i++) {
														
								if(Cstmrmpg.getNachaFields().equalsIgnoreCase(customerMappingInfo[i].getNachaFields())&&
										Cstmrmpg.getReferenceData().equalsIgnoreCase(customerMappingInfo[i].getReferenceData())&&
										(Cstmrmpg.getIrSBankbranch().getBranchId() == customerMappingInfo[i].getBankbranchid())&&
										(Cstmrmpg.getIrSCustomers().getCustomerId()==(customerMappingInfo[i].getCustomersId()))
										){
									
									errMsgs.add("Duplication Message Alert - This row already available");
									errMsgs.add("Nacha field :"+ customerMappingInfo[i].getNachaFields()+" Reference data :"
											+customerMappingInfo[i].getReferenceData() +" and This Bank and Customer already Mapped "
											);
									db_flag = true;
								}
							}
						}
						
						if(db_flag){
							
							errMsgs.add("Duplication Error : This row is alredy available");
							irPlusResponseDetails.setErrMsgs(errMsgs);
							irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
							irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
						}else{
							
							for (int i = 0; i < customerMappingInfo.length; i++) {
								
								CustomerMappingInfo one = customerMappingInfo[i];
								
								IrSCustomermapping irscusmap = new IrSCustomermapping();
															
								irscusmap.setIrMUsers((IrMUsers)session.get(IrMUsers.class, one.getUsersid()));
								irscusmap.setIrSCustomers((IrSCustomers)session.get(IrSCustomers.class, one.getCustomersId()));
								irscusmap.setIrSBankbranch((IrSBankBranch)session.get(IrSBankBranch.class ,one.getBankbranchid() ));
								irscusmap.setNachaFields(one.getNachaFields());
								irscusmap.setReferenceData(one.getReferenceData());
								irscusmap.setIsactive(1);
								
								session.save(irscusmap);
								session.flush();
							}
							
							errMsgs.add("Success : Saved data into DB ");
							irPlusResponseDetails.setErrMsgs(errMsgs);
							irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
							irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
							
						}
						
					}else{
						
						errMsgs.add("Duplication Error : Entered the duplicate row ");
						irPlusResponseDetails.setErrMsgs(errMsgs);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
						irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
					}	
						
			
			}else{
				
				errMsgs.add("Validation Error : Please Select All Fields");
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				
			}
			
		} catch (Exception e) {			
			
		log.error("Exception in create createCustomerGrp",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
		
		}	
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getCustomerMappingById(String custommerId) throws BusinessException {

		List<String> errMsgs = new ArrayList<String>();		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();		
		Session session = sessionFactory.getCurrentSession();		

		try {		
			
			Integer custmrId = Integer.parseInt(custommerId);
			
			Criteria criteria = session.createCriteria(IrSCustomermapping.class , "custmrmpng");	
			criteria.add(Restrictions.idEq(custmrId));
			
			IrSCustomermapping irSCustomermapping =(IrSCustomermapping) criteria.list().get(0);			
							
				CustomerMappingInfo custmpinfo = new CustomerMappingInfo();
								
				custmpinfo.setNachaFields(irSCustomermapping.getNachaFields());
				custmpinfo.setReferenceData(irSCustomermapping.getReferenceData());
				custmpinfo.setUsersid(irSCustomermapping.getIrMUsers().getUserid());
				custmpinfo.setBankbranchid(irSCustomermapping.getIrSBankbranch().getBranchId());
				custmpinfo.setBankName(irSCustomermapping.getIrSBankbranch().getBank().getBankName());
				custmpinfo.setCustomersId(irSCustomermapping.getIrSCustomers().getCustomerId());
				custmpinfo.setIsactive(irSCustomermapping.getIsactive());
			   
				irPlusResponseDetails.setCustomerMappingInfo(custmpinfo);			
				
				errMsgs.add(" Success ::Show  one Customermapping ");
				irPlusResponseDetails.setValidationSuccess(true);
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				
			}catch(Exception e){
			
			log.error("Exception in create createCustomerGrp",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}

	 mapping id do the validation controller

	@Override
	public IRPlusResponseDetails updateCustomerMapping(CustomerMappingInfo customerMappingInfo)
			throws BusinessException {
		
		List<String> errMsgs = new ArrayList<String>();		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();		
		Session session = sessionFactory.getCurrentSession();		
		
		CustomerMappingInfo custmpinfo = new CustomerMappingInfo();
		
		try{
			
			Criteria criteria = session.createCriteria(IrSCustomermapping.class , "custmrmpng");	
			
			List<IrSCustomermapping> custmrmapngList = criteria.list();		
							
			boolean duplicate=true;
			
			for (IrSCustomermapping irSCustomermapping2 : custmrmapngList){
				
				if(irSCustomermapping2.getCustomerMapId() != customerMappingInfo.getCustomerMapId()){
					
					if( ( customerMappingInfo.getBankbranchid() == irSCustomermapping2.getIrSBankbranch().getBranchId() )&&
					 (	customerMappingInfo.getCustomersId() == irSCustomermapping2.getIrSCustomers().getCustomerId() )&&
					 (	customerMappingInfo.getNachaFields().equalsIgnoreCase((irSCustomermapping2.getNachaFields()) )&&
					 (	customerMappingInfo.getReferenceData().equalsIgnoreCase(irSCustomermapping2.getReferenceData()) 
							 )))
					{					
						duplicate = false;
					}					
				}
			}
			IrSCustomermapping irSCustomermapping = null;	
			
			if( duplicate==true){
			
				criteria.add(Restrictions.idEq(customerMappingInfo.getCustomerMapId()));
				
				irSCustomermapping =(IrSCustomermapping)criteria.list().get(0);
				
				irSCustomermapping.setIrMUsers((IrMUsers)session.get(IrMUsers.class, customerMappingInfo.getUsersid()));
				irSCustomermapping.setIrSBankbranch((IrSBankBranch)session.get(IrSBankBranch.class, customerMappingInfo.getBankbranchid()));
				irSCustomermapping.setIrSCustomers((IrSCustomers)session.get(IrSCustomers.class, customerMappingInfo.getCustomersId()));
				irSCustomermapping.setNachaFields(customerMappingInfo.getNachaFields());
				irSCustomermapping.setReferenceData(customerMappingInfo.getReferenceData());
				irSCustomermapping.setIsactive(1);
				
				session.update(irSCustomermapping);
				
				errMsgs.add(" Success :: Updated Customermapping ");
				
				irPlusResponseDetails.setValidationSuccess(true);
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			
			}else{
				
				errMsgs.add(" Duplicate :: Already Available Please Select Different fields");											
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
			
		}catch (Exception e) {
		
		log.error("Exception in create createCustomerGrp",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}
	return irPlusResponseDetails;
	
	}

	@Override
	public IRPlusResponseDetails showAllCustomerMapping() throws BusinessException {
		
		List<String> errMsgs = new ArrayList<String>();		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();		
		Session session = sessionFactory.getCurrentSession();		
		
		List<CustomerMappingInfo> lst = new ArrayList<CustomerMappingInfo>();
		try {
		
			Criteria criteria = session.createCriteria(IrSCustomermapping.class , "custmrmpng");	
			
			List<IrSCustomermapping> custmrmapngList = criteria.list();		
			
			for (IrSCustomermapping irSCustomermapping : custmrmapngList) {
			
				CustomerMappingInfo cmpng = new CustomerMappingInfo();
				
				if (irSCustomermapping.getReferenceData() == null ) {
					
					errMsgs.add("Refferent data not available is null ");
					cmpng.setReferenceData(" ");
				
				}else{
					cmpng.setReferenceData(irSCustomermapping.getReferenceData());
				}
				cmpng.setBankName(irSCustomermapping.getIrSBankbranch().getBank().getBankName());
				cmpng.setBankbranchid(irSCustomermapping.getIrSBankbranch().getBranchId());
				cmpng.setNachaFields(irSCustomermapping.getNachaFields());
				cmpng.setReferenceData(irSCustomermapping.getReferenceData());
				cmpng.setCustomersId(irSCustomermapping.getIrSCustomers().getCustomerId());
				cmpng.setIsactive(irSCustomermapping.getIsactive());
				cmpng.setCustomerMapId(irSCustomermapping.getCustomerMapId());
				
				lst.add(cmpng);
				
			}
			irPlusResponseDetails.setCustomerMpngInfoList(lst);
			
			errMsgs.add(" Success :: Show all Customermapping ");
			irPlusResponseDetails.setValidationSuccess(true);
			irPlusResponseDetails.setErrMsgs(errMsgs);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);			
			
		} catch (Exception e) {
			log.error("Exception in create createCustomerGrp",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		
		return irPlusResponseDetails;
	}
	*/
	public IRPlusResponseDetails statusUpdateCustmrMpng(StatusIdInfo statusIdInfo) throws BusinessException{
	
		List<String> errMsgs = new ArrayList<String>();		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();		
		Session session = sessionFactory.getCurrentSession();		
		
		IrSCustomermapping custmrmpng = null;
		try {
			custmrmpng = (IrSCustomermapping) session.get(IrSCustomermapping.class, statusIdInfo.getId());
			
			if(custmrmpng!=null){
				
				custmrmpng.setIsactive(statusIdInfo.getStatus());
				session.update(custmrmpng);
				
				errMsgs.add(" Success ::  updated Customermapping "+ statusIdInfo.getId()+" Status as :"+statusIdInfo.getStatus() );
				irPlusResponseDetails.setValidationSuccess(true);
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);	
			}else{
				errMsgs.add(" Valoidation Error : This id not available In DB ");											
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
			
		} catch (Exception e) {
			log.error("Exception in create createCustomerGrp",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails CustomerBankingBanks(String userId) throws BusinessException {

		log.info("Inside customerMappingDaoImpl :showAllBanksMapping");
		Session session = null;
		
		session = sessionFactory.getCurrentSession();	
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();

		Dashboard dashboard = new Dashboard();

		List<String> errMsgs = new ArrayList<String>();
		
		ParentBank p = new ParentBank();
		Map<Long, String> mapBankId_Name = new HashMap<Long, String>();
		
		BankInfo bankDetails=null;

		try {
		int userid = Integer.parseInt(userId);
		Query banks = session.createQuery("from IrMUsersBank userBank where userBank.irMUsers.userid=? and userBank.isactive=?");
		banks.setParameter(0,userid);
		banks.setParameter(1,1);
		List<IrMUsersBank> userBankList=banks.list();
		
	if(userBankList.size()>0){
		for(IrMUsersBank irMUsersBank:userBankList){
			    bankDetails = new BankInfo();
			    bankDetails.setBankLogo(irMUsersBank.getBank().getBankLogo());
				bankDetails.setBankName(irMUsersBank.getBank().getBankName());
				bankDetails.setBankId(irMUsersBank.getBank().getBankId());
			

		List <BankBranchInfo> branchbanklist = new ArrayList<BankBranchInfo>();
		
Query branch = session.createQuery("from IrSBankBranch branch where branch.isactive=? and branch.bank.bankId=?");
branch.setParameter(0,1);
BankBranchInfo branchDetails=null;
branch.setParameter(1,irMUsersBank.getBank().getBankId());
List<IrSBankBranch> branchList =branch.list();			
if(branchList.size()>0){
	for(int j=0;j<branchList.size();j++){
		
		branchDetails = new BankBranchInfo();

branchDetails.setBranchLocation(branchList.get(j).getBranchLocation());
branchDetails.setBranchId(branchList.get(j).getBranchId());
branchbanklist.add(branchDetails);

	}	

}

bankDetails.setBankbranchinfo(branchbanklist);
				
				dashboard.getBankInfo().add(bankDetails);
				
			}
			
		}
	else
	{
		
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		
	}
	
	
		irPlusResponseDetails.setDashboardResponse(dashboard);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
	
		
		}
		catch (Exception e) {
			log.error("Exception raised in show list customers",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			throw new HibernateException(e);
		}
			return irPlusResponseDetails;
		}

@Override

	public IRPlusResponseDetails showBranchFile(String branchId) throws BusinessException {
	
	log.info("Inside CUSTOMERMAPPING");

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	Session session = null;
	
	session = sessionFactory.getCurrentSession();

	List<LocationViewInfo>locations = new ArrayList<LocationViewInfo> ();
	
	BusinessProcessInfo processInfo=null;
	FileTypeInfo fileTypeInfo=null;
	List<BusinessProcessInfo> processList = new ArrayList<BusinessProcessInfo>();
	List<FileTypeInfo> fileTypes = new ArrayList<FileTypeInfo>();


	
	
	try
	{
		session = sessionFactory.getCurrentSession();
		
		/*isActive Status Code
		
		0-inactive
		1-active
		2-deleted*/	
		

		
		
		String hql = "from IrSBankBranch branch where branch.bank.bankId=? and branch.isactive=?";
		
		Query query = session.createQuery(hql);

		query.setParameter(0,new Long(branchId));
		query.setParameter(1,1);
		

	//List<IrSBankBranch> bankList = (List<IRSBank>)query.list();
		List<IrSBankBranch> branchList = query.list();

		Iterator<IrSBankBranch> bankListIt = branchList.iterator();

	

		LocationViewInfo locationViewInfo = null;

		for(IrSBankBranch irSBankBranch:branchList){

			locationViewInfo = new LocationViewInfo();

			locationViewInfo.setBranchId(irSBankBranch.getBranchId());

			locationViewInfo.setBranchLocation(irSBankBranch.getBranchLocation());
		
		}
		locations.add(locationViewInfo);
		
		/*if(bankId!=null){
			
			Query bp = session.createQuery("FROM IRSBankLicensing bl Where bl.bank.bankId=?");
			bp.setParameter(0,new Long(bankId));
			List<IRSBankLicensing> bplist = bp.list();
			if(bplist.size()>0){
				for(IRSBankLicensing iRSBankLicensing:bplist){
					
					processInfo = new BusinessProcessInfo();

					processInfo.setBusinessProcessId(iRSBankLicensing.getBusinessProcess().getBusinessProcessId());

					processInfo.setProcessName(iRSBankLicensing.getBusinessProcess().getProcessName());

					processList.add(processInfo);
					
					locationViewInfo.setBankBusinessProcess(processList);
					
					fileTypeInfo = new FileTypeInfo();

					fileTypeInfo.setFiletypeid(iRSBankLicensing.getFileType().getFiletypeid());

					fileTypeInfo.setFiletypename(iRSBankLicensing.getFileType().getFiletypename());

					fileTypes.add(fileTypeInfo);
					
					locationViewInfo.setBankFileTypeInfo(fileTypes);
					
					
				}
				locations.add(locationViewInfo);
			}
			
			
			
		}
*/		
		
		
		
		
		
		
		irPlusResponseDetails.setLocations(locations);
		irPlusResponseDetails.setRecordsTotal(locations.size());
		irPlusResponseDetails.setRecordsFiltered(locations.size());
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
	}
	catch(Exception e)
	{
		log.error("Exception in listBank",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

		throw new HibernateException(e);
	}

	return irPlusResponseDetails;
}




@Override
public IRPlusResponseDetails showBranchCustomer(String branchId) throws BusinessException {



IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

Session session = null;

session = sessionFactory.getCurrentSession();

LocationViewInfo locations = new LocationViewInfo();

BusinessProcessInfo processInfo=null;
FileTypeInfo fileTypeInfo=null;
List<BusinessProcessInfo> processList = new ArrayList<BusinessProcessInfo>();
List<FileTypeInfo> fileTypes = new ArrayList<FileTypeInfo>();

List<CustomerGroupingMngmntInfo> branchLists = new ArrayList<CustomerGroupingMngmntInfo>();



try
{
session = sessionFactory.getCurrentSession();

/*isActive Status Code

0-inactive
1-active
2-deleted*/	

String hql = "from IRSBank bank where bank.bankId=?";
Query branch = session.createQuery(hql);
branch.setParameter(0,new Long(branchId));

List<IRSBank> branchList =branch.list();

if(branchList.size()>0){
	for(int i=0;i<branchList.size();i++){
	
	
	

String cust = "from IrSCustomers customer where customer.irSBankBranch.bank.bankId=? and customer.isactive=?";

Query query = session.createQuery(cust);

query.setParameter(0,branchList.get(i).getBankId());
query.setParameter(1,1);


//List<IrSBankBranch> bankList = (List<IRSBank>query.list();
List<IrSCustomers> queryList = query.list();
/*
Iterator<IrSCustomers> bankListIt = queryList.iterator();





for(IrSCustomers irScustomer:queryList){*/
	if(queryList.size()>0){
		for(int j=0;j<queryList.size();j++){
			CustomerGroupingMngmntInfo customer = null;
		
	customer = new CustomerGroupingMngmntInfo();

	customer.setCustomerid(queryList.get(j).getCustomerId());

	customer.setCompanyName(queryList.get(j).getCompanyName());

branchLists.add(customer);


locations.setBankCustomerList(branchLists);

irPlusResponseDetails.setBankBranches(locations);
	}
	}
	else
	{
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		
	}
}
}
else
{
	irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
	irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
	
}
irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
}
catch(Exception e)
{
log.error("Exception in listBank",e);
irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

throw new HibernateException(e);
}

return irPlusResponseDetails;
}







@Override
public IRPlusResponseDetails createCustomerMappingFields(CustomerMappingInfo[] customerMappingInfo) throws BusinessException {
	

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	Session session = null;
	
	session = sessionFactory.getCurrentSession();
	IrMUsers user = new IrMUsers();
	IrSBankBranch branch = new IrSBankBranch();
	IrSCustomers customer = new IrSCustomers();

	IrSCustomermapping addfielddetails=null;
	
	
	IrSNatchaField natchafield = new IrSNatchaField();
	try
	{
		if(customerMappingInfo.length>0)
		{
			log.debug("lengthhhhhhhhhhhhhh"+customerMappingInfo.length);
		for(int i=0;i<customerMappingInfo.length;i++)
		{
			
			addfielddetails =new IrSCustomermapping();
		
			addfielddetails.setReferenceData(customerMappingInfo[i].getReferenceData());
			addfielddetails.setIsactive(customerMappingInfo[i].getIsactive());
			//addfielddetails(customerMappingInfo[i].getNachaFields());
			
			natchafield.setNatchafieldId(customerMappingInfo[i].getNatchafld());			
			addfielddetails.setIrsNatchaField(natchafield);
			
			
			log.debug("aaaaaa"+customerMappingInfo[i].getReferenceData());
			log.debug("bbbbb"+customerMappingInfo[i].getNatchafld());
			
/*			
			Query query = session.createQuery("from IrSNatchaField field where field.natchafieldId=?");
		
			//BankBranchInfo branchDetails=null;
			query.setParameter(0,customerMappingInfo[i].getNachaFields());
			
		
			
			List<IrSNatchaField> groupdetlist =query.list();		
		
			log.debug("groupdetlisterewrere"+groupdetlist.size());
			if(groupdetlist.size()>0){
				
				for(int j=0;j<groupdetlist.size();j++) {
					//branchDetails = new BankBranchInfo();

				addfielddetails.setNachaFields(groupdetlist.get(j).getNatchafieldName());
			
				log.debug("nameeeeeeeeee"+groupdetlist.get(j).getNatchafieldName());

				
			}
			
			
		}*/
			branch.setBranchId(customerMappingInfo[i].getBankbranchid());			
			addfielddetails.setIrSBankbranch(branch);
			
			customer.setCustomerId(customerMappingInfo[i].getCustomersId());			
			addfielddetails.setIrSCustomers(customer);
			
			
			user.setUserid(customerMappingInfo[i].getUsersid());			
			addfielddetails.setIrMUsers(user);
			session.save(addfielddetails);
			session.flush();
			log.debug("Added custGrpingDetais Info :"+addfielddetails);
		
		
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}
		}
	}catch(Exception e)
	{
		log.error("Exception in addContact",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}

	
	return irPlusResponseDetails;
}




@Override
public IRPlusResponseDetails showAllCustomerMappedFields() throws BusinessException {
	
	log.info("Inside customerDaoImpl");
	log.debug("Show all customers instances");
	
	IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();

	Session session = null;
	
	session = sessionFactory.getCurrentSession();
	List<String> errMsgs = new ArrayList<String>();
	
	IrSNatchaField natchafld = new IrSNatchaField();
	CustomerMappingInfo FieldDetails=null;
	 List<CustomerMappingInfo> customerMpngInfoList=new ArrayList<CustomerMappingInfo>();

	try {
		
		Query customerFields = session.createQuery("from IrSCustomermapping");
	
		List<IrSCustomermapping> custFieldsList =customerFields.list();

	if(custFieldsList.size()>0){
			for(int i=0;i<custFieldsList.size();i++){
				
				
				FieldDetails = new CustomerMappingInfo();
				FieldDetails.setCustomersId(custFieldsList.get(i).getIrSCustomers().getCustomerId());
				FieldDetails.setBankName(custFieldsList.get(i).getIrSBankbranch().getBank().getBankName());
				FieldDetails.setNatchafld(custFieldsList.get(i).getIrsNatchaField().getNatchafieldId());
				
				
			/*	customer.setCustomerId(natchafld.get);			
				FieldDetails.setNatchafld(customer);*/
				FieldDetails.setReferenceData(custFieldsList.get(i).getReferenceData());
				FieldDetails.setCustomerMapId(custFieldsList.get(i).getCustomerMapId());
				
				log.debug("CustomersIDDDDD"+custFieldsList.get(i).getIrSCustomers().getCustomerId());
				log.debug("Banknameeeeee"+custFieldsList.get(i).getIrSBankbranch().getBank().getBankName());
				log.debug("natcafieddd"+custFieldsList.get(i).getIrsNatchaField().getNatchafieldId());
				log.debug("refedataaa"+custFieldsList.get(i).getReferenceData());
				customerMpngInfoList.add(FieldDetails);

			}

			
			irPlusResponseDetails.setCustomerMpngInfoList(customerMpngInfoList);
			irPlusResponseDetails.setErrMsgs(errMsgs);
			//irPlusResponseDetails.setRecordsTotal(listCustomBean.size());
			//irPlusResponseDetails.setRecordsFiltered(listCustomBean.size());
			irPlusResponseDetails.setValidationSuccess(true);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			
		} else {
	
			log.debug("Customers not available :: Empty Data :: Data required to show roles");
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			irPlusResponseDetails.setErrMsgs(errMsgs);
		}
		
	} catch (Exception e) {
		log.error("Exception raised in show list customers",e);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		
		throw new HibernateException(e);
	}
	
	return irPlusResponseDetails;
	}
@Override
public IRPlusResponseDetails createCustomerMapping(CustomerMapSupportInfo customerMapSupportInfo)
		throws BusinessException {
	// TODO Auto-generated method stub
	return null;
}
@Override
public IRPlusResponseDetails getCustomerMappingById(String custommerId) throws BusinessException {
	// TODO Auto-generated method stub
	return null;
}
@Override
public IRPlusResponseDetails updateCustomerMapping(CustomerMappingInfo customerMappingInfo) throws BusinessException {
	// TODO Auto-generated method stub
	return null;
}
@Override
public IRPlusResponseDetails showAllCustomerMapping() throws BusinessException {
	// TODO Auto-generated method stub
	return null;
}	


}