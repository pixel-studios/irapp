package com.irplus.dao.customer_licencing;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.hibernate.entities.IrMBusinessProcess;
import com.irplus.dao.hibernate.entities.IrMUsers;
import com.irplus.dao.hibernate.entities.IrSBankBranch;
import com.irplus.dao.hibernate.entities.IrSCustomerlicensing;
import com.irplus.dao.hibernate.entities.IrSCustomers;
import com.irplus.dto.CustomerLicenceInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;

@Component

@Transactional

public class CustomerLicenceDaoImpl implements ICustomerLicencingDao{

	private static final Logger log= Logger.getLogger(CustomerLicenceDaoImpl.class);
	
	@Autowired
	SessionFactory sessionFactory;
	
	public Session getMyCurrentSession() {		
		Session session =null;
		
		try {	
		session=sessionFactory.getCurrentSession();
		}catch (HibernateException e) {
		log.error("Exception raised DaoImpl :: At current session creation time ::"+e);
		throw new HibernateException(e);
		}		
		return session; 
	}
	
	@Override
	public IRPlusResponseDetails createCustomerLicence(CustomerLicenceInfo customerLicenceInfo)
			throws BusinessException {
		
		log.info("Inside CustomerDaoImpl"+customerLicenceInfo);
			
			IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
			IrSCustomerlicensing irSCustomerlicensing = null;
			try {
	
				irSCustomerlicensing = new IrSCustomerlicensing();
				
				irSCustomerlicensing.setIsactive(customerLicenceInfo.getIsactive());
				irSCustomerlicensing.setFiletypeid(customerLicenceInfo.getFiletypeid());
				irSCustomerlicensing.setIrMBusinessprocess((IrMBusinessProcess)getMyCurrentSession().get(IrMBusinessProcess.class,customerLicenceInfo.getBusinessprocessId()));
		//		irSCustomerlicensing.setCustomerLicenseId(customerLicenceInfo.getCustomerLicenseId());
				irSCustomerlicensing.setIrSCustomers((IrSCustomers)getMyCurrentSession().get(IrSCustomers.class,customerLicenceInfo.getCustomersId()));
				irSCustomerlicensing.setIrMUsers((IrMUsers)getMyCurrentSession().get(IrMUsers.class,customerLicenceInfo.getUserId()));
				irSCustomerlicensing.setIrSBankbranch((IrSBankBranch)getMyCurrentSession().get(IrSBankBranch.class,customerLicenceInfo.getBankbranchId()));			
									
				getMyCurrentSession().save(irSCustomerlicensing);
				
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);				
			}
			catch (Exception e) {
				log.error("Exception in create CustomerLicence",e);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				throw new HibernateException(e);
			}			
			return irPlusResponseDetails;
	}


	@Override
	public IRPlusResponseDetails getCustomerLicenceById(String customerLicenceId) throws BusinessException {

		log.info("Inside CustomerDaoImpl");
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		IrSCustomerlicensing irSCustomerlicensing = null;
		
		CustomerLicenceInfo customerLicenceInfo = new CustomerLicenceInfo();
		
		List<CustomerLicenceInfo> listCustomBean = new ArrayList<CustomerLicenceInfo>();
		
		try {
			
			irSCustomerlicensing =(IrSCustomerlicensing) getMyCurrentSession().get(IrSCustomerlicensing.class, Integer.parseInt(customerLicenceId));
			
			if (irSCustomerlicensing!=null) {				 				
				customerLicenceInfo.setBankbranchId(irSCustomerlicensing.getIrSBankbranch().getBranchId());
				customerLicenceInfo.setBusinessprocessId(irSCustomerlicensing.getIrMBusinessprocess().getBusinessProcessId());
				customerLicenceInfo.setCustomerLicenseId(irSCustomerlicensing.getCustomerLicenseId());
				customerLicenceInfo.setCustomersId(irSCustomerlicensing.getIrSCustomers().getCustomerId());
				customerLicenceInfo.setFiletypeid(irSCustomerlicensing.getFiletypeid());
				customerLicenceInfo.setIsactive(irSCustomerlicensing.getIsactive());
				customerLicenceInfo.setUserId(irSCustomerlicensing.getIrMUsers().getUserid());
								
				listCustomBean.add(customerLicenceInfo);
				
				irPlusResponseDetails.setCustomerLicenceInfo(listCustomBean);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			} else {
				
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);				
			}
			
		} catch (Exception e) {

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception Raised inside customer DaoImple :: "+e);
			throw new HibernateException(e);
		}
				
		return irPlusResponseDetails;
	}


	@Override
	public IRPlusResponseDetails updateCustomerLicence(CustomerLicenceInfo customerLicenceInfo)
			throws BusinessException {

		log.info("Inside RoleDaoImpl");
		log.debug("Update Role instance");
					
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		IrSCustomerlicensing irSCustomerlicensing = null;
		try{	
			irSCustomerlicensing =(IrSCustomerlicensing) getMyCurrentSession().get(IrSCustomerlicensing.class, customerLicenceInfo.getCustomerLicenseId());
					
			irSCustomerlicensing.setFiletypeid(customerLicenceInfo.getFiletypeid());
			irSCustomerlicensing.setIrMBusinessprocess((IrMBusinessProcess)getMyCurrentSession().get(IrMBusinessProcess.class,customerLicenceInfo.getBusinessprocessId()));
			irSCustomerlicensing.setIrMUsers((IrMUsers)getMyCurrentSession().get(IrMUsers.class,customerLicenceInfo.getUserId()));
			irSCustomerlicensing.setIrSBankbranch((IrSBankBranch)getMyCurrentSession().get(IrSBankBranch.class,customerLicenceInfo.getBankbranchId()));
			irSCustomerlicensing.setIrSCustomers((IrSCustomers)getMyCurrentSession().get(IrSCustomers.class,customerLicenceInfo.getCustomersId()));
			irSCustomerlicensing.setIsactive(customerLicenceInfo.getIsactive());
			irSCustomerlicensing.setModifieddate(customerLicenceInfo.getModifieddate());
			irSCustomerlicensing.setCreateddate(customerLicenceInfo.getCreateddate());
			irSCustomerlicensing.setCustomerLicenseId(customerLicenceInfo.getCustomerLicenseId());
			
			getMyCurrentSession().saveOrUpdate(irSCustomerlicensing);
			
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			irPlusResponseDetails.setValidationSuccess(true);
		
		}catch (Exception e) {

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception Raised inside CustomerLicenceInfoDaoImpl :: updateCustomerId : "+e);
		
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails deleteCustomerLicenceById(String customerLicenceId) throws BusinessException {

		log.info("Inside customerLicenceDaoImpl");
		log.debug("deleting customerLicence instance");
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		try{	
			Query query = getMyCurrentSession().createQuery("delete IrSCustomerlicensing d where d.customerLicenseId =:role_Id");
			query.setParameter("role_Id", Integer.parseInt(customerLicenceId));
			
			int result=query.executeUpdate();
			if(result==0){
				
				log.debug("delete customerLicence failed to delete");
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				
			}else {
				log.debug("succcessfully deleted customerLicence :: customerId"+customerLicenceId);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
					
		}catch (Exception e) {
			log.error("Exception raised in delete customerLicence",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails showAllCustomerLicences() throws BusinessException {

		log.info("Inside customerLicenceDaoImpl");
		log.debug("Show all customerLicence instances");
		
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
		List<CustomerLicenceInfo> listCustomerLicenceInfo = new ArrayList<CustomerLicenceInfo>();
		try {
			
			Criteria criteria = getMyCurrentSession().createCriteria(IrSCustomerlicensing.class);
			
			List<IrSCustomerlicensing> list = criteria.list();
			
			if (!list.isEmpty()) {
							
				for (IrSCustomerlicensing irSCustomerlicensing : list) {
			
					CustomerLicenceInfo customerLicenceInfo = new CustomerLicenceInfo();
					
					customerLicenceInfo.setBankbranchId(irSCustomerlicensing.getIrSBankbranch().getBranchId());
					customerLicenceInfo.setBusinessprocessId(irSCustomerlicensing.getIrMBusinessprocess().getBusinessProcessId());
					customerLicenceInfo.setCustomerLicenseId(irSCustomerlicensing.getCustomerLicenseId());
					customerLicenceInfo.setCustomersId(irSCustomerlicensing.getIrSCustomers().getCustomerId());
					customerLicenceInfo.setFiletypeid(irSCustomerlicensing.getFiletypeid());
					customerLicenceInfo.setIsactive(irSCustomerlicensing.getIsactive());
					customerLicenceInfo.setUserId(irSCustomerlicensing.getIrMUsers().getUserid());
					customerLicenceInfo.setCreateddate(irSCustomerlicensing.getCreateddate());
					customerLicenceInfo.setModifieddate(irSCustomerlicensing.getModifieddate());
										
					listCustomerLicenceInfo.add(customerLicenceInfo);
				}
				irPlusResponseDetails.setCustomerLicenceInfo(listCustomerLicenceInfo);
			
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				
			} else {
				
				log.debug("CustomerLicense not available :: Empty Data :: Data required to show roles");
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);				
			}
			
		} catch (Exception e) {
			log.error("Exception raised in show list CustomerLicense",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			throw new HibernateException(e);
		}		
		return irPlusResponseDetails;
		}	
}