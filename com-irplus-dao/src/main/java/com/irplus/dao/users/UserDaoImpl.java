package com.irplus.dao.users;


import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;

import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.irplus.dao.hibernate.entities.IRSBank;
import com.irplus.dao.hibernate.entities.IRSSite;
import com.irplus.dao.hibernate.entities.IrMMenus;
import com.irplus.dao.hibernate.entities.IrMModulemenus;
import com.irplus.dao.hibernate.entities.IrMRoles;
import com.irplus.dao.hibernate.entities.IrMUsers;
import com.irplus.dao.hibernate.entities.IrMUsersBank;
import com.irplus.dao.hibernate.entities.IrSBankBranch;
import com.irplus.dao.roles.IRolesDao;
import com.irplus.dto.BankInfo;

import com.irplus.dto.FileTypeInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.UserBankBean;
import com.irplus.dto.UserFilterInfo;

import com.irplus.dto.menu.MenuInfo;
import com.irplus.dto.module.ModuleBean;
import com.irplus.dto.roles.RoleBean;
import com.irplus.dto.users.UserBean;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;


@Component
@Transactional
public class UserDaoImpl implements IUserDao{

	private static final Log log = LogFactory.getLog(UserDaoImpl.class);

	@Autowired 
	private SessionFactory sessionFactory;

	    public  int AES_KEY_SIZE =128 ;
	    public  int IV_SIZE = 96 ;
	    public  int TAG_BIT_LENGTH = 128 ;
	    public  String ALGO_TRANSFORMATION_STRING = "AES/GCM/PKCS5Padding" ;
	    private String key = "pvncwl-pxl*@asm!";
	    private String initVector = "com*pxl-studio@a";
	

	@Override
	public IRPlusResponseDetails createUser(UserBean userBean) throws BusinessException{

		log.debug("Inside of userDaoImpl :: createUser()");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		IrMUsers irMUsers = new IrMUsers();
		
		IRSSite site = new IRSSite();
		IrMRoles irsRoles = new IrMRoles();

		try {
			
			Session session = null;
			session = sessionFactory.getCurrentSession();
					
			log.debug("Inside of userDaoImpl :: createUser() :: Inside try block");
			irMUsers.setUsername(encrypt(userBean.getUsername()));
			irMUsers.setUserCode(userBean.getUserCode());
			irMUsers.setFirstName(userBean.getFirstName());
			irMUsers.setLastName(userBean.getLastName());

			irMUsers.setPassword(generateHash(userBean.getPassword()));
			irMUsers.setInternalUserCode(userBean.getInternalUserCode());
			irsRoles.setRoleid(userBean.getRoleId());
			irMUsers.setIrMroles(irsRoles);
			irMUsers.setAdminId(userBean.getAdminId());

			irMUsers.setEmailAddress(userBean.getEmailAddress());
			irMUsers.setContactno(encrypt(userBean.getContactno()));
			irMUsers.setUserPhoto(userBean.getUserPhoto());			
			irMUsers.setIsactive(userBean.getIsactive());

			site.setSiteId(new Long(userBean.getSiteId()));
			irMUsers.setSite(site);
			//Active or Inactive setting
			

			Integer userId = (Integer) session.save(irMUsers);
			
			userBean.getUserBank()[0].setUserId(userId);
			
			//session.flush();
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		} catch (Exception e) {

			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);

			log.error("Exception Raised Insideof userDaoImpl :: createUser()"+e);
			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}


	/*
	 *  
	 * 1 user completed
	 * 2 Defoutl response send 
	 * 3 .update 
	 *  Here creating user 
	 * Here creating user and userBank data
	 *
	 **/

	public IRPlusResponseDetails createUserNoDup(UserBean userBean) throws BusinessException{
		
		log.debug("Inside of userDaoImpl :: createUserNoDup()");

		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		List<String> errMsg =  new ArrayList<String>();
		IrMUsers userInfo=null;
		IrMRoles irsRoles= new IrMRoles();
		IrMUsers irMUsers = new IrMUsers();
		IrMUsers user = new IrMUsers();
		IrMUsers user1 = new IrMUsers();
		IRSSite site = new IRSSite();
		UserBankBean[] userBank=null;
		ArrayList<UserBankBean> arr_bankInfo = null;
		IrMUsersBank irUserBank=new IrMUsersBank();
		IrSBankBranch branch = new IrSBankBranch();

		try {
			
			Query userQuery = sessionFactory.getCurrentSession().createQuery("from IrMUsers user where user.username=?");
			userQuery.setParameter(0,userBean.getUsername());
			
			userInfo =(IrMUsers) userQuery.uniqueResult();
			
			if(userInfo==null){
				
				irMUsers.setUsername(userBean.getUsername());
				irMUsers.setUserCode(userBean.getUserCode());
				irMUsers.setFirstName(userBean.getFirstName());
				irMUsers.setLastName(userBean.getLastName());

				irMUsers.setPassword(userBean.getPassword());
				irMUsers.setInternalUserCode(userBean.getInternalUserCode());
				irsRoles.setRoleid(userBean.getRoleId());
			
				irMUsers.setIrMroles(irsRoles);
				irMUsers.setAdminId(userBean.getAdminId());

				irMUsers.setEmailAddress(userBean.getEmailAddress());
				irMUsers.setContactno(userBean.getContactno());
				irMUsers.setUserPhoto(userBean.getUserPhoto());			
				site.setSiteId(new Long(userBean.getSiteId()));
				irMUsers.setSite(site);
				irMUsers.setIsactive(userBean.getIsactive());

				//	getMyCurrentSession().save(irMUsers);

				int userId=(int)sessionFactory.getCurrentSession().save(irMUsers);
				
				userBank=userBean.getUserBank();
				
				
				usersResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				usersResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				usersResponseDetails.setErrMsgs(errMsg);
				usersResponseDetails.setValidationSuccess(true);
				
			}else{
				errMsg.add("Validation Error :: Please select Bank Branch or Your not selected Bank Branch ");
				usersResponseDetails.setErrMsgs(errMsg);
				usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				usersResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
			}
			
			
		} catch (RuntimeException e) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception Raised Insideof userDaoImpl :: createUser()" , e);

			throw new HibernateException(e);
		}
		
		
		return usersResponseDetails;
	}


	/*	2 Defoult response send roles and banks 
	 * 
	 * */

	public IRPlusResponseDetails getAll_Role_Banks(String siteId) throws BusinessException{

		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		log.debug("inside of getAll_Role_Banks()");

		List<String> errMsg = new ArrayList<String>();

		ArrayList<RoleBean> roleArrayList = new ArrayList<>();

		ArrayList<BankInfo> bankList = new ArrayList<>();

		UserBean userBean = new UserBean();
		Session session = null;
		session = sessionFactory.getCurrentSession();
		
		try {

			Query roles = session.createQuery("from IrMRoles irMRoles where irMRoles.isrestricted=? and irMRoles.isactive=?");
			roles.setParameter(0,1);
		
			roles.setParameter(1,1);

			List<IrMRoles> listRoles = roles.list();

			for (IrMRoles irMRoles : listRoles){

				RoleBean r = new RoleBean();
				
				if(irMRoles .getIsactive() !=0){

					r.setRoleid(irMRoles.getRoleid());
					r.setRolename(irMRoles.getRolename());
					r.setStatusId(irMRoles.getIsactive());

					r.setBankVisibility(irMRoles.getBankVisibility());
					r.setIsrestricted(irMRoles.getIsrestricted());
					r.setUserId(irMRoles.getIrMUsers().getUserid());

					roleArrayList.add(r);

				}
			}

			userBean.setRoleArrayList(roleArrayList);
			errMsg.add("Success : Role list added :" +roleArrayList.size());
			
			
			// for getting list of default bank and branch from bankbranch table

			/*Criteria criteria1 = sessionFactory.getCurrentSession().createCriteria(IrSBankBranch.class);

			List<IrSBankBranch> listBbranch = criteria1.list();

			for (IrSBankBranch irSBankBranch : listBbranch) {

				BankInfo b = new BankInfo();

				b.setBankId(irSBankBranch.getBank().getBankId());
				b.setBankName(irSBankBranch.getBank().getBankName());
				b.setBranchId(irSBankBranch.getBranchId());
				b.setBranchLocation(irSBankBranch.getBranchLocation());
				b.setBranchOwnCode(irSBankBranch.getBranchOwnCode());
				b.setIsdefault(irSBankBranch.getIsdefault());

				bankList.add(b);
			}

			userBean.setBankList(bankList);

			*/
			
						
			
			
			
			
		//	Criteria banks = sessionFactory.getCurrentSession().createCriteria(IRSBank.class);

			Query banks = session.createQuery("from IRSBank bank where bank.isactive=? and bank.siteId=?");
			banks.setParameter(0,1);
			banks.setParameter(1,siteId);
			List<IRSBank> banksList = banks.list();

			for (IRSBank irSBank:banksList) {

				BankInfo b = new BankInfo();
				b.setBankId(irSBank.getBankId());
				b.setBankName(irSBank.getBankName());
				bankList.add(b);
			}

			userBean.setBankList(bankList);

			
			if(!bankList.isEmpty() && ! roleArrayList.isEmpty() ){

				usersResponseDetails.setUserBean(userBean);

				errMsg.add("success :: success fully added userbean : UserProfile rendering obeject , it contains role names ,bank/branch defoult values ");
				usersResponseDetails.setErrMsgs(errMsg);		
				usersResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				usersResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				usersResponseDetails.setValidationSuccess(true);

			}else{
				errMsg.add("Error :: Not added userbean : UserProfile rendering obeject , it not contains role names ,bank/branch defoult values not available ");
				usersResponseDetails.setErrMsgs(errMsg);				
				usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				usersResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);

			}
		} catch (RuntimeException re) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);

			log.error("findAllUsers failed", re);
			throw new HibernateException(re);
		}				
		return usersResponseDetails;
	}

	/*	 3.update user and UserBank only    */

	public IRPlusResponseDetails updateUserAndBank2(UserBean userBeanInfo) throws BusinessException {

		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		log.debug(" User Update  updateUser()");

		List<String> errMsgs = new ArrayList<>();

		//	IrMUsersBank buserbank = null;

		ArrayList<IrMUsersBank> listuserBanks = new ArrayList<>();    

		boolean flag = false;

		try {
			Session session = null;
			session = sessionFactory.getCurrentSession();
			IrMRoles irsRoles = new IrMRoles();
			IrMUsers irMUsers = (IrMUsers) sessionFactory.getCurrentSession().get(IrMUsers.class, userBeanInfo.getUserid());

			/*Get the userid in the loop */

			if(irMUsers!= null) {

				/*Get the Alluserbank in the loop */

				Criteria criterian =session.createCriteria(IrMUsersBank.class,"UserBank");

				criterian.createAlias( "UserBank.irMUsersByUserid","user");

				criterian.add(Restrictions.eq("user.userid",userBeanInfo.getUserid() ));

				List<IrMUsersBank> blstn = criterian.list();

				if(!blstn.isEmpty()){
					for (IrMUsersBank irMUsersBank : blstn) {

						//				if(irMUsersBank.getIrMUsersByUserid().getUserid()==irMUsers.getUserid()){

						listuserBanks.add(irMUsersBank); // adding the one user id 
						//				}
					}
				}else{
					errMsgs.add("Message :: This UserId not contains any UserBanks ,BankBranch data ");

				}


				Criteria criteria = session.createCriteria(IrMUsers.class);
				List<IrMUsers> lst = criteria.list();					

				for (IrMUsers irMUsers2 : lst) {

					if(irMUsers2.getUserid()!=userBeanInfo.getUserid()){

						if( irMUsers2.getUsername().equalsIgnoreCase(userBeanInfo.getUsername())){
							flag=true;
							break;
						}}
				}

				if(flag==false){					

					irMUsers.setUsername(userBeanInfo.getUsername());
					irMUsers.setUserCode(userBeanInfo.getUserCode());
					irMUsers.setFirstName(userBeanInfo.getFirstName());
					irMUsers.setLastName(userBeanInfo.getLastName());
					irMUsers.setPassword(userBeanInfo.getPassword());
					irMUsers.setInternalUserCode(userBeanInfo.getInternalUserCode());
					irsRoles.setRoleid(userBeanInfo.getRoleId());				
					irMUsers.setIrMroles(irsRoles);
					irMUsers.setAdminId(userBeanInfo.getAdminId());
					irMUsers.setEmailAddress(userBeanInfo.getEmailAddress());
					irMUsers.setContactno(userBeanInfo.getContactno());
					irMUsers.setUserPhoto(userBeanInfo.getUserPhoto());	

					irMUsers.setIsactive(userBeanInfo.getIsactive());

					session.update(irMUsers); //update user table1

					criterian.add(Restrictions.eq("userid", userBeanInfo.getUserid())); //usersBank

					if(!listuserBanks.isEmpty()){


						for (IrMUsersBank usersb : listuserBanks){    //getting BankBranchId

							//							usersb.setIrSBankbranch(userBeanInfo.get);
							usersb.setIsactive(userBeanInfo.getIsactive());

							IrSBankBranch irSBankBranch =(IrSBankBranch) session.get(IrSBankBranch.class , userBeanInfo.getBankBranchId());

							if(irSBankBranch.getBranchId()!=null && usersb.getIrSBankbranch().getBranchId()== irSBankBranch.getBranchId() ){


								usersb.setIrSBankbranch(irSBankBranch);

								userBeanInfo.setBankBranchId(irSBankBranch.getBranchId());

								session.update(usersb);          //update userbank table2

							}else{
								errMsgs.add("Validate Error : User Not added the Bank & BankBranch");

								IrMUsersBank irMUsersBank = new IrMUsersBank();

								IrMUsers irUsers = (IrMUsers)session.get(IrMUsers.class, userBeanInfo.getUserid()); 
								IrSBankBranch irSBankBranch2 =(IrSBankBranch) session.get(IrSBankBranch.class, userBeanInfo.getBankBranchId());

							//	irMUsersBank.setIrMUsersByAdminId(irUsers);
								//irMUsersBank.setIrMUsersByUserid(irUsers);
								irMUsersBank.setIrSBankbranch(irSBankBranch2);
								irMUsersBank.setIsactive(userBeanInfo.getIsactive());
								irMUsersBank.setIsdefault(userBeanInfo.getOther_bank_defoult());

								session.save(irMUsersBank);




							}

						}	
					}else{


					}								

					errMsgs.add(" Succcess : Updated user : "+irMUsers.getUsername() +" : userId :"+userBeanInfo.getUserid());
					usersResponseDetails.setErrMsgs(errMsgs);
					usersResponseDetails.setUserBean(userBeanInfo);
					usersResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
					usersResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
					usersResponseDetails.setValidationSuccess(true);

				}else{

					errMsgs.add("Validation Error : Name is Duplicated : Name Already Available");
					usersResponseDetails.setErrMsgs(errMsgs);
					usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					usersResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				}

			}else {
				errMsgs.add("Validation Error : user not available :Not there userid");

				usersResponseDetails.setErrMsgs(errMsgs);

				usersResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				usersResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				log.debug("Not Available the Given UserId :: UserUpdate( ) :: UserDaoImpl ");
			}

		}catch (Exception e) {
			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			usersResponseDetails.setValidationSuccess(false);

			log.error("findAllUsers failed", e);
			throw new HibernateException(e);
		}
		return usersResponseDetails;
	}


	/*  6 th nov 2017
	 * 
	 *  Update to the Two table ..Here common Id Is UserId
		1)User Table 
		2)UserSBank Table

	 */



	public IRPlusResponseDetails updateUserAndBank(UserBean userBeanInfo) throws BusinessException {

		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		log.debug(" User Update  updateUser()");

		List<String> errMsgs = new ArrayList<>();

		//	IrMUsersBank buserbank = null;

		ArrayList<IrMUsersBank> listuserBanks = new ArrayList<>();    
		IrMRoles irsRoles = new IrMRoles();
		boolean flag = false;

		try {
			
			Session session = null;
			session = sessionFactory.getCurrentSession();
			
			IrMUsers irMUsers = (IrMUsers) sessionFactory.getCurrentSession().get(IrMUsers.class, userBeanInfo.getUserid());

			if(irMUsers!=null){

				Criteria userBankCriteria = session.createCriteria(IrMUsersBank.class,"userBank");
				userBankCriteria.createAlias("userBank.irMUsersByUserid","user");

				userBankCriteria.add(Restrictions.eq("user.userid" ,userBeanInfo.getUserid()));

				List<IrMUsersBank> userBankList= userBankCriteria.list();

				if (userBankList.isEmpty()) {

					errMsgs.add("Validation Error :: This UserId not Entered the UserBanks DB : Please Enter the Correct UserId");

					
					
					if(userBeanInfo.getBankBranchId_array()==null){
						
						errMsgs.add("Validation Error :: Please Select the Default Bank and Other Bank");
						errMsgs.add("Validation Error :: Skiping the updation");
					}else{
						// just copying the else part area 
						

						/*Updating new BranchesIds In the defoult locations
						 * 
						 * branchid[ 1,32,23,22,55]
						 * 
						 * existing branchId[3,4]
						 * */

						/*			Criteria bankBranchCriteria2 = getMyCurrentSession().createCriteria(IrMUsersBank.class,"userBank");
						bankBranchCriteria2.createAlias("userBank.irSBankbranch ", "bankBranch");			
						List<IrMUsersBank> bankBranchList = bankBranchCriteria2.list();    // all banksBranches
						 */			
						boolean flagUserName = false;

						IrMUsers user =(IrMUsers) session.get(IrMUsers.class, userBeanInfo.getUserid());	

						// here write the validation of userName duplicate

						Criteria criteria = session.createCriteria(IrMUsers.class);
						List<IrMUsers> dbUsersList = criteria.list();

						for (IrMUsers irMUsers2 : dbUsersList) {

							if(irMUsers2.getUserid()!= userBeanInfo.getUserid()){// not enter 1==1  1==2

								if(userBeanInfo.getUsername().equalsIgnoreCase(irMUsers2.getUsername())){
									flagUserName = true;
								}}
						}

						if(flagUserName==false){ 

							irMUsers.setUsername(userBeanInfo.getUsername());  

							irMUsers.setUserCode(userBeanInfo.getUserCode());
							irMUsers.setFirstName(userBeanInfo.getFirstName());
							irMUsers.setLastName(userBeanInfo.getLastName());
							irMUsers.setPassword(userBeanInfo.getPassword());
							irMUsers.setInternalUserCode(userBeanInfo.getInternalUserCode());
							irsRoles.setRoleid(userBeanInfo.getRoleId());
						
							irMUsers.setIrMroles(irsRoles);
							irMUsers.setAdminId(userBeanInfo.getAdminId());
							irMUsers.setEmailAddress(userBeanInfo.getEmailAddress());
							irMUsers.setContactno(userBeanInfo.getContactno());
							irMUsers.setUserPhoto(userBeanInfo.getUserPhoto());	

							irMUsers.setIsactive(userBeanInfo.getIsactive());

							session.update(irMUsers); 	//update user table1 ******************************


							Criteria userBankCriteria3 = session.createCriteria(IrMUsersBank.class,"userBank");					
							userBankCriteria3.createAlias("userBank.irMUsersByUserid","user");
							userBankCriteria3.add(Restrictions.eq("user.userid", userBeanInfo.getUserid()));    // all users

							List<IrMUsersBank> usersBankList = userBankCriteria3.list();  //branch[1,0,2,5,4]
						
							//	Long[] containsDb;

							Criteria bbArrayList = session.createCriteria(IrSBankBranch.class);

							Integer	no =bbArrayList.list().size();
							Long bankBranchSize = no.longValue();

							Long[] containsDb = new Long[no];
							int i =0;						
							for (IrMUsersBank irMUsersBank2 : usersBankList) {// here  branch[4,0,2]

								log.debug("bankBranchId : "+irMUsersBank2.getIrSBankbranch().getBranchId());

								containsDb[i] =	irMUsersBank2.getIrSBankbranch().getBranchId();
								i++;
							}
							
							boolean branches_flag = true;

							if(userBeanInfo.getBankBranchId_array()!=null){
								
							Long[] bankBranchList_arr = userBeanInfo.getBankBranchId_array();//branch[1,0,2,5,4] branch[1,0,2]

							int uIsize1 =	bankBranchList_arr.length;            // 4 ,5 , 6, 8   ..Ui
							int dBsize2 =	containsDb.length;					// 4 , 3 , 5 , 9    ..Db

							Long common_pair_dump[] = new Long[uIsize1+dBsize2]; // common nos getting            // 4, 5

							Long remove[] = new Long[uIsize1+dBsize2]; // Not Ui no getting....goes inactive  // 3 , 9   

							int k = 0;

							for (Long uiLocation : bankBranchList_arr) {

								for (Long dBLocation : bankBranchList_arr) {

									if(dBLocation==uiLocation){
										common_pair_dump[k]=dBLocation;
										++k;
									}
								}					
							}

							/*DB BANK BRANCHIDS DOESNT SELECT THEN GOES TO THE INACTIVE STATE*/

							List<Long> uiBranchList= Arrays.asList(bankBranchList_arr);
							List<Long> dbBranchList= Arrays.asList(containsDb);

							List<Long> common_pair_dump_List= Arrays.asList(common_pair_dump);

							Criteria bankBranchCriteria2 = session.createCriteria(IrMUsersBank.class,"userBank");
							bankBranchCriteria2.createAlias("userBank.irMUsersByUserid","user");
							bankBranchCriteria2.add(Restrictions.eq("user.userid", userBeanInfo.getUserid()));    // all users
							List<IrMUsersBank> getOne = bankBranchCriteria2.list();	

							for (Long dBLocation : dbBranchList) {
								if(Collections.frequency(common_pair_dump_List, dBLocation)==0){ // if it is not there enters 
									// goes inactive if not available 

									//	for (IrMUsersBank userbank : usersBankList) {

									for (IrMUsersBank irMUsersBank : getOne) {

										if(irMUsersBank.getIrSBankbranch().getBranchId()==dBLocation){
											irMUsersBank.setIsactive(0);
											session.update(irMUsersBank);  // if there goes under Inactive state

										}
									}
									//	}										

								}else{											//available goes active
									for (IrMUsersBank irMUsersBank : getOne) {

										if(irMUsersBank.getIrSBankbranch().getBranchId()==dBLocation){
											irMUsersBank.setIsactive(1);
											session.update(irMUsersBank);  // if there goes under Inactive state

										}
									}

								}
							}
							
							
							/*SELECTING IN UI PAGE BANKBRANCHIDS THEN SAVING THOSE DETAILS IN DB*/			

							//				containsDb;  4,5,2    ui 4,8,2,6

							for (int j = 0; j < bankBranchList_arr.length; j++) {

								if( Collections.frequency(dbBranchList,bankBranchList_arr[j])!=0 ){

									errMsgs.add(" Message Alert :: This BankBranchId contains :"+bankBranchList_arr[j]);

								}else{ // new branches creating

									IrMUsersBank userbank = new IrMUsersBank();

									IrMUsers Userid = (IrMUsers)session.get(IrMUsers.class, userBeanInfo.getUserid());
									IrSBankBranch irSBankBranch = (IrSBankBranch)session.get(IrSBankBranch.class,bankBranchList_arr[j]);

								//	userbank.setIrMUsersByAdminId(Userid);
								//	userbank.setIrMUsersByUserid(Userid);
									//userbank.setIrSBankbranch(irSBankBranch);							
									//	userbank.setIsactive(userBeanInfo.getIsactive());
									userbank.setIsactive(1);
									Byte b=0;

									userbank.setIsdefault(b);

									session.save(userbank);  // 2 table ****************************

								}								
							} // close for loop
							
							
							 } //if close BankBranchId_array()
							else{
								branches_flag =false;
								errMsgs.add("Validation Error : Please select BankBranchId Updated Success");
								usersResponseDetails.setErrMsgs(errMsgs);
								usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
								usersResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);

							}
							if(branches_flag){
							errMsgs.add("Success : Updated Success");
							usersResponseDetails.setErrMsgs(errMsgs);
							usersResponseDetails.setValidationSuccess(true);
							usersResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
							usersResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
							}
						}else{

							errMsgs.add("Validation Error :: Duplicate UserName Please Enter Different Name :: "
									+ "This UserName Already Available , Enter Diff Name ");

							errMsgs.add("Validation Error : Name is Duplicated : Name Already Available");
							usersResponseDetails.setErrMsgs(errMsgs);
							usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
							usersResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);

						}

					
						
						// closing
					}
					
					
					usersResponseDetails.setErrMsgs(errMsgs);
					usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					usersResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
					
					

				}else{
					/*Updating new BranchesIds In the defoult locations
					 * 
					 * branchid[ 1,32,23,22,55]
					 * 
					 * existing branchId[3,4]
					 * */

					/*			Criteria bankBranchCriteria2 = getMyCurrentSession().createCriteria(IrMUsersBank.class,"userBank");
					bankBranchCriteria2.createAlias("userBank.irSBankbranch ", "bankBranch");			
					List<IrMUsersBank> bankBranchList = bankBranchCriteria2.list();    // all banksBranches
					 */			
					boolean flagUserName = false;

					IrMUsers user =(IrMUsers) session.get(IrMUsers.class, userBeanInfo.getUserid());	

					// here write the validation of userName duplicate

					Criteria criteria = session.createCriteria(IrMUsers.class);
					List<IrMUsers> dbUsersList = criteria.list();

					for (IrMUsers irMUsers2 : dbUsersList) {

						if(irMUsers2.getUserid()!= userBeanInfo.getUserid()){// not enter 1==1  1==2

							if(userBeanInfo.getUsername().equalsIgnoreCase(irMUsers2.getUsername())){
								flagUserName = true;
							}}
					}

					if(flagUserName==false){ 

						irMUsers.setUsername(userBeanInfo.getUsername());  

						irMUsers.setUserCode(userBeanInfo.getUserCode());
						irMUsers.setFirstName(userBeanInfo.getFirstName());
						irMUsers.setLastName(userBeanInfo.getLastName());
						irMUsers.setPassword(userBeanInfo.getPassword());
						irMUsers.setInternalUserCode(userBeanInfo.getInternalUserCode());
					
						irsRoles.setRoleid(userBeanInfo.getRoleId());
						irMUsers.setIrMroles(irsRoles);
						irMUsers.setAdminId(userBeanInfo.getAdminId());
						irMUsers.setEmailAddress(userBeanInfo.getEmailAddress());
						irMUsers.setContactno(userBeanInfo.getContactno());
						irMUsers.setUserPhoto(userBeanInfo.getUserPhoto());	

						irMUsers.setIsactive(userBeanInfo.getIsactive());

						session.update(irMUsers); 	//update user table1 ******************************


						Criteria userBankCriteria3 = session.createCriteria(IrMUsersBank.class,"userBank");					
						userBankCriteria3.createAlias("userBank.irMUsersByUserid","user");
						userBankCriteria3.add(Restrictions.eq("user.userid", userBeanInfo.getUserid()));    // all users

						List<IrMUsersBank> usersBankList = userBankCriteria3.list();  //branch[1,0,2,5,4]
					
						//	Long[] containsDb;

						Criteria bbArrayList = session.createCriteria(IrSBankBranch.class);

						Integer	no =bbArrayList.list().size();
						Long bankBranchSize = no.longValue();

						Long[] containsDb = new Long[no];
						int i =0;						
						for (IrMUsersBank irMUsersBank2 : usersBankList) {// here  branch[4,0,2]

							log.debug("bankBranchId : "+irMUsersBank2.getIrSBankbranch().getBranchId());

							containsDb[i] =	irMUsersBank2.getIrSBankbranch().getBranchId();
							i++;
						}
						
						boolean branches_flag = true;

						if(userBeanInfo.getBankBranchId_array()!=null){
							
						Long[] bankBranchList_arr = userBeanInfo.getBankBranchId_array();//branch[1,0,2,5,4] branch[1,0,2]

						int uIsize1 =	bankBranchList_arr.length;            // 4 ,5 , 6, 8   ..Ui
						int dBsize2 =	containsDb.length;					// 4 , 3 , 5 , 9    ..Db

						Long common_pair_dump[] = new Long[uIsize1+dBsize2]; // common nos getting            // 4, 5

						Long remove[] = new Long[uIsize1+dBsize2]; // Not Ui no getting....goes inactive  // 3 , 9   

						int k = 0;

						for (Long uiLocation : bankBranchList_arr) {

							for (Long dBLocation : bankBranchList_arr) {

								if(dBLocation==uiLocation){
									common_pair_dump[k]=dBLocation;
									++k;
								}
							}					
						}

						/*DB BANK BRANCHIDS DOESNT SELECT THEN GOES TO THE INACTIVE STATE*/

						List<Long> uiBranchList= Arrays.asList(bankBranchList_arr);
						List<Long> dbBranchList= Arrays.asList(containsDb);

						List<Long> common_pair_dump_List= Arrays.asList(common_pair_dump);

						Criteria bankBranchCriteria2 = session.createCriteria(IrMUsersBank.class,"userBank");
						bankBranchCriteria2.createAlias("userBank.irMUsersByUserid","user");
						bankBranchCriteria2.add(Restrictions.eq("user.userid", userBeanInfo.getUserid()));    // all users
						List<IrMUsersBank> getOne = bankBranchCriteria2.list();	

						for (Long dBLocation : dbBranchList) {
							if(Collections.frequency(common_pair_dump_List, dBLocation)==0){ // if it is not there enters 
								// goes inactive if not available 

								//	for (IrMUsersBank userbank : usersBankList) {

								for (IrMUsersBank irMUsersBank : getOne) {

									if(irMUsersBank.getIrSBankbranch().getBranchId()==dBLocation){
										irMUsersBank.setIsactive(0);
										session.update(irMUsersBank);  // if there goes under Inactive state

									}
								}
								//	}										

							}else{											//available goes active
								for (IrMUsersBank irMUsersBank : getOne) {

									if(irMUsersBank.getIrSBankbranch().getBranchId()==dBLocation){
										irMUsersBank.setIsactive(1);
										session.update(irMUsersBank);  // if there goes under Inactive state

									}
								}

							}
						}
						
						
						/*SELECTING IN UI PAGE BANKBRANCHIDS THEN SAVING THOSE DETAILS IN DB*/			

						//				containsDb;  4,5,2    ui 4,8,2,6

						for (int j = 0; j < bankBranchList_arr.length; j++) {

							if( Collections.frequency(dbBranchList,bankBranchList_arr[j])!=0 ){

								errMsgs.add(" Message Alert :: This BankBranchId contains :"+bankBranchList_arr[j]);

							}else{ // new branches creating

								IrMUsersBank userbank = new IrMUsersBank();

								IrMUsers Userid = (IrMUsers)session.get(IrMUsers.class, userBeanInfo.getUserid());
								IrSBankBranch irSBankBranch = (IrSBankBranch)session.get(IrSBankBranch.class,bankBranchList_arr[j]);

								//userbank.setIrMUsersByAdminId(Userid);
								////userbank.setIrMUsersByUserid(Userid);
								userbank.setIrSBankbranch(irSBankBranch);							
								//	userbank.setIsactive(userBeanInfo.getIsactive());
								userbank.setIsactive(1);
								Byte b=0;

								userbank.setIsdefault(b);

								session.save(userbank);  // 2 table ****************************

							}								
						} // close for loop
						
						
						 } //if close BankBranchId_array()
						else{
							branches_flag =false;
							errMsgs.add("Validation Error : Please select BankBranchId Updated Success");
							usersResponseDetails.setErrMsgs(errMsgs);
							usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
							usersResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);

						}
						if(branches_flag){
						errMsgs.add("Success : Updated Success");
						usersResponseDetails.setErrMsgs(errMsgs);
						usersResponseDetails.setValidationSuccess(true);
						usersResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
						usersResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
						}
					}else{

						errMsgs.add("Validation Error :: Duplicate UserName Please Enter Different Name :: "
								+ "This UserName Already Available , Enter Diff Name ");

						errMsgs.add("Validation Error : Name is Duplicated : Name Already Available");
						usersResponseDetails.setErrMsgs(errMsgs);
						usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
						usersResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);

					}

				}					


			} else { // userid null
				errMsgs.add("Validation Error :: Not valid userId - not contains this userid in DB");
				usersResponseDetails.setErrMsgs(errMsgs);
				usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				usersResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);

			}



		}catch (Exception e){
			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			usersResponseDetails.setValidationSuccess(false);

			log.error("Exception raised in Users update", e);
			throw new HibernateException(e);
		}
		return usersResponseDetails;
	}

	/*4.update status only	 delete status = 2*/

	public IRPlusResponseDetails updateStatusUser(UserBean userBeanInfo) throws BusinessException {

		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		log.debug("updateUser() ::Showing All Users List");
		IrMUsers irMUsers = null;		
		List<String> errMsg = new ArrayList<>(); 
		IrMUsersBank usersBank = null;
		try {
			Session session = null;
			session = sessionFactory.getCurrentSession();
			
			irMUsers = (IrMUsers) sessionFactory.getCurrentSession().get(IrMUsers.class, userBeanInfo.getUserid());

			Criteria criteria = session.createCriteria(IrMUsersBank.class);
			List<IrMUsersBank> listUbank = criteria.list();

			/*users update*/

			if(irMUsers!= null) {

				irMUsers.setIsactive(userBeanInfo.getIsactive());

				session.update(irMUsers);

				errMsg.add("Success :: Updated Status : " +userBeanInfo.getIsactive() +" UserId : "+irMUsers.getUsername());

				/*userbanks update*/

				for (IrMUsersBank irMUsersBank : listUbank) {
/*
					if(userBeanInfo.getUserid()==irMUsersBank.getIrMUsersByUserid().getUserid()){
						irMUsersBank.setIsactive(userBeanInfo.getIsactive());
						session.update(irMUsersBank);
					}
*/
				}

				usersResponseDetails.setErrMsgs(errMsg);					
				usersResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				usersResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);

			}else {

				errMsg.add(" Error : Not updated Status in User :"+irMUsers.getUsername());					
				usersResponseDetails.setErrMsgs(errMsg);
				usersResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				usersResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);

				log.debug("Not Available the Given UserId :: UserUpdate( ) :: UserDaoImpl ");
			}

		}catch (Exception e) {
			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);

			log.error("Exception findAllUsers",e);
			throw new HibernateException(e);
		}
		return usersResponseDetails;
	}

	/* deleteUserByIdStatus =2 */

	/*	 delete meant isactive = 2 not permanent delete in DB
	 */


	@Autowired 
	IRolesDao role;
//
//	public IRPlusResponseDetails filterUsers(UserFilterInfo userFilterInfo) throws BusinessException{
//
//		log.debug("inside filterUsers - IrMUsers instance  " +userFilterInfo.getUsercode());
//		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();
//
//		List<UserFilterInfo> listUserBean = new ArrayList<UserFilterInfo>();
//
//		List<String> errMsgs = new ArrayList<>(); 
//
//		try{
//			Session session = null;
//			session = sessionFactory.getCurrentSession();
//			
//			Criteria criteria = session.createCriteria(IrMUsers.class ,"users"); 
//
//			boolean flag = true;
//			if(userFilterInfo.getUsercode()==null && userFilterInfo.getUsername()==null && userFilterInfo.getRoleId()==null ){
//			
//				flag =false;
//		//		errMsgs.add("Validation Error - Not Matched 3 fields : UserCode ,Name ,Role is Empty");
//			
//			}else{
//					
//					if(userFilterInfo.getUsercode()!=null){
//						criteria.add( Restrictions.eq("users.userCode", userFilterInfo.getUsercode()));
//					}
//						
//					if(userFilterInfo.getUsercode()!=null && userFilterInfo.getUsername()!=null && userFilterInfo.getRoleId()!=null ){
//
//						criteria.add(Restrictions.like( "users.username", userFilterInfo.getUsername()))
//						.add(Restrictions.like("users.roleId", userFilterInfo.getRoleId()))
//						.add( Restrictions.eq("users.userCode", userFilterInfo.getUsercode()));
//						
//					}else{
//			//			errMsgs.add("Validation Error - Not Matched 3 fields : UserCode ,Name ,Role");
//					}
//		
//					if(userFilterInfo.getUsercode()!=null && userFilterInfo.getRoleId()!=null && userFilterInfo .getUsername()==null){
//		
//						criteria.add(Restrictions.like( "users.roleId", userFilterInfo.getRoleId()))
//						.add( Restrictions.eq("users.userCode", userFilterInfo.getUsercode() ) );
//		
//					}else{
//			//			errMsgs.add("Validation Error :: 2 : Filters Not Entered or Not Matched 2 fields : Userid , roleid ");
//					}
//		
//					if(userFilterInfo.getUsercode()!=null && userFilterInfo.getUsername()!=null && userFilterInfo.getRoleId()==null){
//
//						criteria.add(Restrictions.like( "users.username", userFilterInfo.getUsername()))
//						.add( Restrictions.eq("userCode", userFilterInfo.getUsercode()));
//					
//					}else{
//			//			errMsgs.add("Message Alert ::  Not Entered two fields ");
//					}
//		
//					if (userFilterInfo.getUsername()!=null && userFilterInfo.getRoleId()!=null && userFilterInfo.getUsercode()==null) {
//		
//						criteria.add( Restrictions.like("users.username", userFilterInfo.getUsername()))
//						.add(Restrictions.like( "users.roleId", userFilterInfo.getRoleId()));
//		
//					}else{
//			//			errMsgs.add("Message Alert :: Entered fields - UseName , Role ");
//					}
//					if (userFilterInfo.getUsercode()!=null && userFilterInfo.getRoleId()== null && userFilterInfo.getUsername()==null) {
//						
//						criteria.add( Restrictions.eq("users.userCode", userFilterInfo.getUsercode()));
//		
//					}else{
//			//			errMsgs.add("Message Alert ::  Entered Valid UserCode ");
//					}
//		
//					if (userFilterInfo.getUsername()!=null && userFilterInfo.getUsercode() == null && userFilterInfo.getRoleId()== null ) {
//		
//						criteria.add( Restrictions.like("users.username", userFilterInfo.getUsername()));
//		
//					}else{
//			//			errMsgs.add("Message Alert :: Entered UserName ");
//					}
//		
//					if (userFilterInfo.getRoleId()!=null && userFilterInfo.getUsername() == null && userFilterInfo.getUsercode() == null ) {
//		
//						criteria.add( Restrictions.like("users.roleId", userFilterInfo.getRoleId()) );
//		
//					}else{			
//			//			errMsgs.add("Message Alert :: Enter Valid Roleid Only");
//						
//			//			usersResponseDetails.setErrMsgs(errMsgs);
//					}
//					
//			} 
//
//			//		.add( Restrictions.eq("firstName", userFilterInfo.getFirstName()) )
//			//		 .add( Restrictions.eq("roleid", userFilterInfo.getRoleId()) );
//
//		if(flag){
//			
//			List<IrMUsers>	users = criteria.list();
//			if(!users.isEmpty()){
//					for (IrMUsers irMUsers : users) {
//		
//						UserFilterInfo userFilter = new UserFilterInfo();
//						UserBean userBean = new UserBean();
//		
//						log.debug(irMUsers.getUsername());
//		
//						System.out.println("UserName : "+irMUsers.getUsername());
//		
//						userFilter.setUsername(irMUsers.getUsername());
//		
//						userBean.setContactno(irMUsers.getContactno());
//						userBean.setEmailAddress(irMUsers.getEmailAddress());
//						userBean.setFirstName(irMUsers.getFirstName());
//						userBean.setInternalUserCode(irMUsers.getInternalUserCode());
//						userBean.setIsactive(irMUsers.getIsactive());
//						userBean.setLastName(irMUsers.getLastName());
//						/*			userBean.setOther_bank_defoult(irMUsers.get);
//						userBean.setParent_bank_defoult(parent_bank_defoult);
//						 */			
//						userBean.setPassword(irMUsers.getPassword());
//						if(irMUsers.getRoleId()!=null){
//						IrMRoles ir =(IrMRoles) session.get(IrMRoles.class, irMUsers.getRoleId());
//						if(ir!=null){
//						userBean.setPermissionRoleName(ir.getRolename());
//						}else{
//							userBean.setPermissionRoleName("");
//						}
//						}
//						userBean.setRoleId(irMUsers.getRoleId());
//						userBean.setUserCode(irMUsers.getUserCode());
//						userBean.setUserid(irMUsers.getUserid());
//						userBean.setUsername(irMUsers.getUsername());
//						userBean.setUserPhoto(irMUsers.getUserPhoto());		
//						userBean.setAdminId(irMUsers.getAdminId());		
//						userFilter.setUserBean(userBean);
//		
//						listUserBean.add(userFilter);
//					}
//				}else{errMsgs.add("Message Alert : Not Matched");}
//		}else{
//			errMsgs.add("Validation :: Not Entered Fields ");
//		}
//			//	criteria.createAlias("bankLicense.bank", "bank");
//			//	criteria.add(Restrictions.eq("bank.siteId",bankFilterInfo.getSiteId()));
//			if(listUserBean.isEmpty()){				
//
//				errMsgs.add("Validation :: Not Entered Valid propety value ");
//
//				usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
//				usersResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
//			}else{				
//
//				usersResponseDetails.setListUserFilter(listUserBean);
//				errMsgs.add("Success : Entered Valid propety value ");
//				usersResponseDetails.setValidationSuccess(true);
//				usersResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
//				usersResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
//
//			}
//
//			usersResponseDetails.setErrMsgs(errMsgs);
//
//		}catch (RuntimeException re) {
//
//			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
//			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);			
//			log.error("getUser failed", re);	
//			throw new HibernateException(re);
//		}
//
//		return usersResponseDetails;
//	}

	@Override
	public IRPlusResponseDetails filterUsers(UserFilterInfo userFilterInfo) throws BusinessException{
	
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;
		List<UserBean> listUserBean = new ArrayList<UserBean>();

		List<FileTypeInfo>file = new ArrayList<FileTypeInfo>();
		List<UserFilterInfo> filelistss = new ArrayList<UserFilterInfo>();
		try
		{
			
			session = sessionFactory.getCurrentSession();

		Criteria criteria = session.createCriteria(IrMUsers.class, "userList");
		criteria.createAlias("userList.irMroles", "roles");
		/*@Formula(value = "concat(f_street, f_houseNumber, f_postcode)")
		private String fullAddress;
		*/
		
/*		criteria.setProjection(Projections.groupProperty("batchlist.irFPFileHeaderRecord.fileHeaderRecordId"));*/
	
		
	/*	criteria.createAlias("batchlist.irFPFileHeaderRecord", "file");
		criteria.createAlias("file.irSBankbranch", "Banklist");
		criteria.createAlias("Banklist.bank", "Bank");
		criteria.createAlias("Banklist.irMUsersBank", "userBank");
		criteria.createAlias("userBank.irMUsers", "user");
		criteria.createAlias("batchlist.irFPFileHeaderRecord.fileType", "filetype");*/
		
	
//			criteria.add(Restrictions.ne("roles.isactive",(2)));
//			criteria.add(Restrictions.ne("userList.isactive",(2)));
//		
		if(userFilterInfo.getSiteId()!=null)
		{
			criteria.add(Restrictions.eq("userList.site.siteId",(userFilterInfo.getSiteId())));
		}
			if(userFilterInfo.getRoleId()!=null)
			{
				criteria.add(Restrictions.eq("roles.roleid",(userFilterInfo.getRoleId())));
			}
		if(userFilterInfo.getIsactive()!=null)
			{
				criteria.add(Restrictions.eq("userList.isactive",userFilterInfo.getIsactive()));
			}
			/*	if(userFilterInfo.getUserid()!=null)*/
			/*{
				criteria.add(Restrictions.eq("userList.userid",userFilterInfo.getUserid())) ;
			}
		*/
			if(userFilterInfo.getUsername()!=null&&!userFilterInfo.getUsername().isEmpty())
			{
		
				criteria.add(Restrictions.eq("userList.firstName",userFilterInfo.getUsername())) ;
			/*	criteria.like("userList.firstName + ' ' +userList.lastName"),userFilterInfo.getUsername();*/
		    }
			
		
		List<IrMUsers> userLists = (List)criteria.list();

	
		Iterator <IrMUsers>branchListIt = userLists.iterator();
		
		IrMUsers user = null;

	
		IrMRoles roles = null;
		UserBean userBean = null;

		while (branchListIt.hasNext()) 
		{
			
			user = branchListIt.next();

		/*	user = (IrMUsers) user.getIrMRoleses();*/
			
			userBean = new UserBean();
			userBean.setUserid(user.getUserid());
			if(user.getFirstName()!=null){
				userBean.setFirstName(user.getFirstName());}else{
					userBean.setFirstName("");
				}
			
			if(user.getLastName()!=null){
				userBean.setLastName(user.getLastName());}else{
					userBean.setLastName("");
				}
			if(user.getEmailAddress()!=null){
				userBean.setEmailAddress(user.getEmailAddress());}else{
					userBean.setEmailAddress("");
				}
			if(user.getContactno()!=null){
				userBean.setContactno(user.getContactno());}else{
					userBean.setContactno("");
				}
			if(user.getIsactive()!=null){
				userBean.setIsactive(user.getIsactive());}else{
					userBean.setIsactive(0);
				}

		
			
			if(user.getUserPhoto()!=null){
				userBean.setUserPhoto(user.getUserPhoto());}else{
					userBean.setUserPhoto("");
				}
			if(user.getIrMroles().getRoleid()!=null){
				userBean.setRoleId(user.getIrMroles().getRoleid()); }else{
					userBean.setRoleId(0);
				}
			if(user.getIrMroles().getRolename()!=null){
				userBean.setRoleName(user.getIrMroles().getRolename()); }else{
					userBean.setRoleName("");
				}	
			
	
		
				listUserBean.add(userBean);
		
			
				}
			
			irPlusResponseDetails.setUserBeans(listUserBean);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}
		catch(Exception e)
		{
			log.error("Exception in listBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}
	//------------------------------------------------------------------------------	
	// modifying getUserById(String userId) to getUserById2(String userId)

	public IRPlusResponseDetails getUserById2(String userId) throws BusinessException {

		log.debug("getting IrMUsers instance with id: " + userId);
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		List<UserBean> listUserBean = new ArrayList<UserBean>();

		UserBean userBean = new UserBean();
		try {
			Session session = null;
			session = sessionFactory.getCurrentSession();
			
			log.debug("Inside UserDaoImpl :: getById ::  userId " + userId);
			IrMUsers irMUsers = (IrMUsers) sessionFactory.getCurrentSession().get(IrMUsers.class, Integer.parseInt(userId));
			log.debug("Inside UserDaoImpl ::getById ::  IrMUsers " + irMUsers);
			if (irMUsers == null) {

				log.debug("get successful, no instance found");
				usersResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				usersResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);

			} else {
				log.debug("get successful, instance found"); 

				userBean.setUserid(irMUsers.getUserid());

				userBean.setFirstName(irMUsers.getFirstName());
				userBean.setLastName(irMUsers.getLastName());
				userBean.setUsername(irMUsers.getUsername());
				userBean.setUserPhoto(irMUsers.getUserPhoto());
				userBean.setUserCode(irMUsers.getUserCode());
				userBean.setRoleId(irMUsers.getIrMroles().getRoleid());						

				userBean.setPassword((String) irMUsers.getPassword());

				userBean.setContactno(irMUsers.getContactno());
				userBean.setEmailAddress(irMUsers.getEmailAddress());

				userBean.setCreatedDate(irMUsers.getCreatedDate());				
				userBean.setModifiedDate(irMUsers.getModifiedDate());

				userBean.setInternalUserCode(irMUsers.getInternalUserCode());
				userBean.setAdminId(irMUsers.getAdminId()); 

				userBean.setIsactive(irMUsers.getIsactive());		

				Criteria criteria =session.createCriteria(IrMUsersBank.class , "usersBank");
				criteria.createAlias("usersBank.irMUsersByUserid", "user");
				///		criteria.createAlias("user.userid", "Users");
				criteria.add(Restrictions.eq("user.userid",irMUsers.getUserid() ));
				criteria.add(Restrictions.eq("user.isactive",1 ));
				List<IrMUsersBank> lst = criteria.list();

				log.debug(" Inside of OneShow : List Set of bankBranches : "+lst.toString() + " : size :" +lst.size());

				Long dup_bankBranchId_array[] = new Long[lst.size()+1];


				int k=0;
				for (IrMUsersBank element : lst) {

					dup_bankBranchId_array[k]=element.getIrSBankbranch().getBranchId();
					++k;
				}

				log.debug("Inside of Set of bankBranches  array[]: "+lst.toString() + " : size :" +lst.size());


				userBean.setBankBranchId_array(dup_bankBranchId_array);

				listUserBean.add(userBean);					

				usersResponseDetails.setUserBeans(listUserBean);

				//			usersResponseDetails.setErrMsgs(errMsgs);
				usersResponseDetails.setValidationSuccess(true);
				usersResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				usersResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);

			}

		} catch (RuntimeException re) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);			
			log.error("getUser failed", re);	
			throw new HibernateException(re);
		}
		return usersResponseDetails;
	}


	@Override
	public IRPlusResponseDetails findAllUsers(String siteId) throws BusinessException {

		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		List<String> errMsgs = new ArrayList<String>();
		
		Session session = sessionFactory.getCurrentSession();

		log.debug("findAllUsers() ::Showing All Users List");
		try {
			
			
			
			
			//Criteria criteria = session.createCriteria("com.irplus.dao.hibernate.entities.IrMUsers");

			//adding code here 
		//	criteria.add(Restrictions.());
			
			Query users = session.createQuery("from IrMUsers users where users.isactive!=? and users.site.siteId=?");
			users.setParameter(0,2);
			users.setParameter(1,new Long(siteId));
			
			List<IrMUsers> listUsers = users.list();
			log.debug("Shown All Users successful, result size: " + listUsers.size());

			List<UserBean> listUserBean = new ArrayList<UserBean>();

			for (IrMUsers irMUsers : listUsers) {
				
			//	if(irMUsers.getIsactive()!=null && irMUsers.getIsactive()!=2){

				UserBean userBean = new UserBean();

				log.debug("get successful, instance found"); 

				userBean.setUserid(irMUsers.getUserid());
				
				if(irMUsers.getFirstName()!=null){
				userBean.setFirstName(irMUsers.getFirstName());}else{
					userBean.setFirstName("");
				}
				if(irMUsers.getLastName()!=null){
				userBean.setLastName(irMUsers.getLastName());}else{
					userBean.setLastName("");
				}
				if(irMUsers.getUsername()!=null){
				userBean.setUsername(decrypt(irMUsers.getUsername()));}else{
					userBean.setUsername("");
				}
				if(irMUsers.getUserPhoto()!=null){
				userBean.setUserPhoto(irMUsers.getUserPhoto());}else{
					userBean.setUserPhoto("");
				}
				if(irMUsers.getUserCode()!=null){
				userBean.setUserCode(irMUsers.getUserCode());}else{
					userBean.setUserCode("");
				}
				
				if(irMUsers.getIrMroles().getRoleid()!=null){
					userBean.setRoleId(irMUsers.getIrMroles().getRoleid()); }else{
						userBean.setRoleId(0);
					}
				
				if(irMUsers.getIrMroles().getRoleid()!=null){
					String rolname =irMUsers.getIrMroles().getRolename();
					if(rolname!="SUPERADMIN"){
						userBean.setRoleName(rolname); 
					}else{
						userBean.setRoleName(""); 
					}
					}
			
			
/*				if(irMUsers.getRoleId()!=null){
				userBean.setRoleId(irMUsers.getRoleId()); }else{
					userBean.setRoleId(0);
				}
				if(irMUsers.getRoleId()!=null){
					IrMRoles role =(IrMRoles)session.get(IrMRoles.class ,irMUsers.getRoleId());
					if(role!= null){
						
					String rolname = role.getRolename();
					if(rolname!="SUPERADMIN"){
						userBean.setRoleName(rolname); 
					}else{
						userBean.setRoleName(""); 
					}
				}
				}*/
				if((String)irMUsers.getPassword()!=null){
				userBean.setPassword((String)irMUsers.getPassword());}else{
					userBean.setPassword("");	
				}
				if(irMUsers.getContactno()!=null){
				userBean.setContactno(decrypt(irMUsers.getContactno()));}else{
					userBean.setContactno("");
				}
				if(irMUsers.getEmailAddress()!=null){
				userBean.setEmailAddress(irMUsers.getEmailAddress());}else{
					userBean.setEmailAddress("");
				}
				if(irMUsers.getCreatedDate()!=null){
				userBean.setCreatedDate(irMUsers.getCreatedDate());}else{
					
				}				
				if(irMUsers.getModifiedDate()!=null){
				userBean.setModifiedDate(irMUsers.getModifiedDate());}else{
					//userBean.setModifiedDate(0);
				}
				if(irMUsers.getInternalUserCode()!=null){
				userBean.setInternalUserCode(irMUsers.getInternalUserCode());}else{
					userBean.setInternalUserCode("");
				}
				if(irMUsers.getAdminId()!=null){
				userBean.setAdminId(irMUsers.getAdminId());}else{
					userBean.setAdminId(0);
				} 
				if(irMUsers.getIsactive()!=null){
				userBean.setIsactive(irMUsers.getIsactive());
				}else{
					userBean.setIsactive(0);
				}
				/*Adding thd  Defoult Bank and Other Banks*/

				Criteria criteria2 = session.createCriteria(IrMUsersBank.class , "userBank");
				criteria2.createAlias("userBank.irMUsers", "user");
				criteria2.add(Restrictions.eq("user.userid", irMUsers.getUserid()));

				List<IrMUsersBank> usersBankList = criteria2.list();
				boolean flag = false;

				if (usersBankList.isEmpty()) {
					errMsgs.add(" Message Alert :: id : "+irMUsers.getUserid()+" : Name :"+irMUsers.getUsername()+" ..."
							+ "This User Not Contains Any user Bank Branches");
					flag = true;
				}else {

					//	Long[] bankBranches = new Long[usersBankList.size()];

					Map<Long, String> branchid_branchLoc = new HashMap<Long, String>(); 

					IRSBank bb=null;

					for (IrMUsersBank irMUsersBank : usersBankList) {

						branchid_branchLoc.put(irMUsersBank.getBank().getBankId(),irMUsersBank.getBank().getBankName());
						//		userBean.setParentBankId(irMUsersBank.getIrSBankbranch().getBranchId());
						
						if(irMUsersBank.getIsdefault()==1){

							bb=(IRSBank)session.get(IRSBank.class,irMUsersBank.getBank().getBankId() );
							userBean.setParentBankId(bb.getBankId());
							if(bb.getBankName()!=null){
							userBean.setDefaultBankName(bb.getBankName());
							}else{userBean.setDefaultBankName("");}
						}
					}
					if(!branchid_branchLoc.isEmpty()){
					userBean.setBranch_ids_Locations(branchid_branchLoc);
					if(branchid_branchLoc.keySet().size()!=0){
						userBean.setOtherBanks(branchid_branchLoc.keySet().size());}else{userBean.setOtherBanks(0);}
					}else{userBean.setOtherBanks(0);}					
				}	

				/*If user not contains userBank entries then checking bank-getting details*/
				if (flag) {
					Criteria criteria3 = session.createCriteria(IRSBank.class , "bank");

					criteria3.createAlias("bank.irMUsers_id" ,"user");
					criteria3.add(Restrictions.eq("user.userid", irMUsers.getUserid()));

					List<IRSBank> bankList = criteria3.list();

					if(!bankList.isEmpty()){

						for (IRSBank irsBank : bankList) {					
							userBean.setParentBankId(irsBank.getBankId());
							userBean.setDefaultBankName(irsBank.getBankName());							
						}	

					}else{
						errMsgs.add("Message Alert : id : "+ irMUsers.getUserid()+" Name : "+ irMUsers.getUsername()+" : "
								+ "This user Not Contains Default Bank");
					}
				}

				//userBean.setParent_bank_defoult();

				listUserBean.add(userBean);		
				usersResponseDetails.setErrMsgs(errMsgs);
			//} 
		}
			usersResponseDetails.setUserBeans(listUserBean);
			usersResponseDetails.setErrMsgs(errMsgs);
			usersResponseDetails.setRecordsFiltered(listUserBean.size());
			usersResponseDetails.setRecordsTotal(listUserBean.size());
			usersResponseDetails.setValidationSuccess(true);
			usersResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);			
			session.flush();
		} catch (RuntimeException re) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);

			log.error("findAllUsers failed", re);
			throw new HibernateException(re);
		}				
		return usersResponseDetails;
	}


	@Override
	public IRPlusResponseDetails updateUser(UserBean userBeanInfo) throws BusinessException {

		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		log.debug("updateUser() ::Showing All Users List");

		try {
			Session session = null;
			session = sessionFactory.getCurrentSession();
			IrMRoles irsRoles =new IrMRoles();
			IrMUsers irMUsers = (IrMUsers) sessionFactory.getCurrentSession().get(IrMUsers.class, userBeanInfo.getUserid());

			if(irMUsers!= null) {
				irMUsers.setUsername(encrypt(userBeanInfo.getUsername()));
				irMUsers.setUserCode(userBeanInfo.getUserCode());
				irMUsers.setFirstName(userBeanInfo.getFirstName());
				irMUsers.setLastName(userBeanInfo.getLastName());
				irMUsers.setPassword(generateHash(userBeanInfo.getPassword()));
				irMUsers.setInternalUserCode(userBeanInfo.getInternalUserCode());			
				irsRoles.setRoleid(userBeanInfo.getRoleId());
				irMUsers.setIrMroles(irsRoles);
				irMUsers.setAdminId(userBeanInfo.getAdminId());
				irMUsers.setEmailAddress(userBeanInfo.getEmailAddress());
				irMUsers.setContactno(encrypt(userBeanInfo.getContactno()));
				
				irMUsers.setUserPhoto(userBeanInfo.getUserPhoto());	

				irMUsers.setIsactive(userBeanInfo.getIsactive());

				session.update(irMUsers);
				userBeanInfo.getUserBank()[0].setUserId(userBeanInfo.getUserid());
				session.flush();
				usersResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				usersResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);

			}else {
				usersResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				usersResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				usersResponseDetails.setValidationSuccess(false);
				log.debug("Not Available the Given UserId :: UserUpdate( ) :: UserDaoImpl ");
			}

		}catch (Exception e) {
			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			usersResponseDetails.setValidationSuccess(false);

			log.error("findAllUsers failed", e);
			throw new HibernateException(e);
		}
		return usersResponseDetails;
	}

// this is permanent delete 
	@Override
	public IRPlusResponseDetails deleteUserById(String userId) throws BusinessException {

		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		log.debug("Inside UserDaoImpl :: DeleteUserById");

		log.debug("deleting IrMUser instance"+userId);

		try {

			Session session = null;
			session = sessionFactory.getCurrentSession();
			
			Query query = session.createQuery("delete IrMUsers d where d.userid =:userid_Id");

			query.setParameter("userid_Id", Integer.parseInt(userId));

			int result=query.executeUpdate();

			if(result==0){

				log.debug("delete country failed to delete");
				usersResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				usersResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);

			}else {
				usersResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				usersResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}		

			log.debug("delete successful");
		} catch (RuntimeException re) {

			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception in delete Country",re);
			throw new HibernateException(re);
		}		
		return usersResponseDetails;
	}

	/* showOne User 
	 * Show One User with BankId ,BankName ,BankBranches 
	 *  
	 *   */
	
	@Override
	public IRPlusResponseDetails getUserById(String userId) throws BusinessException {

		log.debug("getting IrMUsers instance with id: " + userId);
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		List<UserBean> listUserBean = new ArrayList<UserBean>();

		List<String> errMsgs = new ArrayList<String>();
	
		UserBean userBean = new UserBean();
		try {
			Session session = null;
			session = sessionFactory.getCurrentSession();
			
			log.debug("Inside UserDaoImpl :: getById ::  userId " + userId);
			IrMUsers irMUsers = (IrMUsers) sessionFactory.getCurrentSession().get(IrMUsers.class,Integer.parseInt(userId));
			log.debug("Inside UserDaoImpl ::getById ::  IrMUsers " + irMUsers);
			if (irMUsers == null) {

				log.debug("get successful, no instance found");
				usersResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				usersResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);

			} else {
				log.debug("get successful, instance found"); 

				userBean.setUserid(irMUsers.getUserid());
				userBean.setFirstName(irMUsers.getFirstName());
				userBean.setLastName(irMUsers.getLastName());
				userBean.setUsername(decrypt(irMUsers.getUsername()));
				userBean.setUserPhoto(irMUsers.getUserPhoto());
				userBean.setUserCode(irMUsers.getUserCode());
				userBean.setRoleId(irMUsers.getIrMroles().getRoleid());						

				userBean.setPassword((String) irMUsers.getPassword());

				userBean.setContactno(decrypt(irMUsers.getContactno()));
				userBean.setEmailAddress(irMUsers.getEmailAddress());

				userBean.setCreatedDate(irMUsers.getCreatedDate());				
				userBean.setModifiedDate(irMUsers.getModifiedDate());

				userBean.setInternalUserCode(irMUsers.getInternalUserCode());
				userBean.setAdminId(irMUsers.getAdminId()); 
				userBean.setUserid(irMUsers.getUserid());

				userBean.setIsactive(irMUsers.getIsactive());
				
				
				Query userBankQuery= session.createQuery("FROM IrMUsersBank userBank WHERE userBank.irMUsers.userid=?");
				userBankQuery.setParameter(0, userBean.getUserid());
				
				/*Criteria criteria = session.createCriteria(IrMUsersBank.class ,"usersBank");
				criteria.createAlias("usersBank.irMUsers","user");
				criteria.add(Restrictions.eq("user.userid",userBean.getUserid()));*/
				List<IrMUsersBank>  userBankList = userBankQuery.list();

				if(userBankList.isEmpty()){

					errMsgs.add("Empty : This UserId Not contains BankId and BankBranches");

				}else{

					//==============================================
					List<UserBankBean> bankList = new ArrayList<UserBankBean>();
					
					for(int i=0;i<userBankList.size();i++){
						
						UserBankBean userBank = new UserBankBean();
						userBank.setIsDefault(userBankList.get(i).getIsdefault());
						userBank.setBranchId(userBankList.get(i).getBank().getBankId());
						
						bankList.add(userBank);
						
					}
					
					
					UserBankBean [] userBankBean = new UserBankBean[bankList.size()];
					
					userBean.setUserBank(bankList.toArray(userBankBean));
					
					
					
					/*Criteria criteria1 = session.createCriteria(IrMUsersBank.class ,"usersBank");
					criteria1.createAlias("usersBank.irMUsers", "user");
					///		criteria.createAlias("user.userid", "Users");
					criteria1.add(Restrictions.eq("user.userid",irMUsers.getUserid()));
					//		criteria1.add(Restrictions.eq("user.isactive",1 ));
					List<IrMUsersBank> lst = criteria1.list();
					
					
					

					log.debug(" Inside of OneShow : List Set of bankBranches : "+lst.toString() + " : size :" +lst.size());

					Long dup_bankBranchId_array[] = new Long[lst.size()];


					int k=0;
					for (IrMUsersBank element :lst) {

						if(element.getIsactive()==1){
							dup_bankBranchId_array[k]=element.getIrSBankbranch().getBranchId();
							
						}
						++k;
					}

					log.debug("Inside of Set of bankBranches  array[]: "+lst.toString() + " : size :" +lst.size());

					userBean.setBankBranchId_array(dup_bankBranchId_array);

					errMsgs.add("Message : showOne : size of banks list :"+dup_bankBranchId_array.length +" branches :"+dup_bankBranchId_array);

					//==============================================

					errMsgs.add("User : This UserId contains BankId and BankBranches");
*/
					/*for (IrMUsersBank irMUsersBank : userBankList) {

					//	BankBranchInfo bankBranchInfo = new BankBranchInfo();

						IrSBankBranch b = new IrSBankBranch();

						b =(IrSBankBranch) getMyCurrentSession().get(IrSBankBranch.class, irMUsersBank.getIrSBankbranch().getBranchId());

			///			userBean.setBankBranchId(irMUsersBank.getIrSBankbranch().getBranchId());
						userBean.setParentBankId(b.getBank().getBankId());
						userBean.setBankBranchLocation(b.getBranchLocation());
						userBean.setBankBranchId(b.getBranchId());

					}*/

				}

				listUserBean.add(userBean);				

				usersResponseDetails.setUserBeans(listUserBean);
				usersResponseDetails.setErrMsgs(errMsgs);
				usersResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				usersResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				usersResponseDetails.setValidationSuccess(true);
			}

		} catch (RuntimeException re) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);			
			log.error("getUser failed", re);	
			throw new HibernateException(re);
		}
		return usersResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails validateUser(UserBean userBean)throws BusinessException
	{
	IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

	log.debug("Validating IrMUser instance"+userBean);
	 	
	List<String> errMsgs = new ArrayList<String>();

	IrMUsers userInfo = null;

	try {
		Session session = null;
		session = sessionFactory.getCurrentSession();
		String username=encrypt(userBean.getUsername());
		String generatedSecuredPasswordHash=generateHash(userBean.getPassword());
		String password=encrypt(generatedSecuredPasswordHash);
		
	Query query = session.createQuery("from IrMUsers irMUser where irMUser.username=? and irMUser.password=?");

	query.setParameter(0,username);
	query.setParameter(1,password);

	/*query.setParameter(1,1);*/

	userInfo =(IrMUsers) query.uniqueResult();

	if(userInfo!=null)
	{

	if(userInfo.getIsactive()==1)
	{


	Query roles = session.createQuery("from IrMRoles irMRoles where irMRoles.roleid=? and irMRoles.isactive=?");

	roles.setParameter(0,userInfo.getIrMroles().getRoleid());
	roles.setParameter(1,1);

	List<IrMRoles> rolesList=roles.list();


	if(rolesList.size()>0)
	{
	userBean.setUserid(userInfo.getUserid());
	userBean.setSiteId(userInfo.getSite().getSiteId().toString());
	userBean.setRoleId(userInfo.getIrMroles().getRoleid());

	usersResponseDetails.setUserBean(userBean);
	//fetchMenuForUser(userInfo,usersResponseDetails);
	if(usersResponseDetails.getStatusCode()!=IRPlusConstants.ERR_CODE)
	{
	usersResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
	usersResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
	}
	else
	{


	usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
	usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
	}


	}
	else
	{
	errMsgs.add("Validation Error :: Please Enter Valid Username Password");
	usersResponseDetails.setErrMsgs(errMsgs);
	usersResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
	usersResponseDetails.setStatusMsg(IRPlusConstants.INVALID_CREDENTIALS);

	}


	}
	else
	{
	errMsgs.add("Validation Error :: Please Enter Valid Username Password");
	usersResponseDetails.setErrMsgs(errMsgs);
	usersResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
	usersResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);

	}





	}
	else
	{
	errMsgs.add("Validation Error :: Please Enter Valid Username Password");
	usersResponseDetails.setErrMsgs(errMsgs);
	usersResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
	usersResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);

	}
	session.flush();
	}


	catch (Exception re) {

	usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
	usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
	log.error("Exception in dvalidateUser ",re);
	}

	return usersResponseDetails;
	}

	@Override
	public IRPlusResponseDetails fetchMenu(int roleId) throws BusinessException
	{
		log.debug("fetchMenuForUser "+roleId);

		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();
		MenuInfo menuInfo = null;
		ModuleBean subMenuInfo = null;

		List<MenuInfo> userMenus = new ArrayList<MenuInfo>();
		IrMModulemenus modulemenu = null;
		IrMMenus menu = null;
		
		try {
			Session session = null;
			session = sessionFactory.getCurrentSession();
			
					

			Query query = session.createQuery("select distinct roleperm.irMModulemenus.irMMenus from IrMRolepermissions roleperm where roleperm.irMRoles.roleid=? and roleperm.irMRoles.isactive=?");
		
			query.setParameter(0,roleId);
			query.setParameter(1,1);
			
			
			
			
			List <IrMMenus> menuList = query.list();
			
			
			for(IrMMenus irMMenus:menuList){
				menuInfo = new MenuInfo();
				menuInfo.setMenuName(irMMenus.getMenuname());
				menuInfo.setModulepath(irMMenus.getModulepath());
				menuInfo.setMenuid(irMMenus.getMenuid());
				menuInfo.setMenuicon(irMMenus.getMenuicon());
				
			/*}
			
			
			for (Iterator <IrMMenus> menuListIt = menuList.iterator(); menuListIt.hasNext();) 
			{
				menu = menuListIt.next();
				menuInfo = new MenuInfo();
				menuInfo.setMenuName(menu.getMenuname());
				menuInfo.setModulepath(menu.getModulepath());
				menuInfo.setMenuid(menu.getMenuid());
				menuInfo.setMenuicon(menu.getMenuicon());*/

				
				
				//List<IrMModulemenus> moduleMenus = (List<IrMModulemenus>) menu.getIrMModulemenuses();
				
				List<IrMModulemenus> al=new ArrayList<IrMModulemenus>();  
				al.addAll(irMMenus.getIrMModulemenuses());
				
				//Set<IrMModulemenus> moduleMenus =;
								
				
				Iterator<IrMModulemenus> moduleMenusIt = al.iterator();
				
				List<ModuleBean> subMenuList = new ArrayList<ModuleBean>();
				while (moduleMenusIt.hasNext()) {
					System.out.println();
					modulemenu =  moduleMenusIt.next();
					subMenuInfo = new ModuleBean();
					subMenuInfo.setModulename(modulemenu.getIrMModules().getModulename());
					subMenuInfo.setModulepath(modulemenu.getIrMModules().getModulepath());
					subMenuInfo.setModuleid(modulemenu.getIrMModules().getModuleid());
					subMenuList.add(subMenuInfo);
				}
				
				
				 Collections.sort(subMenuList,ModuleBean.StuRollno);
				menuInfo.setSubMenu(subMenuList);
				userMenus.add(menuInfo);


			}
			usersResponseDetails.setUserMenus(userMenus);

			session.flush();


		}
		catch(Exception e)
		{
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception in fetchMenuForUser ",e);
		}
		return usersResponseDetails;


	}


	@Override
	public IRPlusResponseDetails createUserBank(UserBankBean[] userBean) throws BusinessException {
		log.debug("Inside of userDaoImpl :: createUser()");

		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();
		try{
			Session session = null;
			session = sessionFactory.getCurrentSession();
			
			UserBankBean[] userBank=null;
	
			IrMUsersBank irUserBank=new IrMUsersBank();
			IRSBank bank = null;
			IrMUsers user = new IrMUsers();
			IrMUsers user1 = new IrMUsers();
			
			IrMUsers addUser=null;
			
				
			
			if(userBean.length>0){
				addUser=(IrMUsers) session.get(IrMUsers.class,userBean[0].getUserId());
				
				for(int i=0;i<userBean.length;i++){
					
					
					log.debug("admin Id : "+userBean[i].getAdminId());
					log.debug("User Id : "+userBean[0].getUserId());
					log.debug("Branch Id : "+userBean[i].getBranchId());
					log.debug("isactive : "+userBean[i].getIsactive());
					log.debug("is default: "+userBean[i].getIsDefault());
					
					IrMUsersBank userbank = new IrMUsersBank();
					bank = new IRSBank();
					userbank.setAdminId(userBean[i].getAdminId());
					userbank.setIrMUsers(addUser);
					bank.setBankId(userBean[i].getBranchId());
					userbank.setBank(bank);
					userbank.setIsactive(userBean[i].getIsactive());
					userbank.setIsdefault(userBean[i].getIsDefault());
				
					session.save(userbank);
					
					
				}
			}
			usersResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}catch(Exception e)
		{
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception in fetchMenuForUser ",e);
		}

		return usersResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails updateUserBank(UserBankBean[] userBean) throws BusinessException {
		log.debug("Inside of userDaoImpl :: createUser()");

		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();
		try{
			
			deleteExistingUserbank(userBean[0].getUserId());
			Session session = null;
			session = sessionFactory.getCurrentSession();
			
			UserBankBean[] userBank=null;
	
			IrMUsersBank irUserBank=new IrMUsersBank();
			IRSBank bank = null;
			IrMUsers user = new IrMUsers();
			IrMUsers user1 = new IrMUsers();
			
			IrMUsers addUser=null;
			
				
			
			if(userBean.length>0){
				addUser=(IrMUsers) session.get(IrMUsers.class,userBean[0].getUserId());
				
				for(int i=0;i<userBean.length;i++){
					
					IrMUsersBank userbank = new IrMUsersBank();
					bank = new IRSBank();
					userbank.setAdminId(userBean[i].getAdminId());
					userbank.setIrMUsers(addUser);
					bank.setBankId(userBean[i].getBranchId());
					userbank.setBank(bank);
					userbank.setIsactive(userBean[i].getIsactive());
					userbank.setIsdefault(userBean[i].getIsDefault());
				
					session.save(userbank);
					
					
					session.flush();
				}
			}
			usersResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}catch(Exception e)
		{
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception in fetchMenuForUser ",e);
		}

		return usersResponseDetails;
	}
	

	
	@Override
	public IRPlusResponseDetails showAllUsers() throws BusinessException {

		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		List<String> errMsgs = new ArrayList<String>();
		
		Session session = sessionFactory.getCurrentSession();

		log.debug("findAllUsers() ::Showing All Users List");
		try {
			
			
			
			
			//Criteria criteria = session.createCriteria("com.irplus.dao.hibernate.entities.IrMUsers");

			//adding code here 
		//	criteria.add(Restrictions.());
			
			Query users = session.createQuery("from IrMUsers users where users.isactive!=?");
			users.setParameter(0,2);
			
			
			List<IrMUsers> listUsers = users.list();
			log.debug("Shown All Users successful, result size: " + listUsers.size());

			List<UserBean> listUserBean = new ArrayList<UserBean>();

			for (IrMUsers irMUsers : listUsers) {
				
			//	if(irMUsers.getIsactive()!=null && irMUsers.getIsactive()!=2){

				UserBean userBean = new UserBean();

				log.debug("get successful, instance found"); 

				userBean.setUserid(irMUsers.getUserid());
				
				if(irMUsers.getFirstName()!=null){
				userBean.setFirstName(irMUsers.getFirstName());}else{
					userBean.setFirstName("");
				}
				if(irMUsers.getLastName()!=null){
				userBean.setLastName(irMUsers.getLastName());}else{
					userBean.setLastName("");
				}
				if(irMUsers.getUsername()!=null){
				userBean.setUsername(irMUsers.getUsername());}else{
					userBean.setUsername("");
				}
				if(irMUsers.getUserPhoto()!=null){
				userBean.setUserPhoto(irMUsers.getUserPhoto());}else{
					userBean.setUserPhoto("");
				}
				if(irMUsers.getUserCode()!=null){
				userBean.setUserCode(irMUsers.getUserCode());}else{
					userBean.setUserCode("");
				}
				if(irMUsers.getIrMroles().getRoleid()!=null){
					userBean.setRoleId(irMUsers.getIrMroles().getRoleid()); }else{
						userBean.setRoleId(0);
					}
				if(irMUsers.getIrMroles().getRolename()!=null){
					userBean.setRoleName(irMUsers.getIrMroles().getRolename()); }else{
						userBean.setRoleName("");
					}	
				
				
				if((String)irMUsers.getPassword()!=null){
				userBean.setPassword((String)irMUsers.getPassword());}else{
					userBean.setPassword("");	
				}
				if(irMUsers.getContactno()!=null){
				userBean.setContactno(irMUsers.getContactno());}else{
					userBean.setContactno("");
				}
				if(irMUsers.getEmailAddress()!=null){
				userBean.setEmailAddress(irMUsers.getEmailAddress());}else{
					userBean.setEmailAddress("");
				}
				
				
				if(irMUsers.getCreatedDate()!=null){
				userBean.setCreatedDate(irMUsers.getCreatedDate());}else{
				}
				
				
				if(irMUsers.getModifiedDate()!=null){
				userBean.setModifiedDate(irMUsers.getModifiedDate());}else{
					//userBean.setModifiedDate(0);
				}
				if(irMUsers.getInternalUserCode()!=null){
				userBean.setInternalUserCode(irMUsers.getInternalUserCode());}else{
					userBean.setInternalUserCode("");
				}
				if(irMUsers.getAdminId()!=null){
				userBean.setAdminId(irMUsers.getAdminId());}else{
					userBean.setAdminId(0);
				} 
				if(irMUsers.getIsactive()!=null){
				userBean.setIsactive(irMUsers.getIsactive());
				}else{
					userBean.setIsactive(0);
				}
				/*Adding thd  Defoult Bank and Other Banks*/

				Criteria criteria2 = session.createCriteria(IrMUsersBank.class , "userBank");
				criteria2.createAlias("userBank.irMUsers", "user");
				criteria2.add(Restrictions.eq("user.userid", irMUsers.getUserid()));

				List<IrMUsersBank> usersBankList = criteria2.list();
				boolean flag = false;

				if (usersBankList.isEmpty()) {
					errMsgs.add(" Message Alert :: id : "+irMUsers.getUserid()+" : Name :"+irMUsers.getUsername()+" ..."
							+ "This User Not Contains Any user Bank Branches");
					flag = true;
				}else {

					//	Long[] bankBranches = new Long[usersBankList.size()];

					Map<Long, String> branchid_branchLoc = new HashMap<Long, String>(); 

					IrSBankBranch bb=null;

					for (IrMUsersBank irMUsersBank : usersBankList) {

						branchid_branchLoc.put(irMUsersBank.getIrSBankbranch().getBranchId(),irMUsersBank.getIrSBankbranch().getBranchLocation());
						//		userBean.setParentBankId(irMUsersBank.getIrSBankbranch().getBranchId());
						
						if(irMUsersBank.getIsdefault()==1){

							bb=(IrSBankBranch)session.get(IrSBankBranch.class,irMUsersBank.getIrSBankbranch().getBranchId() );
							userBean.setParentBankId(bb.getBank().getBankId());
							if(bb.getBank().getBankName()!=null){
							userBean.setDefaultBankName(bb.getBank().getBankName());
							}else{userBean.setDefaultBankName("");}
						}
					}
					if(!branchid_branchLoc.isEmpty()){
					userBean.setBranch_ids_Locations(branchid_branchLoc);
					if(branchid_branchLoc.keySet().size()!=0){
						userBean.setOtherBanks(branchid_branchLoc.keySet().size());}else{userBean.setOtherBanks(0);}
					}else{userBean.setOtherBanks(0);}					
				}	

				/*If user not contains userBank entries then checking bank-getting details*/
				if (flag) {
					Criteria criteria3 = session.createCriteria(IRSBank.class , "bank");

					criteria3.createAlias("bank.irMUsers_id" ,"user");
					criteria3.add(Restrictions.eq("user.userid", irMUsers.getUserid()));

					List<IRSBank> bankList = criteria3.list();

					if(!bankList.isEmpty()){

						for (IRSBank irsBank : bankList) {					
							userBean.setParentBankId(irsBank.getBankId());
							userBean.setDefaultBankName(irsBank.getBankName());							
						}	

					}else{
						errMsgs.add("Message Alert : id : "+ irMUsers.getUserid()+" Name : "+ irMUsers.getUsername()+" : "
								+ "This user Not Contains Default Bank");
					}
				}

				//userBean.setParent_bank_defoult();

				listUserBean.add(userBean);		
				usersResponseDetails.setErrMsgs(errMsgs);
			//} 
		}
			usersResponseDetails.setUserBeans(listUserBean);
			usersResponseDetails.setErrMsgs(errMsgs);
			usersResponseDetails.setRecordsFiltered(listUserBean.size());
			usersResponseDetails.setRecordsTotal(listUserBean.size());
			usersResponseDetails.setValidationSuccess(true);
			usersResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);			
			session.flush();
		} catch (RuntimeException re) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);

			log.error("findAllUsers failed", re);
			throw new HibernateException(re);
		}				
		return usersResponseDetails;
	}

	
	

	private void deleteExistingUserbank(int userId) 
	{
		log.info("Inside deleteExistingUserBank userId :"+userId);

		Session session = null;

		try
		{
			session = sessionFactory.getCurrentSession();

			Query query = session.createQuery("delete from IrMUsersBank userBank where userBank.irMUsers.userid=:userId");

			query.setParameter("userId",userId);

			query.executeUpdate();
		}
		catch(Exception e)
		{
			log.error("Exception in deleteExistingUserBank",e);

			throw new HibernateException(e);
		}	

	}


	@Override
	public IRPlusResponseDetails getOtherBank(String bankId) throws BusinessException {
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		log.debug("inside of getAll_Role_Banks()");

		List<String> errMsg = new ArrayList<String>();

		

		ArrayList<BankInfo> bankList = new ArrayList<>();

		UserBean userBean = new UserBean();
		Session session = null;
		session = sessionFactory.getCurrentSession();
		
		try {

			
			Query banks = session.createQuery("from IRSBank bank where bank.bankId!=? and bank.isactive=?");
			banks.setParameter(0,new Long(bankId));
			banks.setParameter(1,1);
					
			List<IRSBank> banksList = banks.list();

			for (IRSBank irSBank:banksList) {

				BankInfo b = new BankInfo();
				b.setBankId(irSBank.getBankId());
				b.setBankName(irSBank.getBankName());
				bankList.add(b);
			}

			  userBean.setOtherbankList(bankList);

				if(!bankList.isEmpty()){

				usersResponseDetails.setUserBean(userBean);

				errMsg.add("success :: success fully added userbean : UserProfile rendering obeject , it contains role names ,bank/branch defoult values ");
				usersResponseDetails.setErrMsgs(errMsg);		
				usersResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				usersResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				usersResponseDetails.setValidationSuccess(true);

			}else{
				errMsg.add("Error :: Not added userbean : UserProfile rendering obeject , it not contains role names ,bank/branch defoult values not available ");
				usersResponseDetails.setErrMsgs(errMsg);				
				usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				usersResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);

			}
		} catch (RuntimeException re) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);

			log.error("findAllUsers failed", re);
			throw new HibernateException(re);
		}				
		return usersResponseDetails;

	}


	@Override
	public IRPlusResponseDetails getUserBank(String userId) throws BusinessException {
		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();

		log.debug("inside of getUserBank()");

		List<String> errMsg = new ArrayList<String>();

		ArrayList<RoleBean> roleArrayList = new ArrayList<>();

		ArrayList<BankInfo> bankList = new ArrayList<>();

		UserBean userBean = new UserBean();
		Session session = null;
		session = sessionFactory.getCurrentSession();
		
		try {

			Query roles = session.createQuery("from IrMRoles irMRoles where irMRoles.isrestricted=? and irMRoles.isactive=?");
			roles.setParameter(0,1);
			roles.setParameter(1,1);

			List<IrMRoles> listRoles = roles.list();

			for (IrMRoles irMRoles : listRoles){

				RoleBean r = new RoleBean();
				
				if(irMRoles .getIsactive() !=0){

					r.setRoleid(irMRoles.getRoleid());
					r.setRolename(irMRoles.getRolename());
					r.setStatusId(irMRoles.getIsactive());

					r.setBankVisibility(irMRoles.getBankVisibility());
					r.setIsrestricted(irMRoles.getIsrestricted());
					r.setUserId(irMRoles.getIrMUsers().getUserid());

					roleArrayList.add(r);

				}
			}

			userBean.setRoleArrayList(roleArrayList);
			errMsg.add("Success : Role list added :" +roleArrayList.size());
			
			
			// for getting list of default bank and branch from bankbranch table

			/*Criteria criteria1 = sessionFactory.getCurrentSession().createCriteria(IrSBankBranch.class);

			List<IrSBankBranch> listBbranch = criteria1.list();

			for (IrSBankBranch irSBankBranch : listBbranch) {

				BankInfo b = new BankInfo();

				b.setBankId(irSBankBranch.getBank().getBankId());
				b.setBankName(irSBankBranch.getBank().getBankName());
				b.setBranchId(irSBankBranch.getBranchId());
				b.setBranchLocation(irSBankBranch.getBranchLocation());
				b.setBranchOwnCode(irSBankBranch.getBranchOwnCode());
				b.setIsdefault(irSBankBranch.getIsdefault());

				bankList.add(b);
			}

			userBean.setBankList(bankList);

			*/
			
		//	Criteria banks = sessionFactory.getCurrentSession().createCriteria(IRSBank.class);

			int usrId = Integer.parseInt(userId);
			Query banks = session.createQuery("from IrMUsersBank userBank where userBank.irMUsers.userid=? and userBank.isactive=?");
			
			banks.setParameter(0,usrId);
			banks.setParameter(1,1);
			List<IrMUsersBank> banksList = banks.list();

			for (IrMUsersBank irSBank:banksList) {

				BankInfo b = new BankInfo();
				b.setBankId(irSBank.getBank().getBankId());
				b.setBankName(irSBank.getBank().getBankName());
				bankList.add(b);
			}

			userBean.setBankList(bankList);

			
			if(!bankList.isEmpty() && ! roleArrayList.isEmpty() ){

				usersResponseDetails.setUserBean(userBean);

				errMsg.add("success :: success fully added userbean : UserProfile rendering obeject , it contains role names ,bank/branch defoult values ");
				usersResponseDetails.setErrMsgs(errMsg);		
				usersResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				usersResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				usersResponseDetails.setValidationSuccess(true);

			}else{
				errMsg.add("Error :: Not added userbean : UserProfile rendering obeject , it not contains role names ,bank/branch defoult values not available ");
				usersResponseDetails.setErrMsgs(errMsg);				
				usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				usersResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);

			}
		} catch (RuntimeException re) {

			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);

			log.error("findAllUsers failed", re);
			throw new HibernateException(re);
		}				
		return usersResponseDetails;

	}
	
	
	
	@Override
	public IRPlusResponseDetails changeDefaultBank(UserBankBean[] userBean) throws BusinessException {
		log.debug("Inside of userDaoImpl :: createUser()");

		IRPlusResponseDetails usersResponseDetails = new IRPlusResponseDetails();
		try{
			
		
			Session session = null;
			session = sessionFactory.getCurrentSession();
			
		
			Query banks = session.createQuery("from IrMUsersBank irMUsersBank where irMUsersBank.irMUsers.userid=?");
			banks.setParameter(0,userBean[0].getUserId());

			List<IrMUsersBank> userBankList = banks.list();
			
			//deleteExistingUserbank(userBean[0].getUserId());
		
			
			for(int i=0;i<userBankList.size();i++){
				
				
			Long bankId=new Long(userBankList.get(i).getBank().getBankId());
			Long uiBranchId= new Long(userBean[0].getBranchId());
			
			log.debug("branch id from db"+bankId);
			log.debug("branch id from UI"+userBean[0].getBranchId());
			
			byte isDefault=1;
			byte isDefault1=0;
			
				if(bankId.equals(uiBranchId)) {
					
					
					 Query q = session.createQuery("update IrMUsersBank irMUsersBank set irMUsersBank.isdefault=? where irMUsersBank.bank.bankId=?");
					 q.setParameter(0,isDefault);
					 q.setParameter(1,uiBranchId);
					 q.executeUpdate();
					
					   
										
				}else {
					
					Query q = session.createQuery("update IrMUsersBank irMUsersBank set irMUsersBank.isdefault=? where irMUsersBank.bank.bankId=?");
					    q.setParameter(0,isDefault1);
						 q.setParameter(1,userBankList.get(i).getBank().getBankId());
						 q.executeUpdate();
					
				}
				
				
			}
			
			/*UserBankBean[] userBank=null;*/
	
			/*if(userBean.length>0){
				addUser=(IrMUsers) session.get(IrMUsers.class,userBean[0].getUserId());
				
				for(int i=0;i<userBean.length;i++){
					
				
					
					
					IrMUsersBank userbank = new IrMUsersBank();
					branch = new IrSBankBranch();
					userbank.setAdminId(userBean[i].getAdminId());
					userbank.setIrMUsers(addUser);
					branch.setBranchId(userBean[i].getBranchId());
					userbank.setIrSBankbranch(branch);
					userbank.setIsactive(userBean[i].getIsactive());
					userbank.setIsdefault(userBean[i].getIsDefault());
				
					session.save(userbank);
					
					
					session.flush();
				}
			}*/
			usersResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}catch(Exception e)
		{
			usersResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			usersResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception in fetchMenuForUser ",e);
		}

		return usersResponseDetails;
	}

	
	@Override
	public IRPlusResponseDetails AutocompleteUsername(String term,String siteId) throws BusinessException
	{
		Session session = null;
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		session = sessionFactory.getCurrentSession();
		BankInfo userList = null;
		 String data;
		 List<BankInfo> list = new ArrayList<BankInfo>();
		 List<BankInfo> bankInfo=new ArrayList<BankInfo>();
		try {
		
/*			Query banks = session.createQuery("select user.firstName+user.lastName from IrMUsers user where user.firstName+user.lastName LIKE ?");
*/
			Query banks = session.createQuery("select user.firstName from IrMUsers user where user.firstName LIKE ? and user.isactive!=?");

			banks.setParameter(0, term + "%");	
			banks.setParameter(1,2);
			List<IrMUsers> userListval=banks.list();
		
		int count=0;
	    if(userListval.size()>0){
		for(int i=0;i<userListval.size();i++){
			userList = new BankInfo();
			Object rows = (Object) userListval.get(i);

			userList.setUserName(rows);
			bankInfo.add(userList);
			irPlusResponseDetails.setList(bankInfo);
			
		}
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}
		else
		{
			irPlusResponseDetails.setValidationSuccess(false);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
		}
		}
	
	
		catch(Exception e)
		{
		
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
		
		
	}
	//Encryption & Decryption
	 public   byte[] aesEncrypt(String message, SecretKey aesKey, GCMParameterSpec gcmParamSpec, byte[] aadData) {
         Cipher c = null ;

         try {
                 c = Cipher.getInstance(ALGO_TRANSFORMATION_STRING); // Transformation specifies algortihm, mode of operation and padding
         }catch(NoSuchAlgorithmException noSuchAlgoExc) {System.out.println("Exception while encrypting. Algorithm being requested is not available in this environment " + noSuchAlgoExc); System.exit(1); }
          catch(NoSuchPaddingException noSuchPaddingExc) {System.out.println("Exception while encrypting. Padding Scheme being requested is not available this environment " + noSuchPaddingExc); System.exit(1); }

         
         try {
             c.init(Cipher.ENCRYPT_MODE, aesKey, gcmParamSpec, new SecureRandom()) ;
         } catch(InvalidKeyException invalidKeyExc) {System.out.println("Exception while encrypting. Key being used is not valid. It could be due to invalid encoding, wrong length or uninitialized " + invalidKeyExc) ; System.exit(1); }
          catch(InvalidAlgorithmParameterException invalidAlgoParamExc) {System.out.println("Exception while encrypting. Algorithm parameters being specified are not valid " + invalidAlgoParamExc) ; System.exit(1); }

        try { 
             c.updateAAD(aadData) ; // add AAD tag data before encrypting
         }catch(IllegalArgumentException illegalArgumentExc) {System.out.println("Exception thrown while encrypting. Byte array might be null " +illegalArgumentExc ); System.exit(1);} 
         catch(IllegalStateException illegalStateExc) {System.out.println("Exception thrown while encrypting. CIpher is in an illegal state " +illegalStateExc); System.exit(1);} 
         catch(UnsupportedOperationException unsupportedExc) {System.out.println("Exception thrown while encrypting. Provider might not be supporting this method " +unsupportedExc); System.exit(1);} 
        
        byte[] cipherTextInByteArr = null ;
        try {
             cipherTextInByteArr = c.doFinal(message.getBytes()) ;
        } catch(IllegalBlockSizeException illegalBlockSizeExc) {System.out.println("Exception while encrypting, due to block size " + illegalBlockSizeExc) ; System.exit(1); }
          catch(BadPaddingException badPaddingExc) {System.out.println("Exception while encrypting, due to padding scheme " + badPaddingExc) ; System.exit(1); }

        return cipherTextInByteArr ;
 }


 public  byte[] aesDecrypt(byte[] encryptedMessage, SecretKey aesKey, GCMParameterSpec gcmParamSpec, byte[] aadData) {
        Cipher c = null ;
 
        try {
            c = Cipher.getInstance(ALGO_TRANSFORMATION_STRING); // Transformation specifies algortihm, mode of operation and padding
         } catch(NoSuchAlgorithmException noSuchAlgoExc) {System.out.println("Exception while decrypting. Algorithm being requested is not available in environment " + noSuchAlgoExc); System.exit(1); }
          catch(NoSuchPaddingException noSuchAlgoExc) {System.out.println("Exception while decrypting. Padding scheme being requested is not available in environment " + noSuchAlgoExc); System.exit(1); }  

         try {
             c.init(Cipher.DECRYPT_MODE, aesKey, gcmParamSpec, new SecureRandom()) ;
         } catch(InvalidKeyException invalidKeyExc) {System.out.println("Exception while encrypting. Key being used is not valid. It could be due to invalid encoding, wrong length or uninitialized " + invalidKeyExc) ; System.exit(1); }
          catch(InvalidAlgorithmParameterException invalidParamSpecExc) {System.out.println("Exception while encrypting. Algorithm Param being used is not valid. " + invalidParamSpecExc) ; System.exit(1); }

         try {
             c.updateAAD(aadData) ; // Add AAD details before decrypting
         }catch(IllegalArgumentException illegalArgumentExc) {System.out.println("Exception thrown while encrypting. Byte array might be null " +illegalArgumentExc ); System.exit(1);}
         catch(IllegalStateException illegalStateExc) {System.out.println("Exception thrown while encrypting. CIpher is in an illegal state " +illegalStateExc); System.exit(1);}
         
         byte[] plainTextInByteArr = null ;
         try {
             plainTextInByteArr = c.doFinal(encryptedMessage) ;
         } catch(IllegalBlockSizeException illegalBlockSizeExc) {System.out.println("Exception while decryption, due to block size " + illegalBlockSizeExc) ; System.exit(1); }
          catch(BadPaddingException badPaddingExc) {System.out.println("Exception while decryption, due to padding scheme " + badPaddingExc) ; System.exit(1); }

         return plainTextInByteArr ;
 }
	
	//pwd hashing
	 private  String generateHash(String password) throws NoSuchAlgorithmException, InvalidKeySpecException
		{
			int iterations = 1000;
			char[] chars = password.toCharArray();
			byte[] salt = key.getBytes();
			
			PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
			SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			byte[] hash = skf.generateSecret(spec).getEncoded();
			return toHex(salt) + ":" + toHex(hash);
					
		}	
		private  String getSalt() throws NoSuchAlgorithmException
		{
			SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
			byte[] salt = new byte[16];
			sr.nextBytes(salt);
			return salt.toString();
		}
		
		private   String toHex(byte[] array) throws NoSuchAlgorithmException
		{
			BigInteger bi = new BigInteger(1, array);
			String hex = bi.toString(16);
			int paddingLength = (array.length * 2) - hex.length();
			if(paddingLength > 0)
			{
				return String.format("%0"  +paddingLength + "d", 0) + hex;
			}else{
				return hex;
			}
		}
		
		private   byte[] fromHex(String hex) throws NoSuchAlgorithmException
		{
			byte[] bytes = new byte[hex.length() / 2];
			for(int i = 0; i<bytes.length ;i++)
			{
				bytes[i] = (byte)Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
			}
			return bytes;
		}
		
		
		public  String encrypt(String txt) {
			String encryted=null;
			try {
				 String messageToEncrypt = txt;
				 byte[] aadData = "random".getBytes() ;
				 byte iv[]=initVector.getBytes(); 
				 SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"),"AES");
				 GCMParameterSpec gcmParamSpec = new GCMParameterSpec(TAG_BIT_LENGTH,iv) ;      
			     byte[] encryptedText = aesEncrypt(messageToEncrypt, skeySpec,gcmParamSpec,aadData) ;          
			     encryted=Base64.getEncoder().encodeToString(encryptedText);   
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return encryted;
		}
	 
		public  String decrypt(String txt) {
			
			String decryted=null;
			try {
				byte[] encryptedText = Base64.getDecoder().decode(txt);
				 byte[] aadData = "random".getBytes() ;
				 byte iv[]=initVector.getBytes(); 
				 SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"),"AES");
				 GCMParameterSpec gcmParamSpec = new GCMParameterSpec(TAG_BIT_LENGTH, iv) ;      
		         byte[] decryptedText = aesDecrypt(encryptedText,skeySpec, gcmParamSpec, aadData) ; // Same key, IV and GCM Specs for decryption as used for encryption.
        
			     decryted= new String(decryptedText);   
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	            
	            

	        
	            
			return decryted;
		}
}
