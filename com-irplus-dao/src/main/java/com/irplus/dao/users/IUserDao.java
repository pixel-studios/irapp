package com.irplus.dao.users;

import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.UserBankBean;
import com.irplus.dto.UserFilterInfo;
import com.irplus.dto.users.UserBean;
import com.irplus.util.BusinessException;

public interface IUserDao {
		
    public IRPlusResponseDetails createUser(UserBean userBeanInfo) throws BusinessException;
    public IRPlusResponseDetails createUserBank(UserBankBean[] userBeanInfo) throws BusinessException;
    public IRPlusResponseDetails updateUserBank(UserBankBean[] userBean) throws BusinessException;

	public IRPlusResponseDetails getUserById(String userId) throws BusinessException;		

	public IRPlusResponseDetails updateUser(UserBean userBeanInfo) throws BusinessException;

	public IRPlusResponseDetails deleteUserById(String userId) throws BusinessException;
		
	public IRPlusResponseDetails findAllUsers(String siteId) throws BusinessException;
	public IRPlusResponseDetails getOtherBank(String bankId) throws BusinessException;
	public IRPlusResponseDetails getUserBank(String userId)  throws BusinessException;
	public IRPlusResponseDetails getAll_Role_Banks(String siteId) throws BusinessException;

	public IRPlusResponseDetails updateUserAndBank(UserBean userBeanInfo) throws BusinessException;

	public IRPlusResponseDetails createUserNoDup(UserBean userBean) throws BusinessException;

	public IRPlusResponseDetails updateStatusUser(UserBean userBeanInfo) throws BusinessException;
	
	//
	public IRPlusResponseDetails filterUsers(UserFilterInfo userFilterInfo) throws BusinessException ;
	
	public IRPlusResponseDetails validateUser(UserBean userBean) throws BusinessException;

	public IRPlusResponseDetails fetchMenu(int roleId) throws BusinessException;
	public IRPlusResponseDetails showAllUsers() throws BusinessException;
	public IRPlusResponseDetails changeDefaultBank(UserBankBean[] userBean) throws BusinessException;
	public IRPlusResponseDetails AutocompleteUsername(String term,String siteId) throws BusinessException; 
}
