package com.irplus.dao.fileprocessing.batchheaderrecord;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.hibernate.entities.IRSBank;
import com.irplus.dao.hibernate.entities.IrFPBatchControlRecord;
import com.irplus.dao.hibernate.entities.IrFPBatchHeaderRecord;
import com.irplus.dao.hibernate.entities.IrFPCCDAddendaRecord;
import com.irplus.dao.hibernate.entities.IrFPEntryDetailRecord;
import com.irplus.dao.hibernate.entities.IrFPFileHeaderRecord;
import com.irplus.dao.hibernate.entities.IrFPPaymentEntryRecord;
import com.irplus.dao.hibernate.entities.IrFPRemittanceEntryRecord;
import com.irplus.dao.hibernate.entities.IrSBankBranch;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.fileprocessing.BatchControlRecordBean;
import com.irplus.dto.fileprocessing.BatchHeaderRecordBean;
import com.irplus.dto.fileprocessing.CCDAddendaRecordBean;
import com.irplus.dto.fileprocessing.FileHeaderRecordBean;
import com.irplus.dto.fileprocessing.PaymentEntryDetailRecordBean;
import com.irplus.dto.fileprocessing.RemittanceEntryDetailRecordBean;
import com.irplus.dto.fileprocessing.WEBEntryDetailRecordBean;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;


@Component
@Transactional
public class BatchHeaderRecordDaoImpl implements BatchHeaderRecordDao{

	private static final Logger LOGGER = Logger.getLogger(BatchHeaderRecordDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;

	public Session getMyCurrentSession(){
			
			Session session =null;
			
			try {	
			
				session=sessionFactory.getCurrentSession();
			
			}catch (HibernateException e) {
				LOGGER.error("Exception raised DaoImpl :: At current session creation time ::"+e);
			}		
			return session; 
	}
	
	
	
	@Override
	public Boolean insertRecord(BatchHeaderRecordBean batchHeaderRecord) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean deleteRecord(BatchHeaderRecordBean batchHeaderRecord) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<BatchHeaderRecordBean> listAllRecords() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean updateRecord(BatchHeaderRecordBean batchHeaderRecord) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<BatchHeaderRecordBean> getByQuery(Integer Id) {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public IRPlusResponseDetails getRecordByBatchId(Integer fileId) throws BusinessException{
		LOGGER.info("Inside FileHeaderRecordDaoImpl");
		LOGGER.debug("GetBatchByFIleID FileHeaderRecordDaoImpl instances");
		
		
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
		

		try {
			
			List<BatchHeaderRecordBean> batchHeaderRecordBean = new ArrayList<BatchHeaderRecordBean>();
			List<WEBEntryDetailRecordBean> entryDetailRecord = new ArrayList<WEBEntryDetailRecordBean>();
			
			IrFPBatchHeaderRecord bhrList = (IrFPBatchHeaderRecord)getMyCurrentSession().get(IrFPBatchHeaderRecord.class,fileId);
			
			//List<IrFPFileHeaderRecord> list = criteria.list();
			
			if (bhrList!=null) {
				
							
				BatchHeaderRecordBean bhrBean = new BatchHeaderRecordBean();
				bhrBean.setBatchHeaderRecordId(bhrList.getBatchHeaderRecordId());
				bhrBean.setCompanyName(bhrList.getCompanyName());

				bhrBean.setCompanyIdentification(bhrList.getCompanyIdentification());
				bhrBean.setStandardEntryClass(bhrList.getStandardEntryClass()); 
				bhrBean.setCompanyEntryDescription(bhrList.getCompanyEntryDescription());
				bhrBean.setCompanyDescriptiveDate(bhrList.getCompanyDescriptiveDate());
				bhrBean.setEffectiveEntryDate(bhrList.getEffectiveEntryDate());
				bhrBean.setBatchNumber(bhrList.getBatchNumber());
				bhrBean.setTransactionCount(bhrList.getTransactionCount());
				bhrBean.setBatchAmount(bhrList.getBatchAmount());
				bhrBean.setCompanyDescriptiveDate(bhrList.getCompanyDescriptiveDate());
				bhrBean.setCustomerName(bhrList.getCustomer().getCompanyName());
				bhrBean.setScandocCount(bhrList.getScandocCount());
				bhrBean.setPaymentCount(bhrList.getPaymentCount());
				bhrBean.setRemittanceCount(bhrList.getRemittanceCount());
				bhrBean.setItemCount(bhrList.getItemCount());
				if(bhrList.getBatchImage()!=null){	
					bhrBean.setFrontImage(bhrList.getBatchImage());
					}
				if(bhrList.getRearTIFF()!=null){	
					
						 
						 bhrBean.setRearImage(bhrList.getRearTIFF());
					
					}
				if(bhrList.getBatchPNG()!=null){	
					try{
						// byte[] bFile = readBytesFromFile(bhrList.getBatchPNG());
						 
						 //String s = new sun.misc.BASE64Encoder().encode(bFile);
						 
						 bhrBean.setBatchPNG(bhrList.getBatchPNG());
					}catch(Exception e){
						
					}
					}
				
				
				Integer fileHeaderId=bhrList.getIrFPFileHeaderRecord().getFileHeaderRecordId();
				
				IrFPFileHeaderRecord fhrList = (IrFPFileHeaderRecord)getMyCurrentSession().get(IrFPFileHeaderRecord.class,fileHeaderId);

				if (fhrList!=null) {
					Query bankQuery = getMyCurrentSession().createQuery("from IRSBank bank where bank.bankId=:bankId ");
					
					bankQuery.setParameter("bankId",new Long(fhrList.getBank().getBankId()));
					List<IRSBank> bankList= bankQuery.list();
					
					if(bankList.size()>0){
						FileHeaderRecordBean fhrBean = new FileHeaderRecordBean();
						fhrBean.setBankName(bankList.get(0).getBankName());
						fhrBean.setBankLogo(bankList.get(0).getBankLogo());
						fhrBean.setAchFileName(fhrList.getAchFileName());
						fhrBean.setFileHeaderRecordId(fhrList.getFileHeaderRecordId());
						fhrBean.setFileType(fhrList.getFileType().getFiletypename());
						bhrBean.setFileHeaderRecordBean(fhrBean);
						
					}
				}
				

						Query edrQuery = getMyCurrentSession().createQuery("from IrFPEntryDetailRecord edr where edr.irFPBatchHeaderRecord.batchHeaderRecordId=?");
							
							edrQuery.setParameter(0,(int)fileId);
							List<IrFPEntryDetailRecord> entryList= edrQuery.list();
							
						
								for (IrFPEntryDetailRecord irFPEntryDetailRecord:entryList) {
									WEBEntryDetailRecordBean entryDetailRecordBean = new WEBEntryDetailRecordBean();
									
									entryDetailRecordBean.setItemCount(irFPEntryDetailRecord.getItemCount());
									entryDetailRecordBean.setPaymentCount(irFPEntryDetailRecord.getPaymentCount());
									entryDetailRecordBean.setRemittanceCount(irFPEntryDetailRecord.getRemittanceCount());
									entryDetailRecordBean.setScandocCount(irFPEntryDetailRecord.getScandocCount());
									entryDetailRecordBean.setWebEntryDetailRecordId(irFPEntryDetailRecord.getEntryDetailRecordId());
									/*if(irFPEntryDetailRecord.getRciName()!=null){	
									try{
										 byte[] bFile = readBytesFromFile(irFPEntryDetailRecord.getRciName());
										 
										 String s = new sun.misc.BASE64Encoder().encode(bFile);
										 
										 entryDetailRecordBean.setImageurl(s);
									}catch(Exception e){
										
									}
									
									}*/
									List<IrFPCCDAddendaRecord> addendaList = irFPEntryDetailRecord.getIrFPCCDAddendaRecord();
									
									for(IrFPCCDAddendaRecord irFPCCDAddendaRecord:addendaList ){

										CCDAddendaRecordBean car= new CCDAddendaRecordBean();
										
										car.setAmount(irFPCCDAddendaRecord.getAmount());
										if(irFPCCDAddendaRecord.getFciName()!=null){	
											car.setFciName(irFPCCDAddendaRecord.getFciName());
											}
										if(irFPCCDAddendaRecord.getRciName()!=null){	
											car.setRciName(irFPCCDAddendaRecord.getRciName());
											}
										
										
										
										
										if(irFPCCDAddendaRecord.getFciPng()!=null){	
										try{
											// byte[] bFile = readBytesFromFile(irFPCCDAddendaRecord.getFciPng());
											 
											 //String s = new sun.misc.BASE64Encoder().encode(bFile);
											 
											 car.setFciPng(irFPCCDAddendaRecord.getFciPng());
										}catch(Exception e){
											
										}
										}
										if(irFPCCDAddendaRecord.getRciPng()!=null){	
											try{
												 byte[] bFile = readBytesFromFile(irFPCCDAddendaRecord.getRciPng());
												 
												 String s = new sun.misc.BASE64Encoder().encode(bFile);
												 
												 car.setRciPng(irFPCCDAddendaRecord.getRciPng());
											}catch(Exception e){
												
											}
											}
										
										car.setEntryType(irFPCCDAddendaRecord.getEntryType());
										//car.setFciPng(fciPng);
										
										//car.setRciPng(rciPng);
								    	
					   					
					   					
					   					entryDetailRecordBean.getcCDAddendaRecordBean().add(car);
					   					
									}
									
									
									entryDetailRecord.add(entryDetailRecordBean);
									bhrBean.setEntryDetailRecordBean(entryDetailRecord);
									
								}
						
					batchHeaderRecordBean.add(bhrBean);
					
				
				//irPlusResponseDetails.setAllFileList(fileHeaderRecordBean);
				irPlusResponseDetails.setBhrBean(batchHeaderRecordBean);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			} else {
				LOGGER.debug("FileHeaderRecord not available :: Empty Data :: Data required to showAll FileHeaderRecord");
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}
			
		}catch(Exception e) {
			LOGGER.debug("Exception raised in show list FileHeaderRecord",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			throw new HibernateException(e);
			
		}
		
		
		
		
		return irPlusResponseDetails;
	}
	  private byte[] readBytesFromFile(String filePath) {

	        FileInputStream fileInputStream = null;
	        byte[] bytesArray = null;

	        try {

	            File file = new File(filePath);
	            bytesArray = new byte[(int) file.length()];

	            //read file into bytes[]
	            fileInputStream = new FileInputStream(file);
	            fileInputStream.read(bytesArray);

	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            if (fileInputStream != null) {
	                try {
	                    fileInputStream.close();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }

	        }

	        return bytesArray;

	    }

}
