package com.irplus.dao.fileprocessing.entrydetailrecord;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.hibernate.entities.IrFPEntryDetailRecord;
import com.irplus.dto.IRPlusResponseDetails;


import com.irplus.dto.fileprocessing.WEBEntryDetailRecordBean;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;

@Component
@Transactional
public class EntryDetailRecordDaoImpl implements EntryDetailRecordDao{

	private static final Logger LOGGER = Logger.getLogger(EntryDetailRecordDaoImpl.class); 
	
	@Autowired
	private SessionFactory sessionFactory;

	public Session getMyCurrentSession(){
			
			Session session =null;
			
			try {	
			
				session=sessionFactory.getCurrentSession();
			
			}catch (HibernateException e) {
				LOGGER.error("Exception raised DaoImpl :: At current session creation time ::"+e);
			}		
			return session; 
	}
	
	
	
	@Override
	public Boolean insertRecord(WEBEntryDetailRecordBean entryDetailRecordBean) {
		
		return null;
	}

	@Override
	public Boolean deleteRecord(WEBEntryDetailRecordBean entryDetailRecordBean) {
		
		return null;
	}

	@Override
	public List<WEBEntryDetailRecordBean> listAllRecords() {
		
		return null;
	}

	@Override
	public IRPlusResponseDetails getRecordByEntryId(Integer fileId) throws BusinessException {
		LOGGER.info("Inside EntryDetailRecordDaoImpl");
		LOGGER.debug("get By Id EntryDetailRecordDaoImpl instances");
		
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
		
		try {

			
			IrFPEntryDetailRecord edrList = (IrFPEntryDetailRecord)getMyCurrentSession().get(IrFPEntryDetailRecord.class,fileId);

			if (edrList!=null) {
				
				
				WEBEntryDetailRecordBean edrBean = new WEBEntryDetailRecordBean();
				
				edrBean.setRecordTypeCode(edrList.getRecordTypeCode());
				edrBean.setTransactionCode(edrList.getTransactionCode());
				edrBean.setIndividualIdentificationNumber(edrList.getIndividualIdentificationNumber());
				edrBean.setReceivingDFIIdentification(edrList.getReceivingDFIIdentification());
				edrBean.setCheckDigit(edrList.getCheckDigit());
				edrBean.setDfiAccountNumber(edrList.getDfiAccountNumber());
				edrBean.setAmount(edrList.getAmount());
				edrBean.setIsDataEntry(edrList.getIsDataEntry());
				edrBean.setPaymentDataEntryId(edrList.getPaymentDataEntryId());
				edrBean.setRemittanceDataEntryId(edrList.getRemittanceDataEntryId());
				edrBean.setAmountInWords(edrList.getAmountInWords());
				edrBean.setIndividualName(edrList.getIndividualName());
				edrBean.setDiscretionaryData(edrList.getDiscretionaryData()); 
				edrBean.setEntryType(edrList.getEntryType());
				edrBean.setAddendaRecordIndicator(edrList.getAddendaRecordIndicator());
				edrBean.setTraceNumber(edrList.getTraceNumber());
				
				
			
				
				
				
				
			
			irPlusResponseDetails.setEntryDetailRecordBean(edrBean);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		} else {
			LOGGER.debug("FileHeaderRecord not available :: Empty Data :: Data required to showAll FileHeaderRecord");
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
		}

			
			
			
			
		}catch(Exception e) {
			LOGGER.error("Exception raised in getByEntryDetailsId EntryDetailRecordDaoImpl",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			throw new HibernateException(e);
			
		}
		
		
		
		
		
		
		
		
		return irPlusResponseDetails;
	}

	@Override
	public Boolean updateRecord(WEBEntryDetailRecordBean batchHeaderRecord) {
		
		return null;
	}

	@Override
	public List<WEBEntryDetailRecordBean> getByQuery(Integer Id) {
		
		return null;
	}

}
