package com.irplus.dao.fieldtypes;

import com.irplus.dto.FieldTypesInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

public interface IFieldTypesDao {

	public IRPlusResponseDetails createFieldTypes(FieldTypesInfo fieldTypesInfo)throws BusinessException;

	public IRPlusResponseDetails listFieldTypes() throws BusinessException;

	public IRPlusResponseDetails getFieldTypesById(String fieldTypesById) throws BusinessException;
	
	public IRPlusResponseDetails updateFieldType(FieldTypesInfo fieldTypesInfo) throws BusinessException;

	public IRPlusResponseDetails deleteFieldTypeById(String fieldTypesById) throws BusinessException;
	    
}
