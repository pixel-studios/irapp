package com.irplus.dao.hibernate.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class IrMFieldtypes implements java.io.Serializable {

	private int fieldTypeId;
	private IrMUsers irMUsers;
	private String fieldFormat;
	private Integer defaultLength;
	private Integer isactive;
	private Date createddate;
	private Date modifieddate;
	
	
	private Set irSFormvalidations = new HashSet(0);

	public IrMFieldtypes() {
	}

	public IrMFieldtypes(int fieldTypeId) {
		this.fieldTypeId = fieldTypeId;
	}

	public IrMFieldtypes(int fieldTypeId, IrMUsers irMUsers, String fieldFormat, Integer defaultLength,
			Integer isactive, Date createddate, Date modifieddate, Set irSFormvalidations) {
		this.fieldTypeId = fieldTypeId;
		this.irMUsers = irMUsers;
		this.fieldFormat = fieldFormat;
		this.defaultLength = defaultLength;
		this.isactive = isactive;
		this.createddate = createddate;
		this.modifieddate = modifieddate;
		this.irSFormvalidations = irSFormvalidations;
	}

	public int getFieldTypeId() {
		return this.fieldTypeId;
	}

	public void setFieldTypeId(int fieldTypeId) {
		this.fieldTypeId = fieldTypeId;
	}

	public IrMUsers getIrMUsers() {
		return this.irMUsers;
	}

	public void setIrMUsers(IrMUsers irMUsers) {
		this.irMUsers = irMUsers;
	}

	public String getFieldFormat() {
		return this.fieldFormat;
	}

	public void setFieldFormat(String fieldFormat) {
		this.fieldFormat = fieldFormat;
	}

	public Integer getDefaultLength() {
		return this.defaultLength;
	}

	public void setDefaultLength(Integer defaultLength) {
		this.defaultLength = defaultLength;
	}

	public Integer getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

	public Set getIrSFormvalidations() {
		return this.irSFormvalidations;
	}

	public void setIrSFormvalidations(Set irSFormvalidations) {
		this.irSFormvalidations = irSFormvalidations;
	}
	
}
