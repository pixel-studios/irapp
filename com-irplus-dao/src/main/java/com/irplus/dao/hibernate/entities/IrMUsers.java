package com.irplus.dao.hibernate.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.Formula;


public class IrMUsers implements java.io.Serializable {

	private Integer userid;
	private String internalUserCode;
	private String userCode;
	private String firstName;
	private String lastName;
	private String username;
	private String password;
/*	private Integer roleId;*/
	private Integer adminId;
	private String emailAddress;
	private String contactno;
	private String userPhoto;
	private Integer isactive;
	private Date createdDate;
	private Date modifiedDate;
	
	private IRSSite site;
	
	private IrMRoles irMroles;
	   
	public IrMRoles getIrMroles() {
		return irMroles;
	}

	public void setIrMroles(IrMRoles irMroles) {
		this.irMroles = irMroles;
	}

	public IRSSite getSite() {
		return site;
	}

	public void setSite(IRSSite site) {
		this.site = site;
	}

	private Set irMRolepermissionses = new HashSet(0);
	private Set irMRoleses = new HashSet(0);
	private Set irMModulemenuses = new HashSet(0);
	private Set irSCustomerses = new HashSet(0);
	private Set irSCustomercontactses = new HashSet(0);
	private Set irMFieldtypeses = new HashSet(0);
	private Set irMReportfileFormatses = new HashSet(0);
	
	private Set irSBanks = new HashSet(0);
	private Set irSBankcontactses = new HashSet(0);
	private Set irSCustomerreportings = new HashSet(0);
	private Set irMFiletypeses = new HashSet(0);
	private Set irSBankBranches = new HashSet(0);
	private Set irSCustomerlicensings = new HashSet(0);
	private Set irMBusinessprocesses = new HashSet(0);
	
	private Set irSBankreportings = new HashSet(0);
	private Set irSCustomerOtherlicensings = new HashSet(0);
	private Set irMUsersBank = new HashSet(0);
	
	
	public Set getIrMUsersBank() {
		return irMUsersBank;
	}

	public void setIrMUsersBank(Set irMUsersBank) {
		this.irMUsersBank = irMUsersBank;
	}

	private Set irSCustomerReportlicensings = new HashSet(0);
	
	// 12 20 17
	private Set irSFormvalidations = new HashSet(0);
	private Set irSFormsetups = new HashSet(0);
	
	
	/*
	private Set irSBanklicensings = new HashSet(0);
	private Set irSContactlicensings = new HashSet(0);
	private Set irSBankgroupings = new HashSet(0);
	private Set irMLicensesetups = new HashSet(0);
	

*/
	
	public IrMUsers() {
	}

	/*public IrMUsers(String internalUserCode, String userCode, String firstName, String lastName,
			String username, Serializable password, Integer roleId, Integer adminId, String emailAddress,
			String contactno, String userPhoto, Integer isactive, Date createdDate, Date modifiedDate,
			Set irMReportfileFormatses, Set irSBanklicensings, Set irSBankreportings, Set irMFieldtypeses,
			Set irMRolepermissionses, Set irMBusinessprocesses, Set irSBankbranches, Set irSCustomerses,
			Set irMUsersBanksForUserid, Set irSCustomerlicensings, Set irSBanks, Set irSBankcontactses,
			Set irSContactlicensings, Set irSCustomercontactses, Set irMFiletypeses, Set irSCustomerOtherlicensings,
			Set irSBankgroupings, Set irSCustomerReportlicensings, Set irMRoleses, Set irMLicensesetups,
			Set irSFormvalidations, Set irSFormsetups, Set irMModulemenuses, Set irMUsersBanksForAdminId,
			Set irSCustomerreportings) {
		this.internalUserCode = internalUserCode;
		this.userCode = userCode;
		this.firstName = firstName;
		this.lastName = lastName;
		this.username = username;
		this.password = password;
		this.roleId = roleId;
		this.adminId = adminId;
		this.emailAddress = emailAddress;
		this.contactno = contactno;
		this.userPhoto = userPhoto;
		this.isactive = isactive;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.irMReportfileFormatses = irMReportfileFormatses;
		this.irSBanklicensings = irSBanklicensings;
		this.irSBankreportings = irSBankreportings;
		this.irMFieldtypeses = irMFieldtypeses;
		this.irMRolepermissionses = irMRolepermissionses;
		this.irMBusinessprocesses = irMBusinessprocesses;
		this.irSBankbranches = irSBankbranches;
		this.irSCustomerses = irSCustomerses;
		this.irMUsersBanksForUserid = irMUsersBanksForUserid;
		this.irSCustomerlicensings = irSCustomerlicensings;
		this.irSBanks = irSBanks;
		this.irSBankcontactses = irSBankcontactses;
		this.irSContactlicensings = irSContactlicensings;
		this.irSCustomercontactses = irSCustomercontactses;
		this.irMFiletypeses = irMFiletypeses;
		this.irSCustomerOtherlicensings = irSCustomerOtherlicensings;
		this.irSBankgroupings = irSBankgroupings;
		this.irSCustomerReportlicensings = irSCustomerReportlicensings;
		this.irMRoleses = irMRoleses;
		this.irMLicensesetups = irMLicensesetups;
		this.irSFormvalidations = irSFormvalidations;
		this.irSFormsetups = irSFormsetups;
		this.irMModulemenuses = irMModulemenuses;
		this.irMUsersBanksForAdminId = irMUsersBanksForAdminId;
		this.irSCustomerreportings = irSCustomerreportings;
	}*/

	
	
	
	public Integer getUserid() {
		return this.userid;
	}

	/*public IrMUsers(Integer userid, String internalUserCode, String userCode, String firstName, String lastName,
			String username, String password, Integer roleId, Integer adminId, String emailAddress,
			String contactno, String userPhoto, Integer isactive, Date createdDate, Date modifiedDate,
			Set irMRolepermissionses, Set irMRoleses, Set irMModulemenuses) {
		super();
		this.userid = userid;
		this.internalUserCode = internalUserCode;
		this.userCode = userCode;
		this.firstName = firstName;
		this.lastName = lastName;
		this.username = username;
		this.password = password;
		this.roleId = roleId;
		this.adminId = adminId;
		this.emailAddress = emailAddress;
		this.contactno = contactno;
		this.userPhoto = userPhoto;
		this.isactive = isactive;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.irMRolepermissionses = irMRolepermissionses;
		this.irMRoleses = irMRoleses;
		this.irMModulemenuses = irMModulemenuses;
	}*/

	
	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public IrMUsers(Integer userid, String internalUserCode, String userCode, String firstName, String lastName,
			String username, String password, Integer roleId, Integer adminId, String emailAddress, String contactno,
			String userPhoto, Integer isactive, Date createdDate, Date modifiedDate, Set irMRolepermissionses,
			Set irMRoleses, Set irMModulemenuses, Set irSCustomerses, Set irSCustomercontactses, Set irMFieldtypeses,
			Set irMReportfileFormatses, Set irSBanks, Set irSBankcontactses, Set irSCustomerreportings,
			Set irMFiletypeses, Set irSBankbranches) {
		super();
		this.userid = userid;
		this.internalUserCode = internalUserCode;
		this.userCode = userCode;
		this.firstName = firstName;
		this.lastName = lastName;
		this.username = username;
		this.password = password;
	/*	this.roleId = roleId;*/
		this.adminId = adminId;
		this.emailAddress = emailAddress;
		this.contactno = contactno;
		this.userPhoto = userPhoto;
		this.isactive = isactive;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.irMRolepermissionses = irMRolepermissionses;
		this.irMRoleses = irMRoleses;
		this.irMModulemenuses = irMModulemenuses;
		this.irSCustomerses = irSCustomerses;
		this.irSCustomercontactses = irSCustomercontactses;
		this.irMFieldtypeses = irMFieldtypeses;
		this.irMReportfileFormatses = irMReportfileFormatses;
		this.irSBanks = irSBanks;
		this.irSBankcontactses = irSBankcontactses;
		this.irSCustomerreportings = irSCustomerreportings;
		this.irMFiletypeses = irMFiletypeses;
		this.irSBankBranches = irSBankbranches;
	}

	public String getInternalUserCode() {
		return this.internalUserCode;
	}

	public void setInternalUserCode(String internalUserCode) {
		this.internalUserCode = internalUserCode;
	}

	public String getUserCode() {
		return this.userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public Set getIrSFormvalidations() {
		return irSFormvalidations;
	}

	public void setIrSFormvalidations(Set irSFormvalidations) {
		this.irSFormvalidations = irSFormvalidations;
	}

	public Set getIrSBankreportings() {
		return irSBankreportings;
	}

	public void setIrSBankreportings(Set irSBankreportings) {
		this.irSBankreportings = irSBankreportings;
	}

	public Set getIrSCustomerOtherlicensings() {
		return irSCustomerOtherlicensings;
	}

	public void setIrSCustomerOtherlicensings(Set irSCustomerOtherlicensings) {
		this.irSCustomerOtherlicensings = irSCustomerOtherlicensings;
	}

	

	public Set getIrSFormsetups() {
		return irSFormsetups;
	}

	public void setIrSFormsetups(Set irSFormsetups) {
		this.irSFormsetups = irSFormsetups;
	}

	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

/*	public Integer getRoleId() {
		return this.roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}*/

	public Integer getAdminId() {
		return this.adminId;
	}

	public void setAdminId(Integer adminId) {
		this.adminId = adminId;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}


	public Set getIrSCustomerReportlicensings() {
		return irSCustomerReportlicensings;
	}

	public void setIrSCustomerReportlicensings(Set irSCustomerReportlicensings) {
		this.irSCustomerReportlicensings = irSCustomerReportlicensings;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getContactno() {
		return this.contactno;
	}

	public void setContactno(String contactno) {
		this.contactno = contactno;
	}

	public String getUserPhoto() {
		return this.userPhoto;
	}

	public void setUserPhoto(String userPhoto) {
		this.userPhoto = userPhoto;
	}

	public Integer getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Set getIrMReportfileFormatses() {
		return this.irMReportfileFormatses;
	}

	public void setIrMReportfileFormatses(Set irMReportfileFormatses) {
		this.irMReportfileFormatses = irMReportfileFormatses;
	}

/*	public Set getIrSBanklicensings() {
		return this.irSBanklicensings;
	}

	public void setIrSBanklicensings(Set irSBanklicensings) {
		this.irSBanklicensings = irSBanklicensings;
	}

	public Set getIrSBankreportings() {
		return this.irSBankreportings;
	}

	public void setIrSBankreportings(Set irSBankreportings) {
		this.irSBankreportings = irSBankreportings;
	}
*/
	public Set getIrMFieldtypeses() {
		return this.irMFieldtypeses;
	}

	public void setIrMFieldtypeses(Set irMFieldtypeses) {
		this.irMFieldtypeses = irMFieldtypeses;
	}

	public Set getIrMRolepermissionses() {
		return this.irMRolepermissionses;
	}

	public void setIrMRolepermissionses(Set irMRolepermissionses) {
		this.irMRolepermissionses = irMRolepermissionses;
	}

	public Set getIrMBusinessprocesses() {
		return this.irMBusinessprocesses;
	}

	public void setIrMBusinessprocesses(Set irMBusinessprocesses) {
		this.irMBusinessprocesses = irMBusinessprocesses;
	}


	public Set getIrSBankBranches() {
		return irSBankBranches;
	}

	public void setIrSBankBranches(Set irSBankBranches) {
		this.irSBankBranches = irSBankBranches;
	}

	public Set getIrSCustomerses() {
		return this.irSCustomerses;
	}

	public void setIrSCustomerses(Set irSCustomerses) {
		this.irSCustomerses = irSCustomerses;
	}
	

/*	public Set getIrMUsersBanksForUserid() {
		return this.irMUsersBanksForUserid;
	}

	public void setIrMUsersBanksForUserid(Set irMUsersBanksForUserid) {
		this.irMUsersBanksForUserid = irMUsersBanksForUserid;
	}
*/
	public Set getIrSCustomerlicensings() {
		return this.irSCustomerlicensings;
	}

	public void setIrSCustomerlicensings(Set irSCustomerlicensings) {
		this.irSCustomerlicensings = irSCustomerlicensings;
	}

	public Set getIrSBanks() {
		return this.irSBanks;
	}

	public void setIrSBanks(Set irSBanks) {
		this.irSBanks = irSBanks;
	}

	public Set getIrSBankcontactses() {
		return this.irSBankcontactses;
	}

	public void setIrSBankcontactses(Set irSBankcontactses) {
		this.irSBankcontactses = irSBankcontactses;
	}
/*
	public Set getIrSContactlicensings() {
		return this.irSContactlicensings;
	}

	public void setIrSContactlicensings(Set irSContactlicensings) {
		this.irSContactlicensings = irSContactlicensings;
	}
*/
	public Set getIrSCustomercontactses() {
		return this.irSCustomercontactses;
	}

	public void setIrSCustomercontactses(Set irSCustomercontactses) {
		this.irSCustomercontactses = irSCustomercontactses;
	}

	public Set getIrMFiletypeses() {
		return this.irMFiletypeses;
	}

	public void setIrMFiletypeses(Set irMFiletypeses) {
		this.irMFiletypeses = irMFiletypeses;
	}

/*	public Set getIrSCustomerOtherlicensings() {
		return this.irSCustomerOtherlicensings;
	}

	public void setIrSCustomerOtherlicensings(Set irSCustomerOtherlicensings) {
		this.irSCustomerOtherlicensings = irSCustomerOtherlicensings;
	}

	public Set getIrSBankgroupings() {
		return this.irSBankgroupings;
	}

	public void setIrSBankgroupings(Set irSBankgroupings) {
		this.irSBankgroupings = irSBankgroupings;
	}

	public Set getIrSCustomerReportlicensings() {
		return this.irSCustomerReportlicensings;
	}

	public void setIrSCustomerReportlicensings(Set irSCustomerReportlicensings) {
		this.irSCustomerReportlicensings = irSCustomerReportlicensings;
	}
*/
	public Set getIrMRoleses() {
		return this.irMRoleses;
	}

	public void setIrMRoleses(Set irMRoleses) {
		this.irMRoleses = irMRoleses;
	}

/*	public Set getIrMLicensesetups() {
		return this.irMLicensesetups;
	}

	public void setIrMLicensesetups(Set irMLicensesetups) {
		this.irMLicensesetups = irMLicensesetups;
	}

	public Set getIrSFormvalidations() {
		return this.irSFormvalidations;
	}

	public void setIrSFormvalidations(Set irSFormvalidations) {
		this.irSFormvalidations = irSFormvalidations;
	}

	public Set getIrSFormsetups() {
		return this.irSFormsetups;
	}

	public void setIrSFormsetups(Set irSFormsetups) {
		this.irSFormsetups = irSFormsetups;
	}
*/
	public Set getIrMModulemenuses() {
		return this.irMModulemenuses;
	}

	public void setIrMModulemenuses(Set irMModulemenuses) {
		this.irMModulemenuses = irMModulemenuses;
	}

/*	public Set getIrMUsersBanksForAdminId() {
		return this.irMUsersBanksForAdminId;
	}

	public void setIrMUsersBanksForAdminId(Set irMUsersBanksForAdminId) {
		this.irMUsersBanksForAdminId = irMUsersBanksForAdminId;
	}
*/
	public Set getIrSCustomerreportings() {
		return this.irSCustomerreportings;
	}

	public void setIrSCustomerreportings(Set irSCustomerreportings) {
		this.irSCustomerreportings = irSCustomerreportings;
	}

}
