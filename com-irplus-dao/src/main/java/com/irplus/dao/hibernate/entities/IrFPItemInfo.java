package com.irplus.dao.hibernate.entities;

public class IrFPItemInfo {

	private int itemId;
	private String receivingDFIIdentification;
	private String checkDigit;
	private String dfiAccountNumber;
	private String amount;
	private String individualIdentificationNumber;
	private String individualName;
	private String amountInWords;
	private char itemType;
	private String traceNumber;
	private IrFPFileProcessInfo irFPFileProcessInfo;
	private IrFPBatchInfo irFPBatchInfo;
	
	
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public String getTraceNumber() {
		return traceNumber;
	}
	public void setTraceNumber(String traceNumber) {
		this.traceNumber = traceNumber;
	}
	public String getReceivingDFIIdentification() {
		return receivingDFIIdentification;
	}
	public void setReceivingDFIIdentification(String receivingDFIIdentification) {
		this.receivingDFIIdentification = receivingDFIIdentification;
	}




	public String getCheckDigit() {
		return checkDigit;
	}




	public void setCheckDigit(String checkDigit) {
		this.checkDigit = checkDigit;
	}




	public String getDfiAccountNumber() {
		return dfiAccountNumber;
	}




	public void setDfiAccountNumber(String dfiAccountNumber) {
		this.dfiAccountNumber = dfiAccountNumber;
	}




	public String getAmount() {
		return amount;
	}




	public void setAmount(String amount) {
		this.amount = amount;
	}




	public String getIndividualIdentificationNumber() {
		return individualIdentificationNumber;
	}




	public void setIndividualIdentificationNumber(String individualIdentificationNumber) {
		this.individualIdentificationNumber = individualIdentificationNumber;
	}




	public String getIndividualName() {
		return individualName;
	}




	public void setIndividualName(String individualName) {
		this.individualName = individualName;
	}




	public String getAmountInWords() {
		return amountInWords;
	}




	public void setAmountInWords(String amountInWords) {
		this.amountInWords = amountInWords;
	}




	public char getItemType() {
		return itemType;
	}




	public void setItemType(char itemType) {
		this.itemType = itemType;
	}




	public IrFPFileProcessInfo getIrFPFileProcessInfo() {
		return irFPFileProcessInfo;
	}




	public void setIrFPFileProcessInfo(IrFPFileProcessInfo irFPFileProcessInfo) {
		this.irFPFileProcessInfo = irFPFileProcessInfo;
	}




	public IrFPBatchInfo getIrFPBatchInfo() {
		return irFPBatchInfo;
	}




	public void setIrFPBatchInfo(IrFPBatchInfo irFPBatchInfo) {
		irFPBatchInfo = irFPBatchInfo;
	}




	public IrFPItemInfo(){
		
	}
	
}
