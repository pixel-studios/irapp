package com.irplus.dao.hibernate.entities;

import java.util.Date;



public class IrMDbinfo implements java.io.Serializable {

	private Integer dbInfoId;
	
	private String hostName;
	private String username;
	private String password;
	private Date createdDate;
	private Date modifiedDate;
	private IRSSite site;
	
	


	public IRSSite getSite() {
		return site;
	}

	public void setSite(IRSSite site) {
		this.site = site;
	}

	public Integer getDbInfoId() {
		return dbInfoId;
	}

	public void setDbInfoId(Integer dbInfoId) {
		this.dbInfoId = dbInfoId;
	}

	public IrMDbinfo() {
	}

	public IrMDbinfo(String hostName, String username, String password, Date createdDate,
			Date modifiedDate) {
		this.hostName = hostName;
		this.username = username;
		this.password = password;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
	}

	
	public String getHostName() {
		return this.hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
