package com.irplus.dao.hibernate.entities;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

public class IRSSite  {
	
	private Long siteId;
	private String siteName;
	private String siteCode;
	private String contactPerson;
	private String contactNo;
	private String address1;
	private String address2;
	private String siteLogo;
	private String rootFolderPath;
	private String emphesoftXmlFolderPath;
	private String imagehawkXmlFolderPath;
	private String outputTransmissionFolderPath;
	private String archiveProcessFilePath;
	
	public String getRootFolderPath() {
		return rootFolderPath;
	}
	public void setRootFolderPath(String rootFolderPath) {
		this.rootFolderPath = rootFolderPath;
	}
	public String getEmphesoftXmlFolderPath() {
		return emphesoftXmlFolderPath;
	}
	public void setEmphesoftXmlFolderPath(String emphesoftXmlFolderPath) {
		this.emphesoftXmlFolderPath = emphesoftXmlFolderPath;
	}
	public String getImagehawkXmlFolderPath() {
		return imagehawkXmlFolderPath;
	}
	public void setImagehawkXmlFolderPath(String imagehawkXmlFolderPath) {
		this.imagehawkXmlFolderPath = imagehawkXmlFolderPath;
	}
	public String getOutputTransmissionFolderPath() {
		return outputTransmissionFolderPath;
	}
	public void setOutputTransmissionFolderPath(String outputTransmissionFolderPath) {
		this.outputTransmissionFolderPath = outputTransmissionFolderPath;
	}
	public String getArchiveProcessFilePath() {
		return archiveProcessFilePath;
	}
	public void setArchiveProcessFilePath(String archiveProcessFilePath) {
		this.archiveProcessFilePath = archiveProcessFilePath;
	}
	public String getSiteLogo() {
		return siteLogo;
	}
	public void setSiteLogo(String siteLogo) {
		this.siteLogo = siteLogo;
	}
	
	
	
	private IRSLicense license;
	
	private Set irMUsers = new HashSet(0);
	
	private Set irMDbinfo= new HashSet(0);
	
	
	
	public IRSLicense getLicense() {
		return license;
	}
	public void setLicense(IRSLicense license) {
		this.license = license;
	}
	
	public Set getIrMDbinfo() {
		return irMDbinfo;
	}
	public void setIrMDbinfo(Set irMDbinfo) {
		this.irMDbinfo = irMDbinfo;
	}
	public Set getIrMUsers() {
		return irMUsers;
	}
	public void setIrMUsers(Set irMUsers) {
		this.irMUsers = irMUsers;
	}
	private int isactive;
	//private IrMUsers user;
	private Timestamp createdDate;
    private Timestamp modifiedDate;
	public Long getSiteId() {
		return siteId;
	}
	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getSiteCode() {
		return siteCode;
	}
	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}
	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	
	public int getIsactive() {
		return isactive;
	}
	public void setIsactive(int isactive) {
		this.isactive = isactive;
	}
	
	
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Timestamp getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IRSSite [siteId=").append(siteId).append(", siteName=").append(siteName).append(", siteCode=")
		.append(", siteLogo=").append(siteLogo).append(siteCode).append(", contactPerson=").append(contactPerson).append(", contactNo=")
				.append(contactNo).append(", address1=").append(address1).append(", address2=").append(address2).append(", irMDbinfo=").append(irMDbinfo)
				.append(", license=").append(license).append(", irMUsers=").append(irMUsers).append(", isactive=").append(isactive).append(", createdDate=")
				.append(createdDate).append(", modifiedDate=").append(modifiedDate).append(", rootFolderPath=").append(rootFolderPath).append(", emphesoftXmlFolderPath=").append(emphesoftXmlFolderPath).append(", imagehawkXmlFolderPath=").append(imagehawkXmlFolderPath).append(", outputTransmissionFolderPath=").append(outputTransmissionFolderPath).append(", archiveProcessFilePath=").append(archiveProcessFilePath).append("]");
		return builder.toString();
	}
}
