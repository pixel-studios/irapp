package com.irplus.dao.hibernate.entities;

import java.util.ArrayList;
import java.util.List;

public class IrFPBatchInfo {
	private int batchId;
	private String batchNumber;
	private IrSCustomers customer;
	private String batchType;
	private IrFPFileProcessInfo irFPFileProcessInfo;
	private List<IrFPItemInfo> irFPItemInfo= new ArrayList<IrFPItemInfo>();
	
	
	public IrFPBatchInfo(){
		
	}



	public int getBatchId() {
		return batchId;
	}

	public void setBatchId(int batchId) {
		this.batchId = batchId;
	}

	public String getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	public IrSCustomers getCustomer() {
		return customer;
	}
	public void setCustomer(IrSCustomers customer) {
		this.customer = customer;
	}



	public String getBatchType() {
		return batchType;
	}



	public void setBatchType(String batchType) {
		this.batchType = batchType;
	}



	public IrFPFileProcessInfo getIrFPFileProcessInfo() {
		return irFPFileProcessInfo;
	}



	public void setIrFPFileProcessInfo(IrFPFileProcessInfo irFPFileProcessInfo) {
		this.irFPFileProcessInfo = irFPFileProcessInfo;
	}



	public List<IrFPItemInfo> getIrFPItemInfo() {
		return irFPItemInfo;
	}



	public void setIrFPItemInfo(List<IrFPItemInfo> irFPItemInfo) {
		this.irFPItemInfo = irFPItemInfo;
	}
	

}
