package com.irplus.dao.hibernate.entities;

import java.util.ArrayList;
import java.util.List;

public class IrFPEntryDetailRecord {
	
	private int entryDetailRecordId;
	private char recordTypeCode;
	private String transactionCode;
	private String receivingDFIIdentification;
	private String checkDigit;
	private String dfiAccountNumber;
	private String amount;
	private String individualIdentificationNumber;
	private String individualName;
	private String paymentDataEntryId;
	private String remittanceDataEntryId;
	private String amountInWords;
	private String isDataEntry;
	private String entryType;
	private String discretionaryData;
	private String fciName;
	private String rciName;
	private String transactionCount;
	private String itemCount;
	private String paymentCount;
	private String remittanceCount;
	private String scandocCount;
	private String paymentTypeCode;
	private String addendaRecordIndicator;
	private String traceNumber;
	private IrFPBatchHeaderRecord irFPBatchHeaderRecord;
	private IrFPFileHeaderRecord irFPFileHeaderRecord;
	private List<IrFPCCDAddendaRecord> irFPCCDAddendaRecord= new ArrayList<IrFPCCDAddendaRecord>();

	
	public String getTransactionCount() {
		return transactionCount;
	}
	public void setTransactionCount(String transactionCount) {
		this.transactionCount = transactionCount;
	}
	public String getItemCount() {
		return itemCount;
	}
	public void setItemCount(String itemCount) {
		this.itemCount = itemCount;
	}
	public String getPaymentCount() {
		return paymentCount;
	}
	public void setPaymentCount(String paymentCount) {
		this.paymentCount = paymentCount;
	}
	public String getRemittanceCount() {
		return remittanceCount;
	}
	public void setRemittanceCount(String remittanceCount) {
		this.remittanceCount = remittanceCount;
	}
	public String getScandocCount() {
		return scandocCount;
	}
	public void setScandocCount(String scandocCount) {
		this.scandocCount = scandocCount;
	}
	public String getFciName() {
		return fciName;
	}
	public void setFciName(String fciName) {
		this.fciName = fciName;
	}

	
	public String getRciName() {
		return rciName;
	}
	public void setRciName(String rciName) {
		this.rciName = rciName;
	}

	private char itemType;
	
	
	public char getItemType() {
		return itemType;
	}
	public void setItemType(char itemType) {
		this.itemType = itemType;
	}
	public String getDiscretionaryData() {
		return discretionaryData;
	}
	public void setDiscretionaryData(String discretionaryData) {
		this.discretionaryData = discretionaryData;
	}


	public IrFPFileHeaderRecord getIrFPFileHeaderRecord() {
		return irFPFileHeaderRecord;
	}
	public void setIrFPFileHeaderRecord(IrFPFileHeaderRecord irFPFileHeaderRecord) {
		this.irFPFileHeaderRecord = irFPFileHeaderRecord;
	}


	
	public List<IrFPCCDAddendaRecord> getIrFPCCDAddendaRecord() {
		return irFPCCDAddendaRecord;
	}
	public void setIrFPCCDAddendaRecord(List<IrFPCCDAddendaRecord> irFPCCDAddendaRecord) {
		this.irFPCCDAddendaRecord = irFPCCDAddendaRecord;
	}
	public String getEntryType() {
		return entryType;
	}
	public void setEntryType(String entryType) {
		this.entryType = entryType;
	}
	
	public String getPaymentDataEntryId() {
		return paymentDataEntryId;
	}

	public void setPaymentDataEntryId(String paymentDataEntryId) {
		this.paymentDataEntryId = paymentDataEntryId;
	}

	public String getRemittanceDataEntryId() {
		return remittanceDataEntryId;
	}

	public void setRemittanceDataEntryId(String remittanceDataEntryId) {
		this.remittanceDataEntryId = remittanceDataEntryId;
	}

	public String getAmountInWords() {
		return amountInWords;
	}

	public void setAmountInWords(String amountInWords) {
		this.amountInWords = amountInWords;
	}

	public String getIsDataEntry() {
		return isDataEntry;
	}

	public void setIsDataEntry(String isDataEntry) {
		this.isDataEntry = isDataEntry;
	}

	
	

	public IrFPEntryDetailRecord() {
		
	}

	

	public int getEntryDetailRecordId() {
		return entryDetailRecordId;
	}



	public void setEntryDetailRecordId(int entryDetailRecordId) {
		this.entryDetailRecordId = entryDetailRecordId;
	}



	public char getRecordTypeCode() {
		return recordTypeCode;
	}

	public void setRecordTypeCode(char recordTypeCode) {
		this.recordTypeCode = recordTypeCode;
	}

	public String getTransactionCode() {
		return transactionCode;
	}

	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

	public String getReceivingDFIIdentification() {
		return receivingDFIIdentification;
	}

	public void setReceivingDFIIdentification(String receivingDFIIdentification) {
		this.receivingDFIIdentification = receivingDFIIdentification;
	}

	public String getCheckDigit() {
		return checkDigit;
	}

	public void setCheckDigit(String checkDigit) {
		this.checkDigit = checkDigit;
	}

	public String getDfiAccountNumber() {
		return dfiAccountNumber;
	}

	public void setDfiAccountNumber(String dfiAccountNumber) {
		this.dfiAccountNumber = dfiAccountNumber;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getIndividualIdentificationNumber() {
		return individualIdentificationNumber;
	}

	public void setIndividualIdentificationNumber(String individualIdentificationNumber) {
		this.individualIdentificationNumber = individualIdentificationNumber;
	}

	public String getIndividualName() {
		return individualName;
	}

	public void setIndividualName(String individualName) {
		this.individualName = individualName;
	}

	public String getPaymentTypeCode() {
		return paymentTypeCode;
	}

	public void setPaymentTypeCode(String paymentTypeCode) {
		this.paymentTypeCode = paymentTypeCode;
	}

	public String getAddendaRecordIndicator() {
		return addendaRecordIndicator;
	}

	public void setAddendaRecordIndicator(String addendaRecordIndicator) {
		this.addendaRecordIndicator = addendaRecordIndicator;
	}

	public String getTraceNumber() {
		return traceNumber;
	}

	public void setTraceNumber(String traceNumber) {
		this.traceNumber = traceNumber;
	}

	public IrFPBatchHeaderRecord getIrFPBatchHeaderRecord() {
		return irFPBatchHeaderRecord;
	}

	public void setIrFPBatchHeaderRecord(IrFPBatchHeaderRecord irFPBatchHeaderRecord) {
		this.irFPBatchHeaderRecord = irFPBatchHeaderRecord;
	}
	
	

}
