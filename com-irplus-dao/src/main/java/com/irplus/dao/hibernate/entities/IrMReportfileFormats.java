package com.irplus.dao.hibernate.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class IrMReportfileFormats implements java.io.Serializable {

	private Integer fileFormatId;
	private IrMFileTypes irMFiletypes;
	private IrMUsers irMUsers;
	private String fileformatName;
	private Integer isactive;
	private Date createddate;
	private Date modifieddate;
	
	private Set irSCustomerReportlicensings = new HashSet(0);

	public IrMReportfileFormats() {
	}

	public IrMReportfileFormats(IrMFileTypes irMFiletypes, IrMUsers irMUsers, String fileformatName,
			Integer isactive, Date createddate, Date modifieddate, Set irSCustomerReportlicensings) {
		this.irMFiletypes = irMFiletypes;
		this.irMUsers = irMUsers;
		this.fileformatName = fileformatName;
		this.isactive = isactive;
		this.createddate = createddate;
		this.modifieddate = modifieddate;
		this.irSCustomerReportlicensings = irSCustomerReportlicensings;
	}

	public Integer getFileFormatId() {
		return this.fileFormatId;
	}

	public void setFileFormatId(Integer fileFormatId) {
		this.fileFormatId = fileFormatId;
	}

	public IrMFileTypes getIrMFiletypes() {
		return this.irMFiletypes;
	}

	public void setIrMFiletypes(IrMFileTypes irMFiletypes) {
		this.irMFiletypes = irMFiletypes;
	}

	public IrMUsers getIrMUsers() {
		return this.irMUsers;
	}

	public void setIrMUsers(IrMUsers irMUsers) {
		this.irMUsers = irMUsers;
	}

	public String getFileformatName() {
		return this.fileformatName;
	}

	public void setFileformatName(String fileformatName) {
		this.fileformatName = fileformatName;
	}

	public Integer getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

	public Set getIrSCustomerReportlicensings() {
		return this.irSCustomerReportlicensings;
	}

	public void setIrSCustomerReportlicensings(Set irSCustomerReportlicensings) {
		this.irSCustomerReportlicensings = irSCustomerReportlicensings;
	}

}
