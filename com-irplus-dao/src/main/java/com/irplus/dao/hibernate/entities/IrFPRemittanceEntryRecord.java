package com.irplus.dao.hibernate.entities;

public class IrFPRemittanceEntryRecord {
	private int remittanceId;
	private char recordTypeCode;
	private String transactionCode;
	private String individualIdentificationNumber;
	private String individualName;
	private String addendaRecordIndicator;
	private String traceNumber;
	private String discretionaryData;
	private IrFPBatchHeaderRecord irFPBatchHeaderRecord;
	private IrFPFileHeaderRecord irFPFileHeaderRecord;
	

	public IrFPFileHeaderRecord getIrFPFileHeaderRecord() {
		return irFPFileHeaderRecord;
	}

	public void setIrFPFileHeaderRecord(IrFPFileHeaderRecord irFPFileHeaderRecord) {
		this.irFPFileHeaderRecord = irFPFileHeaderRecord;
	}

	public IrFPRemittanceEntryRecord() {
	
	}

	public int getRemittanceId() {
		return remittanceId;
	}

	public void setRemittanceId(int remittanceId) {
		this.remittanceId = remittanceId;
	}

	public char getRecordTypeCode() {
		return recordTypeCode;
	}

	public void setRecordTypeCode(char recordTypeCode) {
		this.recordTypeCode = recordTypeCode;
	}

	public String getTransactionCode() {
		return transactionCode;
	}

	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

	public String getIndividualIdentificationNumber() {
		return individualIdentificationNumber;
	}

	public void setIndividualIdentificationNumber(String individualIdentificationNumber) {
		this.individualIdentificationNumber = individualIdentificationNumber;
	}

	public String getIndividualName() {
		return individualName;
	}

	public void setIndividualName(String individualName) {
		this.individualName = individualName;
	}


	public String getAddendaRecordIndicator() {
		return addendaRecordIndicator;
	}

	public void setAddendaRecordIndicator(String addendaRecordIndicator) {
		this.addendaRecordIndicator = addendaRecordIndicator;
	}

	public String getTraceNumber() {
		return traceNumber;
	}

	public void setTraceNumber(String traceNumber) {
		this.traceNumber = traceNumber;
	}

	public String getDiscretionaryData() {
		return discretionaryData;
	}

	public void setDiscretionaryData(String discretionaryData) {
		this.discretionaryData = discretionaryData;
	}

	public IrFPBatchHeaderRecord getIrFPBatchHeaderRecord() {
		return irFPBatchHeaderRecord;
	}

	public void setIrFPBatchHeaderRecord(IrFPBatchHeaderRecord irFPBatchHeaderRecord) {
		this.irFPBatchHeaderRecord = irFPBatchHeaderRecord;
	}

	
	
	
	
}
