package com.irplus.dao.hibernate.entities;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import com.irplus.dto.BankInfo;

public class IrSBankBranch {

	private Long branchId;
	private String branchLocation;
	private String branchOwnCode;
	private Character isdefault;
	private String locationId;
	
	private String website;
	private String country;
	private String state;
	private String city;
	private String zipcode;
	private String branchLogo;
	private int isactive;
	private IrMUsers user;
	private IRSBank bank;
	
	private Timestamp createddate;
    private Timestamp modifieddate;
    
    private Set irSCustomerOtherlicensings = new HashSet(0);
	private Set irSCustomerreportings = new HashSet(0);
	
    //12 8 17	
	private Set irSCustomers = new HashSet(0);
	
	private Set irMUsersBank = new HashSet(0);
	
	public Set getIrMUsersBank() {
		return irMUsersBank;
	}

	public void setIrMUsersBank(Set irMUsersBank) {
		this.irMUsersBank = irMUsersBank;
	}
	//12 11 17 
	
	
	private Set irSCustomersgrouping = new HashSet(0);
	public Set getIrSCustomersgrouping() {
		return irSCustomersgrouping;
	}

	public void setIrSCustomersgrouping(Set irSCustomersgrouping) {
		this.irSCustomersgrouping = irSCustomersgrouping;
	}


	private Set irFPFileHeaderRecord = new HashSet(0);
	
	public Set getIrFPFileHeaderRecord() {
		return irFPFileHeaderRecord;
	}

	public void setIrFPFileHeaderRecord(Set irFPFileHeaderRecord) {
		this.irFPFileHeaderRecord = irFPFileHeaderRecord;
	}
	
	// 12 21 17
	private Set irSFormsetups = new HashSet(0);
	
    public IrSBankBranch() {
		
	}
    
	public String getBranchLogo() {
		return branchLogo;
	}

	public void setBranchLogo(String branchLogo) {
		this.branchLogo = branchLogo;
	}

	public IrSBankBranch(BankInfo bankInfo) {
		this.branchLocation = bankInfo.getBranchLocation();
		this.branchOwnCode= bankInfo.getBranchOwnCode();
		this.locationId= bankInfo.getLocationId();
		this.website= bankInfo.getWebsite();
		this.isdefault= bankInfo.getIsdefault();
		this.country= bankInfo.getCountry();
		this.state= bankInfo.getState();
		this.city= bankInfo.getCity();
		this.zipcode= bankInfo.getZipcode();
		this.isactive= bankInfo.getIsactive();
		
	}
	public Long getBranchId() {
		return branchId;
	}
	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}
	public String getBranchLocation() {
		return branchLocation;
	}
	public void setBranchLocation(String branchLocation) {
		this.branchLocation = branchLocation;
	}
	public String getBranchOwnCode() {
		return branchOwnCode;
	}
	public Set getIrSCustomers() {
		return irSCustomers;
	}
	public void setIrSCustomers(Set irSCustomers) {
		this.irSCustomers = irSCustomers;
	}

	public Set getIrSFormsetups() {
		return irSFormsetups;
	}
	public void setIrSFormsetups(Set irSFormsetups) {
		this.irSFormsetups = irSFormsetups;
	}
	public void setBranchOwnCode(String branchOwnCode) {
		this.branchOwnCode = branchOwnCode;
	}
	public Character getIsdefault() {
		return isdefault;
	}
	public Set getIrSCustomerOtherlicensings() {
		return irSCustomerOtherlicensings;
	}
	public void setIrSCustomerOtherlicensings(Set irSCustomerOtherlicensings) {
		this.irSCustomerOtherlicensings = irSCustomerOtherlicensings;
	}
	public void setIsdefault(Character isdefault) {
		this.isdefault = isdefault;
	}
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public Set getIrSCustomerreportings(){
		return irSCustomerreportings;
	}
	public void setIrSCustomerreportings(Set irSCustomerreportings) {
		this.irSCustomerreportings = irSCustomerreportings;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode){
		this.zipcode = zipcode;
	}
	public int getIsactive() {
		return isactive;
	}
	public void setIsactive(int isactive) {
		this.isactive = isactive;
	}
	public IrMUsers getUser() {
		return user;
	}
	public void setUser(IrMUsers user) {
		this.user = user;
	}
	public Timestamp getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Timestamp createddate) {
		this.createddate = createddate;
	}
	public Timestamp getModifieddate() {
		return modifieddate;
	}
	public void setModifieddate(Timestamp modifieddate) {
		this.modifieddate = modifieddate;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IrMBankBranch [branchId=");
		builder.append(branchId);
		builder.append(", branchLocation=");
		builder.append(branchLocation);
		builder.append(", branchOwnCode=");
		builder.append(branchOwnCode);
		builder.append(", isdefault=");
		builder.append(isdefault);
		builder.append(", locationId=");
		builder.append(locationId);
		builder.append(", website=");
		builder.append(website);
		builder.append(", country=");
		builder.append(country);
		builder.append(", state=");
		builder.append(state);
		builder.append(", city=");
		builder.append(city);
		builder.append(", zipcode=");
		builder.append(zipcode);
		builder.append(", isactive=");
		builder.append(isactive);
		builder.append(", user=");
		builder.append(user);
		builder.append(", createddate=");
		builder.append(createddate);
		builder.append(", modifieddate=");
		builder.append(modifieddate);
		builder.append("]");
		return builder.toString();
	}
	public IRSBank getBank() {
		return bank;
	}
	public void setBank(IRSBank bank) {
		this.bank = bank;
	}   
}