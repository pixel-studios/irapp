package com.irplus.dao.hibernate.entities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.irplus.dto.fileprocessing.BatchControlRecordBean;
import com.irplus.dto.fileprocessing.BatchHeaderRecordBean;
import com.irplus.dto.fileprocessing.CCDAddendaRecordBean;
import com.irplus.dto.fileprocessing.FileControlRecordBean;
import com.irplus.dto.fileprocessing.WEBEntryDetailRecordBean;



public class IrFPFileHeaderRecord {
	
	private int fileHeaderRecordId;
	private String achFileName;
	private char recordTypeCode;
	private String priorityCode;
	private String immediateDestination;
	private String immediateOrigin;
	private String fileCreationDate;
	private String fileCreationTime;
	private String fileIdModifier;
	private String recordSize;
	private String blockingFactor;
	private String formatCode;
	private String immediateDestinationName;
	private String immediateOriginName;
	private String referenceCode;
	private int isProcessed;
	private String processedDate;
	private String processedTime;
	private IRSBank bank;
	private IrMFileTypes fileType;
	private IrSBankBranch irSBankbranch;
	private List<IrFPBatchHeaderRecord> irFPBatchHeaderRecord= new ArrayList<IrFPBatchHeaderRecord>();
	private List<IrFPFileControlRecord> irFPFileControlRecord= new ArrayList<IrFPFileControlRecord>();
	private List<IrFPEntryDetailRecord> irFPEntryDetailRecord= new ArrayList<IrFPEntryDetailRecord>();
	private List<IrFPCCDAddendaRecord> irFPCCDAddendaRecord= new ArrayList<IrFPCCDAddendaRecord>();

	
	
	
	public List<IrFPCCDAddendaRecord> getIrFPCCDAddendaRecord() {
		return irFPCCDAddendaRecord;
	}
	public void setIrFPCCDAddendaRecord(List<IrFPCCDAddendaRecord> irFPCCDAddendaRecord) {
		this.irFPCCDAddendaRecord = irFPCCDAddendaRecord;
	}
	public List<IrFPEntryDetailRecord> getIrFPEntryDetailRecord() {
		return irFPEntryDetailRecord;
	}
	public void setIrFPEntryDetailRecord(List<IrFPEntryDetailRecord> irFPEntryDetailRecord) {
		this.irFPEntryDetailRecord = irFPEntryDetailRecord;
	}
	
	public IRSBank getBank() {
		return bank;
	}
	public void setBank(IRSBank bank) {
		this.bank = bank;
	}
	public IrSBankBranch getIrSBankbranch() {
		return irSBankbranch;
	}
	public void setIrSBankbranch(IrSBankBranch irSBankbranch) {
		this.irSBankbranch = irSBankbranch;
	}
	public IrMFileTypes getFileType() {
		return fileType;
	}
	public void setFileType(IrMFileTypes fileType) {
		this.fileType = fileType;
	}
	
	public List<IrFPFileControlRecord> getIrFPFileControlRecord() {
		return irFPFileControlRecord;
	}

	public void setIrFPFileControlRecord(List<IrFPFileControlRecord> irFPFileControlRecord) {
		this.irFPFileControlRecord = irFPFileControlRecord;
	}

	
	public List<IrFPBatchHeaderRecord> getIrFPBatchHeaderRecord() {
		return irFPBatchHeaderRecord;
	}

	public void setIrFPBatchHeaderRecord(List<IrFPBatchHeaderRecord> irFPBatchHeaderRecord) {
		this.irFPBatchHeaderRecord = irFPBatchHeaderRecord;
	}
	public IrFPFileHeaderRecord() {
		
	}	
	
	public int getIsProcessed() {
		return isProcessed;
	}
	public void setIsProcessed(int isProcessed) {
		this.isProcessed = isProcessed;
	}
	public String getProcessedDate() {
		return processedDate;
	}
	public void setProcessedDate(String processedDate) {
		this.processedDate = processedDate;
	}
	public String getProcessedTime() {
		return processedTime;
	}
	public void setProcessedTime(String processedTime) {
		this.processedTime = processedTime;
	}
	
	
	public int getFileHeaderRecordId() {
		return fileHeaderRecordId;
	}
	public void setFileHeaderRecordId(int fileHeaderRecordId) {
		this.fileHeaderRecordId = fileHeaderRecordId;
	}
	public String getAchFileName() {
		return achFileName;
	}
	public void setAchFileName(String achFileName) {
		this.achFileName = achFileName;
	}
	public char getRecordTypeCode() {
		return recordTypeCode;
	}
	public void setRecordTypeCode(char recordTypeCode) {
		this.recordTypeCode = recordTypeCode;
	}
	public String getPriorityCode() {
		return priorityCode;
	}
	public void setPriorityCode(String priorityCode) {
		this.priorityCode = priorityCode;
	}
	public String getImmediateDestination() {
		return immediateDestination;
	}
	public void setImmediateDestination(String immediateDestination) {
		this.immediateDestination = immediateDestination;
	}
	public String getImmediateOrigin() {
		return immediateOrigin;
	}
	public void setImmediateOrigin(String immediateOrigin) {
		this.immediateOrigin = immediateOrigin;
	}
	public String getFileCreationDate() {
		return fileCreationDate;
	}
	public void setFileCreationDate(String fileCreationDate) {
		this.fileCreationDate = fileCreationDate;
	}
	public String getFileCreationTime() {
		return fileCreationTime;
	}
	public void setFileCreationTime(String fileCreationTime) {
		this.fileCreationTime = fileCreationTime;
	}
	public String getFileIdModifier() {
		return fileIdModifier;
	}
	public void setFileIdModifier(String fileIdModifier) {
		this.fileIdModifier = fileIdModifier;
	}
	public String getRecordSize() {
		return recordSize;
	}
	public void setRecordSize(String recordSize) {
		this.recordSize = recordSize;
	}
	public String getBlockingFactor() {
		return blockingFactor;
	}
	public void setBlockingFactor(String blockingFactor) {
		this.blockingFactor = blockingFactor;
	}
	public String getFormatCode() {
		return formatCode;
	}
	public void setFormatCode(String formatCode) {
		this.formatCode = formatCode;
	}
	public String getImmediateDestinationName() {
		return immediateDestinationName;
	}
	public void setImmediateDestinationName(String immediateDestinationName) {
		this.immediateDestinationName = immediateDestinationName;
	}
	public String getImmediateOriginName() {
		return immediateOriginName;
	}
	public void setImmediateOriginName(String immediateOriginName) {
		this.immediateOriginName = immediateOriginName;
	}
	public String getReferenceCode() {
		return referenceCode;
	}
	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}
	

}
