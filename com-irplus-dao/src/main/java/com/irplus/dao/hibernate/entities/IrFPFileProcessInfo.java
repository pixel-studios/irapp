package com.irplus.dao.hibernate.entities;

import java.util.ArrayList;
import java.util.List;

public class IrFPFileProcessInfo {
	private Long fileId;
	private String achFileName;
	private IRSBank bank;
	private IrMFileTypes fileType;
	private String processedDate;
	private String processedTime;
	private int isProcessed;
	private String remarks;
	private List<IrFPBatchInfo> irFPBatchInfo= new ArrayList<IrFPBatchInfo>();
	private List<IrFPItemInfo> irFPItemInfo= new ArrayList<IrFPItemInfo>();
	
	public IrFPFileProcessInfo(){}
	
	public Long getFileId() {
		return fileId;
	}
	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}
	public String getAchFileName() {
		return achFileName;
	}
	public void setAchFileName(String achFileName) {
		this.achFileName = achFileName;
	}
	public IRSBank getBank() {
		return bank;
	}
	public void setBank(IRSBank bank) {
		this.bank = bank;
	}
	
	public String getProcessedDate() {
		return processedDate;
	}
	public void setProcessedDate(String processedDate) {
		this.processedDate = processedDate;
	}
	public String getProcessedTime() {
		return processedTime;
	}
	public void setProcessedTime(String processedTime) {
		this.processedTime = processedTime;
	}
	public int getIsProcessed() {
		return isProcessed;
	}
	public void setIsProcessed(int isProcessed) {
		this.isProcessed = isProcessed;
	}
	
	public IrMFileTypes getFileType() {
		return fileType;
	}
	public void setFileType(IrMFileTypes fileType) {
		this.fileType = fileType;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public List<IrFPBatchInfo> getIrFPBatchInfo() {
		return irFPBatchInfo;
	}

	public void setIrFPBatchInfo(List<IrFPBatchInfo> irFPBatchInfo) {
		this.irFPBatchInfo = irFPBatchInfo;
	}
	

	public List<IrFPItemInfo> getIrFPItemInfo() {
		return irFPItemInfo;
	}

	public void setIrFPItemInfo(List<IrFPItemInfo> irFPItemInfo) {
		this.irFPItemInfo = irFPItemInfo;
	}

	
	
	
	
	
	

	
	
	
}
