package com.irplus.dao.hibernate.entities;
// Generated Nov 10, 2016 6:30:12 PM by Hibernate Tools 5.2.0.Beta1

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class IRSBank implements java.io.Serializable {

    private static final long serialVersionUID = -3176074231815769436L;
	

    private long bankId;
    
    private String siteId;
    private String bankName;
    private String IRBankCode;
    private String bankLogo;
    private int isactive;
    private int userId;
    private String rtNumber;
	private String ddaNumber;
    private String bankFolder;
  
    private IRSSite site;
    
    public IRSSite getSite() {
		return site;
	}
	public void setSite(IRSSite site) {
		this.site = site;
	}


	public String getRtNumber() {
		return rtNumber;
	}
	public void setRtNumber(String rtNumber) {
		this.rtNumber = rtNumber;
	}
	public String getDdaNumber() {
		return ddaNumber;
	}
	public void setDdaNumber(String ddaNumber) {
		this.ddaNumber = ddaNumber;
	}
	private Set irSCustomersgrouping = new HashSet(0);
	public Set getIrSCustomersgrouping() {
		return irSCustomersgrouping;
	}

	public void setIrSCustomersgrouping(Set irSCustomersgrouping) {
		this.irSCustomersgrouping = irSCustomersgrouping;
	}

	private Set irSBankBranch = new HashSet(0);

	public Set getIrSBankBranch() {
		return irSBankBranch;
	}
	public void setIrSBankBranch(Set irSBankBranch) {
		this.irSBankBranch = irSBankBranch;
	}

	private Set irFPFileHeaderRecord = new HashSet(0);
	
	public Set getIrsCustomerGroupingDetails() {
		return irsCustomerGroupingDetails;
	}
	public void setIrsCustomerGroupingDetails(Set irsCustomerGroupingDetails) {
		this.irsCustomerGroupingDetails = irsCustomerGroupingDetails;
	}


	private Set irsCustomerGroupingDetails = new HashSet(0);
	
	public Set getIrFPFileHeaderRecord() {
		return irFPFileHeaderRecord;
	}
	public void setIrFPFileHeaderRecord(Set irFPFileHeaderRecord) {
		this.irFPFileHeaderRecord = irFPFileHeaderRecord;
	}


	private Timestamp createddate;
    private Timestamp modifieddate;
    
    private IrMUsers irMUsers_id;
    private IRSBankLicensing iRSBankLicensing;
	private Set irMUsersBank = new HashSet(0);
    
    public Set getIrMUsersBank() {
		return irMUsersBank;
	}
	public void setIrMUsersBank(Set irMUsersBank) {
		this.irMUsersBank = irMUsersBank;
	}
	public IRSBankLicensing getiRSBankLicensing() {
		return iRSBankLicensing;
	}
	public void setiRSBankLicensing(IRSBankLicensing iRSBankLicensing) {
		this.iRSBankLicensing = iRSBankLicensing;
	}
	public String getBankFolder() {
		return bankFolder;
	}
	public void setBankFolder(String bankFolder) {
		this.bankFolder = bankFolder;
	}

    public int getUserId() {
	return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public long getBankId() {
		return bankId;
	}
	public void setBankId(long bankId) {
		this.bankId = bankId;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getIRBankCode() {
		return IRBankCode;
	}
	public void setIRBankCode(String iRBankCode) {
		IRBankCode = iRBankCode;
	}
	public String getBankLogo() {
		return bankLogo;
	}
	public void setBankLogo(String bankLogo) {
		this.bankLogo = bankLogo;
	}
	public Timestamp getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Timestamp createddate) {
		this.createddate = createddate;
	}
	public Timestamp getModifieddate() {
		return modifieddate;
	}
	public void setModifieddate(Timestamp modifieddate) {
		this.modifieddate = modifieddate;
	}
	public int getIsactive() {
		return isactive;
	}
	public void setIsactive(int isactive) {
		this.isactive = isactive;
	}
	public IrMUsers getIrMUsers_id() {
		return irMUsers_id;
	}
	public void setIrMUsers_id(IrMUsers irMUsers_id) {
		this.irMUsers_id = irMUsers_id;
	}
	

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IRSBank [bankId=");
		builder.append(bankId);
		builder.append(", siteId=");
		builder.append(siteId);
		builder.append(", bankName=");
		builder.append(bankName);
		builder.append(", IRBankCode=");
		builder.append(IRBankCode);
		builder.append(", bankLogo=");
		builder.append(bankLogo);
		builder.append(", createddate=");
		builder.append(userId);
		builder.append(", userId=");
		builder.append(createddate);
		builder.append(", modifieddate=");
		builder.append(modifieddate);
		builder.append("]");
		return builder.toString();
	}
}
