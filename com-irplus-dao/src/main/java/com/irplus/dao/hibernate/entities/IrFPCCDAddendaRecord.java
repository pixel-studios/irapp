package com.irplus.dao.hibernate.entities;


public class IrFPCCDAddendaRecord {
	
	private int ccdAddendaRecordId;
	private char recordTypeCode;
	private String addendaTypeCode;
	private String paymentRelatedInformation;
	private String addendaSequenceNumber;
	private String entryDetailSequenceNumber;
	private IrFPEntryDetailRecord irFPEntryDetailRecord;
	private IrFPFileHeaderRecord irFPFileHeaderRecord;
	private String fciName;
	private String rciName;
	private String fciPng;
	private String rciPng;
	private String totalDebit;
	private String amount;
	private String entryType;
	
	
	
	
	public String getEntryType() {
		return entryType;
	}
	public void setEntryType(String entryType) {
		this.entryType = entryType;
	}
	public String getFciName() {
		return fciName;
	}
	public void setFciName(String fciName) {
		this.fciName = fciName;
	}

	public String getRciName() {
		return rciName;
	}
	public void setRciName(String rciName) {
		this.rciName = rciName;
	}
	public String getFciPng() {
		return fciPng;
	}
	public void setFciPng(String fciPng) {
		this.fciPng = fciPng;
	}
	public String getRciPng() {
		return rciPng;
	}
	public void setRciPng(String rciPng) {
		this.rciPng = rciPng;
	}
	public String getTotalDebit() {
		return totalDebit;
	}
	public void setTotalDebit(String totalDebit) {
		this.totalDebit = totalDebit;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public IrFPFileHeaderRecord getIrFPFileHeaderRecord() {
		return irFPFileHeaderRecord;
	}
	public void setIrFPFileHeaderRecord(IrFPFileHeaderRecord irFPFileHeaderRecord) {
		this.irFPFileHeaderRecord = irFPFileHeaderRecord;
	}
	public IrFPEntryDetailRecord getIrFPEntryDetailRecord() {
		return irFPEntryDetailRecord;
	}
	public void setIrFPEntryDetailRecord(IrFPEntryDetailRecord irFPEntryDetailRecord) {
		this.irFPEntryDetailRecord = irFPEntryDetailRecord;
	}
	public IrFPCCDAddendaRecord() {
			
	}
	public int getCcdAddendaRecordId() {
		return ccdAddendaRecordId;
	}
	public void setCcdAddendaRecordId(int ccdAddendaRecordId) {
		this.ccdAddendaRecordId = ccdAddendaRecordId;
	}
	public char getRecordTypeCode() {
		return recordTypeCode;
	}
	public void setRecordTypeCode(char recordTypeCode) {
		this.recordTypeCode = recordTypeCode;
	}
	public String getAddendaTypeCode() {
		return addendaTypeCode;
	}
	public void setAddendaTypeCode(String addendaTypeCode) {
		this.addendaTypeCode = addendaTypeCode;
	}
	public String getPaymentRelatedInformation() {
		return paymentRelatedInformation;
	}
	public void setPaymentRelatedInformation(String paymentRelatedInformation) {
		this.paymentRelatedInformation = paymentRelatedInformation;
	}
	public String getAddendaSequenceNumber() {
		return addendaSequenceNumber;
	}
	public void setAddendaSequenceNumber(String addendaSequenceNumber) {
		this.addendaSequenceNumber = addendaSequenceNumber;
	}
	public String getEntryDetailSequenceNumber() {
		return entryDetailSequenceNumber;
	}
	public void setEntryDetailSequenceNumber(String entryDetailSequenceNumber) {
		this.entryDetailSequenceNumber = entryDetailSequenceNumber;
	}
}
