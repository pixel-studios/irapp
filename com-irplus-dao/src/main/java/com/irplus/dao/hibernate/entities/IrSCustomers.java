package com.irplus.dao.hibernate.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class IrSCustomers implements java.io.Serializable {

	private Integer customerId;
	private IrMUsers irMUsers;
	private IrSBankBranch irSBankBranch; 
	
	private String irCustomerCode;
	private String companyName;
	private String referenceName;
	private String bankCustomerCode;
	private String ddaBankAc;
	private String compIdentNo;
	private String website;
	private String country;
	private String state;
	private String city;
	private String zipcode;
	private Integer isactive;
	
	private Date createddate;
	private Date modifieddate;
	
	/*private Timestamp createddate;
    private Timestamp modifieddate;
    */
	private String photofile;
	
	private Set irSCustomercontactses = new HashSet(0);
	
	private Set irSCustomerOtherlicensings = new HashSet(0);
	
	private Set irSCustomerreportings = new HashSet(0);
	
	private Set irSFormsetups = new HashSet(0);
	
	private Set irFPFileHeaderRecord = new HashSet(0);
	
	
	
	public Set getIrFPFileHeaderRecord() {
		return irFPFileHeaderRecord;
	}
	public void setIrFPFileHeaderRecord(Set irFPFileHeaderRecord) {
		this.irFPFileHeaderRecord = irFPFileHeaderRecord;
	}

	private List<IrFPBatchHeaderRecord> irFPBatchHeaderRecord= new ArrayList<IrFPBatchHeaderRecord>();
	
public List<IrFPBatchHeaderRecord> getIrFPBatchHeaderRecord() {
		return irFPBatchHeaderRecord;
	}
	public void setIrFPBatchHeaderRecord(List<IrFPBatchHeaderRecord> irFPBatchHeaderRecord) {
		this.irFPBatchHeaderRecord = irFPBatchHeaderRecord;
	}
	
	
	public List<IrsCustomerGroupingDetails> getIrsCustomerGroupingDetails() {
		return irsCustomerGroupingDetails;
	}
	public void setIrsCustomerGroupingDetails(List<IrsCustomerGroupingDetails> irsCustomerGroupingDetails) {
		this.irsCustomerGroupingDetails = irsCustomerGroupingDetails;
	}

	private List<IrsCustomerGroupingDetails> irsCustomerGroupingDetails= new ArrayList<IrsCustomerGroupingDetails>();
	/*	
 * 	private Set irSCustomerReportlicensings = new HashSet(0);
	private Set irSContactlicensings = new HashSet(0);
	private Set irSBankgroupings = new HashSet(0);
	private Set irSCustomerlicensings = new HashSet(0);
*/
	public IrSCustomers() {
	}

	public IrSCustomers(IrMUsers irMUsers, IrSBankBranch irSBankBranch,
			String irCustomerCode,
			String companyName, String referenceName, String bankCustomerCode, String ddaBankAc,
			String compIdentNo, String website, String country, String state, String city,
			String zipcode, Integer isactive, Date createddate, Date modifieddate,
			Set irSCustomerReportlicensings, Set irSFormsetups, Set irSContactlicensings, Set irSCustomercontactses,
			Set irSCustomerOtherlicensings, Set irSCustomerreportings, Set irSBankgroupings,
			Set irSCustomerlicensings) {
		this.irMUsers = irMUsers;
		this.irSBankBranch = irSBankBranch;
		this.irCustomerCode = irCustomerCode;
		this.companyName = companyName;
		this.referenceName = referenceName;
		this.bankCustomerCode = bankCustomerCode;
		this.ddaBankAc = ddaBankAc;
		this.compIdentNo = compIdentNo;
		this.website = website;
		this.country = country;
		this.state = state;
		this.city = city;
		this.zipcode = zipcode;
		this.isactive = isactive;
		this.createddate = createddate;
		this.modifieddate = modifieddate;
		
		/*this.irSCustomerReportlicensings = irSCustomerReportlicensings;
		this.irSFormsetups = irSFormsetups;
		this.irSContactlicensings = irSContactlicensings;
		this.irSCustomercontactses = irSCustomercontactses;
		this.irSCustomerOtherlicensings = irSCustomerOtherlicensings;
		this.irSCustomerreportings = irSCustomerreportings;
		this.irSBankgroupings = irSBankgroupings;
		this.irSCustomerlicensings = irSCustomerlicensings;
		*/
	}

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public IrMUsers getIrMUsers() {
		return this.irMUsers;
	}

	public void setIrMUsers(IrMUsers irMUsers) {
		this.irMUsers = irMUsers;
	}
 
	public IrSBankBranch getIrSBankBranch() {
		return irSBankBranch;
	}

	public void setIrSBankBranch(IrSBankBranch irSBankBranch) {
		this.irSBankBranch = irSBankBranch;
	}

	public Set getIrSCustomercontactses() {
		return irSCustomercontactses;
	}

	public void setIrSCustomercontactses(Set irSCustomercontactses) {
		this.irSCustomercontactses = irSCustomercontactses;
	}

	public String getIrCustomerCode() {
		return this.irCustomerCode;
	}

	public Set getIrSFormsetups() {
		return irSFormsetups;
	}

	public void setIrSFormsetups(Set irSFormsetups) {
		this.irSFormsetups = irSFormsetups;
	}

	public void setIrCustomerCode(String irCustomerCode) {
		this.irCustomerCode = irCustomerCode;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getReferenceName() {
		return this.referenceName;
	}

	public Set getIrSCustomerreportings() {
		return irSCustomerreportings;
	}

	public void setIrSCustomerreportings(Set irSCustomerreportings) {
		this.irSCustomerreportings = irSCustomerreportings;
	}

	public Set getIrSCustomerOtherlicensings() {
		return irSCustomerOtherlicensings;
	}

	public void setIrSCustomerOtherlicensings(Set irSCustomerOtherlicensings) {
		this.irSCustomerOtherlicensings = irSCustomerOtherlicensings;
	}

	public void setReferenceName(String referenceName) {
		this.referenceName = referenceName;
	}

	public String getPhotofile() {
		return photofile;
	}

	public void setPhotofile(String photofile) {
		this.photofile = photofile;
	}

	public String getBankCustomerCode() {
		return this.bankCustomerCode;
	}

	public void setBankCustomerCode(String bankCustomerCode) {
		this.bankCustomerCode = bankCustomerCode;
	}

	public String getDdaBankAc() {
		return this.ddaBankAc;
	}

	public void setDdaBankAc(String ddaBankAc) {
		this.ddaBankAc = ddaBankAc;
	}

	public String getCompIdentNo() {
		return this.compIdentNo;
	}

	public void setCompIdentNo(String compIdentNo) {
		this.compIdentNo = compIdentNo;
	}

	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipcode() {
		return this.zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public Integer getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

/*	public Set getIrSCustomerReportlicensings() {
		return this.irSCustomerReportlicensings;
	}

	public void setIrSCustomerReportlicensings(Set irSCustomerReportlicensings) {
		this.irSCustomerReportlicensings = irSCustomerReportlicensings;
	}

	public Set getIrSFormsetups() {
		return this.irSFormsetups;
	}

	public void setIrSFormsetups(Set irSFormsetups) {
		this.irSFormsetups = irSFormsetups;
	}

	public Set getIrSContactlicensings() {
		return this.irSContactlicensings;
	}

	public void setIrSContactlicensings(Set irSContactlicensings) {
		this.irSContactlicensings = irSContactlicensings;
	}

	public Set getIrSCustomercontactses() {
		return this.irSCustomercontactses;
	}

	public void setIrSCustomercontactses(Set irSCustomercontactses) {
		this.irSCustomercontactses = irSCustomercontactses;
	}

	public Set getIrSCustomerOtherlicensings() {
		return this.irSCustomerOtherlicensings;
	}

	public void setIrSCustomerOtherlicensings(Set irSCustomerOtherlicensings) {
		this.irSCustomerOtherlicensings = irSCustomerOtherlicensings;
	}

	public Set getIrSCustomerreportings() {
		return this.irSCustomerreportings;
	}

	public void setIrSCustomerreportings(Set irSCustomerreportings) {
		this.irSCustomerreportings = irSCustomerreportings;
	}

	public Set getIrSBankgroupings() {
		return this.irSBankgroupings;
	}

	public void setIrSBankgroupings(Set irSBankgroupings) {
		this.irSBankgroupings = irSBankgroupings;
	}

	public Set getIrSCustomerlicensings() {
		return this.irSCustomerlicensings;
	}

	public void setIrSCustomerlicensings(Set irSCustomerlicensings) {
		this.irSCustomerlicensings = irSCustomerlicensings;
	}
*/
}
