package com.irplus.dao.hibernate.entities;

import java.sql.Timestamp;

import com.irplus.dto.ContactInfo;

public class IrSBankContact {

	private Long contactId;
	//private BankBranch bankBranch;
	private String firstName;
	private String lastName;
	private String contactRole;
	private String contactNo;
	private String emailId;
	private String address1;
	private String address2;
	private Character isdefault;
	private String country;
	private String state;
	private String city;
	private String zipcode;
	private int isactive;
	private IrMUsers user;
	private IrSBankBranch branch;
	private Timestamp createddate;
    private Timestamp modifieddate;
    public IrSBankContact() {
		
	}
	public IrSBankContact(ContactInfo contactInfo) {
		
		
		this.firstName = contactInfo.getFirstName();
		this.lastName= contactInfo.getLastName();
		this.contactRole= contactInfo.getContactRole();
		this.contactNo= contactInfo.getContactNo();
		this.emailId= contactInfo.getEmailId();
		this.address1= contactInfo.getAddress1();
		this.address2= contactInfo.getAddress2();
		this.isdefault= contactInfo.getIsdefault();
		this. country= contactInfo.getCountry();
		this.state= contactInfo.getState();
		this.city= contactInfo.getCity();
		this.zipcode= contactInfo.getZipcode();
		this.isactive= contactInfo.getIsActive();
		
	}
	public Long getContactId() {
		return contactId;
	}
	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public Character getIsdefault() {
		return isdefault;
	}
	public void setIsdefault(Character isdefault) {
		this.isdefault = isdefault;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	
	public IrMUsers getUser() {
		return user;
	}
	public void setUser(IrMUsers user) {
		this.user = user;
	}
	public Timestamp getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Timestamp createddate) {
		this.createddate = createddate;
	}
	public Timestamp getModifieddate() {
		return modifieddate;
	}
	public void setModifieddate(Timestamp modifieddate) {
		this.modifieddate = modifieddate;
	}
	
	public String getContactRole() {
		return contactRole;
	}
	public void setContactRole(String contactRole) {
		this.contactRole = contactRole;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public int getIsactive() {
		return isactive;
	}
	public void setIsactive(int isactive) {
		this.isactive = isactive;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IrMBankContact [contactId=");
		builder.append(contactId);
		builder.append(", firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", contactRole=");
		builder.append(contactRole);
		builder.append(", contactNo=");
		builder.append(contactNo);
		builder.append(", emailId=");
		builder.append(emailId);
		builder.append(", address1=");
		builder.append(address1);
		builder.append(", address2=");
		builder.append(address2);
		builder.append(", isdefault=");
		builder.append(isdefault);
		builder.append(", country=");
		builder.append(country);
		builder.append(", state=");
		builder.append(state);
		builder.append(", city=");
		builder.append(city);
		builder.append(", zipcode=");
		builder.append(zipcode);
		builder.append(", isActive=");
		builder.append(isactive);
		builder.append(", user=");
		builder.append(user);
		builder.append(", createddate=");
		builder.append(createddate);
		builder.append(", modifieddate=");
		builder.append(modifieddate);
		builder.append("]");
		return builder.toString();
	}
	public IrSBankBranch getBranch() {
		return branch;
	}
	public void setBranch(IrSBankBranch branch) {
		this.branch = branch;
	}
	
    
    
}
