package com.irplus.dao.hibernate.entities;

import java.sql.Timestamp;
import java.util.Date;

import com.irplus.dto.CustomerContactsBean;
import com.irplus.dto.CustomerGroupingDetails;

public class IrsCustomerGroupingDetails {

	private Integer customrGrpDtId;
	private Integer isactive;
	//private Integer customerGrpId;
	private String createddate;
	private String createdtime;
	private Integer userId;
	private IrMUsers irMUsers;
	private IRSBank bank;
	private IrSBankBranch irSBankBranch;
	private IrSCustomers irSCustomers;
	private IrsCustomerGrouping irsCustomerGrouping;
	
	public String getCreatedtime() {
		return createdtime;
	}
	public void setCreatedtime(String createdtime) {
		this.createdtime = createdtime;
	}

	public String getCreateddate() {
		return createddate;
	}
	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}

		
	public IRSBank getBank() {
		return bank;
	}
	public void setBank(IRSBank bank) {
		this.bank = bank;
	}
	public Long getBranchId() {
		return branchId;
	}
	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	private Long  branchId;
	private Integer customerId;
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	
	
	
	public IrSBankBranch getIrSBankBranch() {
		return irSBankBranch;
	}
	public void setIrSBankBranch(IrSBankBranch irSBankBranch) {
		this.irSBankBranch = irSBankBranch;
	}
	

	

	public Integer getCustomrGrpDtId() {
		return customrGrpDtId;
	}
	public void setCustomrGrpDtId(Integer customrGrpDtId) {
		this.customrGrpDtId = customrGrpDtId;
	}
	public Integer getIsactive() {
		return isactive;
	}
	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

/*	public Integer getCustomerGrpId() {
		return customerGrpId;
	}
	public void setCustomerGrpId(Integer customerGrpId) {
		this.customerGrpId = customerGrpId;
	}
*/

	public IrMUsers getIrMUsers() {
		return irMUsers;
	}
	public void setIrMUsers(IrMUsers irMUsers) {
		this.irMUsers = irMUsers;
	}

	public IrSCustomers getIrSCustomers() {
		return irSCustomers;
	}
	public IrsCustomerGrouping getIrsCustomerGrouping() {
		return irsCustomerGrouping;
	}
	public void setIrsCustomerGrouping(IrsCustomerGrouping irsCustomerGrouping) {
		this.irsCustomerGrouping = irsCustomerGrouping;
	}
	public void setIrSCustomers(IrSCustomers irSCustomers) {
		this.irSCustomers = irSCustomers;
	}
	
	
	
	
	public IrsCustomerGroupingDetails(){}
	
	
public IrsCustomerGroupingDetails(CustomerGroupingDetails mappingInfo) {
		
		this.isactive= mappingInfo.getIsactive();
		this.createddate= mappingInfo.getCreateddate();
	
	}
	public IrsCustomerGroupingDetails(int customrGrpDtId) {
		this.customrGrpDtId = customrGrpDtId;
	}
	
	
	
	
	
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IrsCustomerGroupingDetails [customrGrpDtId=");
		builder.append(customrGrpDtId);
		builder.append(", isactive=");
		builder.append(isactive);
		builder.append(", createddate=");
		builder.append(createddate);
	
		builder.append(", irMUsers=");
		builder.append(irMUsers);
		builder.append(", IRSBank=");
		builder.append(bank);
		builder.append(", irSCustomers=");
		builder.append(irSCustomers);
		builder.append("]");
		return builder.toString();
	}
	
}