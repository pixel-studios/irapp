package com.irplus.dao.hibernate.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class IrSBankreporting implements java.io.Serializable {

	private Integer reportId;
	private IrMModules irMModules;
	private IrMUsers irMUsers;
	private IRSBank irSBank;
	private Integer isactive;
	
	private Date createddate;
	private Date modifieddate;
	
	private Set irSCustomerreportings = new HashSet(0);

	public IrSBankreporting() {
	}

	public IrSBankreporting(IrMModules irMModules) {
		this.irMModules = irMModules;
	}

	public IrSBankreporting(IrMModules irMModules, IrMUsers irMUsers, IRSBank irSBank, Integer isactive,
			Date createddate, Date modifieddate, Set irSCustomerreportings) {
		this.irMModules = irMModules;
		this.irMUsers = irMUsers;
		this.irSBank = irSBank;
		this.isactive = isactive;
		this.createddate = createddate;
		this.modifieddate = modifieddate;
		this.irSCustomerreportings = irSCustomerreportings;
	}

	public Integer getReportId() {
		return this.reportId;
	}

	public void setReportId(Integer reportId) {
		this.reportId = reportId;
	}

	public IrMModules getIrMModules() {
		return this.irMModules;
	}

	public void setIrMModules(IrMModules irMModules) {
		this.irMModules = irMModules;
	}

	public IrMUsers getIrMUsers() {
		return this.irMUsers;
	}

	public void setIrMUsers(IrMUsers irMUsers) {
		this.irMUsers = irMUsers;
	}

	public IRSBank getIRSBank() {
		return this.irSBank;
	}

	public IRSBank getIrSBank() {
		return irSBank;
	}

	public void setIrSBank(IRSBank irSBank) {
		this.irSBank = irSBank;
	}

	public Integer getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

	public Set getIrSCustomerreportings() {
		return this.irSCustomerreportings;
	}

	public void setIrSCustomerreportings(Set irSCustomerreportings) {
		this.irSCustomerreportings = irSCustomerreportings;
	}

}