package com.irplus.dao.hibernate.entities;

import java.sql.Timestamp;

import com.irplus.dto.ContactInfo;

public class IRSBankLicensing {

	private Long bankLicenseId;
	private IRSBank bank;
	private IrMFileTypes fileType;
	private IrMBusinessProcess businessProcess;
	private int isactive;
	private IrMUsers user;
	private Timestamp createddate;
    private Timestamp modifieddate;
    public IRSBankLicensing() {
    	
    	this.isactive=1;
		
	}
	public Long getBankLicenseId() {
		return bankLicenseId;
	}
	public void setBankLicenseId(Long bankLicenseId) {
		this.bankLicenseId = bankLicenseId;
	}
	public IRSBank getBank() {
		return bank;
	}
	public void setBank(IRSBank bank) {
		this.bank = bank;
	}
	public IrMFileTypes getFileType() {
		return fileType;
	}
	public void setFileType(IrMFileTypes fileType) {
		this.fileType = fileType;
	}
	public IrMBusinessProcess getBusinessProcess() {
		return businessProcess;
	}
	public void setBusinessProcess(IrMBusinessProcess businessProcess) {
		this.businessProcess = businessProcess;
	}
	public int getIsactive() {
		return isactive;
	}
	public void setIsactive(int isactive) {
		this.isactive = isactive;
	}
	public IrMUsers getUser() {
		return user;
	}
	public void setUser(IrMUsers user) {
		this.user = user;
	}
	public Timestamp getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Timestamp createddate) {
		this.createddate = createddate;
	}
	public Timestamp getModifieddate() {
		return modifieddate;
	}
	public void setModifieddate(Timestamp modifieddate) {
		this.modifieddate = modifieddate;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IRSBankLicensing [bankLicenseId=").append(bankLicenseId).append(", bank=").append(bank)
				.append(", fileType=").append(fileType).append(", businessProcess=").append(businessProcess)
				.append(", isactive=").append(isactive).append(", user=").append(user).append(", createddate=")
				.append(createddate).append(", modifieddate=").append(modifieddate).append("]");
		return builder.toString();
	}
	
	
	
}
