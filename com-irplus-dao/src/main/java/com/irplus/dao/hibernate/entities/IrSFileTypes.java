package com.irplus.dao.hibernate.entities;

import java.sql.Timestamp;

public class IrSFileTypes {

	private Long filetypeid;
	//private BankBranch bankBranch;
	private String filetypename;
	
	private Long licenseid;
	private int isactive;
	//private IrMUsers user;
	private Timestamp createddate;
    private Timestamp modifieddate;
    public IrSFileTypes() {
		
	}
	public Long getFiletypeid() {
		return filetypeid;
	}
	public void setFiletypeid(Long filetypeid) {
		this.filetypeid = filetypeid;
	}
	public String getFiletypename() {
		return filetypename;
	}
	public void setFiletypename(String filetypename) {
		this.filetypename = filetypename;
	}
	
	
	public int getIsactive() {
		return isactive;
	}
	public void setIsactive(int isactive) {
		this.isactive = isactive;
	}
	/*public IrMUsers getUser() {
		return user;
	}
	public void setUser(IrMUsers user) {
		this.user = user;
	}*/
	public Timestamp getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Timestamp createddate) {
		this.createddate = createddate;
	}
	public Timestamp getModifieddate() {
		return modifieddate;
	}
	public void setModifieddate(Timestamp modifieddate) {
		this.modifieddate = modifieddate;
	}
	public Long getLicenseid() {
		return licenseid;
	}
	public void setLicenseid(Long licenseid) {
		this.licenseid = licenseid;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IrMFileTypes [filetypeid=").append(filetypeid).append(", filetypename=").append(filetypename)
				.append(", licenseid=").append(licenseid).append(", isactive=").append(isactive).append(", user=")
				.append(", createddate=").append(createddate).append(", modifieddate=")
				.append(modifieddate).append("]");
		return builder.toString();
	}
	
    
	
}
