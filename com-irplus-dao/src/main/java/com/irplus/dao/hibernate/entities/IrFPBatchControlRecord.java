package com.irplus.dao.hibernate.entities;



public class IrFPBatchControlRecord {

	private int batchControlRecordId;
	private char recordTypeCode;
	private String serviceClassCode;
	private String entryAddendaCount;
	private String entryHash;
	private String totalDebitEntryDollarAmount;
	private String totalcreditEntryDollarAmount;
	private String companyIdentification;
	private String messageAuthenticationCode;
	private String reserved;
	private String orginatingFinancialInstitutionID;
	private String batchNumber;
	private IrFPFileHeaderRecord irFPFileHeaderRecord;
	public IrFPFileHeaderRecord getIrFPFileHeaderRecord() {
		return irFPFileHeaderRecord;
	}

	public void setIrFPFileHeaderRecord(IrFPFileHeaderRecord irFPFileHeaderRecord) {
		this.irFPFileHeaderRecord = irFPFileHeaderRecord;
	}

	private IrFPBatchHeaderRecord irFPBatchHeaderRecord;

	

	
	public IrFPBatchControlRecord() {
	
	}

	public int getBatchControlRecordId() {
		return batchControlRecordId;
	}

	public void setBatchControlRecordId(int batchControlRecordId) {
		this.batchControlRecordId = batchControlRecordId;
	}

	public char getRecordTypeCode() {
		return recordTypeCode;
	}

	public void setRecordTypeCode(char recordTypeCode) {
		this.recordTypeCode = recordTypeCode;
	}

	public String getServiceClassCode() {
		return serviceClassCode;
	}

	public void setServiceClassCode(String serviceClassCode) {
		this.serviceClassCode = serviceClassCode;
	}

	public String getEntryAddendaCount() {
		return entryAddendaCount;
	}

	public void setEntryAddendaCount(String entryAddendaCount) {
		this.entryAddendaCount = entryAddendaCount;
	}

	public String getEntryHash() {
		return entryHash;
	}

	public void setEntryHash(String entryHash) {
		this.entryHash = entryHash;
	}

	public String getTotalDebitEntryDollarAmount() {
		return totalDebitEntryDollarAmount;
	}

	public void setTotalDebitEntryDollarAmount(String totalDebitEntryDollarAmount) {
		this.totalDebitEntryDollarAmount = totalDebitEntryDollarAmount;
	}

	public String getTotalcreditEntryDollarAmount() {
		return totalcreditEntryDollarAmount;
	}

	public void setTotalcreditEntryDollarAmount(String totalcreditEntryDollarAmount) {
		this.totalcreditEntryDollarAmount = totalcreditEntryDollarAmount;
	}

	public String getCompanyIdentification() {
		return companyIdentification;
	}

	public void setCompanyIdentification(String companyIdentification) {
		this.companyIdentification = companyIdentification;
	}

	public String getMessageAuthenticationCode() {
		return messageAuthenticationCode;
	}

	public void setMessageAuthenticationCode(String messageAuthenticationCode) {
		this.messageAuthenticationCode = messageAuthenticationCode;
	}

	public String getReserved() {
		return reserved;
	}

	public void setReserved(String reserved) {
		this.reserved = reserved;
	}

	public String getOrginatingFinancialInstitutionID() {
		return orginatingFinancialInstitutionID;
	}

	public void setOrginatingFinancialInstitutionID(String orginatingFinancialInstitutionID) {
		this.orginatingFinancialInstitutionID = orginatingFinancialInstitutionID;
	}

	public String getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	
	public IrFPBatchHeaderRecord getIrFPBatchHeaderRecord() {
		return irFPBatchHeaderRecord;
	}

	public void setIrFPBatchHeaderRecord(IrFPBatchHeaderRecord irFPBatchHeaderRecord) {
		this.irFPBatchHeaderRecord = irFPBatchHeaderRecord;
	}
	
	
	
	
}
