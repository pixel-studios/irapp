package com.irplus.dao.hibernate.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class IrMMenus implements java.io.Serializable {	
	
//	private IrMStatus irMStatus;
	private Integer menuid;
	private String menuname;
	private String description;	
	private String menuicon;
	private String modulepath;
	private Integer sortingorder;
	private Boolean ismodule;
	private Integer userid;
	private Date createddate;
	private Date modifieddate;
	private Integer isactive;
	
	private Set irMModulemenuses = new HashSet(0);
	
	
	
	private List<IrMModulemenus> moduleMenusList = new ArrayList<IrMModulemenus>();

	public IrMMenus() {
	}

	

/*	public IrMMenus(//IrMStatus irMStatus,
			String menuname, String description, String menuicon,
			String modulepath, Integer sortingorder, Boolean ismodule, Integer userid, Date createddate,
			Date modifieddate, Set irMModulemenuses) {
	
	//	this.irMStatus = irMStatus;
		this.menuname = menuname;
		this.description = description;
		this.menuicon = menuicon;
		this.modulepath = modulepath;
		this.sortingorder = sortingorder;
		this.ismodule = ismodule;
		this.userid = userid;
		this.createddate = createddate;
		this.modifieddate = modifieddate;
		this.irMModulemenuses = irMModulemenuses;
	}*/
	
	

	public Integer getMenuid() {
		return this.menuid;
	}

	public IrMMenus(Integer menuid, String menuname, String description, String menuicon, String modulepath,
		Integer sortingorder, Boolean ismodule, Integer userid, Date createddate, Date modifieddate, Integer isactive,
		Set irMModulemenuses) {
	super();
	this.menuid = menuid;
	this.menuname = menuname;
	this.description = description;
	this.menuicon = menuicon;
	this.modulepath = modulepath;
	this.sortingorder = sortingorder;
	this.ismodule = ismodule;
	this.userid = userid;
	this.createddate = createddate;
	this.modifieddate = modifieddate;
	this.isactive = isactive;
	this.irMModulemenuses = irMModulemenuses;
}

	public void setMenuid(Integer menuid) {
		this.menuid = menuid;
	}


	public String getMenuname() {
		return this.menuname;
	}

	public void setMenuname(String menuname) {
		this.menuname = menuname;
	}

	public Serializable getDescription() {
		return this.description;
	}
	public Integer getIsactive() {
		return isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public String getMenuicon() {
		return this.menuicon;
	}

	public void setMenuicon(String menuicon) {
		this.menuicon = menuicon;
	}

	public String getModulepath() {
		return this.modulepath;
	}

	public void setModulepath(String modulepath) {
		this.modulepath = modulepath;
	}

	public Integer getSortingorder() {
		return this.sortingorder;
	}

	public void setSortingorder(Integer sortingorder) {
		this.sortingorder = sortingorder;
	}

	public Boolean getIsmodule() {
		return this.ismodule;
	}

	public void setIsmodule(Boolean ismodule) {
		this.ismodule = ismodule;
	}

	public Integer getUserid() {
		return this.userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}



	public Set getIrMModulemenuses() {
		return irMModulemenuses;
	}



	public void setIrMModulemenuses(Set irMModulemenuses) {
		this.irMModulemenuses = irMModulemenuses;
	}

	

}