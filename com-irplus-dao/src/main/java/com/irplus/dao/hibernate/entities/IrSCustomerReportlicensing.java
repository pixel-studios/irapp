package com.irplus.dao.hibernate.entities;

import java.sql.Timestamp;

public class IrSCustomerReportlicensing implements java.io.Serializable {

	private Integer reportLicensingId;
	
	private IrMReportfileFormats irMReportfileFormats;
	private IrMUsers irMUsers;
	private IrSBankBranch irSBankbranch;
	private IrSCustomers irSCustomers;
	
	private Integer isactive;
	
	private Timestamp createddate;
	private Timestamp modifieddate;

	public IrSCustomerReportlicensing() {
	}

	public IrSCustomerReportlicensing(IrMReportfileFormats irMReportfileFormats, IrMUsers irMUsers,
			IrSBankBranch irSBankbranch, IrSCustomers irSCustomers, Integer isactive, Timestamp createddate,
			Timestamp modifieddate) {
		this.irMReportfileFormats = irMReportfileFormats;
		this.irMUsers = irMUsers;
		this.irSBankbranch = irSBankbranch;
		this.irSCustomers = irSCustomers;
		this.isactive = isactive;
		this.createddate = createddate;
		this.modifieddate = modifieddate;
	}

	public Integer getReportLicensingId() {
		return this.reportLicensingId;
	}

	public void setReportLicensingId(Integer reportLicensingId) {
		this.reportLicensingId = reportLicensingId;
	}

	public IrMReportfileFormats getIrMReportfileFormats() {
		return this.irMReportfileFormats;
	}

	public void setIrMReportfileFormats(IrMReportfileFormats irMReportfileFormats) {
		this.irMReportfileFormats = irMReportfileFormats;
	}

	public IrMUsers getIrMUsers() {
		return this.irMUsers;
	}

	public void setIrMUsers(IrMUsers irMUsers) {
		this.irMUsers = irMUsers;
	}

	public IrSBankBranch getIrSBankbranch() {
		return this.irSBankbranch;
	}

	public void setIrSBankbranch(IrSBankBranch irSBankbranch) {
		this.irSBankbranch = irSBankbranch;
	}

	public IrSCustomers getIrSCustomers() {
		return this.irSCustomers;
	}

	public void setIrSCustomers(IrSCustomers irSCustomers) {
		this.irSCustomers = irSCustomers;
	}

	public Integer getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public void setCreateddate(Timestamp createddate) {
		this.createddate = createddate;
	}

	public Timestamp getCreateddate() {
		return createddate;
	}

	public Timestamp getModifieddate() {
		return modifieddate;
	}

	public void setModifieddate(Timestamp modifieddate) {
		this.modifieddate = modifieddate;
	}

}