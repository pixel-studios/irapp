package com.irplus.dao.hibernate.entities;

import java.util.Date;

import com.irplus.dto.CustomerGroupingDetails;
import com.irplus.dto.DataEntryFormValidationDTO;

public class IrSFormvalidation implements java.io.Serializable {

	private Integer validationId;
	
	private IrMFieldtypes irMFieldtypes;
	private IrMUsers irMUsers;
	private IrSFormsetup irSFormsetup;
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	private String fieldType;
	private String fieldName;
	private Integer fieldLength;
	private Boolean hasValidation;
	private Integer isactive;
	
	private Date createddate;
	private Date modifieddate;

	public IrSFormvalidation() {
	}

	public IrSFormvalidation(DataEntryFormValidationDTO mappingInfo) {
	
		this.isactive= mappingInfo.getIsactive();
		
	}
	public IrSFormvalidation(int validationId) {
		this.validationId = validationId;
	}

	public IrSFormvalidation(int validationId, IrMFieldtypes irMFieldtypes, IrMUsers irMUsers,
			IrSFormsetup irSFormsetup, Integer fieldLength, Boolean hasValidation, Integer isactive, Date createddate,
			Date modifieddate) {
		this.validationId = validationId;
		this.irMFieldtypes = irMFieldtypes;
		this.irMUsers = irMUsers;
		this.irSFormsetup = irSFormsetup;
		this.fieldLength = fieldLength;
		this.hasValidation = hasValidation;
		this.isactive = isactive;
		this.createddate = createddate;
		this.modifieddate = modifieddate;
	}

	public Integer getValidationId() {
		return validationId;
	}

	public IrMFieldtypes getIrMFieldtypes() {
		return this.irMFieldtypes;
	}

	public void setIrMFieldtypes(IrMFieldtypes irMFieldtypes) {
		this.irMFieldtypes = irMFieldtypes;
	}

	public IrMUsers getIrMUsers() {
		return this.irMUsers;
	}

	public void setIrMUsers(IrMUsers irMUsers) {
		this.irMUsers = irMUsers;
	}

	public IrSFormsetup getIrSFormsetup() {
		return this.irSFormsetup;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public void setValidationId(Integer validationId) {
		this.validationId = validationId;
	}

	public void setIrSFormsetup(IrSFormsetup irSFormsetup) {
		this.irSFormsetup = irSFormsetup;
	}

	public Integer getFieldLength() {
		return this.fieldLength;
	}

	public void setFieldLength(Integer fieldLength) {
		this.fieldLength = fieldLength;
	}

	public Boolean getHasValidation() {
		return this.hasValidation;
	}

	public void setHasValidation(Boolean hasValidation) {
		this.hasValidation = hasValidation;
	}

	public Integer getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

}