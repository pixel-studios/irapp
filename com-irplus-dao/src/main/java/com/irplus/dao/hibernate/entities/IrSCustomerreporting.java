
package com.irplus.dao.hibernate.entities;

import java.util.Date;

public class IrSCustomerreporting  {

	private Integer customerReportId;
	
	private IrMUsers irMUsers;	
	private IrSBankBranch irSBankbranch;
	
	private IrSBankreporting irSBankreporting;
	
	private IrSCustomers irSCustomers;

	private String reportTime;
	private Integer isactive;
	private Date createddate;
	private Date modifieddate;
    
	public IrSCustomerreporting() {
	}
    
	public IrSCustomerreporting(IrMUsers irMUsers, //IrSBankbranch irSBankbranch, IrSBankreporting irSBankreporting,
			IrSCustomers irSCustomers, Date reportTime, Integer isactive, Date createddate, Date modifieddate) {
		this.irMUsers = irMUsers;
		
/*		this.irSBankbranch = irSBankbranch;
		this.irSBankreporting = irSBankreporting;
*/
		this.irSCustomers = irSCustomers;		
		this.isactive = isactive;
		this.createddate = createddate;
		this.modifieddate = modifieddate;
	}
    
	public Integer getCustomerReportId() {
		return this.customerReportId;
	}
    
	public void setCustomerReportId(Integer customerReportId) {
		this.customerReportId = customerReportId;
	}
    
	public IrMUsers getIrMUsers() {
		return this.irMUsers;
	}
    
	public void setIrMUsers(IrMUsers irMUsers) {
		this.irMUsers = irMUsers;
	}

	public IrSCustomers getIrSCustomers() {
		return this.irSCustomers;
	}

	public IrSBankBranch getIrSBankbranch() {
		return irSBankbranch;
	}

	public String getReportTime() {
		return reportTime;
	}

	public void setReportTime(String reportTime) {
		this.reportTime = reportTime;
	}

	public void setIrSBankbranch(IrSBankBranch irSBankbranch) {
		this.irSBankbranch = irSBankbranch;
	}

	public IrSBankreporting getIrSBankreporting() {
		return irSBankreporting;
	}

	public void setIrSBankreporting(IrSBankreporting irSBankreporting) {
		this.irSBankreporting = irSBankreporting;
	}

	public void setIrSCustomers(IrSCustomers irSCustomers) {
		this.irSCustomers = irSCustomers;
	}

	public Integer getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate){
		this.modifieddate = modifieddate;
	}
    
}