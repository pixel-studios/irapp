package com.irplus.dao.hibernate.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class IrMModulemenus implements java.io.Serializable {

	private int modulemenuid;
	
	
	private Integer isactive;
	private Integer sortingorder;
	private Date createddate;
	private Date modifieddate;
	private IrMMenus irMMenus;
	private IrMModules irMModules;
	private IrMUsers irMUsers;
	
	private Set<IrMRolepermissions> irMRolepermissionses = new HashSet<IrMRolepermissions>(0);

	public IrMModulemenus() {
	}

	public IrMModulemenus(int modulemenuid) {
		this.modulemenuid = modulemenuid;
	}

	public IrMModulemenus(int modulemenuid, IrMMenus irMMenus, IrMModules irMModules, //IrMStatus irMStatus,
		//	IrMUsers irMUsers,
			Integer sortingorder, Date createddate, Date modifieddate
			//Set<IrMRolepermissions> irMRolepermissionses
			) {
		this.modulemenuid = modulemenuid;
		this.irMMenus = irMMenus;
		this.irMModules = irMModules;
		this.irMUsers = irMUsers;
		this.sortingorder = sortingorder;
		this.createddate = createddate;
		this.modifieddate = modifieddate;
/*		this.irMRolepermissionses = irMRolepermissionses;*/
	}

	public int getModulemenuid() {
		return this.modulemenuid;
	}

	public void setModulemenuid(int modulemenuid) {
		this.modulemenuid = modulemenuid;
	}

	public IrMMenus getIrMMenus() {
		return this.irMMenus;
	}

	public void setIrMMenus(IrMMenus irMMenus) {
		this.irMMenus = irMMenus;
	}

	public IrMModules getIrMModules() {
		return this.irMModules;
	}

	public void setIrMModules(IrMModules irMModules) {
		this.irMModules = irMModules;
	}

	public Integer getIsactive() {
		return isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public IrMUsers getIrMUsers() {
		return irMUsers;
	}

	public void setIrMUsers(IrMUsers irMUsers) {
		this.irMUsers = irMUsers;
	}

	public Integer getSortingorder() {
		return this.sortingorder;
	}

	public void setSortingorder(Integer sortingorder) {
		this.sortingorder = sortingorder;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

	public Set<IrMRolepermissions> getIrMRolepermissionses() {
		return this.irMRolepermissionses;
	}

	public void setIrMRolepermissionses(Set<IrMRolepermissions> irMRolepermissionses) {
		this.irMRolepermissionses = irMRolepermissionses;
	}

}