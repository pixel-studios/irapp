package com.irplus.dao.rolepermissions;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.hibernate.entities.IrMModulemenus;
import com.irplus.dao.hibernate.entities.IrMRolepermissions;
import com.irplus.dao.hibernate.entities.IrMRoles;
import com.irplus.dao.hibernate.entities.IrMUsers;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.RolePrmUpdateNCreateInfo;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.rolepermission.RolepermissionsBean;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;

@Transactional
@Component

public class RolePermissionsDaoImpl implements IRolePermissionDao {

	private static final Log log = LogFactory.getLog(RolePermissionsDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	public Session getMyCurrentSession() {
		Session session = null;

		try {
			session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
			log.error("Exception raised DaoImpl :: At current session creation time ::" + e);
		}
		return session;
	}

	@Override
	public IRPlusResponseDetails createRolePermission(RolepermissionsBean rolepermissionsBean)
			throws BusinessException {

		log.info(" Inside RolepermissionDaoImpl ");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		IrMRolepermissions irMRolepermissions = new IrMRolepermissions();

		try {
			irMRolepermissions.setApproval(rolepermissionsBean.getApproval());
			irMRolepermissions.setAddprm(rolepermissionsBean.getAddprm());
			irMRolepermissions.setEditprm(rolepermissionsBean.getEditprm());
			irMRolepermissions.setDeleteprm(rolepermissionsBean.getDeleteprm());
			irMRolepermissions.setViewprm(rolepermissionsBean.getViewprm());

			irMRolepermissions.setIsactive(rolepermissionsBean.getIsactive());
			irMRolepermissions.setIrMModulemenus((IrMModulemenus) getMyCurrentSession().get(IrMModulemenus.class,
					rolepermissionsBean.getModulemenusId()));

			irMRolepermissions.setIrMRoles(
					(IrMRoles) getMyCurrentSession().get(IrMRoles.class, rolepermissionsBean.getRolesId()));
			// irMRolepermissions.setIrMStatus((IrMStatus)getMyCurrentSession().get(IrMStatus.class,
			// rolepermissionsBean.getStatusId()));
			irMRolepermissions
					.setIrMUsers((IrMUsers) getMyCurrentSession().get(IrMUsers.class, rolepermissionsBean.getUserId()));

			getMyCurrentSession().save(irMRolepermissions);

			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		} catch (Exception e) {
			log.error("Exception in createRolePermission", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails showAllRolePermission() throws BusinessException {

		log.info(" Inside RolepermissionDaoImpl :: show all rolepermissions");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		List<RolepermissionsBean> listRolePMBean = new ArrayList<RolepermissionsBean>();

		try {
			Criteria criteria = getMyCurrentSession().createCriteria(IrMRolepermissions.class);

			List<IrMRolepermissions> listIrmRPmsns = criteria.list();

			for (IrMRolepermissions irMRolepermissions : listIrmRPmsns) {

				RolepermissionsBean rolepermissionsBean = new RolepermissionsBean();

				rolepermissionsBean.setAddprm(irMRolepermissions.getAddprm());
				rolepermissionsBean.setApproval(irMRolepermissions.getApproval());
				rolepermissionsBean.setDeleteprm(irMRolepermissions.getDeleteprm());
				rolepermissionsBean.setEditprm(irMRolepermissions.getEditprm());
				rolepermissionsBean.setViewprm(irMRolepermissions.getViewprm());

				rolepermissionsBean.setIsactive(irMRolepermissions.getIsactive());
				rolepermissionsBean.setPermissionid(irMRolepermissions.getPermissionid());
				rolepermissionsBean.setRolesId(irMRolepermissions.getIrMRoles().getRoleid());

				rolepermissionsBean.setUserId(irMRolepermissions.getIrMUsers().getUserid());
				rolepermissionsBean.setModulemenusId(irMRolepermissions.getIrMModulemenus().getModulemenuid());
				rolepermissionsBean.setCreateddate(irMRolepermissions.getCreateddate());
				rolepermissionsBean.setModifieddate(irMRolepermissions.getModifieddate());
				rolepermissionsBean.setRoleName(irMRolepermissions.getIrMRoles().getRolename());

				listRolePMBean.add(rolepermissionsBean);
			}

			irPlusResponseDetails.setRolePermissonsBean(listRolePMBean);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);

			irPlusResponseDetails.setRecordsTotal(listIrmRPmsns.size());
			irPlusResponseDetails.setRecordsFiltered(listIrmRPmsns.size());

		} catch (Exception e) {

			log.error("Exception in show RolePermission", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getRolePermissionById(String RolepermissionsBeanId) throws BusinessException {

		log.info(" Inside RolepermissionDaoImpl ::deleteRolePermissionById");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		List<RolepermissionsBean> listRolePMBean = new ArrayList<RolepermissionsBean>();

		RolepermissionsBean rolepermissionsBean = new RolepermissionsBean();

		List<String> errMsg = new ArrayList<String>();

		IrMRolepermissions irMRolepermissions = null;
		try {
			irMRolepermissions = (IrMRolepermissions) getMyCurrentSession().get(IrMRolepermissions.class,
					Integer.parseInt(RolepermissionsBeanId));

			log.debug(" Inside dao ::  irMRolepermissions.getPermissionid() :" + irMRolepermissions.getPermissionid());
			if (irMRolepermissions != null) {

				rolepermissionsBean.setAddprm(irMRolepermissions.getAddprm());
				rolepermissionsBean.setApproval(irMRolepermissions.getApproval());
				rolepermissionsBean.setCreateddate(irMRolepermissions.getCreateddate());
				rolepermissionsBean.setDeleteprm(irMRolepermissions.getDeleteprm());
				rolepermissionsBean.setEditprm(irMRolepermissions.getEditprm());
				rolepermissionsBean.setModifieddate(irMRolepermissions.getModifieddate());
				rolepermissionsBean.setModulemenusId(irMRolepermissions.getIrMModulemenus().getModulemenuid());
				rolepermissionsBean.setPermissionid(irMRolepermissions.getPermissionid());
				rolepermissionsBean.setRolesId(irMRolepermissions.getIrMRoles().getRoleid());

				rolepermissionsBean.setIsactive(irMRolepermissions.getIsactive());
				rolepermissionsBean.setUserId(irMRolepermissions.getIrMUsers().getUserid());

				listRolePMBean.add(rolepermissionsBean);

				irPlusResponseDetails.setRolePermissonsBean(listRolePMBean);
				errMsg.add("Success :: Saved Data into Db");
				irPlusResponseDetails.setErrMsgs(errMsg);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);

			} else {
				log.debug("not get successful delete, no Rolepermission instance found");
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}

		} catch (Exception e) {
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception in find Rolepermission", e);
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateRolePermission(RolepermissionsBean rolepermissionsBean)
			throws BusinessException {

		log.info(" Inside  RolepermissionDaoImpl :: updateRolePermissionById" + rolepermissionsBean.getPermissionid());

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		try {
			IrMRolepermissions irMRolepermissions = (IrMRolepermissions) getMyCurrentSession()
					.get(IrMRolepermissions.class, rolepermissionsBean.getPermissionid());

			if (irMRolepermissions != null) {

				irMRolepermissions.setApproval(rolepermissionsBean.getApproval());
				irMRolepermissions.setAddprm(rolepermissionsBean.getAddprm());
				irMRolepermissions.setEditprm(rolepermissionsBean.getEditprm());
				irMRolepermissions.setDeleteprm(rolepermissionsBean.getDeleteprm());
				irMRolepermissions.setViewprm(rolepermissionsBean.getViewprm());

				irMRolepermissions.setIsactive(rolepermissionsBean.getIsactive());
				irMRolepermissions.setIrMModulemenus((IrMModulemenus) getMyCurrentSession().get(IrMModulemenus.class,
						rolepermissionsBean.getModulemenusId()));

				irMRolepermissions.setIrMRoles(
						(IrMRoles) getMyCurrentSession().get(IrMRoles.class, rolepermissionsBean.getRolesId()));
				
				irMRolepermissions.setIrMUsers(
						(IrMUsers) getMyCurrentSession().get(IrMUsers.class, rolepermissionsBean.getUserId()));

				getMyCurrentSession().update(irMRolepermissions);

				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			} else {
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}

		} catch (Exception e) {
			log.error("Exception in createRolePermission", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}

	
	@Override
	public IRPlusResponseDetails deleteRolePermissionById(String rolePermissionId) throws BusinessException {

		log.info(" Inside RolepermissionDaoImpl ::deleteRolePermissionById" + rolePermissionId);

		List<String> errMsg = new ArrayList<String>();
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		try {
			Query query = sessionFactory.getCurrentSession()
					.createQuery("delete IrMRolepermissions d where d.permissionid =:permission_Id");
			query.setParameter("permission_Id", Integer.parseInt(rolePermissionId));

			int result = query.executeUpdate();

				if (result == 0) {
	
					log.debug("delete country failed to delete");
					errMsg.add("Error : Row not Available");
					irPlusResponseDetails.setErrMsgs(errMsg);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
	
				} else {
					
					errMsg.add(" Success : Row Deleted ");
					irPlusResponseDetails.setErrMsgs(errMsg);				
					irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				}

		} catch (HibernateException e) {

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception in delete Country", e);
		}
		return irPlusResponseDetails;
	}
	
	/* 1/nov/2017 rolepermissions Dao*/
	
	
	/*	Status updated based on getRoleid,getRoleid matched then only update happen*/
	/*	This one Delete = isactive =2 */
	
	public IRPlusResponseDetails updateStatusRolePrmn(StatusIdInfo statusIdInfo)throws BusinessException {
		
		List<String> errMsg = new ArrayList<String>();

		log.info(" Inside RolepermissionDaoImpl :: updateStatusRoleprmn");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		boolean flag = false;
		
		IrMRolepermissions irMRprmDup = null;
		try {
			
			IrMRolepermissions irMRolepermissions = (IrMRolepermissions) getMyCurrentSession()
					.get(IrMRolepermissions.class, statusIdInfo.getId());
			
			Criteria criteria = getMyCurrentSession().createCriteria(IrMRolepermissions.class , "roleperm");
		criteria.createAlias("roleperm.irMRoles", "irMRoles");		
			criteria.add(Restrictions.eq("irMRoles.roleid" , statusIdInfo.getId()));	

			List<IrMRolepermissions> listIrmRPmsns = criteria.list();
			
			
			
			for (IrMRolepermissions irMRolepermissions2 : listIrmRPmsns) {
				
				/*if(irMRolepermissions2.getIrMRoles().getRoleid()==statusIdInfo.getId()){
					flag=true;
					irMRprmDup = irMRolepermissions2;
					break;
				}*/
				
				irMRolepermissions2.setIsactive(statusIdInfo.getStatus());
				getMyCurrentSession().update(irMRolepermissions2);
				
				errMsg.add("Success : isActive Status Updated succes"+statusIdInfo.getStatus() +" :"+irMRolepermissions2.getIrMModulemenus().getModulemenuid());
			}
			
			
			if (!listIrmRPmsns.isEmpty()) {
				
			/*	irMRprmDup.setIsactive(statusIdInfo.getStatus());				
				getMyCurrentSession().update(irMRolepermissions);
			*/	
				errMsg.add("Success :: Status Updated in Rolepermission : Status is:"+statusIdInfo.getStatus() +": RolePermissionId :"+irMRprmDup.getPermissionid());
				
				irPlusResponseDetails.setErrMsgs(errMsg);
				irPlusResponseDetails.setValidationSuccess(true);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			} else {
				errMsg.add("Error ::Not Status Updated in Rolepermission :: Not Included any Roleid in RolePermission ");
				irPlusResponseDetails.setErrMsgs(errMsg);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}
			
			
			/*if (irMRprmDup != null) {
				
				irMRprmDup.setIsactive(statusIdInfo.getStatus());				
				getMyCurrentSession().update(irMRolepermissions);
				
				errMsg.add("Success :: Status Updated in Rolepermission : Status is:"+statusIdInfo.getStatus() +": RolePermissionId :"+irMRprmDup.getPermissionid());
				
				irPlusResponseDetails.setErrMsgs(errMsg);
				irPlusResponseDetails.setValidationSuccess(true);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			} else {
				errMsg.add("Error ::Not Status Updated in Rolepermission :: Not Included any Roleid in RolePermission ");
				irPlusResponseDetails.setErrMsgs(errMsg);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}*/

		} catch (Exception e) {
			log.error("Exception in createRolePermission", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}
// 
	/*#1 . method show all role related modules*/
	
	/*2 show roleid matched then all related modules show*/
	
	/*statusid = role id*/
	
	/*  Delete = isactive = 2 */
	
	public IRPlusResponseDetails showAllRoleNMdl(StatusIdInfo statusIdInfo)	throws BusinessException {
		
		List<String> errMsg = new ArrayList<String>();

		log.info(" Inside RolepermissionDaoImpl ::updateStatusRoleprmn");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		boolean flag = false;
		
	//	IrMRolepermissions irMRprmDup = null;
		try {
			
			IrMRolepermissions irMRolepermissions = (IrMRolepermissions) getMyCurrentSession()
					.get(IrMRolepermissions.class, statusIdInfo.getId());
			
			Criteria criteria = getMyCurrentSession().createCriteria(IrMRolepermissions.class , "roleperm");
			criteria.createAlias("roleperm.irMRoles", "Roles");		
			criteria.add(Restrictions.eq("Roles.roleid" , statusIdInfo.getId()));	

			List<IrMRolepermissions> listIrmRPmsns = criteria.list();
			List<RolepermissionsBean> listRolebean = new ArrayList<RolepermissionsBean>();
			
			
			for (IrMRolepermissions irMRolepermissions2 : listIrmRPmsns) {
				
				if (irMRolepermissions2.getIsactive()==2) {
				
					errMsg.add("Message : This module is currently deleted state : isactive = 2"				
							+irMRolepermissions2.getIrMModulemenus().getIrMModules().getModulename());	
				}				
				if(irMRolepermissions2.getIsactive()==0 || irMRolepermissions2.getIsactive()==1){
				
				RolepermissionsBean rolprmnbn = new RolepermissionsBean();	
				
				rolprmnbn.setPermissionid(irMRolepermissions2.getPermissionid());
				rolprmnbn.setAddprm(irMRolepermissions2.getAddprm());
				rolprmnbn.setApproval(irMRolepermissions2.getApproval());
				rolprmnbn.setDeleteprm(irMRolepermissions2.getDeleteprm());
				rolprmnbn.setEditprm(irMRolepermissions2.getEditprm());
				rolprmnbn.setViewprm(irMRolepermissions2.getViewprm());
				rolprmnbn.setIsactive(irMRolepermissions2.getIsactive());
				rolprmnbn.setModulemenusId(irMRolepermissions2.getIrMModulemenus().getModulemenuid());
				rolprmnbn.setRoleName(irMRolepermissions2.getIrMRoles().getRolename());
				rolprmnbn.setRolesId(irMRolepermissions2.getIrMRoles().getRoleid());
				rolprmnbn.setUserId(irMRolepermissions2.getIrMUsers().getUserid());
			
				listRolebean.add(rolprmnbn);}
			}
			
			
			if (!listRolebean.isEmpty()) {
				
				irPlusResponseDetails.setRolePermissonsBean(listRolebean);
				errMsg.add("Success :: Show all roles in Rolepermission : mapped roleid is: "+statusIdInfo.getId());
				
				irPlusResponseDetails.setErrMsgs(errMsg);
				irPlusResponseDetails.setValidationSuccess(true);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			} else {
				errMsg.add("Error ::Not Matched any Roleid :: Not Included any Roleid in RolePermission ");
				irPlusResponseDetails.setErrMsgs(errMsg);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}

		} catch (Exception e) {
			log.error("Exception in createRolePermission", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}	
	//=======================================================
	
	/*Integer[] values = { 1, 3, 7 }; List<Integer> list = Arrays.asList(values);
	But then if you do something like this: 
		list.add(1);
	you get java.lang.UnsupportedOperationException
	*
	*   if add = delete=view=approval=0 then only isActive = 0 
	*
	*/
	
	
	/*update rolepermsn all 
	 * 
	 * update() or create()
	 * #2 method test 
	 * 
	 * */
	
	public IRPlusResponseDetails updateRoleprmUpdNCrt(RolePrmUpdateNCreateInfo rolepermUpNCrte)	throws BusinessException {
		
		List<String> errMsgs = new ArrayList<String>();

		log.info(" Inside RolepermissionDaoImpl ::updateStatusRoleprmn");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		boolean flag = false;
		
		IrMRolepermissions irMRprmDup = null;
		try {
			
		/*	IrMRolepermissions irMRolepermissions = (IrMRolepermissions) getMyCurrentSession()
					.get(IrMRolepermissions.class, statusIdInfo.getId());*/
						
			Criteria criteria = getMyCurrentSession().createCriteria(IrMRolepermissions.class , "roleperm");
			
			criteria.createAlias("roleperm.irMRoles","role" );		
			
			criteria.add(Restrictions.eq("role.roleid" , rolepermUpNCrte.getRolesId()));	//getting role
			
			List<IrMRolepermissions> RolepermsnsList = criteria.list();                //db contain roles-menumodule getting
		
			RolepermissionsBean roleprmSn[] = rolepermUpNCrte.getRoleprmArray();      //ui contans role-menumodule 
			boolean cap = false;
			
			
			if(rolepermUpNCrte.getRolesId()!= null){
				
				for (RolepermissionsBean UirolepermBean : roleprmSn) {    // 1 ,  3  ui data
					
																			//loop  (1  equals -- 2 , 3)  ... flag =false; if(flag==false){create} 
					cap = false;
					if(UirolepermBean.getModulemenusId() ==null){
						errMsgs.add("Validation Error : Please Enter the MenunmoduleId : Not Entered the MenuModuleid");
						break;
					}														//loop  (3  ==	 2 , 3  true update  break; if(flag){break}
					if(UirolepermBean.getRolesId() ==null){
						errMsgs.add("Validation Error : Please Enter the RoleId : Not Entered the Roleid");
						break;
					}
					if(UirolepermBean.getUserId() ==null){
						errMsgs.add("Validation Error : Please Enter the RoleId : Not Entered the Roleid");
						break;
					}
																			//3 ==   2 , 3
				for (IrMRolepermissions dbroleprmsn : RolepermsnsList) {   // db data //   2 , 3
													
					if(UirolepermBean.getPermissionid() ==  dbroleprmsn.getPermissionid() && UirolepermBean.getModulemenusId() == dbroleprmsn.getIrMModulemenus().getModulemenuid()
							&& UirolepermBean.getRolesId() == dbroleprmsn.getIrMRoles().getRoleid()						
							){    //if contains updating content
						
						dbroleprmsn.setIrMModulemenus((IrMModulemenus)getMyCurrentSession().get(IrMModulemenus.class , UirolepermBean.getModulemenusId()));
						dbroleprmsn.setIrMRoles((IrMRoles)getMyCurrentSession().get(IrMRoles.class , UirolepermBean.getRolesId()));
						
						dbroleprmsn.setIrMUsers((IrMUsers)getMyCurrentSession().get(IrMUsers.class , UirolepermBean.getUserId()));
			
						dbroleprmsn.setViewprm(UirolepermBean.getViewprm());
						dbroleprmsn.setAddprm(UirolepermBean.getAddprm());
						dbroleprmsn.setApproval(UirolepermBean.getApproval());
						dbroleprmsn.setDeleteprm(UirolepermBean.getDeleteprm());
						dbroleprmsn.setEditprm(UirolepermBean.getEditprm());
						
								if( UirolepermBean.getEditprm()==true ||   UirolepermBean.getViewprm() == true ||
										UirolepermBean.getDeleteprm()==true || UirolepermBean.getApproval()) {
										
									dbroleprmsn.setIsactive(1);
								}else{
									dbroleprmsn.setIsactive(0);	
								}
						
								getMyCurrentSession().update(dbroleprmsn);
								errMsgs.add("Success : 1 Updated success - rolepermissionid :"+dbroleprmsn.getPermissionid());
						cap = true;						
					} //if 
					// 2nd condition 
					if(UirolepermBean.getPermissionid() == null && cap == false &&
							UirolepermBean.getModulemenusId() == dbroleprmsn.getIrMModulemenus().getModulemenuid()
							&& UirolepermBean.getRolesId() == dbroleprmsn.getIrMRoles().getRoleid()){
						
						
						
						dbroleprmsn.setIrMModulemenus((IrMModulemenus)getMyCurrentSession().get(IrMModulemenus.class , UirolepermBean.getModulemenusId()));
						dbroleprmsn.setIrMRoles((IrMRoles)getMyCurrentSession().get(IrMRoles.class , UirolepermBean.getRolesId()));
						dbroleprmsn.setIrMUsers((IrMUsers)getMyCurrentSession().get(IrMUsers.class , UirolepermBean.getUserId()));
			
						dbroleprmsn.setViewprm(UirolepermBean.getViewprm());
						dbroleprmsn.setAddprm(UirolepermBean.getAddprm());
						dbroleprmsn.setApproval(UirolepermBean.getApproval());
						dbroleprmsn.setDeleteprm(UirolepermBean.getDeleteprm());
						dbroleprmsn.setEditprm(UirolepermBean.getEditprm());
						
								if( UirolepermBean.getEditprm()==true ||   UirolepermBean.getViewprm()==true ||
										UirolepermBean.getDeleteprm()==true || UirolepermBean.getApproval()) {
										
								dbroleprmsn.setIsactive(1);
								}else{
									dbroleprmsn.setIsactive(0);	
								}
						
								getMyCurrentSession().update(dbroleprmsn);
								errMsgs.add("Success : 2 Updated success - rolepermissionid :"+dbroleprmsn.getPermissionid());
								cap = true;	
						
					}//if
					// 3 rd condition 
					if(cap == false && UirolepermBean.getModulemenusId() == dbroleprmsn.getIrMModulemenus().getModulemenuid()
							&& UirolepermBean.getRolesId() == dbroleprmsn.getIrMRoles().getRoleid()
							
							){
						
						IrMRolepermissions irmp = new IrMRolepermissions();
						
						irmp.setAddprm(UirolepermBean.getAddprm());
						irmp.setApproval(UirolepermBean.getApproval());
						
						irmp.setDeleteprm(UirolepermBean.getDeleteprm());
						irmp.setEditprm(UirolepermBean.getEditprm());
						
						if(UirolepermBean.getModulemenusId()!=null){
						irmp.setIrMModulemenus((IrMModulemenus)getMyCurrentSession().get(IrMModulemenus.class , UirolepermBean.getModulemenusId()));
						}
						else{errMsgs.add("Validation Error : ModulemenuId not available or Not Entered");}
						
						if(UirolepermBean.getRolesId()!=null){
						irmp.setIrMRoles((IrMRoles)getMyCurrentSession().get(IrMRoles.class , UirolepermBean.getRolesId()));
						}
						else{errMsgs.add("Validation Error : RoleId not available or Not Entered");}
						
						if(UirolepermBean.getUserId()!=null){
						irmp.setIrMUsers((IrMUsers)getMyCurrentSession().get(IrMUsers.class , UirolepermBean.getUserId()));
						}
						else{errMsgs.add("Validation Error : UserId not available or Not Entered");}
										
						irmp.setViewprm(UirolepermBean.getViewprm());
						
						if(UirolepermBean.getAddprm()==true ||UirolepermBean.getApproval() ==true|| 
								UirolepermBean.getDeleteprm()==true || UirolepermBean.getEditprm() == true){
							irmp.setIsactive(1);
						}else{
							irmp.setIsactive(0);
						}
						
						getMyCurrentSession().save(irmp);
						
						errMsgs.add("Success : new row created Rolepermission : Under role id :"+UirolepermBean.getRolesId());
						cap = true;
					} 
														
					
				} //inner for loop
						// not contains permmisionid then create new row
				
						if(UirolepermBean.getPermissionid()==null && cap == false 
						//	&&	UirolepermBean.getRolesId()== 								
								){
						
							IrMRolepermissions irmp = new IrMRolepermissions();
							
							irmp.setAddprm(UirolepermBean.getAddprm());
							irmp.setApproval(UirolepermBean.getApproval());
							
							irmp.setDeleteprm(UirolepermBean.getDeleteprm());
							irmp.setEditprm(UirolepermBean.getEditprm());
							
							if(UirolepermBean.getModulemenusId()!=null){
							irmp.setIrMModulemenus((IrMModulemenus)getMyCurrentSession().get(IrMModulemenus.class , UirolepermBean.getModulemenusId()));
							}
							else{errMsgs.add("Validation Error : ModulemenuId not available or Not Entered");}
							
							if(UirolepermBean.getRolesId()!=null){
							irmp.setIrMRoles((IrMRoles)getMyCurrentSession().get(IrMRoles.class , UirolepermBean.getRolesId()));
							}
							else{errMsgs.add("Validation Error : RoleId not available or Not Entered");}
							
							if(UirolepermBean.getUserId()!=null){
							irmp.setIrMUsers((IrMUsers)getMyCurrentSession().get(IrMUsers.class , UirolepermBean.getUserId()));
							}
							else{errMsgs.add("Validation Error : UserId not available or Not Entered");}
											
							irmp.setViewprm(UirolepermBean.getViewprm());
							
							if(UirolepermBean.getAddprm()==true ||UirolepermBean.getApproval() ==true|| 
									UirolepermBean.getDeleteprm()==true || UirolepermBean.getEditprm() == true){
								irmp.setIsactive(1);
							}else{
								irmp.setIsactive(0);
							}
							
							getMyCurrentSession().save(irmp);
							
							errMsgs.add("Success : new row created Rolepermission : Under role id :"+UirolepermBean.getRolesId());
						} 
						
				}// UI : for loop close 
				
				errMsgs.add(" Success :: Exccution  Success  RolePermission ");
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setValidationSuccess(true);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			} else {
				errMsgs.add("Validation Error ::Not Entered Role :: Not Included any Roleid in RolePermission ");
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
			}
						
		} catch (Exception e) {
			log.error("Exception in createRolePermission", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}	
		
}