package com.irplus.dao.customer_contacts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.hibernate.entities.IrMUsers;
import com.irplus.dao.hibernate.entities.IrSBankBranch;
import com.irplus.dao.hibernate.entities.IrSBankContact;
import com.irplus.dao.hibernate.entities.IrSCustomercontacts;
import com.irplus.dao.hibernate.entities.IrSCustomers;
import com.irplus.dto.ContactInfo;
import com.irplus.dto.CustomeContactsArrayInfo;
import com.irplus.dto.CustomerBean;
import com.irplus.dto.CustomerContactsBean;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;

@Transactional
@Component
public class CustomerContactsDaoImpl implements ICustomerContactsDao{

private static final Logger log = Logger.getLogger(CustomerContactsDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public Session getMyCurrentSession(){
		
		Session session =null;
		
		try {	
		
			session=sessionFactory.getCurrentSession();
		
		}catch (HibernateException e) {
		log.error("Exception raised DaoImpl :: At current session creation time ::"+e);
		}		
		return session; 
	}


	@Override
	public IRPlusResponseDetails createCustomerContacts(CustomerContactsBean customerContactsBean)throws BusinessException {

		log.info("Inside CustomerContatcsDaoImpl");
		log.debug("Inside CustomerContatcsDaoImpl");
		
	
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
	/*	List<String> errMsgs = new ArrayList<String>();
		
		
		
		try {			
			for (int i=0;i<customerContactsBean.getCustomerId()) {						
			
			IrSCustomercontacts irSCustomercontacts = new IrSCustomercontacts();
			
			irSCustomercontacts.setAddress1(customerContactsBean.getAddress1());
			irSCustomercontacts.setAddress2(customerContactsBean.getAddress2());
			irSCustomercontacts.setCity(customerContactsBean.getCity());
			irSCustomercontacts.setContactNo(customerContactsBean.getContactNo());
			irSCustomercontacts.setContactRole(customerContactsBean.getContactRole());
			irSCustomercontacts.setCountry(customerContactsBean.getCountry());
			irSCustomercontacts.setCreateddate(customerContactsBean.getCreateddate());
			
			irSCustomercontacts.setCustomerContactId(customerContactsBean.getCustomerContactId());
			
			irSCustomercontacts.setEmailId(customerContactsBean.getEmailId());
			irSCustomercontacts.setFirstName(customerContactsBean.getFirstName());
			irSCustomercontacts.setIrMUsers((IrMUsers)getMyCurrentSession().get(IrMUsers.class, customerContactsBean.getUsersId()));
			irSCustomercontacts.setIrSCustomers((IrSCustomers)getMyCurrentSession().get(IrSCustomers.class ,customerContactsBean.getCustomerId()));
			irSCustomercontacts.setIsactive(customerContactsBean.getIsactive());
			irSCustomercontacts.setIsdefault(customerContactsBean.getIsdefault());
			irSCustomercontacts.setLastName(customerContactsBean.getLastName());
			irSCustomercontacts.setModifieddate(customerContactsBean.getModifieddate());
			irSCustomercontacts.setState(customerContactsBean.getState());
			irSCustomercontacts.setZipcode(customerContactsBean.getZipcode());
			
			getMyCurrentSession().save(irSCustomercontacts);
			
			}
			
			errMsgs.add("Success : Saved Data ");
			irPlusResponseDetails.setValidationSuccess(true);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);

		}
		catch (Exception e) {
			log.error("Exception in create CustomerContacts",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}*/
		
		return irPlusResponseDetails;
	}


	@Override
	public IRPlusResponseDetails updateCustomerConstatnts(CustomerContactsBean customerContactsBean) throws BusinessException {
	
		log.info("Inside RoleDaoImpl");
		log.debug("Update Role instance");
					
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		IrSCustomercontacts irSCustomercontacts = null;
		
		try{	
		irSCustomercontacts = (IrSCustomercontacts) getMyCurrentSession().get(IrSCustomercontacts.class, customerContactsBean.getCustomerContactId());	
		
		irSCustomercontacts.setAddress1(customerContactsBean.getAddress1());
		irSCustomercontacts.setAddress2(customerContactsBean.getAddress2());
		irSCustomercontacts.setCity(customerContactsBean.getCity());
		irSCustomercontacts.setContactNo(customerContactsBean.getContactNo());
		irSCustomercontacts.setContactRole(customerContactsBean.getContactRole());
		irSCustomercontacts.setCountry(customerContactsBean.getCountry());
		irSCustomercontacts.setCreateddate(customerContactsBean.getCreateddate());
		irSCustomercontacts.setCustomerContactId(customerContactsBean.getCustomerContactId());
		irSCustomercontacts.setEmailId(customerContactsBean.getEmailId());
		irSCustomercontacts.setFirstName(customerContactsBean.getFirstName());
		irSCustomercontacts.setIrMUsers((IrMUsers)getMyCurrentSession().get(IrSCustomercontacts.class , customerContactsBean.getUsersId()));		
		irSCustomercontacts.setIrSCustomers((IrSCustomers)getMyCurrentSession().get(IrSCustomercontacts.class,customerContactsBean.getCustomerId()));
		irSCustomercontacts.setIsactive(customerContactsBean.getIsactive());
		irSCustomercontacts.setIsdefault(customerContactsBean.getIsdefault());
		irSCustomercontacts.setLastName(customerContactsBean.getLastName());
		irSCustomercontacts.setModifieddate(customerContactsBean.getModifieddate());
		irSCustomercontacts.setState(customerContactsBean.getState());
		irSCustomercontacts.setZipcode(customerContactsBean.getZipcode());
		
		getMyCurrentSession().update(irSCustomercontacts);
		
		irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		irPlusResponseDetails.setValidationSuccess(true);
		
		}catch (Exception e) {

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception Raised inside customerDaoImpl :: updateCustomerContactsId : "+e);
		
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}


	public IRPlusResponseDetails updateCustomerContacts(CustomerContactsBean[] customerContacts) throws BusinessException {
		
		log.info("Inside updateContact ContactInfo :"+customerContacts);

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		session = sessionFactory.getCurrentSession();

	
		
		IrSCustomers Customer = null;

		

		CustomerContactsBean contactInfo = null;

		try
		{
		
			
		contactInfo = customerContacts[0];
			
			Integer CustomerId=contactInfo.getCustomerId();
			deleteExistingCustContacts(CustomerId);
			
			
			if(customerContacts.length>0)
			{
				Customer = (IrSCustomers) session.get(IrSCustomers.class,customerContacts[0].getCustomerId());
				log.debug("Received bankBranch Info :"+Customer);

			}

			for(int i=0;i<customerContacts.length;i++)
			{
				IrSCustomercontacts CustomerContact = new IrSCustomercontacts(customerContacts[i]);
				CustomerContact.setIrSCustomers(Customer);
				session.save(CustomerContact);
				session.flush();
				log.debug("Added bankContact Info :"+CustomerContact);
			}
			
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			
			
			
			/*	
			
			for(int i=0;i<customerContacts.length;i++)
			{
				contactInfo = customerContacts[i];
				
				log.info("Inside updateContact ContactInfo :"+contactInfo);
				
				irSCustomercontacts = null;
				
				if(contactInfo.getCustomerContactId()!=null)
				{
					irSCustomercontacts = (IrSCustomercontacts) session.createQuery("from IrSCustomercontacts contact where contact.customerContactId=:contactId").setParameter("contactId",contactInfo.getCustomerContactId()).uniqueResult();

				}
				
				if(irSCustomercontacts==null)
				{
					irSCustomercontacts = new IrSCustomercontacts();
					irSCustomercontacts.setFirstName(contactInfo.getFirstName());
					irSCustomercontacts.setLastName(contactInfo.getLastName());
					irSCustomercontacts.setContactRole(contactInfo.getContactRole());
					irSCustomercontacts.setContactNo(contactInfo.getContactNo());
					irSCustomercontacts.setEmailId(contactInfo.getEmailId());
					irSCustomercontacts.setAddress1(contactInfo.getAddress1());
					irSCustomercontacts.setAddress2(contactInfo.getAddress2());
					irSCustomercontacts.setIsdefault(contactInfo.getIsdefault());
					irSCustomercontacts.setCountry(contactInfo.getCountry());
					irSCustomercontacts.setState(contactInfo.getState());
					irSCustomercontacts.setCity(contactInfo.getCity());
					irSCustomercontacts.setZipcode(contactInfo.getZipcode());
					irSCustomercontacts.setIsactive(contactInfo.getIsactive());
					log.info("InsideUpdate contacts Adding new ContactInfo :"+customerContacts);
					session.save(irSCustomercontacts);
				}
				else
				{
					irSCustomercontacts.setFirstName(contactInfo.getFirstName());
					irSCustomercontacts.setLastName(contactInfo.getLastName());
					irSCustomercontacts.setContactRole(contactInfo.getContactRole());
					irSCustomercontacts.setContactNo(contactInfo.getContactNo());
					irSCustomercontacts.setEmailId(contactInfo.getEmailId());
					irSCustomercontacts.setAddress1(contactInfo.getAddress1());
					irSCustomercontacts.setAddress2(contactInfo.getAddress2());
					irSCustomercontacts.setIsdefault(contactInfo.getIsdefault());
					irSCustomercontacts.setCountry(contactInfo.getCountry());
					irSCustomercontacts.setState(contactInfo.getState());
					irSCustomercontacts.setCity(contactInfo.getCity());
					irSCustomercontacts.setZipcode(contactInfo.getZipcode());
					irSCustomercontacts.setIsactive(contactInfo.getIsactive());
					session.update(irSCustomercontacts);
				}
	
				session.flush();
				log.debug("updated bankContact Info :"+irSCustomercontacts);
			}*/
			
		

		
		
/*		try{
			CustomerContactsBean[] customerContactsBeans = customeContactsArrayInfo.getCustomerContactsBean();
			
			if(customerContactsBeans.length==0){				
				errMsgs.add(" Validation Error - No Contacts Available ");
				irPlusResponseDetails.setValidationSuccess(true);
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				
			}else{
				
				for (CustomerContactsBean customerContactsBeanVar : customerContactsBeans) {
					
					if(customerContactsBeanVar.getCustomerId()!=null && customerContactsBeanVar.getCustomerContactId()!=null){
						
					IrSCustomercontacts irsc =(IrSCustomercontacts) getMyCurrentSession().get(IrSCustomercontacts.class , customerContactsBeanVar.getCustomerContactId());
					
					if(customerContactsBeanVar.getAddress1()!=null){
					irsc.setAddress1(customerContactsBeanVar.getAddress1());}
					if(customerContactsBeanVar.getAddress2()!=null){
					irsc.setAddress2(customerContactsBeanVar.getAddress2());}
					if(customerContactsBeanVar.getCity()!=null){
					irsc.setCity(customerContactsBeanVar.getCity());}
					if(customerContactsBeanVar.getContactNo()!=null){
					irsc.setContactNo(customerContactsBeanVar.getContactNo());}
					if(customerContactsBeanVar.getContactRole()!=null){
					irsc.setContactRole(customerContactsBeanVar.getContactRole());}
					if(customerContactsBeanVar.getCountry()!=null){
					irsc.setCountry(customerContactsBeanVar.getCountry());}
					if(customerContactsBeanVar.getEmailId()!=null){
					irsc.setEmailId(customerContactsBeanVar.getEmailId());}
					if(customerContactsBeanVar.getFirstName()!=null){
					irsc.setFirstName(customerContactsBeanVar.getFirstName());}
					if(customerContactsBeanVar.getLastName()!=null){
					irsc.setLastName(customerContactsBeanVar.getLastName());}
					if(customerContactsBeanVar.getZipcode()!=null){
					irsc.setZipcode(customerContactsBeanVar.getZipcode());	}				
					if(customerContactsBeanVar.getIsactive()!=null){
					irsc.setIsactive(customerContactsBeanVar.getIsactive());}
					if(customerContactsBeanVar.getState()!=null){
					irsc.setState(customerContactsBeanVar.getState());}
					
					if(customerContactsBeanVar.getUsersId()!=null){
					irsc.setIrMUsers((IrMUsers)getMyCurrentSession().get(IrMUsers.class, customerContactsBeanVar.getUsersId()));}
					
					if(customerContactsBeanVar.getCustomerId()!=null){
					irsc.setIrSCustomers((IrSCustomers)getMyCurrentSession().get(IrSCustomers.class,customerContactsBeanVar.getCustomerId() ));}
				
					}else{
						errMsgs.add(" Validation Error : CustomerId or ContactId Missed ");						
						irPlusResponseDetails.setErrMsgs(errMsgs);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
						irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);		
					}					
				}
				
				errMsgs.add(" Success : Updated ");
				irPlusResponseDetails.setValidationSuccess(true);
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				}*/
			
		}catch (Exception e) {
	
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception Raised inside customerDaoImpl :: updateCustomerContactsId : "+e);
		
			throw new HibernateException(e);
		}
	return irPlusResponseDetails;
	}

	private void deleteExistingCustContacts(Integer CustomerId) 
	{
		

		Session session = null;

		try
		{
		
			session = sessionFactory.getCurrentSession();

			Query query = session.createQuery("delete from IrSCustomercontacts cnct where cnct.irSCustomers.customerId=:customerId");

			query.setParameter("customerId",CustomerId);

			query.executeUpdate();
			
		
			
			
		}
		catch(Exception e)
		{
			log.error("Exception in deleteExistingContacts",e);

			throw new HibernateException(e);
		}	

	}
	
	@Override
	public IRPlusResponseDetails getCustomerContactsById(CustomerBean customerBean) throws BusinessException {

		log.info("Inside customer contact getcustomer by id ");
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		IrSCustomercontacts customerContact =null;
		
		CustomerContactsBean contactInfo  = null;
		List<IrSCustomercontacts> customerContactList =null;
		
		
		List<CustomerContactsBean> contactsList = new ArrayList<CustomerContactsBean>();
		
		try
		{
			Query query = getMyCurrentSession().createQuery("from IrSCustomercontacts contact where contact.irSCustomers.customerId=?");

			query.setParameter(0,customerBean.getCustomerId());

			customerContactList = (List<IrSCustomercontacts>) query.list();

			Iterator<IrSCustomercontacts> customerCtctItr = customerContactList.iterator();
			
			log.debug("Contact Size:"+customerContactList.size());

			while (customerCtctItr.hasNext()) {
				customerContact =  customerCtctItr.next();

				contactInfo = new CustomerContactsBean();
				
				contactInfo.setCustomerContactId(customerContact.getCustomerContactId());
				contactInfo.setFirstName(customerContact.getFirstName());
				contactInfo.setLastName(customerContact.getLastName());
				contactInfo.setContactRole(customerContact.getContactRole());
				contactInfo.setContactNo(customerContact.getContactNo());
				contactInfo.setEmailId(customerContact.getEmailId());
				contactInfo.setAddress1(customerContact.getAddress1());
				contactInfo.setAddress2(customerContact.getAddress2());
				contactInfo.setIsdefault(customerContact.getIsdefault());
				contactInfo.setCountry(customerContact.getCountry());
				contactInfo.setState(customerContact.getState());
				contactInfo.setCity(customerContact.getCity());
				contactInfo.setZipcode(customerContact.getZipcode());
				contactInfo.setIsactive(customerContact.getIsactive());
				contactInfo.setCustomerId(customerContact.getIrSCustomers().getCustomerId());

				contactsList.add(contactInfo);

			}
			CustomerContactsBean[] contactArray = new CustomerContactsBean[contactsList.size()];
			customerBean.setCustomerContact(contactsList.toArray(contactArray));
			irPlusResponseDetails.setCustomer(customerBean);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		

			/*
			irSCustomercontacts =(IrSCustomercontacts) getMyCurrentSession().get(IrSCustomercontacts.class,customerBean.getCustomerId());
			
			if (irSCustomercontacts!=null) {			 
				
				customerContactsBean.setAddress1(irSCustomercontacts.getAddress1());
				customerContactsBean.setAddress2(irSCustomercontacts.getAddress2());
				customerContactsBean.setCity(irSCustomercontacts.getCity());
				customerContactsBean.setContactNo(irSCustomercontacts.getContactNo());
				customerContactsBean.setContactRole(irSCustomercontacts.getContactRole());
				customerContactsBean.setCountry(irSCustomercontacts.getCountry());
				customerContactsBean.setCreateddate(irSCustomercontacts.getCreateddate());
				customerContactsBean.setCustomerContactId(irSCustomercontacts.getCustomerContactId());
				customerContactsBean.setCustomerId(irSCustomercontacts.getIrSCustomers().getCustomerId());
				customerContactsBean.setEmailId(irSCustomercontacts.getEmailId());
				customerContactsBean.setFirstName(irSCustomercontacts.getFirstName());
				customerContactsBean.setIsactive(irSCustomercontacts.getIsactive());
				customerContactsBean.setIsdefault(irSCustomercontacts.getIsdefault());
				customerContactsBean.setLastName(irSCustomercontacts.getLastName());
				customerContactsBean.setModifieddate(irSCustomercontacts.getModifieddate());
				customerContactsBean.setState(irSCustomercontacts.getState());
				customerContactsBean.setUsersId(irSCustomercontacts.getIrMUsers().getUserid());
				customerContactsBean.setZipcode(irSCustomercontacts.getZipcode());
	
				listCustomContactBean.add(customerContactsBean);
				
				irPlusResponseDetails.setCustomerContactsBean(listCustomContactBean);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			} else {
				
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);				
			}*/
			
		} catch (Exception e) {

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception Raised inside customerContacts DaoImple :: "+e);
			throw new HibernateException(e);
		}
				
		return irPlusResponseDetails;
	}

//Delete Query modified status
	@Override
	public IRPlusResponseDetails deleteCustomerContactsById(String customerContactsId) throws BusinessException {
		
		log.info("Inside customerContactsDaoImpl");
		log.debug("deleting CustomerContacts instance");
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		try{	
			
	//		Query query = getMyCurrentSession().createQuery("delete IrSCustomercontacts d where d.customerContactId =:cc_Id");
	//		query.setParameter("cc_Id" , Integer.parseInt(customerContactsId));			
	//		int result=query.executeUpdate();
			
			Integer id = Integer.parseInt(customerContactsId);
			
			if(id!=null){
				
				IrSCustomercontacts contacts = (IrSCustomercontacts)getMyCurrentSession().get(IrSCustomercontacts.class,id);
			
				contacts.setIsactive(2);
			
				log.debug(" Deleted contacts ");
				irPlusResponseDetails.setValidationSuccess(true);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				
			}
			
		}catch (Exception e) {
			log.error("Exception raised in delete customer",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}


	@Override
	public IRPlusResponseDetails showAllCustomerContacts() throws BusinessException {
	
		log.info("Inside customerContactsDaoImpl");
		log.debug("Show all customersContacts instances");
		
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
		List<IrSCustomercontacts> listCustomBean = new ArrayList<IrSCustomercontacts>();
		
		try {

			List<CustomerContactsBean> listCustomContactBean = new ArrayList<CustomerContactsBean>();
			
			Criteria criteria = getMyCurrentSession().createCriteria(IrSCustomercontacts.class);
			List<IrSCustomercontacts> list = criteria.list();
			
			if (!list.isEmpty()) {
							
				for (IrSCustomercontacts irSCustomercontacts : list) {
			
					CustomerContactsBean customerContactsBean  = new CustomerContactsBean();
					
					customerContactsBean.setAddress1(irSCustomercontacts.getAddress1());
					customerContactsBean.setAddress2(irSCustomercontacts.getAddress2());
					customerContactsBean.setCity(irSCustomercontacts.getCity());
					customerContactsBean.setContactNo(irSCustomercontacts.getContactNo());
					customerContactsBean.setContactRole(irSCustomercontacts.getContactRole());
					customerContactsBean.setCountry(irSCustomercontacts.getCountry());
					customerContactsBean.setCreateddate(irSCustomercontacts.getCreateddate());
					customerContactsBean.setCustomerContactId(irSCustomercontacts.getCustomerContactId());
					customerContactsBean.setCustomerId(irSCustomercontacts.getIrSCustomers().getCustomerId());
					customerContactsBean.setEmailId(irSCustomercontacts.getEmailId());
					customerContactsBean.setFirstName(irSCustomercontacts.getFirstName());
					customerContactsBean.setIsactive(irSCustomercontacts.getIsactive());
					customerContactsBean.setIsdefault(irSCustomercontacts.getIsdefault());
					customerContactsBean.setLastName(irSCustomercontacts.getLastName());
					customerContactsBean.setModifieddate(irSCustomercontacts.getModifieddate());
					customerContactsBean.setState(irSCustomercontacts.getState());
					customerContactsBean.setUsersId(irSCustomercontacts.getIrMUsers().getUserid());
					customerContactsBean.setZipcode(irSCustomercontacts.getZipcode());
					
					
					listCustomContactBean.add(customerContactsBean);	
				}

				irPlusResponseDetails.setCustomerContactsBean(listCustomContactBean);
			
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				
			} else {
				log.debug("CustomerContacts not available :: Empty Data :: Data required to showAll CustomerContacts");
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}
			
		} catch (Exception e) {
			log.error("Exception raised in show list CustomerContacts",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			throw new HibernateException(e);
		}
		
		return irPlusResponseDetails;
	}


	@Override
	public IRPlusResponseDetails addContacts(CustomerContactsBean[] customerContactsBean) throws BusinessException {
		log.info("Inside ContactMgmtDaoImpl");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;
		
		session = sessionFactory.getCurrentSession();
		IrMUsers user = new IrMUsers();
		IrSCustomers addcontacts=null;
		
		try
		{
			if(customerContactsBean.length>0)
			{
				addcontacts = (IrSCustomers) session.get(IrSCustomers.class,customerContactsBean[0].getCustomerId());
				log.debug("Received customer Contact Info :"+addcontacts);

			}
			for(int i=0;i<customerContactsBean.length;i++)
			{
				IrSCustomercontacts customerContacts = new IrSCustomercontacts(customerContactsBean[i]);
				
				customerContacts.setIrSCustomers(addcontacts);
				user.setUserid(customerContactsBean[0].getUsersId());
				customerContacts.setIrMUsers(user);
				session.save(customerContacts);
				session.flush();
				log.debug("Added bankContact Info :"+customerContacts);
			}
			
		
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		
		}catch(Exception e)
		{
			log.error("Exception in addContact",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		
		return irPlusResponseDetails;
	}


	@Override
	public IRPlusResponseDetails updateCustomerConstactsOnly(CustomeContactsArrayInfo customeContactsArrayInfo)
			throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}
}