package com.irplus.dao.customerreporting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.hibernate.entities.IrMUsers;
import com.irplus.dao.hibernate.entities.IrSBankBranch;
import com.irplus.dao.hibernate.entities.IrSBankreporting;
import com.irplus.dao.hibernate.entities.IrSCustomerreporting;
import com.irplus.dao.hibernate.entities.IrSCustomers;
import com.irplus.dto.BankReportingsInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.ReportsInfo;
import com.irplus.dto.customerreporting.CustomerRepotingBean;
import com.irplus.dto.customerreporting.CustomerRepotingResponseDetails;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;

@Transactional
@Component

public class CustomerReportingDaoImpl implements ICustomerReportingDao {
	
	private static final Logger log = Logger.getLogger(CustomerReportingDaoImpl.class); 
	
	@Autowired
	SessionFactory sessionFactory;
	
	public Session getMyCurrentSession() {		
		Session session =null;		
		try {	
		session=sessionFactory.getCurrentSession();
		}catch (HibernateException e) {
		log.error("Exception raised DaoImpl :: At current session creation time ::"+e);
		}		
		return session; 
	}

	public IRPlusResponseDetails dynamicCustomerReportsBybankId(BankReportingsInfo banksReportsInfo)throws BusinessException{
	
		log.info("Inside CustomeReportingDaoImpl");
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		Session session = sessionFactory.getCurrentSession();
		List<String> errMsgs = new ArrayList<String>();
		
		try {
			
			boolean flag = true;
			Long id = null;
			id = banksReportsInfo.getBankId();
			
			if (id==null) {
				
				errMsgs.add("Validation Error - Please Select Bank");
				errMsgs.add("Entered bank value null ");
				
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			
				flag = false;
			
			}
			if(flag){
				
				Criteria criteria = session.createCriteria(IrSBankreporting.class,"bankReport")			
				.createAlias("bankReport.irSBank", "bank")
				.add(Restrictions.eq("bank.bankId", id));
				
				List<IrSBankreporting> lst = criteria.list();
								
				if(!lst.isEmpty()){
					
					BankReportingsInfo bankreprts = new BankReportingsInfo();
					
					String[] reports = new String[lst.size()];
					bankreprts.setBankId(id);
					bankreprts.setIsactive(1);
					
					Integer moduleId[] = new Integer[lst.size()]; 
					
					int i= 0;
					for (IrSBankreporting irSBankreporting : lst) {
												
						if(irSBankreporting.getIsactive()==1){
					reports[i]=	irSBankreporting.getIrMModules().getModulename();
					moduleId[i]= irSBankreporting.getIrMModules().getModuleid();
						}
					++i;
					}	
					
					bankreprts.setReports(reports);
					bankreprts.setModuleId(moduleId);					
				
					irPlusResponseDetails.setBankReportingsInfo(bankreprts);
					
					errMsgs.add("Success : Got reports");
					irPlusResponseDetails.setValidationSuccess(true);
					irPlusResponseDetails.setErrMsgs(errMsgs);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
								
				}else{
					
					errMsgs.add("Message Alert : This Bank Not contains any reports");
					irPlusResponseDetails.setErrMsgs(errMsgs);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				}
			}
						
		} catch (Exception e) {
			log.error("Exception at get CustomerReports",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		
		return irPlusResponseDetails;
	}
	
		
	@Override
	public IRPlusResponseDetails createCustomerReport(CustomerRepotingBean customerReportingBean)throws BusinessException{
		
		log.info("Inside CustomeReportingDaoImpl");
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		Session session = sessionFactory.getCurrentSession();
		List<String> errMsgs = new ArrayList<String>();	
			
		try{
			List<ReportsInfo> reprtList =customerReportingBean.getReportsInfo();
			boolean flag = true;
			
		for (ReportsInfo reportsInfo : reprtList) {
						
			/*if (reportsInfo.getModuleId()!=null) {
				
			}*/
			if (reportsInfo.getReportGenTime()==null) {
				errMsgs.add("Validation Error - Please Select Time");
				errMsgs.add("Entered bank value null ");				
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			flag = false;	
			}
			if (reportsInfo.getBankReportId()==null ) {
				errMsgs.add("Validation Error - Please Select Bank");
				errMsgs.add("Entered bank value null ");				
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			flag = false;
			}
			if (reportsInfo.getReportName()==null) {
				errMsgs.add("Validation Error - Please Select ReportName");
				errMsgs.add("Entered bank value null ");				
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			flag = false;
			}
		}	
			
					
					Criteria criteria1 = session.createCriteria(IrSCustomerreporting.class,"custmrReport")			
					.createAlias("custmrReport.irSCustomers", "cusmr" )
					.add(Restrictions.eq("cusmr.customerId", customerReportingBean.getCustomerId()));				
				    List<IrSCustomerreporting> mst =criteria1.list();
				
			if(flag){
						 
			if (mst.isEmpty()) {				

				 	for (ReportsInfo reportsInfo : reprtList) {
				 		
		//				Integer intgr = currentReportIds[i];
		
						IrSCustomerreporting custmreprtng = new IrSCustomerreporting();
						
						//		irSCustomerreporting.setReportTime(customerReportingBean.getReportTime());
						 		custmreprtng.setIrMUsers((IrMUsers)getMyCurrentSession().get(IrMUsers.class, customerReportingBean.getUserid()));
						 		custmreprtng.setIrSCustomers((IrSCustomers)getMyCurrentSession().get(IrSCustomers.class, customerReportingBean.getCustomerId()));				
						 		custmreprtng.setIsactive(1);
						 		custmreprtng.setIrSBankbranch((IrSBankBranch)getMyCurrentSession().get(IrSBankBranch.class, customerReportingBean.getBankBranchId()));
						 		custmreprtng.setIrSBankreporting((IrSBankreporting)getMyCurrentSession().get(IrSBankreporting.class, reportsInfo.getBankReportId()));
						 		custmreprtng.setReportTime(reportsInfo.getReportGenTime());
								getMyCurrentSession().save(custmreprtng);
								
					}
				
				 	errMsgs.add("Succes :  Customerreports saved");
					irPlusResponseDetails.setErrMsgs(errMsgs);
					irPlusResponseDetails.setValidationSuccess(true);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				
			}else{
				
				/*Integer reportId[] = new Integer[reprtList.size()];
				Long branchId[] = new Long[reprtList.size()];
				String reportTime[] = new String[reprtList.size()];
				
				int i =0;
				for (ReportsInfo reportsInfo : reprtList) {					
					reportId[i] = reportsInfo.getBankReportId();			
				    reportTime[i] = reportsInfo.getReportGenTime();
				    ++i;
				}
				
				Integer[] db_reportids =new Integer[mst.size()];
				int j =0 ;
				
				Iterator<IrSCustomerreporting> it =  mst.iterator();
				
				for (Iterator iterator = mst.iterator(); iterator.hasNext();) {
					
					IrSCustomerreporting irSCustomerreporting = (IrSCustomerreporting) iterator.next();
					
				}
				
				for (IrSCustomerreporting irSCustg : mst) {
					
					db_reportids[j] =  irSCustg.getIrSBankreporting().getReportId();
							++j;
				}*/
								
			boolean add_flag = true;
			
			for (ReportsInfo reportsInfo : reprtList) {	//237 ui
				
				add_flag =true;
				for (IrSCustomerreporting irSCustg : mst) { //12345 DB
					
					if(irSCustg.getIrSBankreporting().getReportId()==reportsInfo.getBankReportId()){
						
						irSCustg.setReportTime(reportsInfo.getReportGenTime());
						irSCustg.setIsactive(1);	
						
						getMyCurrentSession().merge(irSCustg);
						errMsgs.add("Succes :  Customerreport , Time Updated :"+reportsInfo.getReportName() +" "+reportsInfo.getReportGenTime());
						irPlusResponseDetails.setErrMsgs(errMsgs);
						getMyCurrentSession().flush();	
						add_flag =false;
						}
					}
				
				if(add_flag){  // new entry 
					
					IrSCustomerreporting custmreprtng = new IrSCustomerreporting();
					
								
					custmreprtng.setIrMUsers((IrMUsers)getMyCurrentSession().get(IrMUsers.class, customerReportingBean.getUserid()));
			 		custmreprtng.setIrSCustomers((IrSCustomers)getMyCurrentSession().get(IrSCustomers.class, customerReportingBean.getCustomerId()));				
			 		custmreprtng.setIsactive(1);
			 		custmreprtng.setIrSBankbranch((IrSBankBranch)getMyCurrentSession().get(IrSBankBranch.class, customerReportingBean.getBankBranchId()));
			 		custmreprtng.setIrSBankreporting((IrSBankreporting)getMyCurrentSession().get(IrSBankreporting.class, reportsInfo.getBankReportId()));
			 		custmreprtng.setReportTime(reportsInfo.getReportGenTime());
			 		
					getMyCurrentSession().save(custmreprtng);
					getMyCurrentSession().flush();									
					errMsgs.add("Succes : new Entry Customerreport , Time Saved data :"+reportsInfo.getReportName() +" "+reportsInfo.getReportGenTime());
					add_flag =true;
				}			
		}					
			
			/*for (ReportsInfo reportsInfo : reprtList) {	//12345 DB
			
				if(Collections.frequency(Arrays.asList(db_reportids), reportsInfo.getBankReportId())==0){
					
					IrSCustomerreporting custmreprtng = new IrSCustomerreporting();
					
					
					custmreprtng.setIrMUsers((IrMUsers)getMyCurrentSession().get(IrMUsers.class, customerReportingBean.getUserid()));
			 		custmreprtng.setIrSCustomers((IrSCustomers)getMyCurrentSession().get(IrSCustomers.class, customerReportingBean.getCustomerId()));				
			 		custmreprtng.setIsactive(1);
			 		custmreprtng.setIrSBankbranch((IrSBankBranch)getMyCurrentSession().get(IrSBankBranch.class, customerReportingBean.getBankBranchId()));
			 		custmreprtng.setIrSBankreporting((IrSBankreporting)getMyCurrentSession().get(IrSBankreporting.class, reportsInfo.getBankReportId()));
			 		custmreprtng.setReportTime(reportsInfo.getReportGenTime());
			 		
					getMyCurrentSession().save(custmreprtng);
					getMyCurrentSession().flush();									
					errMsgs.add("Succes : Customerreport Name , Time Saved data :"+reportsInfo.getReportName() +" "+reportsInfo.getReportGenTime());
					irPlusResponseDetails.setErrMsgs(errMsgs);
				}
			}*/
				
				
			errMsgs.add("Succes : Entered data");
			irPlusResponseDetails.setErrMsgs(errMsgs);
			irPlusResponseDetails.setValidationSuccess(true);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
					
			}
			}
		
		}
		catch (Exception e) {
			log.error("Exception in create CustomerContacts",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
	
		/*
    	        String text = "2011-10-02 18:40:11";
    	        Timestamp ts = Timestamp.valueOf(text);
    	        System.out.println(ts.getNanos());
    	
    	        SimpleDateFormat datetimeFormatter1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		    	Date lFromDate1 = datetimeFormatter1.parse(text);
		    	System.out.println("gpsdate :" + lFromDate1);
		    	Timestamp fromTS1 = new Timestamp(lFromDate1.getTime());
    	        
    	        
    	        String input = "2007-11-11 12:13:14" ;
    	        java.sql.Timestamp tss = java.sql.Timestamp.valueOf( input ) ;
    	        System.out.println("Output: " + tss.toInstant().toString());
    	        
    	        String text2 = "2011-10-02 18:48:05.123";
    	        Timestamp tsd = Timestamp.valueOf(text2);
    	        System.out.println(tsd);
    	       
    	     	gpsdate :Sun Oct 02 18:40:11 IST 2011
				Output: 2007-11-11T06:43:14Z
				2011-10-02 18:48:05.123
    	        *
    	        */
		return irPlusResponseDetails;
	}

	public CustomerRepotingResponseDetails getSelectedBankId(String custommerReportingId) throws BusinessException {
		
		log.info("Inside CustomerReportingDaoImpl");

		CustomerRepotingResponseDetails customerRepotingResponseDetails = new CustomerRepotingResponseDetails();
		
		return null;
	}
	
	@Override
	public IRPlusResponseDetails getCustomerReportById(String custommerReportingId) throws BusinessException {

		log.info("Inside CustomerReportingDaoImpl");
		
		IRPlusResponseDetails responseDetails = new IRPlusResponseDetails();
		
		List<String> errMsgs = new ArrayList<String>(); 
		
		List<CustomerRepotingBean> listcustomerRepotingBean = new ArrayList<CustomerRepotingBean>();
		
		try {
			Integer id = Integer.parseInt(custommerReportingId); 
			
			Criteria criteria = getMyCurrentSession().createCriteria(IrSCustomerreporting.class , "custmrprt")
					.createAlias("custmrprt.irSCustomers", "custmrs")
					.add(Restrictions.eq("custmrs.customerId", id));
			
			List<IrSCustomerreporting > lstcustmr = criteria.list();
			
			if (!lstcustmr.isEmpty()) {			 
							
				for (IrSCustomerreporting irSCustomerreporting : lstcustmr) {
					
				if(irSCustomerreporting.getIsactive()==1){
				
				CustomerRepotingBean customerRepotingBean = new CustomerRepotingBean();
			
				customerRepotingBean.setCustomerId(irSCustomerreporting.getIrSCustomers().getCustomerId());
				customerRepotingBean.setCustomerReportId(irSCustomerreporting.getCustomerReportId());
				customerRepotingBean.setIsactive(irSCustomerreporting.getIsactive());				
				customerRepotingBean.setReportingTime(irSCustomerreporting.getReportTime());
				customerRepotingBean.setUserid(irSCustomerreporting.getIrMUsers().getUserid());
				customerRepotingBean.setReportName(irSCustomerreporting.getIrSBankreporting().getIrMModules().getModulename());
				customerRepotingBean.setReportId(irSCustomerreporting.getIrSBankreporting().getReportId());
				
				listcustomerRepotingBean.add(customerRepotingBean);
				}
				}
				responseDetails.setListcustomerRepotingBean(listcustomerRepotingBean);

				errMsgs.add("Success : Customer Reports ");
				responseDetails.setErrMsgs(errMsgs);
				responseDetails.setValidationSuccess(true);
				responseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				responseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				
			} else {
				errMsgs.add("Validation Error : Given customer not contains reports");
				responseDetails.setErrMsgs(errMsgs);
				responseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				responseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);				
			}
			
		} catch (Exception e) {

			responseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			responseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception Raised inside customerRepotingDaoImpl :: "+e);
			throw new HibernateException(e);
		}
				
		return responseDetails;
	}

	
	@Override
	public CustomerRepotingResponseDetails updateCustomerReports(CustomerRepotingBean customerReportingBean)throws BusinessException {
		
		log.info("Inside customerRepotingDaoImpl");
		log.debug("Update customerRepoting instance");
					
		CustomerRepotingResponseDetails customerRepotingResponseDetails = new CustomerRepotingResponseDetails();
		
		IrSCustomerreporting irSCustomerreporting = null;
		
		try{	
			irSCustomerreporting = (IrSCustomerreporting) getMyCurrentSession().get(IrSCustomerreporting.class, customerReportingBean.getCustomerReportId());	

		//	irSCustomerreporting.setReportTime(customerReportingBean.getReportTime());
			irSCustomerreporting.setIrMUsers((IrMUsers)getMyCurrentSession().get(IrMUsers.class, customerReportingBean.getUserid()));
			irSCustomerreporting.setIsactive(customerReportingBean.getIsactive());

		//	irSCustomerreporting.setCreateddate(customerReportingBean.getCreateddate());
			irSCustomerreporting.setCustomerReportId(customerReportingBean.getCustomerReportId());
		//	irSCustomerreporting.setModifieddate(customerReportingBean.getModifieddate());
			irSCustomerreporting.setIrSCustomers((IrSCustomers)getMyCurrentSession().get(IrSCustomers.class,customerReportingBean.getCustomerId()));
			
		
			getMyCurrentSession().update(irSCustomerreporting);
		
			customerRepotingResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			customerRepotingResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			customerRepotingResponseDetails.setValidationSuccess(true);
		
		}catch (Exception e) {

			customerRepotingResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			customerRepotingResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception Raised inside customerRepotingDaoImpl :: customerRepotingId : "+e);
		
			throw new HibernateException(e);
		}
	
		return customerRepotingResponseDetails;
	}
	
	@Override
	public CustomerRepotingResponseDetails deleteCustomerReportById(String custommerReportingId) throws BusinessException {

		log.info("Inside customerReportingDaoImpl");
		log.debug("deleting CustomerReporting instance");
		
		CustomerRepotingResponseDetails customerRepotingResponseDetails = new CustomerRepotingResponseDetails();
		
		try{	
			Query query = getMyCurrentSession().createQuery("delete IrSCustomerreporting d where d.customerReportId =:cc_Id");
			query.setParameter("cc_Id", Integer.parseInt(custommerReportingId));
			
			int result=query.executeUpdate();
			
			if(result==0){
				
				log.debug("delete customerrepoting failed to delete");
				customerRepotingResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				customerRepotingResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				
			}else {
				log.debug("succcessfully deleted customer ::customerrepoting customerId"+custommerReportingId);
				customerRepotingResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				customerRepotingResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
				
		}catch (Exception e) {
			log.error("Exception raised in delete customerrepoting",e);
			customerRepotingResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			customerRepotingResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			throw new HibernateException(e);
		}
		return customerRepotingResponseDetails;
	}

	@Override
	public CustomerRepotingResponseDetails showAllCustomerReports() throws BusinessException {

		log.info("Inside customerReportingDaoImpl");
		log.debug("Show all customersReporting instances");
		
		CustomerRepotingResponseDetails customerRepotingResponseDetails = new CustomerRepotingResponseDetails();
		
		List<CustomerRepotingBean> listCustomReporting = new ArrayList<CustomerRepotingBean>();
		
		try {
	
			Criteria criteria = getMyCurrentSession().createCriteria(IrSCustomerreporting.class);
			List<IrSCustomerreporting> list = criteria.list();
			
			if (!list.isEmpty()) {
							
				for (IrSCustomerreporting irSCustomerreporting : list) {
			
					CustomerRepotingBean customerRepotingBean = new CustomerRepotingBean();
										
					customerRepotingBean.setIsactive(irSCustomerreporting.getIsactive());
		//			customerRepotingBean.setReportTime(irSCustomerreporting.getReportTime());
					customerRepotingBean.setCustomerId(irSCustomerreporting.getIrSCustomers().getCustomerId());
					customerRepotingBean.setUserid(irSCustomerreporting.getIrMUsers().getUserid());
			//		customerRepotingBean.setCreateddate(irSCustomerreporting.getCreateddate());
			//		customerRepotingBean.setModifieddate(irSCustomerreporting.getModifieddate());
					customerRepotingBean.setCustomerId(irSCustomerreporting.getIrSCustomers().getCustomerId());
					
					
					listCustomReporting.add(customerRepotingBean);	
				}

				customerRepotingResponseDetails.setCustomerRepotingBeans(listCustomReporting);
			
				customerRepotingResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				customerRepotingResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				
			} else {
				log.debug("CustomerContacts not available :: Empty Data :: Data required to showAll CustomerContacts");
				customerRepotingResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				customerRepotingResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}
			
		} catch (Exception e) {
			log.error("Exception raised in show list CustomerContacts",e);
			customerRepotingResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			customerRepotingResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			throw new HibernateException(e);
		}
		
		return customerRepotingResponseDetails;
	}

}