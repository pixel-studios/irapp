package com.irplus.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.irplus.dto.rolepermission.RolepermissionsBean;

public class RolePermissionValidationHelper {

	private static final Log log = LogFactory.getLog(RolePermissionValidationHelper.class);
	
	public static boolean isValidRolepermissionReq(RolepermissionsBean rolepermissionsBean) {
		
		log.info("inside of StateValidationHelper class");

		boolean isValidReq = false;
		
		if (rolepermissionsBean.getRolesId()!= null) {
			isValidReq = true;
			log.debug("isValidRolepermissionReq is true :: validation success");
		}else {
		log.debug("isValidRoelpermissionReq is false :: validation failure");
		}
		return isValidReq;
	}

	public static boolean isValidRolepermissionId(String rolepermissionId) {
		
		log.info("inside of rolepermission ValidationHelper class");
		
		boolean isValidReq = false;
		
		if(rolepermissionId!=null && !rolepermissionId.isEmpty()&&rolepermissionId.matches("[0-9]+")) {
			isValidReq=true;
			log.debug("isValidRolepermissioncodeReq is true :: validation success");
		}else {
			log.debug("isValidRolepermissionReq is false :: validation failure");
			}
		
		return isValidReq;
	}
	
}