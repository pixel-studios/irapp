package com.irplus.util;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.irplus.dto.IRPlusResponseDetails;

public class IRPlusRestUtil {
    public static ResponseEntity<String> getGenericErrorResponse() {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("errMsg", "INTERNAL SERVER ERROR");
        responseHeaders.set("errCode", "500");
        return new ResponseEntity<>(responseHeaders, HttpStatus.CONFLICT);
    }

    public static ResponseEntity<String> getValidationErrorResponse() {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("errMsg", "VALIDATION ERROR");
        responseHeaders.set("errCode", "201");
        return new ResponseEntity<>(responseHeaders, HttpStatus.CONFLICT);
    }

    public static ResponseEntity<String> getCustomErrorResponse(String errCode, String errMsg, String errorData) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("errCode", errCode);
        responseHeaders.set("errMsg", errMsg);
        if (errorData != null) {
            return new ResponseEntity<>(responseHeaders, HttpStatus.CONFLICT);
        } else {
            return new ResponseEntity<>(errorData, responseHeaders, HttpStatus.CONFLICT);
        }
    }

    public static ResponseEntity<String> getCustomErrorResponse(String errCode, String errMsg) {
        return getCustomErrorResponse(errCode, errMsg, null);
    }

    public static ResponseEntity<String> getCustomErrorResponse(int errCode, String errMsg) {
        return getCustomErrorResponse(Integer.toString(errCode), errMsg, null);
    }

    public static ResponseEntity<String> getCustomErrorResponse(int errCode, String errMsg, String errorData) {
        return getCustomErrorResponse(Integer.toString(errCode), errMsg, errorData);
    }

	

}
