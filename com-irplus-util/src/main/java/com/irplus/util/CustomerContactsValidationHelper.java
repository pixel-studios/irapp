package com.irplus.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.irplus.dto.CustomerContactsBean;

public class CustomerContactsValidationHelper {

	private static final Log log = LogFactory.getLog(CustomerContactsValidationHelper.class);
	
	public static boolean isValidCustomerContactReq(CustomerContactsBean customerBean) {
		
		log.info("inside of CustomerValidationHelper class");

		boolean isValidReq = false;
		
		if (customerBean.getFirstName()!= null) {
			isValidReq = true;
			log.debug("isValidCustomerReq is true :: validation success");
		}else {
		log.debug("isValidCustomerReq is false :: validation failure");
		}

		return isValidReq;
	}

	public static boolean isValidCustomerContactId(String customerContactId) {
		
		log.info("inside of UserValidationHelper class");
		
		boolean isValidReq = false;
		
		if(customerContactId!=null && !customerContactId.isEmpty()&& customerContactId.matches("[0-9]+")) {
			isValidReq=true;
			log.debug("isValidCustomerReq is true :: validation success");
		}else {
			log.debug("isValidCustomerReq is false :: validation failure");
			}
		
		return isValidReq;
	}
	
}