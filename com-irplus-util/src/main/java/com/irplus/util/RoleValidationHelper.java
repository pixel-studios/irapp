package com.irplus.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.irplus.dto.menu.AddMenu;
import com.irplus.dto.roles.RoleBean;

public class RoleValidationHelper {

	private static final Log log = LogFactory.getLog(RoleValidationHelper.class);
	
	public static boolean isValidRoleReq(RoleBean roleBean) {
		
		log.info("inside of RoleValidationHelper class");

		boolean isValidReq = false;
		
		if (roleBean.getRolename() != null ) {
			isValidReq = true;
			log.debug("isValidRoleReq is true :: validation success");
		}
		return isValidReq;
	}

	public static boolean isValidRoleId(String roleId) {
		
		log.info("inside of MenuValidationHelper class");
		
		boolean isValidReq = false;
		
		if(roleId!=null && !roleId.isEmpty()&&roleId.matches("[0-9]+")) {
			isValidReq=true;
			log.debug("isValidRoleReq is true :: validation success");
		}		
		return isValidReq;
	}	
}
