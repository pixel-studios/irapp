package com.irplus.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.irplus.dto.module.ModuleBean;

public class ModuleValidationHelper {

	private static final Log log = LogFactory.getLog(ModuleValidationHelper.class);
	
	public static boolean isValidModuleReq(ModuleBean moduleInfo) {
		
		log.info("inside of MenuValidationHelper class");

		boolean isValidReq = false;
		
		if (moduleInfo.getModulename()!= null && moduleInfo.getDescription() != null) {
			isValidReq = true;
			log.debug("isValidModuleReq is true :: validation success");
		}
		return isValidReq;
	}

	public static boolean isValidModuleId(String moduleId) {
		
		log.info("inside of ModuleValidationHelper class");
		
		boolean isValidReq = false;
		
		if(moduleId!=null && !moduleId.isEmpty()&&moduleId.matches("[0-9]+")) {
			isValidReq=true;
			log.debug("isValidModuleReq is true :: validation success");
		}
		
		return isValidReq;
	}
	
}
