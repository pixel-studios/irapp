package com.irplus.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.irplus.dto.customerreporting.CustomerRepotingBean;

public class CustomerReportingValidationHelper {
    
	private static final Log log = LogFactory.getLog(CustomerReportingValidationHelper.class);
	
	public static boolean isValidCustomerReportingReq(CustomerRepotingBean customerRepotingBean){
		
		log.info("inside of CustomerValidationHelper class");
        
		boolean isValidReq = false;
		
		if (customerRepotingBean.getCustomerId()!= null){
			isValidReq = true;
			log.debug("isValidcustomerRepotingReq is true :: validation success");
		}else {
		log.debug("isValidcustomerRepotingBeanReq is false :: validation failure : customerid null");
		}
		return isValidReq;
	}
    
	public static boolean isValidCustomerReportingId(String customerId) {
		
		log.info("inside of UserValidationHelper class");
		
		boolean isValidReq = false;
		
		if(customerId!=null && !customerId.isEmpty()&& customerId.matches("[0-9]+")) {
			isValidReq=true;
			log.debug("isValidcustomerRepotingReq is true :: validation success");
		}else {
			log.debug("isValidcustomerRepotingReq is false :: validation failure");
			}
		
		return isValidReq;
	}
	
}