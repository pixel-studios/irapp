/**
 * 
 */
package com.irplus.dao.client.dbinfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.dbinfo.IDBPropertiesDao;
import com.irplus.dto.DBDTO;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

/**
 * @author arjun
 *
 */

@Component
public class DBPropertiesDaoClientImpl implements IDBPropertiesDaoClient{

@Autowired
IDBPropertiesDao iDBPropertiesDao;
	
	@Override
	public IRPlusResponseDetails createDBProperties(DBDTO dbdto) throws BusinessException {

		return iDBPropertiesDao.createDBProperties(dbdto);
	}

	@Override
	public IRPlusResponseDetails updateDBProperties(DBDTO dbdto) throws BusinessException {

		return iDBPropertiesDao.updateDBProperties(dbdto);
	}

	@Override
	public IRPlusResponseDetails showDBProperties() throws BusinessException {

		return iDBPropertiesDao.showDBProperties();
	}

	@Override
	public IRPlusResponseDetails deleteDBProperties(DBDTO dbdto) throws BusinessException {

		return iDBPropertiesDao.deleteDBProperties(dbdto);
	}

}
