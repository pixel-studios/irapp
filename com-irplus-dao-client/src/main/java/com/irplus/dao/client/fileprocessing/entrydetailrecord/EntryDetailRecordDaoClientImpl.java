package com.irplus.dao.client.fileprocessing.entrydetailrecord;

import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.client.fileprocessing.batchheaderrecord.BatchHeaderRecordDaoClientImpl;
import com.irplus.dao.fileprocessing.entrydetailrecord.EntryDetailRecordDao;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.fileprocessing.WEBEntryDetailRecordBean;
import com.irplus.util.BusinessException;

@Component
public class EntryDetailRecordDaoClientImpl implements EntryDetailRecordDaoClient{

	
	private static final Logger LOGGER = Logger.getLogger(EntryDetailRecordDaoClientImpl.class);

	@Autowired
	private EntryDetailRecordDao entryDetailRecordDao;
	
	@Override
	public Boolean insertRecord(WEBEntryDetailRecordBean entryDetailRecordBean) {
		
		return entryDetailRecordDao.insertRecord(entryDetailRecordBean);
	}

	@Override
	public Boolean deleteRecord(WEBEntryDetailRecordBean entryDetailRecordBean) {
		
		return entryDetailRecordDao.deleteRecord(entryDetailRecordBean);
	}

	@Override
	public List<WEBEntryDetailRecordBean> listAllRecords() {
		
		return entryDetailRecordDao.listAllRecords();
	}

	@Override
	public IRPlusResponseDetails getRecordByEntryId(Integer fileId) throws BusinessException {
		
		return entryDetailRecordDao.getRecordByEntryId(fileId);
	}

	@Override
	public Boolean updateRecord(WEBEntryDetailRecordBean batchHeaderRecord) {
		
		return entryDetailRecordDao.updateRecord(batchHeaderRecord);
	}

	@Override
	public List<WEBEntryDetailRecordBean> getByQuery(Integer Id) {
		
		return entryDetailRecordDao.getByQuery(Id);
	}

}
