package com.irplus.dao.client.contactmgmt;

import com.irplus.dto.ContactInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

public interface IContactMgmtDaoClient {

	 public IRPlusResponseDetails addContact(ContactInfo contactInfo)
	            throws BusinessException;

	public IRPlusResponseDetails listContacts(String bankId) throws BusinessException;

	public IRPlusResponseDetails updateContact(ContactInfo contactInfo) throws BusinessException;
	
	public IRPlusResponseDetails updateContacts(ContactInfo[] contacts) throws BusinessException;

	public IRPlusResponseDetails deleteContact(String contactId) throws BusinessException;

}


/*
public IRPlusResponseDetails createBank(BankInfo bankInfo) throws BusinessException;

public IRPlusResponseDetails listBanks(String siteId) throws BusinessException;

public IRPlusResponseDetails updateBank(BankInfo bankInfo) throws BusinessException;

public IRPlusResponseDetails deleteBank(String bankId) throws BusinessException;
*/

