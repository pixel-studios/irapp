package com.irplus.dao.client.customer_grouping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.customer_grouping.ICustomerGroupingMngmntDao;
import com.irplus.dto.CustomerGroupingDetails;
import com.irplus.dto.CustomerGroupingMngmntInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.util.BusinessException;

@Component
public class CustomerGroupingMngmntDaoClientImpl implements ICustomerGroupingMngmntDaoClient{
	
	@Autowired
	ICustomerGroupingMngmntDao customerGroupingMngmntDao;

	@Override
	public IRPlusResponseDetails createCustomerGrp(CustomerGroupingMngmntInfo customerGroupingMngmntInfo)
			throws BusinessException {

		return customerGroupingMngmntDao.createCustomerGrp(customerGroupingMngmntInfo);
	}
	@Override
	public IRPlusResponseDetails volumeTrendrangewise(CustomerGroupingMngmntInfo trendRange) throws BusinessException {

		return customerGroupingMngmntDao.volumeTrendrangewise(trendRange);
	}
	@Override
	public IRPlusResponseDetails volumeTrendSummary(CustomerGroupingMngmntInfo trendDateRange) throws BusinessException {

		return customerGroupingMngmntDao.volumeTrendSummary(trendDateRange);
	}
	@Override
	public IRPlusResponseDetails showVolumeTrendReportWeekwise() throws BusinessException {

		return customerGroupingMngmntDao.showVolumeTrendReportWeekwise();
	}
	@Override
	public IRPlusResponseDetails showOneCustomerGrpById(String customerGroupingMngmntId) throws BusinessException {

		return customerGroupingMngmntDao.showOneCustomerGrpById(customerGroupingMngmntId);
	}
	@Override
	public IRPlusResponseDetails volumeTrendBankrangewise(CustomerGroupingMngmntInfo BankRange) throws BusinessException {

		return customerGroupingMngmntDao.volumeTrendBankrangewise(BankRange);
	}
	@Override
	public IRPlusResponseDetails showVolumeTrendReport(CustomerGroupingMngmntInfo currentDate) throws BusinessException {

		return customerGroupingMngmntDao.showVolumeTrendReport(currentDate);
	}
	
	@Override
	public IRPlusResponseDetails getdatacustomerDatewise(CustomerGroupingMngmntInfo customerdatewiseinfo) throws BusinessException {

		return customerGroupingMngmntDao.getdatacustomerDatewise(customerdatewiseinfo);
	}
	@Override
	public IRPlusResponseDetails filterCustomerdatewise(CustomerGroupingMngmntInfo customerdateinfo) throws BusinessException {

		return customerGroupingMngmntDao.filterCustomerdatewise(customerdateinfo);
	}
	@Override
	public IRPlusResponseDetails showcustomerWiseReport() throws BusinessException {

		return customerGroupingMngmntDao.showcustomerWiseReport();
	}
	@Override
	public IRPlusResponseDetails getcustomergroupDatewise(CustomerGroupingMngmntInfo customergrpdateinfo) throws BusinessException {

		return customerGroupingMngmntDao.getcustomergroupDatewise(customergrpdateinfo);
	}
	@Override
	public IRPlusResponseDetails showgroupingReport(CustomerGroupingMngmntInfo customerInfo) throws BusinessException {

		return customerGroupingMngmntDao.showgroupingReport(customerInfo);
	}
	@Override
	public IRPlusResponseDetails updateOneCustomerGrps(CustomerGroupingMngmntInfo customerGroupingMngmntInfo)
			throws BusinessException {

		return customerGroupingMngmntDao.updateOneCustomerGrps(customerGroupingMngmntInfo);
	}
	@Override
	public IRPlusResponseDetails getcustomerdateWise(CustomerGroupingMngmntInfo customerdateinfo) throws BusinessException {

		return customerGroupingMngmntDao.getcustomerdateWise(customerdateinfo);
	}
	@Override
	public IRPlusResponseDetails statusUpdatCustmrGrpById(StatusIdInfo statusIdInfo) throws BusinessException {

		return customerGroupingMngmntDao.statusUpdatCustmrGrpById(statusIdInfo);
	}

	@Override
	public IRPlusResponseDetails getCustomrGrpDtalsViewById(String customerGroupingMngmntId) throws BusinessException {

		return customerGroupingMngmntDao.getCustomrGrpDtalsViewById(customerGroupingMngmntId);
	}

	@Override
	public IRPlusResponseDetails showAllCustomerGrps() throws BusinessException {

		return customerGroupingMngmntDao.showAllCustomerGrps();
	}

	
	@Override
	public IRPlusResponseDetails custmrIdArrays(String barnchId) throws BusinessException {

		return customerGroupingMngmntDao.custmrIdArrays(barnchId);
	}

	@Override
	public IRPlusResponseDetails bankIdArrays() throws BusinessException {

		return customerGroupingMngmntDao.bankIdArrays();
	}

	@Override
	public IRPlusResponseDetails createCustomerMappingGroup(CustomerGroupingMngmntInfo customergroupingMngmntInfo)
			throws BusinessException {

		return customerGroupingMngmntDao.createCustomerMappingGroup(customergroupingMngmntInfo);
	}
	@Override
	public IRPlusResponseDetails filterGrpdatewise(CustomerGroupingMngmntInfo grpdateinfo) throws BusinessException {

		return customerGroupingMngmntDao.filterGrpdatewise(grpdateinfo);
	}
	@Override
	public IRPlusResponseDetails addMappingDetails(CustomerGroupingDetails[] customergroupingdetails)
			throws BusinessException {

		return customerGroupingMngmntDao.addMappingDetails(customergroupingdetails);
	}
	
	
	
	
	@Override
	public IRPlusResponseDetails showAllCustomerGroup() throws BusinessException {

		return customerGroupingMngmntDao.showAllCustomerGroup();
	}

	@Override
	public IRPlusResponseDetails deleteGroup(CustomerGroupingMngmntInfo groupInfo)
			throws BusinessException {

		return customerGroupingMngmntDao.deleteGroup(groupInfo);
	}
	
	@Override
	public IRPlusResponseDetails deleteGroupDetails(CustomerGroupingDetails[] customergroupingdetails)
			throws BusinessException {

		return customerGroupingMngmntDao.deleteGroupDetails(customergroupingdetails);
	}
	
	@Override
	public IRPlusResponseDetails getcustomergroupInfo(Integer customerGrpId) throws BusinessException {

		return customerGroupingMngmntDao.getcustomergroupInfo(customerGrpId);
	}
	@Override
	public IRPlusResponseDetails getbankDaywiseReport(String bankid) throws BusinessException {

		return customerGroupingMngmntDao.getbankDaywiseReport(bankid);
	}
	@Override
	public IRPlusResponseDetails getbankwiseReport(String bankid) throws BusinessException {

		return customerGroupingMngmntDao.getbankwiseReport(bankid);
	}
/*	@Override
	public IRPlusResponseDetails EditCustGrp(CustomerGroupingDetails editgroupInfo)
			throws BusinessException {

		return customerGroupingMngmntDao.EditCustGrp(editgroupInfo);
	}*/
	@Override
	public IRPlusResponseDetails updateCustomerMappingGroup(CustomerGroupingMngmntInfo customergroupingMngmntInfo)
			throws BusinessException {

		return customerGroupingMngmntDao.updateCustomerMappingGroup(customergroupingMngmntInfo);
	}
	
	@Override
	public IRPlusResponseDetails updatecustMappingDetails(CustomerGroupingDetails[] customergroupingdetails)
			throws BusinessException {

		return customerGroupingMngmntDao.updatecustMappingDetails(customergroupingdetails);
	}
	
	@Override
	public IRPlusResponseDetails CustomerGrpFilter(CustomerGroupingMngmntInfo GrpListInfo)
			throws BusinessException {

		return customerGroupingMngmntDao.CustomerGrpFilter(GrpListInfo);
	}
	@Override
	public IRPlusResponseDetails showDaywiseReport(CustomerGroupingMngmntInfo currentDate) throws BusinessException {

		return customerGroupingMngmntDao.showDaywiseReport(currentDate);
	}
	@Override
	public IRPlusResponseDetails showDaywiseCustomerReport(CustomerGroupingMngmntInfo customerInfo) throws BusinessException {

		return customerGroupingMngmntDao.showDaywiseCustomerReport(customerInfo);
	}
	@Override
	public IRPlusResponseDetails showDaywiseCustomerGroupReport(CustomerGroupingMngmntInfo customerInfo) throws BusinessException {

		return customerGroupingMngmntDao.showDaywiseCustomerGroupReport(customerInfo);
	}
	@Override
	public IRPlusResponseDetails DaycustomerReport(CustomerGroupingMngmntInfo customerInfo) throws BusinessException {

		return customerGroupingMngmntDao.DaycustomerReport(customerInfo);
	}
	
	@Override
	public IRPlusResponseDetails DaycustomerGroupReport(CustomerGroupingMngmntInfo currentDate) throws BusinessException {

		return customerGroupingMngmntDao.DaycustomerGroupReport(currentDate);
	}
	
	@Override
	public IRPlusResponseDetails getGroupCustomerwise(CustomerGroupingMngmntInfo currentDate) throws BusinessException {

		return customerGroupingMngmntDao.getGroupCustomerwise(currentDate);
	}
	
	@Override
	public IRPlusResponseDetails filterGroups(CustomerGroupingMngmntInfo grpFilterInfo) throws BusinessException {

		return customerGroupingMngmntDao.filterGroups(grpFilterInfo);
	}
	
	@Override
	public IRPlusResponseDetails AutocompleteGroup(String term,String userid) throws BusinessException {

		return customerGroupingMngmntDao.AutocompleteGroup(term,userid);
	}
}
