package com.irplus.dao.client.fileprocessing.batchheaderrecord;

import java.util.List;

import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.fileprocessing.BatchHeaderRecordBean;
import com.irplus.util.BusinessException;

public interface BatchHeaderRecordDaoClient {
	public Boolean insertRecord(BatchHeaderRecordBean batchHeaderRecord);
	public Boolean deleteRecord(BatchHeaderRecordBean batchHeaderRecord);
    public List<BatchHeaderRecordBean> listAllRecords();
    public IRPlusResponseDetails getRecordByBatchId(Integer fileId) throws BusinessException;
    public Boolean updateRecord(BatchHeaderRecordBean batchHeaderRecord);
    public List<BatchHeaderRecordBean> getByQuery(Integer Id);

}
