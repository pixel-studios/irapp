package com.irplus.dao.client.customerFunctionsMgnt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.customerFunctionsMgnt.ICustomerFunctionsMngmntDao;
import com.irplus.dto.CustomerFunctionInfo;
import com.irplus.dto.CustomerReportLicenceInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

@Component
public class CustomerFunctionsMngmntDaoClientImpl implements ICustomerFunctionsMngmntDaoClient {

	
	@Autowired 
	ICustomerFunctionsMngmntDao iCustomerFunctionsMngmntDao;
	
	
	@Override
	public IRPlusResponseDetails createCustomerFunctions(CustomerFunctionInfo customerFunctionInfo) throws BusinessException {
		
		return iCustomerFunctionsMngmntDao.createCustomerFunctions(customerFunctionInfo);
	}

	@Override
	public IRPlusResponseDetails getCustomerFunctionsById(String customerFunctionInfoId) throws BusinessException {

		return iCustomerFunctionsMngmntDao.getCustomerFunctionsById(customerFunctionInfoId);
	}

	@Override
	public IRPlusResponseDetails updateCustomerFunctions(CustomerFunctionInfo customerFunctionInfo)throws BusinessException {

		return iCustomerFunctionsMngmntDao.updateCustomerFunctions(customerFunctionInfo);
	}

	@Override
	public IRPlusResponseDetails statusUpdateCustmrFunctnsById(String customerFunctionInfoId) throws BusinessException {

		return null;
	}

	@Override
	public IRPlusResponseDetails showAllCustmrFunctions() throws BusinessException {

		return null;
	}

}