package com.irplus.dao.client.customer_report_licence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.customer_report_licencing.ICustomerReportLicenceDao;
import com.irplus.dto.CustomerReportLicenceInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

@Component
public class CustomerReportLicenceDaoClientImpl implements ICustomerReportLicenceDaoClient{

	@Autowired
	ICustomerReportLicenceDao iCustomerReportLicenceDao; 
	
	@Override
	public IRPlusResponseDetails createCustomerReportLicence(CustomerReportLicenceInfo customerReportLicenceInfo)
			throws BusinessException {

		return iCustomerReportLicenceDao.createCustomerReportLicence(customerReportLicenceInfo);
	}

	@Override
	public IRPlusResponseDetails getCustomerReportLicenceById(String customerReportLicenceInfoId)
			throws BusinessException {

		return iCustomerReportLicenceDao.getCustomerReportLicenceById(customerReportLicenceInfoId);
	}

	@Override
	public IRPlusResponseDetails updateCustomerReportLicence(CustomerReportLicenceInfo customerReportLicenceInfo)
			throws BusinessException {

		return iCustomerReportLicenceDao.updateCustomerReportLicence(customerReportLicenceInfo);
	}

	@Override
	public IRPlusResponseDetails deleteCustomerReportLicenceById(String ReportLicenceInfoId) throws BusinessException {

		return iCustomerReportLicenceDao.deleteCustomerReportLicenceById(ReportLicenceInfoId);
	}

	@Override
	public IRPlusResponseDetails showAllCustomerReportLicences() throws BusinessException {

		return iCustomerReportLicenceDao.showAllCustomerReportLicences();
	}

}
