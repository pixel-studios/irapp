package com.irplus.dao.client.menumgmt;

import com.irplus.dto.menu.AddMenu;
import com.irplus.dto.menu.MenuResponseDetails;
import com.irplus.util.BusinessException;

public interface MenuMgmtDaoClient {

	public MenuResponseDetails createMenu(AddMenu menuInfo) throws BusinessException;

	public MenuResponseDetails getMenu(String menuId) throws BusinessException;

	public MenuResponseDetails updateMenu(AddMenu menuInfo) throws BusinessException;

	public MenuResponseDetails deleteMenu(String menuId) throws BusinessException;
	
	public MenuResponseDetails findAllMenus()throws BusinessException;

}




