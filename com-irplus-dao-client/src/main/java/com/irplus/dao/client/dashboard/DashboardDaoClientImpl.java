package com.irplus.dao.client.dashboard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.dashboard.DashboardDao;
import com.irplus.dto.CustomerGroupingMngmntInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

@Component
public class DashboardDaoClientImpl implements DashboardDaoClient{

	@Autowired
	DashboardDao dashboardDao;
	
	@Override
	public IRPlusResponseDetails getDashboard(String siteId) throws BusinessException {
		return dashboardDao.getDashboard(siteId);
	}
	@Override
	public IRPlusResponseDetails listAllFiles(Integer userId) throws BusinessException {
		return dashboardDao.listAllFiles(userId);
	}
	@Override
	public IRPlusResponseDetails BankwiseFile(CustomerGroupingMngmntInfo user,Long branchId) throws BusinessException {
		return dashboardDao.BankwiseFile(user,branchId);
	}
}
