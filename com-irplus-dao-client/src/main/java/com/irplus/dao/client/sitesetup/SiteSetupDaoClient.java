package com.irplus.dao.client.sitesetup;

import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.SiteInfo;
import com.irplus.util.BusinessException;

public interface SiteSetupDaoClient {
	public IRPlusResponseDetails getSiteInfo(String siteId) throws BusinessException;
	public IRPlusResponseDetails updateSite(SiteInfo siteInfo) throws BusinessException;
	public IRPlusResponseDetails updateFolder(SiteInfo siteInfo) throws BusinessException;
	public IRPlusResponseDetails licenseValidity() throws BusinessException;
	public IRPlusResponseDetails siteInfo() throws BusinessException;
}
