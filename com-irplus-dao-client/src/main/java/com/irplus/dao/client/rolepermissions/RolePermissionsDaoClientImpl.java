package com.irplus.dao.client.rolepermissions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.rolepermissions.IRolePermissionDao;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.RolePrmUpdateNCreateInfo;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.rolepermission.RolepermissionsBean;
import com.irplus.util.BusinessException;

@Component
public class RolePermissionsDaoClientImpl  implements IRolePermissionDaoClient{

	@Autowired
	IRolePermissionDao iRolePermissionDao;
	
	
	@Override
	public IRPlusResponseDetails createRolePermission(RolepermissionsBean rolepermissionsBean)
			throws BusinessException {
	
		return iRolePermissionDao.createRolePermission(rolepermissionsBean);
	}

	@Override
	public IRPlusResponseDetails getRolePermissionById(String RolepermissionsBeanId) throws BusinessException {
	
		return iRolePermissionDao.getRolePermissionById(RolepermissionsBeanId);
	}

	@Override
	public IRPlusResponseDetails updateRolePermission(RolepermissionsBean rolepermissionsBean)
			throws BusinessException {
	
		return iRolePermissionDao.updateRolePermission(rolepermissionsBean);
	}

	@Override
	public IRPlusResponseDetails deleteRolePermissionById(String rolePermissionId) throws BusinessException {
	
		return iRolePermissionDao.deleteRolePermissionById(rolePermissionId);
	}

	@Override
	public IRPlusResponseDetails showAllRolePermission() throws BusinessException {
	
		return iRolePermissionDao.showAllRolePermission();
	}

	@Override
	public IRPlusResponseDetails updateStatusRolePrmn(StatusIdInfo statusIdInfo) throws BusinessException {
		return iRolePermissionDao.updateStatusRolePrmn(statusIdInfo);
	}

	@Override
	public IRPlusResponseDetails showAllRoleNMdl(StatusIdInfo statusIdInfo) throws BusinessException {

		return iRolePermissionDao.showAllRoleNMdl(statusIdInfo);
	}

	@Override
	public IRPlusResponseDetails updateRoleprmUpdNCrt(RolePrmUpdateNCreateInfo rolepermUpNCrte)	throws BusinessException {

		return iRolePermissionDao.updateRoleprmUpdNCrt(rolepermUpNCrte);
	}
	
}