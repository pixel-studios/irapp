package com.irplus.dao.client.customer_licence;

import com.irplus.dto.CustomerLicenceInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

public interface ICustomerLicencingDaoClient {

	 	public IRPlusResponseDetails createCustomerLicence(CustomerLicenceInfo  customerLicenceInfo) throws BusinessException;

		public IRPlusResponseDetails getCustomerLicenceById(String customerLicenceId) throws BusinessException;
		
		public IRPlusResponseDetails updateCustomerLicence(CustomerLicenceInfo  customerLicenceInfo) throws BusinessException;

		public IRPlusResponseDetails deleteCustomerLicenceById(String customerLicenceId) throws BusinessException;
		
		public IRPlusResponseDetails showAllCustomerLicences() throws BusinessException;
		
}
