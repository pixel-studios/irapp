package com.irplus.dao.client.manageForms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.manageForms.IManagmntFormsetupDao;
import com.irplus.dto.FormsManagementDTO;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.util.BusinessException;


@Component
public class ManagmntFormsetupDaoClientImpl implements IManagmntFormsetupDaoClient{

	@Autowired
	IManagmntFormsetupDao iManagmntFormsetupDao;
	
	@Override
	public IRPlusResponseDetails createMngmntformSetup(FormsManagementDTO formsManagementDTO) throws BusinessException {

		return iManagmntFormsetupDao.createMngmntformSetup(formsManagementDTO);
	}

	@Override
	public IRPlusResponseDetails dynamicSelectFieldTypes() throws BusinessException {

		return iManagmntFormsetupDao.dynamicSelectFieldTypes();
	}

	@Override
	public IRPlusResponseDetails showOneMngmntformSetup(String id) throws BusinessException {

		return iManagmntFormsetupDao.showOneMngmntformSetup(id);
	}

	@Override
	public IRPlusResponseDetails updateMngmntformSetup(FormsManagementDTO formsManagementDTO) throws BusinessException {

		return iManagmntFormsetupDao.updateMngmntformSetup(formsManagementDTO);
	}

	@Override
	public IRPlusResponseDetails statusUpdate(StatusIdInfo statusIdInfo) throws BusinessException {
		
		return iManagmntFormsetupDao.statusUpdate(statusIdInfo);
	}

	@Override
	public IRPlusResponseDetails showListofForms()throws BusinessException {

		return iManagmntFormsetupDao.showListofForms();
	}

	@Override
	public IRPlusResponseDetails showAllCustomerForms(Integer customerid) throws BusinessException {

		return iManagmntFormsetupDao.showAllCustomerForms(customerid);
	}
}
