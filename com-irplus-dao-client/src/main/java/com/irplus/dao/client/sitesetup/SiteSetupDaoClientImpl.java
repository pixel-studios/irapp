package com.irplus.dao.client.sitesetup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.sitesetup.SiteSetupDao;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.SiteInfo;
import com.irplus.util.BusinessException;

@Component
public class SiteSetupDaoClientImpl implements SiteSetupDaoClient {

	@Autowired
	SiteSetupDao siteSetupDao;
	
	@Override
	public IRPlusResponseDetails getSiteInfo(String siteId) throws BusinessException {
		return siteSetupDao.getSiteInfo(siteId);
	}

	@Override
	public IRPlusResponseDetails updateSite(SiteInfo siteInfo) throws BusinessException {
		return siteSetupDao.updateSite(siteInfo);
	}

	@Override
	public IRPlusResponseDetails updateFolder(SiteInfo siteInfo) throws BusinessException {
		return siteSetupDao.updateFolder(siteInfo);
	}

	@Override
	public IRPlusResponseDetails licenseValidity() throws BusinessException {
		return siteSetupDao.licenseValidity();
	}

	@Override
	public IRPlusResponseDetails siteInfo() throws BusinessException {
		// TODO Auto-generated method stub
		return siteSetupDao.siteInfo();
	}

}
