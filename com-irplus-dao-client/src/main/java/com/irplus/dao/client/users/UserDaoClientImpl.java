package com.irplus.dao.client.users;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.users.IUserDao;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.UserFilterInfo;
import com.irplus.dto.users.UserBean;
import com.irplus.util.BusinessException;

@Component
public class UserDaoClientImpl implements IUserDaoClient{

	private static final Log log=LogFactory.getLog(UserDaoClientImpl.class); 
	@Autowired 
	IUserDao userDao;
	
	@Override
	public IRPlusResponseDetails createUser(UserBean userBeanInfo) throws BusinessException {
		
		log.info("Inside UserDaoClientImpl :: createUser");
		
		return userDao.createUser(userBeanInfo);
	}

	@Override
	public IRPlusResponseDetails getUserById(String userId) throws BusinessException {

		log.info("Inside UserDaoClientImpl :: GetOneUser");
		return userDao.getUserById(userId);
	}

	@Override
	public IRPlusResponseDetails updateUser(UserBean userBeanInfo) throws BusinessException {
		log.info("Inside UserDaoClientImpl :: updateUser");
		return userDao.updateUser(userBeanInfo);
	}

	@Override
	public IRPlusResponseDetails deleteUserById(String userId) throws BusinessException {
		log.info("Inside UserDaoClientImpl :: DeleteUser");
		return userDao.deleteUserById(userId);
	}

	@Override
	public IRPlusResponseDetails findAllUsers(String siteId) throws BusinessException {
		log.info("Inside UserDaoClientImpl :: ShowAllUsers");
		return userDao.findAllUsers(siteId);
	}

	@Override
	public IRPlusResponseDetails getAll_Role_Banks(String siteId) throws BusinessException {
		log.info("Inside UserDaoClientImpl :: ShowAllUsers");
		return userDao.getAll_Role_Banks(siteId);
	}

	@Override
	public IRPlusResponseDetails updateUserAndBank(UserBean userBeanInfo) throws BusinessException {

		log.info("Inside UserDaoClientImpl :: ShowAllUsers");
		return userDao.updateUserAndBank(userBeanInfo);
	}

	@Override
	public IRPlusResponseDetails createUserNoDup(UserBean userBean) throws BusinessException {

		log.info("Inside UserDaoClientImpl :: ShowAllUsers");
		return userDao.createUserNoDup(userBean);
	}

	@Override
	public IRPlusResponseDetails updateStatusUser(UserBean userBeanInfo) throws BusinessException {
		log.info("Inside UserDaoClientImpl :: ShowAllUsers");
		return userDao.updateStatusUser(userBeanInfo);
	}

	@Override
	public IRPlusResponseDetails filterUsers(UserFilterInfo userFilterInfo) throws BusinessException {
	
		
		return userDao.filterUsers(userFilterInfo);
	}
	public IRPlusResponseDetails validateUser(UserBean userBean) throws BusinessException
	{
		log.info("Inside UserDaoClientImpl :: ShowAllUsers");
		return userDao.validateUser(userBean);
	}
	
	@Override
	public IRPlusResponseDetails fetchMenu(int roleId)  throws BusinessException
	{
		log.info("Inside UserDaoClientImpl :: fetchMenu");
		return userDao.fetchMenu(roleId);
	}

	@Override
	public IRPlusResponseDetails getOtherBank(String bankId) throws BusinessException {
		// TODO Auto-generated method stub
		return userDao.getOtherBank(bankId);
	}

	@Override
	public IRPlusResponseDetails getUserBank(String userId) throws BusinessException {
		// TODO Auto-generated method stub
		return userDao.getUserBank(userId);
	}
	@Override
	public IRPlusResponseDetails AutocompleteUsername(String term,String siteId) throws BusinessException {
		// TODO Auto-generated method stub
		return userDao.AutocompleteUsername(term,siteId);
	}
	
}