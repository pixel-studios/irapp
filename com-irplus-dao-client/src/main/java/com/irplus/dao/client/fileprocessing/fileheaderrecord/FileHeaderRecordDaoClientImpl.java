package com.irplus.dao.client.fileprocessing.fileheaderrecord;

import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.irplus.dao.fileprocessing.fileheaderrecord.FileHeaderRecordDao;
import com.irplus.dto.CustomerGroupingMngmntInfo;
import com.irplus.dto.FileTypeInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.fileprocessing.FileHeaderRecordBean;
import com.irplus.util.BusinessException;

@Component
public class FileHeaderRecordDaoClientImpl implements FileHeaderRecordDaoClient{
	
	private static final Logger LOGGER = Logger.getLogger(FileHeaderRecordDaoClientImpl.class);
	
	@Autowired
	private FileHeaderRecordDao fileHeaderRecordDao;
	
	@Override
	public Boolean insertRecord(FileHeaderRecordBean fileHeaderRecord) {
		LOGGER.info("Inside FileHeaderRecordDaoClientImpl :: insertRecord");
		return fileHeaderRecordDao.insertRecord(fileHeaderRecord);
	}

	@Override
	public Boolean deleteRecord(FileHeaderRecordBean fileHeaderRecord) {
		LOGGER.info("Inside FileHeaderRecordDaoClientImpl :: deleteRecord");
		return fileHeaderRecordDao.deleteRecord(fileHeaderRecord);
	}

	@Override
	public IRPlusResponseDetails listAllRecords(String userId)throws BusinessException{
		LOGGER.info("Inside FileHeaderRecordDaoClientImpl :: listAllRecords");
		return fileHeaderRecordDao.listAllRecords(userId);
	}

	@Override
	public IRPlusResponseDetails getRecordByBatchId(Integer fileId) throws BusinessException{
		LOGGER.info("Inside FileHeaderRecordDaoClientImpl :: getRecordById");
		return fileHeaderRecordDao.getRecordByBatchId(fileId);
	}

	@Override
	public Boolean updateRecord(FileHeaderRecordBean fileHeaderRecord) {
		LOGGER.info("Inside FileHeaderRecordDaoClientImpl :: updateRecord");
		return fileHeaderRecordDao.updateRecord(fileHeaderRecord);
	}
	@Override
	public IRPlusResponseDetails getMasterDataChart(Integer userId,String siteId) throws BusinessException{
		LOGGER.info("Inside FileHeaderRecordDaoClientImpl :: getRecordById");
		return fileHeaderRecordDao.getMasterDataChart(userId,siteId);
	}
	@Override
	public IRPlusResponseDetails DaywiseFiles(CustomerGroupingMngmntInfo fileheadrec) throws BusinessException{
		LOGGER.info("Inside FileHeaderRecordDaoClientImpl :: getRecordById");
		return fileHeaderRecordDao.DaywiseFiles(fileheadrec);
	}
	
	@Override
	public IRPlusResponseDetails DaywiseFilesGroup(CustomerGroupingMngmntInfo fileheadrec) throws BusinessException{
		LOGGER.info("Inside FileHeaderRecordDaoClientImpl :: getRecordById");
		return fileHeaderRecordDao.DaywiseFilesGroup(fileheadrec);
	}
	@Override
	public IRPlusResponseDetails BankwiseFileList(CustomerGroupingMngmntInfo fileinfo) throws BusinessException{
		LOGGER.info("Inside FileHeaderRecordDaoClientImpl :: getRecordById");
		return fileHeaderRecordDao.BankwiseFileList(fileinfo);
	}
	@Override
	public IRPlusResponseDetails listfileLog() throws BusinessException {
		// TODO Auto-generated method stub
		return fileHeaderRecordDao.listfileLog();
	}
	@Override
	public IRPlusResponseDetails GroupwiseFileList(CustomerGroupingMngmntInfo Grpinfo) throws BusinessException{
		LOGGER.info("Inside FileHeaderRecordDaoClientImpl :: getRecordById");
		return fileHeaderRecordDao.GroupwiseFileList(Grpinfo);
	}
	
	@Override
	public IRPlusResponseDetails filterFiles(FileTypeInfo Grpinfo) throws BusinessException{
		LOGGER.info("Inside FileHeaderRecordDaoClientImpl :: getRecordById");
		return fileHeaderRecordDao.filterFiles(Grpinfo);
	}
}
