package com.irplus.dao.client.customer_reporting;

import com.irplus.dto.BankReportingsInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.customerreporting.CustomerRepotingBean;
import com.irplus.dto.customerreporting.CustomerRepotingResponseDetails;
import com.irplus.util.BusinessException;

public interface ICustomerReportingDaoClient {

	 	public IRPlusResponseDetails createCustomerReport(CustomerRepotingBean customerReportingBean) throws BusinessException;

		public IRPlusResponseDetails getCustomerReportById(String custommerReportingId) throws BusinessException;
		
		public CustomerRepotingResponseDetails updateCustomerReports(CustomerRepotingBean customerReportingBean) throws BusinessException;

		public CustomerRepotingResponseDetails deleteCustomerReportById(String custommerReportingId) throws BusinessException;
		
		public CustomerRepotingResponseDetails showAllCustomerReports() throws BusinessException;
		
		public IRPlusResponseDetails dynamicCustomerReportsBybankId(BankReportingsInfo banksReportsInfo)throws BusinessException;
		
		
}
