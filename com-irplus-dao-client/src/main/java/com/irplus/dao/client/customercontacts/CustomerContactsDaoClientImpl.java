package com.irplus.dao.client.customercontacts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.customer_contacts.ICustomerContactsDao;
import com.irplus.dto.CustomeContactsArrayInfo;
import com.irplus.dto.CustomerBean;
import com.irplus.dto.CustomerContactsBean;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

@Component
public class CustomerContactsDaoClientImpl implements ICustomerContactsDaoClient {

	@Autowired
	ICustomerContactsDao iCustomerContactsDao;
	
	/*@Override
	public IRPlusResponseDetails createCustomerContacts(CustomerContactsBean customerContactsBean)
			throws BusinessException {
		return iCustomerContactsDao.createCustomerContacts(customerContactsBean);
	}*/

	@Override
	public IRPlusResponseDetails getCustomerContactsById(CustomerBean customerBean) throws BusinessException {
		return iCustomerContactsDao.getCustomerContactsById(customerBean);
	}

	@Override
	public IRPlusResponseDetails updateCustomerConstatnts(CustomerContactsBean customerContactsBean)
			throws BusinessException {
		return iCustomerContactsDao.updateCustomerConstatnts(customerContactsBean);
	}

	@Override
	public IRPlusResponseDetails deleteCustomerContactsById(String customerContactsId) throws BusinessException {
		return iCustomerContactsDao.deleteCustomerContactsById(customerContactsId);
	}

	@Override
	public IRPlusResponseDetails showAllCustomerContacts() throws BusinessException {

		return iCustomerContactsDao.showAllCustomerContacts();
	}

	@Override
	public IRPlusResponseDetails updateCustomerConstactsOnly(CustomeContactsArrayInfo customeContactsArrayInfo)
			throws BusinessException {
		
		return iCustomerContactsDao.updateCustomerConstactsOnly(customeContactsArrayInfo);
	}

	@Override
	public IRPlusResponseDetails createCustomerContacts(CustomerContactsBean customerContactsBean)
			throws BusinessException {

		return iCustomerContactsDao.createCustomerContacts(customerContactsBean);
	}

	@Override
	public IRPlusResponseDetails addContacts(CustomerContactsBean[] customerContactsBean) throws BusinessException {
		return iCustomerContactsDao.addContacts(customerContactsBean);
	}


}