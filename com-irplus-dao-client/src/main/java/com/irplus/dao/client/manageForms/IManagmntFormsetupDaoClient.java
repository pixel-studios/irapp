package com.irplus.dao.client.manageForms;

import com.irplus.dto.FormsManagementDTO;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.util.BusinessException;

public interface IManagmntFormsetupDaoClient {
		

    public IRPlusResponseDetails createMngmntformSetup(FormsManagementDTO formsManagementDTO) throws BusinessException;
    
    public IRPlusResponseDetails dynamicSelectFieldTypes()throws BusinessException;
    
	public IRPlusResponseDetails showOneMngmntformSetup(String id) throws BusinessException ;
	
	public IRPlusResponseDetails updateMngmntformSetup(FormsManagementDTO formsManagementDTO) throws BusinessException ;
	
	public IRPlusResponseDetails statusUpdate(StatusIdInfo statusIdInfo)throws BusinessException ;
	
	public IRPlusResponseDetails showListofForms()throws BusinessException ;
	public IRPlusResponseDetails showAllCustomerForms(Integer customerid) throws BusinessException;

	/*
	public IRPlusResponseDetails getMenu(String menuid) throws BusinessException;
	
	public IRPlusResponseDetails updateMenu(AddMenu menuInfo) throws BusinessException;

	public IRPlusResponseDetails deleteMenu(String menuId) throws BusinessException;

	public IRPlusResponseDetails findAllMenus() throws BusinessException;

	public IRPlusResponseDetails updateMenuStatusPrm(StatusIdInfo siInfo) throws BusinessException ;
	
	public IRPlusResponseDetails createMenuWithoutDpct(AddMenu menuInfo) throws BusinessException ;*/
	
}
