package com.irplus.dao.client.bankmgmt;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.bankbranchdao.IBankBranchMgmtDao;
import com.irplus.dao.bankdao.IBankMgmtDao;
import com.irplus.dto.BankFilterInfo;
import com.irplus.dto.BankInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

@Component
public class BankMgmtDaoClientImpl implements IBankMgmtDaoClient {

	@Autowired
    IBankMgmtDao iBankMgmtDao;
	
	@Autowired
    IBankBranchMgmtDao iBankBranchMgmtDao;
	
	

	
	private static final Logger LOGGER = Logger.getLogger(BankMgmtDaoClientImpl.class);
	
	@Override
	public IRPlusResponseDetails createBank(BankInfo bankInfo) throws BusinessException {
        
		LOGGER.info("Inside BankMgmtDaoClientImpl :: createBank");

        return iBankMgmtDao.createBank(bankInfo);
        
	}

	@Override
	public IRPlusResponseDetails listBanks(String siteId) throws BusinessException {

		LOGGER.info("Inside BankMgmtDaoClientImpl :: listBanks");

        return iBankMgmtDao.listBanks(siteId);
	}
	@Override
	public IRPlusResponseDetails filterBanks(BankFilterInfo bankFilterInfo) throws BusinessException
	{
		LOGGER.info("Inside BankMgmtDaoClientImpl :: filterBanks");

        return iBankMgmtDao.filterBanks(bankFilterInfo);
	}
	
	@Override
	public IRPlusResponseDetails updateBank(BankInfo bankInfo) throws BusinessException {
        
		LOGGER.info("Inside BankMgmtDaoClientImpl :: updateBank");

        return iBankMgmtDao.updateBank(bankInfo);
        
	}
	@Override
	public IRPlusResponseDetails updateBankStatus(BankInfo bankInfo) throws BusinessException {
        
		LOGGER.info("Inside BankMgmtDaoClientImpl :: updateBankStatus");

        return iBankMgmtDao.updateBankStatus(bankInfo);
        
	}
	
	@Override
	public IRPlusResponseDetails updateBranchStatus(BankInfo bankInfo) throws BusinessException {
		
		return iBankMgmtDao.updateBranchStatus(bankInfo);
	}
	
	
	@Override
	public IRPlusResponseDetails getBankInfo(String bankId) throws BusinessException
	{
		LOGGER.info("Inside BankMgmtDaoClientImpl :: getBankInfo");

        return iBankMgmtDao.getBankInfo(bankId);
	}
	
	@Override
	public IRPlusResponseDetails getBranchInfo(BankInfo bankInfo)  throws BusinessException
	{
	
		LOGGER.info("Inside BankMgmtDaoClientImpl :: getBranchInfo");

        return iBankBranchMgmtDao.getBranchInfo(bankInfo);

	}
	
	@Override
	public IRPlusResponseDetails getBranchInfo(Long branchId)  throws BusinessException
	{
	
		LOGGER.info("Inside BankMgmtDaoClientImpl :: getBranchInfo");

        return iBankBranchMgmtDao.getBranchInfo(branchId);

	}
	
	@Override
	public IRPlusResponseDetails deleteBank(BankInfo bankInfo) throws BusinessException {

		LOGGER.info("Inside BankMgmtDaoClientImpl :: deleteBank");

        return iBankMgmtDao.deleteBank(bankInfo);
	}

	@Override
	public IRPlusResponseDetails createBankBranch(BankInfo bankInfo) throws BusinessException {
		
		LOGGER.info("Inside BankMgmtDaoClientImpl :: createBankBranch");

        return iBankBranchMgmtDao.createBankBranch(bankInfo);
	}

	@Override
	public IRPlusResponseDetails listBankBranches(String siteId) throws BusinessException {
		LOGGER.info("Inside BankMgmtDaoClientImpl :: listBankBranches");

        return iBankBranchMgmtDao.listBankBranches(siteId);
	}

	@Override
	public IRPlusResponseDetails updateBranch(BankInfo bankInfo) throws BusinessException {
		
		LOGGER.info("Inside BankMgmtDaoClientImpl :: updateBranch");

        return iBankBranchMgmtDao.updateBranch(bankInfo);
	}

	@Override
	public IRPlusResponseDetails deleteBranch(BankInfo bankInfo) throws BusinessException {
		
		LOGGER.info("Inside BankMgmtDaoClientImpl :: deleteBranch");

        return iBankBranchMgmtDao.deleteBranch(bankInfo);
	}
	
	@Override
	
	public IRPlusResponseDetails addBankLicense(BankInfo bankInfo) throws BusinessException
	{
		LOGGER.info("Inside BankMgmtDaoClientImpl :: deleteBranch");

        return iBankMgmtDao.addBankLicense(bankInfo);
		
	}
	
	@Override
	public IRPlusResponseDetails filterBranches(BankFilterInfo bankFilterInfo) throws BusinessException
	{
		LOGGER.info("Inside BankMgmtDaoClientImpl :: filterBanks");

        return iBankMgmtDao.filterBranches(bankFilterInfo);
	}



}
