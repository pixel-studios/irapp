package com.irplus.dao.client.customerMappingMngmnt;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.customerMappingMngmnt.ICustomerMappingDao;
import com.irplus.dto.CustomerMapSupportInfo;
import com.irplus.dto.CustomerMappingInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.util.BusinessException;

@Component
public class CustomerMappingDaoClientImpl implements ICustomerMappingDaoClient{

	private static final Logger log = Logger.getLogger(CustomerMappingDaoClientImpl.class);
	
	@Autowired 
	ICustomerMappingDao iCustomerMappingDao;

	@Override
	public IRPlusResponseDetails getCustomerMappingById(String custommerId) throws BusinessException {

		return iCustomerMappingDao.getCustomerMappingById(custommerId);
		
	}

	@Override
	public IRPlusResponseDetails updateCustomerMapping(CustomerMappingInfo customerMappingInfo)
			throws BusinessException {

		return iCustomerMappingDao.updateCustomerMapping(customerMappingInfo);
		
	}

	@Override
	public IRPlusResponseDetails showAllCustomerMapping() throws BusinessException {

		return iCustomerMappingDao.showAllCustomerMapping();
		
	}

	@Override
	public IRPlusResponseDetails statusUpdateCustmrMpng(StatusIdInfo statusIdInfo) throws BusinessException {

		return iCustomerMappingDao.statusUpdateCustmrMpng(statusIdInfo);
		
	}

	@Override
	public IRPlusResponseDetails createCustomerMapping(CustomerMapSupportInfo customerMapSupportInfo)
			throws BusinessException {
		return iCustomerMappingDao.createCustomerMapping(customerMapSupportInfo);
	}
	@Override
	public IRPlusResponseDetails CustomerBankingBanks(String SiteId)
			throws BusinessException {
		return iCustomerMappingDao.CustomerBankingBanks(SiteId);
	}
	
	@Override
	public IRPlusResponseDetails showBranchFile(String branchId)
			throws BusinessException {
		return iCustomerMappingDao.showBranchFile(branchId);
	}
	@Override
	public IRPlusResponseDetails showBranchCustomer(String branchId)
			throws BusinessException {
		return iCustomerMappingDao.showBranchCustomer(branchId);
	}
	
	
	@Override
	public IRPlusResponseDetails createCustomerMappingFields(CustomerMappingInfo[] customerMappingInfo)
			throws BusinessException {

		return iCustomerMappingDao.createCustomerMappingFields(customerMappingInfo);
		
	}
	
	@Override
	public IRPlusResponseDetails showAllCustomerMappedFields() throws BusinessException {

		return iCustomerMappingDao.showAllCustomerMappedFields();
		
	}
}