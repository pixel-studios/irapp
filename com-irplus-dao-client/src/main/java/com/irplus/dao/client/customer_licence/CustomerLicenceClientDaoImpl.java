package com.irplus.dao.client.customer_licence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.customer_licencing.ICustomerLicencingDao;
import com.irplus.dto.CustomerLicenceInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

@Component
public class CustomerLicenceClientDaoImpl implements ICustomerLicencingDaoClient{

	@Autowired
	ICustomerLicencingDao iCustomerLicencingDao;
	
	@Override
	public IRPlusResponseDetails createCustomerLicence(CustomerLicenceInfo customerLicenceInfo)
			throws BusinessException {

		return iCustomerLicencingDao.createCustomerLicence(customerLicenceInfo);
	}

	@Override
	public IRPlusResponseDetails getCustomerLicenceById(String customerLicenceId) throws BusinessException {

		return iCustomerLicencingDao.getCustomerLicenceById(customerLicenceId);
	}

	@Override
	public IRPlusResponseDetails updateCustomerLicence(CustomerLicenceInfo customerLicenceInfo)
			throws BusinessException {

		return iCustomerLicencingDao.updateCustomerLicence(customerLicenceInfo);
	}

	@Override
	public IRPlusResponseDetails deleteCustomerLicenceById(String customerLicenceId) throws BusinessException {

		return iCustomerLicencingDao.deleteCustomerLicenceById(customerLicenceId);
	}

	@Override
	public IRPlusResponseDetails showAllCustomerLicences() throws BusinessException {

		return iCustomerLicencingDao.showAllCustomerLicences();
	}

}
