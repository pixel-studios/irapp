package com.irplus.dao.client.report_file_formats;

import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.ReportFileFormatInfo;
import com.irplus.util.BusinessException;

public interface IReportFileFormatsDaoClient {
		
    public IRPlusResponseDetails createReportFileFormat(ReportFileFormatInfo reportFileFormatInfo) throws BusinessException;

	public IRPlusResponseDetails getReportFileFormatById(String reportFileFormatInfoId) throws BusinessException;		

	public IRPlusResponseDetails updateReportFileFormat(ReportFileFormatInfo reportFileFormatInfo) throws BusinessException;

	public IRPlusResponseDetails deleteReportFileFormatById(String reportFileFormatInfoId) throws BusinessException;
		
	public IRPlusResponseDetails showAllReportFileFormat() throws BusinessException;
	
}
