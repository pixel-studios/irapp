package com.irplus.dao.client.module_menu;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.module_menu.IModuleMenusDao;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.menu_modules_association.ModuleMenuBean;
import com.irplus.util.BusinessException;

@Component

public class ModuleMenusDaoClientImpl implements IModuleMenusDaoClient {

	private static final Logger log = Logger.getLogger(ModuleMenusDaoClientImpl.class);

	@Autowired 
	IModuleMenusDao moduleMenuDao;
	
	@Override
	public IRPlusResponseDetails createMenuModule(ModuleMenuBean moduleMenuInfo) throws BusinessException {

		return moduleMenuDao.createMenuModule(moduleMenuInfo);
	}

	@Override
	public IRPlusResponseDetails getMenuModuleById(String moduleMenuId) throws BusinessException {

		return moduleMenuDao.getMenuModuleById(moduleMenuId);
	}

	@Override
	public IRPlusResponseDetails updateMenuModule(ModuleMenuBean moduleMenuInfo) throws BusinessException {
		
		return moduleMenuDao.updateMenuModule(moduleMenuInfo);
	}

	@Override
	public IRPlusResponseDetails deleteMenuModuleById(String moduleMenuId) throws BusinessException {
		
		return moduleMenuDao.deleteMenuModuleById(moduleMenuId);
	}

	@Override
	public IRPlusResponseDetails findAllMenuModule() throws BusinessException {
		return moduleMenuDao.findAllMenuModule();
	}

	@Override
	public IRPlusResponseDetails findAllByStatus() throws BusinessException {		
		return moduleMenuDao.findAllByStatus();
	}

	@Override
	public IRPlusResponseDetails UpdateStatus(StatusIdInfo sidInfo) throws BusinessException {		
		return moduleMenuDao.UpdateStatus(sidInfo);
	}

	@Override
	public IRPlusResponseDetails DeleteUpdateStatus(StatusIdInfo sidInfo) throws BusinessException {		
		return moduleMenuDao.DeleteUpdateStatus(sidInfo);
	}

	@Override
	public IRPlusResponseDetails createMenuModuleNoDuplicate(ModuleMenuBean moduleMenuInfo) throws BusinessException {
		return moduleMenuDao.createMenuModuleNoDuplicate(moduleMenuInfo);
	}

	
}